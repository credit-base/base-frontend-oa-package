<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand;
use Sdk\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand;
use Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize\WebsiteCustomizeCommandHandlerFactory;

class WebsiteCustomizeOperationController extends Controller implements IOperateAbleController
{
    use WebTrait,
        UtilsTrait,
        OperateControllerTrait,
        WebsiteCustomizeValidateTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new WebsiteCustomizeCommandHandlerFactory());
    }

    protected function getCommandBus():CommandBus
    {
        return $this->commandBus;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function checkUserHasPurview(string $resource = '') : bool
    {
        unset($resource);
        return $this->checkUserHasWebsiteCustomizePurview();
    }

    protected function addAction()
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview()) {
                return false;
            }
        }
    
        $request = $this->getRequest();
        
        $category =  marmot_decode($request->post('category', ''));
        $status = marmot_decode($request->post('status', ''));
        $content =  $request->post('content', array());
        
        $content = $this->getFormatContentByDecode($content);
        
        if ($this->validateAddScenario(
            $category,
            $status,
            $content
        )) {
            $command = new AddWebsiteCustomizeCommand(
                $category,
                $status,
                $content
            );
        
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    public function publish(string $id):bool
    {
        $id = marmot_decode($id);

        $command = new PublishWebsiteCustomizeCommand($id);
        
        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }

    const TOOL_ITEMS = [
        'nav','footerNav',
        'footerTwo','footerThree',
        'footerBanner','headerSearch',
        'animateWindow','rightToolBar',
        'headerBarLeft','leftFloatCard',
        'specialColumn','headerBarRight',
        'organizationGroup'
    ];

    const STATUS_ITEMS = [
        'silhouette','frameWindow',
        'centerDialog','partyAndGovernmentOrgans',
        'governmentErrorCorrection'
    ];

    protected function getFormatContentByDecode(array $content):array
    {
        foreach (self::TOOL_ITEMS as $value) {
            if (isset($content[$value])) {
                $content[$value] = $this->getItemsListByDecode($content[$value]);
            }
        }

        foreach (self::STATUS_ITEMS as $val) {
            if (isset($content[$val])) {
                $content[$val] = $this->getDataStatusByDecode($content[$val]);
            }
        }

        if (isset($content['memorialStatus'])) {
            $content['memorialStatus'] = marmot_decode($content['memorialStatus']);
        }

        if (isset($content['relatedLinks'])) {
            $content['relatedLinks'] = $this->getRelatedLinksByDecode($content['relatedLinks']);
        }

        return $content;
    }

    protected function getItemsListByDecode(array $list):array
    {
        foreach ($list as $key => $val) {
            $val['status'] = marmot_decode($val['status']);

            if (isset($val['category'])) {
                $val['category'] = marmot_decode($val['category']);
            }

            if (isset($val['type'])) {
                $val['type'] = marmot_decode($val['type']);
            }
            $list[$key] = $val;
        }

        return $list;
    }

    protected function getDataStatusByDecode(array $data):array
    {
        $data['status'] = marmot_decode($data['status']);

        return $data;
    }

    protected function getRelatedLinksByDecode(array $relatedLinks):array
    {
        foreach ($relatedLinks as $key => $val) {
            $val['status'] = marmot_decode($val['status']);
            if (!empty($val['items'])) {
                $val['items'] = $this->getItemsListByDecode($val['items']);
            }

            $relatedLinks[$key] = $val;
        }

        return $relatedLinks;
    }
}
