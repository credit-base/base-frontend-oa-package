<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Marmot\Core;
use Sdk\WebsiteCustomize\WidgetRules\WebsiteCustomizeWidgetRules;
use Sdk\Crew\Model\Crew;

trait WebsiteCustomizeValidateTrait
{
    protected function getWebsiteCustomizeWidgetRules() : WebsiteCustomizeWidgetRules
    {
        return new WebsiteCustomizeWidgetRules();
    }

    protected function validateAddScenario(
        $category,
        $status,
        $content
    ) : bool {
        $websiteCustomizeWidgetRules = $this->getWebsiteCustomizeWidgetRules();
      
        return $websiteCustomizeWidgetRules->category($category, 'category')
            && $websiteCustomizeWidgetRules->status($status, 'status')
            && $websiteCustomizeWidgetRules->content($category, $content);
    }

    protected function checkUserHasWebsiteCustomizePurview() : bool
    {
        $crewCategory = Core::$container->get('crew')->getCategory();
        
        if ($crewCategory != Crew::CATEGORY['SUPERTUBE'] &&
            $crewCategory != Crew::CATEGORY['PLATFORM_ADMINISTRATORS']) {
            Core::setLastError(PURVIEW_UNDEFINED);
            return false;
        }

        return true;
    }
}
