<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\WebsiteCustomize\View\Json\View;
use Base\Package\WebsiteCustomize\View\Json\ListView;

class WebsiteCustomizeFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, WebsiteCustomizeValidateTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WebsiteCustomizeRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : WebsiteCustomizeRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview()) {
                return false;
            }
        }
       
        list($size,$page) = $this->getPageAndSize(COMMON_SIZES);
        list($filter, $sort) = $this->filterFormatChange();

        $websiteCustomizeList = array();

        list($count, $websiteCustomizeList) = $this->getRepository()
            ->scenario(WebsiteCustomizeRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($websiteCustomizeList, $count));
        return true;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-id');
        $version = $request->get('version', '');
        $category = $request->get('category', '');
        $status = $request->get('status', '');
        
        $filter = array();

        if (!empty($version)) {
            $filter['version'] = $version;
        }
        if (!empty($category)) {
            $filter['category'] =  marmot_decode($category);
        }
        if (!empty($status)) {
            $filter['status'] = marmot_decode($status);
        }
       
        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview()) {
                return false;
            }
        }
        
        $websiteCustomize = $this->getRepository()
                                 ->scenario(WebsiteCustomizeRepository::FETCH_ONE_MODEL_UN)
                                 ->fetchOne($id);
                                 
        // if ($websiteCustomize instanceof INull) {
        //     Core::setLastError(RESOURCE_NOT_EXIST);
        //     return false;
        // }
        
        $this->render(new View($websiteCustomize));
        return true;
    }

    public function customize(string $category):bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview()) {
                $this->displayError();
                return false;
            }
        }
       
        $websiteCustomize = $this->getRepository()
                                 ->customize($category);

        // if ($websiteCustomize instanceof INull) {
        //     Core::setLastError(RESOURCE_NOT_EXIST);
        //     return false;
        // }
        
        $this->render(new View($websiteCustomize));
        return true;
    }

    protected function checkUserHasPurview(string $resource = '') : bool
    {
        unset($resource);
        return $this->checkUserHasWebsiteCustomizePurview();
    }
}
