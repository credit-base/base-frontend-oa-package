<?php
namespace Base\Package\WebsiteCustomize\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;

class ListView extends JsonView implements IView
{
    private $websiteCustomize;

    private $count;

    private $translator;

    public function __construct(
        array $websiteCustomize,
        int $count
    ) {
        $this->websiteCustomize = $websiteCustomize;
        $this->count = $count;
        $this->translator = new WebsiteCustomizeTranslator();
        parent::__construct();
    }

    protected function getWebsiteCustomize(): array
    {
        return $this->websiteCustomize;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): WebsiteCustomizeTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getWebsiteCustomize() as $websiteCustomize) {
            $data[] = $this->getTranslator()->objectToArray(
                $websiteCustomize,
                array(
                    'id',
                    'version',
                    'status',
                    'createTime',
                    'updateTime',
                    'statusTime'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
