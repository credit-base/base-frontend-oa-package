<?php
namespace Base\Package\WebsiteCustomize\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Filter;

use Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;

class View extends JsonView implements IView
{
    protected $websiteCustomize;

    protected $translator;

    public function __construct($websiteCustomize)
    {
        $this->websiteCustomize = $websiteCustomize;
        $this->translator = new WebsiteCustomizeTranslator();
        parent::__construct();
    }

    protected function getWebsiteCustomize()
    {
        return $this->websiteCustomize;
    }

    protected function getTranslator(): WebsiteCustomizeTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getTranslator()->objectToArray(
            $this->getWebsiteCustomize()
        );

        if (!empty($data['content'])) {
            $data['content'] = $this->getFormatContentByEncode($data['content']);
        }

        $this->encode($data);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getFormatContentByEncode(array $content):array
    {
        if (isset($content['nav'])) {
            $content['nav'] = $this->getItemsListByEncode($content['nav']);
        }
        if (isset($content['footerNav'])) {
            $content['footerNav'] = $this->getItemsListByEncode($content['footerNav']);
        }
        if (isset($content['footerTwo'])) {
            $content['footerTwo'] = $this->getItemsListByEncode($content['footerTwo']);
        }
        if (isset($content['footerThree'])) {
            $content['footerThree'] = $this->getItemsListByEncode($content['footerThree']);
        }
        if (isset($content['footerBanner'])) {
            $content['footerBanner'] = $this->getItemsListByEncode($content['footerBanner']);
        }
        if (isset($content['headerSearch'])) {
            $content['headerSearch'] = $this->getItemsListByEncode($content['headerSearch']);
        }
        if (isset($content['animateWindow'])) {
            $content['animateWindow'] = $this->getItemsListByEncode($content['animateWindow']);
        }
        if (isset($content['headerBarLeft'])) {
            $content['headerBarLeft'] = $this->getItemsListByEncode($content['headerBarLeft']);
        }
        if (isset($content['leftFloatCard'])) {
            $content['leftFloatCard'] = $this->getItemsListByEncode($content['leftFloatCard']);
        }
        if (isset($content['specialColumn'])) {
            $content['specialColumn'] = $this->getItemsListByEncode($content['specialColumn']);
        }
        if (isset($content['headerBarRight'])) {
            $content['headerBarRight'] = $this->getItemsListByEncode($content['headerBarRight']);
        }
        if (isset($content['organizationGroup'])) {
            $content['organizationGroup'] = $this->getItemsListByEncode($content['organizationGroup']);
        }
        if (isset($content['rightToolBar'])) {
            $content['rightToolBar'] = $this->getItemsListByEncode($content['rightToolBar']);
        }
        
        if (isset($content['silhouette'])) {
            $content['silhouette'] = $this->getDataStatusByEncode($content['silhouette']);
        }
        if (isset($content['frameWindow'])) {
            $content['frameWindow'] = $this->getDataStatusByEncode($content['frameWindow']);
        }
        if (isset($content['centerDialog'])) {
            $content['centerDialog'] = $this->getDataStatusByEncode($content['centerDialog']);
        }
        if (isset($content['partyAndGovernmentOrgans'])) {
            $content['partyAndGovernmentOrgans'] = $this->getDataStatusByEncode($content['partyAndGovernmentOrgans']);
        }
        if (isset($content['governmentErrorCorrection'])) {
            $content['governmentErrorCorrection'] = $this->getDataStatusByEncode($content['governmentErrorCorrection']);
        }

        if (isset($content['memorialStatus'])) {
            $content['memorialStatus'] = marmot_encode($content['memorialStatus']);
        }

        if (isset($content['relatedLinks'])) {
            $content['relatedLinks'] = $this->getRelatedLinksByEncode($content['relatedLinks']);
        }

       

        return $content;
    }

    protected function getItemsListByEncode(array $list):array
    {
        foreach ($list as $key => $val) {
            $val['status'] = marmot_encode($val['status']);

            if (isset($val['category'])) {
                $val['category'] = marmot_encode($val['category']);
            }

            if (isset($val['type'])) {
                $val['type'] = marmot_encode($val['type']);
            }

            if (isset($val['description'])) {
                $val['description'] = Filter::dhtmlspecialchars($val['description']);
            }

            $list[$key] = $val;
        }

        return $list;
    }

    protected function getDataStatusByEncode(array $data):array
    {
        $data['status'] = marmot_encode($data['status']);

        return $data;
    }

    protected function getRelatedLinksByEncode(array $relatedLinks):array
    {
        foreach ($relatedLinks as $key => $val) {
            $val['status'] = marmot_encode($val['status']);
            if (!empty($val['items'])) {
                $val['items'] = $this->getItemsListByEncode($val['items']);
            }

            $relatedLinks[$key] = $val;
        }

        return $relatedLinks;
    }
}
