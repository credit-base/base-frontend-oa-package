<?php
namespace Base\Package\Enterprise\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Enterprise\Translator\EnterpriseTranslator;

class ListView extends JsonView implements IView
{
    private $enterprises;

    private $count;

    private $translator;

    public function __construct(
        array $enterprises,
        int $count
    ) {
        $this->enterprises = $enterprises;
        $this->count = $count;
        $this->translator = new EnterpriseTranslator();
        parent::__construct();
    }

    protected function getEnterprise(): array
    {
        return $this->enterprises;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): EnterpriseTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();
        $list = $this->getEnterprise();

        foreach ($list as $enterprise) {
            $data[] = $this->getTranslator()->objectToArray(
                $enterprise
            );
        }
       
        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
