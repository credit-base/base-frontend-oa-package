<?php
namespace Base\Package\Enterprise\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\Enterprise\Translator\EnterpriseTranslator;

use Base\Package\Statistical\View\StatisticalViewTrait;

class View extends JsonView implements IView
{
    use StatisticalViewTrait;

    const STATISTICAL = 'enterpriseRelationInformationCount';

    private $enterprise;

    private $translator;

    private $statisticalNumber;

    public function __construct($enterprise, $statisticalNumber)
    {
        $this->enterprise = $enterprise;
        $this->statisticalNumber = $statisticalNumber;
        $this->translator = new EnterpriseTranslator();
        parent::__construct();
    }

    protected function getEnterprise()
    {
        return $this->enterprise;
    }

    protected function getTranslator(): EnterpriseTranslator
    {
        return $this->translator;
    }

    protected function getStatisticalNumber()
    {
        return $this->statisticalNumber;
    }

    public function display(): void
    {
        $data = array();

        $data['enterprise'] = $this->getTranslator()->objectToArray(
            $this->getEnterprise()
        );
        
        if (!empty($this->getStatisticalNumber())) {
            $data['enterpriseRelationInformationCount'] = $this->getEnterpriseRelationStaticsData();
        }

        $this->encode($data);
    }

    protected function getEnterpriseRelationStaticsData()
    {
        $statisticalNumber = $this->statisticalArray(
            self::STATISTICAL,
            $this->getStatisticalNumber()
        );

        return $statisticalNumber;
    }
}
