<?php
namespace Base\Package\Enterprise\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Enterprise\Repository\EnterpriseRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Enterprise\View\Json\View;
use Base\Package\Enterprise\View\Json\ListView;

use Base\Package\Statistical\Controller\StatisticalControllerTrait;

class EnterpriseFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, StatisticalControllerTrait;

    const STATISTICAL = 'enterpriseRelationInformationCount';

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new EnterpriseRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : EnterpriseRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();
        
        list($count, $list) = $this->getRepository()
            ->scenario(EnterpriseRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
        
        $this->render(new ListView($list, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $identify = $request->get('identify', '');
        
        $filter = array();

        if (!empty($identify)) {
            $filter['identify'] = $identify;
        }

        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $enterprise = $this->getRepository()
            ->scenario(EnterpriseRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($enterprise instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $unifiedSocialCreditCode = $enterprise->getUnifiedSocialCreditCode();
        $statisticalNumber = $this->statisticalNumber($unifiedSocialCreditCode);
        
        $this->render(new View($enterprise, $statisticalNumber));
        return true;
    }

    protected function statisticalNumber(string $unifiedSocialCreditCode)
    {
        $filter['unifiedSocialCreditCode'] = $unifiedSocialCreditCode;
        $statistical = $this->statistical(self::STATISTICAL, $filter);

        return $statistical;
    }
}
