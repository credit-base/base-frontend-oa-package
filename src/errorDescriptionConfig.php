<?php

return array(
    CAPTCHA_ERROR =>
    array(
        'id' => CAPTCHA_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CAPTCHA_ERROR',
        'title'=>'验证码错误',
        'detail'=>'验证码错误',
        'source'=>array(
            'pointer'=>'captcha'
        ),
        'meta'=>array()
    ),
    NEED_SIGNIN=>
        array(
            'id'=>NEED_SIGNIN,
            'link'=>'',
            'status'=>403,
            'code'=>'NEED_SIGNIN',
            'title'=>'用户未登录',
            'detail'=>'用户未登录',
            'source'=>array(
            ),
            'meta'=>array()
        ),
);
