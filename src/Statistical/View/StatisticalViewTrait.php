<?php
namespace Base\Package\Statistical\View;

use Base\Sdk\Statistical\Translator\ITranslatorFactory;

trait StatisticalViewTrait
{
    protected function getStatisticalTranslator(string $type)
    {
        $translatorFactory = new ITranslatorFactory();
    
        $translator = $translatorFactory->getTranslator($type);
      
        return new $translator;
    }

    public function statisticalArray(string $type, $statistical)
    {
        $data = array();
    
        $translator = $this->getStatisticalTranslator($type);
      
        $data = $translator->objectToArray(
            $statistical
        );

        return $data;
    }
}
