<?php
namespace Base\Package\Statistical\Controller;

use Sdk\Statistical\Repository\Statistical\StatisticalRepository;
use Base\Sdk\Statistical\Adapter\Statistical\IAdapterFactory;

trait StatisticalControllerTrait
{
    protected function getStatisticalRepository(string $type) : StatisticalRepository
    {
       
        $adapterFactory = new IAdapterFactory();
        $adapter = $adapterFactory->getAdapter($type);
        
        return new StatisticalRepository(new $adapter);
    }

    public function statistical(string $type, array $filter = array())
    {
        $repository = $this->getStatisticalRepository($type);
    
        $statistical = $repository->analyse($filter);

        return $statistical;
    }
}
