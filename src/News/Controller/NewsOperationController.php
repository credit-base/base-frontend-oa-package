<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\News\Repository\NewsRepository;
use Sdk\News\Command\News\AddNewsCommand;
use Sdk\News\Command\News\EditNewsCommand;
use Sdk\News\Command\News\MoveNewsCommand;
use Sdk\News\CommandHandler\News\NewsCommandHandlerFactory;

class NewsOperationController extends Controller implements IOperateAbleController
{
    use WebTrait, UtilsTrait, OperateControllerTrait, NewsValidateTrait, NewsRequestCommonTrait;

    protected $repository;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new NewsCommandHandlerFactory());
        $this->repository = new NewsRepository();
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        $requestData = $this->getRequestCommonData();

        if ($this->validateCommonScenario(
            $requestData['title'],
            $requestData['source'],
            $requestData['cover'],
            $requestData['attachments'],
            $requestData['bannerImage'],
            $requestData['content'],
            $requestData['newsType'],
            $requestData['dimension'],
            $requestData['status'],
            $requestData['stick'],
            $requestData['bannerStatus'],
            $requestData['homePageShowStatus']
        )) {
            $command = new AddNewsCommand(
                $requestData['title'],
                $requestData['source'],
                $requestData['content'],
                $requestData['newsType'],
                $requestData['dimension'],
                $requestData['status'],
                $requestData['stick'],
                $requestData['bannerStatus'],
                $requestData['homePageShowStatus'],
                $requestData['cover'],
                $requestData['attachments'],
                $requestData['bannerImage']
            );
           
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        $requestData = $this->getRequestCommonData();
        
        if ($this->validateCommonScenario(
            $requestData['title'],
            $requestData['source'],
            $requestData['cover'],
            $requestData['attachments'],
            $requestData['bannerImage'],
            $requestData['content'],
            $requestData['newsType'],
            $requestData['dimension'],
            $requestData['status'],
            $requestData['stick'],
            $requestData['bannerStatus'],
            $requestData['homePageShowStatus']
        )) {
            $command = new EditNewsCommand(
                $requestData['title'],
                $requestData['source'],
                $requestData['content'],
                $requestData['newsType'],
                $requestData['dimension'],
                $requestData['status'],
                $requestData['stick'],
                $requestData['bannerStatus'],
                $requestData['homePageShowStatus'],
                $requestData['cover'],
                $requestData['attachments'],
                $requestData['bannerImage'],
                $id
            );
            
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function move(string $id, string $type)
    {
        $id = marmot_decode($id);
        $newType = marmot_decode($type);
        
        if ($this->validateMoveScenario($newType)) {
            $command = new MoveNewsCommand(
                $newType,
                $id
            );
            
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
