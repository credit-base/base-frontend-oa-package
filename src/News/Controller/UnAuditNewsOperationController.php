<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\News\Repository\UnAuditNewsRepository;
use Sdk\News\Command\UnAuditNews\EditUnAuditNewsCommand;
use Sdk\News\CommandHandler\UnAuditNews\UnAuditNewsCommandHandlerFactory;

class UnAuditNewsOperationController extends Controller implements IOperateAbleController
{
    use WebTrait, UtilsTrait, OperateControllerTrait, NewsValidateTrait, NewsRequestCommonTrait;

    protected $repository;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditNewsCommandHandlerFactory());
        $this->repository = new UnAuditNewsRepository();
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        $requestData = $this->getRequestCommonData();

        if ($this->validateCommonScenario(
            $requestData['title'],
            $requestData['source'],
            $requestData['cover'],
            $requestData['attachments'],
            $requestData['bannerImage'],
            $requestData['content'],
            $requestData['newsType'],
            $requestData['dimension'],
            $requestData['status'],
            $requestData['stick'],
            $requestData['bannerStatus'],
            $requestData['homePageShowStatus']
        )) {
            $command = new EditUnAuditNewsCommand(
                $requestData['title'],
                $requestData['source'],
                $requestData['content'],
                $requestData['newsType'],
                $requestData['dimension'],
                $requestData['status'],
                $requestData['stick'],
                $requestData['bannerStatus'],
                $requestData['homePageShowStatus'],
                $requestData['cover'],
                $requestData['attachments'],
                $requestData['bannerImage'],
                $id
            );
            
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        
        $this->displayError();
        return false;
    }
}
