<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Base\Package\Common\Controller\Traits\ToolTrait;

trait NewsRequestCommonTrait
{
    use ToolTrait;

    //@crewId 同理, 后续略
    protected function getRequestCommonData() : array
    {
        $request = $this->getRequest();
        $requestData = array();

        $requestData['title'] =  $request->post('title', '');
        $requestData['source'] =  $request->post('source', '');
        $requestData['cover'] =  $request->post('cover', array());
        $requestData['attachments'] =  $request->post('attachments', array());
        $requestData['bannerImage']=  $request->post('bannerImage', array());
        $content= $request->post('content', '');
        $requestData['content'] = htmlspecialchars_decode($this->formatString($content), ENT_QUOTES);
        
        $newsType = $request->post('newsType', '');
        $requestData['newsType'] = intval(marmot_decode($newsType));
        $dimension = $request->post('dimension', '');
        $requestData['dimension'] = intval(marmot_decode($dimension));
        $status = $request->post('status', 0);
        $requestData['status'] = intval(marmot_decode($status));
        $stick = $request->post('stick', 0);
        $requestData['stick'] = intval(marmot_decode($stick));
        $bannerStatus = $request->post('bannerStatus', 0);
        $requestData['bannerStatus'] = intval(marmot_decode($bannerStatus));
        $homePageShowStatus = $request->post('homePageShowStatus', 0);
        $requestData['homePageShowStatus'] = intval(marmot_decode($homePageShowStatus));
        
        return $requestData;
    }
}
