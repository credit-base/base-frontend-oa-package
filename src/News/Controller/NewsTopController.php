<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\TopControllerTrait;
use Base\Package\Common\Controller\Interfaces\ITopAbleController;

use Sdk\News\Command\News\TopNewsCommand;
use Sdk\News\Command\News\CancelTopNewsCommand;
use Sdk\News\CommandHandler\News\NewsCommandHandlerFactory;

class NewsTopController extends Controller implements ITopAbleController
{
    use WebTrait, TopControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new NewsCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function topAction(int $id) : bool
    {
        $command = new TopNewsCommand(
            $id
        );

        return $this->getCommandBus()->send($command);
    }

    protected function cancelTopAction(int $id) : bool
    {
        $command = new CancelTopNewsCommand(
            $id
        );

        return $this->getCommandBus()->send($command);
    }
}
