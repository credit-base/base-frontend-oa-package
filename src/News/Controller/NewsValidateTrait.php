<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\News\WidgetRules\NewsWidgetRules;
use Sdk\Crew\Model\Crew;

use Base\Sdk\Purview\Model\IPurviewAble;

trait NewsValidateTrait
{
    protected function getNewsWidgetRules() : NewsWidgetRules
    {
        return new NewsWidgetRules();
    }

    protected function getWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function validateCommonScenario(
        $title,
        $source,
        $cover,
        $attachments,
        $bannerImage,
        $content,
        $newsType,
        $dimension,
        $status,
        $stick,
        $bannerStatus,
        $homePageShowStatus
    ) : bool {
        $commonWidgetRules = $this->getWidgetRules();
        $newsWidgetRules = $this->getNewsWidgetRules();

        return $commonWidgetRules->title($title, 'title')
            && $commonWidgetRules->source($source, 'source')
            && $commonWidgetRules->status($status, 'status')
            && $commonWidgetRules->stick($stick, 'stick')
            && $commonWidgetRules->formatString($content, 'content')
            && (empty($cover) ? true : $commonWidgetRules->image($cover, 'cover'))
            && (empty($attachments) ? true : $commonWidgetRules->attachments($attachments, 'attachments'))
            && $newsWidgetRules->bannerImage($bannerStatus, $bannerImage)
            && $newsWidgetRules->newsType($newsType, 'newsType')
            && $newsWidgetRules->dimension($dimension, 'dimension')
            && $newsWidgetRules->homePageShowStatus($homePageShowStatus, 'homePageShowStatus');
    }

    protected function validateRejectScenario($rejectReason)
    {
        $commonWidgetRules = $this->getWidgetRules();

        return  $commonWidgetRules->reason($rejectReason, 'rejectReason');
    }

    protected function validateMoveScenario($newsType)
    {
        $newsWidgetRules = $this->getNewsWidgetRules();

        return  $newsWidgetRules->newsType($newsType, 'newsType');
    }

    protected function checkUserHasNewsPurview($resource) : bool
    {
        $category = $this->getPurviewCategoryFactory()->getCategory($resource);
        $crewCategory = Core::$container->get('crew')->getCategory();
        
        $newsPurview = [IPurviewAble::CATEGORY['NEWS'], IPurviewAble::CATEGORY['UNAUDITED_NEWS']];
        if ($crewCategory != Crew::CATEGORY['SUPERTUBE']) {
            if (!in_array($category, $newsPurview)) {
                Core::setLastError(PURVIEW_UNDEFINED);
                return false;
            }
        }

        return true;
    }
}
