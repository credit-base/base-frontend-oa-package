<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\News\Repository\NewsRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\News\View\Json\News\View;
use Base\Package\News\View\Json\News\ListView;
use Base\Package\News\View\Json\News\NewsTypeListView;

class NewsFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, NewsValidateTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new NewsRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : NewsRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('news')) {
                return false;
            }
        }
        
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $newsList = array();

        list($count, $newsList) = $this->getRepository()
            ->scenario(NewsRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($newsList, $count));
        return true;
    }

    /**
     * @SuppressWarnings(PHPMD)
     * @todo $userGroupId 同 $crewId
     * @ $userGroupId 主要为获取当前登陆账户的参数，无法与$crewId相同的修改
     */
    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $title = $request->get('title', '');
        $newsType = $request->get('newsType', '');
        $dimension = $request->get('dimension', '');
        $status = $request->get('status', '');
        $stick = $request->get('stick', '');
        $bannerStatus = $request->get('bannerStatus', '');
        $homePageShowStatus = $request->get('homePageShowStatus', '');
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $filter = array();

        if (empty($userGroupId)) {
            $userGroupId = marmot_decode($request->get('userGroup', ''));
        }
        if (!empty($userGroupId)) {
            $filter['publishUserGroup'] = $userGroupId;
        }
        if (!empty($title)) {
            $filter['title'] = $title;
        }
        if (!empty($newsType)) {
            $filter['newsType'] = marmot_decode($newsType);
        }
        if (!empty($dimension)) {
            $filter['dimension'] =  marmot_decode($dimension);
        }
        if (!empty($status)) {
            $filter['status'] = marmot_decode($status);
        }
        if (!empty($stick)) {
            $filter['stick'] = marmot_decode($stick);
        }
        if (!empty($bannerStatus)) {
            $filter['bannerStatus'] =  marmot_decode($bannerStatus);
        }
        if (!empty($homePageShowStatus)) {
            $filter['homePageShowStatus'] = marmot_decode($homePageShowStatus);
        }

        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $news = $this->getRepository()->scenario(NewsRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
        
        if ($news instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($news));
        return true;
    }

    /**
     * 获取新闻分类
     */
    public function newsType()
    {
        $data = [];

        $this->render(new NewsTypeListView($data));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        return $this->checkUserHasNewsPurview($resource);
    }
}
