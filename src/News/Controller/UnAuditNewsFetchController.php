<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\News\Repository\UnAuditNewsRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\News\View\Json\UnAuditNews\View;
use Base\Package\News\View\Json\UnAuditNews\ListView;

use Base\Sdk\Common\Model\IApplyAble;

class UnAuditNewsFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, NewsValidateTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditNewsRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditNewsRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('unAuditedNews')) {
                return false;
            }
        }
        
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();
      
        $unAuditNewsList = array();

        list($count, $unAuditNewsList) = $this->getRepository()
            ->scenario(UnAuditNewsRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($unAuditNewsList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $title = $request->get('title', '');
        $newsType = $request->get('newsType', '');
        $applyStatus = $request->get('applyStatus', '');
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();

        $filter = array();

        if (empty($userGroupId)) {
            $userGroupId = marmot_decode($request->get('userGroup', ''));
        }
        if (!empty($userGroupId)) {
            $filter['applyUserGroup'] = $userGroupId;
        }
        if (!empty($title)) {
            $filter['title'] = $title;
        }
        if (!empty($newsType)) {
            $filter['applyInfoType'] = marmot_decode($newsType);
        }
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['PENDING'].','.
            IApplyAble::APPLY_STATUS['REJECT'];
        if (!empty($applyStatus)) {
            $filter['applyStatus'] = marmot_decode($applyStatus);
        }
        
        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $unAuditNews = $this->getRepository()
            ->scenario(UnAuditNewsRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);
            
        if ($unAuditNews instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($unAuditNews));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        return $this->checkUserHasNewsPurview($resource);
    }
}
