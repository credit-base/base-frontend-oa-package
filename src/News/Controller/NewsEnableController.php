<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Traits\EnableControllerTrait;
use Base\Package\Common\Controller\Interfaces\IEnableAbleController;

use Sdk\News\Command\News\EnableNewsCommand;
use Sdk\News\Command\News\DisableNewsCommand;
use Sdk\News\CommandHandler\News\NewsCommandHandlerFactory;

class NewsEnableController extends Controller implements IEnableAbleController
{
    use WebTrait, EnableControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new NewsCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function enableAction(int $id) : bool
    {
        $command = new EnableNewsCommand(
            $id
        );

        return $this->getCommandBus()->send($command);
    }

    protected function disableAction(int $id) : bool
    {
        $command = new DisableNewsCommand(
            $id
        );

        return $this->getCommandBus()->send($command);
    }
}
