<?php
namespace Base\Package\News\View\Json\UnAuditNews;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\News\Translator\UnAuditNewsTranslator;

class ListView extends JsonView implements IView
{
    private $list;

    private $count;

    private $translator;

    public function __construct(
        array $list,
        int $count
    ) {
        $this->list = $list;
        $this->count = $count;
        $this->translator = new UnAuditNewsTranslator();
        parent::__construct();
    }

    protected function getList(): array
    {
        return $this->list;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): UnAuditNewsTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $value) {
            $data[] = $this->getTranslator()->objectToArray(
                $value,
                array(
                    'id',
                    'title',
                    'source',
                    'newsType',
                    'dimension',
                    'stick',
                    'status',
                    'bannerStatus',
                    'homePageShowStatus',
                    'applyStatus',
                    'applyInfoType',
                    'operationType',
                    'relation' =>['id','realName'],
                    'crew' => ['id','realName'],
                    'applyCrew' => ['id','realName'],
                    'applyUserGroup' => ['id','name'],
                    'publishUserGroup' => ['id','name'],
                    'createTime',
                    'updateTime',
                    'statusTime'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
