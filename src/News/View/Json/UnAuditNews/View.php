<?php
namespace Base\Package\News\View\Json\UnAuditNews;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\News\Model\News;
use Sdk\News\Translator\UnAuditNewsTranslator;

class View extends JsonView implements IView
{
    private $unAuditNews;

    private $translator;

    public function __construct($unAuditNews)
    {
        $this->unAuditNews = $unAuditNews;
        $this->translator = new UnAuditNewsTranslator();
        parent::__construct();
    }

    protected function getUnAuditNews()
    {
        return $this->unAuditNews;
    }

    protected function getTranslator(): UnAuditNewsTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getTranslator()->objectToArray(
            $this->getUnAuditNews(),
            array(
                'id',
                'title',
                'source',
                'cover',
                'rejectReason',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'stick',
                'status',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'applyStatus',
                'applyInfoType',
                'operationType',
                'relation' =>['id','realName'],
                'crew' => ['id','realName'],
                'applyCrew' => ['id','realName'],
                'applyUserGroup' => ['id','name'],
                'publishUserGroup' => ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        );
        
        $this->encode($data);
    }
}
