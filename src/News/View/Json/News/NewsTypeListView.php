<?php
namespace Base\Package\News\View\Json\News;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

class NewsTypeListView extends JsonView implements IView
{
    private $data;

    public function __construct(
        array $data
    ) {
        $this->data = $data;
        parent::__construct();
    }

    protected function getData(): array
    {
        return $this->data;
    }

    protected function formartArray($data) : array
    {
        $resultList = [];
        $parentList = [];
       
        foreach ($data['newsType'] as $key => $newsType) {
            foreach ($data['category'] as $categoryKey => $category) {
                if ($newsType[1] == $categoryKey) {
                    $parentList[] = [
                        'value' => marmot_encode($key),
                        'label' => NEWS_TYPE_CN[$key],
                        'pid' => $newsType[0],
                        'cid' => $newsType[1]
                    ];
                }
                unset($category);
            }
        }
        $resultList = $this->formartParentList($parentList);
   
        return $resultList;
    }

    protected function formartParentList($parentList) : array
    {
        $resultList = [];
        $newParentList = [];
        $categoryList = [];
        foreach ($parentList as $value) {
            $newParentList[$value['pid']][] = $value;
        }
        
        foreach ($newParentList as $key => $parentCategory) {
            $parentChildren = $this->formartCategoryList($parentCategory);
        
            if (!empty($key)) {
                $resultList[$key] = [
                    'value' => marmot_encode($key),
                    'label' => NEWS_PARENT_CATEGORY_CN[$key],
                    'children' => array_values($parentChildren)
                ];
            }

            if (empty($key)) {
                $categoryList = $parentChildren;
            }
        }
  
        $resultList = $this->pinArray($resultList, $categoryList);
      
        return array_values($resultList);
    }

    protected function pinArray($list, $array):array
    {
        foreach ($array as $value) {
            $list[] = $value;
        }
        
        return $list;
    }

    protected function formartCategoryList($categoryList) : array
    {
        $newCategoryList = [];
        $currentList = [];
        $childrenList = [];
        foreach ($categoryList as $val) {
            $currentList[$val['cid']][] = $val;
        }
        
        foreach ($currentList as $key => $value) {
            $value = $this->dealNotUseData($value);

            if (!empty($key)) {
                $newCategoryList[$key] = [
                    'value' => marmot_encode($key),
                    'label' => NEWS_CATEGORY_CN[$key],
                    'children' => $value
                ];
            }
            if (empty($key)) {
                $childrenList = $value;
            }
        }
        $newCategoryList = $this->pinArray($newCategoryList, $childrenList);

        return $newCategoryList;
    }

    protected function dealNotUseData($data) : array
    {
        $newData = [];

        foreach ($data as $key => $val) {
            unset($val['pid']);
            unset($val['cid']);
            $newData[$key] = $val;
        }
        
        return $newData;
    }

    public function display(): void
    {
        $data = array();

        $data['newsType'] = NEWS_TYPE_MAPPING;
        $data['parentCategory'] = NEWS_PARENT_CATEGORY_CN;
        $data['category'] = NEWS_CATEGORY_CN;
        
        $data = $this->formartArray($data);

        $dataList = array(
            'total' => count($data),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
