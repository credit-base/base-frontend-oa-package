<?php
namespace Base\Package\News\View\Json\News;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\News\Translator\NewsTranslator;

class ListView extends JsonView implements IView
{
    private $news;

    private $count;

    private $translator;

    public function __construct(
        array $news,
        int $count
    ) {
        $this->news = $news;
        $this->count = $count;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getNews(): array
    {
        return $this->news;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): NewsTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getNews() as $news) {
            $data[] = $this->getTranslator()->objectToArray(
                $news,
                array(
                    'id',
                    'title',
                    'source',
                    'newsType',
                    'dimension',
                    'status',
                    'stick',
                    'bannerStatus',
                    'homePageShowStatus',
                    'crew' => ['id','realName'],
                    'userGroup'=> ['id','name'],
                    'createTime',
                    'updateTime',
                    'statusTime'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
