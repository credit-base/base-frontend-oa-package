<?php
namespace Base\Package\News\View\Json\News;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\News\Translator\NewsTranslator;

class View extends JsonView implements IView
{
    private $news;

    private $translator;

    public function __construct($news)
    {
        $this->news = $news;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getNews()
    {
        return $this->news;
    }

    protected function getTranslator(): NewsTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getTranslator()->objectToArray(
            $this->getNews(),
            array(
                'id',
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'description',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew' => ['id','realName'],
                'userGroup'=> ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        );

        $this->encode($data);
    }
}
