<?php
namespace Base\Package\CreditPhotography\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ApproveControllerTrait;
use Base\Package\Common\Controller\Interfaces\IApproveAbleController;

use Sdk\Common\WidgetRules\WidgetRules;

use Sdk\CreditPhotography\Command\CreditPhotography\ApproveCreditPhotographyCommand;
use Sdk\CreditPhotography\Command\CreditPhotography\RejectCreditPhotographyCommand;
use Sdk\CreditPhotography\CommandHandler\CreditPhotography\CreditPhotographyCommandHandlerFactory;

class CreditPhotographyApproveController extends Controller implements IApproveAbleController
{
    use WebTrait, ApproveControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new CreditPhotographyCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function approveAction(int $id) : bool
    {
        $command = new ApproveCreditPhotographyCommand(
            $id
        );
        
        return $this->getCommandBus()->send($command);
    }

    protected function rejectAction(int $id) : bool
    {
        $request = $this->getRequest();
        
        $rejectReason = $request->post('rejectReason', '');

        if ($this->validateRejectScenario($rejectReason)) {
            $command = new RejectCreditPhotographyCommand(
                $rejectReason,
                $id
            );

            return $this->getCommandBus()->send($command);
        }

        return false;
    }

    protected function getWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }
   
    protected function validateRejectScenario($rejectReason)
    {
        $commonWidgetRules = $this->getWidgetRules();

        return  $commonWidgetRules->reason($rejectReason, 'rejectReason');
    }
}
