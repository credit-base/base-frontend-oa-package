<?php
namespace Base\Package\CreditPhotography\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\CreditPhotography\View\Json\View;
use Base\Package\CreditPhotography\View\Json\ListView;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\Common\Model\IApplyAble;

class CreditPhotographyFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new CreditPhotographyRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : CreditPhotographyRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();
        
        list($count, $list) = $this->getRepository()
            ->scenario(CreditPhotographyRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
        
        $this->render(new ListView($list, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $applyStatus = $request->get('applyStatus', '');
        $realName = $request->get('realName', '');
        $status = $request->get('status', '');
        
        $filter = array();

        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['PENDING'].','.
                                 IApplyAble::APPLY_STATUS['REJECT'];
        if (!empty($applyStatus)) {
            $filter['applyStatus'] = marmot_decode($applyStatus);
        }

        $filter['status'] = CreditPhotography::STATUS['NOMAL'];
        if (!empty($status)) {
            $filter['status'] = marmot_decode($status);
        }

        if (!empty($realName)) {
            $filter['realName'] = $realName;
        }

        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $creditPhotography = $this->getRepository()
            ->scenario(CreditPhotographyRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($creditPhotography instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($creditPhotography));
        return true;
    }
}
