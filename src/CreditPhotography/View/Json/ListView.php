<?php
namespace Base\Package\CreditPhotography\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;

class ListView extends JsonView implements IView
{
    private $creditPhotography;

    private $count;

    private $translator;

    public function __construct(
        array $creditPhotography,
        int $count
    ) {
        $this->creditPhotography = $creditPhotography;
        $this->count = $count;
        $this->translator = new CreditPhotographyTranslator();
        parent::__construct();
    }

    protected function getCreditPhotography(): array
    {
        return $this->creditPhotography;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): CreditPhotographyTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getCreditPhotography() as $creditPhotography) {
            $data[] = $this->getTranslator()->objectToArray(
                $creditPhotography,
                array(
                    'id',
                    'description',
                    'applyStatus',
                    'rejectReason',
                    'member'=>['id','realName'],
                    'applyCrew'=>['id','realName'],
                    'status',
                    'createTime',
                    'updateTime',
                    'statusTime'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
