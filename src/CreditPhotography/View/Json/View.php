<?php
namespace Base\Package\CreditPhotography\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;

class View extends JsonView implements IView
{
    private $creditPhotography;

    private $translator;

    public function __construct($creditPhotography)
    {
        $this->creditPhotography = $creditPhotography;
        $this->translator = new CreditPhotographyTranslator();
        parent::__construct();
    }

    protected function getCreditPhotography()
    {
        return $this->creditPhotography;
    }

    protected function getTranslator(): CreditPhotographyTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getTranslator()->objectToArray(
            $this->getCreditPhotography(),
            array(
                'id',
                'description',
                'attachments',
                'applyStatus',
                'rejectReason',
                'member'=>['id','realName'],
                'applyCrew'=>['id','realName'],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        );

        $this->encode($data);
    }
}
