<?php
/**
 * 默认使用常量0/默认状态
 */
define('STATUS_NORMAL', 0);
/**
 * 默认使用常量0
 */
define('CONSTANT', 0);
/**
 * 公用最大常量
 */
define('COMMON_SIZES', 1000);

define('NAV', array(
    'NULL' => 0,
    'NAV_INDEX' => 1, // 首页
    'NAV_CREDIT_DYNAMICS' => 2, //信用动态
    'NAV_POLICY_STATUTE' => 3, //政策法规
    'NAV_STANDARD_SPECIFICATION' => 4, //标准规范
    'NAV_CREDIT_PUBLICITY' => 5, //信息公示
    'NAV_DISCIPLINARY' => 6, //联合奖惩
    'NAV_PROMISE' => 7, //信用承诺
    'NAV_CLASSIC_CASE' => 8, //典型案例
    'NAV_CREDIT_RESEARCH' => 9, //信用研究
    'NAV_JOURNAL' => 10, //信用刊物
    'NAV_CREDIT_PHOTOGRAPHY' => 11, //信用随手拍
));

/**
 * 门户页面配置项分类
 */
define(
    'CATEGORY',
    // @codingStandardsIgnoreLine
    'homePage'
);
