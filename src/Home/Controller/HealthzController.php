<?php
namespace Home\Controller;

use Marmot\Framework\Classes\Controller;

class HealthzController extends Controller
{
    public function healthz()
    {
        return true;
    }
}
