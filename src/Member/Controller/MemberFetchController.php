<?php
namespace Base\Package\Member\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Member\Repository\MemberRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Member\View\Json\View;
use Base\Package\Member\View\Json\ListView;

class MemberFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new MemberRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : MemberRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();
        
        list($count, $list) = $this->getRepository()
            ->scenario(MemberRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($list, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $cellphone = $request->get('cellphone', '');
        $realName = $request->get('realName', '');
        $status = $request->get('status', '');
        
        $filter = array();

        if (!empty($cellphone)) {
            $filter['cellphone'] = $cellphone;
        }
        if (!empty($realName)) {
            $filter['realName'] = $realName;
        }
        if (!empty($status)) {
            $filter['status'] = marmot_decode($status);
        }

        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $member = $this->getRepository()
            ->scenario(MemberRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($member instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($member));
        return true;
    }
}
