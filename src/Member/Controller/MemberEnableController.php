<?php
namespace Base\Package\Member\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Traits\EnableControllerTrait;
use Base\Package\Common\Controller\Interfaces\IEnableAbleController;

use Sdk\Member\Command\Member\EnableMemberCommand;
use Sdk\Member\Command\Member\DisableMemberCommand;
use Sdk\Member\CommandHandler\Member\MemberCommandHandlerFactory;

class MemberEnableController extends Controller implements IEnableAbleController
{
    use WebTrait, EnableControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new MemberCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function enableAction(int $id) : bool
    {
        $command = new EnableMemberCommand($id);

        return $this->getCommandBus()->send($command);
    }

    protected function disableAction(int $id) : bool
    {
        $command = new DisableMemberCommand($id);

        return $this->getCommandBus()->send($command);
    }
}
