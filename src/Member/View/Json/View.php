<?php
namespace Base\Package\Member\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\Member\Translator\MemberTranslator;

class View extends JsonView implements IView
{
    private $member;

    private $translator;

    public function __construct($member)
    {
        $this->member = $member;
        $this->translator = new MemberTranslator();
        parent::__construct();
    }

    protected function getMember()
    {
        return $this->member;
    }

    protected function getTranslator(): MemberTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getTranslator()->objectToArray(
            $this->getMember(),
            array(
                'id',
                'userName',
                'realName',
                'cardId',
                'cellphone',
                'email',
                'contactAddress',
                'gender',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        );

        $this->encode($data);
    }
}
