<?php
namespace Base\Package\Member\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Member\Translator\MemberTranslator;

class ListView extends JsonView implements IView
{
    private $member;

    private $count;

    private $translator;

    public function __construct(
        array $member,
        int $count
    ) {
        $this->member = $member;
        $this->count = $count;
        $this->translator = new MemberTranslator();
        parent::__construct();
    }

    protected function getMember(): array
    {
        return $this->member;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): MemberTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getMember() as $member) {
            $data[] = $this->getTranslator()->objectToArray(
                $member,
                array(
                    'id',
                    'userName',
                    'realName',
                    'cellphone',
                    'gender',
                    'status',
                    'createTime',
                    'updateTime',
                    'statusTime'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
