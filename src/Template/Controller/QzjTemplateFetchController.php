<?php
namespace Base\Package\Template\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\Controller;

use Sdk\Template\Repository\QzjTemplateRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Template\View\Json\QzjTemplateDetailView;
use Base\Package\Template\View\Json\QzjTemplateListView;

class QzjTemplateFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait, WebTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new QzjTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository(): QzjTemplateRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size, $page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();
        list($count, $list) = $this->getRepository()
                ->scenario(QzjTemplateRepository::LIST_MODEL_UN)
                ->search($filter, $sort, $page, $size);

        $this->render(new QzjTemplateListView($list, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $sort = $this->getRequest()->get('sort', '');
        $name = $this->getRequest()->get('name', '');
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $filter = array();
        if (!empty($name)) {
            $filter['name'] = $name;
        }
        if (!empty($userGroupId)) {
            $filter['sourceUnit'] = $userGroupId;
        }
        if (empty($sort)) {
            $sort = '-updateTime';
        }
        
        return [$filter, array($sort)];
    }

    protected function fetchOneAction(int $id): bool
    {
        $data = $this->getRepository()
            ->scenario(QzjTemplateRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($data instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new QzjTemplateDetailView($data));
        return true;
    }
}
