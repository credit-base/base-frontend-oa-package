<?php

namespace  Base\Package\Template\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\Template\Model\GbTemplate;

use Sdk\Template\Command\GbTemplate\AddGbTemplateCommand;
use Sdk\Template\Command\GbTemplate\EditGbTemplateCommand;
use Sdk\Template\CommandHandler\GbTemplate\GbTemplateCommandHandlerFactory;

use Base\Package\Template\View\Json\ConfigureView;

class GbTemplateOperationController extends Controller implements IOperateAbleController
{
    use WebTrait, OperateControllerTrait, ControllerRequestValidateTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new GbTemplateCommandHandlerFactory());
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        $request = $this->getCommonRequest();

        if ($this->validateCommonScenario(
            $request['name'],
            $request['identify'],
            $request['subjectCategory'],
            $request['dimension'],
            $request['exchangeFrequency'],
            $request['infoClassify'],
            $request['infoCategory'],
            $request['description'],
            $request['items']
        )) {
            $command = new AddGbTemplateCommand(
                $request['name'],
                $request['identify'],
                $request['description'],
                $request['subjectCategory'],
                $request['items'],
                $request['dimension'],
                $request['exchangeFrequency'],
                $request['infoClassify'],
                $request['infoCategory']
            );
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        $request = $this->getCommonRequest();

        if ($this->validateCommonScenario(
            $request['name'],
            $request['identify'],
            $request['subjectCategory'],
            $request['dimension'],
            $request['exchangeFrequency'],
            $request['infoClassify'],
            $request['infoCategory'],
            $request['description'],
            $request['items']
        )) {
            $command = new EditGbTemplateCommand(
                $request['name'],
                $request['identify'],
                $request['description'],
                $request['subjectCategory'],
                $request['items'],
                $request['dimension'],
                $request['exchangeFrequency'],
                $request['infoClassify'],
                $request['infoCategory'],
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 获取配置项
     */
    public function configure()
    {
        $data = array();
        $data['subjectCategory'] = GbTemplate::SUBJECT_CATEGORY;
        $data['dimension'] = GbTemplate::DIMENSION;
        $data['exchangeFrequency'] = GbTemplate::EXCHANGE_FREQUENCY;
        $data['infoClassify'] = GbTemplate::INFO_CLASSIFY;
        $data['infoCategory'] = GbTemplate::INFO_CATEGORY;
        $data['type'] = GbTemplate::TYPE;

        $list = array(
            'subjectCategory' => $this->getSubjectCategoryCn($data['subjectCategory']),
            'dimension' => $this->getDimensionCn($data['dimension']),
            'exchangeFrequency' => $this->getExchangeFrequencyCn($data['exchangeFrequency']),
            'infoClassify' => $this->getInfoClassifyCn($data['infoClassify']),
            'infoCategory' => $this->getInfoCategoryCn($data['infoCategory']),
            'type' => $this->getTypeCn($data['type'])
         );

         $this->render(new ConfigureView($list));
         return true;
    }
}
