<?php
namespace Base\Package\Template\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\Template\Command\BaseTemplate\AddBaseTemplateCommand;
use Sdk\Template\Command\BaseTemplate\EditBaseTemplateCommand;
use Sdk\Template\CommandHandler\BaseTemplate\BaseTemplateCommandHandlerFactory;

class BaseTemplateOperationController extends Controller implements IOperateAbleController
{
    use WebTrait, OperateControllerTrait, ControllerRequestValidateTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new BaseTemplateCommandHandlerFactory());
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        $request = $this->getCommonRequest();
        
        if ($this->validateCommonScenario(
            $request['name'],
            $request['identify'],
            $request['subjectCategory'],
            $request['dimension'],
            $request['exchangeFrequency'],
            $request['infoClassify'],
            $request['infoCategory'],
            $request['description'],
            $request['items']
        )) {
            $command = new EditBaseTemplateCommand(
                $request['name'],
                $request['identify'],
                $request['description'],
                $request['subjectCategory'],
                $request['items'],
                $request['dimension'],
                $request['exchangeFrequency'],
                $request['infoClassify'],
                $request['infoCategory'],
                $id
            );
           
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }
}
