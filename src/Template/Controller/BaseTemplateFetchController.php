<?php
namespace Base\Package\Template\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\Controller;

use Sdk\Template\Repository\BaseTemplateRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Template\View\Json\BaseTemplateDetailView;
use Base\Package\Template\View\Json\BaseTemplateListView;

class BaseTemplateFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait, WebTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new BaseTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository(): BaseTemplateRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size, $page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();
        list($count, $list) =
            $this->getRepository()->scenario(BaseTemplateRepository::LIST_MODEL_UN)
                ->search($filter, $sort, $page, $size);

        $this->render(new BaseTemplateListView($list, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $sort = $this->getRequest()->get('sort', '-updateTime');
        $identify = $this->getRequest()->get('identify', '');
        $name = $this->getRequest()->get('name', '');
        
        $filter = array();
        if (!empty($name)) {
            $filter['name'] = $name;
        }
        if (!empty($identify)) {
            $filter['identify'] = $identify;
        }
        
        return [$filter, array($sort)];
    }

    protected function fetchOneAction(int $id): bool
    {
        $data = $this->getRepository()
            ->scenario(BaseTemplateRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($data instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new BaseTemplateDetailView($data));
        return true;
    }
}
