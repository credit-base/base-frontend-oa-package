<?php
namespace Base\Package\Template\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\Controller;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Template\View\Json\GbTemplateDetailView;
use Base\Package\Template\View\Json\GbTemplateListView;

use Sdk\Template\Repository\GbTemplateRepository;

class GbTemplateFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait, WebTrait;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new GbTemplateRepository();
    }

    protected function getRepository(): GbTemplateRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size, $page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();
        list($count, $list) = $this->getRepository()
            ->scenario(GbTemplateRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new GbTemplateListView($list, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $identify = $this->getRequest()->get('identify', '');
        $sort = $this->getRequest()->get('sort', '-updateTime');
        $name = $this->getRequest()->get('name', '');
        
        $filter = array();
        if (!empty($name)) {
            $filter['name'] = $name;
        }
        if (!empty($identify)) {
            $filter['identify'] = $identify;
        }

        return [$filter, array($sort)];
    }

    protected function fetchOneAction(int $id): bool
    {
        $data = $this->getRepository()
            ->scenario(GbTemplateRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($data instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new GbTemplateDetailView($data));
        return true;
    }
}
