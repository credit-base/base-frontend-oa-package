<?php
namespace Base\Package\Template\Controller;

use Sdk\Template\WidgetRule\TemplateWidgetRule;
use Sdk\Template\Model\GbTemplate;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
*/
trait ControllerRequestValidateTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonRequest()
    {
        $request = $this->getRequest();
        $requestData = array();
        $requestData['name'] =  $request->post('name', '');
        $requestData['identify'] =  $request->post('identify', '');
        $requestData['description'] = $request->post('description', '');

        $subjectCategory = $request->post('subjectCategory', array());
        $requestData['subjectCategory'] = $this->getSubjectCategory($subjectCategory);

        $dimension = $request->post('dimension', '');
        $requestData['dimension'] = is_null($dimension) ? '' : intval(marmot_decode($dimension));

        $exchangeFrequency = $request->post('exchangeFrequency', '');
        $requestData['exchangeFrequency'] = is_null($exchangeFrequency) ? ''
        : intval(marmot_decode($exchangeFrequency));

        $infoClassify = $request->post('infoClassify', '');
        $requestData['infoClassify'] = is_null($infoClassify) ? '' : intval(marmot_decode($infoClassify));

        $infoCategory = $request->post('infoCategory', '');
        $requestData['infoCategory'] = is_null($infoCategory) ? '' : intval(marmot_decode($infoCategory));

        $sourceUnit = $request->post('sourceUnit', '');
        $requestData['sourceUnit'] = is_null($sourceUnit) ? '' : intval(marmot_decode($sourceUnit));

        $gbTemplate = $request->post('gbTemplate', '');
        $requestData['gbTemplate'] = is_null($gbTemplate) ? '' : intval(marmot_decode($gbTemplate));

        $items = $request->post('items', array());
        $requestData['items'] = $this->getItems($items);

        $category = $request->post('category', '');
        $requestData['category'] = marmot_decode($category);
        
        return $requestData;
    }

    protected function getSubjectCategory($subjectCategory)
    {
        foreach ($subjectCategory as $key => $item) {
            $subjectCategory[$key] = marmot_decode($item);
        }

        return $subjectCategory;
    }
        
    protected function getItems($items)
    {
        foreach ($items as $key => $item) {
            $items[$key]['dimension'] = is_null($item['dimension'])
            ? '' :
            intval(marmot_decode($item['dimension']));

            $items[$key]['type'] = is_null($item['type'])
            ? '' :
            intval(marmot_decode($item['type']));

            $items[$key]['isNecessary'] = is_null($item['isNecessary'])
            ? '' :
            intval(marmot_decode($item['isNecessary']));
            
            $items[$key]['isMasked'] = is_null($item['isMasked'])
            ? '' :
            intval(marmot_decode($item['isMasked']));
        }

        return $items;
    }

    protected function validateCommonScenario(
        $name,
        $identify,
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $description,
        $items
    ) : bool {
        return $this->getTemplateWidgetRule()->name($name)
            && $this->getTemplateWidgetRule()->subjectCategory($subjectCategory)
            && $this->getTemplateWidgetRule()->identify($identify)
            && $this->getTemplateWidgetRule()->dimension($dimension)
            && $this->getTemplateWidgetRule()->formatNumeric($exchangeFrequency)
            && $this->getTemplateWidgetRule()->formatNumeric($infoClassify)
            && (empty($infoCategory) ? true : $this->getTemplateWidgetRule()->formatNumeric($infoCategory))
            && $this->getTemplateWidgetRule()->items($subjectCategory, $items)
            && $this->getTemplateWidgetRule()->description($description);
    }

    protected function validateQzjCommonScenario($infoCategory, $category):bool
    {
        return $this->getTemplateWidgetRule()->category($category)
            && $this->getTemplateWidgetRule()->infoCategory($infoCategory);
    }

    protected function getSubjectCategoryCn($subjectCategory)
    {
        $data = array();
        foreach ($subjectCategory as $key => $item) {
            $data[$key]['id'] = marmot_encode($item);
            $data[$key]['name'] = GbTemplate::SUBJECT_CATEGORY_CN[$item];
        }

        return array_values($data);
    }
    
    protected function getDimensionCn($dimension)
    {
        $data = array();
        foreach ($dimension as $key => $item) {
            $data[$key]['id'] = marmot_encode($item);
            $data[$key]['name'] = GbTemplate::DIMENSION_CN[$item];
        }

        return array_values($data);
    }

    protected function getInfoClassifyCn($infoClassify)
    {
        $data = array();
        foreach ($infoClassify as $key => $item) {
            $data[$key]['id'] = marmot_encode($item);
            $data[$key]['name'] = GbTemplate::INFO_CLASSIFY_CN[$item];
        }

        return array_values($data);
    }

    protected function getInfoCategoryCn($infoCategory)
    {
        $data = array();
        foreach ($infoCategory as $key => $item) {
            $data[$key]['id'] = marmot_encode($item);
            $data[$key]['name'] = GbTemplate::INFO_CATEGORY_CN[$item];
        }

        return array_values($data);
    }

    protected function getExchangeFrequencyCn($exchangeFrequency)
    {
        $data = array();
        foreach ($exchangeFrequency as $key => $item) {
            $data[$key]['id'] = marmot_encode($item);
            $data[$key]['name'] = GbTemplate::EXCHANGE_FREQUENCY_CN[$item];
        }

        return array_values($data);
    }

    protected function getTypeCn($type)
    {
        $data = array();
        foreach ($type as $key => $item) {
            $data[$key]['id'] = marmot_encode($item);
            $data[$key]['name'] = GbTemplate::TYPE_CN[$item];
        }

        return array_values($data);
    }
}
