<?php
namespace Base\Package\Template\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\Template\Command\WbjTemplate\AddWbjTemplateCommand;
use Sdk\Template\Command\WbjTemplate\EditWbjTemplateCommand;
use Sdk\Template\CommandHandler\WbjTemplate\WbjTemplateCommandHandlerFactory;

class WbjTemplateOperationController extends Controller implements IOperateAbleController
{
    use WebTrait, OperateControllerTrait, ControllerRequestValidateTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new WbjTemplateCommandHandlerFactory());
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        $request = $this->getCommonRequest();

        if ($this->validateCommonScenario(
            $request['name'],
            $request['identify'],
            $request['subjectCategory'],
            $request['dimension'],
            $request['exchangeFrequency'],
            $request['infoClassify'],
            $request['infoCategory'],
            $request['description'],
            $request['items']
        )) {
            $command = new AddWbjTemplateCommand(
                $request['name'],
                $request['identify'],
                $request['description'],
                $request['subjectCategory'],
                $request['items'],
                $request['sourceUnit'],
                $request['dimension'],
                $request['exchangeFrequency'],
                $request['infoClassify'],
                $request['infoCategory']
            );
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        $request = $this->getCommonRequest();

        if ($this->validateCommonScenario(
            $request['name'],
            $request['identify'],
            $request['subjectCategory'],
            $request['dimension'],
            $request['exchangeFrequency'],
            $request['infoClassify'],
            $request['infoCategory'],
            $request['description'],
            $request['items']
        )) {
            $command = new EditWbjTemplateCommand(
                $request['name'],
                $request['identify'],
                $request['description'],
                $request['subjectCategory'],
                $request['items'],
                $request['sourceUnit'],
                $request['dimension'],
                $request['exchangeFrequency'],
                $request['infoClassify'],
                $request['infoCategory'],
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }
}
