<?php
namespace Base\Package\Template\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\Controller;

use Sdk\Template\Repository\WbjTemplateRepository;
use Sdk\Template\Translator\WbjTemplateTranslator;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Template\View\Json\WbjTemplateDetailView;
use Base\Package\Template\View\Json\WbjTemplateListView;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;

class WbjTemplateFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait, WebTrait;

    const SOURCE_UNIT = 'MA';

    const HEAD_LETTER = array(
        'A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1',
        'J1', 'K1', 'L1', 'M1', 'N1',
        'O1', 'P1', 'Q1', 'R1', 'S1','T1', 'U1', 'V1', 'W1',
        'X1', 'Y1', 'Z1', 'AA1', 'AB1',
        'AC1', 'AD1', 'AE1', 'AF1', 'AG1', 'AH1', 'AI1',
        'AJ1','AK1', 'AL1', 'AM1', 'AN1',
        'AO1', 'AP1', 'AQ1', 'AR1', 'AS1', 'AT1', 'AU1',
        'AV1', 'AW1', 'AX1', 'AY1', 'AZ1',
        'BA1', 'BB1', 'BC1', 'BD1', 'BE1', 'BF1', 'BG1',
        'BH1', 'BI1', 'BJ1', 'BK1', 'BL1',
        'BM1', 'BN1', 'BO1', 'BP1','BQ1', 'BR1',
        'BS1', 'BT1', 'BU1', 'BV1', 'BW1', 'BX1', 'BY1', 'BZ1'
      );

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WbjTemplateRepository();
    }

    protected function getRepository(): WbjTemplateRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size, $page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();
        list($count, $list) =
            $this->getRepository()->scenario(WbjTemplateRepository::LIST_MODEL_UN)
                ->search($filter, $sort, $page, $size);

        $this->render(new WbjTemplateListView($list, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $sort = $this->getRequest()->get('sort', '-updateTime');
        $name = $this->getRequest()->get('name', '');
        $identify = $this->getRequest()->get('identify', '');
        $sourceUnit = $this->getRequest()->get('sourceUnit', self::SOURCE_UNIT);
        
        $filter = array();

        if ($sourceUnit != self::SOURCE_UNIT) {
            $filter['sourceUnit'] = marmot_decode($sourceUnit);
        }
        if (!empty($name)) {
            $filter['name'] = $name;
        }
        if (!empty($identify)) {
            $filter['identify'] = $identify;
        }
        
        return [$filter, array($sort)];
    }

    protected function fetchOneAction(int $id): bool
    {
        $data = $this->getWbjTemplate($id);

        if ($data instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new WbjTemplateDetailView($data));
        return true;
    }

    protected function getWbjTemplate(int $id)
    {
        $data = $this->getRepository()
        ->scenario(WbjTemplateRepository::FETCH_ONE_MODEL_UN)
        ->fetchOne($id);

        return $data;
    }

    protected function getWbjTemplateTranslator():WbjTemplateTranslator
    {
        return new WbjTemplateTranslator();
    }
  
    public function download(string $id) : bool
    {
        $data = $this->getWbjTemplateArrayData($id);
       
        if ($data && !empty($data['items'])) {
            if (isset($data['identify']) && isset($data['category'])) {
                $name = $data['identify'].'_'.$id.'_'.$data['category'];
               
                $spreadsheet = $this->getTemplateSpreadsheet();
                $spreadsheet = $this->getItemCellValue($data['items'], $spreadsheet);
        
                $this->getResultWriter($name, $spreadsheet);
            }
        }
        $this->displayError();
        return false;
    }

    protected function getWbjTemplateArrayData(string $id) : array
    {
        $id = marmot_decode($id);
        $data = $this->getWbjTemplate($id);

        $wbjTemplateData = [];
        if (!($data instanceof INull)) {
            $wbjTemplateData = $this->getWbjTemplateTranslator()->objectToArray($data);
        }

        return $wbjTemplateData;
    }

    protected function getSpreadsheet() : Spreadsheet
    {
        return new Spreadsheet();
    }

    protected function getTemplateSpreadsheet()
    {
        $spreadsheet = $this->getSpreadsheet();
       
        $spreadsheet->getProperties()
            ->setCreator("qixinyun")
            ->setLastModifiedBy("qixinyun")
            ->setTitle("wbj")
            ->setSubject("wbj")
            ->setDescription("wbj")
            ->setKeywords("wbj")
            ->setCategory("resource catalog");
       
        return $spreadsheet;
    }

    protected function getItemCellValue(array $item, Spreadsheet $spreadsheet)
    {
        $sheet = $spreadsheet->getActiveSheet();

        foreach ($item as $key => $val) {
            $sheet->setCellValue(self::HEAD_LETTER[$key], $val['name']);
            
            if (!isset($val['remarks'])) {
                Core::setLastError(REMARKS_FORMAT_ERROR);
                return false;
            }
                
            $sheet->getComment(self::HEAD_LETTER[$key])->getText()->createTextRun(
                $val['remarks']
            );
        }

        return $spreadsheet;
    }

    protected function getWriter(Spreadsheet $spreadsheet) : Writer
    {
        return new Writer($spreadsheet);
    }

    /**
     * @codeCoverageIgnore
     * header方法未封装，暂时不写单元测试
     */
    protected function getResultWriter($filName, $spreadsheet)
    {
        $writer = $this->getWriter($spreadsheet);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$filName.'.xlsx"');
        header('fileName:'.$filName.'.xlsx');

        $writer->save("php://output");
    }
}
