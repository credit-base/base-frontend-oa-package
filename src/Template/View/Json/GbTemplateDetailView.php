<?php
namespace Base\Package\Template\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\Template\Translator\GbTemplateTranslator;

class GbTemplateDetailView extends JsonView implements IView
{
    private $data;

    private $translator;

    public function __construct($data)
    {
        $this->data = $data;
        $this->translator = new GbTemplateTranslator();
        parent::__construct();
    }

    protected function getGbTemplate()
    {
        return $this->data;
    }

    protected function getTranslator() : GbTemplateTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();
        $gbTemplate = $this->getGbTemplate();
        
        $translator = $this->getTranslator();
        $data = $translator->objectToArray($gbTemplate);

        $this->encode($data);
    }
}
