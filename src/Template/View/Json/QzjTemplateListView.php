<?php
namespace Base\Package\Template\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\Template\Translator\QzjTemplateTranslator;

class QzjTemplateListView extends JsonView implements IView
{
    private $qzjTemplates;

    private $count;

    private $translator;

    public function __construct(
        array $qzjTemplates,
        int $count
    ) {
        parent::__construct();
        $this->qzjTemplates = $qzjTemplates;
        $this->count = $count;
        $this->translator = new QzjTemplateTranslator();
    }

    protected function getQzjTemplateList() : array
    {
        return $this->qzjTemplates;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : QzjTemplateTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $translator = $this->getTranslator();

        $data = array();
        foreach ($this->getQzjTemplateList() as $value) {
            $data[] = $translator->objectToArray(
                $value,
                array(
                    'id',
                    'name',
                    'identify',
                    'subjectCategory',
                    'dimension',
                    'infoClassify',
                    'category',
                    'sourceUnit',
                    'updateTime',
                )
            );
        }
      
        $result = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($result);
    }
}
