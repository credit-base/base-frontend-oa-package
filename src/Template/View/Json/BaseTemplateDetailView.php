<?php
namespace Base\Package\Template\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\Template\Translator\BaseTemplateTranslator;

class BaseTemplateDetailView extends JsonView implements IView
{
    private $data;

    private $translator;

    public function __construct($data)
    {
        parent::__construct();
        $this->data = $data;
        $this->translator = new BaseTemplateTranslator();
    }

    protected function getBaseTemplate()
    {
        return $this->data;
    }

    protected function getTranslator() : BaseTemplateTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();
        $bjTemplate = $this->getBaseTemplate();
        
        $translator = $this->getTranslator();
        $data = $translator->objectToArray($bjTemplate);

        $this->encode($data);
    }
}
