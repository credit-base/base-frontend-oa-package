<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\WbjTemplateTranslator;

trait WbjTemplateListTrait
{
    private $list;

    private $count;

    private $translator;

    public function __construct(
        array $list,
        int $count
    ) {
        parent::__construct();
        $this->list = $list;
        $this->count = $count;
        $this->translator = new WbjTemplateTranslator();
    }

    protected function getList() : array
    {
        return $this->list;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : WbjTemplateTranslator
    {
        return $this->translator;
    }
}
