<?php
namespace Base\Package\Template\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

class WbjTemplateDetailView extends JsonView implements IView
{
    use WbjTemplateDetailTrait;

    public function display() : void
    {
        $data = $this->getWbjTemplateDetail();

        $this->encode($data);
    }
}
