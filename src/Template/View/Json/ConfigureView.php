<?php
namespace Base\Package\Template\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

class ConfigureView extends JsonView implements IView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
        parent::__construct();
    }

    protected function getData()
    {
        return $this->data;
    }

    public function display() : void
    {
        $list = $this->getData();

        $this->encode($list);
    }
}
