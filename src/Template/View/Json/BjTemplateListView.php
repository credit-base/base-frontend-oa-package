<?php
namespace Base\Package\Template\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

class BjTemplateListView extends JsonView implements IView
{
    use BjTemplateListTrait;

    public function display() : void
    {
        $translator = $this->getTranslator();

        $data = array();
        foreach ($this->getList() as $item) {
            $data[] = $translator->objectToArray(
                $item,
                array(
                    'id',
                    'name',
                    'identify',
                    'subjectCategory',
                    'exchangeFrequency',
                    'dimension',
                    'infoClassify',
                    'sourceUnit',
                    'gbTemplate',
                    'description',
                    'updateTime',
                    'infoCategory',
                    'items',
                    'ruleCount',
                    'dataTotal'
                )
            );
        }

        $result = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($result);
    }
}
