<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\BaseTemplateTranslator;

trait BaseTemplateListTrait
{
    private $list;

    private $count;

    private $translator;

    public function __construct(
        array $list,
        int $count
    ) {
        parent::__construct();
        $this->list = $list;
        $this->count = $count;
        $this->translator = new BaseTemplateTranslator();
    }

    protected function getList() : array
    {
        return $this->list;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : BaseTemplateTranslator
    {
        return $this->translator;
    }
}
