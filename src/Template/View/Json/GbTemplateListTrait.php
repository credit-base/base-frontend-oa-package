<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\GbTemplateTranslator;

trait GbTemplateListTrait
{
    private $list;

    private $count;

    private $translator;

    public function __construct(
        array $list,
        int $count
    ) {
        parent::__construct();
        $this->list = $list;
        $this->count = $count;
        $this->translator = new GbTemplateTranslator();
    }

    protected function getList() : array
    {
        return $this->list;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : GbTemplateTranslator
    {
        return $this->translator;
    }
}
