<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\BjTemplateTranslator;

trait BjTemplateListTrait
{
    private $list;

    private $count;

    private $translator;

    public function __construct(
        array $list,
        int $count
    ) {
        parent::__construct();
        $this->list = $list;
        $this->count = $count;
        $this->translator = new BjTemplateTranslator();
    }

    protected function getList() : array
    {
        return $this->list;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : BjTemplateTranslator
    {
        return $this->translator;
    }
}
