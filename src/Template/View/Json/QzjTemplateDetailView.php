<?php
namespace Base\Package\Template\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\Template\Translator\QzjTemplateTranslator;

class QzjTemplateDetailView extends JsonView implements IView
{
    private $qzjTemplate;

    private $translator;

    public function __construct($qzjTemplate)
    {
        parent::__construct();
        $this->qzjTemplate = $qzjTemplate;
        $this->translator = new QzjTemplateTranslator();
    }

    protected function getQzjTemplate()
    {
        return $this->qzjTemplate;
    }

    protected function getTranslator() : QzjTemplateTranslator
    {
        return $this->translator;
    }

    public function getQzjTemplateDetail() : array
    {
        $data = array();
        $qzjTemplate = $this->getQzjTemplate();
        
        $translator = $this->getTranslator();
        $data = $translator->objectToArray($qzjTemplate);

        return $data;
    }

    public function display() : void
    {
        $data = $this->getQzjTemplateDetail();

        $this->encode($data);
    }
}
