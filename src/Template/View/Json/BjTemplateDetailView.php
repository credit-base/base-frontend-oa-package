<?php
namespace Base\Package\Template\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\Template\Translator\BjTemplateTranslator;

class BjTemplateDetailView extends JsonView implements IView
{
    private $data;

    private $translator;

    public function __construct($data)
    {
        parent::__construct();
        $this->data = $data;
        $this->translator = new BjTemplateTranslator();
    }

    protected function getBjTemplate()
    {
        return $this->data;
    }

    protected function getTranslator() : BjTemplateTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();
        $bjTemplate = $this->getBjTemplate();
        
        $translator = $this->getTranslator();
        $data = $translator->objectToArray($bjTemplate);

        $this->encode($data);
    }
}
