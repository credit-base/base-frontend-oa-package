<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\WbjTemplateTranslator;

trait WbjTemplateDetailTrait
{
    private $data;

    private $translator;

    public function __construct($data)
    {
        parent::__construct();
        $this->data = $data;
        $this->translator = new WbjTemplateTranslator();
    }

    protected function getWbjTemplate()
    {
        return $this->data;
    }

    protected function getTranslator() : WbjTemplateTranslator
    {
        return $this->translator;
    }

    public function getWbjTemplateDetail() : array
    {
        $data = array();
        $bjTemplate = $this->getWbjTemplate();
        
        $translator = $this->getTranslator();
        $data = $translator->objectToArray($bjTemplate);

        return $data;
    }
}
