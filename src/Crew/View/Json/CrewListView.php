<?php
namespace Base\Package\Crew\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Crew\Translator\CrewTranslator;

class CrewListView extends JsonView implements IView
{
    private $crews;

    private $count;

    private $translator;

    public function __construct(
        array $crews,
        int $count
    ) {
        $this->crews = $crews;
        $this->count = $count;
        $this->translator = new CrewTranslator();
        parent::__construct();
    }

    protected function getCrews() : array
    {
        return $this->crews;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : CrewTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getCrews() as $crew) {
            $data[] = $this->getTranslator()->objectToArray(
                $crew,
                array(
                    'id',
                    'realName',
                    'cellphone',
                    'category',
                    'userGroup',
                    'department',
                    'status',
                    'updateTime',
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
