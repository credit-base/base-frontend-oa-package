<?php
namespace Base\Package\Crew\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

class SignInView extends JsonView implements IView
{
    private $res;

    public function __construct(array $res)
    {
        $this->res = $res;
        parent::__construct();
    }

    public function display() : void
    {
        $this->encode($this->res);
    }
}
