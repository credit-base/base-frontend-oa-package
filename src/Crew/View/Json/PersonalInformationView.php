<?php
namespace Base\Package\Crew\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Translator\CrewTranslator;

class PersonalInformationView extends JsonView implements IView
{
    private $crew;

    private $translator;

    public function __construct($crew)
    {
        parent::__construct();
        $this->crew = $crew;
        $this->translator = new CrewTranslator();
    }

    protected function getCrew()
    {
        return $this->crew;
    }

    protected function getTranslator(): CrewTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getTranslator()->objectToArray(
            $this->getCrew()
        );

        $this->encode($data);
    }
}
