<?php
namespace Base\Package\Crew\Controller;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\User\WidgetRules\UserWidgetRules;
use Sdk\Crew\WidgetRules\CrewWidgetRules;

trait CrewValidateTrait
{
    protected function getUserWidgetRules() : UserWidgetRules
    {
        return new UserWidgetRules();
    }

    protected function getCrewWidgetRules() : CrewWidgetRules
    {
        return new CrewWidgetRules();
    }

    protected function getWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }

    protected function validateCommonScenario(
        $realName,
        $cardId,
        $category,
        $purview,
        $userGroupId,
        $departmentId
    ) : bool {
        $commonWidgetRules = $this->getWidgetRules();
        $crewWidgetRules = $this->getCrewWidgetRules();
        $userWidgetRules = $this->getUserWidgetRules();

        return $userWidgetRules->realName($realName)
            && (empty($cardId) ? true : $userWidgetRules->cardId($cardId))
            && $crewWidgetRules->category($category)
            && $crewWidgetRules->purview($purview)
            && (empty($userGroupId) ? true : $commonWidgetRules->formatNumeric($userGroupId, 'userGroupId'))
            && (empty($departmentId) ? true : $commonWidgetRules->formatNumeric($departmentId, 'departmentId'));
    }

    protected function validateAddScenario(
        $cellphone,
        $password,
        $confirmPassword
    ) : bool {

        $userWidgetRules = $this->getUserWidgetRules();

        return $userWidgetRules->cellphone($cellphone)
            && $userWidgetRules->password($password)
            && $userWidgetRules->confirmPassword($password, $confirmPassword);
    }

    protected function validateSignInScenario(
        $userName,
        $password,
        $captcha
    ) : bool {
        $userWidgetRules = $this->getUserWidgetRules();

        return $userWidgetRules->hash()
            && $userWidgetRules->cellphone($userName)
            && $userWidgetRules->password($password)
            && $this->validateCaptcha($captcha);
    }
}
