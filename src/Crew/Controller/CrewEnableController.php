<?php
namespace Base\Package\Crew\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Traits\EnableControllerTrait;
use Base\Package\Common\Controller\Interfaces\IEnableAbleController;

use Sdk\Crew\Command\Crew\EnableCrewCommand;
use Sdk\Crew\Command\Crew\DisableCrewCommand;
use Sdk\Crew\CommandHandler\Crew\CrewCommandHandlerFactory;

class CrewEnableController extends Controller implements IEnableAbleController
{
    use WebTrait, EnableControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new CrewCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function enableAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new EnableCrewCommand($id));
    }

    protected function disableAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new DisableCrewCommand($id));
    }
}
