<?php
namespace Base\Package\Crew\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Repository\CrewRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Crew\View\Json\CrewView;
use Base\Package\Crew\View\Json\CrewListView;
use Base\Package\Crew\View\Json\PersonalInformationView;

class CrewFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait;

    const CATEGORY_SCENE= array(
        Crew::CATEGORY['SUPERTUBE'] =>
        Crew::CATEGORY['PLATFORM_ADMINISTRATORS'].','.Crew::CATEGORY['PLATFORM_ADMINISTRATORS'].
        ','.Crew::CATEGORY['USERGROUP_ADMINISTRATORS'].
        ','.Crew::CATEGORY['USERGROUP_OPERATION'],
        Crew::CATEGORY['PLATFORM_ADMINISTRATORS'] =>
        Crew::CATEGORY['USERGROUP_ADMINISTRATORS'].','.Crew::CATEGORY['USERGROUP_OPERATION'],
        Crew::CATEGORY['USERGROUP_ADMINISTRATORS'] => Crew::CATEGORY['USERGROUP_OPERATION'],
        Crew::CATEGORY['USERGROUP_OPERATION'] => '',
    );

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new CrewRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : CrewRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $crewList = array();

        list($count, $crewList) = $this->getRepository()->scenario(
            CrewRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        $this->render(new CrewListView($crewList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $departmentId = 0;
        $sort = $request->get('sort', '-updateTime');
        $realName = $request->get('realName', '');
        $cellphone = $request->get('cellphone', '');
        $category = $request->get('category', '');
        $category = marmot_decode($category);
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();

        if (empty($userGroupId)) {
            $userGroupId = $request->get('userGroupId', '');
            $userGroupId = marmot_decode($userGroupId);
            $departmentId = $request->get('departmentId', '');
            $departmentId = marmot_decode($departmentId);
        }

        $filter = array();

        if (!empty($realName)) {
            $filter['realName'] = $realName;
        }
        if (!empty($cellphone)) {
            $filter['cellphone'] = $cellphone;
        }
        if (!empty($userGroupId)) {
            $filter['userGroup'] = $userGroupId;
        }
        if (!empty($departmentId)) {
            $filter['department'] = $departmentId;
        }

        $filter['category'] = '';

        $currentLoginCrewCategory = Core::$container->get('crew')->getCategory();

        if (isset(self::CATEGORY_SCENE[$currentLoginCrewCategory])) {
            $filter['category'] = self::CATEGORY_SCENE[$currentLoginCrewCategory];
        }
        
        if (!empty($category)) {
            $filter['category'] = $category;
        }

        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $crew = $this->getRepository()->scenario(CrewRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        if ($crew instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new CrewView($crew));
        return true;
    }

    /**
     * 获取个人信息
     */
    public function personalInformation()
    {
        $id = Core::$container->get('crew')->getId();

        $crew = $this->getRepository()->scenario(CrewRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
        
        if ($crew instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new PersonalInformationView($crew));
        return true;
    }
}
