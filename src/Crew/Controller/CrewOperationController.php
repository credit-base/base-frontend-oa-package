<?php
namespace Base\Package\Crew\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\Crew\Repository\CrewRepository;
use Sdk\Crew\Command\Crew\AddCrewCommand;
use Sdk\Crew\Command\Crew\EditCrewCommand;
use Sdk\Crew\Command\Crew\SignInCrewCommand;
use Sdk\Crew\Command\Crew\SignOutCrewCommand;
use Sdk\Crew\CommandHandler\Crew\CrewCommandHandlerFactory;

use Base\Package\Crew\View\Json\CrewView;
use Base\Package\Crew\View\Json\SignInView;

class CrewOperationController extends Controller implements IOperateAbleController
{
    use WebTrait, UtilsTrait, OperateControllerTrait, CrewValidateTrait;

    protected $repository;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new CrewCommandHandlerFactory());
        $this->repository = new CrewRepository();
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        $request = $this->getRequest();

        $cardId =  $request->post('cardId', '');
        $realName =  $request->post('realName', '');
        $cellphone =  $request->post('cellphone', '');
        $purview =  $request->post('purview', array());
        $userGroupId = $request->post('userGroupId', '');
        $userGroupId = intval(marmot_decode($userGroupId));
        $departmentId = $request->post('departmentId', '');
        $departmentId = intval(marmot_decode($departmentId));
        $category = $request->post('category', '');
        $category = intval(marmot_decode($category));

        $password = $request->post('password', '');
        $password = $this->decrypt($password);
        $password = is_null($password) ? '' : $password;

        $confirmPassword = $request->post('confirmPassword', '');
        $confirmPassword = $this->decrypt($confirmPassword);
        $confirmPassword = is_null($confirmPassword) ? '' : $confirmPassword;

        if ($this->validateCommonScenario(
            $realName,
            $cardId,
            $category,
            $purview,
            $userGroupId,
            $departmentId
        ) && $this->validateAddScenario(
            $cellphone,
            $password,
            $confirmPassword
        )) {
            $command = new AddCrewCommand(
                $realName,
                $cellphone,
                $password,
                $cardId,
                $userGroupId,
                $category,
                $departmentId,
                $purview
            );
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        $crew = $this->getRepository()->scenario(CrewRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        if ($crew instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new CrewView($crew));

        return true;
    }

    protected function editAction(int $id)
    {
        $request = $this->getRequest();

        $cardId =  $request->post('cardId', '');
        $realName =  $request->post('realName', '');
        $userGroupId = $request->post('userGroupId', '');
        $userGroupId = intval(marmot_decode($userGroupId));
        $departmentId = $request->post('departmentId', '');
        $departmentId = intval(marmot_decode($departmentId));
        $purview =  $request->post('purview', array());
        $category = $request->post('category', '');
        $category = intval(marmot_decode($category));

        if ($this->validateCommonScenario(
            $realName,
            $cardId,
            $category,
            $purview,
            $userGroupId,
            $departmentId
        )) {
            $command = new EditCrewCommand(
                $realName,
                $cardId,
                $id,
                $userGroupId,
                $category,
                $departmentId,
                $purview
            );
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function signIn()
    {
        $request = $this->getRequest();

        $userName = $request->post('userName', '');
        $captcha = $request->post('captcha', '');
        $password = $request->post('password', '');
        $password = $this->decrypt($password);
        $password = is_null($password) ? '' : $password;

        if ($this->validateSignInScenario(
            $userName,
            $password,
            $captcha
        )) {
            $command = new SignInCrewCommand(
                $userName,
                $password
            );

            if ($this->getCommandBus()->send($command)) {
                $res = Core::$container->get('jwt');

                $this->render(new SignInView($res));
                return true;
            }
        }
        
        $this->displayError();
        return false;
    }
    
    public function signOut()
    {
        if ($this->getCommandBus()->send(new SignOutCrewCommand())) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
}
