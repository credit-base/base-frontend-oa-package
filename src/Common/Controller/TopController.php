<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Factory\TopControllerFactory;
use Base\Package\Common\Controller\Interfaces\ITopAbleController;

use Base\Package\Common\Controller\Traits\ValidateUrlTrait;

class TopController extends Controller
{
    use WebTrait, ValidateUrlTrait;
    
    protected function getTopController(string $resource) : ITopAbleController
    {
        return TopControllerFactory::getTopController($resource);
    }

    public function index(string $resource, $id, string $status)
    {
        $id = marmot_decode($id);

        if ($this->validateIndexScenario($id)) {
            $enableController = $this->getTopController($resource);
            return $enableController->$status($id, $resource);
        }

        $this->displayError();
        return false;
    }
}
