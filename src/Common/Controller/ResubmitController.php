<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ValidateUrlTrait;
use Base\Package\Common\Controller\Interfaces\IResubmitAbleController;
use Base\Package\Common\Controller\Factory\ResubmitControllerFactory;

class ResubmitController extends Controller
{
    use WebTrait, ValidateUrlTrait;
    
    protected function getResubmitController(string $resource) : IResubmitAbleController
    {
        return ResubmitControllerFactory::getResubmitController($resource);
    }

    public function resubmit(string $resource, $id): bool
    {
        $id = marmot_decode($id);
        if ($this->validateIndexScenario($id)) {
            $resubmitController = $this->getResubmitController($resource);

            return $resubmitController->resubmit($id, $resource);
        }

        $this->displayError();
        return false;
    }
}
