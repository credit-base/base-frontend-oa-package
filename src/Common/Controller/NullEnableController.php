<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Package\Common\Controller\Interfaces\IEnableAbleController;

class NullEnableController implements IEnableAbleController, INull
{
    public function enable(int $id, string $resource = '')
    {
        unset($id);
        unset($resource);
        Core::setLastError(ROUTE_NOT_EXIST);
    }

    public function disable(int $id, string $resource = '')
    {
        unset($id);
        unset($resource);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
