<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Package\Common\Controller\Interfaces\IRevokeAbleController;

class NullRevokeController implements IRevokeAbleController, INull
{
    public function revoke(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
