<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Package\Common\Controller\Interfaces\IDeleteAbleController;

class NullDeleteController implements IDeleteAbleController, INull
{
    public function delete(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
