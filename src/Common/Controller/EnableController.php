<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ValidateUrlTrait;
use Base\Package\Common\Controller\Factory\EnableControllerFactory;
use Base\Package\Common\Controller\Interfaces\IEnableAbleController;

class EnableController extends Controller
{
    use WebTrait, ValidateUrlTrait;
    
    protected function getEnableController(string $resource) : IEnableAbleController
    {
        return EnableControllerFactory::getEnableController($resource);
    }

    public function index(string $resource, $id, string $status)
    {
        $id = marmot_decode($id);

        if ($this->validateIndexScenario($id)) {
            $enableController = $this->getEnableController($resource);
            return $enableController->$status($id, $resource);
        }

        $this->displayError();
        return false;
    }
}
