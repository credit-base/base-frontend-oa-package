<?php
namespace Base\Package\Common\Controller\Traits;

use Base\Package\Common\Utils\Captcha;

trait CaptchaTrait
{

    protected function validateCaptcha(string $phrase) : bool
    {
        $captcha = new Captcha();
        return $captcha->validate($phrase);
    }
}
