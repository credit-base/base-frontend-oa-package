<?php
namespace Base\Package\Common\Controller\Traits;

trait OperateControllerTrait
{
    use GlobalCheckTrait;
    /**
     * 如果是GET请求返回页面, POST请求提交数据
     */
    public function add(string $resource = '')
    {
        if ($this->getRequest()->isGetMethod()) {
            return $this->globalCheck($resource) && $this->addView();
        }

        return $this->globalCheck($resource) && $this->addAction() ;
    }

    abstract protected function addView() : bool;
    /**
     * 请求数据前需要先判断数据是否合理: 验证添加场景
     * 验证成功进行 提交添加场景
     */
    abstract protected function addAction() : bool;

    public function edit(int $id, string $resource = '')
    {
        if ($this->getRequest()->isGetMethod()) {
            return $this->globalCheck($resource) && $this->editView($id);
        }

        return $this->globalCheck($resource) && $this->editAction($id);
    }
    abstract protected function editView(int $id) : bool;

    abstract protected function editAction(int $id) : bool;
}
