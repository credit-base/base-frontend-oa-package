<?php
namespace Base\Package\Common\Controller\Traits;

trait ToolTrait
{
    //换行
    protected function formatString($str)
    {
      //ℑ为占位符，替换掉&lt;br&gt;，用来做字符串长度的验证
        $str = str_replace("&lt;br&gt;", "ℑ", $str);
        $str = str_replace("\'", "'", $str);

        return $str;
    }

    /**
     *
     */
    protected function implodeArray(array $array) : string
    {
        $idDecodeData = array();
        
        foreach ($array as $val) {
            $idDecodeData[] = marmot_decode($val);
        }

        return implode(',', $idDecodeData);
    }

    /**
     *
     */
    protected function getImplodeIds(array $objects) : string
    {
        $objectIds = array();
        foreach ($objects as $object) {
            $objectIds[] = $object->getId();
        }

        return implode(",", $objectIds);
    }
}
