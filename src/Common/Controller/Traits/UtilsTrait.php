<?php
namespace Base\Package\Common\Controller\Traits;

use Base\Package\Common\Controller\Traits\CryptojsTrait;
use Base\Package\Common\Controller\Traits\CaptchaTrait;

trait UtilsTrait
{
    use CryptojsTrait, CaptchaTrait;
}
