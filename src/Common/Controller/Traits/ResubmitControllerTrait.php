<?php
namespace Base\Package\Common\Controller\Traits;

trait ResubmitControllerTrait
{
    use GlobalCheckTrait;

    public function resubmit(int $id, string $resource = '')
    {
        return $this->globalCheck($resource) && $this->resubmitAction($id);
    }

    abstract protected function resubmitAction(int $id) : bool;
}
