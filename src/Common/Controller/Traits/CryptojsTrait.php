<?php
namespace Base\Package\Common\Controller\Traits;

use Base\Package\Common\Utils\Cryptojs;

trait CryptojsTrait
{
    public function decrypt(string $jsonString)
    {
        $cryptojs = new Cryptojs();
        return $cryptojs->decrypt($jsonString);
    }

    public function hash()
    {
        $cryptojs = new Cryptojs();
        return $cryptojs->generateHash();
    }
}
