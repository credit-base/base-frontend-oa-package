<?php
namespace Base\Package\Common\Controller\Traits;

trait ApproveControllerTrait
{
    use GlobalCheckTrait;

    public function approve(int $id, string $resource = '')
    {
        return $this->globalCheck($resource) && $this->approveAction($id) ?
            $this->displaySuccess() : $this->displayError();
    }

    abstract protected function approveAction(int $id) : bool;

    public function reject(int $id, string $resource = '')
    {
        return $this->globalCheck($resource) && $this->rejectAction($id) ?
            $this->displaySuccess() : $this->displayError();
    }

    abstract protected function rejectAction(int $id) : bool;
}
