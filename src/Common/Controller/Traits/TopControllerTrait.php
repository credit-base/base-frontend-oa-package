<?php
namespace Base\Package\Common\Controller\Traits;

trait TopControllerTrait
{
    use GlobalCheckTrait;
    
    public function top(int $id, string $resource = '')
    {
        return $this->globalCheck($resource) && $this->topAction($id) ?
            $this->displaySuccess() : $this->displayError();
    }

    abstract protected function topAction(int $id) : bool;

    public function cancelTop(int $id, string $resource = '')
    {
        return $this->globalCheck($resource) && $this->cancelTopAction($id) ?
            $this->displaySuccess() : $this->displayError();
    }

    abstract protected function cancelTopAction(int $id) : bool;
}
