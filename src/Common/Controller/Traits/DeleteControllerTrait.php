<?php
namespace Base\Package\Common\Controller\Traits;

trait DeleteControllerTrait
{
    use GlobalCheckTrait;

    public function delete(int $id, string $resource = '')
    {
        return $this->globalCheck($resource) && $this->deleteAction($id) ?
                $this->displaySuccess() :
                $this->displayError();
    }

    abstract protected function deleteAction(int $id) : bool;
}
