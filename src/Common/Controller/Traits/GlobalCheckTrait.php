<?php
namespace Base\Package\Common\Controller\Traits;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Crew\Model\Crew;

use Base\Sdk\Purview\Model\PurviewCategoryFactory;

trait GlobalCheckTrait
{
    protected function getPurviewCategoryFactory() : PurviewCategoryFactory
    {
        return new PurviewCategoryFactory();
    }

    public function globalCheck(string $resource = '')
    {
        return $this->checkUserExist() && $this->checkUserStatusEnable() && $this->checkUserHasPurview($resource);
    }

    protected function checkUserExist() : bool
    {
        if (Core::$container->get('crew') instanceof INull) {
            Core::setLastError(NEED_SIGNIN);
            return false;
        }

        return true;
    }

    protected function checkUserStatusEnable() : bool
    {
        if (Core::$container->get('crew')->isDisabled()) {
            Core::setLastError(USER_STATUS_DISABLE);
            return false;
        }

        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        if (Core::$container->get('purview.status')) {
            $category = $this->getPurviewCategoryFactory()->getCategory($resource);
            $purview = Core::$container->get('crew')->getPurview();
            $crewCategory = Core::$container->get('crew')->getCategory();
            
            if ($crewCategory != Crew::CATEGORY['SUPERTUBE']) {
                if (!in_array($category, $purview)) {
                    Core::setLastError(PURVIEW_UNDEFINED);
                    return false;
                }
            }
        }

        return true;
    }
}
