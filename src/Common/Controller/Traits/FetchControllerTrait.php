<?php
namespace Base\Package\Common\Controller\Traits;

trait FetchControllerTrait
{
    use GlobalCheckTrait;
    
    public function getPageAndSize($size = 10)
    {
        $size = $this->getRequest()->get('limit', $size);
        $page = $this->getRequest()->get('page', 1);
     
        return [$size, $page];
    }

    public function filter(string $resource = '') : bool
    {
        if ($this->globalCheck($resource) && $this->filterAction()) {
            return true;
        }

        $this->displayError();
        return false;
    }

    abstract protected function filterAction() : bool;

    public function fetchOne($id, string $resource = '') : bool
    {
        if ($this->globalCheck($resource) && $this->fetchOneAction($id)) {
            return true;
        }
        $this->displayError();
        return false;
    }

    abstract protected function fetchOneAction(int $id) : bool;
}
