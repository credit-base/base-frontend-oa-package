<?php
namespace Base\Package\Common\Controller\Traits;

trait EnableControllerTrait
{
    use GlobalCheckTrait;
    
    public function enable(int $id, string $resource = '')
    {
        return $this->globalCheck($resource) && $this->enableAction($id) ?
                $this->displaySuccess() :
                $this->displayError();
    }

    abstract protected function enableAction(int $id) : bool;

    public function disable(int $id, string $resource = '')
    {
        return $this->globalCheck($resource) && $this->disableAction($id) ?
                $this->displaySuccess() :
                $this->displayError();
    }

    abstract protected function disableAction(int $id) : bool;
}
