<?php
namespace Base\Package\Common\Controller\Traits;

trait RevokeControllerTrait
{
    use GlobalCheckTrait;

    public function revoke(int $id, string $resource = '')
    {
        return $this->globalCheck($resource) && $this->revokeAction($id)
                ? $this->displaySuccess()
                : $this->displayError();
    }

    abstract protected function revokeAction(int $id) : bool;
}
