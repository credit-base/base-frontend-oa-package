<?php
namespace Base\Package\Common\Controller\Traits;

use Sdk\Common\WidgetRules\WidgetRules;

trait ValidateUrlTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }

    protected function validateIndexScenario($id) : bool
    {
        return $this->getWidgetRules()->formatNumeric($id, 'id');
    }
}
