<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullResubmitController;
use Base\Package\Common\Controller\Interfaces\IResubmitAbleController;

/**
 * @todo 该文件没有被创建，但是此项目中需要该文件，实际依赖了上层credit-frontend-oa项目中的
 *       `Common\Controller\Factory\ResubmitControllerFactory`文件
 */
class ResubmitControllerFactory
{
    const MAPS = array(
        'unAuditedPraises'=>'\Base\Package\Interaction\Controller\Praise\UnAuditPraiseOperationController',
        'unAuditedQas'=>'\Base\Package\Interaction\Controller\Qa\UnAuditQaOperationController',
        'unAuditedAppeals'=>'\Base\Package\Interaction\Controller\Appeal\UnAuditAppealOperationController',
        'unAuditedFeedbacks'=>'\Base\Package\Interaction\Controller\Feedback\UnAuditFeedbackOperationController',
        'unAuditedComplaints'=>'\Base\Package\Interaction\Controller\Complaint\UnAuditComplaintOperationController',
    );

    public static function getResubmitController(string $resource) : IResubmitAbleController
    {
        $operationController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';
        
        return class_exists($operationController) ? new $operationController : new NullResubmitController();
    }
}
