<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullTopController;
use Base\Package\Common\Controller\Interfaces\ITopAbleController;

class TopControllerFactory
{
    const MAPS = array(
        'news'=>'\Base\Package\News\Controller\NewsTopController',
    );

    public static function getTopController(string $resource) : ITopAbleController
    {
        $topController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($topController) ? new $topController : new NullTopController();
    }
}
