<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullFetchController;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

/**
 * @todo 该文件没有被创建，但是此项目中需要该文件，实际依赖了上层credit-frontend-oa项目中的
 *       `Common\Controller\Factory\FetchControllerFactory`文件，
 */
class FetchControllerFactory
{
    const MAPS = array(
        'crews'=>'\Base\Package\Crew\Controller\CrewFetchController',
        'departments'=>'\Base\Package\UserGroup\Controller\DepartmentFetchController',
        'userGroups'=>'\Base\Package\UserGroup\Controller\UserGroupFetchController',
        'baseTemplates'=>'\Base\Package\Template\Controller\BaseTemplateFetchController',
        'gbTemplates'=>'\Base\Package\Template\Controller\GbTemplateFetchController',
        'bjTemplates'=>'\Base\Package\Template\Controller\BjTemplateFetchController',
        'wbjTemplates'=>'\Base\Package\Template\Controller\WbjTemplateFetchController',
        'workOrderTasks'=>'\Base\Package\WorkOrderTask\Controller\WorkOrderTaskFetchController',
        'parentTasks'=>'\Base\Package\WorkOrderTask\Controller\ParentTaskFetchController',
        'news'=>'\Base\Package\News\Controller\NewsFetchController',
        'unAuditedNews'=>'\Base\Package\News\Controller\UnAuditNewsFetchController',
        'members'=>'\Base\Package\Member\Controller\MemberFetchController',
        'journals'=>'\Base\Package\Journal\Controller\JournalFetchController',
        'unAuditedJournals'=>'\Base\Package\Journal\Controller\UnAuditJournalFetchController',
        'creditPhotography'=>'\Base\Package\CreditPhotography\Controller\CreditPhotographyFetchController',
        'wbjSearchData'=>'\Base\Package\ResourceCatalog\Controller\WbjSearchDataFetchController',
        'bjSearchData'=>'\Base\Package\ResourceCatalog\Controller\BjSearchDataFetchController',
        'gbSearchData'=>'\Base\Package\ResourceCatalog\Controller\GbSearchDataFetchController',
        'rules'=>'\Base\Package\Rule\Controller\RuleFetchController',
        'unAuditedRules'=>'\Base\Package\Rule\Controller\UnAuditRuleFetchController',
        'baseRules'=>'\Base\Package\Rule\Controller\BaseRuleFetchController',
        'unAuditedBaseRules'=>'\Base\Package\Rule\Controller\UnAuditBaseRuleFetchController',
        'gbTasks'=>'\Base\Package\ResourceCatalog\Controller\Task\GbTaskFetchController',
        'bjTasks'=>'\Base\Package\ResourceCatalog\Controller\Task\BjTaskFetchController',
        'wbjTasks'=>'\Base\Package\ResourceCatalog\Controller\Task\WbjTaskFetchController',
        'errorData'=>'\Base\Package\ResourceCatalog\Controller\ErrorDataFetchController',
        'websiteCustomizes'=>'\Base\Package\WebsiteCustomize\Controller\WebsiteCustomizeFetchController',
        'enterprises'=>'\Base\Package\Enterprise\Controller\EnterpriseFetchController',
        'qzjTemplates'=>'\Base\Package\Template\Controller\QzjTemplateFetchController',

        'praises'=>'\Base\Package\Interaction\Controller\Praise\PraiseFetchController',
        'unAuditedPraises'=>'\Base\Package\Interaction\Controller\Praise\UnAuditPraiseFetchController',
        'qas'=>'\Base\Package\Interaction\Controller\Qa\QaFetchController',
        'unAuditedQas'=>'\Base\Package\Interaction\Controller\Qa\UnAuditQaFetchController',
        'appeals'=>'\Base\Package\Interaction\Controller\Appeal\AppealFetchController',
        'unAuditedAppeals'=>'\Base\Package\Interaction\Controller\Appeal\UnAuditAppealFetchController',
        'feedbacks'=>'\Base\Package\Interaction\Controller\Feedback\FeedbackFetchController',
        'unAuditedFeedbacks'=>'\Base\Package\Interaction\Controller\Feedback\UnAuditFeedbackFetchController',
        'complaints'=>'\Base\Package\Interaction\Controller\Complaint\ComplaintFetchController',
        'unAuditedComplaints'=>'\Base\Package\Interaction\Controller\Complaint\UnAuditComplaintFetchController',
    );
        
    public static function getFetchController(string $resource) : IFetchAbleController
    {
        $fetchController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($fetchController) ? new $fetchController : new NullFetchController();
    }
}
