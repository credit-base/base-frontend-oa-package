<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullOperationController;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

/**
 * @todo 该文件没有被创建，但是此项目中需要该文件，实际依赖了上层credit-frontend-oa项目中的
 *       `Common\Controller\Factory\OperationControllerFactory`文件
 */
class OperationControllerFactory
{
    const MAPS = array(
        'crews'=>'\Base\Package\Crew\Controller\CrewOperationController',
        'departments'=>'\Base\Package\UserGroup\Controller\DepartmentOperationController',
        'baseTemplates'=>'\Base\Package\Template\Controller\BaseTemplateOperationController',
        'gbTemplates'=>'\Base\Package\Template\Controller\GbTemplateOperationController',
        'bjTemplates'=>'\Base\Package\Template\Controller\BjTemplateOperationController',
        'wbjTemplates'=>'\Base\Package\Template\Controller\WbjTemplateOperationController',
        'news'=>'\Base\Package\News\Controller\NewsOperationController',
        'unAuditedNews'=>'\Base\Package\News\Controller\UnAuditNewsOperationController',
        'journals'=>'\Base\Package\Journal\Controller\JournalOperationController',
        'unAuditedJournals'=>'\Base\Package\Journal\Controller\UnAuditJournalOperationController',
        'rules'=>'\Base\Package\Rule\Controller\RuleOperationController',
        'unAuditedRules'=>'\Base\Package\Rule\Controller\UnAuditRuleOperationController',
        'baseRules'=>'\Base\Package\Rule\Controller\BaseRuleOperationController',
        'unAuditedBaseRules'=>'\Base\Package\Rule\Controller\UnAuditBaseRuleOperationController',
        'errorData'=>'\Base\Package\ResourceCatalog\Controller\ErrorDataOperationController',
        'websiteCustomizes'=>'\Base\Package\WebsiteCustomize\Controller\WebsiteCustomizeOperationController',
        'qzjTemplates'=>'\Base\Package\Template\Controller\QzjTemplateOperationController',
    );

    public static function getOperationController(string $resource) : IOperateAbleController
    {
        $operationController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';
        
        return class_exists($operationController) ? new $operationController : new NullOperationController();
    }
}
