<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullEnableController;
use Base\Package\Common\Controller\Interfaces\IEnableAbleController;

class EnableControllerFactory
{
    const MAPS = array(
        'crews'=>'\Base\Package\Crew\Controller\CrewEnableController',
        'news'=>'\Base\Package\News\Controller\NewsEnableController',
        'members'=>'\Base\Package\Member\Controller\MemberEnableController',
        'journals'=>'\Base\Package\Journal\Controller\JournalEnableController',
        'wbjSearchData'=>'\Base\Package\ResourceCatalog\Controller\WbjSearchDataEnableController',
        'bjSearchData'=>'\Base\Package\ResourceCatalog\Controller\BjSearchDataEnableController',
        'gbSearchData'=>'\Base\Package\ResourceCatalog\Controller\GbSearchDataEnableController',
    );

    public static function getEnableController(string $resource) : IEnableAbleController
    {
        $enableController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($enableController) ? new $enableController : new NullEnableController();
    }
}
