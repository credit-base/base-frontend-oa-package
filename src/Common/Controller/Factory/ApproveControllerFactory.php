<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullApproveController;
use Base\Package\Common\Controller\Interfaces\IApproveAbleController;

class ApproveControllerFactory
{
    const MAPS = array(
        'unAuditedNews'=>'\Base\Package\News\Controller\UnAuditNewsApproveController',
        'unAuditedJournals'=>'\Base\Package\Journal\Controller\UnAuditJournalApproveController',
        'creditPhotography'=>'\Base\Package\CreditPhotography\Controller\CreditPhotographyApproveController',
        'unAuditedRules'=>'\Base\Package\Rule\Controller\UnAuditRuleApproveController',
        'unAuditedBaseRules'=>'\Base\Package\Rule\Controller\UnAuditBaseRuleApproveController',

        'unAuditedPraises'=>'\Base\Package\Interaction\Controller\Praise\UnAuditPraiseApproveController',
        'unAuditedQas'=>'\Base\Package\Interaction\Controller\Qa\UnAuditQaApproveController',
        'unAuditedAppeals'=>'\Base\Package\Interaction\Controller\Appeal\UnAuditAppealApproveController',
        'unAuditedFeedbacks'=>'\Base\Package\Interaction\Controller\Feedback\UnAuditFeedbackApproveController',
        'unAuditedComplaints'=>'\Base\Package\Interaction\Controller\Complaint\UnAuditComplaintApproveController',
    );

    public static function getApproveController(string $resource) : IApproveAbleController
    {
        $approveController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';
        
        return class_exists($approveController) ? new $approveController : new NullApproveController();
    }
}
