<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullRevokeController;
use Base\Package\Common\Controller\Interfaces\IRevokeAbleController;

class RevokeControllerFactory
{
    const MAPS = array(
        'unAuditedRules'=>'\Base\Package\Rule\Controller\UnAuditRuleRevokeController',
        'unAuditedBaseRules'=>'\Base\Package\Rule\Controller\UnAuditBaseRuleRevokeController',
    );

    public static function getRevokeController(string $resource) : IRevokeAbleController
    {
        $enableController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($enableController) ? new $enableController : new NullRevokeController();
    }
}
