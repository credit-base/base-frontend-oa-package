<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullDeleteController;
use Base\Package\Common\Controller\Interfaces\IDeleteAbleController;

class DeleteControllerFactory
{
    const MAPS = array(
        'bjSearchData'=>'\Base\Package\ResourceCatalog\Controller\BjSearchDataDeleteController',
        'gbSearchData'=>'\Base\Package\ResourceCatalog\Controller\GbSearchDataDeleteController',
        'rules'=>'\Base\Package\Rule\Controller\RuleDeleteController',
        'baseRules'=>'\Base\Package\Rule\Controller\BaseRuleDeleteController',
    );

    public static function getDeleteController(string $resource) : IDeleteAbleController
    {
        $deleteController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($deleteController) ? new $deleteController : new NullDeleteController();
    }
}
