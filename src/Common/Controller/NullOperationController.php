<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

class NullOperationController implements IOperateAbleController, INull
{
    public function add(string $resource = '')
    {
        unset($resource);
        Core::setLastError(ROUTE_NOT_EXIST);
    }

    public function edit(int $id, string $resource = '')
    {
        unset($id);
        unset($resource);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
