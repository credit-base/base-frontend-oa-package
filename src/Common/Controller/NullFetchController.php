<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

class NullFetchController implements IFetchAbleController, INull
{
    public function filter(string $resource = '')
    {
        unset($resource);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
    
    public function fetchOne($id, string $resource = '')
    {
        unset($resource);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
