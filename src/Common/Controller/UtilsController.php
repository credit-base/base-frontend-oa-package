<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Utils\Cryptojs;
use Base\Package\Common\Utils\Captcha;
use Base\Package\Common\Utils\OSS;

use Base\Package\Common\View\Json\UtilsView;

class UtilsController extends Controller
{
    use WebTrait;

    //获取hash
    public function getHash() : bool
    {
        $cryptojs = new Cryptojs();
        $hash = $cryptojs->generateHash();

        $data = ['hash'=>$hash];

        $this->render(new UtilsView($data));
        return true;
    }

    //获取图形验证
    public function captcha()
    {
        $captcha = $this->getCaptcha();
        $captcha->render();
    }

    protected function getCaptcha():Captcha
    {
        return new Captcha();
    }
}
