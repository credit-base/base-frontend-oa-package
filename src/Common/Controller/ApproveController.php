<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Factory\ApproveControllerFactory;
use Base\Package\Common\Controller\Interfaces\IApproveAbleController;

use Base\Package\Common\Controller\Traits\ValidateUrlTrait;

class ApproveController extends Controller
{
    use WebTrait, ValidateUrlTrait;
    
    protected function getApproveController(string $resource) : IApproveAbleController
    {
        return ApproveControllerFactory::getApproveController($resource);
    }

    public function index(string $resource, $id, string $status)
    {
        $id = marmot_decode($id);
        
        if ($this->validateIndexScenario($id)) {
            $approveController = $this->getApproveController($resource);
           
            return $approveController->$status($id, $resource);
        }

        $this->displayError();
        return false;
    }
}
