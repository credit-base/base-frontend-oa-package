<?php
namespace Base\Package\Common\Controller\Interfaces;

interface IEnableAbleController
{
    public function enable(int $id, string $resource = '');

    public function disable(int $id, string $resource = '');
}
