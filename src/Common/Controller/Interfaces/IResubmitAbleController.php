<?php
namespace Base\Package\Common\Controller\Interfaces;

interface IResubmitAbleController
{
    public function resubmit(int $id, string $resource = '');
}
