<?php
namespace Base\Package\Common\Controller\Interfaces;

interface IFetchAbleController
{
    const LIST_SCENE = array(
        'LIST' => 1,
        'MY_LIST' => 2
    );

    const UNAUDITED_LIST_SCENE = array(
        'PUBLISHER_LIST' => 1,
        'AUDITOR_LIST' => 2
    );
    
    public function filter(string $resource = '');
    
    public function fetchOne($id, string $resource = '');
}
