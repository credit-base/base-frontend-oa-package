<?php
namespace Base\Package\Common\Controller\Interfaces;

interface IDeleteAbleController
{
    public function delete(int $id);
}
