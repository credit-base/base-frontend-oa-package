<?php
namespace Base\Package\Common\Controller\Interfaces;

interface IOperateAbleController
{
    public function add(string $resource = '');

    public function edit(int $id, string $resource = '');
}
