<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Package\Common\Controller\Interfaces\ITopAbleController;

class NullTopController implements ITopAbleController, INull
{
    public function top(int $id, string $resource = '')
    {
        unset($id);
        unset($resource);
        Core::setLastError(ROUTE_NOT_EXIST);
    }

    public function cancelTop(int $id, string $resource = '')
    {
        unset($id);
        unset($resource);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
