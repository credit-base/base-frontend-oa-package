<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ValidateUrlTrait;
use Base\Package\Common\Controller\Factory\DeleteControllerFactory;
use Base\Package\Common\Controller\Interfaces\IDeleteAbleController;

class DeleteController extends Controller
{
    use WebTrait, ValidateUrlTrait;

    protected function getDeleteController(string $resource) : IDeleteAbleController
    {
        return DeleteControllerFactory::getDeleteController($resource);
    }

    public function delete(string $resource, $id)
    {
        $id = marmot_decode($id);
        
        if ($this->validateIndexScenario($id)) {
            $deleteController = $this->getDeleteController($resource);
           
            return $deleteController->delete($id, $resource);
        }

        $this->displayError();
        return false;
    }
}
