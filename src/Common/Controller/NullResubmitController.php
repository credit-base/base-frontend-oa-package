<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Package\Common\Controller\Interfaces\IResubmitAbleController;

class NullResubmitController implements IResubmitAbleController, INull
{
    public function resubmit(int $id, string $resource = '')
    {
        unset($id);
        unset($resource);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
