<?php
namespace Base\Package\Common\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

class UtilsView extends JsonView implements IView
{
    private $data;

    public function __construct($data = array())
    {
        $this->data = $data;
        parent::__construct();
    }

    protected function getData():array
    {
        return $this->data;
    }

    public function display(): void
    {
        $data = $this->getData();

        $this->encode($data);
    }
}
