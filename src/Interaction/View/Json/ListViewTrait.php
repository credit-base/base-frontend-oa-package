<?php
namespace Base\Package\Interaction\View\Json;

trait ListViewTrait
{
    private $list;

    private $count;

    public function __construct(
        array $list,
        int $count
    ) {
        $this->list = $list;
        $this->count = $count;
        parent::__construct();
    }

    protected function getList(): array
    {
        return $this->list;
    }

    protected function getCount(): int
    {
        return $this->count;
    }
}
