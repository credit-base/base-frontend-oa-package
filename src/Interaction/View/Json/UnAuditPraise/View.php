<?php
namespace Base\Package\Interaction\View\Json\UnAuditPraise;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Praise\UnAuditPraiseTranslator;

use Base\Package\Interaction\View\Json\ViewTrait;

class View extends JsonView implements IView
{
    use ViewTrait;

    protected function getUnAuditPraiseTranslator(): UnAuditPraiseTranslator
    {
        return new UnAuditPraiseTranslator();
    }

    public function display(): void
    {
        $unAuditPraiseData = array();

        $unAuditPraiseData = $this->getUnAuditPraiseTranslator()->objectToArray(
            $this->getData()
        );

        $unAuditPraiseData = $this->getFormatData($unAuditPraiseData);
        $unAuditPraiseData['praiseData'] = $unAuditPraiseData['interaction'];
        unset($unAuditPraiseData['interaction']);

        $this->encode($unAuditPraiseData);
    }
}
