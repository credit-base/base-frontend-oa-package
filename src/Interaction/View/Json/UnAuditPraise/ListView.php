<?php
namespace Base\Package\Interaction\View\Json\UnAuditPraise;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Praise\UnAuditPraiseTranslator;

use Base\Package\Interaction\View\Json\ListViewTrait;

class ListView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getUnAuditPraiseTranslator(): UnAuditPraiseTranslator
    {
        return new UnAuditPraiseTranslator();
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $unAuditPraise) {
            $data[] = $this->getUnAuditPraiseTranslator()->objectToArray($unAuditPraise);
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
