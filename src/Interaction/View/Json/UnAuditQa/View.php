<?php
namespace Base\Package\Interaction\View\Json\UnAuditQa;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Qa\UnAuditQaTranslator;

use Base\Package\Interaction\View\Json\ViewTrait;

class View extends JsonView implements IView
{
    use ViewTrait;

    protected function getUnAuditQaTranslator(): UnAuditQaTranslator
    {
        return new UnAuditQaTranslator();
    }

    public function display(): void
    {
        $unAuditQaData = array();

        $unAuditQaData = $this->getUnAuditQaTranslator()->objectToArray(
            $this->getData()
        );

        $unAuditQaData = $this->getFormatData($unAuditQaData);
        $unAuditQaData['qaData'] = $unAuditQaData['interaction'];
        unset($unAuditQaData['interaction']);

        $this->encode($unAuditQaData);
    }
}
