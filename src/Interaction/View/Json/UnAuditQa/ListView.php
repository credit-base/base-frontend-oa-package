<?php
namespace Base\Package\Interaction\View\Json\UnAuditQa;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Qa\UnAuditQaTranslator;

use Base\Package\Interaction\View\Json\ListViewTrait;

class ListView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getUnAuditQaTranslator(): UnAuditQaTranslator
    {
        return new UnAuditQaTranslator();
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $unAuditQa) {
            $data[] = $this->getUnAuditQaTranslator()->objectToArray($unAuditQa);
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
