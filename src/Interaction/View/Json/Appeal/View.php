<?php
namespace Base\Package\Interaction\View\Json\Appeal;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Appeal\AppealTranslator;

use Base\Package\Interaction\View\Json\ViewTrait;

class View extends JsonView implements IView
{
    use ViewTrait;

    protected function getAppealTranslator(): AppealTranslator
    {
        return new AppealTranslator();
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getAppealTranslator()->objectToArray(
            $this->getData(),
            array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'certificates',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'member'=>['id','realName'],
                'reply'=>[],
            )
        );

        $data = $this->getFormatData($data);
        $data['appealData'] = $data['interaction'];
        unset($data['interaction']);

        $this->encode($data);
    }
}
