<?php
namespace Base\Package\Interaction\View\Json\Appeal;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Appeal\AppealTranslator;

use Base\Package\Interaction\View\Json\ListViewTrait;

class ListView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getAppealTranslator(): AppealTranslator
    {
        return new AppealTranslator();
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $appeal) {
            $data[] = $this->getAppealTranslator()->objectToArray($appeal);
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
