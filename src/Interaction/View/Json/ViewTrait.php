<?php
namespace Base\Package\Interaction\View\Json;

trait ViewTrait
{
    private $data;

    private $count;

    public function __construct($data)
    {
        $this->data = $data;
        parent::__construct();
    }

    protected function getData()
    {
        return $this->data;
    }

    protected function getFormatData(array $arrayData):array
    {
        $replyAcceptData = [
           'acceptStatus'=> $arrayData['acceptStatus'],
           'acceptUserGroup'=> $arrayData['acceptUserGroup']
        ];
        unset($arrayData['acceptStatus']);
        unset($arrayData['acceptUserGroup']);

        $arrayData['reply'] = array_merge($arrayData['reply'], $replyAcceptData);

        $arrayData['interaction'] = [
            'title'=>$arrayData['title'],
            'type'=>$arrayData['type'],
            'name'=>$arrayData['name'],
            'identify'=>$arrayData['identify'],
            'images'=>$arrayData['images'],
            'content'=>$arrayData['content'],
            'contact'=>$arrayData['contact'],
            'updateTimeFormat'=>$arrayData['updateTimeFormat'],
            'status'=>$arrayData['status'],
            'member'=>$arrayData['member'],
        ];

        if (isset($arrayData['certificates'])) {
            $arrayData['interaction']['certificates'] = $arrayData['certificates'];
        }

        if (isset($arrayData['subject'])) {
            $arrayData['interaction']['subject'] = $arrayData['subject'];
        }

        unset($arrayData['title']);
        unset($arrayData['type']);
        unset($arrayData['name']);
        unset($arrayData['identify']);
        unset($arrayData['certificates']);
        unset($arrayData['content']);
        unset($arrayData['contact']);
        unset($arrayData['updateTime']);
        unset($arrayData['updateTimeFormat']);
        unset($arrayData['status']);
        unset($arrayData['member']);
        unset($arrayData['subject']);
        unset($arrayData['images']);

        return $arrayData;
    }
}
