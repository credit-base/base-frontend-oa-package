<?php
namespace Base\Package\Interaction\View\Json\Praise;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Praise\PraiseTranslator;

use Base\Package\Interaction\View\Json\ViewTrait;

class View extends JsonView implements IView
{
    use ViewTrait;

    protected function getPraiseTranslator(): PraiseTranslator
    {
        return new PraiseTranslator();
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getPraiseTranslator()->objectToArray(
            $this->getData(),
            array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'member'=>['id','realName'],
                'reply'=>[],
            )
        );

        $data = $this->getFormatData($data);
        $data['praiseData'] = $data['interaction'];
        unset($data['interaction']);

        $this->encode($data);
    }
}
