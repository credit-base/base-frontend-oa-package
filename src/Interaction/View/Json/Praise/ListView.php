<?php
namespace Base\Package\Interaction\View\Json\Praise;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Praise\PraiseTranslator;

use Base\Package\Interaction\View\Json\ListViewTrait;

class ListView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getPraiseTranslator(): PraiseTranslator
    {
        return new PraiseTranslator();
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $appeal) {
            $data[] = $this->getPraiseTranslator()->objectToArray(
                $appeal,
                array(
                    'id',
                    'title',
                    'name',
                    'acceptStatus',
                    'admissibility',
                    'type',
                    'updateTime',
                    'status',
                    'acceptUserGroup'=>['id','name'],
                    'reply'=>[]
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
