<?php
namespace Base\Package\Interaction\View\Json\UnAuditFeedback;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Feedback\UnAuditFeedbackTranslator;

use Base\Package\Interaction\View\Json\ListViewTrait;

class ListView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getUnAuditFeedbackTranslator(): UnAuditFeedbackTranslator
    {
        return new UnAuditFeedbackTranslator();
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $unAuditFeedback) {
            $data[] = $this->getUnAuditFeedbackTranslator()->objectToArray($unAuditFeedback);
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
