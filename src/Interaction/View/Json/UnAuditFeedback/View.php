<?php
namespace Base\Package\Interaction\View\Json\UnAuditFeedback;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Feedback\UnAuditFeedbackTranslator;

use Base\Package\Interaction\View\Json\ViewTrait;

class View extends JsonView implements IView
{
    use ViewTrait;

    protected function getUnAuditFeedbackTranslator(): UnAuditFeedbackTranslator
    {
        return new UnAuditFeedbackTranslator();
    }

    public function display(): void
    {
        $unAuditFeedbackData = array();

        $unAuditFeedbackData = $this->getUnAuditFeedbackTranslator()->objectToArray(
            $this->getData()
        );

        $unAuditFeedbackData = $this->getFormatData($unAuditFeedbackData);
        $unAuditFeedbackData['feedbackData'] = $unAuditFeedbackData['interaction'];
        unset($unAuditFeedbackData['interaction']);

        $this->encode($unAuditFeedbackData);
    }
}
