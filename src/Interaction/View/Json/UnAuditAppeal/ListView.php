<?php
namespace Base\Package\Interaction\View\Json\UnAuditAppeal;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Appeal\UnAuditAppealTranslator;

use Base\Package\Interaction\View\Json\ListViewTrait;

class ListView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getUnAuditAppealTranslator(): UnAuditAppealTranslator
    {
        return new UnAuditAppealTranslator();
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $unAuditAppeal) {
            $data[] = $this->getUnAuditAppealTranslator()->objectToArray($unAuditAppeal);
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
