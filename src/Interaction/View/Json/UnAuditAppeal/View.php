<?php
namespace Base\Package\Interaction\View\Json\UnAuditAppeal;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Appeal\UnAuditAppealTranslator;

use Base\Package\Interaction\View\Json\ViewTrait;

class View extends JsonView implements IView
{
    use ViewTrait;

    protected function getUnAuditAppealTranslator(): UnAuditAppealTranslator
    {
        return new UnAuditAppealTranslator();
    }

    public function display(): void
    {
        $unAuditAppealData = array();

        $unAuditAppealData = $this->getUnAuditAppealTranslator()->objectToArray(
            $this->getData()
        );

        $unAuditAppealData = $this->getFormatData($unAuditAppealData);
        $unAuditAppealData['appealData'] = $unAuditAppealData['interaction'];
        unset($unAuditAppealData['interaction']);

        $this->encode($unAuditAppealData);
    }
}
