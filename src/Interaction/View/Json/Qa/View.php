<?php
namespace Base\Package\Interaction\View\Json\Qa;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Qa\QaTranslator;

use Base\Package\Interaction\View\Json\ViewTrait;

class View extends JsonView implements IView
{
    use ViewTrait;

    protected function getQaTranslator(): QaTranslator
    {
        return new QaTranslator();
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getQaTranslator()->objectToArray(
            $this->getData(),
            array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'member'=>['id','realName'],
                'reply'=>[],
            )
        );

        $data = $this->getFormatData($data);
        $data['qaData'] = $data['interaction'];
        unset($data['interaction']);

        $this->encode($data);
    }
}
