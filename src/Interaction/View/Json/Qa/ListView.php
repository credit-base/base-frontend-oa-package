<?php
namespace Base\Package\Interaction\View\Json\Qa;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Qa\QaTranslator;

use Base\Package\Interaction\View\Json\ListViewTrait;

class ListView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getQaTranslator(): QaTranslator
    {
        return new QaTranslator();
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $appeal) {
            $data[] = $this->getQaTranslator()->objectToArray($appeal);
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
