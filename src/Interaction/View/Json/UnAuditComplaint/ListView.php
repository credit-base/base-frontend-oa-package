<?php
namespace Base\Package\Interaction\View\Json\UnAuditComplaint;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Complaint\UnAuditComplaintTranslator;

use Base\Package\Interaction\View\Json\ListViewTrait;

class ListView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getUnAuditComplaintTranslator(): UnAuditComplaintTranslator
    {
        return new UnAuditComplaintTranslator();
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $unAuditComplaint) {
            $data[] = $this->getUnAuditComplaintTranslator()->objectToArray($unAuditComplaint);
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
