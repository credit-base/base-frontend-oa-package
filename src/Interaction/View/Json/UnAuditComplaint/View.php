<?php
namespace Base\Package\Interaction\View\Json\UnAuditComplaint;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Complaint\UnAuditComplaintTranslator;

use Base\Package\Interaction\View\Json\ViewTrait;

class View extends JsonView implements IView
{
    use ViewTrait;

    protected function getUnAuditComplaintTranslator(): UnAuditComplaintTranslator
    {
        return new UnAuditComplaintTranslator();
    }

    public function display(): void
    {
        $unAuditComplaintData = array();

        $unAuditComplaintData = $this->getUnAuditComplaintTranslator()->objectToArray(
            $this->getData()
        );

        $unAuditComplaintData = $this->getFormatData($unAuditComplaintData);
        $unAuditComplaintData['complaintData'] = $unAuditComplaintData['interaction'];
        unset($unAuditComplaintData['interaction']);

        $this->encode($unAuditComplaintData);
    }
}
