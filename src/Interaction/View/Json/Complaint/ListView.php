<?php
namespace Base\Package\Interaction\View\Json\Complaint;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;

use Base\Package\Interaction\View\Json\ListViewTrait;

class ListView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getComplaintTranslator(): ComplaintTranslator
    {
        return new ComplaintTranslator();
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $appeal) {
            $data[] = $this->getComplaintTranslator()->objectToArray($appeal);
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
