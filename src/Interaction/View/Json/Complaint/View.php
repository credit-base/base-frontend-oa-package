<?php
namespace Base\Package\Interaction\View\Json\Complaint;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;

use Base\Package\Interaction\View\Json\ViewTrait;

class View extends JsonView implements IView
{
    use ViewTrait;

    protected function getComplaintTranslator(): ComplaintTranslator
    {
        return new ComplaintTranslator();
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getComplaintTranslator()->objectToArray(
            $this->getData(),
            array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'member'=>['id','realName'],
                'reply'=>[],
            )
        );

        $data = $this->getFormatData($data);
        $data['complaintData'] = $data['interaction'];
        unset($data['interaction']);

        $this->encode($data);
    }
}
