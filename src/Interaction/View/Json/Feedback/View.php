<?php
namespace Base\Package\Interaction\View\Json\Feedback;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

use Base\Package\Interaction\View\Json\ViewTrait;

class View extends JsonView implements IView
{
    use ViewTrait;

    protected function getFeedbackTranslator(): FeedbackTranslator
    {
        return new FeedbackTranslator();
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getFeedbackTranslator()->objectToArray(
            $this->getData(),
            array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'member'=>['id','realName'],
                'reply'=>[],
            )
        );

        $data = $this->getFormatData($data);
        $data['feedbackData'] = $data['interaction'];
        unset($data['interaction']);

        $this->encode($data);
    }
}
