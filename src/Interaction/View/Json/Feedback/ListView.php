<?php
namespace Base\Package\Interaction\View\Json\Feedback;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

use Base\Package\Interaction\View\Json\ListViewTrait;

class ListView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getFeedbackTranslator(): FeedbackTranslator
    {
        return new FeedbackTranslator();
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $appeal) {
            $data[] = $this->getFeedbackTranslator()->objectToArray($appeal);
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
