<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;

use Sdk\Interaction\Command\Praise\AcceptPraiseCommand;
use Sdk\Interaction\Command\Praise\PublishPraiseCommand;
use Sdk\Interaction\Command\Praise\UnPublishPraiseCommand;
use Sdk\Interaction\CommandHandler\Praise\PraiseCommandHandlerFactory;

use Base\Package\Interaction\Controller\InteractionValidateTrait;
use Base\Package\Interaction\Controller\RequestCommonTrait;

class PraiseOperationController extends Controller
{
    use WebTrait, UtilsTrait, InteractionValidateTrait, RequestCommonTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new PraiseCommandHandlerFactory());
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    public function accept(string $id) : bool
    {
        $id = marmot_decode($id);

        $requestData = $this->getReplyRequestCommonData();
        
        if ($this->validateCommonScenario(
            $requestData['content'],
            $requestData['images'],
            $requestData['admissibility']
        )) {
            $command = new AcceptPraiseCommand(
                $requestData,
                $id
            );
         
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function publish(string $id):bool
    {
        $id = marmot_decode($id);

        $command = new PublishPraiseCommand($id);
            
        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }

    public function unPublish(string $id):bool
    {
        $id = marmot_decode($id);

        $command = new UnPublishPraiseCommand($id);
            
        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
}
