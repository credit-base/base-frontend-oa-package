<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\UnAuditPraiseRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Interaction\View\Json\UnAuditPraise\View;
use Base\Package\Interaction\View\Json\UnAuditPraise\ListView;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class UnAuditPraiseFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditPraiseRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditPraiseRepository
    {
        return $this->repository;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        $resource = 'praises';
        return $this->checkUserHasInteractionPurview($resource);
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();
      
        $unAuditPraiseList = array();

        list($count, $unAuditPraiseList) = $this->getRepository()
            ->scenario(UnAuditPraiseRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($unAuditPraiseList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $unAuditPraise = $this->getRepository()
            ->scenario(UnAuditPraiseRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);
            
        if ($unAuditPraise instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($unAuditPraise));
        return true;
    }
}
