<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ApproveControllerTrait;
use Base\Package\Common\Controller\Interfaces\IApproveAbleController;

use Sdk\Interaction\Command\UnAuditPraise\ApproveUnAuditPraiseCommand;
use Sdk\Interaction\Command\UnAuditPraise\RejectUnAuditPraiseCommand;
use Sdk\Interaction\CommandHandler\UnAuditPraise\UnAuditPraiseCommandHandlerFactory;

use Base\Package\Interaction\Controller\InteractionValidateTrait;

class UnAuditPraiseApproveController extends Controller implements IApproveAbleController
{
    use WebTrait, ApproveControllerTrait, InteractionValidateTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditPraiseCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function approveAction(int $id) : bool
    {
        $command = new ApproveUnAuditPraiseCommand(
            $id
        );
        
        return $this->getCommandBus()->send($command);
    }

    protected function rejectAction(int $id) : bool
    {
        $request = $this->getRequest();
        
        $rejectReason = $request->post('rejectReason', '');

        if ($this->validateRejectScenario($rejectReason)) {
            $command = new RejectUnAuditPraiseCommand(
                $rejectReason,
                $id
            );

            return $this->getCommandBus()->send($command);
        }

        return false;
    }
}
