<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\UnAuditAppealRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Interaction\View\Json\UnAuditAppeal\View;
use Base\Package\Interaction\View\Json\UnAuditAppeal\ListView;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class UnAuditAppealFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditAppealRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditAppealRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();
      
        $unAuditAppealList = array();

        list($count, $unAuditAppealList) = $this->getRepository()
            ->scenario(UnAuditAppealRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($unAuditAppealList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $unAuditAppeal = $this->getRepository()
            ->scenario(UnAuditAppealRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);
            
        if ($unAuditAppeal instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($unAuditAppeal));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        $resource = 'appeals';
        return $this->checkUserHasInteractionPurview($resource);
    }
}
