<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;
use Base\Package\Common\Controller\Traits\ResubmitControllerTrait;
use Base\Package\Common\Controller\Interfaces\IResubmitAbleController;

use Sdk\Interaction\Command\UnAuditAppeal\ResubmitUnAuditAppealCommand;
use Sdk\Interaction\CommandHandler\UnAuditAppeal\UnAuditAppealCommandHandlerFactory;

use Base\Package\Interaction\Controller\InteractionValidateTrait;
use Base\Package\Interaction\Controller\RequestCommonTrait;

class UnAuditAppealOperationController extends Controller implements IResubmitAbleController
{
    use WebTrait, UtilsTrait, ResubmitControllerTrait, InteractionValidateTrait, RequestCommonTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditAppealCommandHandlerFactory());
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    protected function resubmitAction(int $id)
    {
        $requestData = $this->getReplyRequestCommonData();

        if ($this->validateCommonScenario(
            $requestData['content'],
            $requestData['images'],
            $requestData['admissibility']
        )) {
            $command = new ResubmitUnAuditAppealCommand(
                $requestData,
                $id
            );
            
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        
        $this->displayError();
        return false;
    }
}
