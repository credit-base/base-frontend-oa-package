<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;

use Sdk\Interaction\Command\Appeal\AcceptAppealCommand;
use Sdk\Interaction\CommandHandler\Appeal\AppealCommandHandlerFactory;

use Base\Package\Interaction\Controller\InteractionValidateTrait;
use Base\Package\Interaction\Controller\RequestCommonTrait;

class AppealOperationController extends Controller
{
    use WebTrait, UtilsTrait, InteractionValidateTrait, RequestCommonTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new AppealCommandHandlerFactory());
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    public function accept(string $id) : bool
    {
        $id = marmot_decode($id);

        $requestData = $this->getReplyRequestCommonData();
        
        if ($this->validateCommonScenario(
            $requestData['content'],
            $requestData['images'],
            $requestData['admissibility']
        )) {
            $command = new AcceptAppealCommand(
                $requestData,
                $id
            );
         
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
