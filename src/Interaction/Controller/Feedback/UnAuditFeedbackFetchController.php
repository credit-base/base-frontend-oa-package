<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\UnAuditFeedbackRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Interaction\View\Json\UnAuditFeedback\View;
use Base\Package\Interaction\View\Json\UnAuditFeedback\ListView;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class UnAuditFeedbackFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditFeedbackRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditFeedbackRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();
      
        $unAuditFeedbackList = array();

        list($count, $unAuditFeedbackList) = $this->getRepository()
            ->scenario(UnAuditFeedbackRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($unAuditFeedbackList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $unAuditFeedback = $this->getRepository()
            ->scenario(UnAuditFeedbackRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);
            
        if ($unAuditFeedback instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($unAuditFeedback));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        $resource = 'feedbacks';
        return $this->checkUserHasInteractionPurview($resource);
    }
}
