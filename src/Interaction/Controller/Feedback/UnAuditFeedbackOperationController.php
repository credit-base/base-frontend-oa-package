<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;
use Base\Package\Common\Controller\Traits\ResubmitControllerTrait;
use Base\Package\Common\Controller\Interfaces\IResubmitAbleController;

use Sdk\Interaction\Command\UnAuditFeedback\ResubmitUnAuditFeedbackCommand;
use Sdk\Interaction\CommandHandler\UnAuditFeedback\UnAuditFeedbackCommandHandlerFactory;

use Base\Package\Interaction\Controller\InteractionValidateTrait;
use Base\Package\Interaction\Controller\RequestCommonTrait;

class UnAuditFeedbackOperationController extends Controller implements IResubmitAbleController
{
    use WebTrait, UtilsTrait, ResubmitControllerTrait, InteractionValidateTrait, RequestCommonTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditFeedbackCommandHandlerFactory());
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    protected function resubmitAction(int $id)
    {
        $requestData = $this->getReplyRequestCommonData();

        if ($this->validateCommonScenario(
            $requestData['content'],
            $requestData['images'],
            $requestData['admissibility']
        )) {
            $command = new ResubmitUnAuditFeedbackCommand(
                $requestData,
                $id
            );
            
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        
        $this->displayError();
        return false;
    }
}
