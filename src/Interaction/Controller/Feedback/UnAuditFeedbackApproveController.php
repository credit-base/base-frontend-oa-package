<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ApproveControllerTrait;
use Base\Package\Common\Controller\Interfaces\IApproveAbleController;

use Sdk\Interaction\Command\UnAuditFeedback\ApproveUnAuditFeedbackCommand;
use Sdk\Interaction\Command\UnAuditFeedback\RejectUnAuditFeedbackCommand;
use Sdk\Interaction\CommandHandler\UnAuditFeedback\UnAuditFeedbackCommandHandlerFactory;

use Base\Package\Interaction\Controller\InteractionValidateTrait;

class UnAuditFeedbackApproveController extends Controller implements IApproveAbleController
{
    use WebTrait, ApproveControllerTrait, InteractionValidateTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditFeedbackCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function approveAction(int $id) : bool
    {
        $command = new ApproveUnAuditFeedbackCommand(
            $id
        );
        
        return $this->getCommandBus()->send($command);
    }

    protected function rejectAction(int $id) : bool
    {
        $request = $this->getRequest();
        
        $rejectReason = $request->post('rejectReason', '');

        if ($this->validateRejectScenario($rejectReason)) {
            $command = new RejectUnAuditFeedbackCommand(
                $rejectReason,
                $id
            );

            return $this->getCommandBus()->send($command);
        }

        return false;
    }
}
