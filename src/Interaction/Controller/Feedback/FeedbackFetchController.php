<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\FeedbackRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Interaction\Controller\RequestCommonTrait;

use Base\Package\Interaction\View\Json\Feedback\View;
use Base\Package\Interaction\View\Json\Feedback\ListView;

class FeedbackFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new FeedbackRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : FeedbackRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();

        list($count, $list) = $this->getRepository()
            ->scenario(FeedbackRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($list, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $data = $this->getRepository()
                        ->scenario(FeedbackRepository::FETCH_ONE_MODEL_UN)
                        ->fetchOne($id);
        
        if ($data instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($data));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        return $this->checkUserHasInteractionPurview($resource);
    }
}
