<?php
namespace Base\Package\Interaction\Controller\Qa;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\QaRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Interaction\Controller\RequestCommonTrait;

use Base\Package\Interaction\View\Json\Qa\View;
use Base\Package\Interaction\View\Json\Qa\ListView;

class QaFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new QaRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : QaRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();
        
        $list = array();

        list($count, $list) = $this->getRepository()
            ->scenario(QaRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($list, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $data = $this->getRepository()
                        ->scenario(QaRepository::FETCH_ONE_MODEL_UN)
                        ->fetchOne($id);
        
        if ($data instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($data));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        return $this->checkUserHasInteractionPurview($resource);
    }
}
