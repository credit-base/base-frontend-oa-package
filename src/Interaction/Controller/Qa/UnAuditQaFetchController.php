<?php
namespace Base\Package\Interaction\Controller\Qa;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\UnAuditQaRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Interaction\View\Json\UnAuditQa\View;
use Base\Package\Interaction\View\Json\UnAuditQa\ListView;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class UnAuditQaFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditQaRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditQaRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();
      
        $unAuditQaList = array();

        list($count, $unAuditQaList) = $this->getRepository()
            ->scenario(UnAuditQaRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($unAuditQaList, $count));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        $resource = 'qas';
        return $this->checkUserHasInteractionPurview($resource);
    }

    protected function fetchOneAction(int $id) : bool
    {
        $unAuditQa = $this->getRepository()
            ->scenario(UnAuditQaRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);
            
        if ($unAuditQa instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($unAuditQa));
        return true;
    }
}
