<?php
namespace Base\Package\Interaction\Controller\Qa;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;

use Sdk\Interaction\Command\Qa\AcceptQaCommand;
use Sdk\Interaction\Command\Qa\PublishQaCommand;
use Sdk\Interaction\Command\Qa\UnPublishQaCommand;
use Sdk\Interaction\CommandHandler\Qa\QaCommandHandlerFactory;

use Base\Package\Interaction\Controller\InteractionValidateTrait;
use Base\Package\Interaction\Controller\RequestCommonTrait;

class QaOperationController extends Controller
{
    use WebTrait, UtilsTrait, InteractionValidateTrait, RequestCommonTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new QaCommandHandlerFactory());
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    public function accept(string $id) : bool
    {
        $id = marmot_decode($id);

        $requestData = $this->getReplyRequestCommonData();
        
        if ($this->validateCommonScenario(
            $requestData['content'],
            $requestData['images'],
            $requestData['admissibility']
        )) {
            $command = new AcceptQaCommand(
                $requestData,
                $id
            );
         
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function publish(string $id):bool
    {
        $id = marmot_decode($id);

        $command = new PublishQaCommand($id);
            
        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }

    public function unPublish(string $id):bool
    {
        $id = marmot_decode($id);

        $command = new UnPublishQaCommand($id);
            
        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
}
