<?php
namespace Base\Package\Interaction\Controller;

use Marmot\Core;

use Sdk\Crew\Model\Crew;
use Base\Sdk\Purview\Model\IPurviewAble;
use Base\Sdk\Purview\Model\PurviewCategoryFactory;

use Base\Sdk\Common\Model\IApplyAble;

trait RequestCommonTrait
{
    protected function getReplyRequestCommonData() : array
    {
        $request = $this->getRequest();
        $requestData = array();

        $requestData['content'] =  $request->post('content', '');
        $requestData['images'] =  $request->post('images', array());
        $requestData['admissibility'] =  marmot_decode($request->post('admissibility', ''));
        
        return $requestData;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $title = $request->get('title', '');
        $applyStatus = $request->get('applyStatus', '');
        $acceptStatus = $request->get('acceptStatus', '');
        $status = $request->get('status', '');
        $acceptUserGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $filter = array();

        if (empty($acceptUserGroupId)) {
            $acceptUserGroupId = marmot_decode($request->get('acceptUserGroupId', ''));
        }
        if (!empty($title)) {
            $filter['title'] = $title;
        }
        if (!empty($acceptStatus)) {
            $filter['acceptStatus'] =  marmot_decode($acceptStatus);
        }
        if (!empty($status)) {
            $filter['status'] = marmot_decode($status);
        }

        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['PENDING'].','.
            IApplyAble::APPLY_STATUS['REJECT'];
        
        if (!empty($applyStatus)) {
            $filter['applyStatus'] = marmot_decode($applyStatus);
        }
        if (!empty($acceptUserGroupId)) {
            $filter['acceptUserGroup'] = $acceptUserGroupId;
        }
        if (!empty($acceptUserGroupId)) {
            $filter['applyUserGroup'] = $acceptUserGroupId;
        }
        
        return [$filter, [$sort]];
    }
    
    protected function checkUserHasInteractionPurview($resource) : bool
    {
        if (Core::$container->get('purview.status')) {
            $category = $this->getPurviewCategory($resource);
            $crewCategory = Core::$container->get('crew')->getCategory();

            $purview = $this->getInteractionPurview($resource);

            if ($crewCategory != Crew::CATEGORY['SUPERTUBE']) {
                if (!in_array($category, $purview)) {
                    Core::setLastError(PURVIEW_UNDEFINED);
                    return false;
                }
            }
        }

        return true;
    }

    protected function getPurviewCategory(string $resource) : int
    {
        return PurviewCategoryFactory::getCategory($resource);
    }

    protected function getInteractionPurview($resource):array
    {
        $category = $this->getPurviewCategory($resource);
       
        $purview = [];

        switch ($category) {
            case IPurviewAble::CATEGORY['PRAISE']:
                $purview = [
                    IPurviewAble::CATEGORY['PRAISE'],
                    IPurviewAble::CATEGORY['UN_AUDITED_PRAISE']
                ];
                break;
            case IPurviewAble::CATEGORY['COMPLAINT']:
                $purview = [
                    IPurviewAble::CATEGORY['COMPLAINT'],
                    IPurviewAble::CATEGORY['UN_AUDITED_COMPLAINT']
                ];
                break;
            case IPurviewAble::CATEGORY['QA']:
                $purview = [
                    IPurviewAble::CATEGORY['QA'],
                    IPurviewAble::CATEGORY['UN_AUDITED_QA']
                ];
                break;
            case IPurviewAble::CATEGORY['APPEAL']:
                $purview = [
                    IPurviewAble::CATEGORY['APPEAL'],
                    IPurviewAble::CATEGORY['UN_AUDITED_APPEAL']
                ];
                break;
            case IPurviewAble::CATEGORY['FEEDBACK']:
                $purview = [
                    IPurviewAble::CATEGORY['FEEDBACK'],
                    IPurviewAble::CATEGORY['UN_AUDITED_FEEDBACK']
                ];
                break;
        }

        return $purview;
    }
}
