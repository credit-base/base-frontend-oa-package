<?php
namespace Base\Package\Interaction\Controller;

use Sdk\Interaction\WidgetRules\InteractionWidgetRules;

use Sdk\Common\WidgetRules\WidgetRules;

trait InteractionValidateTrait
{
    protected function getInteractionWidgetRules() : InteractionWidgetRules
    {
        return new InteractionWidgetRules();
    }

    protected function getWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }

    protected function validateCommonScenario(
        $content,
        $images,
        $admissibility
    ) : bool {
        $interactionWidgetRules = $this->getInteractionWidgetRules();
      
        return $interactionWidgetRules->content($content, 'content')
            && $interactionWidgetRules->images($images, 'images')
            && $interactionWidgetRules->admissibility($admissibility, 'admissibility');
    }

    protected function validateRejectScenario($rejectReason)
    {
        $commonWidgetRules = $this->getWidgetRules();

        return  $commonWidgetRules->reason($rejectReason, 'rejectReason');
    }
}
