<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\UnAuditComplaintRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Interaction\View\Json\UnAuditComplaint\View;
use Base\Package\Interaction\View\Json\UnAuditComplaint\ListView;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class UnAuditComplaintFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditComplaintRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditComplaintRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();
      
        $unAuditComplaintList = array();

        list($count, $unAuditComplaintList) = $this->getRepository()
            ->scenario(UnAuditComplaintRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($unAuditComplaintList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $unAuditComplaint = $this->getRepository()
            ->scenario(UnAuditComplaintRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);
            
        if ($unAuditComplaint instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($unAuditComplaint));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        $resource = 'complaints';
        return $this->checkUserHasInteractionPurview($resource);
    }
}
