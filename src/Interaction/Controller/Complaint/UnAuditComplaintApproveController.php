<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ApproveControllerTrait;
use Base\Package\Common\Controller\Interfaces\IApproveAbleController;

use Sdk\Interaction\Command\UnAuditComplaint\ApproveUnAuditComplaintCommand;
use Sdk\Interaction\Command\UnAuditComplaint\RejectUnAuditComplaintCommand;
use Sdk\Interaction\CommandHandler\UnAuditComplaint\UnAuditComplaintCommandHandlerFactory;

use Base\Package\Interaction\Controller\InteractionValidateTrait;

class UnAuditComplaintApproveController extends Controller implements IApproveAbleController
{
    use WebTrait, ApproveControllerTrait, InteractionValidateTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditComplaintCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function approveAction(int $id) : bool
    {
        $command = new ApproveUnAuditComplaintCommand(
            $id
        );
        
        return $this->getCommandBus()->send($command);
    }

    protected function rejectAction(int $id) : bool
    {
        $request = $this->getRequest();
        
        $rejectReason = $request->post('rejectReason', '');

        if ($this->validateRejectScenario($rejectReason)) {
            $command = new RejectUnAuditComplaintCommand(
                $rejectReason,
                $id
            );

            return $this->getCommandBus()->send($command);
        }

        return false;
    }
}
