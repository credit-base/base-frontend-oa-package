<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\ResourceCatalog\Translator\GbSearchDataTranslator;

use Base\Package\ResourceCatalog\View\SearchDataFormatTrait;

class GbSearchDataView extends JsonView implements IView
{
    use SearchDataFormatTrait;

    private $gbSearchData;

    private $translator;

    public function __construct($gbSearchData)
    {
        $this->gbSearchData = $gbSearchData;
        $this->translator = new GbSearchDataTranslator();
        parent::__construct();
    }

    protected function getGbSearchData()
    {
        return $this->gbSearchData;
    }

    protected function getTranslator(): GbSearchDataTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $gbData = array();

        $gbData = $this->getTranslator()->objectToArray(
            $this->getGbSearchData()
        );

        if (!empty($gbData['itemsData']) && !empty($gbData['template'])) {
            $gbData['itemsData'] = $this->itemsRelationTemplate($gbData['itemsData'], $gbData['template']);
            unset($gbData['template']);
        }

        $this->encode($gbData);
    }
}
