<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\ResourceCatalog\View\BjSearchDataListTrait;

class BjSearchDataListView extends JsonView implements IView
{
    use BjSearchDataListTrait;

    public function display(): void
    {
        $data = array();

        foreach ($this->getBjSearchData() as $bjSearchData) {
            $data[] = $this->getTranslator()->objectToArray(
                $bjSearchData,
                array(
                    'id',
                    'name',
                    'identify',
                    'status',
                    'updateTime',
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
