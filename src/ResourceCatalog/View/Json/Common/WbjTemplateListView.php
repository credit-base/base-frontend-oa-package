<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Template\View\Json\WbjTemplateListTrait;

class WbjTemplateListView extends JsonView implements IView
{
    use WbjTemplateListTrait;

    public function display() : void
    {
        $translator = $this->getTranslator();

        $data = array();
        foreach ($this->getList() as $item) {
            $data[] = $translator->objectToArray($item);
        }

        $result = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($result);
    }
}
