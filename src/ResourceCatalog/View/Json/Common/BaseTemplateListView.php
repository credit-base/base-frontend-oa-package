<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Template\View\Json\BaseTemplateListTrait;

class BaseTemplateListView extends JsonView implements IView
{
    use BaseTemplateListTrait;

    public function display() : void
    {
        $translator = $this->getTranslator();

        $data = array();
        foreach ($this->getList() as $item) {
            $data[] = $translator->objectToArray($item);
        }

        $result = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($result);
    }
}
