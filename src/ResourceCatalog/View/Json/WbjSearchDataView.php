<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\ResourceCatalog\Translator\WbjSearchDataTranslator;

use Base\Package\ResourceCatalog\View\SearchDataFormatTrait;

class WbjSearchDataView extends JsonView implements IView
{
    use SearchDataFormatTrait;
    
    private $wbjSearchData;

    private $translator;

    public function __construct($wbjSearchData)
    {
        $this->wbjSearchData = $wbjSearchData;
        $this->translator = new WbjSearchDataTranslator();
        parent::__construct();
    }

    protected function getWbjSearchData()
    {
        return $this->wbjSearchData;
    }

    protected function getTranslator(): WbjSearchDataTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $wbjData = array();

        $wbjData = $this->getTranslator()->objectToArray(
            $this->getWbjSearchData()
        );

        if (!empty($wbjData['itemsData']) && !empty($wbjData['template'])) {
            $wbjData['itemsData'] = $this->itemsRelationTemplate($wbjData['itemsData'], $wbjData['template']);
            unset($wbjData['template']);
        }

        $this->encode($wbjData);
    }
}
