<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\ResourceCatalog\View\ErrorDataFormatTrait;

class ErrorDataView extends JsonView implements IView
{
    use ErrorDataViewTrait;

    public function display(): void
    {
        $errorData = $this->getData();
        
        $errorData = $this->getErrorDataDetail($errorData);
        
        $this->encode($errorData);
    }
}
