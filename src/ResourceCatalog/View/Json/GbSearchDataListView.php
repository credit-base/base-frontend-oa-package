<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\ResourceCatalog\Translator\GbSearchDataTranslator;

class GbSearchDataListView extends JsonView implements IView
{
    private $gbSearchDatas;

    private $count;

    private $translator;

    public function __construct(
        array $gbSearchDatas,
        int $count
    ) {
        $this->gbSearchDatas = $gbSearchDatas;
        $this->count = $count;
        $this->translator = new GbSearchDataTranslator();
        parent::__construct();
    }

    protected function getGbSearchDatas(): array
    {
        return $this->gbSearchDatas;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): GbSearchDataTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getGbSearchDatas() as $gbSearchData) {
            $data[] = $this->getTranslator()->objectToArray(
                $gbSearchData,
                array(
                    'id',
                    'name',
                    'identify',
                    'status',
                    'updateTime',
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
