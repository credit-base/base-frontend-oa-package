<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

use Base\Package\ResourceCatalog\View\SearchDataFormatTrait;

class BjSearchDataView extends JsonView implements IView
{
    use SearchDataFormatTrait;

    private $bjSearchData;

    private $translator;

    public function __construct($bjSearchData)
    {
        $this->bjSearchData = $bjSearchData;
        $this->translator = new BjSearchDataTranslator();
        parent::__construct();
    }

    protected function getBjSearchData()
    {
        return $this->bjSearchData;
    }

    protected function getTranslator(): BjSearchDataTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $bjData = array();

        $bjData = $this->getTranslator()->objectToArray(
            $this->getBjSearchData()
        );
        
        if (!empty($bjData['itemsData']) && !empty($bjData['template'])) {
            $bjData['itemsData'] = $this->itemsRelationTemplate($bjData['itemsData'], $bjData['template']);
            unset($bjData['template']);
        }
        
        $this->encode($bjData);
    }
}
