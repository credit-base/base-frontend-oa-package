<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\ResourceCatalog\Translator\ErrorDataTranslator;

class ErrorDataListView extends JsonView implements IView
{
    use ErrorDataViewTrait;

    private $errorData;

    private $count;

    private $translator;

    public function __construct(
        array $errorData,
        int $count
    ) {
        $this->errorData = $errorData;
        $this->count = $count;
        $this->translator = new ErrorDataTranslator();
        parent::__construct();
    }

    protected function getErrorDataList(): array
    {
        return $this->errorData;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): ErrorDataTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getErrorDataList() as $errorData) {
            $data[] = $this->getTranslator()->objectToArray(
                $errorData,
                array(
                    'id',
                    'category',
                    'template',
                    'itemsData',
                    'errorReason',
                    'taskData',
                    'status',
                    'createTime',
                    'updateTime',
                    'statusTime'
                )
            );
        }
       
        $data = $this->getResultErrorDataList($data);

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        if (!empty($data)) {
            $dataList['taskData'] = $data[0]['taskData'];
        }

        $this->encode($dataList);
    }

    protected function getResultErrorDataList(array $list):array
    {
        foreach ($list as $key => $val) {
            $val = $this->getFormatErrorData($val);

            if (!empty($val['itemsData'])) {
                $val['errorReason'] = $this->getErrorDataListToReason($val['itemsData']);
            }
            
            unset($val['itemsData']);
            unset($val['template']);
            unset($val['items']);
            
            $list[$key] = $val;
        }
        return $list;
    }

    protected function getErrorDataListToReason(array $items) : string
    {
        foreach ($items as $item) {
            if (!empty($item['errorReason'])) {
                $errorReason[] = $item['errorReason'];
            }
        }

        return implode(';', $errorReason);
    }
}
