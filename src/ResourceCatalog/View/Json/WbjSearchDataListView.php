<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\ResourceCatalog\Translator\WbjSearchDataTranslator;

class WbjSearchDataListView extends JsonView implements IView
{
    private $wbjSearchDatas;

    private $count;

    private $translator;

    public function __construct(
        array $wbjSearchDatas,
        int $count
    ) {
        $this->wbjSearchDatas = $wbjSearchDatas;
        $this->count = $count;
        $this->translator = new WbjSearchDataTranslator();
        parent::__construct();
    }

    protected function getWbjSearchDatas(): array
    {
        return $this->wbjSearchDatas;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): WbjSearchDataTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getWbjSearchDatas() as $wbjSearchData) {
            $data[] = $this->getTranslator()->objectToArray(
                $wbjSearchData,
                array(
                    'id',
                    'name',
                    'identify',
                    'status',
                    'updateTime',
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
