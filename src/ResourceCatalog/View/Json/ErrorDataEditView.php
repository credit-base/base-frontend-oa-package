<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\ResourceCatalog\View\ErrorDataFormatTrait;

class ErrorDataEditView extends JsonView implements IView
{
    use ErrorDataViewTrait;

    public function display(): void
    {
        $errorData = $this->getData();
        
        $this->encode($errorData);
    }
}
