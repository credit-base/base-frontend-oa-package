<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Sdk\ResourceCatalog\Translator\ErrorDataTranslator;
use Base\Package\ResourceCatalog\View\ErrorDataFormatTrait;

trait ErrorDataViewTrait
{
    use ErrorDataFormatTrait;

    private $errorData;

    private $translator;

    public function __construct($errorData)
    {
        $this->errorData = $errorData;
        $this->translator = new ErrorDataTranslator();
        parent::__construct();
    }

    protected function getErrorData()
    {
        return $this->errorData;
    }

    protected function getTranslator(): ErrorDataTranslator
    {
        return $this->translator;
    }

    protected function getData():array
    {
        $errorData = array();

        $errorData = $this->getTranslator()->objectToArray(
            $this->getErrorData(),
            array(
                'id',
                'category',
                'template',
                'itemsData',
                'errorReason',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        );

        if (!empty($errorData['itemsData']) && isset($errorData['template']['items'])) {
            $errorData = $this->getFormatErrorData($errorData);
        }
      
        return $errorData;
    }
}
