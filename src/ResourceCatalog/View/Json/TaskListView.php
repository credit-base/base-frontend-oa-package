<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\ResourceCatalog\Translator\TaskTranslator;

class TaskListView extends JsonView implements IView
{
    private $list;

    private $count;

    private $translator;

    public function __construct(
        array $list,
        int $count
    ) {
        $this->list = $list;
        $this->count = $count;
        $this->translator = new TaskTranslator();
        parent::__construct();
    }

    protected function getList(): array
    {
        return $this->list;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): TaskTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getList() as $task) {
            $data[] = $this->getTranslator()->objectToArray(
                $task,
                array(
                    'id',
                    'total',
                    'successNumber',
                    'failureNumber',
                    'fileName',
                    'crew',
                    'status',
                    'updateTime',
                    'createTime'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
