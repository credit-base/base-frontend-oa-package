<?php
namespace Base\Package\ResourceCatalog\View;

use Sdk\ResourceCatalog\Translator\ErrorDataTranslator;
use Sdk\ResourceCatalog\Model\ErrorData;

trait ErrorDataFormatTrait
{
    /**
     * 获取当前错误数据关联的资源目录数据的主体名称和主体标识
     * 主体标识为统一社会信用代码\证件号码
     */
    protected function getErrorDataFormatName(array $errorData):array
    {
        $errorData['name'] = $errorData['itemsData']['ZTMC'];
      
        $errorDataIdentify = $errorData['itemsData']['TYSHXYDM'] ?? $errorData['itemsData']['ZJHM'];

        $errorData['identify'] = $errorDataIdentify;

        return $errorData;
    }
    /**
     * 获取当前错误数据关联的资源目录数据中，
     * 每一条资源目录信息对应的详细数据和错误原因
     * 错误原因的具体格式为errorReason = array(array(资源目录信息标识=>错误代码),array("ZTMC"=> [1,2,8]))
     */
    protected function getFormatErrorData(array $errorData):array
    {
        $errorData = $this->getErrorDataFormatName($errorData);

        $oldItemsData = $errorData['itemsData'];
        $newItemsData = [];
        foreach ($errorData['template']['items'] as $key => $val) {
            $val['value'] = $this->getNewItemsByIdentify(
                $val['identify'],
                $oldItemsData
            );
           
            if (!empty($errorData['errorReason'])) {
                $errorReason = $this->getErrorReasonByIdentify(
                    $val['identify'],
                    $val['name'],
                    $errorData['errorReason']
                );
                
                $val['errorReason'] = $errorReason;
            }

            $newItemsData[$key] = $val;
        }
        $errorData['itemsData'] = $newItemsData;

        unset($errorData['template']);
        unset($errorData['errorReason']);
        
        return $errorData;
    }

    /**
     * 通过错误原因中的具体标识在资源目录信息中匹配其相对应的名称
     * 资源目录信息具体格式为itemsData=[ "ZTMC"=> "四川瑞煜建筑有限公司信阳分公司",]
     * @return string itemData[0]['value']
     */
    protected function getNewItemsByIdentify(
        string $identify,
        array $itemsData
    ) : string {

        $value = '';
        foreach ($itemsData as $key => $item) {
            if ($identify == $key) {
                $value = $item;
            }
        }
       
        return $value;
    }
    /**
     * 通过资源目录信息中的具体标识匹配对错误原因的标识，
     * 并通过错误原因中的错误代码匹配其对应的中文解释，
     * 然后与主体名称拼接成具体的错误原因
     * @return string itemData[0]['errorReason']
     */
    protected function getErrorReasonByIdentify(
        string $identify,
        string $itemName,
        array $errorReasonData
    ):string {
        
        $errorReason = '';
        foreach ($errorReasonData as $key => $item) {
            if ($identify == $key) {
                $errorReason = $this->getErrorReasonCn($item);
            }
        }
        
        if (!empty($errorReason)) {
            $errorReason = $itemName.$errorReason;
        }
       
        return $errorReason;
    }
    /**
     * 错误代码的中文映射
     * @return string
     */
    protected function getErrorReasonCn(array $errorReasonData) : string
    {
        foreach ($errorReasonData as $key => $val) {
            $errorReason = ErrorDataTranslator::ERROR_TYPE_CN[$val];
            if (count($errorReasonData) > 2
                && $val == ErrorData::ERROR_TYPE['FORMAT_VALIDATION_FAILED']
            ) {
                $errorReason = "且".$errorReason;
            }

            $errorReasonData[$key] = $errorReason;
        }
        
        return implode(',', $errorReasonData);
    }
    /**
     * 用于错误数据的详情接口
     * itemsData数组只取name、value、errorReason
     */
    protected function getErrorDataDetail(array $errorData):array
    {
        $errorData['itemsData'] = $this->getFormatItemsData(
            $errorData['itemsData']
        );
       
        return $errorData;
    }

    protected function getFormatItemsData(array $itemsData):array
    {
        $newItemsData = [];
        foreach ($itemsData as $identify => $val) {
            $newItemsData[$identify] = [
                'name'=> $val['name'],
                'value'=>$val['value'],
                'errorReason'=> $val['errorReason'],
            ];
        }

        return $newItemsData;
    }
}
