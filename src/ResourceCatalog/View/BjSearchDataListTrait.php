<?php
namespace Base\Package\ResourceCatalog\View;

use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

trait BjSearchDataListTrait
{
    private $bjSearchData;

    private $count;

    private $translator;

    public function __construct(
        array $bjSearchData,
        int $count
    ) {
        $this->bjSearchData = $bjSearchData;
        $this->count = $count;
        $this->translator = new BjSearchDataTranslator();
        parent::__construct();
    }

    protected function getBjSearchData(): array
    {
        return $this->bjSearchData;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): BjSearchDataTranslator
    {
        return $this->translator;
    }
}
