<?php
namespace Base\Package\ResourceCatalog\View;

trait SearchDataFormatTrait
{
    protected function itemsRelationTemplate(array $itemsData, array $template):array
    {
        $resultList = [];
        $templateItems = $this->getTemplateIdentify($template['items']);
        foreach ($itemsData['data'] as $key => $value) {
            foreach ($templateItems as $identify => $val) {
                if ($key == $identify) {
                    $resultList[$key] = [
                        'name'=>$val['name'],
                        'value'=>$value
                    ];
                }
            }
        }
       
        return array_values($resultList);
    }

    protected function getTemplateIdentify(array $items) : array
    {
        $list = [];
       
        foreach ($items as $val) {
            $list[$val['identify']]= $val;
        }
    
        return $list;
    }
}
