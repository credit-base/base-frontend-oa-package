<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Sdk\ResourceCatalog\Command\BjSearchData\ConfirmBjSearchDataCommand;
use Sdk\ResourceCatalog\CommandHandler\BjSearchData\BjSearchDataCommandHandlerFactory;

class BjSearchDataStatusController extends Controller
{
    use WebTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new BjSearchDataCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    public function confirm(string $id) : bool
    {
        $id = marmot_decode($id);
        $command = new ConfirmBjSearchDataCommand($id);
       
        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }
        
        $this->displayError();
        return false;
    }
}
