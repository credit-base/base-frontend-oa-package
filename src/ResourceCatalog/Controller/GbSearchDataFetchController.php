<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\ResourceCatalog\Repository\GbSearchDataRepository;
use Sdk\ResourceCatalog\Model\GbSearchData;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\ResourceCatalog\View\Json\GbSearchDataView;
use Base\Package\ResourceCatalog\View\Json\GbSearchDataListView;

class GbSearchDataFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new GbSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : GbSearchDataRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $gbSearchDataList = array();

        list($count, $gbSearchDataList) = $this->getRepository()->scenario(
            GbSearchDataRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        $this->render(new GbSearchDataListView($gbSearchDataList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $name = $request->get('name', '');
        $identify = $request->get('identify', '');
        $status = $request->get('status', '');
        $templateId = $request->get('templateId', '');
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $filter = array();

        if (empty($userGroupId)) {
            $userGroupId = marmot_decode($request->get('userGroup', ''));
        }
        if (!empty($userGroupId)) {
            $filter['sourceUnit'] = $userGroupId;
        }

        if (!empty($name)) {
            $filter['name'] = $name;
        }
        if (!empty($identify)) {
            $filter['identify'] = $identify;
        }
        if (!empty($templateId)) {
            $filter['template'] = marmot_decode($templateId);
        }

        $filter['status'] = GbSearchData::DATA_STATUS['ENABLED'].','.GbSearchData::DATA_STATUS['CONFIRM'];
        if (!empty($status)) {
            $filter['status'] = marmot_decode($status);
        }
        
        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $gbSearchData = $this->getRepository()->scenario(GbSearchDataRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
      
        if ($gbSearchData instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new GbSearchDataView($gbSearchData));
        return true;
    }
}
