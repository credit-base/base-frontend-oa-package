<?php
namespace Base\Package\ResourceCatalog\Controller\Task;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\ResourceCatalog\Model\Task\Task;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

class BjTaskFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, TaskFetchControllerTrait;

    protected function filterAction() : bool
    {
        $targetCategory = Task::CATEGORY['BJ'];
        return $this->filterList($targetCategory);
    }

    protected function fetchOneAction(int $id) : bool
    {
        unset($id);
        return false;
    }
}
