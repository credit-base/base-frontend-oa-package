<?php
namespace Base\Package\ResourceCatalog\Controller\Task;

use Marmot\Core;

use Sdk\ResourceCatalog\Repository\TaskRepository;
use Sdk\ResourceCatalog\Model\Task\Task;

use Base\Package\ResourceCatalog\View\Json\TaskListView;

trait TaskFetchControllerTrait
{
    protected function getRepository() : TaskRepository
    {
        return new TaskRepository();
    }

    protected function filterList(int $targetCategory) : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($targetCategory);

        $taskList = array();

        list($count, $taskList) = $this->getRepository()->scenario(
            TaskRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);
      
        $this->render(new TaskListView($taskList, $count));
        return true;
    }
  
    protected function filterFormatChange($targetCategory)
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-createTime');
        $status = $request->get('status', '');
        $templateId = $request->get('templateId', '');
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $filter = array();

        $filter['targetCategory'] = $targetCategory;

        if (empty($userGroupId)) {
            $userGroupId = marmot_decode($request->get('userGroup', ''));
        }
        if (!empty($userGroupId)) {
            $filter['userGroup'] = $userGroupId;
        }
        if (!empty($status)) {
            $filter['status'] = $this->getStatus($status);
        }
        if (!empty($templateId)) {
            $filter['targetTemplate'] = marmot_decode($templateId);
        }
        
        return [$filter, [$sort]];
    }

    protected function getStatus($status)
    {
        $status = marmot_decode($status);
        if ($status == Task::STATUS['FAILURE']) {
            $status = Task::STATUS['FAILURE'].','.Task::STATUS['FAILURE_FILE_DOWNLOAD'];
        }

        return $status;
    }
}
