<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Sdk\ResourceCatalog\Command\GbSearchData\ConfirmGbSearchDataCommand;
use Sdk\ResourceCatalog\CommandHandler\GbSearchData\GbSearchDataCommandHandlerFactory;

class GbSearchDataStatusController extends Controller
{
    use WebTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new GbSearchDataCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    
    public function confirm(string $id) : bool
    {
        $id = marmot_decode($id);
        $command = new ConfirmGbSearchDataCommand($id);
        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }
        
        $this->displayError();
        return false;
    }
}
