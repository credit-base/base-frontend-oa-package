<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Template\Repository\BaseTemplateRepository;

use Base\Package\ResourceCatalog\View\Json\Common\BaseTemplateListView;

class BaseTemplateFetchController extends Controller
{
    use WebTrait, FetchControllerTrait;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new BaseTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : BaseTemplateRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        $baseTemplateList = array();
        list($count, $baseTemplateList) = $this->getRepository()
            ->scenario(BaseTemplateRepository::LIST_MODEL_UN)
            ->search(array(), ['-updateTime'], PAGE, COMMON_SIZES);

        $this->render(new BaseTemplateListView($baseTemplateList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        unset($id);
        return false;
    }
}
