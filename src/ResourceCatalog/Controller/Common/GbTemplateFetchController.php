<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Template\Repository\GbTemplateRepository;

use Base\Package\ResourceCatalog\View\Json\Common\GbTemplateListView;

class GbTemplateFetchController extends Controller
{
    use WebTrait, FetchControllerTrait;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new GbTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : GbTemplateRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        $gbTemplateList = array();
        list($count, $gbTemplateList) = $this->getRepository()
            ->scenario(GbTemplateRepository::LIST_MODEL_UN)
            ->search(array(), ['-updateTime'], PAGE, COMMON_SIZES);

        $this->render(new GbTemplateListView($gbTemplateList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        unset($id);
        return false;
    }
}
