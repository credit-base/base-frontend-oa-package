<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Template\Repository\WbjTemplateRepository;

use Base\Package\ResourceCatalog\View\Json\Common\WbjTemplateListView;

class WbjTemplateFetchController extends Controller
{
    use WebTrait, FetchControllerTrait;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WbjTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : WbjTemplateRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        $wbjTemplateList = array();
        list($count, $wbjTemplateList) = $this->getRepository()
            ->scenario(WbjTemplateRepository::LIST_MODEL_UN)
            ->search(array(), ['-updateTime'], PAGE, COMMON_SIZES);

        $this->render(new WbjTemplateListView($wbjTemplateList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        unset($id);
        return false;
    }
}
