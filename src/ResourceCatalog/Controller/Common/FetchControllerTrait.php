<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

trait FetchControllerTrait
{
    public function getPageAndSize($size = 10)
    {
        $size = $this->getRequest()->get('limit', $size);
        $page = $this->getRequest()->get('page', 1);
     
        return [$size, $page];
    }

    public function filter() : bool
    {
        if ($this->filterAction()) {
            return true;
        }

        $this->displayError();
        return false;
    }

    abstract protected function filterAction() : bool;

    public function fetchOne(string $id) : bool
    {
        $id = marmot_decode($id);
    
        if ($this->fetchOneAction($id)) {
            return true;
        }
   
        $this->displayError();
        return false;
    }

    abstract protected function fetchOneAction(int $id) : bool;
}
