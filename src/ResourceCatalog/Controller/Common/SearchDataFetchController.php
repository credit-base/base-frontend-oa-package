<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;
use Sdk\ResourceCatalog\Model\BjSearchData;

use Base\Package\ResourceCatalog\View\Json\Common\SearchDataListView;
use Base\Package\ResourceCatalog\View\Json\BjSearchDataView;

class SearchDataFetchController extends Controller
{
    use WebTrait, FetchControllerTrait;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new BjSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : BjSearchDataRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $searchDataList = array();

        list($count, $searchDataList) = $this->getRepository()->scenario(
            BjSearchDataRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);
    
        $this->render(new SearchDataListView($searchDataList, $count));
        return true;
    }
    
    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $infoClassify = $request->get('infoClassify', '');
        $identify = $request->get('identify', '');
        $sort = $request->get('sort', '-updateTime');
        
        $filter = array();

        $filter['status'] = BjSearchData::DATA_STATUS['CONFIRM'];

        if (!empty($infoClassify)) {
            $filter['infoClassify'] = marmot_decode($infoClassify);
        }
        if (!empty($identify)) {
            $filter['identify'] = $identify;
        }

        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $bjSearchData = $this->getRepository()->scenario(BjSearchDataRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
       
        if ($bjSearchData instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new BjSearchDataView($bjSearchData));
        return true;
    }
}
