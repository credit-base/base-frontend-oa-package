<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Template\Repository\BjTemplateRepository;

use Base\Package\Template\View\Json\BjTemplateDetailView;
use Base\Package\ResourceCatalog\View\Json\Common\BjTemplateListView;

class BjTemplateFetchController extends Controller
{
    use WebTrait, FetchControllerTrait;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new BjTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : BjTemplateRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        $bjTemplateList = array();
        list($count, $bjTemplateList) = $this->getRepository()
            ->scenario(BjTemplateRepository::LIST_MODEL_UN)
            ->search(array(), ['-updateTime'], PAGE, COMMON_SIZES);

        $this->render(new BjTemplateListView($bjTemplateList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $template = $this->getRepository()
            ->scenario(BjTemplateRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($template instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new BjTemplateDetailView($template));
        return true;
    }
}
