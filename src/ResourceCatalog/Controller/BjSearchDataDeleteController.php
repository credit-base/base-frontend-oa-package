<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Traits\DeleteControllerTrait;
use Base\Package\Common\Controller\Interfaces\IDeleteAbleController;

use Sdk\ResourceCatalog\Command\BjSearchData\DeleteBjSearchDataCommand;
use Sdk\ResourceCatalog\CommandHandler\BjSearchData\BjSearchDataCommandHandlerFactory;

class BjSearchDataDeleteController extends Controller implements IDeleteAbleController
{
    use WebTrait, DeleteControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new BjSearchDataCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function deleteAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new DeleteBjSearchDataCommand($id));
    }
}
