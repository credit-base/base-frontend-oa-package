<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\ResourceCatalog\Repository\ErrorDataRepository;

use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Base\Package\ResourceCatalog\View\Json\ErrorDataEditView;

class ErrorDataOperationController extends Controller implements IOperateAbleController
{
    use WebTrait, OperateControllerTrait, ErrorDataControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        unset($id);
        
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function getRepository() : ErrorDataRepository
    {
        return new ErrorDataRepository();
    }

    protected function editView(int $id) : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('errorData')) {
                return false;
            }
        }

        $errorData = $this->getRepository()
                          ->scenario(ErrorDataRepository::FETCH_ONE_MODEL_UN)
                          ->fetchOne($id);

        
        if ($errorData instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
       
        $this->render(new ErrorDataEditView($errorData));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        return $this->checkUserHasErrorDataPurview($resource);
    }
}
