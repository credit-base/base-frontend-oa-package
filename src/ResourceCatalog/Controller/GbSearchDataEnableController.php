<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Traits\EnableControllerTrait;
use Base\Package\Common\Controller\Interfaces\IEnableAbleController;

use Sdk\ResourceCatalog\Command\GbSearchData\DisableGbSearchDataCommand;
use Sdk\ResourceCatalog\CommandHandler\GbSearchData\GbSearchDataCommandHandlerFactory;

class GbSearchDataEnableController extends Controller implements IEnableAbleController
{
    use WebTrait, EnableControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new GbSearchDataCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function enableAction(int $id) : bool
    {
        unset($id);
        return false;
    }

    protected function disableAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new DisableGbSearchDataCommand($id));
    }
}
