<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;
use Sdk\ResourceCatalog\Model\BjSearchData;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\ResourceCatalog\View\Json\BjSearchDataView;
use Base\Package\ResourceCatalog\View\Json\BjSearchDataListView;

class BjSearchDataFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new BjSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : BjSearchDataRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $bjSearchDataList = array();

        list($count, $bjSearchDataList) = $this->getRepository()->scenario(
            BjSearchDataRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);
    
        $this->render(new BjSearchDataListView($bjSearchDataList, $count));
        return true;
    }
    
    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $name = $request->get('name', '');
        $status = $request->get('status', '');
        $identify = $request->get('identify', '');
        $templateId = $request->get('templateId', '');
        $sort = $request->get('sort', '-updateTime');
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $filter = array();

        $filter['status'] = BjSearchData::DATA_STATUS['ENABLED'].','.BjSearchData::DATA_STATUS['CONFIRM'];
        if (!empty($status)) {
            $filter['status'] = marmot_decode($status);
        }

        if (empty($userGroupId)) {
            $userGroupId = marmot_decode($request->get('userGroup', ''));
        }
        if (!empty($userGroupId)) {
            $filter['sourceUnit'] = $userGroupId;
        }

        if (!empty($name)) {
            $filter['name'] = $name;
        }
        if (!empty($templateId)) {
            $filter['template'] = marmot_decode($templateId);
        }
        if (!empty($identify)) {
            $filter['identify'] = $identify;
        }

        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $bjSearchData = $this->getRepository()->scenario(BjSearchDataRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
       
        if ($bjSearchData instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new BjSearchDataView($bjSearchData));
        return true;
    }
}
