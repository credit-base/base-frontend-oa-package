<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\ResourceCatalog\Repository\ErrorDataRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\ResourceCatalog\View\Json\ErrorDataView;
use Base\Package\ResourceCatalog\View\Json\ErrorDataListView;

class ErrorDataFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, ErrorDataControllerTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ErrorDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ErrorDataRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('errorData')) {
                return false;
            }
        }
        
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $errorDataList = array();

        list($count, $errorDataList) = $this->getRepository()->scenario(
            ErrorDataRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);
            
        $this->render(new ErrorDataListView($errorDataList, $count));
        return true;
    }
    
    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $taskId = $request->get('taskId', '');
        $templateId = $request->get('templateId', '');
        $sort = $request->get('sort', '-updateTime');
        
        $filter = array();

        if (!empty($taskId)) {
            $filter['task'] = marmot_decode($taskId);
        }
        if (!empty($templateId)) {
            $filter['template'] = marmot_decode($templateId);
        }

        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('errorData')) {
                return false;
            }
        }

        $errorData = $this->getRepository()
                          ->scenario(ErrorDataRepository::FETCH_ONE_MODEL_UN)
                          ->fetchOne($id);
      
        if ($errorData instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
       
        $this->render(new ErrorDataView($errorData));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        return $this->checkUserHasErrorDataPurview($resource);
    }
}
