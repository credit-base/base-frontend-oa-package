<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\ResourceCatalog\Repository\WbjSearchDataRepository;
use Sdk\ResourceCatalog\Model\WbjSearchData;
use Sdk\ResourceCatalog\Model\Attachment;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\ResourceCatalog\View\Json\WbjSearchDataView;
use Base\Package\ResourceCatalog\View\Json\WbjSearchDataListView;

class WbjSearchDataFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WbjSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : WbjSearchDataRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $wbjSearchDataList = array();

        list($count, $wbjSearchDataList) = $this->getRepository()->scenario(
            WbjSearchDataRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);
    
        $this->render(new WbjSearchDataListView($wbjSearchDataList, $count));
        return true;
    }
    
    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $name = $request->get('name', '');
        $identify = $request->get('identify', '');
        $templateId = $request->get('templateId', '');
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $filter = array();

        if (empty($userGroupId)) {
            $userGroupId = marmot_decode($request->get('userGroup', ''));
        }
        if (!empty($userGroupId)) {
            $filter['sourceUnit'] = $userGroupId;
        }

        if (!empty($name)) {
            $filter['name'] = $name;
        }
        if (!empty($identify)) {
            $filter['identify'] = $identify;
        }
        if (!empty($templateId)) {
            $filter['template'] = marmot_decode($templateId);
        }

        $filter['status'] = WbjSearchData::DATA_STATUS['CONFIRM'];

        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $wbjSearchData = $this->getRepository()->scenario(WbjSearchDataRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        if ($wbjSearchData instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new WbjSearchDataView($wbjSearchData));
        return true;
    }

    protected function getAttachment():Attachment
    {
        return new Attachment();
    }

    public function upload(string $templateId) : bool
    {
        $templateId = marmot_decode($templateId);
        
        $file = $this->getFile();
      
        if (!empty($file) && $this->isExitCrewId()) {
            $attachment = $this->getAttachment();
            $result = $attachment->upload($templateId, 'file');
            
            if ($result) {
                $this->displaySuccess();
                return true;
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getFile()
    {
        return $_FILES;
    }

    protected function isExitCrewId() : bool
    {
        $crewId = Core::$container->get('crew')->getId();
        if (empty($crewId)) {
            Core::setLastError(CREW_IS_EMPTY);
            return false;
        }
        return true;
    }
}
