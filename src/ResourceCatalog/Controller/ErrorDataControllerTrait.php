<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;

use Base\Sdk\Purview\Model\IPurviewAble;

use Sdk\Crew\Model\Crew;

trait ErrorDataControllerTrait
{
    protected function checkUserHasErrorDataPurview($resource) : bool
    {
        $category = $this->getPurviewCategoryFactory()->getCategory($resource);
        $crewCategory = Core::$container->get('crew')->getCategory();
        
        $errorDataPurview = [
            IPurviewAble::CATEGORY['GB_DATA_MANAGE'],
            IPurviewAble::CATEGORY['BJ_DATA_MANAGEMENT'],
            IPurviewAble::CATEGORY['WBJ_DATA_MANAGE']
        ];
        
        if ($crewCategory != Crew::CATEGORY['SUPERTUBE']) {
            if (!in_array($category, $errorDataPurview)) {
                Core::setLastError(PURVIEW_UNDEFINED);
                return false;
            }
        }
        
        return true;
    }
}
