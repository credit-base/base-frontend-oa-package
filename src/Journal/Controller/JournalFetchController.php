<?php
namespace Base\Package\Journal\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Journal\Repository\JournalRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Journal\View\Json\Journal\View;
use Base\Package\Journal\View\Json\Journal\ListView;

class JournalFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, JournalValidateTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new JournalRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : JournalRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('journals')) {
                return false;
            }
        }
       
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $journalList = array();

        list($count, $journalList) = $this->getRepository()
            ->scenario(JournalRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($journalList, $count));
        return true;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $title = $request->get('title', '');
        $year = $request->get('year', '');
        $status = $request->get('status', '');
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $filter = array();

        if (empty($userGroupId)) {
            $userGroupId = marmot_decode($request->get('userGroup', ''));
        }
        if (!empty($title)) {
            $filter['title'] = $title;
        }
        if (!empty($year)) {
            $filter['year'] =  $year;
        }
        if (!empty($status)) {
            $filter['status'] = marmot_decode($status);
        }
        if (!empty($userGroupId)) {
            $filter['publishUserGroup'] = $userGroupId;
        }
       
        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $journal = $this->getRepository()->scenario(JournalRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
        
        if ($journal instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($journal));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        return $this->checkUserHasJournalPurview($resource);
    }
}
