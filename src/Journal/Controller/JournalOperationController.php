<?php
namespace Base\Package\Journal\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\Journal\Repository\JournalRepository;
use Sdk\Journal\Command\Journal\AddJournalCommand;
use Sdk\Journal\Command\Journal\EditJournalCommand;
use Sdk\Journal\Command\Journal\MoveJournalCommand;
use Sdk\Journal\CommandHandler\Journal\JournalCommandHandlerFactory;

class JournalOperationController extends Controller implements IOperateAbleController
{
    use WebTrait, UtilsTrait, OperateControllerTrait, JournalValidateTrait, JournalRequestCommonTrait;

    protected $repository;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new JournalCommandHandlerFactory());
        $this->repository = new JournalRepository();
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        $requestData = $this->getRequestCommonData();
        
        if ($this->validateCommonScenario(
            $requestData['title'],
            $requestData['source'],
            $requestData['cover'],
            $requestData['attachment'],
            $requestData['authImages'],
            $requestData['description'],
            $requestData['year'],
            $requestData['status']
        )) {
            $command = new AddJournalCommand(
                $requestData['title'],
                $requestData['source'],
                $requestData['description'],
                $requestData['status'],
                $requestData['year'],
                CONSTANT,
                $requestData['cover'],
                $requestData['attachment'],
                $requestData['authImages']
            );
         
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        $requestData = $this->getRequestCommonData();
        
        if ($this->validateCommonScenario(
            $requestData['title'],
            $requestData['source'],
            $requestData['cover'],
            $requestData['attachment'],
            $requestData['authImages'],
            $requestData['description'],
            $requestData['year'],
            $requestData['status']
        )) {
            $command = new EditJournalCommand(
                $requestData['title'],
                $requestData['source'],
                $requestData['description'],
                $requestData['status'],
                $requestData['year'],
                $id,
                $requestData['cover'],
                $requestData['attachment'],
                $requestData['authImages']
            );
            
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
