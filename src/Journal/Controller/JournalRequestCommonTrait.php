<?php
namespace Base\Package\Journal\Controller;

use Marmot\Core;

trait JournalRequestCommonTrait
{
    protected function getRequestCommonData() : array
    {
        $request = $this->getRequest();
        $requestData = array();

        $requestData['title'] =  $request->post('title', '');
        $requestData['source'] =  $request->post('source', '');
        $requestData['description'] =  $request->post('description', '');
        $requestData['year'] =  $request->post('year', '');
        $requestData['cover'] =  $request->post('cover', array());
        $requestData['attachment'] =  $request->post('attachment', array());
        $requestData['authImages']=  $request->post('authImages', array());
        
        $status = $request->post('status', 0);
        $requestData['status'] = intval(marmot_decode($status));
        
        return $requestData;
    }
}
