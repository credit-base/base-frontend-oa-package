<?php
namespace Base\Package\Journal\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ApproveControllerTrait;
use Base\Package\Common\Controller\Interfaces\IApproveAbleController;

use Sdk\Journal\Command\UnAuditJournal\ApproveUnAuditJournalCommand;
use Sdk\Journal\Command\UnAuditJournal\RejectUnAuditJournalCommand;
use Sdk\Journal\CommandHandler\UnAuditJournal\UnAuditJournalCommandHandlerFactory;

class UnAuditJournalApproveController extends Controller implements IApproveAbleController
{
    use WebTrait, ApproveControllerTrait, JournalValidateTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditJournalCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function approveAction(int $id) : bool
    {
        $command = new ApproveUnAuditJournalCommand(
            $id
        );
        
        return $this->getCommandBus()->send($command);
    }

    protected function rejectAction(int $id) : bool
    {
        $request = $this->getRequest();
        
        $rejectReason = $request->post('rejectReason', '');

        if ($this->validateRejectScenario($rejectReason)) {
            $command = new RejectUnAuditJournalCommand(
                $rejectReason,
                $id
            );

            return $this->getCommandBus()->send($command);
        }

        return false;
    }
}
