<?php
namespace Base\Package\Journal\Controller;

use Marmot\Core;
use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\Journal\WidgetRules\JournalWidgetRules;
use Sdk\Crew\Model\Crew;

use Base\Sdk\Purview\Model\IPurviewAble;

trait JournalValidateTrait
{
    protected function getJournalWidgetRules() : JournalWidgetRules
    {
        return new JournalWidgetRules();
    }

    protected function getWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function validateCommonScenario(
        $title,
        $source,
        $cover,
        $attachment,
        $authImages,
        $description,
        $year,
        $status
    ) : bool {
        $commonWidgetRules = $this->getWidgetRules();
        $journalWidgetRules = $this->getJournalWidgetRules();
      
        return $commonWidgetRules->title($title, 'title')
            && $commonWidgetRules->source($source, 'source')
            && $commonWidgetRules->status($status, 'status')
            && $commonWidgetRules->description($description, 'description')
            && (empty($cover) ? true : $commonWidgetRules->image($cover, 'cover'))
            && (empty($attachment) ? true : $journalWidgetRules->attachment($attachment, 'attachment'))
            && (empty($authImages) ? true : $journalWidgetRules->authImages($authImages, 'authImages'))
            && $journalWidgetRules->year($year, 'year');
    }

    protected function validateRejectScenario($rejectReason)
    {
        $commonWidgetRules = $this->getWidgetRules();

        return  $commonWidgetRules->reason($rejectReason, 'rejectReason');
    }

    protected function checkUserHasJournalPurview($resource) : bool
    {
        $category = $this->getPurviewCategoryFactory()->getCategory($resource);
        $crewCategory = Core::$container->get('crew')->getCategory();
        
        $journalPurview = [IPurviewAble::CATEGORY['JOURNAL'], IPurviewAble::CATEGORY['UNAUDITED_JOURNAL']];
        if ($crewCategory != Crew::CATEGORY['SUPERTUBE']) {
            if (!in_array($category, $journalPurview)) {
                Core::setLastError(PURVIEW_UNDEFINED);
                return false;
            }
        }

        return true;
    }
}
