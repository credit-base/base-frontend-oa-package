<?php
namespace Base\Package\Journal\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Traits\EnableControllerTrait;
use Base\Package\Common\Controller\Interfaces\IEnableAbleController;

use Sdk\Journal\Command\Journal\EnableJournalCommand;
use Sdk\Journal\Command\Journal\DisableJournalCommand;
use Sdk\Journal\CommandHandler\Journal\JournalCommandHandlerFactory;

class JournalEnableController extends Controller implements IEnableAbleController
{
    use WebTrait, EnableControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new JournalCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function enableAction(int $id) : bool
    {
        $command = new EnableJournalCommand(
            $id
        );

        return $this->getCommandBus()->send($command);
    }

    protected function disableAction(int $id) : bool
    {
        $command = new DisableJournalCommand(
            $id
        );

        return $this->getCommandBus()->send($command);
    }
}
