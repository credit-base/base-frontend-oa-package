<?php
namespace Base\Package\Journal\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Journal\Repository\UnAuditJournalRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Journal\View\Json\UnAuditJournal\View;
use Base\Package\Journal\View\Json\UnAuditJournal\ListView;

use Base\Sdk\Common\Model\IApplyAble;

class UnAuditJournalFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, JournalValidateTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditJournalRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditJournalRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('unAuditedJournals')) {
                return false;
            }
        }
        
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();
      
        $unAuditJournalList = array();

        list($count, $unAuditJournalList) = $this->getRepository()
            ->scenario(UnAuditJournalRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($unAuditJournalList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $title = $request->get('title', '');
        $applyStatus = $request->get('applyStatus', '');
        $operationType = $request->get('operationType', '');
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $filter = array();

        if (empty($userGroupId)) {
            $userGroupId = marmot_decode($request->get('userGroup', ''));
        }
        if (!empty($title)) {
            $filter['title'] = $title;
        }
        if (!empty($operationType)) {
            $filter['operationType'] = marmot_decode($operationType);
        }

        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['PENDING'].','.
            IApplyAble::APPLY_STATUS['REJECT'];
        
        if (!empty($applyStatus)) {
            $filter['applyStatus'] = marmot_decode($applyStatus);
        }
        if (!empty($userGroupId)) {
            $filter['applyUserGroup'] = $userGroupId;
        }
        
        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $unAuditJournal = $this->getRepository()
            ->scenario(UnAuditJournalRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);
            
        if ($unAuditJournal instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($unAuditJournal));
        return true;
    }

    protected function checkUserHasPurview($resource) : bool
    {
        return $this->checkUserHasJournalPurview($resource);
    }
}
