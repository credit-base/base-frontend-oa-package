<?php
namespace Base\Package\Journal\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\UtilsTrait;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\Journal\Repository\UnAuditJournalRepository;
use Sdk\Journal\Command\UnAuditJournal\EditUnAuditJournalCommand;
use Sdk\Journal\CommandHandler\UnAuditJournal\UnAuditJournalCommandHandlerFactory;

class UnAuditJournalOperationController extends Controller implements IOperateAbleController
{
    use WebTrait, UtilsTrait, OperateControllerTrait, JournalValidateTrait, JournalRequestCommonTrait;

    protected $repository;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditJournalCommandHandlerFactory());
        $this->repository = new UnAuditJournalRepository();
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        $requestData = $this->getRequestCommonData();

        if ($this->validateCommonScenario(
            $requestData['title'],
            $requestData['source'],
            $requestData['cover'],
            $requestData['attachment'],
            $requestData['authImages'],
            $requestData['description'],
            $requestData['year'],
            $requestData['status']
        )) {
            $command = new EditUnAuditJournalCommand(
                $requestData['title'],
                $requestData['source'],
                $requestData['description'],
                $requestData['status'],
                $requestData['year'],
                $id,
                $requestData['cover'],
                $requestData['attachment'],
                $requestData['authImages']
            );
            
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        
        $this->displayError();
        return false;
    }
}
