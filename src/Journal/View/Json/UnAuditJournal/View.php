<?php
namespace Base\Package\Journal\View\Json\UnAuditJournal;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\Journal\Translator\UnAuditJournalTranslator;

class View extends JsonView implements IView
{
    private $unAuditJournal;

    private $translator;

    public function __construct($unAuditJournal)
    {
        $this->unAuditJournal = $unAuditJournal;
        $this->translator = new UnAuditJournalTranslator();
        parent::__construct();
    }

    protected function getUnAuditJournal()
    {
        return $this->unAuditJournal;
    }

    protected function getTranslator(): UnAuditJournalTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getTranslator()->objectToArray(
            $this->getUnAuditJournal(),
            array(
                'id',
                'title',
                'source',
                'cover',
                'rejectReason',
                'attachment',
                'description',
                'year',
                'status',
                'authImages',
                'applyStatus',
                'applyInfoType',
                'operationType',
                'relation' =>['id','realName'],
                'crew' => ['id','realName'],
                'applyCrew' => ['id','realName'],
                'applyUserGroup' => ['id','name'],
                'publishUserGroup' => ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        );
        
        $this->encode($data);
    }
}
