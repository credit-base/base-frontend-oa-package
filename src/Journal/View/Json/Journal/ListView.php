<?php
namespace Base\Package\Journal\View\Json\Journal;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Journal\Translator\JournalTranslator;

class ListView extends JsonView implements IView
{
    private $journal;

    private $count;

    private $translator;

    public function __construct(
        array $journal,
        int $count
    ) {
        $this->journal = $journal;
        $this->count = $count;
        $this->translator = new JournalTranslator();
        parent::__construct();
    }

    protected function getJournal(): array
    {
        return $this->journal;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): JournalTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        foreach ($this->getJournal() as $journal) {
            $data[] = $this->getTranslator()->objectToArray(
                $journal,
                array(
                    'id',
                    'title',
                    'source',
                    'year',
                    'status',
                    'crew' => ['id','realName'],
                    'userGroup'=> ['id','name'],
                    'createTime',
                    'updateTime',
                    'statusTime'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
