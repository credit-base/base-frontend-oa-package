<?php
namespace Base\Package\Journal\View\Json\Journal;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Sdk\Journal\Translator\JournalTranslator;

class View extends JsonView implements IView
{
    private $journal;

    private $translator;

    public function __construct($journal)
    {
        $this->journal = $journal;
        $this->translator = new JournalTranslator();
        parent::__construct();
    }

    protected function getJournal()
    {
        return $this->journal;
    }

    protected function getTranslator(): JournalTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $data = $this->getTranslator()->objectToArray(
            $this->getJournal(),
            array(
                'id',
                'title',
                'source',
                'cover',
                'attachment',
                'description',
                'authImages',
                'year',
                'status',
                'crew' => ['id','realName'],
                'userGroup'=> ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        );

        $this->encode($data);
    }
}
