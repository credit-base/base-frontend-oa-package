<?php
namespace Base\Package\WorkOrderTask\View\Json;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
trait WorkOrderTaskViewTrait
{
    protected function compareResult(array $baseItems, array $feedbackRecords)
    {
        foreach ($feedbackRecords as $feedbackRecord) {
            $result[] = $this->compare($baseItems, $feedbackRecord);
        }

        $compareResult = $this->getResultsData($result, $baseItems, $feedbackRecords);

        return $compareResult;
    }

    protected function compare(array $baseItems, array $feedbackRecord) : array
    {
        // 获取基础items的identify
        $baseItemsIdentifies = $this->getIdentifies($baseItems);
        // 获取反馈items的identify
        $feedbackRecordIdentifies = $this->getIdentifies($feedbackRecord['items']);

        // 通过比较基础items的identify和反馈items的identify，获取新增的identify
        $addResults = array_diff($feedbackRecordIdentifies, $baseItemsIdentifies);

        // 通过比较基础items的identify和反馈items的identify，获取删除的identify
        $delResults = array_diff($baseItemsIdentifies, $feedbackRecordIdentifies);

        $results = array_merge($addResults, $delResults);

        // 通过比较合并items的identify和反馈items的identify，获取需要比较的identify
        $compareIdentities = array_diff($feedbackRecordIdentifies, $results);

        $diffResult = array();
        foreach ($compareIdentities as $key => $value) {
            foreach ($baseItems as $baseItemsKey => $baseItemsValue) {
                if ($value == $baseItemsValue['identify']) {
                    $compareBaseItem = $baseItemsValue;
                }
            }
            foreach ($feedbackRecord['items'] as $feedbackRecordKey => $feedbackRecordValue) {
                if ($value == $feedbackRecordValue['identify']) {
                    $compareFeedbackRecord = $feedbackRecordValue;
                }
            }

            foreach ($compareBaseItem as $compareBaseItemKey => $compareBaseItemValue) {
                if (is_array($compareBaseItemValue)) {
                    if (isset($compareBaseItemValue['id'])) {
                        if (strcmp($compareBaseItemValue['id'], $compareFeedbackRecord[$compareBaseItemKey]['id'])) {
                            $diffResult[$value][$compareBaseItemKey] = $compareFeedbackRecord[$compareBaseItemKey];
                        }
                    }
                    if (!isset($compareBaseItemValue['id'])) {
                        if (array_diff($compareFeedbackRecord[$compareBaseItemKey], $compareBaseItemValue)) {
                            $diffResult[$value][$compareBaseItemKey] = $compareFeedbackRecord[$compareBaseItemKey];
                        }
                    }
                }
                if (!is_array($compareBaseItemValue)) {
                    if (strcmp($compareBaseItemValue, $compareFeedbackRecord[$compareBaseItemKey])) {
                        $diffResult[$value][$compareBaseItemKey] = $compareFeedbackRecord[$compareBaseItemKey];
                    }
                }
            }
        }

        return array(
            'addResults' => $addResults,
            'delResults' => $delResults,
            'diffResults' => $diffResult
        );
    }

    protected function getResultsData(array $results, array $baseItems, array $feedbackRecords) : array
    {
        foreach ($results as $resultsKey => $resultsValue) {
            $addResultData = $delResultData = $diffResultData = array();
            // 新增
            $addResults = $resultsValue['addResults'];
            if (!empty($addResults)) {
                foreach ($addResults as $key => $value) {
                    foreach ($feedbackRecords[$resultsKey]['items'] as $feedbackRecordsValue) {
                        if ($value == $feedbackRecordsValue['identify']) {
                            $push = array(
                                'feedbackType' => 'add'
                            );
                            $addResultData[$resultsKey][] = array_merge($push, $feedbackRecordsValue);
                        }
                    }
                }
            }

            // 删除
            $delResults = $resultsValue['delResults'];
            if (!empty($delResults)) {
                foreach ($delResults as $key => $value) {
                    foreach ($baseItems as $baseItemsValue) {
                        if ($value == $baseItemsValue['identify']) {
                            $push = array(
                                'feedbackType' => 'delete'
                            );
                            $delResultData[$resultsKey][] = array_merge($push, $baseItemsValue);
                        }
                    }
                }
            }

            // 修改
            $diffResults = $resultsValue['diffResults'];
            if (!empty($diffResults)) {
                foreach ($diffResults as $key => $value) {
                    foreach ($baseItems as $baseItemsValue) {
                        if ($key == $baseItemsValue['identify']) {
                            $push = array(
                                'feedbackType' => 'edit',
                                'latestRelease' => $value
                            );
                            $diffResultData[$resultsKey][] = array_merge($push, $baseItemsValue);
                        }
                    }
                }
            }
            $resultsData[] = array_merge($addResultData, $delResultData, $diffResultData);
        }

        return $resultsData;
    }

    protected function getIdentifies(array $items) : array
    {
        $identifies = array();
        foreach ($items as $each) {
            $identifies[] = $each['identify'];
        }

        return $identifies;
    }
}
