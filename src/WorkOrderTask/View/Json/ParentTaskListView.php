<?php
namespace Base\Package\WorkOrderTask\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\WorkOrderTask\Translator\ParentTaskTranslator;

class ParentTaskListView extends JsonView implements IView
{
    private $parentTask;

    private $count;

    private $translator;

    public function __construct(
        array $parentTask,
        int $count
    ) {
        $this->count = $count;
        $this->parentTask = $parentTask;
        $this->translator = new ParentTaskTranslator();
        parent::__construct();
    }

    protected function getParentTask() : array
    {
        return $this->parentTask;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : ParentTaskTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();

        $translator = $this->getTranslator();
        foreach ($this->getParentTask() as $parentTask) {
            $data[] = $translator->objectToArray(
                $parentTask,
                array(
                    'id',
                    'title',
                    'ratio',
                    'templateType',
                    'status',
                    'updateTime',
                    'endTime',
                    'template'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
