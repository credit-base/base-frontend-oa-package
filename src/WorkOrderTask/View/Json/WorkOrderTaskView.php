<?php

namespace Base\Package\WorkOrderTask\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\WorkOrderTask\Translator\WorkOrderTaskTranslator;

class WorkOrderTaskView extends JsonView implements IView
{
    use WorkOrderTaskViewTrait;

    private $workOrderTask;

    private $translator;

    public function __construct($workOrderTask)
    {
        $this->workOrderTask = $workOrderTask;
        $this->translator = new WorkOrderTaskTranslator();
        parent::__construct();
    }

    protected function getWorkOrderTask()
    {
        return $this->workOrderTask;
    }

    protected function getTranslator() : WorkOrderTaskTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();
        $workOrderTask = $this->getWorkOrderTask();

        $translator = $this->getTranslator();
        $data = $translator->objectToArray($workOrderTask);

        $feedbackRecords = $data['feedbackRecords'];
        $baseItems = $data['template']['items'];

        $compareResult = [];
        if (!empty($feedbackRecords)) {
            $compareResult = $this->compareResult($baseItems, $feedbackRecords);
        }

        $data['compareResult'] = $compareResult;

        $this->encode($data);
    }
}
