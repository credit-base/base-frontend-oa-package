<?php
namespace Base\Package\WorkOrderTask\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\WorkOrderTask\Translator\WorkOrderTaskTranslator;

class WorkOrderTaskListView extends JsonView implements IView
{
    private $workOrderTask;

    private $count;

    private $translator;

    public function __construct(
        array $workOrderTask,
        int $count
    ) {
        $this->count = $count;
        $this->workOrderTask = $workOrderTask;
        $this->translator = new WorkOrderTaskTranslator();
        parent::__construct();
    }

    protected function getWorkOrderTask() : array
    {
        return $this->workOrderTask;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : WorkOrderTaskTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();

        $translator = $this->getTranslator();
        foreach ($this->getWorkOrderTask() as $workOrderTask) {
            $data[] = $translator->objectToArray(
                $workOrderTask
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
