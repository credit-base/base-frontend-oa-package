<?php
namespace Base\Package\WorkOrderTask\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\WorkOrderTask\Repository\ParentTaskRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\WorkOrderTask\View\Json\ParentTaskListView;

class ParentTaskFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait, WebTrait;

    protected $repository;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ParentTaskRepository();
    }

    protected function getRepository(): ParentTaskRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size, $page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();
        list($count, $list) = $this->getRepository()
            ->scenario(ParentTaskRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ParentTaskListView($list, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();

        $title = $request->get('title', '');
        $sort = $request->get('sort', '-endTime');

        $filter = array();
        
        if (!empty($title)) {
            $filter['title'] = $title;
        }
        
        return [$filter, array($sort)];
    }

    protected function fetchOneAction(int $id): bool
    {
        unset($id);
        return false;
    }
}
