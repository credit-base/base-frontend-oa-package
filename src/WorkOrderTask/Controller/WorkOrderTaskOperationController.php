<?php
namespace Base\Package\WorkOrderTask\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand;
use Sdk\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand;
use Sdk\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand;
use Sdk\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand;
use Sdk\WorkOrderTask\CommandHandler\WorkOrderTask\WorkOrderTaskCommandHandlerFactory;
use Sdk\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;

class WorkOrderTaskOperationController extends Controller
{
    use WebTrait;

    private $commandBus;

    private $workOrderTaskWidgetRule;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new WorkOrderTaskCommandHandlerFactory());
        $this->workOrderTaskWidgetRule = new WorkOrderTaskWidgetRule();
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getWorkOrderTaskWidgetRule() : WorkOrderTaskWidgetRule
    {
        return $this->workOrderTaskWidgetRule;
    }

    public function end(string $id)
    {
        $reason = $this->getRequest()->post('reason', '');
        $id = marmot_decode($id);

        if ($this->validateReasonScenario(
            $reason
        )) {
            $command = new EndWorkOrderTaskCommand(
                $id,
                $reason
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        
        $this->displayError();
        return false;
    }

    public function revoke(string $id)
    {
        $reason = $this->getRequest()->post('reason', '');
        $id = marmot_decode($id);

        if ($this->validateReasonScenario(
            $reason
        )) {
            $command = new RevokeWorkOrderTaskCommand(
                $id,
                $reason
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        
        $this->displayError();
        return false;
    }

    public function confirm(string $id)
    {
        $id = marmot_decode($id);
        $command = new ConfirmWorkOrderTaskCommand($id);

        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }
        
        $this->displayError();
        return false;
    }

    public function feedback(string $id)
    {
        $id = marmot_decode($id);
        $reason = $this->getRequest()->post('reason', '');
        $userGroup = $this->getRequest()->post('userGroup', '');
        $isExistedTemplate = $this->getRequest()->post('isExistedTemplate', 0);
        $templateId = $this->getRequest()->post('templateId', '');
        $crew = Core::$container->get('crew')->getId();

        $itemsEncode = $this->getRequest()->post('items', array());
        $items = $this->getItemsDecode($itemsEncode);

        $feedbackRecords = array(
            array(
                'crew' => $crew,
                'userGroup' => marmot_decode($userGroup),
                'isExistedTemplate' => $isExistedTemplate,
                'templateId' => is_null($templateId) ? '' : marmot_decode($templateId),
                'reason' => $reason,
                'items' => $items
            )
        );

        if ($this->validateReasonScenario(
            $reason
        )) {
            $command = new FeedbackWorkOrderTaskCommand(
                $feedbackRecords,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        
        $this->displayError();
        return false;
    }

    protected function validateReasonScenario(
        $reason
    ) : bool {
        return $this->getWorkOrderTaskWidgetRule()->reason($reason);
    }

    protected function getItemsDecode(array $items) : array
    {
        if (!empty($items)) {
            foreach ($items as $key => $item) {
                $items[$key]['type'] = marmot_decode($item['type']);
                $items[$key]['isMasked'] = marmot_decode($item['isMasked']);
                $items[$key]['dimension'] = marmot_decode($item['dimension']);
                $items[$key]['isNecessary'] = marmot_decode($item['isNecessary']);
            }
        }
        
        return $items;
    }
}
