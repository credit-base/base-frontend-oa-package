<?php
namespace Base\Package\WorkOrderTask\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\WorkOrderTask\View\Json\WorkOrderTaskListView;
use Base\Package\WorkOrderTask\View\Json\WorkOrderTaskView;

class WorkOrderTaskFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait, WebTrait;

    const TYPE = array(
        'PARENT_TASK' => 1, //父级任务（一般为发改委）
        'ASSIGN_OBJECT' => 2  //指派对象（一般为下属委办局）
    );

    protected $repository;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new WorkOrderTaskRepository();
    }

    protected function getRepository(): WorkOrderTaskRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($size, $page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();
        
        $list = array();
        list($count, $list) = $this->getRepository()
            ->scenario(WorkOrderTaskRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new WorkOrderTaskListView($list, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();

        $type = $request->get('type', '');
        $title = $request->get('title', '');
        $parentTask = $request->get('parentTask', '');
        $assignObject = $request->get('assignObject', '');
        $sort = $request->get('sort', '-updateTime');
        $type = marmot_decode($type);

        $filter = array();
        
        if (!empty($title)) {
            $filter['title'] = $title;
        }
        if ($type == self::TYPE['PARENT_TASK']) {
            $filter['parentTask'] = marmot_decode($parentTask);
            if (!empty($assignObject)) {
                $filter['assignObject'] = marmot_decode($assignObject);
            }
        }
        if ($type == self::TYPE['ASSIGN_OBJECT']) {
            $sort = $request->get('sort', '-endTime');
            $filter['assignObject'] = marmot_decode($assignObject);
            $filter['status'] = '0,2,3,4';
        }
       
        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id): bool
    {
        $userGroup = $this->getRepository()
            ->scenario(WorkOrderTaskRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($userGroup instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new WorkOrderTaskView($userGroup));
        return true;
    }
}
