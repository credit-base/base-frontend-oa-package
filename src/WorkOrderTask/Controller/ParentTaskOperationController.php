<?php
namespace Base\Package\WorkOrderTask\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\WorkOrderTask\Command\ParentTask\AssignParentTaskCommand;
use Sdk\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;
use Sdk\WorkOrderTask\CommandHandler\ParentTask\ParentTaskCommandHandlerFactory;
use Sdk\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;

class ParentTaskOperationController extends Controller
{
    use WebTrait;

    private $commandBus;

    private $workOrderTaskWidgetRule;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ParentTaskCommandHandlerFactory());
        $this->workOrderTaskWidgetRule = new WorkOrderTaskWidgetRule();
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getWorkOrderTaskWidgetRule() : WorkOrderTaskWidgetRule
    {
        return $this->workOrderTaskWidgetRule;
    }

    public function assign()
    {
        $request = $this->getRequest();
        $title = $request->post('title', '');
        $description = $request->post('description', '');
        $attachment = $request->post('attachment', array());
        $templateType = $request->post('templateType', '');
        $template = $request->post('template', '');
        $endTime = $request->post('endTime', '');
        $assignObjects = $request->post('assignObjects', array());

        $templateType = marmot_decode($templateType);
        $template = marmot_decode($template);
        $assignObjects = $this->getAssignObjectCode($assignObjects);

        if ($this->validateAssignScenario(
            $title,
            $description,
            $attachment,
            $templateType
        )) {
            $command = new AssignParentTaskCommand(
                $title,
                $description,
                $endTime,
                $attachment,
                $assignObjects,
                $templateType,
                $template
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateAssignScenario(
        $title,
        $description,
        $attachment,
        $templateType
    ) : bool {
        return $this->getWorkOrderTaskWidgetRule()->title($title)
            && $this->getWorkOrderTaskWidgetRule()->description($description)
            && (empty($attachment) ? true : $this->getWorkOrderTaskWidgetRule()->attachments($attachment))
            && $this->getWorkOrderTaskWidgetRule()->formatNumeric($templateType);
    }

    public function revoke(string $id)
    {
        $reason = $this->getRequest()->post('reason', '');
        $id = marmot_decode($id);

        if ($this->validateReasonScenario(
            $reason
        )) {
            $command = new RevokeParentTaskCommand(
                $id,
                $reason
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        
        $this->displayError();
        return false;
    }

    protected function validateReasonScenario(
        $reason
    ) : bool {
        return $this->getWorkOrderTaskWidgetRule()->reason($reason);
    }

    protected function getAssignObjectCode(array $assignObjects) : array
    {
        $idDecodeData = array();
        foreach ($assignObjects as $assignObject) {
            $idDecodeData[] = marmot_decode($assignObject);
        }

        return $idDecodeData;
    }
}
