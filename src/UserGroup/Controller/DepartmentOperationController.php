<?php

namespace  Base\Package\UserGroup\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\UserGroup\Repository\DepartmentRepository;
use Base\Package\UserGroup\View\Json\DepartmentView;
use Sdk\UserGroup\WidgetRules\DepartmentWidgetRules;

use Sdk\Common\WidgetRules\WidgetRules;

use Sdk\UserGroup\Command\AddDepartmentCommand;
use Sdk\UserGroup\Command\EditDepartmentCommand;
use Sdk\UserGroup\CommandHandler\DepartmentCommandHandlerFactory;

class DepartmentOperationController extends Controller implements IOperateAbleController
{
    use WebTrait, OperateControllerTrait;

    private $commandBus;

    private $departmentWidgetRules;

    private $widgetRules;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new DepartmentCommandHandlerFactory());
        $this->departmentWidgetRules = new DepartmentWidgetRules();
        $this->widgetRules = new WidgetRules();
        $this->repository = new DepartmentRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getDepartmentWidgetRules() : DepartmentWidgetRules
    {
        return $this->departmentWidgetRules;
    }

    protected function getWidgetRules() : WidgetRules
    {
        return $this->widgetRules;
    }

    protected function getRepository() : DepartmentRepository
    {
        return $this->repository;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        $userGroupId = $this->getRequest()->post('userGroupId', '');
        $name = $this->getRequest()->post('name', '');

        $userGroupId = marmot_decode($userGroupId);

        if ($this->validateAddScenario($name, $userGroupId)) {
            $command = new AddDepartmentCommand(
                $name,
                $userGroupId
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateAddScenario(
        $name,
        $userGroupId
    ) : bool {
        return $this->getWidgetRules()->formatNumeric($userGroupId, 'userGroup')
            && $this->getDepartmentWidgetRules()->departmentName($name);
    }

    protected function validateEditScenario(string $name) : bool
    {
        return $this->getDepartmentWidgetRules()->departmentName($name);
    }

    protected function editView(int $id) : bool
    {
        $department = $this->getRepository()
            ->scenario(DepartmentRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($department instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new DepartmentView($department));
        return true;
    }

    protected function editAction($id)
    {
        $name = $this->getRequest()->post('name', '');
        if ($this->validateEditScenario($name)) {
            $command = new EditDepartmentCommand(
                $id,
                $name
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        $this->displayError();
        return false;
    }
}
