<?php
namespace Base\Package\UserGroup\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\UserGroup\Repository\DepartmentRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\UserGroup\View\Json\DepartmentListView;
use Base\Package\UserGroup\View\Json\DepartmentView;

class DepartmentFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait, WebTrait;

    protected $repository;

    const PAGE_SCENE = [
        'DEFAULT' => 'Lw',
        'ALL' => 'MA'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->repository = new DepartmentRepository();
    }

    protected function getRepository(): DepartmentRepository
    {
        return $this->repository;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $department = $this->getRepository()
            ->scenario(DepartmentRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($department instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new DepartmentView($department));
        return true;
    }

    protected function filterAction() : bool
    {
        list($filter, $sort, $page, $size) = $this->filterFormatChange();

        $departmentList = array();

        list($count, $departmentList) =
            $this->getRepository()->scenario(DepartmentRepository::LIST_MODEL_UN)
                 ->search($filter, $sort, $page, $size);

       
        $this->render(new DepartmentListView($departmentList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $userGroupId = $this->getRequest()->get('userGroupId', 'Lw');
        $userGroupId = marmot_decode($userGroupId);
        $name = $this->getRequest()->get('name', '');
        $sort = $this->getRequest()->get('sort', '-updateTime');
        $pageScene = $this->getRequest()->get('pageScene', 'Lw');

        list($size, $page) = $this->getPageAndSize();
        if ($pageScene == self::PAGE_SCENE['ALL']) {
            $size = COMMON_SIZES;
        }

        $filter = array();
        if ($userGroupId) {
            $filter['userGroup'] = $userGroupId;
        }
        if ($name) {
            $filter['name'] = $name;
        }
        
        return [$filter, array($sort), $page, $size];
    }
}
