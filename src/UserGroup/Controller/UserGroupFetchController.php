<?php
namespace Base\Package\UserGroup\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Sdk\UserGroup\Repository\UserGroupRepository;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\UserGroup\View\Json\UserGroupListView;
use Base\Package\UserGroup\View\Json\UserGroupView;

use Sdk\Crew\Repository\CrewRepository;

use Sdk\UserGroup\Repository\DepartmentRepository;

use Base\Package\Crew\Controller\CrewFetchController;

class UserGroupFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait, WebTrait;

    protected $repository;

    protected $crewRepository;

    protected $departmentRepository;

    protected $concurrentAdapter;

    const PAGE_SCENE = [
        'DEFAULT' => 'Lw',
        'ALL' => 'MA'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UserGroupRepository();
        $this->crewRepository = new CrewRepository();
        $this->departmentRepository = new DepartmentRepository();
        $this->concurrentAdapter = new ConcurrentAdapter();
    }

    protected function getRepository(): UserGroupRepository
    {
        return $this->repository;
    }

    protected function getCrewRepository(): CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getDepartmentRepository(): DepartmentRepository
    {
        return $this->departmentRepository;
    }

    protected function getConcurrentAdapter(): ConcurrentAdapter
    {
        return $this->concurrentAdapter;
    }
    
    protected function fetchOneAction(int $id): bool
    {
        $userGroup = $this->getRepository()
            ->scenario(UserGroupRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($userGroup instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $crewRepository = $this->getCrewRepository();
        $departmentRepository = $this->getDepartmentRepository();

        list($size, $page) = $this->getPageAndSize(COMMON_SIZES);

        $sort = ['-updateTime'];

        $currentLoginCrewCategory = Core::$container->get('crew')->getCategory();
        $crewFilter['userGroup'] = $id;
        $crewFilter['category'] = isset(CrewFetchController::CATEGORY_SCENE[$currentLoginCrewCategory]) ?
        CrewFetchController::CATEGORY_SCENE[$currentLoginCrewCategory] :
        0;

        $departmentFilter['userGroup'] = $id;

        $this->getConcurrentAdapter()->addPromise(
            'crews',
            $crewRepository->scenario($crewRepository::LIST_MODEL_UN)
                ->searchAsync($crewFilter, $sort, $page, $size),
            $crewRepository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'departments',
            $departmentRepository->scenario($departmentRepository::LIST_MODEL_UN)
                ->searchAsync($departmentFilter, $sort, $page, $size),
            $departmentRepository->getAdapter()
        );

        $data = $this->getConcurrentAdapter()->run();

        $this->render(new UserGroupView($userGroup, $data));
        return true;
    }

    protected function filterAction(): bool
    {
        list($filter, $sort, $page, $size) = $this->filterFormatChange();

        $userGroupList = array();

        list($count, $userGroupList) = $this->getRepository()->scenario(
            UserGroupRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        $this->render(new UserGroupListView($userGroupList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $name = $this->getRequest()->get('name', '');
        $pageScene = $this->getRequest()->get('pageScene', 'Lw');
        $sort = $this->getRequest()->get('sort', 'id');

        $filter = array();

        if ($name) {
            $filter['name'] = $name;
        }

        list($size, $page) = $this->getPageAndSize();

        if ($pageScene == self::PAGE_SCENE['ALL']) {
            $size = COMMON_SIZES;
        }

        return [$filter, array($sort), $page, $size];
    }
}
