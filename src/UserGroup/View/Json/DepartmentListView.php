<?php
namespace  Base\Package\UserGroup\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\UserGroup\Translator\DepartmentTranslator;

class DepartmentListView extends JsonView implements IView
{
    private $department;

    private $count;

    private $translator;

    public function __construct(
        array $department,
        int $count
    ) {
        $this->department = $department;
        $this->count = $count;
        $this->translator = new DepartmentTranslator();
        parent::__construct();
    }

    protected function getDepartment() : array
    {
        return $this->department;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : DepartmentTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();

        $translator = $this->getTranslator();
        foreach ($this->getDepartment() as $department) {
            $data[] = $translator->objectToArray(
                $department,
                array(
                    'id',
                    'name',
                    'userGroup',
                    'updateTime'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
