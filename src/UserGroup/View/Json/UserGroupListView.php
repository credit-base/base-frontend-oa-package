<?php
namespace  Base\Package\UserGroup\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\UserGroup\Translator\UserGroupTranslator;

class UserGroupListView extends JsonView implements IView
{
    private $userGroup;

    private $count;

    private $translator;

    public function __construct(
        array $userGroup,
        int $count
    ) {
        $this->userGroup = $userGroup;
        $this->count = $count;
        $this->translator = new UserGroupTranslator();
        parent::__construct();
    }

    protected function getUserGroup() : array
    {
        return $this->userGroup;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : UserGroupTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();

        $translator = $this->getTranslator();
        foreach ($this->getUserGroup() as $department) {
            $data[] = $translator->objectToArray(
                $department,
                array(
                    'id',
                    'name',
                    'shortName'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
