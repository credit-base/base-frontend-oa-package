<?php
namespace  Base\Package\UserGroup\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\UserGroup\Translator\DepartmentTranslator;

class DepartmentView extends JsonView implements IView
{
    private $department;

    private $translator;

    public function __construct($department)
    {
        $this->department = $department;
        $this->translator = new DepartmentTranslator();
        parent::__construct();
    }

    protected function getDepartment()
    {
        return $this->department;
    }

    protected function getTranslator() : DepartmentTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();
        $department = $this->getDepartment();

        $translator = $this->getTranslator();
        $data = $translator->objectToArray($department);

        $this->encode($data);
    }
}
