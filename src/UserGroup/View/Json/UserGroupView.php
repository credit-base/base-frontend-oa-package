<?php

namespace  Base\Package\UserGroup\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Translator\UserGroupTranslator;

use Sdk\Crew\Translator\CrewTranslator;

use Sdk\UserGroup\Translator\DepartmentTranslator;

class UserGroupView extends JsonView implements IView
{
    private $userGroup;

    private $data;

    private $translator;

    private $crewTranslator;

    private $departmentTranslator;

    public function __construct($userGroup, array $data)
    {
        $this->userGroup = $userGroup;
        $this->data = $data;
        $this->translator = new UserGroupTranslator();
        $this->crewTranslator = new CrewTranslator();
        $this->departmentTranslator = new DepartmentTranslator();
        parent::__construct();
    }

    protected function getUserGroup()
    {
        return $this->userGroup;
    }

    protected function getData() : array
    {
        return $this->data;
    }

    protected function getTranslator() : UserGroupTranslator
    {
        return $this->translator;
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return $this->crewTranslator;
    }

    protected function getDepartmentTranslator() : DepartmentTranslator
    {
        return $this->departmentTranslator;
    }

    public function display() : void
    {
        $data = array();

        $translator = $this->getTranslator();
        $data = $translator->objectToArray($this->getUserGroup());

        $data['crewList'] = $this->getCrewList();
        $data['departmentList'] = $this->getDepartmentList();

        $this->encode($data);
    }

    public function getCrewList()
    {
        $data = $this->getData();

        $crews = empty($data['crews']) ? [] : $data['crews'][1];
        $total = empty($data['crews']) ? [] : $data['crews'][0];

        $translator = $this->getCrewTranslator();

        $crewList = array();

        $crewList['total'] = $total;

        foreach ($crews as $val) {
            $crewList['list'][] = $translator->objectToArray(
                $val,
                array('id', 'realName', 'cellphone', 'category', 'updateTime', 'status', 'department')
            );
        }
        
        return $crewList;
    }

    public function getDepartmentList()
    {
        $data = $this->getData();
        
        $departments = empty($data['departments']) ? [] : $data['departments'][1];
        $total = empty($data['departments']) ? [] : $data['departments'][0];

        $translator = $this->getDepartmentTranslator();

        $departmentList = array();

        $departmentList['total'] = $total;

        foreach ($departments as $val) {
            $departmentList['list'][] = $translator->objectToArray(
                $val,
                array('id', 'name', 'updateTime')
            );
        }
        
        return $departmentList;
    }
}
