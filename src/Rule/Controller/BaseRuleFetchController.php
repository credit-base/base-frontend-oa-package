<?php
namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Marmot\Core;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Rule\Controller\Traits\RuleFetchControllerTrait;

use Base\Sdk\Rule\Model\IRule;

use Base\Package\Rule\Controller\Traits\RuleValidateTrait;

class BaseRuleFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RuleFetchControllerTrait, RuleValidateTrait;

    protected function filterAction() : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('baseRules')) {
                return false;
            }
        }

        $ruleType = IRule::CATEGORY['BASE_RULE'];
        return $this->getRuleList($ruleType);
    }

    protected function fetchOneAction(int $id) : bool
    {
        return $this->getRuleOne($id);
    }

    protected function checkUserHasPurview($resource) : bool
    {
        $type = IRule::CATEGORY['BASE_RULE'];
        return $this->checkUserHasRulePurview($resource, $type);
    }
}
