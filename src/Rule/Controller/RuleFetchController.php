<?php
namespace Base\Package\Rule\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Sdk\Rule\Model\IRule;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Rule\Controller\Traits\RuleFetchControllerTrait;

use Base\Package\Rule\Controller\Traits\RuleValidateTrait;

class RuleFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RuleFetchControllerTrait, RuleValidateTrait;

    protected function filterAction() : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('rules')) {
                return false;
            }
        }

        return $this->getRuleList();
    }

    protected function fetchOneAction(int $id) : bool
    {
        return $this->getRuleOne($id);
    }

    protected function checkUserHasPurview($resource) : bool
    {
        return $this->checkUserHasRulePurview($resource);
    }
}
