<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Rule\Repository\RuleRepository;

use Base\Package\Rule\View\Json\Rule\View;
use Base\Package\Rule\View\Json\Rule\ListView;

trait RuleFetchControllerTrait
{
    use RuleVersionRecodeTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new RuleRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : RuleRepository
    {
        return $this->repository;
    }

    public function getRuleList(int $type = 0):bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($type);

        $ruleList = array();

        list($count, $ruleList) = $this->getRepository()->scenario(
            RuleRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);
    
        $this->render(new ListView($ruleList, $count));
        return true;
    }


    public function getRuleOne(int $id):bool
    {
        $rule = $this->getRepository()->scenario(RuleRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
        
        if ($rule instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $versionRecode = $this->getUnAuditRuleList($id, true);

        $this->render(new View($rule, $versionRecode));
        return true;
    }
}
