<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Rule\Command\Rule\DeleteRuleCommand;
use Sdk\Rule\CommandHandler\Rule\RuleCommandHandlerFactory;

trait RuleDeleteControllerTrait
{
    use WebTrait;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new RuleCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    public function ruleDelete(int $id) : bool
    {
        $command = new DeleteRuleCommand(
            $id
        );

        return $this->getCommandBus()->send($command);
    }
}
