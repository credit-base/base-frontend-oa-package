<?php
namespace Base\Package\Rule\Controller\Traits;

use Base\Sdk\Rule\Model\IRule;
use Marmot\Core;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\Rule\WidgetRules\RuleWidgetRules;

use Sdk\Crew\Model\Crew;

use Base\Sdk\Purview\Model\IPurviewAble;

trait RuleValidateTrait
{
    protected function getRuleWidgetRules() : RuleWidgetRules
    {
        return new RuleWidgetRules();
    }

    protected function getWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }

    protected function validateAddScenario(
        $rules,
        $transformationTemplate,
        $sourceTemplate,
        $transformationCategory,
        $sourceCategory
    ) : bool {
        $commonWidgetRules = $this->getWidgetRules();
        $ruleWidgetRules = $this->getRuleWidgetRules();
      
        return $commonWidgetRules->formatNumeric($transformationTemplate, 'transformationTemplate')
            && $commonWidgetRules->formatNumeric($sourceTemplate, 'sourceTemplate')
            && $ruleWidgetRules->templateCategory($transformationCategory, 'transformationCategory')
            && $ruleWidgetRules->templateCategory($sourceCategory, 'sourceCategory')
            && $ruleWidgetRules->rules($rules);
    }

    protected function validateEditScenario(
        $rules
    ) : bool {
        $ruleWidgetRules = $this->getRuleWidgetRules();
      
        return $ruleWidgetRules->rules($rules);
    }

    protected function validateRejectScenario($rejectReason)
    {
        $commonWidgetRules = $this->getWidgetRules();

        return  $commonWidgetRules->reason($rejectReason, 'rejectReason');
    }

    protected function checkUserHasRulePurview($resource, $type = 0) : bool
    {
        $category = $this->getPurviewCategoryFactory()->getCategory($resource);
        $crewCategory = Core::$container->get('crew')->getCategory();
        
        $rulePurview = [IPurviewAble::CATEGORY['RULE_MANAGE'], IPurviewAble::CATEGORY['UNAUDITED_RULE_MANAGE']];
        if ($type == IRule::CATEGORY['BASE_RULE']) {
            $rulePurview = [
                IPurviewAble::CATEGORY['BASE_RULE_MANAGE'],
                IPurviewAble::CATEGORY['UNAUDITED_BASE_RULE_MANAGE']
            ];
        }
       
        if ($crewCategory != Crew::CATEGORY['SUPERTUBE']) {
            if (!in_array($category, $rulePurview)) {
                Core::setLastError(PURVIEW_UNDEFINED);
                return false;
            }
        }

        return true;
    }
}
