<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Core;

trait RuleRequestCommonTrait
{
    protected function getRequestCommonData() : array
    {
        $request = $this->getRequest();
        $requestData = array();
        
        $requestData['transformationTemplate'] =  marmot_decode($request->post('transformationTemplateId', ''));
        $requestData['transformationCategory'] =  marmot_decode($request->post('transformationCategory', ''));
        $requestData['sourceTemplate'] =  marmot_decode($request->post('sourceTemplateId', ''));
        $requestData['sourceCategory'] =  marmot_decode($request->post('sourceCategory', ''));
        $requestData['rules'] =  $request->post('rules', array());

        if (!empty($requestData['rules'])) {
            $requestData['rules'] = $this->getRulesDecode($requestData['rules']);
        }
        
        return $requestData;
    }

    protected function getRulesDecode(array $rules):array
    {
        if (isset($rules['completionRule'])) {
            foreach ($rules['completionRule'] as $key => $value) {
                $completionRules[$key]=$this->getCompletionRuleDecode($value);
            }
            $rules['completionRule'] = $completionRules;
        }
        if (isset($rules['comparisonRule'])) {
            foreach ($rules['comparisonRule'] as $comparisonKey => $val) {
                $comparisonRules[$comparisonKey]=$this->getCompletionRuleDecode($val);
            }
            $rules['comparisonRule'] = $comparisonRules;
        }
        if (isset($rules['deDuplicationRule'])) {
            $rules['deDuplicationRule'] =
                $this->getDeDuplicationRuleDecode($rules['deDuplicationRule']);
        }
       
        return $rules;
    }

    protected function getCompletionRuleDecode($completionRules):array
    {

        foreach ($completionRules as $key => $completionRule) {
            $completionRules[$key] = [
                'id'=>marmot_decode($completionRule['id']),
                'base'=> $this->getBaseDecode($completionRule['base']),
                'item'=>$completionRule['item'],
            ];
        }

        return $completionRules;
    }

    protected function getBaseDecode(array $base):array
    {
        foreach ($base as $key => $val) {
            $base[$key] = marmot_decode($val);
        }
        
        return $base;
    }

    protected function getDeDuplicationRuleDecode(array $deDuplicationRule):array
    {
        $deDuplicationRule['result'] = marmot_decode($deDuplicationRule['result']);
        
        return $deDuplicationRule;
    }
}
