<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Core;

use Sdk\Rule\Repository\UnAuditRuleRepository;
use Sdk\Rule\Model\UnAuditRule;

use Base\Sdk\Rule\Model\IRule;
use Base\Sdk\Common\Model\IEnableAble;

trait RuleVersionRecodeTrait
{
    protected function getUnAuditRuleRepository() : UnAuditRuleRepository
    {
        return new UnAuditRuleRepository();
    }

    protected function getUnAuditRuleList(int $relationId, $resourceType)
    {
        $page = 1;
        $maxSize = 1000;

        $filter = array();
        $filter['relationId'] = $relationId;
        
        if ($resourceType == true) {
            $filter['applyStatus'] = UnAuditRule::APPLY_STATUS['APPROVE'];
        }

        $unAuditRuleList = [];
        list($count, $unAuditRuleList) = $this->getUnAuditRuleRepository()
            ->scenario(UnAuditRuleRepository::LIST_MODEL_UN)
            ->search($filter, ['-updateTime'], $page, $maxSize);
        unset($count);
       
        return $unAuditRuleList;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function filterFormatChange(int $type = 0)
    {
        $request = $this->getRequest();
        
        $transformationCategory = $request->get('transformationCategory', '');
        $transformationTemplateId = $request->get('transformationTemplateId', '');
        $sourceCategory = $request->get('sourceCategory', '');
        $sourceTemplateId = $request->get('sourceTemplateId', '');
        $applyStatus = $request->get('applyStatus', '');
        $sort = $request->get('sort', '');
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $filter = array();

        $filter['status'] = IEnableAble::STATUS['ENABLED'];
        $filter['applyStatus'] = UnAuditRule::APPLY_STATUS['PENDING'].','.UnAuditRule::APPLY_STATUS['REJECT'];

        if (!empty($applyStatus)) {
            $filter['applyStatus'] = marmot_decode($applyStatus);
        }

        if (!empty($userGroupId)) {
            $filter['userGroup'] = $userGroupId;
        }
        
        if (empty($type)) {
            $filter['excludeTransformationCategory'] = IRule::CATEGORY['BASE_RULE'];
        }
        if (!empty($type)) {
            $filter['transformationCategory'] = $type;
        }

        if (!empty($transformationCategory)) {
            if (!empty($transformationTemplateId)) {
                $filter['transformationTemplate'] = marmot_decode($transformationTemplateId);
            }
            $filter['transformationCategory'] = marmot_decode($transformationCategory);
        }
        if (!empty($sourceCategory)) {
            $filter['sourceCategory'] = marmot_decode($sourceCategory);
            
            if (!empty($sourceTemplateId)) {
                $filter['sourceTemplate'] = marmot_decode($sourceTemplateId);
            }
        }
        if (empty($sort)) {
            $sort = '-updateTime';
        }
       
        return [$filter, [$sort]];
    }
}
