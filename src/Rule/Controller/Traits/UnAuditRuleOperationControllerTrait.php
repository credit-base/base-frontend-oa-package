<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Rule\Command\UnAuditRule\EditUnAuditRuleCommand;
use Sdk\Rule\CommandHandler\UnAuditRule\UnAuditRuleCommandHandlerFactory;

trait UnAuditRuleOperationControllerTrait
{
    use WebTrait, RuleValidateTrait, RuleRequestCommonTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditRuleCommandHandlerFactory());
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    public function unAuditRuleEdit(int $id)
    {
        $requestData = $this->getRequestCommonData();

        if ($this->validateEditScenario(
            $requestData['rules']
        )) {
            $command = new EditUnAuditRuleCommand(
                $requestData['rules'],
                $id
            );
            
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        
        $this->displayError();
        return false;
    }
}
