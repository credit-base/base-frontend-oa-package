<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Rule\Command\Rule\AddRuleCommand;
use Sdk\Rule\Command\Rule\EditRuleCommand;

use Sdk\Rule\CommandHandler\Rule\RuleCommandHandlerFactory;

trait RuleOperationControllerTrait
{
    use WebTrait, RuleValidateTrait, RuleRequestCommonTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new RuleCommandHandlerFactory());
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    public function ruleAdd() : bool
    {
        $requestData = $this->getRequestCommonData();
        
        if ($this->validateAddScenario(
            $requestData['rules'],
            $requestData['transformationTemplate'],
            $requestData['sourceTemplate'],
            $requestData['transformationCategory'],
            $requestData['sourceCategory']
        )) {
            $command = new AddRuleCommand(
                $requestData['rules'],
                $requestData['transformationTemplate'],
                $requestData['sourceTemplate'],
                $requestData['transformationCategory'],
                $requestData['sourceCategory']
            );
          
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function ruleEdit(int $id):bool
    {
        $requestData = $this->getRequestCommonData();
        
        if ($this->validateEditScenario(
            $requestData['rules']
        )) {
            $command = new EditRuleCommand(
                $requestData['rules'],
                $id
            );
            
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
