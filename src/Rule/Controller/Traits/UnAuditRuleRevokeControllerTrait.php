<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Rule\Command\UnAuditRule\RevokeUnAuditRuleCommand;
use Sdk\Rule\CommandHandler\UnAuditRule\UnAuditRuleCommandHandlerFactory;

trait UnAuditRuleRevokeControllerTrait
{
    use WebTrait;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditRuleCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    public function unAuditRuleRevoke(int $id) : bool
    {
        $command = new RevokeUnAuditRuleCommand(
            $id
        );

        return $this->getCommandBus()->send($command);
    }
}
