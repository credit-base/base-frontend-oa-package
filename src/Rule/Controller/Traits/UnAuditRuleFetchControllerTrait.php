<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Rule\Repository\UnAuditRuleRepository;

use Base\Package\Rule\View\Json\UnAuditRule\View;
use Base\Package\Rule\View\Json\UnAuditRule\ListView;

trait UnAuditRuleFetchControllerTrait
{
    use RuleVersionRecodeTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditRuleRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditRuleRepository
    {
        return $this->repository;
    }

    protected function fetchUnAuditRuleList(int $type = 0) : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($type);
      
        $unAuditRuleList = array();

        list($count, $unAuditRuleList) = $this->getRepository()
            ->scenario(UnAuditRuleRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ListView($unAuditRuleList, $count));
        return true;
    }

    protected function fetchUnAuditRuleOne(int $id) : bool
    {
        $unAuditRule = $this->getRepository()
            ->scenario(UnAuditRuleRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);
            
        if ($unAuditRule instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $ruleId = $unAuditRule->getRelationId();
       
        $unAuditedInformation = [];
        if (!empty($ruleId)) {
            $unAuditedInformation = $this->getUnAuditRuleList($ruleId, false);
        }
       
        $this->render(new View($unAuditRule, $unAuditedInformation));
        return true;
    }
}
