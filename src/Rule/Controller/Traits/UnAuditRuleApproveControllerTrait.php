<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Rule\Command\UnAuditRule\ApproveUnAuditRuleCommand;
use Sdk\Rule\Command\UnAuditRule\RejectUnAuditRuleCommand;
use Sdk\Rule\CommandHandler\UnAuditRule\UnAuditRuleCommandHandlerFactory;

trait UnAuditRuleApproveControllerTrait
{
    use WebTrait,RuleValidateTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new UnAuditRuleCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    public function unAuditRuleApprove(int $id):bool
    {
        $command = new ApproveUnAuditRuleCommand(
            $id
        );
        
        return $this->getCommandBus()->send($command);
    }

    public function unAuditRuleReject(int $id):bool
    {
        $request = $this->getRequest();
        
        $rejectReason = $request->post('rejectReason', '');

        if ($this->validateRejectScenario($rejectReason)) {
            $command = new RejectUnAuditRuleCommand(
                $rejectReason,
                $id
            );

            return $this->getCommandBus()->send($command);
        }

        return false;
    }
}
