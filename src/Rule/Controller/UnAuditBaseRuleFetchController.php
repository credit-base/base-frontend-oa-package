<?php
namespace Base\Package\Rule\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Rule\Controller\Traits\RuleValidateTrait;
use Base\Package\Rule\Controller\Traits\UnAuditRuleFetchControllerTrait;

use Base\Sdk\Rule\Model\IRule;

class UnAuditBaseRuleFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RuleValidateTrait, UnAuditRuleFetchControllerTrait;

    protected function filterAction() : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('unAuditedBaseRules')) {
                return false;
            }
        }
        
        $type = IRule::CATEGORY['BASE_RULE'];
        return $this->fetchUnAuditRuleList($type);
    }

    protected function fetchOneAction(int $id) : bool
    {
        return $this->fetchUnAuditRuleOne($id);
    }

    protected function checkUserHasPurview($resource) : bool
    {
        $type = IRule::CATEGORY['BASE_RULE'];
        return $this->checkUserHasRulePurview($resource, $type);
    }
}
