<?php
namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\Controller;
use Base\Package\Common\Controller\Traits\RevokeControllerTrait;
use Base\Package\Common\Controller\Interfaces\IRevokeAbleController;

use Base\Package\Rule\Controller\Traits\UnAuditRuleRevokeControllerTrait;

class UnAuditRuleRevokeController extends Controller implements IRevokeAbleController
{
    use RevokeControllerTrait, UnAuditRuleRevokeControllerTrait;
    
    protected function revokeAction(int $id) : bool
    {
        return $this->unAuditRuleRevoke($id);
    }
}
