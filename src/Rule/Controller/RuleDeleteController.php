<?php
namespace Base\Package\Rule\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;


use Base\Package\Common\Controller\Traits\DeleteControllerTrait;
use Base\Package\Common\Controller\Interfaces\IDeleteAbleController;

use Base\Package\Rule\Controller\Traits\RuleDeleteControllerTrait;

class RuleDeleteController extends Controller implements IDeleteAbleController
{
    use DeleteControllerTrait, RuleDeleteControllerTrait;

    protected function deleteAction(int $id) : bool
    {
        return $this->ruleDelete($id);
    }
}
