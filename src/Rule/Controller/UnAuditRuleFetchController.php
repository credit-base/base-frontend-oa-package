<?php
namespace Base\Package\Rule\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Rule\Controller\Traits\RuleValidateTrait;
use Base\Package\Rule\Controller\Traits\UnAuditRuleFetchControllerTrait;

class UnAuditRuleFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RuleValidateTrait, UnAuditRuleFetchControllerTrait;

    protected function filterAction() : bool
    {
        if (Core::$container->get('purview.status')) {
            if (!$this->checkUserHasPurview('unAuditedRules')) {
                return false;
            }
        }
        return $this->fetchUnAuditRuleList();
    }

    protected function fetchOneAction(int $id) : bool
    {
        return $this->fetchUnAuditRuleOne($id);
    }

    protected function checkUserHasPurview($resource) : bool
    {
        return $this->checkUserHasRulePurview($resource);
    }
}
