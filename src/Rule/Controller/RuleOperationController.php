<?php
namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\Controller;

use Base\Package\Common\Controller\Traits\UtilsTrait;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Base\Sdk\Rule\Model\IRule;

use Base\Package\Rule\Controller\Traits\RuleOperationControllerTrait;

class RuleOperationController extends Controller implements IOperateAbleController
{
    use UtilsTrait,
        OperateControllerTrait,
        RuleOperationControllerTrait;

    protected function addAction()
    {
        return $this->ruleAdd();
    }

    protected function editAction(int $id)
    {
        return $this->ruleEdit($id);
    }
}
