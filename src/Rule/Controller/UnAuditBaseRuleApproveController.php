<?php
namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\Controller;
use Base\Package\Common\Controller\Traits\ApproveControllerTrait;
use Base\Package\Common\Controller\Interfaces\IApproveAbleController;

use Base\Package\Rule\Controller\Traits\UnAuditRuleApproveControllerTrait;

class UnAuditBaseRuleApproveController extends Controller implements IApproveAbleController
{
    use ApproveControllerTrait, UnAuditRuleApproveControllerTrait;
    
    protected function approveAction(int $id) : bool
    {
        return $this->unAuditRuleApprove($id);
    }

    protected function rejectAction(int $id) : bool
    {
        return $this->unAuditRuleReject($id);
    }
}
