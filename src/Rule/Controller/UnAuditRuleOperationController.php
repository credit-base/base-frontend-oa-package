<?php
namespace Base\Package\Rule\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;

use Base\Package\Common\Controller\Traits\UtilsTrait;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Base\Package\Rule\Controller\Traits\UnAuditRuleOperationControllerTrait;

class UnAuditRuleOperationController extends Controller implements IOperateAbleController
{
    use UtilsTrait,
        OperateControllerTrait,
        UnAuditRuleOperationControllerTrait;

    protected function addAction()
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        return $this->unAuditRuleEdit($id);
    }
}
