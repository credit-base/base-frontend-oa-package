<?php
namespace Base\Package\Rule\View\Json;

use Sdk\Rule\Translator\RuleTranslator;
use Sdk\Rule\Translator\UnAuditRuleTranslator;

trait RuleJsonViewTrait
{
    use RulesContentFormatTrait;

    private $rule;

    private $translator;

    private $versionRecode;

    public function __construct($rule, $versionRecode = array())
    {
        parent::__construct();
        $this->rule = $rule;
        $this->versionRecode = $versionRecode;
        $this->translator = new RuleTranslator();
    }

    protected function getRule()
    {
        return $this->rule;
    }

    protected function getTranslator() : RuleTranslator
    {
        return $this->translator;
    }

    protected function getRuleData():array
    {
        $translator = $this->getTranslator();
        $data= array();
       
        $data = $translator->objectToArray(
            $this->getRule(),
            array(
                'id',
                'rules',
                'dataTotal',
                'version',
                'transformationCategory',
                'sourceCategory',
                'sourceTemplate'=>[],
                'transformationTemplate'=>[],
                'crew'=>['id','realName'],
                'userGroup'=>['id','name'],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
                )
        );

        if (!empty($data['rules'])) {
            $data['content'] = $this->getRulesContent(
                $data['rules'],
                $data['transformationTemplate'],
                $data['sourceTemplate']
            );
            unset($data['rules']);
        }

        if (!empty($this->getVersionRecode())) {
            $data['versionRecode'] = $this->getRuleVersionRecode();
        }
        $data['testResult'] = '暂无';
        
        return $data;
    }

    protected function getVersionRecode()
    {
        return $this->versionRecode;
    }

    protected function getUnAuditRuleTranslator() : UnAuditRuleTranslator
    {
        return new UnAuditRuleTranslator();
    }

    protected function getRuleVersionRecode()
    {
        $unAuditRuleTranslator = $this->getUnAuditRuleTranslator();
        $list = [];

        foreach ($this->getVersionRecode() as $value) {
            $list[] = $unAuditRuleTranslator->objectToArray(
                $value,
                array(
                    'id',
                    'version',
                    'crew'=>['id','realName','category'],
                    'updateTime',
                    'userGroup'=>['id','name']
                )
            );
        }

        $versionRecode = [
            'total'=>count($list),
            'list' => $list
        ];

        return $versionRecode;
    }
}
