<?php
namespace Base\Package\Rule\View\Json;

use Sdk\Rule\Translator\RuleTranslator;

trait RuleListJsonTrait
{
    private $ruleList;

    private $count;

    private $ruleTranslator;

    public function __construct(
        array $ruleList,
        int $count
    ) {
        parent::__construct();
        $this->ruleList = $ruleList;
        $this->count = $count;
        $this->ruleTranslator = new RuleTranslator();
    }

    protected function getRuleList() : array
    {
        return $this->ruleList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getRuleTranslator() : RuleTranslator
    {
        return $this->ruleTranslator;
    }

    protected function getResultRuleList() : array
    {
        $translator = $this->getRuleTranslator();

        $ruleList = array();
        foreach ($this->getRuleList() as $item) {
            $ruleList[] = $translator->objectToArray(
                $item,
                array(
                    'id',
                    'version',
                    'dataTotal',
                    'updateTime',
                    'transformationCategory',
                    'sourceCategory',
                    'sourceTemplate'=>['id','name'],
                    'transformationTemplate'=>['id','name'],
                    'userGroup'=>['id','name']
                )
            );
        }

        return $ruleList;
    }
}
