<?php
namespace Base\Package\Rule\View\Json\Rule;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Rule\View\Json\RuleJsonViewTrait;

class View extends JsonView implements IView
{
    use RuleJsonViewTrait;

    public function display() : void
    {
        $data = $this->getRuleData();

        $this->encode($data);
    }
}
