<?php
namespace Base\Package\Rule\View\Json\Rule;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Rule\View\Json\RuleListJsonTrait;

class ListView extends JsonView implements IView
{
    use RuleListJsonTrait;

    public function display() : void
    {
        $ruleList = $this->getResultRuleList();

        $result = array(
            'total' => $this->getCount(),
            'list' => $ruleList
        );

        $this->encode($result);
    }
}
