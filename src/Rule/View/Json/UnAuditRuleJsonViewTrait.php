<?php
namespace Base\Package\Rule\View\Json;

use Sdk\Rule\Translator\UnAuditRuleTranslator;

trait UnAuditRuleJsonViewTrait
{
    use RulesContentFormatTrait;

    private $unAuditRule;

    private $translator;

    private $unAuditedInformation;

    public function __construct($unAuditRule, $unAuditedInformation = array())
    {
        parent::__construct();
        $this->unAuditRule = $unAuditRule;
        $this->unAuditedInformation = $unAuditedInformation;
        $this->unAuditRuleTranslator = new UnAuditRuleTranslator();
    }

    protected function getUnAuditRule()
    {
        return $this->unAuditRule;
    }

    protected function getUnAuditRuleTranslator() : UnAuditRuleTranslator
    {
        return $this->unAuditRuleTranslator;
    }

    protected function getUnAuditRuleData():array
    {
        $translator = $this->getUnAuditRuleTranslator();
        
        $data= array();
        $data = $translator->objectToArray(
            $this->getUnAuditRule(),
            array(
                'id',
                'rules',
                'dataTotal',
                'version',
                'applyStatus',
                'operationType',
                'relationId',
                'rejectReason',
                'transformationCategory',
                'sourceCategory',
                'sourceTemplate'=>[],
                'transformationTemplate'=>[],
                'applyCrew'=>['id','realName'],
                'crew'=>['id','realName'],
                'userGroup'=>['id','name'],
                'applyUserGroup'=>['id','name'],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
                )
        );
     
        if (!empty($data['rules'])) {
            $data['content'] = $this->getRulesContent(
                $data['rules'],
                $data['transformationTemplate'],
                $data['sourceTemplate']
            );
            unset($data['rules']);
        }
     
        if (!empty($this->getVersionRecode())) {
            $data['unAuditedInformation'] = $this->getUnAuditedInformation();
        }
        $data['testResult'] = '暂无';

        return $data;
    }

    protected function getVersionRecode()
    {
        return $this->unAuditedInformation;
    }

    protected function getUnAuditedInformation()
    {
        $unAuditRuleTranslator = $this->getUnAuditRuleTranslator();
        $list = [];

        foreach ($this->getVersionRecode() as $value) {
            $list[] = $unAuditRuleTranslator->objectToArray(
                $value,
                array(
                    'id',
                    'version',
                    'rejectReason',
                    'applyStatus',
                    'crew'=>['id','realName','category'],
                    'updateTime',
                    'userGroup'=>['id','name']
                )
            );
        }

        $versionRecode = [
            'total'=>count($list),
            'list' => $list
        ];

        return $versionRecode;
    }
}
