<?php
namespace Base\Package\Rule\View\Json;

use Base\Sdk\Rule\Model\IRule;

use Sdk\Rule\Translator\RuleTranslator;

trait RulesContentFormatTrait
{
    protected function getRulesContent($rules, $transformationTemplate, $sourceTemplate):array
    {
        if (isset($rules['transformationRule'])) {
            $rules['transformationRule'] = $this->getTransformationRuleFormat(
                $rules['transformationRule'],
                $transformationTemplate['items'],
                $sourceTemplate
            );
        }
        if (isset($rules['completionRule'])) {
            $rules['completionRule'] = $this->getCompletionRuleFormat(
                $rules['completionRule'],
                $transformationTemplate['items']
            );
        }
       
        if (isset($rules['comparisonRule'])) {
            $rules['comparisonRule'] = $this->getCompletionRuleFormat(
                $rules['comparisonRule'],
                $transformationTemplate['items']
            );
        }
        if (isset($rules['deDuplicationRule'])) {
            $rules['deDuplicationRule'] = $this->getDeDuplicationRuleFormat(
                $rules['deDuplicationRule'],
                $transformationTemplate['items']
            );
        }
        
        return $rules;
    }

    protected function getTransformationRuleFormat(
        $transformationRule,
        $transformationTemplateItems,
        $sourceTemplate
    ):array {
        $data =[];
        
        foreach ($transformationRule as $key => $val) {
            $data[$key] = [
                'target' => $this->getDataByIdentify($key, $transformationTemplateItems),
                'origin' => $this->getDataByIdentify($val, $sourceTemplate['items']),
                'template' => $this->getSimplifyTemplate($sourceTemplate)
            ];
        }
        
        return array_values($data);
    }

    protected function getDataByIdentify($identify, $templateItems):array
    {
        $target = [];
        foreach ($templateItems as $item) {
            if ($identify == $item['identify']) {
                $target = [
                    'identify' => $identify,
                    'name' => $item['name'],
                ];
            }
        }

        return $target;
    }

    protected function getCompletionRuleFormat(
        $completionRule,
        $templateItems
    ):array {
        
        $data =[];
        foreach ($completionRule as $key => $val) {
            $data[$key] = $this->getItemData($key, $val, $templateItems);
        }
        
        return array_values($data);
    }

    protected function getItemData($targetIdentify, $items, $targetTemplate):array
    {
        $resultItems = [];
        foreach ($items as $key => $item) {
            $templateData = [
                'id'=> marmot_encode($item['id']),
                'name'=>$item['name'],
            ];

            $originData = [
                'identify'=>$item['item'],
                'name'=>$item['itemName']
            ];

            $resultItems[$key] = [
                'target' => $this->getDataByIdentify($targetIdentify, $targetTemplate),
                'origin' => $originData,
                'sourceCategory'=> "国标",
                'base' => $this->getFormatDataByBase($item['base']),
                'template' => $templateData
            ];
        }

        return $resultItems;
    }

    protected function getFormatDataByBase(array $base):array
    {
        $baseData = [];
        foreach ($base as $key => $item) {
            $baseData[$key] = [
                'id' => marmot_encode($item),
                'name' => IRule::COMPLETION_BASE_CN[$item],
            ];
        }
        return $baseData;
    }

    protected function getDeDuplicationRuleFormat(
        $deDuplicationRule,
        $templateItems
    ):array {
        $deDuplicationRule['result'] = [
            'id'=> marmot_encode($deDuplicationRule['result']),
            'name'=> IRule::DE_DUPLICATION_RESULT_CN[$deDuplicationRule['result']]
        ];

        $current = [];
        foreach ($deDuplicationRule['items'] as $key => $item) {
            $current[$key] = $this->getDataByIdentify($item, $templateItems);
        }
        $deDuplicationRule['items'] = $current;

        return $deDuplicationRule;
    }

    protected function getSimplifyTemplate(array $templates):array
    {
        $templateData = [
            'id'=> $templates['id'],
            'name' =>$templates['name']
        ] ;
        return $templateData;
    }
}
