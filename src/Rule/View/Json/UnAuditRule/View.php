<?php
namespace Base\Package\Rule\View\Json\UnAuditRule;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Rule\View\Json\UnAuditRuleJsonViewTrait;

class View extends JsonView implements IView
{
    use UnAuditRuleJsonViewTrait;

    public function display() : void
    {
        $unAuditRuleData = $this->getUnAuditRuleData();

        $this->encode($unAuditRuleData);
    }
}
