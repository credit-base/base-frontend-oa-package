<?php
namespace Base\Package\Rule\View\Json\UnAuditRule;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Rule\View\Json\UnAuditRuleListJsonTrait;

class ListView extends JsonView implements IView
{
    use UnAuditRuleListJsonTrait;

    public function display() : void
    {
        $unAuditRuleList = $this->getResultUnAuditRuleList();

        $result = array(
            'total' => $this->getCount(),
            'list' => $unAuditRuleList
        );

        $this->encode($result);
    }
}
