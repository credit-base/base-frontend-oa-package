<?php
namespace Base\Package\Rule\View\Json;

use Sdk\Rule\Translator\UnAuditRuleTranslator;

trait UnAuditRuleListJsonTrait
{
    private $unAuditRules;

    private $count;

    private $unAuditRuleTranslator;

    public function __construct(
        array $unAuditRules,
        int $count
    ) {
        parent::__construct();
        $this->unAuditRules = $unAuditRules;
        $this->count = $count;
        $this->unAuditRuleTranslator = new UnAuditRuleTranslator();
    }

    protected function getUnAuditRuleList() : array
    {
        return $this->unAuditRules;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getUnAuditRuleTranslator() : UnAuditRuleTranslator
    {
        return $this->unAuditRuleTranslator;
    }

    protected function getResultUnAuditRuleList() : array
    {
        $translator = $this->getUnAuditRuleTranslator();

        $list = array();
        foreach ($this->getUnAuditRuleList() as $item) {
            $list[] = $translator->objectToArray(
                $item,
                array(
                    'id',
                    'version',
                    'dataTotal',
                    'updateTime',
                    'applyStatus',
                    'rejectReason',
                    'transformationCategory',
                    'sourceCategory',
                    'sourceTemplate'=>['id','name'],
                    'transformationTemplate'=>['id','name'],
                    'userGroup'=>['id','name']
                )
            );
        }
        
        return $list;
    }
}
