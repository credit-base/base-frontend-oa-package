/**
 * @file 常量 - 插件
 * @module constants/plugin
 */

describe(`Constants - plugin`, () => {
  const OLD_ENV = process.env

  beforeEach(() => {
    jest.resetModules() // clears the cache
    process.env = { ...OLD_ENV } // Make a copy
  })

  afterEach(() => {
    process.env = OLD_ENV // Restore old environment
  })

  it(`Use process.env`, () => {
    const constants = require('@/constants/plugin')

    expect(constants.OSS_SUCCESS_ACTION_STATUS).toBe(`200`)
    // expect(constants.OSS_UPLOAD_HOST).toBe(ossUploadHost)
    // expect(constants.OSS_UPLOAD_PATH).toBe(ossUploadPath)
    // expect(constants.OSS_PREFIX).toBe(`${ossUploadHost}/${ossUploadPath}`)
    // expect(constants.TINYMCE_API_KEY).toBe(tinymceApiKey)
    // expect(constants.BAIDU_MAP_APIKEY).toBe(baiduMapApikey)
  })
})
