/**
 * 常量 - storage
 * @module constants/storage
 */

const STORAGE_PREFIX = `CREDIT_`

describe('Constants - storage', () => {
  beforeEach(() => {
    jest.resetModules()
  })

  it('storage-key', () => {
    const {
      APP_THEME_KEY,
      APP_TOKEN_KEY,
      APP_USERNAME_KEY,
      APP_LANGUAGE_KEY,
      APP_RULES_TEMPLATE_KEY,
    } = require('@/constants/storage')

    expect(APP_THEME_KEY).toBe(`${STORAGE_PREFIX}THEME`)
    expect(APP_TOKEN_KEY).toBe(`${STORAGE_PREFIX}TOKEN`)
    expect(APP_USERNAME_KEY).toBe(`${STORAGE_PREFIX}USERNAME`)
    expect(APP_LANGUAGE_KEY).toBe(`${STORAGE_PREFIX}LANGUAGE`)
    expect(APP_RULES_TEMPLATE_KEY).toBe(`${STORAGE_PREFIX}_RULES_TEMPLATE_DATA`)
  })
})
