/**
 * @file 加密函数 - 测试
 * @module utils/encrypt
 */

import {
  encrypt,
  decrypt,
} from '@/utils/encrypt'

const hash = global.randomString()
const text = global.randomString()

describe('Utils - encrypt', () => {
  it(`encrypt`, () => {
    const ciphertext = encrypt(hash, text)
    const cipherTextObj = JSON.parse(ciphertext)

    expect(cipherTextObj).toHaveProperty('ct')
    expect(cipherTextObj).toHaveProperty('iv')
    expect(cipherTextObj).toHaveProperty('s')
  })

  it(`decrypt`, () => {
    const ciphertext = encrypt(hash, text)
    const decryptText = decrypt(hash, ciphertext)

    expect(JSON.parse(decryptText)).toBe(text)
  })
})
