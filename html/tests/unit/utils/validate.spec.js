/**
 * @file 验证方法 - 测试
 * @module utils/validate
 */

import {
  isExternalUrl,
  isValidRejectReason,
  isValidRevokeReason,
  isValidFiles,
} from '@/utils/validate'

describe(`utils/validate`, () => {
  it(`isExternalUrl`, () => {
    expect(isExternalUrl(`https://www.qixinyun.com`)).toBeTruthy()
    expect(isExternalUrl(`this is com`)).toBeFalsy()
  })

  it(`isValidRejectReason`, () => {
    expect(isValidRejectReason()).toBe('驳回原因不能为空')
    expect(isValidRejectReason(global.randomString({ length: 200 }))).toBeTruthy()
    expect(isValidRejectReason(global.randomString({ length: 201 }))).toBe('驳回原因格式不正确，应为1-200字符，请重新输入')
  })

  it(`isValidRevokeReason`, () => {
    expect(isValidRevokeReason()).toBe('撤销原因不能为空')
    expect(isValidRevokeReason(global.randomString({ length: 200 }))).toBeTruthy()
    expect(isValidRevokeReason(global.randomString({ length: 201 }))).toBe('撤销原因格式不正确，应为1-200字符，请重新输入')
  })

  it('Test function isValidFiles', () => {
    let fileExtensionsList = ['doc', 'docx', 'xls', 'xlsx', 'pdf', 'rar', 'zip', 'ppt']
    const fileSize = 5
    let message = {}
    let file = {
      name: 'demo.doc',
      size: 3 * 1024 * 1024,
    }
    expect(isValidFiles(null, null, null, message)).toBeFalsy()

    expect(isValidFiles(file, fileSize, fileExtensionsList, message)).toBeTruthy()

    message = {
      messageSizeExceed: global.randomString(),
      messageTypeInvalid: global.randomString(),
    }
    expect(isValidFiles(file, fileSize, fileExtensionsList, message)).toBeTruthy()

    fileExtensionsList = ['png', 'jpg', 'jpeg']
    file = {
      name: 'demo.png',
      size: 3 * 1024 * 1024,
    }
    expect(isValidFiles(file, fileSize, fileExtensionsList, message)).toBeTruthy()

    fileExtensionsList = ['png', 'jpg', 'jpeg']
    file = {
      name: 'demo.gif',
      size: 3 * 1024 * 1024,
    }
    expect(isValidFiles(file, fileSize, fileExtensionsList, message)).toBeFalsy()

    fileExtensionsList = ['png', 'jpg', 'jpeg']
    file = {
      name: 'demo.jpeg',
      size: 12 * 1024 * 1024,
    }
    expect(isValidFiles(file, fileSize, fileExtensionsList, message)).toBeFalsy()
  })
})
