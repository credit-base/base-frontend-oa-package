/**
 * @file 日期时间处理 - 测试
 * @module utils/dat-time
 */

import {
  formatTime,
  getUnixTimeStamp,
} from '@/utils/date-time'

const time = new Date('2021-01-12 05:13:14') // 2020-07-13 05:13:14
const RE_DEFAULT_FORMATTER = /\d{4}年\d{2}月\d{2}日 \d{2}时\d{2}分/
const RE_CUSTOM_FORMATTER = /\d{4}年\d{2}月\d{2}/

describe(`Utils - date-time`, () => {
  it(`formatTime`, () => {
    expect(formatTime(Date.now())).toMatch(RE_DEFAULT_FORMATTER)
    expect(formatTime(~~(Date.now() / 1000))).toMatch(RE_DEFAULT_FORMATTER)
    expect(formatTime(time, 'YYYY年MM月DD')).toMatch(RE_CUSTOM_FORMATTER)
  })

  it(`getUnixTimeStamp`, () => {
    expect(getUnixTimeStamp()).toBe(~~(Date.now() / 1000))
    expect(getUnixTimeStamp(time)).toBe(~~(new Date(time).getTime() / 1000))
  })
})
