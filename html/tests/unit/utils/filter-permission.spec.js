import { filterAsyncRoutes } from '@/utils/filter-permission'
import route from '@/app-modules/news/router'

describe('Test utils filter-permission.js', () => {
  it('Test function filterAsyncRoutes', () => {
    const purview = [11, 12]
    expect(filterAsyncRoutes(route, purview)).toBeTruthy()
  })

  it(`Return routes when no purview`, () => {
    const routes = []
    expect(filterAsyncRoutes(routes)).toStrictEqual(routes)
  })
})
