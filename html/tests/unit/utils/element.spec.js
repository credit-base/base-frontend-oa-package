/**
 * @file ElementUI 工具函数 - 测试
 * @module utils/element
 */

import Vue from 'vue'
import { openConfirm, openPrompt } from '@/utils/element'

describe(`Utils - element`, () => {
  const onConfirm = jest.fn()
  const onPrompt = jest.fn()
  const EMPTY_STRING = ''
  const TIP_DEFAULT = '提示'

  Vue.prototype.$confirm = onConfirm
  Vue.prototype.$prompt = onPrompt

  beforeEach(() => {
    jest.resetAllMocks()
    jest.resetModules()
  })

  it(`openConfirm`, () => {
    openConfirm()

    expect(onConfirm).toBeCalledWith(EMPTY_STRING, TIP_DEFAULT, {
      type: 'warning',
      confirmButtonText: '确认',
      confirmButtonClass: 'is-plain el-button--success el-button--medium',
      cancelButtonText: '取消',
      cancelButtonClass: 'is-plain el-button--medium',
    })
  })

  it(`openPrompt`, () => {
    openPrompt()

    expect(onPrompt).toBeCalledWith(EMPTY_STRING, TIP_DEFAULT, {
      confirmButtonText: '确认',
      confirmButtonClass: 'is-plain el-button--success el-button--medium',
      cancelButtonText: '取消',
      cancelButtonClass: 'is-plain el-button--medium',
      inputType: 'textarea',
      inputPattern: /\S/,
      inputPlaceholder: '请输入',
      inputErrorMessage: '请输入原因',
    })
  })
})
