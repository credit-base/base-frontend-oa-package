/**
 * @file 工具函数 - 测试
 * @module utils/index
 */

import {
  makeMap,
  isInteger,
  randomNum,
  randomNumber,
  formatNumber,
  filterViewRouter,
  sleep,
  compare,
  getRandomString,
  renameFile,
  getNameFromIdentify,
  filterObject,
} from '@/utils'
import { CONSTANTS } from '@/plugins/constants'

const { FILTER_TYPE } = CONSTANTS

jest.useFakeTimers()

describe('Test utils index.js', () => {
  it('Test methods sleep', async () => {
    const delay = 1000
    jest.advanceTimersByTime(delay)
    const res = await sleep(delay)
    expect(res).toBeTruthy()
  })

  it('Test methods randomNum', () => {
    const minNum = 10
    const maxNum = 100
    expect(randomNum()).toBe(0)
    expect(randomNum(maxNum)).toBeLessThanOrEqual(maxNum)
    expect(randomNum(maxNum, minNum)).toBeLessThanOrEqual(maxNum)
    expect(randomNum(maxNum, minNum)).toBeGreaterThanOrEqual(minNum)
  })

  it('Test methods filterViewRouter', () => {
    const routerNoVisible = [
      {
        name: 'System',
        meta: {
          title: 'SYSTEM',
        },
        children: [
          {
            name: 'Crew',
            meta: {
              title: 'CREW',
            },
          },
          {
            name: 'UserGroup',
            meta: {
              title: 'USER_GROUP',
            },
          },
          {
            name: 'Department',
            meta: {
              title: 'DEPARTMENT',
            },
          },
        ],
      },
    ]

    const routerVisible = [
      {
        name: 'System',
        meta: {
          title: 'SYSTEM',
        },
        children: [
          {
            name: 'Crew',
            meta: {
              title: 'CREW',
            },
          },
          {
            name: 'UserGroup',
            meta: {
              title: 'USER_GROUP',
            },
          },
          {
            name: 'Department',
            meta: {
              title: 'DEPARTMENT',
              isVisible: true,
            },
          },
        ],
      },
    ]

    const routerVisibleTrue = [
      {
        name: 'System',
        meta: {
          title: 'SYSTEM',
        },
        children: [
          {
            name: 'Crew',
            meta: {
              title: 'CREW',
            },
          },
          {
            name: 'UserGroup',
            meta: {
              title: 'USER_GROUP',
            },
          },
          {
            name: 'Department',
            meta: {
              title: 'DEPARTMENT',
              isVisible: true,
            },
          },
        ],
      },
    ]

    const routerVisibleFalse = [
      {
        name: 'System',
        meta: {
          title: 'SYSTEM',
        },
        children: [
          {
            name: 'Crew',
            meta: {
              title: 'CREW',
            },
          },
          {
            name: 'UserGroup',
            meta: {
              title: 'USER_GROUP',
            },
          },
          {
            name: 'Department',
            meta: {
              title: 'DEPARTMENT',
              isVisible: true,
            },
          },
          {
            name: 'Dashboard',
            meta: {
              title: 'DEPARTMENT',
              isVisible: false,
            },
          },
        ],
      },
    ]

    expect(filterViewRouter(routerNoVisible, FILTER_TYPE.VISIBLE)).toStrictEqual(routerNoVisible)
    expect(filterViewRouter(routerVisibleTrue, FILTER_TYPE.VISIBLE)).toStrictEqual(routerVisible)
    expect(filterViewRouter(routerVisibleFalse, FILTER_TYPE.VISIBLE)).toStrictEqual(routerVisible)
  })

  it(`isInteger`, () => {
    // Falsy
    expect(isInteger()).toBeFalsy()
    expect(isInteger('')).toBeFalsy()
    expect(isInteger(true)).toBeFalsy()
    expect(isInteger(false)).toBeFalsy()
    expect(isInteger(1.1)).toBeFalsy()
    expect(isInteger([])).toBeFalsy()
    expect(isInteger({})).toBeFalsy()
    expect(isInteger(null)).toBeFalsy()
    expect(isInteger(undefined)).toBeFalsy()
    expect(isInteger('1.1', { allowString: true })).toBeFalsy()

    // Truthy
    expect(isInteger(10)).toBeTruthy()
    expect(isInteger(0)).toBeTruthy()
    expect(isInteger(-10)).toBeTruthy()
    expect(isInteger('11', { allowString: true })).toBeTruthy()
  })

  it(`makeMap`, () => {
    expect(makeMap(global.randomString())).toBeInstanceOf(Map)
    expect(makeMap(global.randomString()).size).toBe(0)
  })

  it(`randomNumber`, () => {
    expect(randomNumber(0)).toBe(0)
  })

  it(`formatNumber`, () => {
    expect(formatNumber(global.randomString())).toBe(0)
    expect(formatNumber(123456789)).toBe(`123,456,789`)
  })

  it('Test function compare', () => {
    const data = [
      {
        age: 13,
        name: '张睿',
      },
      {
        age: 9,
        name: '王艳丽',
      },
      {
        age: 5,
        name: '刘欢',
      },
    ]

    expect(data.sort(compare('age'))).toStrictEqual([
      {
        age: 5,
        name: '刘欢',
      },
      {
        age: 9,
        name: '王艳丽',
      },
      {
        age: 13,
        name: '张睿',
      },
    ])

    expect(data.sort(compare('age', 'DESCENDING'))).toStrictEqual([
      {
        age: 13,
        name: '张睿',
      },
      {
        age: 9,
        name: '王艳丽',
      },
      {
        age: 5,
        name: '刘欢',
      },
    ])
  })

  it('Test function getRandomString', () => {
    expect(getRandomString({ length: 12 })).toHaveLength(12)
  })

  it('Test function renameFile', () => {
    const ext = 'bar'
    const file = { name: `foo.${ext}` }
    const RE_NEW_FILE_NAME = new RegExp(`[a-z1-9]{10}_\\d{13}\\.${ext}`)

    expect(renameFile(file)).toMatch(RE_NEW_FILE_NAME)
  })

  it('Test function getNameFromIdentify', () => {
    const name = 'foo'
    const identify = `${name}.bar`

    expect(getNameFromIdentify()).toBe('')
    expect(getNameFromIdentify(identify)).toBe(name)
  })

  it('Test filterObject', () => {
    let params = {
      id: 'MA',
      title: '你好',
      cover: {
        name: '123445',
        identify: '12345.jpg',
      },
      attachments: [
        {
          name: '123445',
          identify: '12345.jpg',
        },
      ],
      userGroupId: 'MA',
    }
    expect(filterObject(params)).toStrictEqual({ ...params })
    params = {
      id: 'MA',
      title: '',
      cover: {
      },
      attachments: [
      ],
      userGroupId: 'MA',
    }
    expect(filterObject(params)).toStrictEqual({ id: 'MA', userGroupId: 'MA' })
    expect(filterObject()).toStrictEqual({})
  })
})
