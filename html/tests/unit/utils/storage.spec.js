/**
 * @file 存储工具函数 - 测试
 * @module utils/storage
 */

import {
  getToken,
  setToken,
  removeToken,
  getTheme,
  setTheme,
  removeTheme,
  getUsername,
  setUsername,
  removeUsername,
  getLanguage,
  setLanguage,
  removeLanguage,
  getRulesTemplateData,
  setRulesTemplateData,
} from '@/utils/storage'

import {
  APP_TOKEN_KEY,
  APP_THEME_KEY,
  APP_LANGUAGE_KEY,
  APP_USERNAME_KEY,
  APP_RULES_TEMPLATE_KEY,
} from '@/constants/storage'

const str = 'HelloTest'
const result = `[STORAGE_KEY]=${str}; path=/`

describe('Utils -> storage', () => {
  it('Token', () => {
    expect(getToken()).toBeUndefined()
    expect(setToken(str)).toBe(result.replace(/\[STORAGE_KEY\]/, APP_TOKEN_KEY))
    expect(getToken()).toBe(str)
    expect(removeToken()).toBeUndefined()
  })

  it('Language', () => {
    expect(getLanguage()).toBeUndefined()
    expect(setLanguage(str)).toBe(result.replace(/\[STORAGE_KEY\]/, APP_LANGUAGE_KEY))
    expect(getLanguage()).toBe(str)
    expect(removeLanguage()).toBeUndefined()
  })

  it('Theme', () => {
    expect(getTheme()).toBeUndefined()
    expect(setTheme(str)).toBe(result.replace(/\[STORAGE_KEY\]/, APP_THEME_KEY))
    expect(getTheme()).toBe(str)
    expect(removeTheme()).toBeUndefined()
  })

  it('UserName', () => {
    expect(getUsername()).toBeUndefined()
    expect(setUsername(str)).toBe(result.replace(/\[STORAGE_KEY\]/, APP_USERNAME_KEY))
    expect(getUsername()).toBe(str)
    expect(removeUsername()).toBeUndefined()
  })

  it(`RulesTemplate`, () => {
    expect(getRulesTemplateData()).toBeUndefined()
    expect(setRulesTemplateData(str)).toBe(result.replace(/\[STORAGE_KEY\]/, APP_RULES_TEMPLATE_KEY))
    expect(getRulesTemplateData()).toBe(str)
  })
})
