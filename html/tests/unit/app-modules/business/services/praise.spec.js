jest.mock('@/utils/request')
import request from '@/utils/request'

import {
  getPraiseList,
  getPraiseDetail,
  acceptPraise,
  publishPraise,
  unPublishPraise,
  getUnAuditedPraiseList,
  getUnAuditedPraiseDetail,
  resubmitPraise,
  approveUnAuditedPraise,
  rejectUnAuditedPraise,
} from '@/app-modules/business/services/praise'

const params = {}
const id = 'MA'

describe('Test methods services', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services getPraiseList', () => {
    getPraiseList(params)
    expect(request).toBeCalledWith(`/api/praises`, { params })
  })

  it('Test services getPraiseDetail', () => {
    getPraiseDetail(id, params)
    expect(request).toBeCalledWith(`/api/praises/${id}`, { params })
  })

  it('Test services acceptPraise', () => {
    acceptPraise(id, params)
    expect(request).toBeCalledWith(`/api/praises/${id}/accept`, { method: 'POST', params })
  })

  it('Test services publishPraise', () => {
    publishPraise(id, params)
    expect(request).toBeCalledWith(`/api/praises/${id}/publish`, { method: 'POST', params })
  })

  it('Test services unPublishPraise', () => {
    unPublishPraise(id, params)
    expect(request).toBeCalledWith(`/api/praises/${id}/unPublish`, { method: 'POST', params })
  })

  it('Test services getUnAuditedPraiseList', () => {
    getUnAuditedPraiseList(params)
    expect(request).toBeCalledWith(`/api/unAuditedPraises`, { params })
  })

  it('Test services getUnAuditedPraiseDetail', () => {
    getUnAuditedPraiseDetail(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedPraises/${id}`, { params })
  })

  it('Test services resubmitPraise', () => {
    resubmitPraise(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedPraises/${id}/resubmit`, { method: 'POST', params })
  })

  it('Test services approveUnAuditedPraise', () => {
    approveUnAuditedPraise(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedPraises/${id}/approve`, { method: 'POST', params })
  })

  it('Test services rejectUnAuditedPraise', () => {
    rejectUnAuditedPraise(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedPraises/${id}/reject`, { method: 'POST', params })
  })
})
