jest.mock('@/utils/request')
import request from '@/utils/request'

import {
  getComplaintList,
  getComplaintDetail,
  acceptComplaint,
  getUnAuditedComplaintList,
  getUnAuditedComplaintDetail,
  resubmitComplaint,
  approveUnAuditedComplaint,
  rejectUnAuditedComplaint,
} from '@/app-modules/business/services/complaint'

const params = {}
const id = 'MA'

describe('Test methods services', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services getComplaintList', () => {
    getComplaintList(params)
    expect(request).toBeCalledWith(`/api/complaints`, { params })
  })

  it('Test services getComplaintDetail', () => {
    getComplaintDetail(id, params)
    expect(request).toBeCalledWith(`/api/complaints/${id}`, { params })
  })

  it('Test services acceptComplaint', () => {
    acceptComplaint(id, params)
    expect(request).toBeCalledWith(`/api/complaints/${id}/accept`, { method: 'POST', params })
  })

  it('Test services getUnAuditedComplaintList', () => {
    getUnAuditedComplaintList(params)
    expect(request).toBeCalledWith(`/api/unAuditedComplaints`, { params })
  })

  it('Test services getUnAuditedComplaintDetail', () => {
    getUnAuditedComplaintDetail(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedComplaints/${id}`, { params })
  })

  it('Test services resubmitComplaint', () => {
    resubmitComplaint(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedComplaints/${id}/resubmit`, { method: 'POST', params })
  })

  it('Test services approveUnAuditedComplaint', () => {
    approveUnAuditedComplaint(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedComplaints/${id}/approve`, { method: 'POST', params })
  })

  it('Test services rejectUnAuditedComplaint', () => {
    rejectUnAuditedComplaint(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedComplaints/${id}/reject`, { method: 'POST', params })
  })
})
