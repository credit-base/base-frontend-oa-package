jest.mock('@/utils/request')
import request from '@/utils/request'

import {
  getQaList,
  getQaDetail,
  acceptQa,
  getUnAuditedQaList,
  getUnAuditedQaDetail,
  resubmitQa,
  approveUnAuditedQa,
  rejectUnAuditedQa,
} from '@/app-modules/business/services/qa'

const params = {}
const id = 'MA'

describe('Test methods services', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services getQaList', () => {
    getQaList(params)
    expect(request).toBeCalledWith(`/api/qas`, { params })
  })

  it('Test services getQaDetail', () => {
    getQaDetail(id, params)
    expect(request).toBeCalledWith(`/api/qas/${id}`, { params })
  })

  it('Test services acceptQa', () => {
    acceptQa(id, params)
    expect(request).toBeCalledWith(`/api/qas/${id}/accept`, { method: 'POST', params })
  })

  it('Test services getUnAuditedQaList', () => {
    getUnAuditedQaList(params)
    expect(request).toBeCalledWith(`/api/unAuditedQas`, { params })
  })

  it('Test services getUnAuditedQaDetail', () => {
    getUnAuditedQaDetail(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedQas/${id}`, { params })
  })

  it('Test services resubmitQa', () => {
    resubmitQa(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedQas/${id}/resubmit`, { method: 'POST', params })
  })

  it('Test services approveUnAuditedQa', () => {
    approveUnAuditedQa(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedQas/${id}/approve`, { method: 'POST', params })
  })

  it('Test services rejectUnAuditedQa', () => {
    rejectUnAuditedQa(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedQas/${id}/reject`, { method: 'POST', params })
  })
})
