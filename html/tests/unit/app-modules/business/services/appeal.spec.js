jest.mock('@/utils/request')
import request from '@/utils/request'

import {
  getAppealList,
  getAppealDetail,
  acceptAppeal,
  getUnAuditedAppealList,
  getUnAuditedAppealDetail,
  resubmitAppeal,
  approveUnAuditedAppeal,
  rejectUnAuditedAppeal,
} from '@/app-modules/business/services/appeal'

const params = {}
const id = 'MA'

describe('Test methods services', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services getAppealList', () => {
    getAppealList(params)
    expect(request).toBeCalledWith(`/api/appeals`, { params })
  })

  it('Test services getAppealDetail', () => {
    getAppealDetail(id, params)
    expect(request).toBeCalledWith(`/api/appeals/${id}`, { params })
  })

  it('Test services acceptAppeal', () => {
    acceptAppeal(id, params)
    expect(request).toBeCalledWith(`/api/appeals/${id}/accept`, { method: 'POST', params })
  })

  it('Test services getUnAuditedAppealList', () => {
    getUnAuditedAppealList(params)
    expect(request).toBeCalledWith(`/api/unAuditedAppeals`, { params })
  })

  it('Test services getUnAuditedAppealDetail', () => {
    getUnAuditedAppealDetail(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedAppeals/${id}`, { params })
  })

  it('Test services resubmitAppeal', () => {
    resubmitAppeal(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedAppeals/${id}/resubmit`, { method: 'POST', params })
  })

  it('Test services approveUnAuditedAppeal', () => {
    approveUnAuditedAppeal(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedAppeals/${id}/approve`, { method: 'POST', params })
  })

  it('Test services rejectUnAuditedAppeal', () => {
    rejectUnAuditedAppeal(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedAppeals/${id}/reject`, { method: 'POST', params })
  })
})
