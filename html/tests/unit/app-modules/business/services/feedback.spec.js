jest.mock('@/utils/request')
import request from '@/utils/request'

import {
  getFeedbackList,
  getFeedbackDetail,
  acceptFeedback,
  getUnAuditedFeedbackList,
  getUnAuditedFeedbackDetail,
  resubmitFeedback,
  approveUnAuditedFeedback,
  rejectUnAuditedFeedback,
} from '@/app-modules/business/services/feedback'

const params = {}
const id = 'MA'

describe('Test methods services', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services getFeedbackList', () => {
    getFeedbackList(params)
    expect(request).toBeCalledWith(`/api/feedbacks`, { params })
  })

  it('Test services getFeedbackDetail', () => {
    getFeedbackDetail(id, params)
    expect(request).toBeCalledWith(`/api/feedbacks/${id}`, { params })
  })

  it('Test services acceptFeedback', () => {
    acceptFeedback(id, params)
    expect(request).toBeCalledWith(`/api/feedbacks/${id}/accept`, { method: 'POST', params })
  })

  it('Test services getUnAuditedFeedbackList', () => {
    getUnAuditedFeedbackList(params)
    expect(request).toBeCalledWith(`/api/unAuditedFeedbacks`, { params })
  })

  it('Test services getUnAuditedFeedbackDetail', () => {
    getUnAuditedFeedbackDetail(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedFeedbacks/${id}`, { params })
  })

  it('Test services resubmitFeedback', () => {
    resubmitFeedback(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedFeedbacks/${id}/resubmit`, { method: 'POST', params })
  })

  it('Test services approveUnAuditedFeedback', () => {
    approveUnAuditedFeedback(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedFeedbacks/${id}/approve`, { method: 'POST', params })
  })

  it('Test services rejectUnAuditedFeedback', () => {
    rejectUnAuditedFeedback(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedFeedbacks/${id}/reject`, { method: 'POST', params })
  })
})
