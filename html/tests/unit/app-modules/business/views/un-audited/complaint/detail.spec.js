jest.mock('@/app-modules/business/services/complaint')
jest.mock('@/utils/element')
import ComplaintUnAuditedDetail from '@//app-modules/business/views/un-audited/complaint/detail.vue'
import { openConfirm, openPrompt } from '@/utils/element'
import { getUnAuditedComplaintDetail, approveUnAuditedComplaint, rejectUnAuditedComplaint } from '@/app-modules/business/services/complaint'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue ComplaintUnAuditedDetail', () => {
  let wrapper
  // const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(ComplaintUnAuditedDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedComplaintDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getUnAuditedComplaintDetail).toBeCalled()
  })

  it('Test methods handleApprove', async () => {
    openConfirm.mockResolvedValue()
    approveUnAuditedComplaint.mockResolvedValue()
    await wrapper.vm.handleApprove()
    expect(approveUnAuditedComplaint).toBeCalled()
  })

  it('Test methods handleReject', async () => {
    openPrompt.mockResolvedValue({ rejectReason: 'demos' })
    rejectUnAuditedComplaint.mockResolvedValue()
    await wrapper.vm.handleReject()
    expect(rejectUnAuditedComplaint).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
