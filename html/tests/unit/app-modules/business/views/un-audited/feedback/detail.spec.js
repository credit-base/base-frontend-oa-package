jest.mock('@/app-modules/business/services/feedback')
jest.mock('@/utils/element')
import FeedbackUnAuditedDetail from '@//app-modules/business/views/un-audited/feedback/detail.vue'
import { openConfirm, openPrompt } from '@/utils/element'
import { getUnAuditedFeedbackDetail, approveUnAuditedFeedback, rejectUnAuditedFeedback } from '@/app-modules/business/services/feedback'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue FeedbackUnAuditedDetail', () => {
  let wrapper
  // const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FeedbackUnAuditedDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedFeedbackDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getUnAuditedFeedbackDetail).toBeCalled()
  })

  it('Test methods handleApprove', async () => {
    openConfirm.mockResolvedValue()
    approveUnAuditedFeedback.mockResolvedValue()
    await wrapper.vm.handleApprove()
    expect(approveUnAuditedFeedback).toBeCalled()
  })

  it('Test methods handleReject', async () => {
    openPrompt.mockResolvedValue({ rejectReason: 'demos' })
    rejectUnAuditedFeedback.mockResolvedValue()
    await wrapper.vm.handleReject()
    expect(rejectUnAuditedFeedback).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
