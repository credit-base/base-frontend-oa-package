jest.mock('@/app-modules/business/services/qa')
jest.mock('@/utils/element')
import QaUnAuditedDetail from '@//app-modules/business/views/un-audited/qa/detail.vue'
import { openConfirm, openPrompt } from '@/utils/element'
import { getUnAuditedQaDetail, approveUnAuditedQa, rejectUnAuditedQa } from '@/app-modules/business/services/qa'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue QaUnAuditedDetail', () => {
  let wrapper
  // const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(QaUnAuditedDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedQaDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getUnAuditedQaDetail).toBeCalled()
  })

  it('Test methods handleApprove', async () => {
    openConfirm.mockResolvedValue()
    approveUnAuditedQa.mockResolvedValue()
    await wrapper.vm.handleApprove()
    expect(approveUnAuditedQa).toBeCalled()
  })

  it('Test methods handleReject', async () => {
    openPrompt.mockResolvedValue({ rejectReason: 'demos' })
    rejectUnAuditedQa.mockResolvedValue()
    await wrapper.vm.handleReject()
    expect(rejectUnAuditedQa).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
