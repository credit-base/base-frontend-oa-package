jest.mock('@/app-modules/business/services/praise')
jest.mock('@/utils/element')
import PraiseUnAuditedDetail from '@//app-modules/business/views/un-audited/praise/detail.vue'
import { openConfirm, openPrompt } from '@/utils/element'
import { getUnAuditedPraiseDetail, approveUnAuditedPraise, rejectUnAuditedPraise } from '@/app-modules/business/services/praise'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue PraiseUnAuditedDetail', () => {
  let wrapper
  // const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(PraiseUnAuditedDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedPraiseDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getUnAuditedPraiseDetail).toBeCalled()
  })

  it('Test methods handleApprove', async () => {
    openConfirm.mockResolvedValue()
    approveUnAuditedPraise.mockResolvedValue()
    await wrapper.vm.handleApprove()
    expect(approveUnAuditedPraise).toBeCalled()
  })

  it('Test methods handleReject', async () => {
    openPrompt.mockResolvedValue({ rejectReason: 'demos' })
    rejectUnAuditedPraise.mockResolvedValue()
    await wrapper.vm.handleReject()
    expect(rejectUnAuditedPraise).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
