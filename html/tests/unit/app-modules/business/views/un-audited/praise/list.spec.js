jest.mock('@/app-modules/business/services/praise')
import UnAuditedPraiseList from '@//app-modules/business/views/un-audited/praise/list.vue'

import { getUnAuditedPraiseList } from '@/app-modules/business/services/praise'

const { shallowMount, createComponentMocks } = global
const id = 'MA'
const store = {
  getters: {
    fullUserGroup: [],
    userGroup: {
      id,
      name: '',
    },
    superAdminPurview: false,
    platformAdminPurview: false,
  },
}

describe('Test vue UnAuditedPraiseList', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(UnAuditedPraiseList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', async () => {
    await wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedPraiseList.mockResolvedValue()
    await wrapper.vm.fetchData()
    await wrapper.vm.fetchData()
    expect(getUnAuditedPraiseList).toBeCalled()
  })

  it('Test methods generateQuery', () => {
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
        title: 'Test',
        sort: 'updateTime',
        acceptUserGroupId: 'MA',
      },
    })

    expect(wrapper.vm.generateQuery(true)).toStrictEqual({
      limit: 10,
      page: 1,
      title: 'Test',
      sort: 'updateTime',
      acceptUserGroupId: 'MA',
    })
  })

  it('Test methods searchData', async () => {
    getUnAuditedPraiseList.mockResolvedValue()
    await wrapper.vm.searchData()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods sortChange', () => {
    getUnAuditedPraiseList.mockResolvedValue()

    wrapper.vm.sortChange()
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
    })
    expect(getUnAuditedPraiseList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: 'updateTime',
    })
    expect(getUnAuditedPraiseList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: '-updateTime',
    })
    expect(getUnAuditedPraiseList).toBeCalled()
  })

  it('Test methods filterData', async () => {
    getUnAuditedPraiseList.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData()
    await wrapper.vm.generateQuery()
    expect(getUnAuditedPraiseList).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    const value = {
      command: 'AUDIT_VIEW',
      id,
    }
    wrapper.vm.handleCommand(value)
    expect(onRouterPush).toBeCalledWith({
      path: `/un-audited/praise/detail/${id}`,
      query: {
        route: '/un-audited/praise',
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
