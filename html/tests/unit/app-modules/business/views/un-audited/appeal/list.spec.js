jest.mock('@/app-modules/business/services/appeal')
import UnAuditedAppealList from '@//app-modules/business/views/un-audited/appeal/list.vue'

import { getUnAuditedAppealList } from '@/app-modules/business/services/appeal'

const { shallowMount, createComponentMocks } = global
const id = 'MA'
const store = {
  getters: {
    fullUserGroup: [],
    userGroup: {
      id,
      name: '',
    },
    superAdminPurview: false,
    platformAdminPurview: false,
  },
}

describe('Test vue UnAuditedAppealList', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(UnAuditedAppealList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', async () => {
    await wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedAppealList.mockResolvedValue()
    await wrapper.vm.fetchData()
    await wrapper.vm.fetchData()
    expect(getUnAuditedAppealList).toBeCalled()
  })

  it('Test methods generateQuery', () => {
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
        title: 'Test',
        sort: 'updateTime',
        acceptUserGroupId: 'MA',
      },
    })

    expect(wrapper.vm.generateQuery(true)).toStrictEqual({
      limit: 10,
      page: 1,
      title: 'Test',
      sort: 'updateTime',
      acceptUserGroupId: 'MA',
    })
  })

  it('Test methods searchData', async () => {
    getUnAuditedAppealList.mockResolvedValue()
    await wrapper.vm.searchData()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods sortChange', () => {
    getUnAuditedAppealList.mockResolvedValue()

    wrapper.vm.sortChange()
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
    })
    expect(getUnAuditedAppealList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: 'updateTime',
    })
    expect(getUnAuditedAppealList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: '-updateTime',
    })
    expect(getUnAuditedAppealList).toBeCalled()
  })

  it('Test methods filterData', async () => {
    getUnAuditedAppealList.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData()
    await wrapper.vm.generateQuery()
    expect(getUnAuditedAppealList).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    const value = {
      command: 'AUDIT_VIEW',
      id,
    }
    wrapper.vm.handleCommand(value)
    expect(onRouterPush).toBeCalledWith({
      path: `/un-audited/appeal/detail/${id}`,
      query: {
        route: '/un-audited/appeal',
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
