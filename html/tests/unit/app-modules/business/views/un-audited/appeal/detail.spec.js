jest.mock('@/app-modules/business/services/appeal')
jest.mock('@/utils/element')
import AppealUnAuditedDetail from '@//app-modules/business/views/un-audited/appeal/detail.vue'
import { openConfirm, openPrompt } from '@/utils/element'
import { getUnAuditedAppealDetail, approveUnAuditedAppeal, rejectUnAuditedAppeal } from '@/app-modules/business/services/appeal'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AppealUnAuditedDetail', () => {
  let wrapper
  // const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(AppealUnAuditedDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedAppealDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getUnAuditedAppealDetail).toBeCalled()
  })

  it('Test methods handleApprove', async () => {
    openConfirm.mockResolvedValue()
    approveUnAuditedAppeal.mockResolvedValue()
    await wrapper.vm.handleApprove()
    expect(approveUnAuditedAppeal).toBeCalled()
  })

  it('Test methods handleReject', async () => {
    openPrompt.mockResolvedValue({ rejectReason: 'demos' })
    rejectUnAuditedAppeal.mockResolvedValue()
    await wrapper.vm.handleReject()
    expect(rejectUnAuditedAppeal).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
