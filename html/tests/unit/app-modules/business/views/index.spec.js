import Business from '@/app-modules/business/views/index'

const { shallowMount, createComponentMocks } = global

describe('Test vue Business', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Business, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test Business snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
