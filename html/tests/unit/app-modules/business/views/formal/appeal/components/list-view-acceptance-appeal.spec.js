import ListViewAcceptanceAppeal from '@//app-modules/business/views/formal/appeal/components/list-view-acceptance-appeal.vue'

const { shallowMount, createComponentMocks } = global

describe('Test vue ListViewAcceptanceAppeal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ListViewAcceptanceAppeal, {
      ...createComponentMocks({

      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fnFilterChangeInit', () => {
    const filter = {
      applyStatus: ['Lw'],
    }
    wrapper.vm.fnFilterChangeInit(filter)
    expect(wrapper.emitted().filter).toStrictEqual([['Lw', 'applyStatus']])
  })

  it('Test methods sortChange', () => {
    // descending
    let column = {
      prop: 'updateTime',
      order: 'ascending',
    }

    wrapper.vm.sortChange(column)
    expect(wrapper.emitted().sort).toStrictEqual([['updateTime']])
    column = {
      prop: 'updateTime',
      order: 'descending',
    }

    wrapper.vm.sortChange(column)
    expect(wrapper.emitted().sort).toStrictEqual([['updateTime'], ['-updateTime']])
  })

  it('Test methods handleCommand', () => {
    const command = 'AUDIT_VIEW'
    const id = 'MA'

    wrapper.vm.handleCommand(command, { id })
    expect(wrapper.emitted().change).toStrictEqual([[{ command: 'AUDIT_VIEW', id: 'MA' }]])
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
