
jest.mock('@/app-modules/business/services/appeal')
import ReAcceptAppeal from '@//app-modules/business/views/formal/appeal/re-accept.vue'

import { getUnAuditedAppealDetail, resubmitAppeal } from '@/app-modules/business/services/appeal'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue ReAcceptAppeal', () => {
  let wrapper
  getUnAuditedAppealDetail.mockResolvedValue({})

  beforeEach(() => {
    wrapper = shallowMount(ReAcceptAppeal, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    await wrapper.vm.fetchData()
    expect(getUnAuditedAppealDetail).toBeCalled()
  })

  it('Test methods handleSubmit', async () => {
    resubmitAppeal.mockResolvedValue({ status: 1, data: {} })

    const value = {
      content: 'test',
      images: [{ name: 'demo.png', identify: 'demo.png' }],
      admissibility: 'Lw',
    }

    await wrapper.vm.handleSubmit(value)
    expect(resubmitAppeal).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
