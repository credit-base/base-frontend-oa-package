jest.mock('@/app-modules/business/services/appeal')
jest.mock('@/utils/element')
import FormalAppealDetail from '@//app-modules/business/views/formal/appeal/formal-detail.vue'

import { getAppealDetail } from '@/app-modules/business/services/appeal'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue FormalAppealDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormalAppealDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getAppealDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getAppealDetail).toBeCalled()
  })

  it('Test methods handleAccept', () => {
    wrapper.vm.handleAccept()
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/appeal/accept/${id}`,
      query: {
        route: `/formal/appeal/formal-detail/${id}`,
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
