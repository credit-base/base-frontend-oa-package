jest.mock('@/app-modules/business/services/appeal')
import AcceptanceAppealDetail from '@//app-modules/business/views/formal/appeal/acceptance-detail.vue'

import { getUnAuditedAppealDetail } from '@/app-modules/business/services/appeal'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AcceptanceAppealDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(AcceptanceAppealDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedAppealDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getUnAuditedAppealDetail).toBeCalled()
  })

  it('Test methods handleAccept', () => {
    wrapper.vm.handleAccept()
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/appeal/re-accept/${id}`,
      query: {
        route: `/formal/appeal/acceptance-detail/${id}`,
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
