jest.mock('@/app-modules/business/services/appeal')
jest.mock('@/utils/element')
import FormalAppealList from '@//app-modules/business/views/formal/appeal/list.vue'

import { BUSINESS_TABS } from '@/app-modules/business/helpers/constants'
import {
  getAppealList,
  getUnAuditedAppealList,
} from '@/app-modules/business/services/appeal'

const { shallowMount, createComponentMocks } = global
const id = 'MA'
const store = {
  getters: {
    fullUserGroup: [],
    userGroup: {
      id,
      name: '',
    },
    superAdminPurview: false,
    platformAdminPurview: false,
  },
}

describe('Test vue FormalAppealList', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormalAppealList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', async () => {
    await wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods fetchData', async () => {
    getAppealList.mockResolvedValue()
    getUnAuditedAppealList.mockResolvedValue()
    wrapper.setData({
      activeTab: BUSINESS_TABS[0].name,
    })
    await wrapper.vm.fetchData()
    expect(getAppealList).toBeCalled()
    wrapper.setData({
      activeTab: BUSINESS_TABS[1].name,
    })
    await wrapper.vm.fetchData()
    expect(getUnAuditedAppealList).toBeCalled()
  })

  it('Test methods generateQuery', () => {
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
        title: 'Test',
        sort: 'updateTime',
        acceptUserGroupId: 'MA',
      },
    })

    expect(wrapper.vm.generateQuery(true)).toStrictEqual({
      limit: 10,
      page: 1,
      title: 'Test',
      sort: 'updateTime',
      acceptUserGroupId: 'MA',
    })
  })

  it('Test methods changeTabs', async () => {
    getAppealList.mockResolvedValue()
    getUnAuditedAppealList.mockResolvedValue()

    await wrapper.vm.changeTabs()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(getAppealList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    getAppealList.mockResolvedValue()
    getUnAuditedAppealList.mockResolvedValue()
    await wrapper.vm.searchData()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(getAppealList).toBeCalled()
  })

  it('Test methods sortChange', () => {
    getAppealList.mockResolvedValue()
    getUnAuditedAppealList.mockResolvedValue()

    wrapper.vm.sortChange()
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
    })
    expect(getAppealList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: 'updateTime',
    })
    expect(getAppealList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: '-updateTime',
    })
    expect(getAppealList).toBeCalled()
  })

  it('Test methods filterData', async () => {
    getAppealList.mockResolvedValue()
    getUnAuditedAppealList.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData()
    await wrapper.vm.generateQuery()
    expect(getAppealList).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    await wrapper.vm.handleCommand({ command: 'VIEW', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/appeal/formal-detail/${id}`,
      query: {
        route: '/formal/appeal',
      },
    })

    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/appeal/acceptance-detail/${id}`,
      query: {
        route: `/formal/appeal?activeTab=${BUSINESS_TABS[1].name}`,
      },
    })

    await wrapper.vm.handleCommand({ command: 'ACCEPT', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/appeal/accept/${id}`,
      query: {
        route: '/formal/appeal',
      },
    })

    await wrapper.vm.handleCommand({ command: 'RE_ACCEPT', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/appeal/re-accept/${id}`,
      query: {
        route: `/formal/appeal?activeTab=${BUSINESS_TABS[1].name}`,
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
