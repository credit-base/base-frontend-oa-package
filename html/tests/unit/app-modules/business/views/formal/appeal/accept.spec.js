
jest.mock('@/app-modules/business/services/appeal')
import AcceptAppeal from '@//app-modules/business/views/formal/appeal/accept.vue'

import { acceptAppeal } from '@/app-modules/business/services/appeal'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AcceptAppeal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(AcceptAppeal, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleSubmit', async () => {
    acceptAppeal.mockResolvedValue({ status: 1, data: {} })

    const value = {
      content: 'test',
      images: [{ name: 'demo.png', identify: 'demo.png' }],
      admissibility: 'Lw',
    }

    await wrapper.vm.handleSubmit(value)
    expect(acceptAppeal).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
