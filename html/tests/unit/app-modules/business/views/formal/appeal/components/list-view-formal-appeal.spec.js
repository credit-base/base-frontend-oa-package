import ListViewFormalAppeal from '@//app-modules/business/views/formal/appeal/components/list-view-formal-appeal.vue'

const { shallowMount, createComponentMocks } = global

describe('Test vue ListViewFormalAppeal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ListViewFormalAppeal, {
      ...createComponentMocks({

      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fnFilterChangeInit', () => {
    let filter = {
      acceptStatus: ['Lw'],
    }
    wrapper.vm.fnFilterChangeInit(filter)
    expect(wrapper.emitted().filter).toStrictEqual([['Lw', 'acceptStatus']])
    filter = {
      status: ['Lw'],
    }
    wrapper.vm.fnFilterChangeInit(filter)
    expect(wrapper.emitted().filter).toStrictEqual([['Lw', 'acceptStatus'], ['Lw', 'status']])
  })

  it('Test methods sortChange', () => {
    // descending
    let column = {
      prop: 'updateTime',
      order: 'ascending',
    }

    wrapper.vm.sortChange(column)
    expect(wrapper.emitted().sort).toStrictEqual([['updateTime']])
    column = {
      prop: 'updateTime',
      order: 'descending',
    }

    wrapper.vm.sortChange(column)
    expect(wrapper.emitted().sort).toStrictEqual([['updateTime'], ['-updateTime']])
  })

  it('Test methods handleCommand', () => {
    const command = 'VIEW'
    const id = 'MA'

    wrapper.vm.handleCommand(command, { id })
    expect(wrapper.emitted().change).toStrictEqual([[{ command: 'VIEW', id: 'MA' }]])
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
