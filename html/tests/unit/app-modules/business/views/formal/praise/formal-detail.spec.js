jest.mock('@/app-modules/business/services/praise')
jest.mock('@/utils/element')
import FormalPraiseDetail from '@//app-modules/business/views/formal/praise/formal-detail.vue'

import { getPraiseDetail, publishPraise, unPublishPraise } from '@/app-modules/business/services/praise'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue FormalPraiseDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormalPraiseDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getPraiseDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getPraiseDetail).toBeCalled()
  })

  it('Test methods handleAccept', () => {
    wrapper.vm.handleAccept()
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/praise/accept/${id}`,
      query: {
        route: `/formal/praise/formal-detail/${id}`,
      },
    })
  })

  it('Test methods handlePublicity', async () => {
    publishPraise.mockResolvedValue()
    openConfirm.mockResolvedValue()
    await wrapper.vm.handlePublicity(id)
    expect(publishPraise).toBeCalled()
  })

  it('Test methods handleCancelPublicity', async () => {
    unPublishPraise.mockResolvedValue()
    openConfirm.mockResolvedValue()
    await wrapper.vm.handleCancelPublicity(id)
    expect(unPublishPraise).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
