jest.mock('@/app-modules/business/services/praise')
import AcceptancePraiseDetail from '@//app-modules/business/views/formal/praise/acceptance-detail.vue'

import { getUnAuditedPraiseDetail } from '@/app-modules/business/services/praise'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AcceptancePraiseDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(AcceptancePraiseDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedPraiseDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getUnAuditedPraiseDetail).toBeCalled()
  })

  it('Test methods handleAccept', () => {
    wrapper.vm.handleAccept()
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/praise/re-accept/${id}`,
      query: {
        route: `/formal/praise/acceptance-detail/${id}`,
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
