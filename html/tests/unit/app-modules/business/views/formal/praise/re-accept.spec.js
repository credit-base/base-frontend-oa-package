
jest.mock('@/app-modules/business/services/praise')
import ReAcceptPraise from '@//app-modules/business/views/formal/praise/re-accept.vue'

import { getUnAuditedPraiseDetail, resubmitPraise } from '@/app-modules/business/services/praise'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue ReAcceptPraise', () => {
  let wrapper
  getUnAuditedPraiseDetail.mockResolvedValue({})

  beforeEach(() => {
    wrapper = shallowMount(ReAcceptPraise, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    await wrapper.vm.fetchData()
    expect(getUnAuditedPraiseDetail).toBeCalled()
  })

  it('Test methods handleSubmit', async () => {
    resubmitPraise.mockResolvedValue({ status: 1, data: {} })

    const value = {
      content: 'test',
      images: [{ name: 'demo.png', identify: 'demo.png' }],
      admissibility: 'Lw',
    }

    await wrapper.vm.handleSubmit(value)
    expect(resubmitPraise).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
