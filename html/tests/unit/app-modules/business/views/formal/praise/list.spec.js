jest.mock('@/app-modules/business/services/praise')
jest.mock('@/utils/element')
import FormalPraiseList from '@//app-modules/business/views/formal/praise/list.vue'

import { BUSINESS_TABS } from '@/app-modules/business/helpers/constants'
import {
  getPraiseList,
  getUnAuditedPraiseList,
  publishPraise,
  unPublishPraise,
} from '@/app-modules/business/services/praise'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = 'MA'
const store = {
  getters: {
    fullUserGroup: [],
    userGroup: {
      id,
      name: '',
    },
    superAdminPurview: false,
    platformAdminPurview: false,
  },
}

describe('Test vue FormalPraiseList', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormalPraiseList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', async () => {
    await wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods fetchData', async () => {
    getPraiseList.mockResolvedValue()
    getUnAuditedPraiseList.mockResolvedValue()
    wrapper.setData({
      activeTab: BUSINESS_TABS[0].name,
    })
    await wrapper.vm.fetchData()
    expect(getPraiseList).toBeCalled()
    wrapper.setData({
      activeTab: BUSINESS_TABS[1].name,
    })
    await wrapper.vm.fetchData()
    expect(getUnAuditedPraiseList).toBeCalled()
  })

  it('Test methods generateQuery', () => {
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
        title: 'Test',
        sort: 'updateTime',
        acceptUserGroupId: 'MA',
      },
    })

    expect(wrapper.vm.generateQuery(true)).toStrictEqual({
      limit: 10,
      page: 1,
      title: 'Test',
      sort: 'updateTime',
      acceptUserGroupId: 'MA',
    })
  })

  it('Test methods changeTabs', async () => {
    getPraiseList.mockResolvedValue()
    getUnAuditedPraiseList.mockResolvedValue()

    await wrapper.vm.changeTabs()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(getPraiseList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    getPraiseList.mockResolvedValue()
    getUnAuditedPraiseList.mockResolvedValue()
    await wrapper.vm.searchData()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(getPraiseList).toBeCalled()
  })

  it('Test methods sortChange', () => {
    getPraiseList.mockResolvedValue()
    getUnAuditedPraiseList.mockResolvedValue()

    wrapper.vm.sortChange()
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
    })
    expect(getPraiseList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: 'updateTime',
    })
    expect(getPraiseList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: '-updateTime',
    })
    expect(getPraiseList).toBeCalled()
  })

  it('Test methods filterData', async () => {
    getPraiseList.mockResolvedValue()
    getUnAuditedPraiseList.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData()
    await wrapper.vm.generateQuery()
    expect(getPraiseList).toBeCalled()
  })

  it('Test methods handlePublicity', async () => {
    publishPraise.mockResolvedValue()
    openConfirm.mockResolvedValue()
    await wrapper.vm.handlePublicity(id)
    expect(publishPraise).toBeCalled()
  })

  it('Test methods handleCancelPublicity', async () => {
    unPublishPraise.mockResolvedValue()
    openConfirm.mockResolvedValue()
    await wrapper.vm.handleCancelPublicity(id)
    expect(unPublishPraise).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    openConfirm.mockResolvedValue()
    publishPraise.mockResolvedValue()
    unPublishPraise.mockResolvedValue()

    await wrapper.vm.handleCommand({ command: 'VIEW', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/praise/formal-detail/${id}`,
      query: {
        route: '/formal/praise',
      },
    })

    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/praise/acceptance-detail/${id}`,
      query: {
        route: `/formal/praise?activeTab=${BUSINESS_TABS[1].name}`,
      },
    })

    await wrapper.vm.handleCommand({ command: 'ACCEPT', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/praise/accept/${id}`,
      query: {
        route: '/formal/praise',
      },
    })

    await wrapper.vm.handleCommand({ command: 'RE_ACCEPT', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/praise/re-accept/${id}`,
      query: {
        route: `/formal/praise?activeTab=${BUSINESS_TABS[1].name}`,
      },
    })

    await wrapper.vm.handleCommand({ command: 'PUBLICITY', id })
    expect(publishPraise).toBeCalled()

    await wrapper.vm.handleCommand({ command: 'CANCEL_PUBLICITY', id })
    expect(unPublishPraise).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
