
jest.mock('@/app-modules/business/services/praise')
import AcceptPraise from '@//app-modules/business/views/formal/praise/accept.vue'

import { acceptPraise } from '@/app-modules/business/services/praise'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AcceptPraise', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(AcceptPraise, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleSubmit', async () => {
    acceptPraise.mockResolvedValue({ status: 1, data: {} })

    const value = {
      content: 'test',
      images: [{ name: 'demo.png', identify: 'demo.png' }],
      admissibility: 'Lw',
    }

    await wrapper.vm.handleSubmit(value)
    expect(acceptPraise).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
