
jest.mock('@/app-modules/business/services/feedback')
import AcceptFeedback from '@//app-modules/business/views/formal/feedback/accept.vue'

import { acceptFeedback } from '@/app-modules/business/services/feedback'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AcceptFeedback', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(AcceptFeedback, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleSubmit', async () => {
    acceptFeedback.mockResolvedValue({ status: 1, data: {} })

    const value = {
      content: 'test',
      images: [{ name: 'demo.png', identify: 'demo.png' }],
      admissibility: 'Lw',
    }

    await wrapper.vm.handleSubmit(value)
    expect(acceptFeedback).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
