
jest.mock('@/app-modules/business/services/feedback')
import ReAcceptFeedback from '@//app-modules/business/views/formal/feedback/re-accept.vue'

import { getUnAuditedFeedbackDetail, resubmitFeedback } from '@/app-modules/business/services/feedback'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue ReAcceptFeedback', () => {
  let wrapper
  getUnAuditedFeedbackDetail.mockResolvedValue({})

  beforeEach(() => {
    wrapper = shallowMount(ReAcceptFeedback, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    await wrapper.vm.fetchData()
    expect(getUnAuditedFeedbackDetail).toBeCalled()
  })

  it('Test methods handleSubmit', async () => {
    resubmitFeedback.mockResolvedValue({ status: 1, data: {} })

    const value = {
      content: 'test',
      images: [{ name: 'demo.png', identify: 'demo.png' }],
      admissibility: 'Lw',
    }

    await wrapper.vm.handleSubmit(value)
    expect(resubmitFeedback).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
