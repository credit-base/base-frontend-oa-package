jest.mock('@/app-modules/business/services/feedback')
jest.mock('@/utils/element')
import FormalFeedbackDetail from '@//app-modules/business/views/formal/feedback/formal-detail.vue'

import { getFeedbackDetail } from '@/app-modules/business/services/feedback'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue FormalFeedbackDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormalFeedbackDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getFeedbackDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getFeedbackDetail).toBeCalled()
  })

  it('Test methods handleAccept', () => {
    wrapper.vm.handleAccept()
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/feedback/accept/${id}`,
      query: {
        route: `/formal/feedback/formal-detail/${id}`,
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
