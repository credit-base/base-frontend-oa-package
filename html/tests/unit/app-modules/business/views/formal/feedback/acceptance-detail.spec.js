jest.mock('@/app-modules/business/services/feedback')
import AcceptanceFeedbackDetail from '@//app-modules/business/views/formal/feedback/acceptance-detail.vue'

import { getUnAuditedFeedbackDetail } from '@/app-modules/business/services/feedback'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AcceptanceFeedbackDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(AcceptanceFeedbackDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedFeedbackDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getUnAuditedFeedbackDetail).toBeCalled()
  })

  it('Test methods handleAccept', () => {
    wrapper.vm.handleAccept()
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/feedback/re-accept/${id}`,
      query: {
        route: `/formal/feedback/acceptance-detail/${id}`,
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
