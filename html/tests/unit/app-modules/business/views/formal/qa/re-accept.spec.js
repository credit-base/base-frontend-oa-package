
jest.mock('@/app-modules/business/services/qa')
import ReAcceptQa from '@//app-modules/business/views/formal/qa/re-accept.vue'

import { getUnAuditedQaDetail, resubmitQa } from '@/app-modules/business/services/qa'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue ReAcceptQa', () => {
  let wrapper
  getUnAuditedQaDetail.mockResolvedValue({})

  beforeEach(() => {
    wrapper = shallowMount(ReAcceptQa, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    await wrapper.vm.fetchData()
    expect(getUnAuditedQaDetail).toBeCalled()
  })

  it('Test methods handleSubmit', async () => {
    resubmitQa.mockResolvedValue({ status: 1, data: {} })

    const value = {
      content: 'test',
      images: [{ name: 'demo.png', identify: 'demo.png' }],
      admissibility: 'Lw',
    }

    await wrapper.vm.handleSubmit(value)
    expect(resubmitQa).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
