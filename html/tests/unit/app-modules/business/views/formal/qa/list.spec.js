jest.mock('@/app-modules/business/services/qa')
jest.mock('@/utils/element')
import FormalQaList from '@//app-modules/business/views/formal/qa/list.vue'

import { BUSINESS_TABS } from '@/app-modules/business/helpers/constants'
import {
  getQaList,
  getUnAuditedQaList,
  publishQa,
  unPublishQa,
} from '@/app-modules/business/services/qa'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = 'MA'
const store = {
  getters: {
    fullUserGroup: [],
    userGroup: {
      id,
      name: '',
    },
    superAdminPurview: false,
    platformAdminPurview: false,
  },
}

describe('Test vue FormalQaList', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormalQaList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', async () => {
    await wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods fetchData', async () => {
    getQaList.mockResolvedValue()
    getUnAuditedQaList.mockResolvedValue()
    wrapper.setData({
      activeTab: BUSINESS_TABS[0].name,
    })
    await wrapper.vm.fetchData()
    expect(getQaList).toBeCalled()
    wrapper.setData({
      activeTab: BUSINESS_TABS[1].name,
    })
    await wrapper.vm.fetchData()
    expect(getUnAuditedQaList).toBeCalled()
  })

  it('Test methods generateQuery', () => {
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
        title: 'Test',
        sort: 'updateTime',
        acceptUserGroupId: 'MA',
      },
    })

    expect(wrapper.vm.generateQuery(true)).toStrictEqual({
      limit: 10,
      page: 1,
      title: 'Test',
      sort: 'updateTime',
      acceptUserGroupId: 'MA',
    })
  })

  it('Test methods changeTabs', async () => {
    getQaList.mockResolvedValue()
    getUnAuditedQaList.mockResolvedValue()

    await wrapper.vm.changeTabs()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(getQaList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    getQaList.mockResolvedValue()
    getUnAuditedQaList.mockResolvedValue()
    await wrapper.vm.searchData()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(getQaList).toBeCalled()
  })

  it('Test methods sortChange', () => {
    getQaList.mockResolvedValue()
    getUnAuditedQaList.mockResolvedValue()

    wrapper.vm.sortChange()
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
    })
    expect(getQaList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: 'updateTime',
    })
    expect(getQaList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: '-updateTime',
    })
    expect(getQaList).toBeCalled()
  })

  it('Test methods filterData', async () => {
    getQaList.mockResolvedValue()
    getUnAuditedQaList.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData()
    await wrapper.vm.generateQuery()
    expect(getQaList).toBeCalled()
  })

  it('Test methods handlePublicity', async () => {
    publishQa.mockResolvedValue()
    openConfirm.mockResolvedValue()
    await wrapper.vm.handlePublicity(id)
    expect(publishQa).toBeCalled()
  })

  it('Test methods handleCancelPublicity', async () => {
    unPublishQa.mockResolvedValue()
    openConfirm.mockResolvedValue()
    await wrapper.vm.handleCancelPublicity(id)
    expect(unPublishQa).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    openConfirm.mockResolvedValue()
    publishQa.mockResolvedValue()
    unPublishQa.mockResolvedValue()

    await wrapper.vm.handleCommand({ command: 'VIEW', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/qa/formal-detail/${id}`,
      query: {
        route: '/formal/qa',
      },
    })

    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/qa/acceptance-detail/${id}`,
      query: {
        route: `/formal/qa?activeTab=${BUSINESS_TABS[1].name}`,
      },
    })

    await wrapper.vm.handleCommand({ command: 'ACCEPT', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/qa/accept/${id}`,
      query: {
        route: '/formal/qa',
      },
    })

    await wrapper.vm.handleCommand({ command: 'RE_ACCEPT', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/qa/re-accept/${id}`,
      query: {
        route: `/formal/qa?activeTab=${BUSINESS_TABS[1].name}`,
      },
    })

    await wrapper.vm.handleCommand({ command: 'PUBLICITY', id })
    expect(publishQa).toBeCalled()

    await wrapper.vm.handleCommand({ command: 'CANCEL_PUBLICITY', id })
    expect(unPublishQa).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
