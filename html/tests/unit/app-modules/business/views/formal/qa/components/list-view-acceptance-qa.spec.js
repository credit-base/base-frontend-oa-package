import ListViewAcceptanceQa from '@//app-modules/business/views/formal/qa/components/list-view-acceptance-qa.vue'

const { shallowMount, createComponentMocks } = global

describe('Test vue ListViewAcceptanceQa', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ListViewAcceptanceQa, {
      ...createComponentMocks({

      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fnFilterChangeInit', () => {
    const filter = {
      acceptStatus: ['Lw'],
    }
    wrapper.vm.fnFilterChangeInit(filter)
    expect(wrapper.emitted().filter).toStrictEqual([['Lw', 'acceptStatus']])
  })

  it('Test methods sortChange', () => {
    // descending
    let column = {
      prop: 'updateTime',
      order: 'ascending',
    }

    wrapper.vm.sortChange(column)
    expect(wrapper.emitted().sort).toStrictEqual([['updateTime']])
    column = {
      prop: 'updateTime',
      order: 'descending',
    }

    wrapper.vm.sortChange(column)
    expect(wrapper.emitted().sort).toStrictEqual([['updateTime'], ['-updateTime']])
  })

  it('Test methods handleCommand', () => {
    const command = 'AUDIT_VIEW'
    const id = 'MA'

    wrapper.vm.handleCommand(command, { id })
    expect(wrapper.emitted().change).toStrictEqual([[{ command: 'AUDIT_VIEW', id: 'MA' }]])
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
