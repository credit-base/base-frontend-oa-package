
jest.mock('@/app-modules/business/services/qa')
import AcceptQa from '@//app-modules/business/views/formal/qa/accept.vue'

import { acceptQa } from '@/app-modules/business/services/qa'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AcceptQa', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(AcceptQa, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleSubmit', async () => {
    acceptQa.mockResolvedValue({ status: 1, data: {} })

    const value = {
      content: 'test',
      images: [{ name: 'demo.png', identify: 'demo.png' }],
      admissibility: 'Lw',
    }

    await wrapper.vm.handleSubmit(value)
    expect(acceptQa).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
