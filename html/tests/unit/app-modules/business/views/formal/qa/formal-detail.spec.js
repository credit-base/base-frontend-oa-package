jest.mock('@/app-modules/business/services/qa')
jest.mock('@/utils/element')
import FormalQaDetail from '@//app-modules/business/views/formal/qa/formal-detail.vue'

import { getQaDetail, publishQa, unPublishQa } from '@/app-modules/business/services/qa'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue FormalQaDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormalQaDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getQaDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getQaDetail).toBeCalled()
  })

  it('Test methods handleAccept', () => {
    wrapper.vm.handleAccept()
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/qa/accept/${id}`,
      query: {
        route: `/formal/qa/formal-detail/${id}`,
      },
    })
  })

  it('Test methods handlePublicity', async () => {
    publishQa.mockResolvedValue()
    openConfirm.mockResolvedValue()
    await wrapper.vm.handlePublicity(id)
    expect(publishQa).toBeCalled()
  })

  it('Test methods handleCancelPublicity', async () => {
    unPublishQa.mockResolvedValue()
    openConfirm.mockResolvedValue()
    await wrapper.vm.handleCancelPublicity(id)
    expect(unPublishQa).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
