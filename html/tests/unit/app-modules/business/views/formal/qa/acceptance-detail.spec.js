jest.mock('@/app-modules/business/services/qa')
import AcceptanceQaDetail from '@//app-modules/business/views/formal/qa/acceptance-detail.vue'

import { getUnAuditedQaDetail } from '@/app-modules/business/services/qa'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AcceptanceQaDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(AcceptanceQaDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedQaDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getUnAuditedQaDetail).toBeCalled()
  })

  it('Test methods handleAccept', () => {
    wrapper.vm.handleAccept()
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/qa/re-accept/${id}`,
      query: {
        route: `/formal/qa/acceptance-detail/${id}`,
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
