jest.mock('@/app-modules/business/services/complaint')
import AcceptanceComplaintDetail from '@//app-modules/business/views/formal/complaint/acceptance-detail.vue'

import { getUnAuditedComplaintDetail } from '@/app-modules/business/services/complaint'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AcceptanceComplaintDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(AcceptanceComplaintDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getUnAuditedComplaintDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getUnAuditedComplaintDetail).toBeCalled()
  })

  it('Test methods handleAccept', () => {
    wrapper.vm.handleAccept()
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/complaint/re-accept/${id}`,
      query: {
        route: `/formal/complaint/acceptance-detail/${id}`,
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
