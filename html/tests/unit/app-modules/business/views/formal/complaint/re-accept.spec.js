
jest.mock('@/app-modules/business/services/complaint')
import ReAcceptComplaint from '@//app-modules/business/views/formal/complaint/re-accept.vue'

import { getUnAuditedComplaintDetail, resubmitComplaint } from '@/app-modules/business/services/complaint'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue ReAcceptComplaint', () => {
  let wrapper
  getUnAuditedComplaintDetail.mockResolvedValue({})

  beforeEach(() => {
    wrapper = shallowMount(ReAcceptComplaint, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    await wrapper.vm.fetchData()
    expect(getUnAuditedComplaintDetail).toBeCalled()
  })

  it('Test methods handleSubmit', async () => {
    resubmitComplaint.mockResolvedValue({ status: 1, data: {} })

    const value = {
      content: 'test',
      images: [{ name: 'demo.png', identify: 'demo.png' }],
      admissibility: 'Lw',
    }

    await wrapper.vm.handleSubmit(value)
    expect(resubmitComplaint).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
