jest.mock('@/app-modules/business/services/complaint')
jest.mock('@/utils/element')
import FormalComplaintList from '@//app-modules/business/views/formal/complaint/list.vue'

import { BUSINESS_TABS } from '@/app-modules/business/helpers/constants'
import {
  getComplaintList,
  getUnAuditedComplaintList,
} from '@/app-modules/business/services/complaint'

const { shallowMount, createComponentMocks } = global
const id = 'MA'
const store = {
  getters: {
    fullUserGroup: [],
    userGroup: {
      id,
      name: '',
    },
    superAdminPurview: false,
    platformAdminPurview: false,
  },
}

describe('Test vue FormalComplaintList', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormalComplaintList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', async () => {
    await wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods fetchData', async () => {
    getComplaintList.mockResolvedValue()
    getUnAuditedComplaintList.mockResolvedValue()
    wrapper.setData({
      activeTab: BUSINESS_TABS[0].name,
    })
    await wrapper.vm.fetchData()
    expect(getComplaintList).toBeCalled()
    wrapper.setData({
      activeTab: BUSINESS_TABS[1].name,
    })
    await wrapper.vm.fetchData()
    expect(getUnAuditedComplaintList).toBeCalled()
  })

  it('Test methods generateQuery', () => {
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
        title: 'Test',
        sort: 'updateTime',
        acceptUserGroupId: 'MA',
      },
    })

    expect(wrapper.vm.generateQuery(true)).toStrictEqual({
      limit: 10,
      page: 1,
      title: 'Test',
      sort: 'updateTime',
      acceptUserGroupId: 'MA',
    })
  })

  it('Test methods changeTabs', async () => {
    getComplaintList.mockResolvedValue()
    getUnAuditedComplaintList.mockResolvedValue()

    await wrapper.vm.changeTabs()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(getComplaintList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    getComplaintList.mockResolvedValue()
    getUnAuditedComplaintList.mockResolvedValue()
    await wrapper.vm.searchData()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(getComplaintList).toBeCalled()
  })

  it('Test methods sortChange', () => {
    getComplaintList.mockResolvedValue()
    getUnAuditedComplaintList.mockResolvedValue()

    wrapper.vm.sortChange()
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
    })
    expect(getComplaintList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: 'updateTime',
    })
    expect(getComplaintList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      acceptUserGroupId: 'MA',
      sort: '-updateTime',
    })
    expect(getComplaintList).toBeCalled()
  })

  it('Test methods filterData', async () => {
    getComplaintList.mockResolvedValue()
    getUnAuditedComplaintList.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData()
    await wrapper.vm.generateQuery()
    expect(getComplaintList).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    await wrapper.vm.handleCommand({ command: 'VIEW', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/complaint/formal-detail/${id}`,
      query: {
        route: '/formal/complaint',
      },
    })

    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/complaint/acceptance-detail/${id}`,
      query: {
        route: `/formal/complaint?activeTab=${BUSINESS_TABS[1].name}`,
      },
    })

    await wrapper.vm.handleCommand({ command: 'ACCEPT', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/complaint/accept/${id}`,
      query: {
        route: '/formal/complaint',
      },
    })

    await wrapper.vm.handleCommand({ command: 'RE_ACCEPT', id })
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/complaint/re-accept/${id}`,
      query: {
        route: `/formal/complaint?activeTab=${BUSINESS_TABS[1].name}`,
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
