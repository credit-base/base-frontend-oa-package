
jest.mock('@/app-modules/business/services/complaint')
import AcceptComplaint from '@//app-modules/business/views/formal/complaint/accept.vue'

import { acceptComplaint } from '@/app-modules/business/services/complaint'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue AcceptComplaint', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(AcceptComplaint, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleSubmit', async () => {
    acceptComplaint.mockResolvedValue({ status: 1, data: {} })

    const value = {
      content: 'test',
      images: [{ name: 'demo.png', identify: 'demo.png' }],
      admissibility: 'Lw',
    }

    await wrapper.vm.handleSubmit(value)
    expect(acceptComplaint).toBeCalled()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
