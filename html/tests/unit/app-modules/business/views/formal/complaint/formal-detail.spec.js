jest.mock('@/app-modules/business/services/complaint')
jest.mock('@/utils/element')
import FormalComplaintDetail from '@//app-modules/business/views/formal/complaint/formal-detail.vue'

import { getComplaintDetail } from '@/app-modules/business/services/complaint'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue FormalComplaintDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormalComplaintDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route: '/',
            },
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    getComplaintDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(getComplaintDetail).toBeCalled()
  })

  it('Test methods handleAccept', () => {
    wrapper.vm.handleAccept()
    expect(onRouterPush).toBeCalledWith({
      path: `/formal/complaint/accept/${id}`,
      query: {
        route: `/formal/complaint/formal-detail/${id}`,
      },
    })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
