jest.mock('@/utils/element')
jest.mock('@/app-modules/credit-photography/services/credit-photography')
import { openConfirm, openPrompt } from '@/utils/element'
import CreditPhotographyAuditDetail from '@/app-modules/credit-photography/views/detail/audit-detail'
import { fetchCreditPhotographyDetail, updateApproveCreditPhotography, updateRejectCreditPhotography } from '@/app-modules/credit-photography/services/credit-photography'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue CreditPhotographyAuditDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(CreditPhotographyAuditDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            params: {
              id,
            },
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchCreditPhotographyDetail', async () => {
    fetchCreditPhotographyDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(fetchCreditPhotographyDetail).toBeCalled()
  })

  it('Test methods handleCommand - updateRejectCreditPhotography updateApproveCreditPhotography updateRejectCreditPhotography', async () => {
    const res = {
      value: 'rejectReason',
    }
    openConfirm.mockResolvedValue()
    fetchCreditPhotographyDetail.mockResolvedValue()
    updateRejectCreditPhotography.mockResolvedValue()
    updateApproveCreditPhotography.mockResolvedValue()
    openPrompt.mockResolvedValue(res)

    await wrapper.vm.handleCommand('APPROVE', id)
    expect(updateApproveCreditPhotography).toBeCalledWith(id)
    expect(fetchCreditPhotographyDetail).toBeCalled()

    await wrapper.vm.handleCommand('REJECT', id)
    expect(updateRejectCreditPhotography).toBeCalledWith(id, { rejectReason: 'rejectReason' })
    expect(fetchCreditPhotographyDetail).toBeCalled()
  })

  it('Test CreditPhotographyAuditDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
