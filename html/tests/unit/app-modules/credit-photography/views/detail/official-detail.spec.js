jest.mock('@/app-modules/credit-photography/services/credit-photography')
import CreditPhotographyOfficialDetail from '@/app-modules/credit-photography/views/detail/official-detail'
import { fetchCreditPhotographyDetail } from '@/app-modules/credit-photography/services/credit-photography'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue CreditPhotographyOfficialDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(CreditPhotographyOfficialDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            params: {
              id,
            },
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchCreditPhotographyDetail', async () => {
    fetchCreditPhotographyDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(fetchCreditPhotographyDetail).toBeCalled()
  })

  it('Test CreditPhotographyOfficialDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
