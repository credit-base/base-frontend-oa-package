jest.mock('@/app-modules/credit-photography/services/credit-photography')
import CreditPhotographyList from '@/app-modules/credit-photography/views/list'
import { fetchCreditPhotographyList } from '@/app-modules/credit-photography/services/credit-photography'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue CreditPhotographyList', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(CreditPhotographyList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => [
    wrapper && wrapper.destroy(),
  ])

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', () => {
    wrapper.vm.initQuery()
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
    })
  })

  it('Test methods fetchData - fetchCreditPhotographyList', async () => {
    fetchCreditPhotographyList.mockResolvedValue()
    await wrapper.vm.fetchData()
    const params = {
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
    }
    expect(fetchCreditPhotographyList).toBeCalledWith(params)
  })

  it('Test methods changeTabs', async () => {
    fetchCreditPhotographyList.mockResolvedValue()
    await wrapper.vm.changeTabs()
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
    })
    fetchCreditPhotographyList.mockResolvedValue()
    const params = {
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
    }
    expect(fetchCreditPhotographyList).toBeCalledWith(params)
  })

  it('Test methods sortChange', async () => {
    fetchCreditPhotographyList.mockResolvedValue()
    await wrapper.vm.sortChange('updateTime')
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
      sort: 'updateTime',
    })
    expect(fetchCreditPhotographyList).toBeCalledWith({
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
      sort: 'updateTime',
    })
    await wrapper.vm.sortChange('-updateTime')
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
      sort: '-updateTime',
    })
    expect(fetchCreditPhotographyList).toBeCalledWith({
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
      sort: '-updateTime',
    })
    await wrapper.vm.sortChange('')
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
      sort: '',
    })
    expect(fetchCreditPhotographyList).toBeCalledWith({
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
    })
  })

  it('Test methods filterData', async () => {
    fetchCreditPhotographyList.mockResolvedValue()
    await wrapper.vm.filterData('Lw', 'applyStatus')
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      applyStatus: 'MQ',
    })
    expect(fetchCreditPhotographyList).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    await wrapper.vm.handleCommand({ command: 'VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/credit-photography/official-detail/${id}`, query: { route: '/credit-photography/list' } })
    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/credit-photography/audit-detail/${id}`, query: { route: `/credit-photography/list?activeTab=audit-credit-photography` } })
  })

  it('Test CreditPhotographyList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
