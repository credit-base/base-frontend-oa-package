jest.mock('@/utils/request')
import request from '@/utils/request'

import {
  fetchCreditPhotographyList,
  fetchCreditPhotographyDetail,
  updateApproveCreditPhotography,
  updateRejectCreditPhotography,
} from '@/app-modules/credit-photography/services/credit-photography'

import {
  CREDIT_PHOTOGRAPHY_LIST,
  CREDIT_PHOTOGRAPHY_VIDEO_DETAIL,
} from '@/app-modules/credit-photography/helpers/mocks'

const params = {}
const id = 'MA'
const useMock = { useMock: true }

describe('Test credit-photography services', () => {
  it('test services fetchCreditPhotographyList', () => {
    fetchCreditPhotographyList()
    expect(request).toBeCalledWith('/api/creditPhotography', { method: 'GET', params })
    fetchCreditPhotographyList(params)
    expect(request).toBeCalledWith('/api/creditPhotography', { method: 'GET', params })
    expect(fetchCreditPhotographyList(useMock)).toStrictEqual({
      total: 4,
      list: [
        ...CREDIT_PHOTOGRAPHY_LIST,
      ],
    })
  })
  it('test services fetchCreditPhotographyDetail', () => {
    fetchCreditPhotographyDetail(id)
    expect(request).toBeCalledWith(`/api/creditPhotography/${id}`, { method: 'GET' })
    expect(fetchCreditPhotographyDetail(id, useMock)).toStrictEqual({
      ...CREDIT_PHOTOGRAPHY_VIDEO_DETAIL,
    })
  })
  it('test services updateApproveCreditPhotography', () => {
    updateApproveCreditPhotography(id)
    expect(request).toBeCalledWith(`/api/creditPhotography/${id}/approve`, { method: 'POST' })
  })
  it('test services updateRejectCreditPhotography', () => {
    updateRejectCreditPhotography(id)
    expect(request).toBeCalledWith(`/api/creditPhotography/${id}/reject`, { method: 'POST', params })
    updateRejectCreditPhotography(id, params)
    expect(request).toBeCalledWith(`/api/creditPhotography/${id}/reject`, { method: 'POST', params })
  })
})
