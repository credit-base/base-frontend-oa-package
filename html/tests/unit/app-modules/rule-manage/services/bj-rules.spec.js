/**
 * @file 国标规则管理 - 接口
 * @module rule-manage/bj
 */

jest.mock('@/utils/request')
import request from '@/utils/request'
import {
  fetchBjTemplateList,
  fetchBjRulesList,
  fetchBjRulesDetail,
  createBjRules,
  fetchEditBjRules,
  updateEditBjRules,
  deleteBjRules,
  fetchUnAuditedBjRules,
  fetchUnAuditedBjRulesDetail,
  fetchUnAuditedEditBjRules,
  updateUnAuditedEditBjRules,
  revokeUnAuditedBjRules,
  approveUnAuditedBjRules,
  rejectUnAuditedBjRules,
  fetchAllBjTemplates,
  fetchAllWbjTemplates,
} from '@/app-modules/rule-manage/services/bj-rules'
import { gbRuleDetail } from '@/app-modules/rule-manage/helpers/mocks'

const id = global.randomString()
const params = {}
const useMock = { useMock: true }

describe(`Services - rule-manage - bj`, () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it(`fetchBjTemplateList`, () => {
    fetchBjTemplateList()
    expect(request).toBeCalledWith(`/api/bjRule/bjTemplates`, { params })
    expect(fetchBjTemplateList(useMock)).toHaveProperty(`total`)
    expect(fetchBjTemplateList(useMock)).toHaveProperty(`list`)
  })

  it(`fetchBjRulesList`, () => {
    fetchBjRulesList()
    expect(request).toBeCalledWith(`/api/bjRules`, { params })
    expect(fetchBjRulesList(useMock)).toHaveProperty(`total`)
    expect(fetchBjRulesList(useMock)).toHaveProperty(`list`)
  })

  it(`fetchBjRulesDetail`, () => {
    fetchBjRulesDetail(id)
    expect(request).toBeCalledWith(`/api/bjRules/${id}`)
    expect(fetchBjRulesDetail(id, useMock)).toStrictEqual({
      ...gbRuleDetail,
    })
  })

  it('createBjRules', () => {
    createBjRules(params)
    expect(request).toBeCalledWith(`/api/bjRules/add`, { method: 'POST', params })
  })

  it(`fetchEditBjRules`, () => {
    fetchEditBjRules(id)
    expect(request).toBeCalledWith(`/api/bjRules/${id}`)
    expect(fetchEditBjRules(id, useMock)).toStrictEqual({
      ...gbRuleDetail,
    })
  })

  it('updateEditBjRules', () => {
    updateEditBjRules(id, params)
    expect(request).toBeCalledWith(`/api/bjRules/${id}/edit`, { method: 'POST', params })
  })

  it('deleteBjRules', () => {
    deleteBjRules(id)
    expect(request).toBeCalledWith(`/api/bjRules/${id}/delete`, { method: 'POST' })
  })

  it(`fetchUnAuditedBjRules`, () => {
    fetchUnAuditedBjRules()
    expect(request).toBeCalledWith(`/api/unAuditedBjRules`, { params })
    expect(fetchUnAuditedBjRules(useMock)).toHaveProperty('list')
    expect(fetchUnAuditedBjRules(useMock)).toHaveProperty('total')
  })

  it(`fetchUnAuditedBjRulesDetail`, () => {
    fetchUnAuditedBjRulesDetail(id)
    expect(request).toBeCalledWith(`/api/unAuditedBjRules/${id}`)
    expect(fetchUnAuditedBjRulesDetail(id, useMock)).toStrictEqual({
      ...gbRuleDetail,
    })
  })

  it('fetchUnAuditedEditBjRules', () => {
    fetchUnAuditedEditBjRules(id)
    expect(request).toBeCalledWith(`/api/unAuditedBjRules/${id}`)
    expect(fetchUnAuditedEditBjRules(id, useMock)).toStrictEqual({
      ...gbRuleDetail,
    })
  })

  it('updateUnAuditedEditBjRules', () => {
    updateUnAuditedEditBjRules(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedBjRules/${id}/edit`, { method: 'POST', params })
  })

  it(`revokeUnAuditedBjRules`, () => {
    revokeUnAuditedBjRules(id)
    expect(request).toBeCalledWith(`/api/unAuditedBjRules/${id}/revoke`, { method: 'POST' })
  })

  it(`approveUnAuditedBjRules`, () => {
    approveUnAuditedBjRules(id)
    expect(request).toBeCalledWith(`/api/unAuditedBjRules/${id}/approve`, { method: 'POST' })
  })

  it(`rejectUnAuditedBjRules`, () => {
    rejectUnAuditedBjRules(id)
    expect(request).toBeCalledWith(`/api/unAuditedBjRules/${id}/reject`, { method: 'POST', params })
  })

  it(`fetchAllBjTemplates`, () => {
    const params = {
      limit: 10000,
      page: 1,
    }
    fetchAllBjTemplates()
    expect(request).toBeCalledWith('/api/bjTemplates', { params })
  })

  it(`fetchAllWbjTemplates`, () => {
    const params = {
      limit: 10000,
      page: 1,
    }
    fetchAllWbjTemplates()
    expect(request).toBeCalledWith('/api/wbjTemplates', { params })
  })
})
