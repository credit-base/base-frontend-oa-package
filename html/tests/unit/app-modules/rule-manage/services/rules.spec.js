/**
 * @file 国标规则管理 - 接口
 * @module rule-manage/gb
 */

jest.mock('@/utils/request')
import request from '@/utils/request'
import {
  fetchRulesList,
  fetchRulesDetail,
  createRules,
  fetchEditRules,
  updateEditRules,
  deleteRules,
  fetchUnAuditedRulesList,
  fetchUnAuditedRulesDetail,
  fetchEditUnAuditedRulesDetail,
  updateUnAuditedRules,
  updateApproveUnAuditedRules,
  updateRejectUnAuditedRules,
  updateRevokeUnAuditedRules,
  fetchCommonBjTemplateList,
  fetchCommonGbTemplateList,
  fetchCommonWbjTemplateList,
} from '@/app-modules/rule-manage/services/rules'

const id = global.randomString()
const params = {}

describe(`Services - rule-manage - rule`, () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it(`fetchRulesList`, () => {
    fetchRulesList()
    expect(request).toBeCalledWith(`/api/rules`, { params })
  })

  it(`fetchRulesDetail`, () => {
    fetchRulesDetail(id)
    expect(request).toBeCalledWith(`/api/rules/${id}`)
  })

  it(`createRules`, () => {
    createRules(params)
    expect(request).toBeCalledWith(`/api/rules/add`, { method: 'POST', params })
  })

  it('fetchEditRules', () => {
    fetchEditRules(id)
    expect(request).toBeCalledWith(`/api/rules/${id}`)
  })

  it(`updateEditRules`, () => {
    updateEditRules(id, params)
    expect(request).toBeCalledWith(`/api/rules/${id}/edit`, { method: 'POST', params })
  })

  it('deleteRules', () => {
    deleteRules(id)
    expect(request).toBeCalledWith(`/api/rules/${id}/delete`, { method: 'POST' })
  })

  it('fetchUnAuditedRulesList', () => {
    fetchUnAuditedRulesList(params)
    expect(request).toBeCalledWith(`/api/unAuditedRules`, { params })
  })

  it(`fetchUnAuditedRulesDetail`, () => {
    fetchUnAuditedRulesDetail(id)
    expect(request).toBeCalledWith(`/api/unAuditedRules/${id}`)
  })

  it(`fetchEditUnAuditedRulesDetail`, () => {
    fetchEditUnAuditedRulesDetail(id)
    expect(request).toBeCalledWith(`/api/unAuditedRules/${id}`)
  })

  it('updateUnAuditedRules', () => {
    updateUnAuditedRules(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedRules/${id}/edit`, { method: 'POST', params })
  })

  it(`updateApproveUnAuditedRules`, () => {
    updateApproveUnAuditedRules(id)
    expect(request).toBeCalledWith(`/api/unAuditedRules/${id}/approve`, { method: 'POST' })
  })

  it(`updateRejectUnAuditedRules`, () => {
    updateRejectUnAuditedRules(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedRules/${id}/reject`, { method: 'POST', params })
  })

  it(`updateRevokeUnAuditedRules`, () => {
    updateRevokeUnAuditedRules(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedRules/${id}/revoke`, { method: 'POST', params })
  })

  it('fetchCommonBjTemplateList', () => {
    fetchCommonBjTemplateList(id)
    expect(request).toBeCalledWith(`/api/commonBjTemplates`)
  })

  it(`fetchCommonGbTemplateList`, () => {
    fetchCommonGbTemplateList()
    expect(request).toBeCalledWith(`/api/commonGbTemplates`)
  })

  it(`fetchCommonWbjTemplateList`, () => {
    fetchCommonWbjTemplateList()
    expect(request).toBeCalledWith(`/api/commonWbjTemplates`)
  })
})
