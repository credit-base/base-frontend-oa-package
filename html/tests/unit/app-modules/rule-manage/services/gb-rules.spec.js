/**
 * @file 国标规则管理 - 接口
 * @module rule-manage/gb
 */

jest.mock('@/utils/request')
import request from '@/utils/request'
import {
  fetchGbTemplateList,
  fetchGbRulesList,
  fetchGbRulesDetail,
  createGbRules,
  fetchEditGbRules,
  updateEditGbRules,
  deleteGbRules,
  fetchUnAuditedGbRules,
  fetchUnAuditedGbRulesDetail,
  fetchUnAuditedEditGbRules,
  updateUnAuditedEditGbRules,
  revokeUnAuditedGbRules,
  approveUnAuditedGbRules,
  rejectUnAuditedGbRules,
  fetchAllGbTemplates,
  fetchAllWbjTemplates,
} from '@/app-modules/rule-manage/services/gb-rules'
import { gbRuleDetail } from '@/app-modules/rule-manage/helpers/mocks'

const id = global.randomString()
const params = {}
const useMock = { useMock: true }

describe(`Services - rule-manage - gb`, () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it(`fetchGbTemplateList`, () => {
    fetchGbTemplateList()
    expect(request).toBeCalledWith(`/api/gbRule/gbTemplates`, { params })
    expect(fetchGbTemplateList(useMock)).toHaveProperty(`total`)
    expect(fetchGbTemplateList(useMock)).toHaveProperty(`list`)
  })

  it(`fetchGbRulesList`, () => {
    fetchGbRulesList()
    expect(request).toBeCalledWith(`/api/gbRules`, { params })
    expect(fetchGbRulesList(useMock)).toHaveProperty(`total`)
    expect(fetchGbRulesList(useMock)).toHaveProperty(`list`)
  })

  it(`fetchGbRulesDetail`, () => {
    fetchGbRulesDetail(id)
    expect(request).toBeCalledWith(`/api/gbRules/${id}`)
    expect(fetchGbRulesDetail(id, useMock)).toStrictEqual({
      ...gbRuleDetail,
    })
  })

  it('createGbRules', () => {
    createGbRules(params)
    expect(request).toBeCalledWith(`/api/gbRules/add`, { method: 'POST', params })
  })

  it(`fetchEditGbRules`, () => {
    fetchEditGbRules(id)
    expect(request).toBeCalledWith(`/api/gbRules/${id}`)
    expect(fetchEditGbRules(id, useMock)).toStrictEqual({
      ...gbRuleDetail,
    })
  })

  it('updateEditGbRules', () => {
    updateEditGbRules(id, params)
    expect(request).toBeCalledWith(`/api/gbRules/${id}/edit`, { method: 'POST', params })
  })

  it('deleteGbRules', () => {
    deleteGbRules(id)
    expect(request).toBeCalledWith(`/api/gbRules/${id}/delete`, { method: 'POST' })
  })

  it(`fetchUnAuditedGbRules`, () => {
    fetchUnAuditedGbRules()
    expect(request).toBeCalledWith(`/api/unAuditedGbRules`, { params })
    expect(fetchUnAuditedGbRules(useMock)).toHaveProperty('list')
    expect(fetchUnAuditedGbRules(useMock)).toHaveProperty('total')
  })

  it(`fetchUnAuditedGbRulesDetail`, () => {
    fetchUnAuditedGbRulesDetail(id)
    expect(request).toBeCalledWith(`/api/unAuditedGbRules/${id}`)
    expect(fetchUnAuditedGbRulesDetail(id, useMock)).toStrictEqual({
      ...gbRuleDetail,
    })
  })

  it('fetchUnAuditedEditGbRules', () => {
    fetchUnAuditedEditGbRules(id)
    expect(request).toBeCalledWith(`/api/unAuditedGbRules/${id}`)
    expect(fetchUnAuditedEditGbRules(id, useMock)).toStrictEqual({
      ...gbRuleDetail,
    })
  })

  it('updateUnAuditedEditGbRules', () => {
    updateUnAuditedEditGbRules(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedGbRules/${id}/edit`, { method: 'POST', params })
  })

  it(`revokeUnAuditedGbRules`, () => {
    revokeUnAuditedGbRules(id)
    expect(request).toBeCalledWith(`/api/unAuditedGbRules/${id}/revoke`, { method: 'POST' })
  })

  it(`approveUnAuditedGbRules`, () => {
    approveUnAuditedGbRules(id)
    expect(request).toBeCalledWith(`/api/unAuditedGbRules/${id}/approve`, { method: 'POST' })
  })

  it(`rejectUnAuditedGbRules`, () => {
    rejectUnAuditedGbRules(id)
    expect(request).toBeCalledWith(`/api/unAuditedGbRules/${id}/reject`, { method: 'POST', params })
  })

  it(`fetchAllGbTemplates`, () => {
    const params = {
      limit: 10000,
      page: 1,
    }
    fetchAllGbTemplates()
    expect(request).toBeCalledWith('/api/gbTemplates', { params })
  })

  it(`fetchAllWbjTemplates`, () => {
    const params = {
      limit: 10000,
      page: 1,
    }
    fetchAllWbjTemplates()
    expect(request).toBeCalledWith('/api/wbjTemplates', { params })
  })
})
