jest.mock('@/app-modules/rule-manage/services/gb-rules')
import UnAuditedGbRulesList from '@/app-modules/rule-manage/views/review/un-audited-gb-rules-list'

import { fetchUnAuditedGbRules } from '@/app-modules/rule-manage/services/gb-rules'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test Vue UnAuditedGbRulesList', () => {
  let wrapper

  const onRouterPush = jest.fn()
  const store = {
    getters: {
      fullUserGroup: [],
      superAdminPurview: false,
      platformAdminPurview: false,
      userGroup: {
        id,
      },
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(UnAuditedGbRulesList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: {},
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test renders fullUserGroup', () => {
    expect(wrapper.vm.fullUserGroup).toStrictEqual([])
  })

  it('Test methods initQuery', async () => {
    wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods generateQuery', () => {
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods fetchData', async () => {
    fetchUnAuditedGbRules.mockResolvedValue()

    await wrapper.vm.fetchData()
    await wrapper.vm.generateQuery()
    expect(fetchUnAuditedGbRules).toBeCalled()
  })

  it('Test methods sortChange', async () => {
    let column = {
      prop: 'updateTime',
      order: 'ascending',
    }
    fetchUnAuditedGbRules.mockResolvedValue()
    await wrapper.vm.sortChange(column)
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(wrapper.vm.query.sort).toBe('updateTime')
    await wrapper.vm.generateQuery()
    await wrapper.vm.initQuery()
    expect(fetchUnAuditedGbRules).toBeCalled()
    column = {
      prop: 'updateTime',
      order: 'descending',
    }
    await wrapper.vm.sortChange(column)
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(wrapper.vm.query.sort).toBe('-updateTime')
  })
  it('Test methods searchData', () => {
    fetchUnAuditedGbRules.mockResolvedValue()
    wrapper.vm.searchData()
    wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(fetchUnAuditedGbRules).toBeCalled()
  })

  it('Test methods filterTag', () => {
    expect(wrapper.vm.filterTag()).toBeTruthy()
  })

  it('Test methods fnFilterChangeInit', () => {
    const filter = {
      applyStatus: ['LC0'],
    }

    wrapper.vm.fnFilterChangeInit(filter)
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    // expect(wrapper.vm.query.applyStatus).toBe('LC0')
    wrapper.vm.fnFilterChangeInit({})
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    // expect(wrapper.vm.query.applyStatus).toBeUndefined()
  })

  it('Test methods handleCommand', async () => {
    const command = 'AUDIT_VIEW'
    const row = {
      id,
    }
    await wrapper.vm.handleCommand(command, row)
    expect(onRouterPush).toBeCalledWith({
      path: `/rule-manage/gb-rules/review/detail/${id}`,
      query: { route: `/rule-manage/gb-rules/review/list` },
    })
  })

  it('Test UnAuditedGbRulesList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
