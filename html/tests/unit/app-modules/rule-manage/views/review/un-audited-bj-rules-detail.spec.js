jest.mock('@/utils/element')
jest.mock('@/app-modules/rule-manage/services/bj-rules')
import UnAuditedBjRulesDetail from '@/app-modules/rule-manage/views/review/un-audited-bj-rules-detail'
import { openConfirm, openPrompt } from '@/utils/element'
import {
  rejectUnAuditedBjRules,
  approveUnAuditedBjRules,
  fetchUnAuditedBjRulesDetail,
} from '@/app-modules/rule-manage/services/bj-rules'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue UnAuditedBjRulesDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(UnAuditedBjRulesDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchUnAuditedJournalsDetail', async () => {
    fetchUnAuditedBjRulesDetail.mockResolvedValue()
    const res = {
      applyStatus: {
        id,
      },
    }
    fetchUnAuditedBjRulesDetail.mockResolvedValue(res)
    await wrapper.vm.fetchData(id)
    expect(fetchUnAuditedBjRulesDetail).toBeCalledWith(id)
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test methods handleCommand - approveUnAuditedBjRules 、rejectUnAuditedBjRules', async () => {
    const reason = {
      value: 'rejectReason',
    }

    const res = {
      applyStatus: {
        id,
      },
    }
    fetchUnAuditedBjRulesDetail.mockResolvedValue(res)
    rejectUnAuditedBjRules.mockResolvedValue()
    approveUnAuditedBjRules.mockResolvedValue()
    openConfirm.mockResolvedValue()
    openPrompt.mockResolvedValue(reason)

    await wrapper.vm.handleCommand('APPROVE', id)
    expect(approveUnAuditedBjRules).toBeCalledWith(id)

    await wrapper.vm.handleCommand('REJECT', id)
    expect(rejectUnAuditedBjRules).toBeCalledWith(id, { rejectReason: 'rejectReason' })
  })

  it('Test UnAuditedBjRulesDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
