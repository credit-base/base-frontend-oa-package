jest.mock('@/utils/element')
jest.mock('@/app-modules/rule-manage/services/gb-rules')
import UnAuditedGbRulesDetail from '@/app-modules/rule-manage/views/review/un-audited-gb-rules-detail'
import { openConfirm, openPrompt } from '@/utils/element'
import {
  rejectUnAuditedGbRules,
  approveUnAuditedGbRules,
  fetchUnAuditedGbRulesDetail,
} from '@/app-modules/rule-manage/services/gb-rules'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue UnAuditedGbRulesDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(UnAuditedGbRulesDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchUnAuditedJournalsDetail', async () => {
    fetchUnAuditedGbRulesDetail.mockResolvedValue()
    const res = {
      applyStatus: {
        id,
      },
    }
    fetchUnAuditedGbRulesDetail.mockResolvedValue(res)
    await wrapper.vm.fetchData(id)
    expect(fetchUnAuditedGbRulesDetail).toBeCalledWith(id)
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test methods handleCommand - approveUnAuditedGbRules 、rejectUnAuditedGbRules', async () => {
    const reason = {
      value: 'rejectReason',
    }

    const res = {
      applyStatus: {
        id,
      },
    }
    fetchUnAuditedGbRulesDetail.mockResolvedValue(res)
    rejectUnAuditedGbRules.mockResolvedValue()
    approveUnAuditedGbRules.mockResolvedValue()
    openConfirm.mockResolvedValue()
    openPrompt.mockResolvedValue(reason)

    await wrapper.vm.handleCommand('APPROVE', id)
    expect(approveUnAuditedGbRules).toBeCalledWith(id)

    await wrapper.vm.handleCommand('REJECT', id)
    expect(rejectUnAuditedGbRules).toBeCalledWith(id, { rejectReason: 'rejectReason' })
  })

  it('Test UnAuditedGbRulesDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
