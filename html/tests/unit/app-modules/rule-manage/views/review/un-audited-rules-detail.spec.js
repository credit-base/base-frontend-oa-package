jest.mock('@/utils/element')
jest.mock('@/app-modules/rule-manage/services/rules')
import UnAuditedRulesDetail from '@/app-modules/rule-manage/views/review/un-audited-rules-detail'
import { openConfirm, openPrompt } from '@/utils/element'
import {
  updateRejectUnAuditedRules,
  updateApproveUnAuditedRules,
  fetchUnAuditedRulesDetail,
} from '@/app-modules/rule-manage/services/rules'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue UnAuditedRulesDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(UnAuditedRulesDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchUnAuditedJournalsDetail', async () => {
    fetchUnAuditedRulesDetail.mockResolvedValue()
    const res = {
      applyStatus: {
        id,
      },
    }
    fetchUnAuditedRulesDetail.mockResolvedValue(res)
    await wrapper.vm.fetchData(id)
    expect(fetchUnAuditedRulesDetail).toBeCalledWith(id)
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test methods handleCommand - updateApproveUnAuditedRules 、updateRejectUnAuditedRules', async () => {
    const reason = {
      value: 'rejectReason',
    }

    const res = {
      applyStatus: {
        id,
      },
    }
    fetchUnAuditedRulesDetail.mockResolvedValue(res)
    updateRejectUnAuditedRules.mockResolvedValue()
    updateApproveUnAuditedRules.mockResolvedValue()
    openConfirm.mockResolvedValue()
    openPrompt.mockResolvedValue(reason)

    await wrapper.vm.handleCommand('APPROVE', id)
    expect(updateApproveUnAuditedRules).toBeCalledWith(id)

    await wrapper.vm.handleCommand('REJECT', id)
    expect(updateRejectUnAuditedRules).toBeCalledWith(id, { rejectReason: 'rejectReason' })
  })

  it('Test UnAuditedRulesDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
