/**
 * @file 国标规则 - 列表 - 委办局
 * @module rule-manage/components/gb-user-group
 */

/* eslint-disable */

import TabViewUserGroup from '@/app-modules/rule-manage/views/components/tab-view-user-group'

describe(`TabViewUserGroup`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(TabViewUserGroup, {
      ...global.createComponentMocks({}),
      propsData: {
        isReview: true,
      },
      data () {
        return {
          list: [],
          total: 0,
          query: {
            page: 1,
            limit: 10,
            gbTemplate: '',
            wbjTemplate: '',
          },
        }
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
