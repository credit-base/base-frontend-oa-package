/**
 * @file 国标规则 - 列表
 * @module rule-manage/views/list/release/bj-rules-list
 */
jest.mock('@/utils/element')
jest.mock('@/app-modules/rule-manage/services/bj-rules')
import BjRulesList from '@/app-modules/rule-manage/views/release/bj-rules-list'
import {
  fetchBjRulesList,
  fetchUnAuditedBjRules,
  revokeUnAuditedBjRules,
  deleteBjRules,
} from '@/app-modules/rule-manage/services/bj-rules'
import { BJ_RULE_TABS } from '@/app-modules/rule-manage/helpers/constants'
import { openConfirm } from '@/utils/element'

const id = 'MA'
// const userGroup = global.randomString()
const onRouterPush = jest.fn()

describe(`BjRulesList`, () => {
  let wrapper

  const store = {
    getters: {
      superAdminPurview: false,
      platformAdminPurview: false,
      fullUserGroup: [],
      userGroup: {
        id,
      },
    },
  }

  beforeEach(() => {
    wrapper = global.shallowMount(BjRulesList, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: { templateId: id },
            query: {},
          },
          $router: { push: onRouterPush },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', () => {
    wrapper.vm.initQuery()
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
    })
  })

  it(`computed`, () => {
    expect(wrapper.vm.fullUserGroup).toStrictEqual([])
  })

  it('Test methods fetchData', async () => {
    fetchBjRulesList.mockResolvedValue()
    fetchUnAuditedBjRules.mockResolvedValue()
    wrapper.setData({
      activeTab: BJ_RULE_TABS[0].name,
    })
    await wrapper.vm.fetchData()
    expect(fetchBjRulesList).toBeCalled()

    wrapper.setData({
      activeTab: BJ_RULE_TABS[1].name,
    })
    await wrapper.vm.fetchData()
    expect(fetchUnAuditedBjRules).toBeCalled()
  })

  it('Test methods filterData', async () => {
    fetchBjRulesList.mockResolvedValue()
    fetchUnAuditedBjRules.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData()
    await wrapper.vm.generateQuery()
    expect(fetchBjRulesList).toBeCalled()
  })

  it('Test methods sortChange', async () => {
    fetchBjRulesList.mockResolvedValue()
    fetchUnAuditedBjRules.mockResolvedValue()

    wrapper.vm.sortChange()
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
    })
    expect(fetchBjRulesList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
      sort: 'updateTime',
    })
    expect(fetchBjRulesList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
      sort: '-updateTime',
    })
    expect(fetchBjRulesList).toBeCalled()

    wrapper.vm.sortChange()
    // await wrapper.vm.fetchData(false)
    // expect(wrapper.vm.generateQuery()).toStrictEqual({
    //   limit: 10,
    //   page: 1,
    //   userGroup: 'MA',
    // })
    // expect(fetchUnAuditedBjRules).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    await wrapper.vm.fetchData(false)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
      sort: 'updateTime',
    })
    expect(fetchBjRulesList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    await wrapper.vm.fetchData(false)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
      sort: '-updateTime',
    })
    expect(fetchBjRulesList).toBeCalled()
  })

  it('Test methods changeTabs', async () => {
    fetchBjRulesList.mockResolvedValue()
    fetchUnAuditedBjRules.mockResolvedValue()

    await wrapper.vm.changeTabs()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchBjRulesList).toBeCalled()

    // await wrapper.vm.changeTabs()
    // await wrapper.vm.fetchData(false)
    // await wrapper.vm.generateQuery()
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    fetchBjRulesList.mockResolvedValue()
    fetchUnAuditedBjRules.mockResolvedValue()
    await wrapper.vm.searchData()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchBjRulesList).toBeCalled()
  })

  it('Test methods addBjRule', () => {
    wrapper.vm.addBjRule()
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/bj-rules/release/add/${id}`, query: { route: `/rule-manage/bj-rules/release/list/${id}` } })
  })

  it('Test methods handleCommand - onRouterPush deleteBjRules revokeUnAuditedBjRules', async () => {
    openConfirm.mockResolvedValue()
    deleteBjRules.mockResolvedValue()
    revokeUnAuditedBjRules.mockResolvedValue()

    await wrapper.vm.handleCommand({ command: 'VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/bj-rules/release/detail/${id}`, query: { route: `/rule-manage/bj-rules/release/list/${id}` } })
    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/bj-rules/release/un-audited-detail/${id}`, query: { route: `/rule-manage/bj-rules/release/list/${id}?activeTab=${BJ_RULE_TABS[1].name}` } })
    await wrapper.vm.handleCommand({ command: 'DELETE', id })
    expect(deleteBjRules).toBeCalledWith(id)
    await wrapper.vm.handleCommand({ command: 'REVOKE', id })
    expect(revokeUnAuditedBjRules).toBeCalledWith(id)
    await wrapper.vm.handleCommand({ command: 'EDIT', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/bj-rules/release/edit/${id}`, query: { route: `/rule-manage/bj-rules/release/list/${id}` } })
    await wrapper.vm.handleCommand({ command: 'AUDIT_EDIT', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/bj-rules/release/un-audited-edit/${id}`, query: { route: `/rule-manage/bj-rules/release/list/${id}?activeTab=${BJ_RULE_TABS[1].name}` } })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
