/**
 * @file 本级规则 - 审核 - 编辑
 * @module rule-manage/views/release/un-audited-gb-rules-detail
 */
jest.mock('@/app-modules/rule-manage/services/gb-rules')
jest.mock('@/utils/element')
import UnAuditedGbRulesDetail from '@/app-modules/rule-manage/views/release/un-audited-gb-rules-detail'
import { openConfirm } from '@/utils/element'
import {
  revokeUnAuditedGbRules,
  fetchUnAuditedGbRulesDetail,
} from '@/app-modules/rule-manage/services/gb-rules'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe(`UnAuditedGbRulesDetail`, () => {
  let wrapper

  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(UnAuditedGbRulesDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test methods handleCommand - fetchUnAuditedGbRulesDetail revokeUnAuditedGbRules', async () => {
    fetchUnAuditedGbRulesDetail.mockResolvedValue()
    revokeUnAuditedGbRules.mockResolvedValue()
    openConfirm.mockResolvedValue()

    await wrapper.vm.handleCommand('REVOKE', id)
    expect(revokeUnAuditedGbRules).toBeCalled()

    await wrapper.vm.handleCommand('AUDIT_EDIT', id)
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/gb-rules/release/un-audited-edit/${id}`, query: { route: `/rule-manage/gb-rules/release/un-audited-detail/${id}` } })
    expect(fetchUnAuditedGbRulesDetail).toBeCalled()
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
