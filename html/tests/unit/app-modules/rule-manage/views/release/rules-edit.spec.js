/**
 * @file 国标规则 - 编辑
 * @module rule-manage/views/release/rules-edit
 */

import RulesEdit from '@/app-modules/rule-manage/views/release/rules-edit'

describe(`RulesEdit`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(RulesEdit, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
