/**
 * @file 国标规则 - 编辑
 * @module rule-manage/views/release/gb-rules-edit
 */

import GbRulesEdit from '@/app-modules/rule-manage/views/release/gb-rules-edit'

describe(`GbRulesEdit`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(GbRulesEdit, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
