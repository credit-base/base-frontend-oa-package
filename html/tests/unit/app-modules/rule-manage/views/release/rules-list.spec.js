/**
 * @file 国标规则 - 列表
 * @module rule-manage/views/list/release/rules-list
 */
jest.mock('@/utils/element')
jest.mock('@/app-modules/rule-manage/services/rules')
import RulesList from '@/app-modules/rule-manage/views/release/rules-list'
import {
  fetchRulesList,
  fetchUnAuditedRulesList,
  updateRevokeUnAuditedRules,
  deleteRules,
} from '@/app-modules/rule-manage/services/rules'
import { RULE_TABS } from '@/app-modules/rule-manage/helpers/constants'
import { openConfirm } from '@/utils/element'

const id = 'MA'
// const userGroup = global.randomString()
const onRouterPush = jest.fn()

describe(`RulesList`, () => {
  let wrapper

  const store = {
    getters: {
      superAdminPurview: false,
      platformAdminPurview: false,
      fullUserGroup: [],
      userGroup: {
        id,
      },
    },
  }

  beforeEach(() => {
    wrapper = global.shallowMount(RulesList, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: { templateId: id },
            query: {},
          },
          $router: { push: onRouterPush },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', () => {
    wrapper.vm.initQuery()
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      sourceCategory: '',
      sourceTemplateId: '',
      transformationCategory: '',
      transformationTemplateId: '',
    })
  })

  it('Test methods fetchData', async () => {
    fetchRulesList.mockResolvedValue()
    fetchUnAuditedRulesList.mockResolvedValue()
    wrapper.setData({
      activeTab: RULE_TABS[0].name,
    })
    await wrapper.vm.fetchData()
    expect(fetchRulesList).toBeCalled()

    wrapper.setData({
      activeTab: RULE_TABS[1].name,
    })
    await wrapper.vm.fetchData()
    expect(fetchUnAuditedRulesList).toBeCalled()
  })

  it('Test methods filterData', async () => {
    fetchRulesList.mockResolvedValue()
    fetchUnAuditedRulesList.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData()
    await wrapper.vm.generateQuery()
    expect(fetchRulesList).toBeCalled()
  })

  it('Test methods sortChange', async () => {
    fetchRulesList.mockResolvedValue()
    fetchUnAuditedRulesList.mockResolvedValue()

    wrapper.vm.sortChange()
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
    })
    expect(fetchRulesList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      sort: 'updateTime',
    })
    expect(fetchRulesList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      sort: '-updateTime',
    })
    expect(fetchRulesList).toBeCalled()

    wrapper.vm.sortChange()
    // await wrapper.vm.fetchData(false)
    // expect(wrapper.vm.generateQuery()).toStrictEqual({
    //   limit: 10,
    //   page: 1,
    //   userGroup: 'MA',
    // })
    // expect(fetchUnAuditedRulesList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    await wrapper.vm.fetchData(false)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      sort: 'updateTime',
    })
    expect(fetchRulesList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    await wrapper.vm.fetchData(false)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      sort: '-updateTime',
    })
    expect(fetchRulesList).toBeCalled()
  })

  it('Test methods changeTabs', async () => {
    fetchRulesList.mockResolvedValue()
    fetchUnAuditedRulesList.mockResolvedValue()

    await wrapper.vm.changeTabs()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchRulesList).toBeCalled()

    // await wrapper.vm.changeTabs()
    // await wrapper.vm.fetchData(false)
    // await wrapper.vm.generateQuery()
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    fetchRulesList.mockResolvedValue()
    fetchUnAuditedRulesList.mockResolvedValue()
    await wrapper.vm.searchData()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchRulesList).toBeCalled()
  })

  it('Test methods addRule', () => {
    wrapper.vm.addRule()
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/rules/release/add`, query: { route: `/rule-manage/rules/release/list` } })
  })

  it('Test methods handleCommand - onRouterPush deleteRules updateRevokeUnAuditedRules', async () => {
    openConfirm.mockResolvedValue()
    deleteRules.mockResolvedValue()
    updateRevokeUnAuditedRules.mockResolvedValue()

    await wrapper.vm.handleCommand({ command: 'VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/rules/release/detail/${id}`, query: { route: `/rule-manage/rules/release/list` } })
    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/rules/release/un-audited-detail/${id}`, query: { route: `/rule-manage/rules/release/list?activeTab=${RULE_TABS[1].name}` } })
    await wrapper.vm.handleCommand({ command: 'DELETE', id })
    expect(deleteRules).toBeCalledWith(id)
    await wrapper.vm.handleCommand({ command: 'REVOKE', id })
    expect(updateRevokeUnAuditedRules).toBeCalledWith(id)
    await wrapper.vm.handleCommand({ command: 'EDIT', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/rules/release/edit/${id}`, query: { route: `/rule-manage/rules/release/list` } })
    await wrapper.vm.handleCommand({ command: 'AUDIT_EDIT', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/rules/release/un-audited-edit/${id}`, query: { route: `/rule-manage/rules/release/list?activeTab=${RULE_TABS[1].name}` } })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
