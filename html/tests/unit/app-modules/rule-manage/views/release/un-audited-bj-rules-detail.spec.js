/**
 * @file 本级规则 - 审核 - 编辑
 * @module rule-manage/views/release/un-audited-bj-rules-detail
 */
jest.mock('@/app-modules/rule-manage/services/bj-rules')
jest.mock('@/utils/element')
import UnAuditedBjRulesDetail from '@/app-modules/rule-manage/views/release/un-audited-bj-rules-detail'
import { openConfirm } from '@/utils/element'
import {
  revokeUnAuditedBjRules,
  fetchUnAuditedBjRulesDetail,
} from '@/app-modules/rule-manage/services/bj-rules'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe(`UnAuditedBjRulesDetail`, () => {
  let wrapper

  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(UnAuditedBjRulesDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test methods handleCommand - fetchUnAuditedBjRulesDetail revokeUnAuditedBjRules', async () => {
    fetchUnAuditedBjRulesDetail.mockResolvedValue()
    revokeUnAuditedBjRules.mockResolvedValue()
    openConfirm.mockResolvedValue()

    await wrapper.vm.handleCommand('REVOKE', id)
    expect(revokeUnAuditedBjRules).toBeCalled()

    await wrapper.vm.handleCommand('AUDIT_EDIT', id)
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/bj-rules/release/un-audited-edit/${id}`, query: { route: `/rule-manage/bj-rules/release/un-audited-detail/${id}` } })
    expect(fetchUnAuditedBjRulesDetail).toBeCalled()
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
