/**
 * @file 国标规则 - 详情
 * @module rule-manage/views/release/rules-detail
 */

jest.mock('@/utils/element')
jest.mock('@/app-modules/rule-manage/services/rules')
import RulesDetail from '@/app-modules/rule-manage/views/release/rules-detail'
import { openConfirm } from '@/utils/element'
import {
  deleteRules,
  fetchRulesDetail,
} from '@/app-modules/rule-manage/services/rules'

const id = 'MA'

describe(`RulesDetail`, () => {
  const store = {
    getters: {
      userInfo: {
        userGroup: { id },
      },
    },
  }
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = global.shallowMount(RulesDetail, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
            params: { id },
          },
          $router: { push: onRouterPush },
        },
        stubs: {
          DetailViewRule: true,
          DetailViewTest: true,
          DetailViewUsage: true,
          DetailViewRecord: true,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchUnAuditedJournalsDetail', async () => {
    fetchRulesDetail.mockResolvedValue()
    await wrapper.vm.fetchData(id)
    expect(fetchRulesDetail).toBeCalledWith(id)
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test methods handleCommand - onRouterPush deleteRules revokeUnAuditedRules', async () => {
    openConfirm.mockResolvedValue()
    deleteRules.mockResolvedValue()

    await wrapper.vm.handleCommand('EDIT', id)
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/rules/release/edit/${id}`, query: { route: `/rule-manage/rules/release/detail/${id}` } })
    await wrapper.vm.handleCommand('DELETE', id)
    expect(deleteRules).toBeCalledWith(id)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
