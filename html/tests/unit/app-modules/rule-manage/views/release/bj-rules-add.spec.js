/**
 * @file 国标规则 - 新增
 * @module rule-manage/views/release/bj-rules-add
 */

import BjRulesAdd from '@/app-modules/rule-manage/views/release/bj-rules-add'

describe(`BjRulesAdd`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(BjRulesAdd, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
