/**
 * @file 国标目录 - 列表
 * @module rule-manage/views/release/gb-templates
 */
jest.mock('@/app-modules/rule-manage/services/gb-rules')
import GbTemplates from '@/app-modules/rule-manage/views/release/gb-templates'
import { fetchGbTemplateList } from '@/app-modules/rule-manage/services/gb-rules'

const id = 'MA'

describe(`GbTemplates`, () => {
  const store = {
    getters: {
      superAdminPurview: false,
      platformAdminPurview: false,
      userGroup: {
        id,
      },
    },
  }
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = global.shallowMount(GbTemplates, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
    jest.clearAllMocks()
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', () => {
    wrapper.vm.initQuery()
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      userGroup: id,
    })
  })

  it('Test methods fetchData - fetchGbTemplateList', async () => {
    fetchGbTemplateList.mockResolvedValue()
    await wrapper.vm.fetchData()
    const params = {
      limit: 10,
      page: 1,
      userGroup: id,
    }
    expect(fetchGbTemplateList).toBeCalledWith(params)
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test methods searchData', async () => {
    await wrapper.vm.searchData()
    fetchGbTemplateList.mockResolvedValue()
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      userGroup: id,
    })
    expect(fetchGbTemplateList).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    const value = { command: 'VIEW', id }
    await wrapper.vm.handleCommand(value)
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/gb-rules/release/list/${id}`, query: { route: '/rule-manage/release/gb-templates' } })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
