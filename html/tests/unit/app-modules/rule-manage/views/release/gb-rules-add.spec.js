/**
 * @file 国标规则 - 新增
 * @module rule-manage/views/release/gb-rules-add
 */

import GbRulesAdd from '@/app-modules/rule-manage/views/release/gb-rules-add'

describe(`GbRulesAdd`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(GbRulesAdd, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
