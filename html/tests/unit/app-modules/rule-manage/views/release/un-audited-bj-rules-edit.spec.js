/**
 * @file 本级规则 - 审核 - 编辑
 * @module rule-manage/views/release/un-audited-bj-rules-edit
 */

import UnAuditedBjRulesEdit from '@/app-modules/rule-manage/views/release/un-audited-bj-rules-edit'

describe(`UnAuditedBjRulesEdit`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(UnAuditedBjRulesEdit, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
