/**
 * @file 本级规则 - 审核 - 编辑
 * @module rule-manage/views/release/un-audited-rules-detail
 */
jest.mock('@/app-modules/rule-manage/services/rules')
jest.mock('@/utils/element')
import UnAuditedRulesDetail from '@/app-modules/rule-manage/views/release/un-audited-rules-detail'
import { openConfirm } from '@/utils/element'
import {
  updateRevokeUnAuditedRules,
  fetchUnAuditedRulesDetail,
} from '@/app-modules/rule-manage/services/rules'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe(`UnAuditedRulesDetail`, () => {
  let wrapper

  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(UnAuditedRulesDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test methods handleCommand - fetchUnAuditedRulesDetail updateRevokeUnAuditedRules', async () => {
    fetchUnAuditedRulesDetail.mockResolvedValue()
    updateRevokeUnAuditedRules.mockResolvedValue()
    openConfirm.mockResolvedValue()

    await wrapper.vm.handleCommand('REVOKE', id)
    expect(updateRevokeUnAuditedRules).toBeCalled()

    await wrapper.vm.handleCommand('AUDIT_EDIT', id)
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/rules/release/un-audited-edit/${id}`, query: { route: `/rule-manage/rules/release/un-audited-detail/${id}` } })
    expect(fetchUnAuditedRulesDetail).toBeCalled()
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
