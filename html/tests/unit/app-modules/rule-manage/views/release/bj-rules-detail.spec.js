/**
 * @file 国标规则 - 详情
 * @module rule-manage/views/release/bj-rules-detail
 */

jest.mock('@/utils/element')
jest.mock('@/app-modules/rule-manage/services/bj-rules')
import BjRulesDetail from '@/app-modules/rule-manage/views/release/bj-rules-detail'
import { openConfirm } from '@/utils/element'
import {
  deleteBjRules,
  fetchBjRulesDetail,
} from '@/app-modules/rule-manage/services/bj-rules'

const id = 'MA'

describe(`BjRulesDetail`, () => {
  const store = {
    getters: {
      userInfo: {
        userGroup: { id },
      },
    },
  }
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = global.shallowMount(BjRulesDetail, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
            params: { id },
          },
          $router: { push: onRouterPush },
        },
        stubs: {
          DetailViewRule: true,
          DetailViewTest: true,
          DetailViewUsage: true,
          DetailViewRecord: true,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchUnAuditedJournalsDetail', async () => {
    fetchBjRulesDetail.mockResolvedValue()
    await wrapper.vm.fetchData(id)
    expect(fetchBjRulesDetail).toBeCalledWith(id)
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test methods handleCommand - onRouterPush deleteBjRules revokeUnAuditedBjRules', async () => {
    openConfirm.mockResolvedValue()
    deleteBjRules.mockResolvedValue()

    await wrapper.vm.handleCommand('EDIT', id)
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/bj-rules/release/edit/${id}`, query: { route: `/rule-manage/bj-rules/release/detail/${id}` } })
    await wrapper.vm.handleCommand('DELETE', id)
    expect(deleteBjRules).toBeCalledWith(id)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
