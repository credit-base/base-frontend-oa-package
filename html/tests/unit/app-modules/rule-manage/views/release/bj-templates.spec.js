/**
 * @file 本级目录 - 列表
 * @module rule-manage/views/release/bj-templates
 */
jest.mock('@/app-modules/rule-manage/services/bj-rules')
import BjTemplates from '@/app-modules/rule-manage/views/release/bj-templates'
import { fetchBjTemplateList } from '@/app-modules/rule-manage/services/bj-rules'

const id = 'MA'

describe(`BjTemplates`, () => {
  const store = {
    getters: {
      superAdminPurview: false,
      platformAdminPurview: false,
      userGroup: {
        id,
      },
    },
  }
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = global.shallowMount(BjTemplates, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
    jest.clearAllMocks()
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', () => {
    wrapper.vm.initQuery()
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      userGroup: id,
    })
  })

  it('Test methods fetchData - fetchBjTemplateList', async () => {
    fetchBjTemplateList.mockResolvedValue()
    await wrapper.vm.fetchData()
    const params = {
      limit: 10,
      page: 1,
      userGroup: id,
    }
    expect(fetchBjTemplateList).toBeCalledWith(params)
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test methods searchData', async () => {
    await wrapper.vm.searchData()
    fetchBjTemplateList.mockResolvedValue()
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      userGroup: id,
    })
    expect(fetchBjTemplateList).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    const value = { command: 'VIEW', id }
    await wrapper.vm.handleCommand(value)
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/bj-rules/release/list/${id}`, query: { route: '/rule-manage/release/bj-templates' } })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
