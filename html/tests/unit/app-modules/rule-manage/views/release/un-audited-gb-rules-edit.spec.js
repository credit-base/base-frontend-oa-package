/**
 * @file 国标规则 - 审核 - 编辑
 * @module rule-manage/views/release/un-audited-gb-rules-edit
 */

import UnAuditedGbRulesEdit from '@/app-modules/rule-manage/views/release/un-audited-gb-rules-edit'

describe(`UnAuditedGbRulesEdit`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(UnAuditedGbRulesEdit, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
