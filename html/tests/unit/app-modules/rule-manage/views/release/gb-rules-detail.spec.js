/**
 * @file 国标规则 - 详情
 * @module rule-manage/views/release/gb-rules-detail
 */

jest.mock('@/utils/element')
jest.mock('@/app-modules/rule-manage/services/gb-rules')
import GbRulesDetail from '@/app-modules/rule-manage/views/release/gb-rules-detail'
import { openConfirm } from '@/utils/element'
import {
  deleteGbRules,
  fetchGbRulesDetail,
} from '@/app-modules/rule-manage/services/gb-rules'

const id = 'MA'

describe(`GbRulesDetail`, () => {
  const store = {
    getters: {
      userInfo: {
        userGroup: { id },
      },
    },
  }
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = global.shallowMount(GbRulesDetail, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
            params: { id },
          },
          $router: { push: onRouterPush },
        },
        stubs: {
          DetailViewRule: true,
          DetailViewTest: true,
          DetailViewUsage: true,
          DetailViewRecord: true,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchUnAuditedJournalsDetail', async () => {
    fetchGbRulesDetail.mockResolvedValue()
    await wrapper.vm.fetchData(id)
    expect(fetchGbRulesDetail).toBeCalledWith(id)
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test methods handleCommand - onRouterPush deleteGbRules revokeUnAuditedGbRules', async () => {
    openConfirm.mockResolvedValue()
    deleteGbRules.mockResolvedValue()

    await wrapper.vm.handleCommand('EDIT', id)
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/gb-rules/release/edit/${id}`, query: { route: `/rule-manage/gb-rules/release/detail/${id}` } })
    await wrapper.vm.handleCommand('DELETE', id)
    expect(deleteGbRules).toBeCalledWith(id)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
