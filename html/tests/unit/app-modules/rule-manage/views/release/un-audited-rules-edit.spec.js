/**
 * @file 国标规则 - 审核 - 编辑
 * @module rule-manage/views/release/un-audited-rules-edit
 */

import UnAuditedRulesEdit from '@/app-modules/rule-manage/views/release/un-audited-rules-edit'

describe(`UnAuditedRulesEdit`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(UnAuditedRulesEdit, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
