/**
 * @file 国标规则 - 列表
 * @module rule-manage/views/list/release/gb-rules-list
 */
jest.mock('@/utils/element')
jest.mock('@/app-modules/rule-manage/services/gb-rules')
import GbRulesList from '@/app-modules/rule-manage/views/release/gb-rules-list'
import {
  fetchGbRulesList,
  fetchUnAuditedGbRules,
  revokeUnAuditedGbRules,
  deleteGbRules,
} from '@/app-modules/rule-manage/services/gb-rules'
import { GB_RULE_TABS } from '@/app-modules/rule-manage/helpers/constants'
import { openConfirm } from '@/utils/element'

const id = 'MA'
// const userGroup = global.randomString()
const onRouterPush = jest.fn()

describe(`GbRulesList`, () => {
  let wrapper

  const store = {
    getters: {
      superAdminPurview: false,
      platformAdminPurview: false,
      fullUserGroup: [],
      userGroup: {
        id,
      },
    },
  }

  beforeEach(() => {
    wrapper = global.shallowMount(GbRulesList, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: { templateId: id },
            query: {},
          },
          $router: { push: onRouterPush },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', () => {
    wrapper.vm.initQuery()
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
    })
  })

  it(`computed`, () => {
    expect(wrapper.vm.fullUserGroup).toStrictEqual([])
  })

  it('Test methods fetchData', async () => {
    fetchGbRulesList.mockResolvedValue()
    fetchUnAuditedGbRules.mockResolvedValue()
    wrapper.setData({
      activeTab: GB_RULE_TABS[0].name,
    })
    await wrapper.vm.fetchData()
    expect(fetchGbRulesList).toBeCalled()

    wrapper.setData({
      activeTab: GB_RULE_TABS[1].name,
    })
    await wrapper.vm.fetchData()
    expect(fetchUnAuditedGbRules).toBeCalled()
  })

  it('Test methods filterData', async () => {
    fetchGbRulesList.mockResolvedValue()
    fetchUnAuditedGbRules.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData()
    await wrapper.vm.generateQuery()
    expect(fetchGbRulesList).toBeCalled()
  })

  it('Test methods sortChange', async () => {
    fetchGbRulesList.mockResolvedValue()
    fetchUnAuditedGbRules.mockResolvedValue()

    wrapper.vm.sortChange()
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
    })
    expect(fetchGbRulesList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
      sort: 'updateTime',
    })
    expect(fetchGbRulesList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
      sort: '-updateTime',
    })
    expect(fetchGbRulesList).toBeCalled()

    wrapper.vm.sortChange()
    // await wrapper.vm.fetchData(false)
    // expect(wrapper.vm.generateQuery()).toStrictEqual({
    //   limit: 10,
    //   page: 1,
    //   userGroup: 'MA',
    // })
    // expect(fetchUnAuditedGbRules).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    await wrapper.vm.fetchData(false)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
      sort: 'updateTime',
    })
    expect(fetchGbRulesList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    await wrapper.vm.fetchData(false)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      templateId: id,
      // userGroup: id,
      sort: '-updateTime',
    })
    expect(fetchGbRulesList).toBeCalled()
  })

  it('Test methods changeTabs', async () => {
    fetchGbRulesList.mockResolvedValue()
    fetchUnAuditedGbRules.mockResolvedValue()

    await wrapper.vm.changeTabs()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchGbRulesList).toBeCalled()

    // await wrapper.vm.changeTabs()
    // await wrapper.vm.fetchData(false)
    // await wrapper.vm.generateQuery()
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    fetchGbRulesList.mockResolvedValue()
    fetchUnAuditedGbRules.mockResolvedValue()
    await wrapper.vm.searchData()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchGbRulesList).toBeCalled()
  })

  it('Test methods addGbRule', () => {
    wrapper.vm.addGbRule()
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/gb-rules/release/add/${id}`, query: { route: `/rule-manage/gb-rules/release/list/${id}` } })
  })

  it('Test methods handleCommand - onRouterPush deleteGbRules revokeUnAuditedGbRules', async () => {
    openConfirm.mockResolvedValue()
    deleteGbRules.mockResolvedValue()
    revokeUnAuditedGbRules.mockResolvedValue()

    await wrapper.vm.handleCommand({ command: 'VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/gb-rules/release/detail/${id}`, query: { route: `/rule-manage/gb-rules/release/list/${id}` } })
    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/gb-rules/release/un-audited-detail/${id}`, query: { route: `/rule-manage/gb-rules/release/list/${id}?activeTab=${GB_RULE_TABS[1].name}` } })
    await wrapper.vm.handleCommand({ command: 'DELETE', id })
    expect(deleteGbRules).toBeCalledWith(id)
    await wrapper.vm.handleCommand({ command: 'REVOKE', id })
    expect(revokeUnAuditedGbRules).toBeCalledWith(id)
    await wrapper.vm.handleCommand({ command: 'EDIT', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/gb-rules/release/edit/${id}`, query: { route: `/rule-manage/gb-rules/release/list/${id}` } })
    await wrapper.vm.handleCommand({ command: 'AUDIT_EDIT', id })
    expect(onRouterPush).toBeCalledWith({ path: `/rule-manage/gb-rules/release/un-audited-edit/${id}`, query: { route: `/rule-manage/gb-rules/release/list/${id}?activeTab=${GB_RULE_TABS[1].name}` } })
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
