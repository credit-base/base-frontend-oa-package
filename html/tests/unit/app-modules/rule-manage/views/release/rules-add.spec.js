/**
 * @file 国标规则 - 新增
 * @module rule-manage/views/release/rules-add
 */

import RulesAdd from '@/app-modules/rule-manage/views/release/rules-add'

describe(`RulesAdd`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(RulesAdd, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
