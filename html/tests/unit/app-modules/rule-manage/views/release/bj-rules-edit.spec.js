/**
 * @file 本级规则 - 编辑
 * @module rule-manage/views/release/bj-rules-edit
 */

import BjRulesEdit from '@/app-modules/rule-manage/views/release/bj-rules-edit'

describe(`BjRulesEdit`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(BjRulesEdit, {
      ...global.createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
