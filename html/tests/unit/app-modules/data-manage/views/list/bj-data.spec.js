import BjDataList from '@/app-modules/data-manage/views/list/bj-data'

const { shallowMount, createComponentMocks } = global

describe('Test components BjDataList', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(BjDataList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id: 'MA',
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test BjDataList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
