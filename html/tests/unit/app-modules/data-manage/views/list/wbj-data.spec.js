import WbjDataList from '@/app-modules/data-manage/views/list/wbj-data'

const { shallowMount, createComponentMocks } = global

describe('Test components WbjDataList', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(WbjDataList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id: 'MA',
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test WbjDataList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
