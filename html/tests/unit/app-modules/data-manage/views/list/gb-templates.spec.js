jest.mock('@/app-modules/data-manage/services/gb-data')
import GbTemplates from '@/app-modules/data-manage/views/list/gb-templates'

const { shallowMount, createComponentMocks } = global
import { fetchGbTemplateList } from '@/app-modules/data-manage/services/gb-data'

describe('Test components GbTemplates', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(GbTemplates, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id: 'MA',
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    fetchGbTemplateList.mockResolvedValue()
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
      },
    })

    await wrapper.vm.fetchData()
    expect(fetchGbTemplateList).toBeCalled()
  })

  it('Test methods changeTabs', async () => {
    fetchGbTemplateList.mockResolvedValue()
    wrapper.setData({
      activeTab: 'template-list',
    })
    await wrapper.vm.changeTabs()
    expect(wrapper.vm.isTemplateDataList).toBeTruthy()
    expect(fetchGbTemplateList).toBeCalled()
  })

  it('Test GbTemplates snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
