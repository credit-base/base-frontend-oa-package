jest.mock('@/app-modules/data-manage/services/wbj-data')
import WbjTemplates from '@/app-modules/data-manage/views/list/wbj-templates'

const { shallowMount, createComponentMocks } = global

import { fetchWbjTemplateList } from '@/app-modules/data-manage/services/wbj-data'

describe('Test components WbjTemplates', () => {
  let wrapper
  const store = {
    getters: {
      userGroup: {
        id: 'MA',
      },
      superAdminPurview: false,
      platformAdminPurview: false,
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(WbjTemplates, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
            params: {
              id: 'MA',
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    fetchWbjTemplateList.mockResolvedValue()
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
      },
    })

    await wrapper.vm.fetchData()
    expect(fetchWbjTemplateList).toBeCalled()
  })
  it('Test methods changeTabs', async () => {
    fetchWbjTemplateList.mockResolvedValue()
    wrapper.setData({
      activeTab: 'template-list',
    })
    await wrapper.vm.changeTabs()
    expect(wrapper.vm.isTemplateDataList).toBeTruthy()
    expect(fetchWbjTemplateList).toBeCalled()
  })

  it('Test WbjTemplates snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
