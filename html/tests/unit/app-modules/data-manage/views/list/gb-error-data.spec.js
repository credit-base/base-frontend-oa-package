jest.mock('@/app-modules/data-manage/services/task')
jest.mock('@/utils/element')
jest.mock('@/utils')
import GbErrorDataList from '@/app-modules/data-manage/views/list/gb-error-data'
import { fetchErrorDataList } from '@/app-modules/data-manage/services/task'
import { downloadFile } from '@/utils'
import { openConfirm } from '@/utils/element'

import {
  GB_ERROR_DATA_LIST_PATH,
  GB_ERROR_DATA_EDIT_PATH,
  GB_ERROR_DATA_DETAIL_PATH,
} from '@/app-modules/data-manage/helpers/constants'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test components GbErrorDataList', () => {
  let wrapper
  const onRoutePush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(GbErrorDataList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              taskId: 'MA',
              templateId: 'MQ',
            },
          },
          $router: {
            push: onRoutePush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', async () => {
    await wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods fetchData', async () => {
    fetchErrorDataList.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(fetchErrorDataList).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    let value = {
      command: 'VIEW',
      id,
    }

    await wrapper.vm.handleCommand(value)
    expect(onRoutePush).toBeCalledWith({
      path: `${GB_ERROR_DATA_DETAIL_PATH}${id}/${id}/MQ`,
      query: {
        route: `${GB_ERROR_DATA_LIST_PATH}MA/MQ`,
      },
    })
    value = {
      command: 'EDIT',
      id,
    }
    await wrapper.vm.handleCommand(value)
    expect(onRoutePush).toBeCalledWith({
      path: `${GB_ERROR_DATA_EDIT_PATH}${id}/MQ`,
      query: {
        route: `${GB_ERROR_DATA_LIST_PATH}MA/MQ`,
      },
    })
  })

  it('Test methods handleDownload', async () => {
    downloadFile.mockResolvedValue()
    openConfirm.mockResolvedValue()
    const fileName = '/upload/DEMO_01.xls'
    await wrapper.vm.handleDownload(fileName)
    expect(downloadFile).toBeCalled()
  })

  it('Test GbErrorDataList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
