jest.mock('@/app-modules/data-manage/services/bj-data')

import BjTemplates from '@/app-modules/data-manage/views/list/bj-templates'
import { fetchBjTemplateList } from '@/app-modules/data-manage/services/bj-data'

const { shallowMount, createComponentMocks } = global

describe('Test components BjTemplates', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(BjTemplates, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id: 'MA',
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    fetchBjTemplateList.mockResolvedValue()
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
      },
    })

    await wrapper.vm.fetchData()
    expect(fetchBjTemplateList).toBeCalled()
  })

  it('Test methods changeTabs', async () => {
    fetchBjTemplateList.mockResolvedValue()
    wrapper.setData({
      activeTab: 'template-list',
    })
    await wrapper.vm.changeTabs()
    expect(wrapper.vm.isTemplateDataList).toBeTruthy()
    expect(fetchBjTemplateList).toBeCalled()
  })

  it('Test BjTemplates snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
