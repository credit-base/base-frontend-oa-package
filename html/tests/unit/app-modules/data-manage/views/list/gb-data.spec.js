import GbDataList from '@/app-modules/data-manage/views/list/gb-data'

const { shallowMount, createComponentMocks } = global

describe('Test components GbDataList', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(GbDataList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id: 'MA',
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test GbDataList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
