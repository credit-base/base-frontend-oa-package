import WbjDataDetail from '@/app-modules/data-manage/views/detail/wbj-data'

const { shallowMount, createComponentMocks } = global

describe('Test components WbjDataDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(WbjDataDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test WbjDataDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
