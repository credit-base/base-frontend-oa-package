import GbDataDetail from '@/app-modules/data-manage/views/detail/gb-data'

const { shallowMount, createComponentMocks } = global

describe('Test components GbDataDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(GbDataDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test GbDataDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
