jest.mock('@/utils/element')
jest.mock('@/app-modules/data-manage/services/task')
jest.mock('@/app-modules/data-manage/helpers/translate')
import BjErrorDataDetail from '@/app-modules/data-manage/views/detail/bj-error-data'
import { fetchErrorDataDetail } from '@/app-modules/data-manage/services/task'
// import { translateErrorDataDetail } from '@/app-modules/data-manage/helpers/translate'
import {
  BJ_ERROR_DATA_DETAIL_PATH,
  BJ_ERROR_DATA_EDIT_PATH,
} from '@/app-modules/data-manage/helpers/constants'

const { shallowMount, createComponentMocks } = global

describe('Test components BjErrorDataDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(BjErrorDataDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id: 'MA',
              taskId: 'MA',
              templateId: 'MQ',
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleEdit', async () => {
    await wrapper.vm.handleEdit()
    expect(onRouterPush).toBeCalledWith({
      path: `${BJ_ERROR_DATA_EDIT_PATH}MA/MQ`,
      query: {
        route: `${BJ_ERROR_DATA_DETAIL_PATH}MA/MA/MQ`,
      },
    })
  })

  it('Test methods fetchData - fetchUnAuditedNewsDetail', async () => {
    fetchErrorDataDetail.mockResolvedValue()
    // translateErrorDataDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(fetchErrorDataDetail).toBeCalled()
    // expect(translateErrorDataDetail).toBeCalled()
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test BjErrorDataDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
