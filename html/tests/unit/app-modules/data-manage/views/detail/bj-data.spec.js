import BjDataDetail from '@/app-modules/data-manage/views/detail/bj-data'

const { shallowMount, createComponentMocks } = global

describe('Test components BjDataDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(BjDataDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test BjDataDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
