jest.mock('@/utils/element')
jest.mock('@/app-modules/data-manage/services/task')
jest.mock('@/app-modules/data-manage/helpers/translate')
import WbjErrorDataDetail from '@/app-modules/data-manage/views/detail/wbj-error-data'
import { fetchErrorDataDetail } from '@/app-modules/data-manage/services/task'
// import { translateErrorDataDetail } from '@/app-modules/data-manage/helpers/translate'
import {
  WBJ_ERROR_DATA_DETAIL_PATH,
  WBJ_ERROR_DATA_EDIT_PATH,
} from '@/app-modules/data-manage/helpers/constants'

const { shallowMount, createComponentMocks } = global

describe('Test components WbjErrorDataDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(WbjErrorDataDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id: 'MA',
              taskId: 'MA',
              templateId: 'MQ',
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleEdit', async () => {
    await wrapper.vm.handleEdit()
    expect(onRouterPush).toBeCalledWith({
      path: `${WBJ_ERROR_DATA_EDIT_PATH}MA/MQ`,
      query: {
        route: `${WBJ_ERROR_DATA_DETAIL_PATH}MA/MA/MQ`,
      },
    })
  })

  it('Test methods fetchData - fetchUnAuditedNewsDetail', async () => {
    fetchErrorDataDetail.mockResolvedValue()
    // translateErrorDataDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(fetchErrorDataDetail).toBeCalled()
    // expect(translateErrorDataDetail).toBeCalled()
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test WbjErrorDataDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
