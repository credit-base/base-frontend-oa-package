import FrontEndProcessor from '@/app-modules/data-manage/views/form/fill-in-online'

const { shallowMount, createComponentMocks } = global

describe('Test components FrontEndProcessor', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FrontEndProcessor, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test FrontEndProcessor snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
