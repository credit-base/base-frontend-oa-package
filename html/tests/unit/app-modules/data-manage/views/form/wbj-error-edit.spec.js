import WbjErrorDataEdit from '@/app-modules/data-manage/views/form/wbj-error-edit'

const { shallowMount, createComponentMocks } = global

describe('Test components WbjErrorDataEdit', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(WbjErrorDataEdit, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test WbjErrorDataEdit snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
