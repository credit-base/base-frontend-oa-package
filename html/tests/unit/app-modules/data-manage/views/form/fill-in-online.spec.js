import FillInOnline from '@/app-modules/data-manage/views/form/front-end-processor'

const { shallowMount, createComponentMocks } = global

describe('Test components FillInOnline', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FillInOnline, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test FillInOnline snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
