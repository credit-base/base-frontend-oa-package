import GbErrorDataEdit from '@/app-modules/data-manage/views/form/gb-error-edit'

const { shallowMount, createComponentMocks } = global

describe('Test components GbErrorDataEdit', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(GbErrorDataEdit, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: { query: {} },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test GbErrorDataEdit snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
