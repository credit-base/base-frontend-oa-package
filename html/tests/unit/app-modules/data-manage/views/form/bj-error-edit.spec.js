import BjErrorDataEdit from '@/app-modules/data-manage/views/form/bj-error-edit'

const { shallowMount, createComponentMocks } = global

describe('Test components BjErrorDataEdit', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(BjErrorDataEdit, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test BjErrorDataEdit snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
