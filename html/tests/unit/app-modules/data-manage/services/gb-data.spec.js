jest.mock('@/utils/request')
import request from '@/utils/request'

import {
  taskList,
  templateDetail,
  templateDataDetail,
} from '@/app-modules/data-manage/helpers/mocks'

import {
  fetchGbTemplateList,
  fetchGbTemplateDetail,
  fetchGbSearchData,
  fetchGbSearchDataDetail,
  updateGbDataConfirm,
  updateGbDataDisable,
  updateGbDataDelete,
  fetchGbTaskList,
  fetchDataConversionRate,
  fetchDataVolume,
  fetchStaticsByUserGroup,
  fetchDataTask,
} from '@/app-modules/data-manage/services/gb-data'

const params = {}
const id = 'MA'
const useMock = { useMock: true }

describe('Test gb-data services', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services fetchGbTemplateList', () => {
    fetchGbTemplateList(params)

    expect(request).toBeCalledWith('/api/gbTemplates', { params })
  })

  it('Test services fetchGbTemplateDetail', () => {
    fetchGbTemplateDetail(id)
    expect(request).toBeCalledWith(`/api/gbTemplates/${id}`, { params })
    expect(fetchGbTemplateDetail(id, useMock)).toStrictEqual({
      ...templateDetail,
    })
  })

  it('Test services fetchGbSearchData', () => {
    fetchGbSearchData(params)
    expect(request).toBeCalledWith(`/api/gbSearchData`, { params })
  })

  it('Test services fetchGbSearchDataDetail', () => {
    fetchGbSearchDataDetail(id)
    expect(request).toBeCalledWith(`/api/gbSearchData/${id}`)
    expect(fetchGbSearchDataDetail(id, useMock)).toStrictEqual({
      ...templateDataDetail,
    })
  })

  it('Test services updateGbDataConfirm', () => {
    updateGbDataConfirm(id)
    expect(request).toBeCalledWith(`/api/gbSearchData/${id}/confirm`, { method: 'POST' })
  })

  it('Test services updateGbDataDisable', () => {
    updateGbDataDisable(id)
    expect(request).toBeCalledWith(`/api/gbSearchData/${id}/disable`, { method: 'POST' })
  })

  it('Test services updateGbDataDelete', () => {
    updateGbDataDelete(id)
    expect(request).toBeCalledWith(`/api/gbSearchData/${id}/delete`, { method: 'POST' })
  })

  it('Test services fetchGbTaskList', () => {
    fetchGbTaskList(params)
    expect(request).toBeCalledWith(`/api/gbTasks`, { params })
    expect(fetchGbTaskList(useMock)).toStrictEqual({
      list: [
        ...taskList,
      ],
      total: 3,
    })
  })

  it('Test services fetchDataConversionRate', () => {
    fetchDataConversionRate(id, params)
    expect(request).toBeCalledWith(`/api/statisticals/staticsByCategoryTotal?administrativeArea=${id}`, { params })
    expect(fetchDataConversionRate(id, useMock)).toStrictEqual({
      data: {
        status: 1,
        data: {
          columns: ['category', 'total'],
          rows: [
            { category: '数据转换', total: 24 },
            { category: '数据补全', total: 87 },
            { category: '数据比对', total: 35 },
            { category: '数据去重', total: 39 },
          ],
        },
      },
    })
  })

  it('Test services fetchDataVolume', () => {
    expect(fetchDataVolume()).toStrictEqual({
      data: {
        status: 1,
        data: {
          list: [
            { date: '2日', area: 45, areaLimit: 100, industry: 80 },
            { date: '3日', area: 43, areaLimit: 267, industry: 120 },
            { date: '4日', area: 120, areaLimit: 122, industry: 60 },
            { date: '5日', area: 45, areaLimit: 100, industry: 80 },
            { date: '6日', area: 68, areaLimit: 267, industry: 120 },
            { date: '7日', area: 72, areaLimit: 122, industry: 60 },
            { date: '8日', area: 54, areaLimit: 82, industry: 110 },
          ],
        },
      },
    })
  })

  it('Test services fetchDataTask', () => {
    const list = [
      {
        month: '1月',
        punishment: 80,
        reward: 140,
      },
      {
        month: '2月',
        punishment: 50,
        reward: 140,
      },
      {
        month: '3月',
        punishment: 100,
        reward: 20,
      },
      {
        month: '4月',
        punishment: 70,
        reward: 120,
      },
      {
        month: '5月',
        punishment: 10,
        reward: 180,
      },
      {
        month: '6月',
        punishment: 40,
        reward: 160,
      },
    ]

    expect(fetchDataTask()).toStrictEqual({
      data: {
        status: 1,
        data: { list },
      },
    })
  })

  it('Test services fetchStaticsByUserGroup', () => {
    fetchStaticsByUserGroup(id, params)
    expect(request).toBeCalledWith(`/api/statisticals/staticsByCategory?=administrativeArea=${id}`, { params })
    expect(fetchStaticsByUserGroup(id, useMock)).toStrictEqual({
      data: {
        status: 1,
        data: {
          columns: [
            'name',
            '发展和改革委员会',
            '市场监督管理局',
            '民政局',
            '法院',
            '公安局',
            '交通厅',
          ],
          rows: {
            name: '',
            发展和改革委员会: '237',
            市场监督管理局: '261',
            民政局: '173',
            法院: '147',
            公安局: '210',
            交通厅: '220',
          },
        },
      },
    })
  })
})
