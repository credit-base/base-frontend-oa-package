jest.mock('@/utils/request')
import request from '@/utils/request'

import {
  templateList,
  templateDetail,
  templateWbjDataList,
  templateWbjDataDetail,
  taskList,
} from '@/app-modules/data-manage/helpers/mocks'
// import { getRecentMonths } from '@/plugins/echarts/utils'
// import { randomNum } from '@/utils'

import {
  fetchWbjTemplateList,
  fetchWbjTemplateDetail,
  fetchWbjSearchData,
  fetchWbjSearchDataDetail,
  updateWbjDataDisable,
  fetchWbjTaskList,
  fetchDataConversionRate,
  fetchDataVolume,
  fetchStaticsByUserGroup,
  fetchFillInOnlineInfo,
} from '@/app-modules/data-manage/services/wbj-data'

const params = {}
const id = 'MA'
const useMock = { useMock: true }

describe('Test wbj-data services', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services fetchWbjTemplateList', () => {
    fetchWbjTemplateList(params)

    expect(request).toBeCalledWith('/api/wbjTemplates', { params })
    expect(fetchWbjTemplateList(useMock)).toStrictEqual({
      list: [
        ...templateList,
      ],
      total: 3,
    })
  })

  it('Test services fetchWbjTemplateDetail', () => {
    fetchWbjTemplateDetail(id)
    expect(request).toBeCalledWith(`/api/wbjTemplates/${id}`, { params })
    expect(fetchWbjTemplateDetail(id, useMock)).toStrictEqual({
      ...templateDetail,
    })
  })

  it('Test services fetchWbjSearchData', () => {
    fetchWbjSearchData(params)
    expect(request).toBeCalledWith(`/api/wbjSearchData`, { params })
    expect(fetchWbjSearchData(useMock)).toStrictEqual({
      list: [
        ...templateWbjDataList,
      ],
      total: 8,
    })
  })

  it('Test services fetchWbjSearchDataDetail', () => {
    fetchWbjSearchDataDetail(id)
    expect(request).toBeCalledWith(`/api/wbjSearchData/${id}`)
    expect(fetchWbjSearchDataDetail(id, useMock)).toStrictEqual({
      ...templateWbjDataDetail,
    })
  })

  it('Test services updateWbjDataDisable', () => {
    updateWbjDataDisable(id)
    expect(request).toBeCalledWith(`/api/wbjSearchData/${id}/disable`, { method: 'POST' })
  })

  it('Test services fetchWbjTaskList', () => {
    fetchWbjTaskList(params)
    expect(request).toBeCalledWith(`/api/wbjTasks`, { params })
    expect(fetchWbjTaskList(useMock)).toStrictEqual({
      list: [
        ...taskList,
      ],
      total: 3,
    })
  })

  it('Test services fetchFillInOnlineInfo', () => {
    fetchFillInOnlineInfo(id, params)
    expect(request).toBeCalledWith(`/api/wbjTemplates/${id}`)
    expect(fetchFillInOnlineInfo(id, useMock)).toStrictEqual({})
  })

  it('Test services fetchDataConversionRate', () => {
    fetchDataConversionRate(id, params)
    expect(request).toBeCalledWith(`/api/statisticals/staticsByCategoryTotal?administrativeArea=${id}`, { params })
    expect(fetchDataConversionRate(id, useMock)).toStrictEqual({
      data: {
        status: 1,
        data: {
          columns: ['category', 'total'],
          rows: [
            { category: '数据转换', total: 24 },
            { category: '数据补全', total: 87 },
            { category: '数据比对', total: 35 },
            { category: '数据去重', total: 39 },
          ],
        },
      },
    })
  })

  it('Test services fetchDataVolume', () => {
    expect(fetchDataVolume()).toStrictEqual({
      data: {
        status: 1,
        data: {
          list: [
            { date: '2日', area: 45, areaLimit: 100, industry: 80 },
            { date: '3日', area: 43, areaLimit: 267, industry: 120 },
            { date: '4日', area: 120, areaLimit: 122, industry: 60 },
            { date: '5日', area: 45, areaLimit: 100, industry: 80 },
            { date: '6日', area: 68, areaLimit: 267, industry: 120 },
            { date: '7日', area: 72, areaLimit: 122, industry: 60 },
            { date: '8日', area: 54, areaLimit: 82, industry: 110 },
          ],
        },
      },
    })
  })

  it('Test services fetchStaticsByUserGroup', () => {
    fetchStaticsByUserGroup(id, params)
    expect(request).toBeCalledWith(`/api/statisticals/staticsByCategory?=administrativeArea=${id}`, { params })
    expect(fetchStaticsByUserGroup(id, useMock)).toStrictEqual({
      data: {
        status: 1,
        data: {
          columns: [
            'name',
            '发展和改革委员会',
            '市场监督管理局',
            '民政局',
            '法院',
            '公安局',
            '交通厅',
          ],
          rows: {
            name: '',
            发展和改革委员会: '237',
            市场监督管理局: '261',
            民政局: '173',
            法院: '147',
            公安局: '210',
            交通厅: '220',
          },
        },
      },
    })
  })
})
