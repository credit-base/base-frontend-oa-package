jest.mock('@/utils/request')
import request from '@/utils/request'

import {
  templateList,
  templateDetail,
  templateDataList,
  templateDataDetail,
  taskList,
} from '@/app-modules/data-manage/helpers/mocks'
// import { getRecentMonths } from '@/plugins/echarts/utils'
// import { randomNum } from '@/utils'

import {
  fetchBjTemplateList,
  fetchBjTemplateDetail,
  fetchBjSearchData,
  fetchBjSearchDataDetail,
  updateBjDataConfirm,
  updateBjDataDisable,
  updateBjDataDelete,
  fetchBjTaskList,
  fetchDataConversionRate,
  fetchDataVolume,
  fetchStaticsByUserGroup,
} from '@/app-modules/data-manage/services/bj-data'

const params = {}
const id = 'MA'
const useMock = { useMock: true }

describe('Test bj-data services', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services fetchBjTemplateList', () => {
    fetchBjTemplateList(params)

    expect(request).toBeCalledWith('/api/bjTemplates', { params })
    expect(fetchBjTemplateList(useMock)).toStrictEqual({
      list: [
        ...templateList,
      ],
      total: 3,
    })
  })

  it('Test services fetchBjTemplateDetail', () => {
    fetchBjTemplateDetail(id)
    expect(request).toBeCalledWith(`/api/bjTemplates/${id}`, { params })
    expect(fetchBjTemplateDetail(id, useMock)).toStrictEqual({
      ...templateDetail,
    })
  })

  it('Test services fetchBjSearchData', () => {
    fetchBjSearchData(params)
    expect(request).toBeCalledWith(`/api/bjSearchData`, { params })
    expect(fetchBjSearchData(useMock)).toStrictEqual({
      list: [
        ...templateDataList,
      ],
      total: 7,
    })
  })

  it('Test services fetchBjSearchDataDetail', () => {
    fetchBjSearchDataDetail(id)
    expect(request).toBeCalledWith(`/api/bjSearchData/${id}`)
    expect(fetchBjSearchDataDetail(id, useMock)).toStrictEqual({
      ...templateDataDetail,
    })
  })

  it('Test services updateBjDataConfirm', () => {
    updateBjDataConfirm(id)
    expect(request).toBeCalledWith(`/api/bjSearchData/${id}/confirm`, { method: 'POST' })
  })

  it('Test services updateBjDataDisable', () => {
    updateBjDataDisable(id)
    expect(request).toBeCalledWith(`/api/bjSearchData/${id}/disable`, { method: 'POST' })
  })

  it('Test services updateBjDataDelete', () => {
    updateBjDataDelete(id)
    expect(request).toBeCalledWith(`/api/bjSearchData/${id}/delete`, { method: 'POST' })
  })

  it('Test services fetchBjTaskList', () => {
    fetchBjTaskList(params)
    expect(request).toBeCalledWith(`/api/bjTasks`, { params })
    expect(fetchBjTaskList(useMock)).toStrictEqual({
      list: [
        ...taskList,
      ],
      total: 3,
    })
  })

  it('Test services fetchDataConversionRate', () => {
    fetchDataConversionRate(id, params)
    expect(request).toBeCalledWith(`/api/statisticals/staticsByCategoryTotal?administrativeArea=${id}`, { params })
    expect(fetchDataConversionRate(id, useMock)).toStrictEqual({
      data: {
        status: 1,
        data: {
          columns: ['category', 'total'],
          rows: [
            { category: '数据转换', total: 24 },
            { category: '数据补全', total: 87 },
            { category: '数据比对', total: 35 },
            { category: '数据去重', total: 39 },
          ],
        },
      },
    })
  })

  it('Test services fetchDataVolume', () => {
    expect(fetchDataVolume()).toStrictEqual({
      data: {
        status: 1,
        data: {
          list: [
            { date: '2日', area: 45, areaLimit: 100, industry: 80 },
            { date: '3日', area: 43, areaLimit: 267, industry: 120 },
            { date: '4日', area: 120, areaLimit: 122, industry: 60 },
            { date: '5日', area: 45, areaLimit: 100, industry: 80 },
            { date: '6日', area: 68, areaLimit: 267, industry: 120 },
            { date: '7日', area: 72, areaLimit: 122, industry: 60 },
            { date: '8日', area: 54, areaLimit: 82, industry: 110 },
          ],
        },
      },
    })
  })

  it('Test services fetchStaticsByUserGroup', () => {
    fetchStaticsByUserGroup(id, params)
    expect(request).toBeCalledWith(`/api/statisticals/staticsByCategory?=administrativeArea=${id}`, { params })
    expect(fetchStaticsByUserGroup(id, useMock)).toStrictEqual({
      data: {
        status: 1,
        data: {
          columns: [
            'name',
            '发展和改革委员会',
            '市场监督管理局',
            '民政局',
            '法院',
            '公安局',
            '交通厅',
          ],
          rows: {
            name: '',
            发展和改革委员会: '237',
            市场监督管理局: '261',
            民政局: '173',
            法院: '147',
            公安局: '210',
            交通厅: '220',
          },
        },
      },
    })
  })
})
