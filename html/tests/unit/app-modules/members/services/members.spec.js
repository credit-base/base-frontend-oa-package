jest.mock('@/utils/request')
import request from '@/utils/request'
import { updateMemberStatus, fetchMembersDetail, fetchMembers } from '@/app-modules/members/services/members'
import {
  membersList,
  membersDetail,
} from '@/app-modules/members/helpers/mocks'

const params = {}
const id = global.randomString()
const useMock = { useMock: true }

describe('Test Services - members', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services fetchMembers', () => {
    fetchMembers(params)
    expect(request).toBeCalledWith('/api/members', { method: 'GET', params })

    expect(fetchMembers(useMock)).toStrictEqual({
      list: [
        ...membersList,
      ],
      total: 1,
    })
  })

  it('Test services fetchMembersDetail', () => {
    fetchMembersDetail(id, params)

    expect(request).toBeCalledWith(`/api/members/${id}`)
    expect(fetchMembersDetail(id, useMock)).toStrictEqual({
      ...membersDetail,
    })
  })

  it('Test services updateMemberStatus', () => {
    const type = 'enable'
    updateMemberStatus(id, type)
    expect(request).toBeCalledWith(`/api/members/${id}/${type}`, { method: 'POST' })
  })
})
