jest.mock('@/app-modules/members/services/members')
jest.mock('@/utils/element')
import MemberDetail from '@/app-modules/members/views/detail'
import { openConfirm } from '@/utils/element'
import { updateMemberStatus, fetchMembersDetail } from '@/app-modules/members/services/members'

const id = global.randomString()
const { shallowMount, createComponentMocks } = global

describe('Test vue MemberDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(MemberDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
            params: {
              id,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchMembersDetail', async () => {
    fetchMembersDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(fetchMembersDetail).toBeCalled()
  })

  it('Test methods changeStatus - updateMemberStatus', async () => {
    openConfirm.mockResolvedValue()
    updateMemberStatus.mockResolvedValue()

    await wrapper.vm.changeStatus('enable', id)
    expect(updateMemberStatus).toBeCalled()
  })

  it('Test MemberDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
