
import ListViewMembers from '@/app-modules/members/views/components/list-view-members'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()

describe('Test vue ListViewMembers', () => {
  let wrapper
  const store = {
    getters: {
      userInfo: {
        id,
      },
    },
  }
  const onCommand = jest.fn()
  const onSort = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(ListViewMembers, {
      ...createComponentMocks({
        mocks: {
          $store: store,
        },
      }),
      listeners: {
        command: onCommand,
        sort: onSort,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleCommand', async () => {
    await wrapper.vm.handleCommand('VIEW', { id })
    expect(onCommand).toBeCalledWith('VIEW', id)
  })

  it('Test methods sortChange', async () => {
    let column = {
      prop: 'updateTime', // updateTime
      order: 'ascending', // '', ascending, descending
    }
    await wrapper.vm.sortChange(column)
    expect(onSort).toBeCalledWith('updateTime')
    column = {
      prop: 'updateTime',
      order: 'descending',
    }
    await wrapper.vm.sortChange(column)
    expect(onSort).toBeCalledWith('-updateTime')
    column = {
      prop: '',
      order: '',
    }
    await wrapper.vm.sortChange(column)
    expect(onSort).toBeCalledWith(undefined)
  })

  it('Test ListViewMembers Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
