jest.mock('@/app-modules/members/services/members')
jest.mock('@/utils/element')
import MemberList from '@/app-modules/members/views/list'
import { openConfirm } from '@/utils/element'
import { updateMemberStatus, fetchMembers } from '@/app-modules/members/services/members'

const id = global.randomString()
const { shallowMount, createComponentMocks } = global

describe('Test vue MemberList', () => {
  let wrapper
  const onRoutePush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(MemberList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $router: {
            push: onRoutePush,
          },
        },
      }),
    })

    // await flushPromises()
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchMembers', async () => {
    const res = {
      status: 1,
      data: {
        list: [],
        total: 0,
      },
    }
    fetchMembers.mockResolvedValue({ ...res })
    await wrapper.vm.fetchData()
    expect(fetchMembers).toBeCalled()
  })

  it('Test methods changeStatus - updateMemberStatus', async () => {
    openConfirm.mockResolvedValue()
    updateMemberStatus.mockResolvedValue()

    await wrapper.vm.changeStatus('enable', id)
    expect(updateMemberStatus).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    updateMemberStatus.mockResolvedValue()
    openConfirm.mockResolvedValue()
    let command = 'VIEW'
    await wrapper.vm.handleCommand(command, id)
    expect(wrapper.vm.$router.push).toBeCalledWith({ path: `/members/detail/${id}`, query: { route: `/members/list` } })
    command = 'ENABLE'
    await wrapper.vm.handleCommand(command, id)
    expect(updateMemberStatus).toBeCalled()
    command = 'DISABLE'
    await wrapper.vm.handleCommand(command, id)
    expect(updateMemberStatus).toBeCalled()
  })

  it('Test methods initQuery', () => {
    wrapper.vm.initQuery()
    expect(wrapper.vm.query).toStrictEqual({
      limit: 10,
      page: 1,
    })
  })

  it('Test methods searchData', () => {
    const query = {
      limit: 10,
      page: 1,
    }
    wrapper.vm.searchData()
    fetchMembers.mockResolvedValue(query)
    expect(fetchMembers).toBeCalledWith(query)
  })

  it('Test methods sortChange', async () => {
    const value = 'updateTime'
    const query = {
      limit: 10,
      page: 1,
      sort: value,
    }
    await wrapper.vm.sortChange(value)
    fetchMembers.mockResolvedValue(query)

    expect(fetchMembers).toBeCalledWith(query)
  })

  it('Test methods filterData', () => {
    const statusId = 'Lw'
    const query = {
      limit: 10,
      page: 1,
      status: statusId,
    }

    wrapper.vm.filterData(statusId)
    fetchMembers.mockResolvedValue(query)
    expect(fetchMembers).toBeCalledWith(query)
  })

  it('Test MemberList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
