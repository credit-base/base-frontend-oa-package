import GbTemplateAdd from '@/app-modules/resource-catalog/views/add/gb-template'

describe('Test component GbTemplateAdd', () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(GbTemplateAdd, {
      ...global.createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it('Test component GbTemplateAdd snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
