/**
 * @file 本级资源目录 - 新增
 * @module add/bjTemplate
 */

import BjTemplateAdd from '@/app-modules/resource-catalog/views/add/bj-template.vue'

describe('BjTemplateAdd', () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(BjTemplateAdd, {
      ...global.createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
