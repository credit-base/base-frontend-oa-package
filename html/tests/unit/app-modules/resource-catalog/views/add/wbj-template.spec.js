import WbjTemplateAdd from '@/app-modules/resource-catalog/views/add/wbj-template'

describe('Test component WbjTemplateAdd', () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(WbjTemplateAdd, {
      ...global.createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it('Test component WbjTemplateAdd snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
