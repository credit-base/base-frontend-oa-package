/**
 * @file 工单任务 - 新增
 * @module add/order-task
 */

import OrderTask from '@/app-modules/resource-catalog/views/add/order-task'

describe('OrderTask - add', () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(OrderTask, {
      ...global.createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
