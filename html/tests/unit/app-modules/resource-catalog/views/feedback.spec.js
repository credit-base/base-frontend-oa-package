import Feedback from '@/app-modules/resource-catalog/views/feedback.vue'

describe('Feedback', () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(Feedback, {
      ...global.createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
