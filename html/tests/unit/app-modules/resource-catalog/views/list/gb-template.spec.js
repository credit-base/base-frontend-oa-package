import GbTemplate from '@/app-modules/resource-catalog/views/list/gb-template.vue'

const { shallowMount, createComponentMocks } = global

describe('Test vue GbTemplate', () => {
  let wrapper
  // const onRouterPush = jest.fn()

  beforeEach(() => {
    jest.resetModules()
    jest.resetAllMocks()

    wrapper = shallowMount(GbTemplate, {
      ...createComponentMocks({
        router: true,
        mocks: {
          // $route: { query: {} },
          // $router: {
          //   push: onRouterPush,
          // },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test component list-view-common commandChange', () => {
    // const listView = wrapper.find('listviewcommon-stub')
    // const id = 'MA'
    // listView.vm.$emit('command', { id, command: wrapper.vm.$CONSTANTS.COMMAND_TYPE.VIEW })
    // expect(onRouterPush).toBeCalledWith(`/resource-catalog/gb-template/detail/${id}`)
    // expect(onRouterPush).toBeCalled()
    // listView.vm.$emit('command', { id, command: wrapper.vm.$CONSTANTS.COMMAND_TYPE.EDIT })
    // expect(onRouterPush).toBeCalledWith(`/resource-catalog/gb-template/edit/${id}`)
    // expect(onRouterPush).toBeCalled()
  })

  // it('Test methods sortChange', () => {
  //   const listView = wrapper.find('listviewcommon-stub')
  //   listView.$emit('sort', 'updateTime')
  //   // expect(wrapper.vm.sortChange()).toBeCalled()
  // })
})
