import Bjtemplate from '@/app-modules/resource-catalog/views/list/bj-template.vue'

const { shallowMount, createComponentMocks } = global

describe('Test vue Bjtemplate', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(Bjtemplate, {
      ...createComponentMocks({
        // router: true,
        mocks: {
          // $route: { fullPath: '/resource-catalog-subsystem' },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  // it('Test resource-catalog Bjtemplate Snapshot', () => {
  //   expect(wrapper.html()).toMatchSnapshot()
  // })
})
