import Index from '@/app-modules/resource-catalog/views'

const { shallowMount, createComponentMocks } = global

describe('Test vue Index', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(Index, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: { fullPath: '/resource-catalog-subsystem' },
        },
      }),
    })
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test resource-catalog Index Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
