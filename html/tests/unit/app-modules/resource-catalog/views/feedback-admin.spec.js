import FeedbackAdmin from '@/app-modules/resource-catalog/views/feedback-admin.vue'

describe('FeedbackAdmin', () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(FeedbackAdmin, {
      ...global.createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
