import ListViewCommon from '@/app-modules/resource-catalog/views/components/list-view-common.vue'
import { TEMPLATE_CATEGORY } from '@/app-modules/resource-catalog/helpers/constants'

describe('ListViewCommon', () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(ListViewCommon, {
      ...global.createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it('Test computed isWbjTemplate', () => {
    const isWbjTemplate = ListViewCommon.computed.isWbjTemplate
    const { BJ, WBJ } = TEMPLATE_CATEGORY
    expect(isWbjTemplate.call({ category: 'user-group', WBJ })).toBeTruthy()
    expect(isWbjTemplate.call({ category: 'current-group', WBJ })).toBeFalsy()

    const isBjtemplate = ListViewCommon.computed.isBjtemplate
    expect(isBjtemplate.call({ category: 'current-unit', BJ })).toBeTruthy()
    expect(isBjtemplate.call({ category: 'user-group', BJ })).toBeFalsy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
