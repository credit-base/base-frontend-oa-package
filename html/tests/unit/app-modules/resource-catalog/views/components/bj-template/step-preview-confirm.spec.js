/**
 * @file 本级资源目录 - 预览确认
 * @module components/step-preview-confirm
 */

import StepPreviewConfirm from '@/app-modules/resource-catalog/views/components/bj-template/step-preview-confirm.vue'

const { shallowMount, createComponentMocks } = global

describe(`components - step-preview-confirm`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(StepPreviewConfirm, {
      ...createComponentMocks({
        mocks: {

        },
      }),
      propsData: { catalogInfo: {}, templateInfo: [] },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
