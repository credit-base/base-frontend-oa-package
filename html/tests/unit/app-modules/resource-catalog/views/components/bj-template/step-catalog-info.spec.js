/**
 * @file 本级资源目录 - 目录信息
 * @module components/step-catalog-info
 */

import StepCatalogInfo from '@/app-modules/resource-catalog/views/components/bj-template/step-catalog-info.vue'

describe(`components - step-catalog-info`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(StepCatalogInfo, {
      ...global.createComponentMocks({}),
      propsData: { payload: {} },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
