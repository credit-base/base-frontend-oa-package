/**
 * @file 本级资源目录 - 国标目录
 * @module components/step-nation-catalog
 */

import StepNationCatalog from '@/app-modules/resource-catalog/views/components/bj-template/step-nation-catalog.vue'

const { shallowMount, createComponentMocks } = global

describe(`components - step-nation-catalog`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(StepNationCatalog, {
      ...createComponentMocks({}),
      propsData: { payload: '' },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
