/**
 * @file 本级资源目录 - 模板信息
 * @module components/step-template-info
 */

import StepTemplateInfo from '@/app-modules/resource-catalog/views/components/bj-template/step-template-info.vue'

const { shallowMount, createComponentMocks } = global

describe(`components - step-template-info`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(StepTemplateInfo, {
      ...createComponentMocks({
        mocks: {},
      }),
      propsData: {
        payload: [],
        catalogInfo: {},
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
