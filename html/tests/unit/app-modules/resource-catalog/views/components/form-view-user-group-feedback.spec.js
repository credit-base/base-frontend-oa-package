/**
 * @file 工单任务 - 委办局 - 反馈
 * @module components/form-view-user-group-feedback
 */

import FormViewUserGroupFeedback from '@/app-modules/resource-catalog/views/components/form-view-user-group-feedback.vue'

const { shallowMount, createComponentMocks } = global

describe(`components - form-view-user-group-feedback`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormViewUserGroupFeedback, {
      ...createComponentMocks({
        mocks: {

        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  // it(`shallowMount`, () => {
  //   expect(wrapper.exists()).toBeTruthy()
  // })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
