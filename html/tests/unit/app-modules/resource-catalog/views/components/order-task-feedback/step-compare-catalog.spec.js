/**
 * @file 工单任务反馈 - 目录对比
 * @module order-task-feedback/step-catalog-compare
 */

import StepCatalogCompare from '@/app-modules/resource-catalog/views/components/order-task-feedback/step-catalog-compare.vue'

describe(`StepCatalogCompare`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(StepCatalogCompare, {
      ...global.createComponentMocks({
        mocks: {},
        propsData: { catalog: {} },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
