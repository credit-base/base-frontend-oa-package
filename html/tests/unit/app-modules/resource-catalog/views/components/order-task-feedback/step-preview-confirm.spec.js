/**
 * @file 工单任务反馈 - 预览确认
 * @module order-task-feedback/step-preview-confirm-exist
 */

import StepPreviewConfirm from '@/app-modules/resource-catalog/views/components/order-task-feedback/step-preview-confirm-exist.vue'

describe(`StepPreviewConfirm`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(StepPreviewConfirm, {
      ...global.createComponentMocks({
        mocks: {},
        propsData: { catalog: {} },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
