/**
 * @file 工单任务反馈 - 选择目录
 * @module order-task-feedback/step-select-catalog
 */

import StepSelectCatalog from '@/app-modules/resource-catalog/views/components/order-task-feedback/step-select-catalog.vue'

describe(`StepSelectCatalog`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(StepSelectCatalog, {
      ...global.createComponentMocks({}),
      propsData: { catalogId: 'MA' },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
