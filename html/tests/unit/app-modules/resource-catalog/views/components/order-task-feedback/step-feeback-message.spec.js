/**
 * @file 工单任务反馈 - 反馈原因
 * @module order-task-feedback/step-feedback-message
 */

import StepFeedbackMessage from '@/app-modules/resource-catalog/views/components/order-task-feedback/step-feedback-message.vue'

describe(`StepFeedbackMessage`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(StepFeedbackMessage, {
      ...global.createComponentMocks({
        mocks: {},
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
