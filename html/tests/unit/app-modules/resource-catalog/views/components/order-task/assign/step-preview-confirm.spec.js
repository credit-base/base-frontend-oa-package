/**
 * @file 工单任务指派 - 预览确认
 * @module order-task-assign/step-preview-confirm
 */

import StepPreviewConfirm from '@/app-modules/resource-catalog/views/components/order-task/assign/step-fill-information.vue'

describe(`StepPreviewConfirm`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(StepPreviewConfirm, {
      ...global.createComponentMocks({}),
      propsData: { catalog: {}, task: {} },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
