/**
 * @file 工单任务指派 - 选择目录
 * @module order-task-assign/step-select-catalog
 */

jest.mock('@/app-modules/resource-catalog/services/gb-template')
jest.mock('@/app-modules/resource-catalog/services/bj-template')
import StepSelectCatalog from '@/app-modules/resource-catalog/views/components/order-task/assign/step-select-catalog.vue'

describe(`StepSelectCatalog`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(StepSelectCatalog, {
      ...global.createComponentMocks({}),
      propsData: { catalogType: 'MA', catalogId: 'MA' },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
