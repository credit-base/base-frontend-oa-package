/**
 * @file 工单任务 - 指派
 * @module components/form-view
 */

import FormView from '@/app-modules/resource-catalog/views/components/order-task/assign/form-view.vue'

describe(`components - form-view`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(FormView, {
      ...global.createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
