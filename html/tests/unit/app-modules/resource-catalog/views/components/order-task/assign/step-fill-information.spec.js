/**
 * @file 工单任务指派 - 填写信息
 * @module order-task-assign/step-fill-information
 */

import StepFillInformation from '@/app-modules/resource-catalog/views/components/order-task/assign/step-fill-information.vue'

describe(`StepFillInformation`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(StepFillInformation, {
      ...global.createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
