/**
 * @file 工单任务指派 - 基本目录
 * @module order-task-assign/step-base-catalog
 */

import StepBaseCatalog from '@/app-modules/resource-catalog/views/components/order-task/assign/step-base-catalog.vue'

describe(`StepBaseCatalog`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(StepBaseCatalog, {
      ...global.createComponentMocks({}),
      propsData: { catalogType: '' },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
