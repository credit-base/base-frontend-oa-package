/**
 * @file 本级资源目录 - 表单
 * @module components/form-view-bj-template
 */

import FormViewBjtemplate from '@/app-modules/resource-catalog/views/components/form-view-bj-template.vue'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()

describe(`components - form-view-bj-template - common`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormViewBjtemplate, {
      ...createComponentMocks(),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })
})

describe(`components - form-view-bj-template - add`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormViewBjtemplate, {
      ...createComponentMocks(),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})

describe(`components - form-view-bj-template - edit`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormViewBjtemplate, {
      ...createComponentMocks({
        mocks: {
          $route: { params: { id } },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`Computed`, () => {
    expect(wrapper.vm.id).toBe(id)
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
