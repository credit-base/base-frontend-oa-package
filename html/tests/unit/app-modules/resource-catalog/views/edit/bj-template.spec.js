/**
 * @file 本级资源目录 - 编辑
 * @module edit/bjTemplate
 */

import BjTemplate from '@/app-modules/resource-catalog/views/edit/bj-template.vue'

const { shallowMount, createComponentMocks } = global

describe('bjTemplate - edit', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(BjTemplate, {
      ...createComponentMocks({
        mocks: {

        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`shallowMount`, () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  it(`snapshot`, () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
