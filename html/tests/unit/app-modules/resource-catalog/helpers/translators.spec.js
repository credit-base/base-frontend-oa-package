import {
  translateGbTemplateList,
  translateGbTemplateDetail,
  translateGbTemplateVersionList,
  translateBjTemplateList,
  translateBjTemplateDetail,
  translateBjTemplateVersionList,
  translateWbjTemplateList,
  translateWbjTemplateDetail,
  translateWbjTemplateVersionList,
} from '@/app-modules/resource-catalog/helpers/translators'
import {
  gbTemplateList,
  gbTemplateDetail,
  gbTemplateVersionList,
  bjTemplateList,
  bjTemplateDetail,
  bjTemplateVersionList,
  wbjTemplateList,
  wbjTemplateDetail,
  wbjTemplateVersionList,
} from '@/app-modules/resource-catalog/helpers/mocks'

const noData = {}
const errData = {
  list: {},
}
const resData = {
  list: [],
  total: 0,
}

describe('Test translators', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  // GbTemplate
  it('Test translateGbTemplateList', () => {
    const res = {
      list: [
        ...gbTemplateList,
      ],
      total: 1,
    }
    expect(translateGbTemplateList()).toStrictEqual(resData)

    expect(translateGbTemplateList(noData)).toStrictEqual(resData)

    expect(translateGbTemplateList(errData)).toStrictEqual(resData)

    expect(translateGbTemplateList(res)).toStrictEqual(res)
  })

  it('Test translateGbTemplateDetail', () => {
    const emptyGbTemplateDetail = {
      id: undefined,
      name: undefined,
      identify: undefined,
      subjectCategory: [],
      dimension: {},
      exchangeFrequency: {},
      infoClassify: {},
      infoCategory: {},
      description: undefined,
      items: [],
      updateTime: undefined,
      version: undefined,
      year: undefined,
      createTime: undefined,
      crew: {},
    }

    expect(translateGbTemplateDetail()).toStrictEqual({
      ...emptyGbTemplateDetail,
    })

    expect(translateGbTemplateDetail(noData)).toStrictEqual({
      ...emptyGbTemplateDetail,
    })
    expect(translateGbTemplateDetail(gbTemplateDetail)).toStrictEqual(gbTemplateDetail)
  })

  it('Test translateGbTemplateVersionList', () => {
    const res = {
      list: [
        ...gbTemplateVersionList,
      ],
      total: 0,
    }

    expect(translateGbTemplateVersionList()).toStrictEqual(resData)

    expect(translateGbTemplateVersionList(noData)).toStrictEqual(resData)

    expect(translateGbTemplateVersionList(errData)).toStrictEqual(resData)

    expect(translateGbTemplateVersionList(res)).toStrictEqual(res)
  })

  // Bjtemplate
  it('Test translateBjTemplateList', () => {
    const res = {
      list: [
        ...bjTemplateList,
      ],
      total: 1,
    }
    expect(translateBjTemplateList()).toStrictEqual(resData)

    expect(translateBjTemplateList(noData)).toStrictEqual(resData)

    expect(translateBjTemplateList(errData)).toStrictEqual(resData)

    expect(translateBjTemplateList(res)).toStrictEqual(res)
  })

  it('Test translateBjTemplateDetail', () => {
    const emptyBjtemplateDetail = {
      id: undefined,
      name: undefined,
      identify: undefined,
      category: undefined,
      subjectCategory: [],
      dimension: {},
      exchangeFrequency: {},
      gbTemplate: {},
      infoClassify: {},
      infoCategory: {},
      description: undefined,
      items: [],
      sourceUnit: {},
      updateTime: undefined,
      version: undefined,
      year: undefined,
      createTime: undefined,
      crew: {},
    }

    expect(translateBjTemplateDetail()).toStrictEqual({
      ...emptyBjtemplateDetail,
    })

    expect(translateBjTemplateDetail(noData)).toStrictEqual({
      ...emptyBjtemplateDetail,
    })
    expect(translateBjTemplateDetail(bjTemplateDetail)).toStrictEqual(bjTemplateDetail)
  })

  it('Test translateBjTemplateVersionList', () => {
    const res = {
      list: [
        ...bjTemplateVersionList,
      ],
      total: 0,
    }

    expect(translateBjTemplateVersionList()).toStrictEqual(resData)

    expect(translateBjTemplateVersionList(noData)).toStrictEqual(resData)

    expect(translateBjTemplateVersionList(errData)).toStrictEqual(resData)

    expect(translateBjTemplateVersionList(res)).toStrictEqual(res)
  })

  // WbjTemplate
  it('Test translateWbjTemplateList', () => {
    const res = {
      list: [
        ...wbjTemplateList,
      ],
      total: 1,
    }
    expect(translateWbjTemplateList()).toStrictEqual(resData)

    expect(translateWbjTemplateList(noData)).toStrictEqual(resData)

    expect(translateWbjTemplateList(errData)).toStrictEqual(resData)

    expect(translateWbjTemplateList(res)).toStrictEqual(res)
  })

  it('Test translateWbjTemplateDetail', () => {
    const emptyWbjTemplateDetail = {
      id: undefined,
      name: undefined,
      identify: undefined,
      subjectCategory: [],
      dimension: {},
      exchangeFrequency: {},
      infoClassify: {},
      infoCategory: {},
      sourceUnit: {},
      description: undefined,
      items: [],
      updateTime: undefined,
      version: undefined,
      year: undefined,
      createTime: undefined,
      crew: {},
    }

    expect(translateWbjTemplateDetail()).toStrictEqual({
      ...emptyWbjTemplateDetail,
    })

    expect(translateWbjTemplateDetail(noData)).toStrictEqual({
      ...emptyWbjTemplateDetail,
    })
    expect(translateWbjTemplateDetail(wbjTemplateDetail)).toStrictEqual(wbjTemplateDetail)
  })

  it('Test translateWbjTemplateVersionList', () => {
    const res = {
      list: [
        ...wbjTemplateVersionList,
      ],
      total: 0,
    }

    expect(translateWbjTemplateVersionList()).toStrictEqual(resData)

    expect(translateWbjTemplateVersionList(noData)).toStrictEqual(resData)

    expect(translateWbjTemplateVersionList(errData)).toStrictEqual(resData)

    expect(translateWbjTemplateVersionList(res)).toStrictEqual(res)
  })
})
