jest.mock('@/utils/request')
import request from '@/utils/request'
import {
  fetchBjTemplateList,
  fetchBjTemplateDetail,
  createBjTemplate,
  fetchBjTemplateEditInfo,
  updateBjTemplateInfo,
  fetchBjTemplateVersionList,
  addBjTemplate,
  editBjTemplate,
  fetchTemplateConfigs,
} from '@/app-modules/resource-catalog/services/bj-template'
import {
  bjTemplateList,
  bjTemplateDetail,
  bjTemplateVersionList,
} from '@/app-modules/resource-catalog/helpers/mocks'

const params = {}
const id = 'MA'
const useMock = { useMock: true }

describe('Services - bjTemplate', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services fetchBjTemplateList', () => {
    fetchBjTemplateList(params)

    // 测试正常请求
    expect(request).toBeCalledWith('/api/bjTemplates', { params })

    // 测试使用mock
    expect(fetchBjTemplateList(useMock)).toStrictEqual({
      list: [
        ...bjTemplateList,
      ],
      total: 1,
    })
  })

  it('Test services fetchBjTemplateDetail', () => {
    // 测试传递ID请求接口
    fetchBjTemplateDetail(id, params)

    expect(request).toBeCalledWith(`api/bjTemplates/${id}`, { params })

    // 测试mock
    expect(fetchBjTemplateDetail(id, useMock)).toStrictEqual({ ...bjTemplateDetail })
  })

  it('Test services createBjTemplate', () => {
    createBjTemplate(params)

    expect(request).toBeCalledWith('/api/bjTemplates/add', { method: 'POST', params })
  })

  it('Test services fetchBjTemplateEditInfo', () => {
    fetchBjTemplateEditInfo(id, params)
    expect(request).toBeCalledWith(`/api/bjTemplates/${id}`, { params })

    expect(fetchBjTemplateEditInfo(id, useMock)).toStrictEqual({
      status: 1,
      data: bjTemplateDetail,
    })
  })

  it('Test services updateBjTemplateInfo', () => {
    updateBjTemplateInfo(id, params)
    expect(request).toBeCalledWith(`/api/bjTemplates/${id}/edit`, { method: 'POST', params })
  })

  it('Test services fetchBjTemplateVersionList', () => {
    fetchBjTemplateVersionList(id, params)

    expect(request).toBeCalledWith(`/api/bjTemplates/${id}/versions`, { params })

    expect(fetchBjTemplateVersionList(id, useMock)).toStrictEqual({
      list: [
        ...bjTemplateVersionList,
      ],
      total: 1,
    })
  })

  it(`addBjTemplate`, () => {
    addBjTemplate(params)

    expect(request).toBeCalledWith(`/api/bjTemplates/add`, { method: 'POST', params })
  })

  it(`editBjTemplate`, () => {
    editBjTemplate(id, params)

    expect(request).toBeCalledWith(`/api/bjTemplates/${id}/edit`, { method: 'POST', params })
  })

  it(`fetchTemplateConfigs`, () => {
    fetchTemplateConfigs(params)

    expect(request).toBeCalledWith(`/api/template/configure`, { params })
  })
})
