/**
 * @file 工单任务 - 接口 - 测试
 * @module services/work-order-task
 */

jest.mock('@/utils/request')
import request from '@/utils/request'
import {
  fetchSubtasks,
  addWorkOrderTask,
  fetchWorkOrderTasksList,
  fetchWorkOrderTasksDetail,
} from '@/app-modules/resource-catalog/services/work-order-task'
import {
  subtasksList,
  workOrderTasksList,
  workOrderTasksDetail,
} from '@/app-modules/resource-catalog/helpers/mocks'

const params = {}
const id = 'MA'
const useMock = { useMock: true }

describe(`Services - work-order-task`, () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it(`fetchSubtasks`, () => {
    fetchSubtasks(params)

    expect(request).toBeCalledWith(`/api/workOrderTasks`, { params })

    expect(fetchSubtasks(useMock)).toStrictEqual({
      list: [
        ...subtasksList,
      ],
      total: 1,
    })
  })

  it(`addWorkOrderTask`, () => {
    addWorkOrderTask(params)

    expect(request).toBeCalledWith(`/api/parentTasks/assign`, { method: 'POST', params })
  })

  it(`fetchWorkOrderTasksList`, () => {
    fetchWorkOrderTasksList(params)

    expect(request).toBeCalledWith(`/api/parentTasks`, { params })

    expect(fetchWorkOrderTasksList(useMock)).toStrictEqual({
      list: [
        ...workOrderTasksList,
      ],
      total: 1,
    })
  })

  it(`fetchWorkOrderTasksDetail`, () => {
    fetchWorkOrderTasksDetail(id, params)

    expect(request).toBeCalledWith(`/api/workOrderTasks/${id}`, { params })

    expect(fetchWorkOrderTasksDetail(id, useMock)).toStrictEqual(workOrderTasksDetail)
  })
})
