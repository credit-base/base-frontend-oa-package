import request from '@/utils/request'
import {
  fetchWbjTemplateList,
  fetchWbjTemplateDetail,
  createWbjTemplate,
  fetchWbjTemplateEditInfo,
  updateWbjTemplateInfo,
  fetchWbjTemplateVersionList,
} from '@/app-modules/resource-catalog/services/wbj-template'

import {
  wbjTemplateList,
  wbjTemplateDetail,
  wbjTemplateVersionList,
} from '@/app-modules/resource-catalog/helpers/mocks'

jest.mock('@/utils/request')

const params = {}
const id = 'MA'
const useMock = { useMock: true }

describe('Services - wbjTemplate', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services fetchWbjTemplateList', () => {
    fetchWbjTemplateList(params)

    // 测试正常请求
    expect(request).toBeCalledWith(
      '/api/wbjTemplates',
      { method: 'GET', params },
    )

    // 测试使用mock
    expect(fetchWbjTemplateList(useMock)).toStrictEqual({
      list: [
        ...wbjTemplateList,
      ],
      total: 1,
    })
  })

  it('Test services fetchWbjTemplateDetail', () => {
    // 测试传递ID请求接口
    fetchWbjTemplateDetail(id)

    expect(request).toBeCalledWith(
      `api/wbjTemplates/${id}`,
      { method: 'GET' },
    )

    // 测试mock
    expect(fetchWbjTemplateDetail(id, useMock)).toStrictEqual({ ...wbjTemplateDetail })
  })

  it('Test services createWbjTemplate', () => {
    createWbjTemplate(params)

    expect(request).toBeCalledWith('/api/wbjTemplates/add', { method: 'POST', params })
  })

  it('Test services fetchWbjTemplateEditInfo', () => {
    fetchWbjTemplateEditInfo(id, params)
    expect(request).toBeCalledWith(`/api/wbjTemplates/${id}`, { method: 'GET' })

    fetchWbjTemplateEditInfo(id)
    expect(request).toBeCalledWith(`/api/wbjTemplates/${id}`, { method: 'GET' })

    expect(fetchWbjTemplateEditInfo(id, useMock)).toStrictEqual({
      status: 1,
      data: wbjTemplateDetail,
    })
  })

  it('Test services updateWbjTemplateInfo', () => {
    updateWbjTemplateInfo(id, params)
    expect(request).toBeCalledWith(`/api/wbjTemplates/${id}/edit`, { method: 'POST', params })
  })

  it('Test services fetchWbjTemplateVersionList', () => {
    fetchWbjTemplateVersionList(id, params)

    expect(request).toBeCalledWith(`/api/wbjTemplates/${id}/versions`, params)

    expect(fetchWbjTemplateVersionList(id, useMock)).toStrictEqual({
      list: [
        ...wbjTemplateVersionList,
      ],
      total: 1,
    })
  })
})
