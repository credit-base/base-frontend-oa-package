jest.mock('@/utils/request')
import request from '@/utils/request'
import {
  fetchGbTemplateList,
  fetchGbTemplateDetail,
  createGbTemplate,
  fetchGbTemplateEditInfo,
  updateGbTemplateInfo,
  fetchGbTemplateVersionList,
} from '@/app-modules/resource-catalog/services/gb-template'
import { gbTemplateVersionList } from '@/app-modules/resource-catalog/helpers/mocks'

const params = {}
const id = 'MA'
const useMock = { useMock: true }

describe('Services - gbTemplate', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services fetchGbTemplateList', () => {
    fetchGbTemplateList(params)

    expect(request).toBeCalledWith('/api/gbTemplates', { params })
  })

  it('Test services fetchGbTemplateDetail', () => {
    fetchGbTemplateDetail(id, params)

    expect(request).toBeCalledWith(`/api/gbTemplates/${id}`, { params })
  })

  it('Test services createGbTemplate', () => {
    createGbTemplate(params)

    expect(request).toBeCalledWith('/api/gbTemplates/add', { method: 'POST', params })
  })

  it('Test services fetchGbTemplateEditInfo', () => {
    fetchGbTemplateEditInfo(id, params)
    expect(request).toBeCalledWith(`/api/gbTemplates/${id}`, { params })
  })

  it('Test services updateGbTemplateInfo', () => {
    updateGbTemplateInfo(id, params)
    expect(request).toBeCalledWith(`/api/gbTemplates/${id}/edit`, { method: 'POST', params })
  })

  it('Test services fetchGbTemplateVersionList', () => {
    fetchGbTemplateVersionList(id, params)

    expect(request).toBeCalledWith(`/api/gbTemplates/${id}/versions`, params)

    expect(fetchGbTemplateVersionList(id, useMock)).toStrictEqual({
      list: [
        ...gbTemplateVersionList,
      ],
      total: 1,
    })
  })
})
