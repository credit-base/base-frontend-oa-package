jest.mock('@/utils/request')
import request from '@/utils/request'
import {
  fetchNewsList,
  fetchNewsDetail,
  createNews,
  fetchEditNews,
  updateNews,
  updateEnableNews,
  updateDisableNews,
  updateTopNews,
  updateCancelTopNews,
  updateMoveNews,
  fetchUnAuditedNewsList,
  fetchUnAuditedNewsDetail,
  fetchEditUnAuditedNews,
  updateUnAuditedNews,
  updateApproveUnAuditedNews,
  updateRejectUnAuditedNews,
  fetchNewsType,
} from '@/app-modules/news/services/news'

const params = {}
const id = 'MA'
const newsTypeId = 'MA'

describe('Test methods services', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services fetchNewsList', () => {
    fetchNewsList(params)

    expect(request).toBeCalledWith('/api/news', { params })
  })

  it('Test services fetchNewsDetail', () => {
    fetchNewsDetail(id, params)
    expect(request).toBeCalledWith(`/api/news/${id}`, { params })
  })

  it('Test services createNews', () => {
    createNews(params)
    expect(request).toBeCalledWith('/api/news/add', { method: 'POST', params })
  })

  it('Test services fetchEditNews', () => {
    fetchEditNews(id, params)
    expect(request).toBeCalledWith(`/api/news/${id}`, { params })
  })

  it('Test services updateNews', () => {
    updateNews(id, params)
    expect(request).toBeCalledWith(`/api/news/${id}/edit`, { method: 'POST', params })
  })

  it('Test services updateEnableNews', () => {
    updateEnableNews(id)
    expect(request).toBeCalledWith(`/api/news/${id}/enable`, { method: 'POST' })
  })

  it('Test services updateDisableNews', () => {
    updateDisableNews(id)
    expect(request).toBeCalledWith(`/api/news/${id}/disable`, { method: 'POST' })
  })

  it('Test services updateTopNews', () => {
    updateTopNews(id)
    expect(request).toBeCalledWith(`/api/news/${id}/top`, { method: 'POST' })
  })

  it('Test services updateCancelTopNews', () => {
    updateCancelTopNews(id)
    expect(request).toBeCalledWith(`/api/news/${id}/cancelTop`, { method: 'POST' })
  })

  it('Test services updateMoveNews', () => {
    updateMoveNews(id, newsTypeId)
    expect(request).toBeCalledWith(`/api/news/${id}/move/${newsTypeId}`, { method: 'POST' })
  })

  it('Test services fetchUnAuditedNewsList', () => {
    fetchUnAuditedNewsList(params)

    expect(request).toBeCalledWith('/api/unAuditedNews', { params })
  })

  it('Test services fetchUnAuditedNewsDetail', () => {
    fetchUnAuditedNewsDetail(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedNews/${id}`, { params })
  })

  it('Test services fetchEditUnAuditedNews', () => {
    fetchEditUnAuditedNews(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedNews/${id}`, { params })
  })

  it('Test services updateUnAuditedNews', () => {
    updateUnAuditedNews(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedNews/${id}/edit`, { method: 'POST', params })
  })

  it('Test services updateApproveUnAuditedNews', () => {
    updateApproveUnAuditedNews(id)
    expect(request).toBeCalledWith(`/api/unAuditedNews/${id}/approve`, { method: 'POST' })
  })

  it('Test services updateRejectUnAuditedNews', () => {
    updateRejectUnAuditedNews(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedNews/${id}/reject`, { method: 'POST', params })
  })

  it('Test services fetchNewsType', () => {
    fetchNewsType(params)
    expect(request).toBeCalledWith(`/api/newsType`, { params })
  })
})
