jest.mock('@/app-modules/news/services/news')
jest.mock('@/utils/element')
import AuditDetail from '@/app-modules/news/views/detail/audit-detail'
import {
  fetchUnAuditedNewsDetail,
  updateApproveUnAuditedNews,
  updateRejectUnAuditedNews,
} from '@/app-modules/news/services/news'
import {
  unAuditedNewsDetail,
} from '@/app-modules/news/helpers/mocks'

import { openConfirm, openPrompt } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()

describe('Test vue AuditDetail', () => {
  let wrapper
  const onRoutePush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(AuditDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            params: id,
            query: {},
          },
          $router: {
            push: onRoutePush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchUnAuditedNewsDetail', async () => {
    fetchUnAuditedNewsDetail.mockResolvedValue(() => Promise.resolve({ ...unAuditedNewsDetail }))
    await wrapper.vm.fetchData()
    expect(fetchUnAuditedNewsDetail).toBeCalled()
  })

  it('Test methods handleCommand - updateApproveUnAuditedNews updateRejectUnAuditedNews', async () => {
    updateApproveUnAuditedNews.mockResolvedValue()
    updateRejectUnAuditedNews.mockResolvedValue()
    openConfirm.mockResolvedValue()
    openPrompt.mockResolvedValue(() => Promise.resolve({ value: deleteReason }))
    const deleteReason = global.randomString()
    let command = 'EDIT'
    await wrapper.vm.handleCommand(command, id)
    expect(wrapper.vm.$router.push).toBeCalledWith({ path: `/news/edit/${id}`, query: { route: `/news/audit-detail/${id}`, isAudit: true } })
    command = 'APPROVE'
    await wrapper.vm.handleCommand(command, id)
    expect(updateApproveUnAuditedNews).toBeCalled()
    command = 'REJECT'
    await wrapper.vm.handleCommand(command, id)
    expect(updateRejectUnAuditedNews).toBeCalled()
  })

  it('Test AuditDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
