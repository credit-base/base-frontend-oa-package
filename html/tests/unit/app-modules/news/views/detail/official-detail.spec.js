jest.mock('@/app-modules/news/services/news')
jest.mock('@/utils/element')
import OfficialDetail from '@/app-modules/news/views/detail/official-detail'

import {
  fetchNewsDetail,
  updateTopNews,
  updateCancelTopNews,
  updateEnableNews,
  updateDisableNews,
  updateMoveNews,
  fetchNewsType,
} from '@/app-modules/news/services/news'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()

describe('Test vue OfficialDetail', () => {
  let wrapper
  const onRoutePush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(OfficialDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            params: id,
            query: {},
          },
          $router: {
            push: onRoutePush,
          },
        },
      }),
      data () {
        return {
          newsType: 'MA',
        }
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchNewsDetail', async () => {
    fetchNewsDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(fetchNewsDetail).toBeCalled()
  })

  it('Test methods getNewsType - fetchNewsType', async () => {
    const data = {
      list: [],
    }
    fetchNewsType.mockResolvedValue(data)
    await wrapper.vm.getNewsType()
    expect(fetchNewsType).toBeCalled()
  })

  it('Test methods handleCommand - updateTopNews,updateCancelTopNews,updateEnableNews,updateDisableNews,updateMoveNews', async () => {
    updateTopNews.mockResolvedValue()
    updateCancelTopNews.mockResolvedValue()
    updateEnableNews.mockResolvedValue()
    updateDisableNews.mockResolvedValue()
    updateMoveNews.mockResolvedValue()
    openConfirm.mockResolvedValue()

    let command = 'EDIT'
    await wrapper.vm.handleCommand(command, id)
    expect(wrapper.vm.$router.push).toBeCalledWith({ path: `/news/edit/${id}`, query: { route: `/news/detail/${id}` } })
    command = 'TOP'
    await wrapper.vm.handleCommand(command, id)
    expect(updateTopNews).toBeCalled()
    command = 'CANCEL_TOP'
    await wrapper.vm.handleCommand(command, id)
    expect(updateCancelTopNews).toBeCalled()
    command = 'ENABLE'
    await wrapper.vm.handleCommand(command, id)
    expect(updateEnableNews).toBeCalled()
    command = 'DISABLE'
    await wrapper.vm.handleCommand(command, id)
    expect(updateDisableNews).toBeCalled()
    // command = 'MOVE'
    // await wrapper.vm.handleCommand(command, id)
    // console.log(wrapper.vm.newsType)
    // expect(updateMoveNews).toBeCalled()
  })

  it('Test OfficialDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
