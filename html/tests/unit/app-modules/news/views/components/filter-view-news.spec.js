import FormViewFilterNews from '@/app-modules/news/views/components/form-view-filter-news'

const { shallowMount, createComponentMocks } = global

describe('Test vue FormViewFilterNews', () => {
  let wrapper
  const store = {
    getters: {
      fullUserGroup: [],
    },
  }
  const onChange = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormViewFilterNews, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
          },
        },
        propsData: {
          payload: {},
        },
      }),
      listeners: {
        change: onChange,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData', async () => {
    await wrapper.vm.fetchData()
    expect(onChange).toBeCalled()
  })

  it('Test FormViewFilterNews snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
