// import FormViewNews from '@/app-modules/news/views/components/form-view-news'
// jest.mock('@/app-modules/news/services/news')
// import {
//   fetchNewsType,
//   createNews,
//   fetchEditNews,
//   updateNews,
//   updateUnAuditedNews,
//   fetchEditUnAuditedNews,
// } from '@/app-modules/news/services/news'

// const { shallowMount, createComponentMocks, randomString } = global
// const id = randomString()

// describe('Test vue FormViewNews', () => {
//   let wrapper
//   const store = {
//     getters: {
//       fullUserGroup: [],
//       userGroup: {},
//     },
//   }

//   beforeEach(() => {
//     wrapper = shallowMount(FormViewNews, {
//       ...createComponentMocks({
//         router: true,
//         mocks: {
//           $store: store,
//           $route: {
//             query: {
//               isAudit: false,
//               route: randomString(),
//             },
//             params: {
//               id,
//             },
//           },
//         },
//         propsData: {
//           payload: {},
//         },
//       }),
//     })
//   })

//   afterEach(() => {
//     wrapper && wrapper.destroy()
//   })

// it('Test shallowMount', () => {
//   expect(wrapper.exists()).toBe(true)
// })

// it('Test methods submitCreate - createNews', async() => {
//   const params = {}
//   createNews.mockResolvedValue()
//   await wrapper.vm.submitCreate(params)
//   expect(createNews).toBeCalled()
// })

// it('Test methods getNewsType - fetchNewsType', async() => {
//   const params = {}
//   fetchNewsType.mockResolvedValue()
//   await wrapper.vm.getNewsType(params)
//   expect(fetchNewsType).toBeCalled()
// })

// it('Test methods submitUpdate - updateNews updateUnAuditedNews', async() => {
//   const params = {}
//   updateNews.mockResolvedValue()
//   updateUnAuditedNews.mockResolvedValue()
//   await wrapper.vm.submitUpdate(params)
//   expect(updateNews).toBeCalled()
//   // await wrapper.setProps({isAudit: true})
//   // expect(updateUnAuditedNews).toBeCalled()
// })
// it('Test FormViewNews snapshot', () => {
//   expect(wrapper.html()).toMatchSnapshot()
// })
// })
