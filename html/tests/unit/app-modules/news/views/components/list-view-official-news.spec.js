import ListViewOfficialNews from '@/app-modules/news/views/components/list-view-official-news'

const { shallowMount, createComponentMocks, randomString } = global
const id = randomString()

describe('Test vue ListViewOfficialNews', () => {
  let wrapper
  const store = {
    getters: {
      fullUserGroup: [],
    },
  }
  const onChange = jest.fn()
  const onSort = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(ListViewOfficialNews, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
          },
        },
        propsData: {
          payload: {},
        },
      }),
      listeners: {
        change: onChange,
        sort: onSort,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods sortChange', async () => {
    const column = {
      prop: '',
      order: '',
    }
    await wrapper.vm.sortChange(column)
    expect(onSort).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    const command = {}
    await wrapper.vm.handleCommand(command, { id })
    expect(onChange).toBeCalled()
  })

  it('Test ListViewOfficialNews snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
