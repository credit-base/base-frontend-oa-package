import DetailViewNews from '@/app-modules/news/views/components/detail-view-news'

const { shallowMount, createComponentMocks } = global

describe('Test vue DetailViewNews', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(DetailViewNews, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
        propsData: {
          data: {},
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test DetailViewNews snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
