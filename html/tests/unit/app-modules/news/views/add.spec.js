import NewsAdd from '@/app-modules/news/views/add'

const { shallowMount, createComponentMocks } = global

describe('Test vue NewsAdd', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(NewsAdd, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test NewsAdd snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
