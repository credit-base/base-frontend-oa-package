import NewsEdit from '@/app-modules/news/views/edit'

const { shallowMount, createComponentMocks } = global

describe('Test vue NewsEdit', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(NewsEdit, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test NewsEdit snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
