jest.mock('@/app-modules/news/services/news')
import AuditList from '@/app-modules/news/views/list/audit-list'
import {
  fetchNewsList,
  fetchNewsType,
  fetchUnAuditedNewsList,
} from '@/app-modules/news/services/news'
import { NEWS_TABS } from '@/app-modules/news/helpers/constants'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()

describe('Test vue AuditList', () => {
  let wrapper
  const onRoutePush = jest.fn()
  const store = {
    getters: {
      fullUserGroup: [],
      userGroup: {
        id,
      },
      superAdminPurview: false,
      platformAdminPurview: false,
    },
  }
  beforeEach(() => {
    wrapper = shallowMount(AuditList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {
              activeTab: '',
            },
          },
          $router: {
            push: onRoutePush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleCommand', async () => {
    fetchNewsType.mockResolvedValue({ total: 0, list: [] })
    fetchNewsList.mockResolvedValue({ total: 0, list: [] })

    await wrapper.vm.handleCommand({ command: 'VIEW', id })

    expect(onRoutePush).toBeCalledWith({
      path: `/unaudited-news/detail/${id}`,
      query: {
        route: '/un-audited-news/list',
        isAudit: true,
      },
    })

    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })

    expect(onRoutePush).toBeCalledWith({
      path: `/unaudited-news/audit-detail/${id}`,
      query: {
        route: `/un-audited-news/list?activeTab=audit-news`,
        isAudit: true,
      },
    })
  })

  it('Test methods initQuery', async () => {
    await wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods fetchData', async () => {
    fetchNewsList.mockResolvedValue({ total: 0, list: [] })
    fetchUnAuditedNewsList.mockResolvedValue({ total: 0, list: [] })
    wrapper.setData({
      activeTab: NEWS_TABS[0].name,
    })
    await wrapper.vm.fetchData()
    expect(fetchNewsList).toBeCalled()

    wrapper.setData({
      activeTab: NEWS_TABS[1].name,
    })
    await wrapper.vm.fetchData()
    expect(fetchUnAuditedNewsList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    await wrapper.vm.searchData()
    fetchNewsList.mockResolvedValue({ total: 0, list: [] })
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(fetchNewsList).toBeCalled()
  })

  it('Test methods sortChange', async () => {
    fetchNewsList.mockResolvedValue({ total: 0, list: [] })
    await wrapper.vm.sortChange(`updateTime`)

    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(wrapper.vm.query.sort).toBe(`updateTime`)
    expect(fetchNewsList).toBeCalled()
  })

  it('Test methods changeTabs', async () => {
    await wrapper.vm.changeTabs()
    fetchNewsList.mockResolvedValue({ total: 0, list: [] })
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(fetchNewsList).toBeCalled()
  })

  it('Test methods getNewsType fetchNewsType', async () => {
    fetchNewsType.mockResolvedValue([])
    await wrapper.vm.getNewsType()
    expect(fetchNewsType).toBeCalled()
    expect(wrapper.vm.fullNewsType).toStrictEqual([])
  })

  it('Test AuditList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
