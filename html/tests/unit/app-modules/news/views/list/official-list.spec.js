jest.mock('@/app-modules/news/services/news')
jest.mock('@/utils/element')
import OfficialList from '@/app-modules/news/views/list/official-list'
import {
  fetchNewsType,
  fetchNewsList,
  fetchUnAuditedNewsList,
  updateTopNews,
  updateCancelTopNews,
  updateEnableNews,
  updateDisableNews,
  updateMoveNews,
} from '@/app-modules/news/services/news'
import { NEWS_TABS } from '@/app-modules/news/helpers/constants'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()

describe('Test vue components OfficialList', () => {
  let wrapper
  const store = {
    getters: {
      fullUserGroup: [],
      userGroup: {
        id,
      },
      superAdminPurview: false,
      platformAdminPurview: false,
    },
  }

  const onMessage = jest.fn()
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(OfficialList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
          },
          $router: {
            push: onRouterPush,
          },
          $message: onMessage,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods addNews', () => {
    wrapper.vm.addNews()
    expect(onRouterPush).toBeCalledWith({ name: 'NewsAdd', query: { route: '/news/list' } })
  })

  it('Test methods getNewsType - fetchNewsType', async () => {
    const data = {
      list: [],
    }
    fetchNewsType.mockResolvedValue(data)
    await wrapper.vm.getNewsType()
    expect(fetchNewsType).toBeCalled()
  })

  it('Test methods handleCommand', async () => {
    updateTopNews.mockResolvedValue()
    updateCancelTopNews.mockResolvedValue()
    updateEnableNews.mockResolvedValue()
    updateDisableNews.mockResolvedValue()
    updateMoveNews.mockResolvedValue()
    openConfirm.mockResolvedValue()
    let value = {
      command: 'VIEW',
      id,
    }
    await wrapper.vm.handleCommand(value)
    expect(onRouterPush).toBeCalledWith({ path: `/news/detail/${id}`, query: { route: '/news/list' } })
    value = {
      command: 'AUDIT_VIEW',
      id,
    }
    await wrapper.vm.handleCommand(value)
    expect(onRouterPush).toBeCalledWith({ path: `/news/audit-detail/${id}`, query: { route: `/news/list?activeTab=audit-news` } })
    value = {
      command: 'EDIT',
      id,
    }
    await wrapper.vm.handleCommand(value)
    expect(onRouterPush).toBeCalledWith({ path: `/news/edit/${id}`, query: { route: '/news/list' } })
    value = {
      command: 'AUDIT_EDIT',
      id,
    }
    await wrapper.vm.handleCommand(value)
    expect(onRouterPush).toBeCalledWith({ path: `/news/edit/${id}`, query: { route: '/news/list', isAudit: true } })
    value = {
      command: 'TOP',
      id,
    }
    await wrapper.vm.handleCommand(value)
    expect(updateTopNews).toBeCalled()
    value = {
      command: 'CANCEL_TOP',
      id,
    }
    await wrapper.vm.handleCommand(value)
    expect(updateCancelTopNews).toBeCalled()
    value = {
      command: 'ENABLE',
      id,
    }
    await wrapper.vm.handleCommand(value)
    expect(updateEnableNews).toBeCalled()
    value = {
      command: 'DISABLE',
      id,
    }
    await wrapper.vm.handleCommand(value)
    expect(updateDisableNews).toBeCalled()
  })

  it('Test methods initQuery', async () => {
    wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods fetchData', async () => {
    fetchNewsList.mockResolvedValue()
    fetchUnAuditedNewsList.mockResolvedValue()
    wrapper.setData({
      activeTab: NEWS_TABS[0].name,
    })
    await wrapper.vm.fetchData()
    expect(fetchNewsList).toBeCalled()

    wrapper.setData({
      activeTab: NEWS_TABS[1].name,
    })
    await wrapper.vm.fetchData()
    expect(fetchUnAuditedNewsList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    await wrapper.vm.searchData()
    fetchNewsList.mockResolvedValue()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(fetchNewsList).toBeCalled()
  })

  it('Test methods sortChange', async () => {
    const val = 'updateTime'
    fetchNewsList.mockResolvedValue()
    await wrapper.vm.sortChange(val)
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(wrapper.vm.query.sort).toBe(val)
    expect(fetchNewsList).toBeCalled()
  })

  it('Test methods changeTabs', async () => {
    await wrapper.vm.changeTabs()
    fetchNewsList.mockResolvedValue()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(fetchNewsList).toBeCalled()
  })

  it('Test methods changeMove', async () => {
    await wrapper.vm.changeMove(id)
    const data = {
      list: [],
    }
    fetchNewsType.mockResolvedValue(data)
    await wrapper.vm.getNewsType()
    expect(fetchNewsType).toBeCalled()
    expect(wrapper.vm.newsId).toBe(id)
    expect(wrapper.vm.dialogFormVisible).toBeTruthy()
  })

  it('Test methods handleMove', async () => {
    wrapper.setData({
      newsType: '',
    })
    await wrapper.vm.handleMove()
    expect(onMessage).toBeCalledWith({ type: 'error', message: '新闻栏目不能为空，请继续完善信息' })

    updateMoveNews.mockResolvedValue()
    fetchNewsList.mockResolvedValue()
    wrapper.setData({
      newsType: 'MA',
      newsId: 'MQ',
    })
    await wrapper.vm.handleMove()
    expect(updateMoveNews).toBeCalled()
    expect(fetchNewsList).toBeCalled()
    expect(onMessage).toBeCalledWith({ type: 'success', message: '已成功提交' })
  })

  it('Test OfficialList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
