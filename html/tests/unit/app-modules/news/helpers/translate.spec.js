// import News from '@/app-modules/news/helpers/news'
import {
  translateNewsList,
  translateNewsDetail,
  translateUnAuditedNewsList,
  translateUnAuditedNewsDetail,
  translateEditNews,
} from '@/app-modules/news/helpers/translate'

import {
  newsList,
  translateResList,
  newsDetail,
  translateResDetail,
  unAuditedNewsList,
  translateResUnAuditedNewsList,
  unAuditedNewsDetail,
  translateResUnAuditedNewsDetail,
  // newsType,
} from '@/app-modules/news/helpers/mocks'

const noData = {}
const errData = {
  list: {},
}
const resData = {
  list: [],
  total: 0,
}

describe('Test translate', () => {
  it('Test translateNewsList', () => {
    const data = {
      list: [...newsList],
      total: 1,
    }
    const res = {
      list: [...translateResList],
      total: 1,
    }

    expect(translateNewsList()).toStrictEqual(resData)
    expect(translateNewsList(noData)).toStrictEqual(resData)
    expect(translateNewsList(errData)).toStrictEqual(resData)

    expect(translateNewsList(data)).toStrictEqual(res)
  })

  it('Test translateNewsDetail', () => {
    const emptRes = {
      id: undefined,
      newsType: '',
      publishUserGroup: '',
      source: undefined,
      status: {
        id: '',
        name: '',
        type: '',
      },
      stick: {
        id: '',
        name: '',
        type: '',
      },
      title: undefined,
      updateTimeFormat: undefined,
      attachments: [],
      bannerImage: {
        identify: '',
        name: '',
      },
      bannerStatus: {
        id: '',
        name: '',
        type: '',
      },
      content: undefined,
      cover: {
        identify: '',
        name: '',
      },
      crew: '',
      dimension: '',
      homePageShowStatus: {
        id: '',
        name: '',
        type: '',
      },
    }

    expect(translateNewsDetail()).toStrictEqual({ ...emptRes })
    expect(translateNewsDetail(noData)).toStrictEqual({ ...emptRes })

    expect(translateNewsDetail(newsDetail)).toStrictEqual(translateResDetail)
  })

  it('Test translateUnAuditedNewsList', () => {
    const data = {
      list: [...unAuditedNewsList],
      total: 1,
    }
    const res = {
      list: [...translateResUnAuditedNewsList],
      total: 1,
    }

    expect(translateUnAuditedNewsList()).toStrictEqual(resData)
    expect(translateUnAuditedNewsList(noData)).toStrictEqual(resData)
    expect(translateUnAuditedNewsList(errData)).toStrictEqual(resData)

    expect(translateUnAuditedNewsList(data)).toStrictEqual(res)
  })

  it('Test translateUnAuditedNewsDetail', () => {
    const emptRes = {
      id: undefined,
      newsType: '',
      publishUserGroup: '',
      source: undefined,
      applyCrew: '',
      applyStatus: {
        id: '',
        name: '',
        type: '',
      },
      status: {
        id: '',
        name: '',
        type: '',
      },
      stick: {
        id: '',
        name: '',
        type: '',
      },
      applyUserGroup: '',
      operationType: '',
      title: undefined,
      rejectReason: undefined,
      updateTimeFormat: undefined,
      attachments: [],
      bannerImage: {
        identify: '',
        name: '',
      },
      bannerStatus: {
        id: '',
        name: '',
        type: '',
      },
      content: undefined,
      cover: {
        identify: '',
        name: '',
      },
      crew: '',
      dimension: '',
      homePageShowStatus: {
        id: '',
        name: '',
        type: '',
      },
    }

    expect(translateUnAuditedNewsDetail()).toStrictEqual({ ...emptRes })
    expect(translateUnAuditedNewsDetail(noData)).toStrictEqual({ ...emptRes })

    expect(translateUnAuditedNewsDetail(unAuditedNewsDetail)).toStrictEqual(translateResUnAuditedNewsDetail)
  })

  it('Test translateEditNews', () => {
    const emptRes = {
      status: undefined,
      homePageShowStatus: undefined,
      bannerStatus: undefined,
      stick: undefined,
      title: undefined,
      source: undefined,
      newsType: undefined,
      content: undefined,
      dimension: undefined,
      cover: {},
      attachments: [],
      bannerImage: {},
    }
    expect(translateEditNews()).toStrictEqual(emptRes)
  })
})
