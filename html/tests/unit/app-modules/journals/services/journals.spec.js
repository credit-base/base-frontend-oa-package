jest.mock('@/utils/request')
import request from '@/utils/request'

import {
  fetchJournalsList,
  fetchJournalsDetail,
  createJournals,
  fetchEditJournals,
  updateJournals,
  updateJournalsStatus,
  fetchUnAuditedJournalsList,
  fetchUnAuditedJournalsDetail,
  fetchEditUnAuditedJournals,
  updateUnAuditedJournals,
  updateApproveUnAuditedJournals,
  updateRejectUnAuditedJournals,

} from '@/app-modules/journals/services/journals'
import {
  journalsList,
  journalsDetail,
  unAuditedJournalsList,
  unAuditedJournalsDetail,
} from '@/app-modules/journals/helpers/mocks'

const params = {}
const id = 'MA'
const useMock = { useMock: true }

describe('Test journals services', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Test services fetchJournalsList', () => {
    fetchJournalsList()
    expect(request).toBeCalledWith('/api/journals', { method: 'GET', params })
    fetchJournalsList(params)
    expect(request).toBeCalledWith('/api/journals', { method: 'GET', params })
    expect(fetchJournalsList(useMock)).toStrictEqual({
      total: 4,
      list: [
        ...journalsList,
      ],
    })
  })

  it('Test services fetchJournalsDetail', () => {
    fetchJournalsDetail(id)
    expect(request).toBeCalledWith(`/api/journals/${id}`, { method: 'GET' })
    expect(fetchJournalsDetail(id, useMock)).toStrictEqual({
      ...journalsDetail,
    })
  })

  it('Test services createJournals', () => {
    createJournals()
    expect(request).toBeCalledWith('/api/journals/add', { method: 'POST', params })
    createJournals(params)
    expect(request).toBeCalledWith('/api/journals/add', { method: 'POST', params })
  })

  it('Test services fetchEditJournals', () => {
    fetchEditJournals(id)
    expect(request).toBeCalledWith(`/api/journals/${id}`, { method: 'GET' })
    expect(fetchEditJournals(id, useMock)).toStrictEqual({
      ...journalsDetail,
    })
  })

  it('Test services updateJournals', () => {
    updateJournals(id)
    expect(request).toBeCalledWith(`/api/journals/${id}/edit`, { method: 'POST', params })
    updateJournals(id, params)
    expect(request).toBeCalledWith(`/api/journals/${id}/edit`, { method: 'POST', params })
  })

  it('Test services updateJournalsStatus', () => {
    let type = 'enable'
    updateJournalsStatus(id, type)
    expect(request).toBeCalledWith(`/api/journals/${id}/enable`, { method: 'POST' })
    type = 'disable'
    updateJournalsStatus(id, type)
    expect(request).toBeCalledWith(`/api/journals/${id}/disable`, { method: 'POST' })
  })

  it('Test services fetchUnAuditedJournalsList', () => {
    fetchUnAuditedJournalsList()
    expect(request).toBeCalledWith('/api/unAuditedJournals', { method: 'GET', params })
    fetchUnAuditedJournalsList(params)
    expect(request).toBeCalledWith('/api/unAuditedJournals', { method: 'GET', params })
    expect(fetchUnAuditedJournalsList(useMock)).toStrictEqual({
      total: 2,
      list: [
        ...unAuditedJournalsList,
      ],
    })
  })

  it('Test services fetchUnAuditedJournalsDetail', () => {
    fetchUnAuditedJournalsDetail(id)
    expect(request).toBeCalledWith(`/api/unAuditedJournals/${id}`, { method: 'GET' })
    expect(fetchUnAuditedJournalsDetail(id, useMock)).toStrictEqual({
      ...unAuditedJournalsDetail,
    })
  })

  it('Test services fetchEditUnAuditedJournals', () => {
    fetchEditUnAuditedJournals(id)
    expect(request).toBeCalledWith(`/api/unAuditedJournals/${id}`, { method: 'GET' })
    expect(fetchEditUnAuditedJournals(id, useMock)).toStrictEqual({
      ...unAuditedJournalsDetail,
    })
  })

  it('Test services updateUnAuditedJournals', () => {
    updateUnAuditedJournals(id)
    expect(request).toBeCalledWith(`/api/unAuditedJournals/${id}/edit`, { method: 'POST', params })
    updateUnAuditedJournals(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedJournals/${id}/edit`, { method: 'POST', params })
  })

  it('Test services updateApproveUnAuditedJournals', () => {
    updateApproveUnAuditedJournals(id)
    expect(request).toBeCalledWith(`/api/unAuditedJournals/${id}/approve`, { method: 'POST' })
  })

  it('Test services updateRejectUnAuditedJournals', () => {
    updateRejectUnAuditedJournals(id)
    expect(request).toBeCalledWith(`/api/unAuditedJournals/${id}/reject`, { method: 'POST', params })
    updateRejectUnAuditedJournals(id, params)
    expect(request).toBeCalledWith(`/api/unAuditedJournals/${id}/reject`, { method: 'POST', params })
  })
})
