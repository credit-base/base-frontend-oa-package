jest.mock('@/utils/element')
jest.mock('@/app-modules/journals/services/journals')
import JournalsAuditDetail from '@/app-modules/journals/views/review/audit-detail'
import { openConfirm, openPrompt } from '@/utils/element'
import { fetchUnAuditedJournalsDetail, updateApproveUnAuditedJournals, updateRejectUnAuditedJournals } from '@/app-modules/journals/services/journals'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue JournalsAuditDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(JournalsAuditDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            params: {
              id,
            },
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchUnAuditedJournalsDetail', async () => {
    fetchUnAuditedJournalsDetail.mockResolvedValue()
    const res = {
      applyStatus: {
        id,
      },
    }
    fetchUnAuditedJournalsDetail.mockResolvedValue(res)
    await wrapper.vm.fetchData(id)
    expect(fetchUnAuditedJournalsDetail).toBeCalledWith(id)
    expect(wrapper.vm.applyStatusId).toBe(id)
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Test methods handleCommand - fetchUnAuditedJournalsDetail updateApproveUnAuditedJournals updateRejectUnAuditedJournals', async () => {
    const res = {
      value: 'rejectReason',
    }
    openConfirm.mockResolvedValue()
    fetchUnAuditedJournalsDetail.mockResolvedValue()
    updateApproveUnAuditedJournals.mockResolvedValue()
    updateRejectUnAuditedJournals.mockResolvedValue()
    openPrompt.mockResolvedValue(res)

    await wrapper.vm.handleCommand('APPROVE', id)
    expect(updateApproveUnAuditedJournals).toBeCalledWith(id)
    expect(fetchUnAuditedJournalsDetail).toBeCalledWith(id)

    await wrapper.vm.handleCommand('REJECT', id)
    expect(updateRejectUnAuditedJournals).toBeCalledWith(id, { rejectReason: 'rejectReason' })
    expect(fetchUnAuditedJournalsDetail).toBeCalledWith(id)
  })

  it('Test JournalsAuditDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
