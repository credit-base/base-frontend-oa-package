jest.mock('@/app-modules/journals/services/journals')
import JournalsReviewList from '@/app-modules/journals/views/review/list'
import { fetchJournalsList, fetchUnAuditedJournalsList } from '@/app-modules/journals/services/journals'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue components JournalsReviewList', () => {
  let wrapper
  const store = {
    getters: {
      userGroup: {
        id,
      },
      fullUserGroup: [],
      superAdminPurview: false,
      platformAdminPurview: false,
    },
  }

  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(JournalsReviewList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => [
    wrapper && wrapper.destroy(),
  ])

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - generateQuery fetchJournalsList / fetchUnAuditedJournalsList', async () => {
    fetchJournalsList.mockResolvedValue()
    fetchUnAuditedJournalsList.mockResolvedValue()

    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchJournalsList).toBeCalled()

    // await wrapper.vm.fetchData(false)
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods generateQuery', () => {
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
        title: 'Test',
        status: 'MA',
        userGroup: 'MA',
        sort: 'updateTime',
        applyStatus: 'LC0',
        year: 2021,
      },
    })
    expect(wrapper.vm.generateQuery(true)).toStrictEqual({
      applyStatus: 'LC0',
      limit: 10,
      page: 1,
      title: 'Test',
      status: 'MA',
      userGroup: 'MA',
      sort: 'updateTime',
      year: 2021,
    })
    expect(wrapper.vm.generateQuery(false)).toStrictEqual({
      limit: 10,
      page: 1,
      status: 'MA',
      title: 'Test',
      applyStatus: 'LC0',
      userGroup: 'MA',
      sort: 'updateTime',
      year: 2021,
    })
  })

  it('Test methods handleCommand', async () => {
    await wrapper.vm.handleCommand({ command: 'VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/journals/review/official-detail/${id}`, query: { route: '/journals/review/list' } })
    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/journals/review/audit-detail/${id}`, query: { route: `/journals/review/list?activeTab=audit-journals` } })
  })

  it('Test methods changeTabs', async () => {
    fetchJournalsList.mockResolvedValue()
    fetchUnAuditedJournalsList.mockResolvedValue()

    await wrapper.vm.changeTabs()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchJournalsList).toBeCalled()

    // await wrapper.vm.changeTabs()
    // await wrapper.vm.fetchData(false)
    // await wrapper.vm.generateQuery()
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods filterData', async () => {
    fetchJournalsList.mockResolvedValue()
    fetchUnAuditedJournalsList.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchJournalsList).toBeCalled()

    // wrapper.vm.filterData()
    // await wrapper.vm.fetchData(false)
    // await wrapper.vm.generateQuery()
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    fetchJournalsList.mockResolvedValue()
    fetchUnAuditedJournalsList.mockResolvedValue()

    wrapper.vm.searchData()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchJournalsList).toBeCalled()

    // wrapper.vm.searchData()
    // await wrapper.vm.fetchData(false)
    // await wrapper.vm.generateQuery()
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods sortChange', async () => {
    fetchJournalsList.mockResolvedValue()
    fetchUnAuditedJournalsList.mockResolvedValue()

    wrapper.vm.sortChange()
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      userGroup: 'MA',
    })
    expect(fetchJournalsList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      sort: 'updateTime',
      userGroup: 'MA',
    })
    expect(fetchJournalsList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      sort: '-updateTime',
      userGroup: 'MA',
    })
    expect(fetchJournalsList).toBeCalled()

    // wrapper.vm.sortChange()
    // await wrapper.vm.fetchData(false)
    // expect(wrapper.vm.generateQuery()).toStrictEqual({
    //   limit: 10,
    //   page: 1,
    //   userGroup: 'MA',
    // })
    // expect(fetchUnAuditedJournalsList).toBeCalled()

    // wrapper.vm.sortChange('updateTime')
    // await wrapper.vm.fetchData(false)
    // expect(wrapper.vm.generateQuery()).toStrictEqual({
    //   limit: 10,
    //   page: 1,
    //   userGroup: 'MA',
    //   sort: 'updateTime',
    // })
    // expect(fetchUnAuditedJournalsList).toBeCalled()

    // wrapper.vm.sortChange('-updateTime')
    // await wrapper.vm.fetchData(false)
    // expect(wrapper.vm.generateQuery()).toStrictEqual({
    //   limit: 10,
    //   page: 1,
    //   userGroup: 'MA',
    //   sort: '-updateTime',
    // })
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test JournalsReviewList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
