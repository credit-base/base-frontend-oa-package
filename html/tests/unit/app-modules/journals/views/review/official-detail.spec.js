jest.mock('@/utils/element')
jest.mock('@/app-modules/journals/services/journals')
import JournalsOfficialDetail from '@/app-modules/journals/views/review/official-detail'
import { fetchJournalsDetail } from '@/app-modules/journals/services/journals'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue JournalsOfficialDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(JournalsOfficialDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            params: {
              id,
            },
            query: {},
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchJournalsDetail', async () => {
    const res = {
      applyStatus: {
        id,
      },
    }
    fetchJournalsDetail.mockResolvedValue(res)
    await wrapper.vm.fetchData(id)
    expect(fetchJournalsDetail).toBeCalledWith(id)
    expect(wrapper.vm.applyStatusId).toBe(id)
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })
  it('Test JournalsOfficialDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
