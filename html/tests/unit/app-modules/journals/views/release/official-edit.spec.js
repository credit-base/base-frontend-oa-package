import JournalsOfficialEdit from '@/app-modules/journals/views/release/official-edit'

const { shallowMount, createComponentMocks } = global

describe('Test vue components JournalsOfficialEdit', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(JournalsOfficialEdit, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test JournalsOfficialEdit snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
