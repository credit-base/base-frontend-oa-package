jest.mock('@/utils/element')
jest.mock('@/app-modules/journals/services/journals')
jest.mock('@/app-modules/journals/helpers/translate')
import JournalsAuditDetail from '@/app-modules/journals/views/release/audit-detail'
// import { fetchUnAuditedJournalsDetail } from '@/app-modules/journals/services/journals'
// import { translateUnAuditedJournalsDetail } from '@/app-modules/journals/helpers/translate'
// import { unAuditedJournalsDetail } from '@/app-modules/journals/helpers/mocks'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()

describe('Test vue components JournalsAuditDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(JournalsAuditDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            params: {
              id,
            },
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  // it('Test methods fetchData - fetchUnAuditedJournalsDetail', async () => {
  //   fetchUnAuditedJournalsDetail.mockResolvedValue({...unAuditedJournalsDetail})
  //   await wrapper.vm.fetchData(id)

  //   expect(fetchUnAuditedJournalsDetail).toBeCalled()
  //   // expect(wrapper.vm.applyStatusId).toBeTruthy()
  //   expect(wrapper.vm.detail).toStrictEqual({
  //     id,
  //     applyStatus,
  //     rejectReason,
  //     applyCrew,
  //     crew,
  //     updateTimeFormat,
  //     status,
  //     title,
  //     year,
  //     source,
  //     cover,
  //     authImages,
  //     attachment,
  //     description,
  //   })
  //   expect(wrapper.vm.pageLoading).toBeFalsy()
  // })

  it('Test JournalsAuditDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
