jest.mock('@/utils/element')
jest.mock('@/app-modules/journals/services/journals')
import JournalsOfficialDetail from '@/app-modules/journals/views/release/official-detail'
import { fetchJournalsDetail, updateJournalsStatus } from '@/app-modules/journals/services/journals'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()

describe('Test vue components JournalsOfficialDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()
  beforeEach(() => {
    wrapper = shallowMount(JournalsOfficialDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            params: {
              id,
            },
            query: {},
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchJournalsDetail', async () => {
    fetchJournalsDetail.mockResolvedValue()
    await wrapper.vm.fetchData(id)
    expect(fetchJournalsDetail).toBeCalled()
  })

  it('Test methods handleCommand - updateJournalsStatus', async () => {
    openConfirm.mockResolvedValue()
    updateJournalsStatus.mockResolvedValue()

    let command = 'EDIT'
    await wrapper.vm.handleCommand(command, id)
    expect(onRouterPush).toBeCalledWith({ path: `/journals/release/official-edit/${id}`, query: { route: `/journals/release/official-detail/${id}` } })

    command = 'ENABLE'
    await wrapper.vm.handleCommand(command, id)
    expect(updateJournalsStatus).toBeCalledWith(id, 'enable')

    command = 'DISABLE'
    await wrapper.vm.handleCommand(command, id)
    expect(updateJournalsStatus).toBeCalledWith(id, 'disable')
  })

  it('Test JournalsOfficialDetail snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
