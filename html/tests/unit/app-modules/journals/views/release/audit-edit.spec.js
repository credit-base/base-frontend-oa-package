import JournalsAuditEdit from '@/app-modules/journals/views/release/audit-edit'

const { shallowMount, createComponentMocks } = global

describe('Test vue components JournalsAuditEdit', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(JournalsAuditEdit, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test JournalsAuditEdit snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
