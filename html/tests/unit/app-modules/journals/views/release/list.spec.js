jest.mock('@/utils/element')
jest.mock('@/app-modules/journals/services/journals')
import JournalsReleaseList from '@/app-modules/journals/views/release/list'
import { fetchJournalsList, fetchUnAuditedJournalsList, updateJournalsStatus } from '@/app-modules/journals/services/journals'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue components JournalsReleaseList', () => {
  let wrapper
  const store = {
    getters: {
      userGroup: {
        id,
      },
      fullUserGroup: [],
      superAdminPurview: false,
      platformAdminPurview: false,
    },
  }

  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(JournalsReleaseList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods addJournals', () => {
    wrapper.vm.addJournals()
    expect(onRouterPush).toBeCalledWith({ path: `/journals/add`, query: { route: '/journals/release/list' } })
  })

  it('Test methods generateQuery', () => {
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
        title: 'Test',
        status: 'MA',
        userGroup: 'MA',
        sort: 'updateTime',
        year: 2021,
      },
    })
    expect(wrapper.vm.generateQuery(true)).toStrictEqual({
      limit: 10,
      page: 1,
      title: 'Test',
      status: 'MA',
      userGroup: 'MA',
      sort: 'updateTime',
      year: 2021,
    })
    expect(wrapper.vm.generateQuery(false)).toStrictEqual({
      limit: 10,
      page: 1,
      status: 'MA',
      title: 'Test',
      userGroup: 'MA',
      sort: 'updateTime',
      year: 2021,
    })
  })

  it('Test methods handleCommand - onRouterPush updateJournalsStatus', async () => {
    openConfirm.mockResolvedValue()
    updateJournalsStatus.mockResolvedValue()

    await wrapper.vm.handleCommand({ command: 'VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/journals/release/official-detail/${id}`, query: { route: '/journals/release/list' } })
    await wrapper.vm.handleCommand({ command: 'AUDIT_VIEW', id })
    expect(onRouterPush).toBeCalledWith({ path: `/journals/release/audit-detail/${id}`, query: { route: `/journals/release/list?activeTab=audit-journals` } })
    await wrapper.vm.handleCommand({ command: 'EDIT', id })
    expect(onRouterPush).toBeCalledWith({ path: `/journals/release/official-edit/${id}`, query: { route: '/journals/release/list' } })
    await wrapper.vm.handleCommand({ command: 'AUDIT_EDIT', id })
    expect(onRouterPush).toBeCalledWith({ path: `/journals/release/audit-edit/${id}`, query: { route: `/journals/release/list?activeTab=audit-journals` } })
    await wrapper.vm.handleCommand({ command: 'DISABLE', id })
    expect(updateJournalsStatus).toBeCalledWith(id, 'disable')
    await wrapper.vm.handleCommand({ command: 'ENABLE', id })
    expect(updateJournalsStatus).toBeCalledWith(id, 'enable')
  })

  it('Test methods fetchData - generateQuery fetchJournalsList / fetchUnAuditedJournalsList', async () => {
    fetchJournalsList.mockResolvedValue()
    fetchUnAuditedJournalsList.mockResolvedValue()

    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchJournalsList).toBeCalled()

    // await wrapper.vm.fetchData(false)
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods changeTabs', async () => {
    fetchJournalsList.mockResolvedValue()
    fetchUnAuditedJournalsList.mockResolvedValue()

    await wrapper.vm.changeTabs()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchJournalsList).toBeCalled()

    // await wrapper.vm.changeTabs()
    // await wrapper.vm.fetchData(false)
    // await wrapper.vm.generateQuery()
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods filterData', async () => {
    fetchJournalsList.mockResolvedValue()
    fetchUnAuditedJournalsList.mockResolvedValue()

    wrapper.vm.filterData()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchJournalsList).toBeCalled()

    // wrapper.vm.filterData()
    // await wrapper.vm.fetchData(false)
    // await wrapper.vm.generateQuery()
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods fetchData', async () => {
    fetchJournalsList.mockResolvedValue()
    fetchUnAuditedJournalsList.mockResolvedValue()

    wrapper.vm.fetchData()
    await wrapper.vm.fetchData(true)
    await wrapper.vm.generateQuery()
    expect(fetchJournalsList).toBeCalled()

    // wrapper.vm.fetchData()
    // await wrapper.vm.fetchData(false)
    // await wrapper.vm.generateQuery()
    // expect(fetchUnAuditedJournalsList).toBeCalled()
  })

  it('Test methods sortChange', async () => {
    fetchJournalsList.mockResolvedValue()
    fetchUnAuditedJournalsList.mockResolvedValue()

    wrapper.vm.sortChange()
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      userGroup: 'MA',
    })
    expect(fetchJournalsList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      userGroup: 'MA',
      sort: 'updateTime',
    })
    expect(fetchJournalsList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    await wrapper.vm.fetchData(true)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      userGroup: 'MA',
      sort: '-updateTime',
    })
    expect(fetchJournalsList).toBeCalled()

    wrapper.vm.sortChange()
    // await wrapper.vm.fetchData(false)
    // expect(wrapper.vm.generateQuery()).toStrictEqual({
    //   limit: 10,
    //   page: 1,
    //   userGroup: 'MA',
    // })
    // expect(fetchUnAuditedJournalsList).toBeCalled()

    wrapper.vm.sortChange('updateTime')
    await wrapper.vm.fetchData(false)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      userGroup: 'MA',
      sort: 'updateTime',
    })
    expect(fetchJournalsList).toBeCalled()

    wrapper.vm.sortChange('-updateTime')
    await wrapper.vm.fetchData(false)
    expect(wrapper.vm.generateQuery()).toStrictEqual({
      limit: 10,
      page: 1,
      userGroup: 'MA',
      sort: '-updateTime',
    })
    expect(fetchJournalsList).toBeCalled()
  })

  it('Test JournalsReleaseList snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
