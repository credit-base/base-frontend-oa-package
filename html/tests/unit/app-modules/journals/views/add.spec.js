import JournalsAdd from '@/app-modules/journals/views/add'

const { shallowMount, createComponentMocks } = global

describe('Test vue JournalsAdd', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(JournalsAdd, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test JournalsAdd snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
