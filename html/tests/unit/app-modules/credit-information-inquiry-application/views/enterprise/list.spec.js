import List from '@/app-modules/credit-information-inquiry-application/views/enterprise/list'

describe(`List - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(List, global.createComponentMocks({
      mocks: {
        $route: { query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', async () => {
    await wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
