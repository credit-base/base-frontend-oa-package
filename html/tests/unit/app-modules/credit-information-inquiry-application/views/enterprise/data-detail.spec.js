jest.mock('@/app-modules/credit-information-inquiry-application/services/index')
import BjDataDetail from '@/app-modules/credit-information-inquiry-application/views/enterprise/data-detail'
import { fetchDataDetail } from '@/app-modules/credit-information-inquiry-application/services/index'

const id = `MA`

describe(`BjDataDetail - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(BjDataDetail, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchDataDetail', async () => {
    fetchDataDetail.mockResolvedValue()
    // translateErrorDataDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(fetchDataDetail).toBeCalled()
    // expect(translateErrorDataDetail).toBeCalled()
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
