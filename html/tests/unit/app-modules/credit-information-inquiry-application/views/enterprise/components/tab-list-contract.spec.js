import TabListContract from '@/app-modules/credit-information-inquiry-application/views/enterprise/components/tab-list-contract'

const id = `MA`

describe(`TabListContract - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(TabListContract, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
