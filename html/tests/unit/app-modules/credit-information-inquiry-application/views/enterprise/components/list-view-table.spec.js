import ListViewTable from '@/app-modules/credit-information-inquiry-application/views/enterprise/components/list-view-table'

const id = `MA`

describe(`ListViewTable - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(ListViewTable, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods sortChange', async () => {
    // ascending descending
    let column = {
      prop: 'updateTime',
      order: '',
    }
    await wrapper.vm.sortChange(column)
    let emitted = wrapper.emitted()
    expect(emitted.sort).toStrictEqual([['']])

    column = {
      prop: 'updateTime',
      order: 'ascending',
    }
    await wrapper.vm.sortChange(column)
    emitted = wrapper.emitted()
    expect(emitted.sort).toStrictEqual([[''], ['updateTime']])

    column = {
      prop: 'updateTime',
      order: 'descending',
    }
    await wrapper.vm.sortChange(column)
    emitted = wrapper.emitted()
    expect(emitted.sort).toStrictEqual([[''], ['updateTime'], ['-updateTime']])
  })

  it('Test methods handleCommand', async () => {
    await wrapper.vm.handleCommand('VIEW', { id })
    const emitted = wrapper.emitted()

    expect(emitted.change).toBeTruthy()
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
