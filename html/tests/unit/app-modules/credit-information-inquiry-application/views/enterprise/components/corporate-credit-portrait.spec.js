import CorporateCreditPortrait from '@/app-modules/credit-information-inquiry-application/views/enterprise/components/corporate-credit-portrait'

const id = `MA`

describe(`CorporateCreditPortrait - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(CorporateCreditPortrait, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
