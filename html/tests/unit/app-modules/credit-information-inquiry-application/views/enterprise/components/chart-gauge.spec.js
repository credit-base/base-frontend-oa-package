import ChartGauge from '@/app-modules/credit-information-inquiry-application/views/enterprise/components/chart-gauge'

const id = `MA`

describe(`ChartGauge - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(ChartGauge, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
