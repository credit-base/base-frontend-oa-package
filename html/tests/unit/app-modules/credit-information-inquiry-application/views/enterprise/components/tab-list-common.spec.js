import TabListCommon from '@/app-modules/credit-information-inquiry-application/views/enterprise/components/tab-list-common'

const id = `MA`

describe(`TabListCommon - enterprise`, () => {
  let wrapper
  const onRoutePush = jest.fn()

  beforeEach(() => {
    wrapper = global.shallowMount(TabListCommon, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: { push: onRoutePush },
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleCommand', async () => {
    await wrapper.vm.handleCommand('VIEW', { id })
    expect(onRoutePush).toBeCalledWith({ name: `EnterpriseCatalogDetail`, params: { id } })
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
