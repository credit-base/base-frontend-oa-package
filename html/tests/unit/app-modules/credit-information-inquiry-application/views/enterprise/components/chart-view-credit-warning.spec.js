import ChartViewCreditWarning from '@/app-modules/credit-information-inquiry-application/views/enterprise/components/chart-view-credit-warning'

const id = `MA`

describe(`ChartViewCreditWarning - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(ChartViewCreditWarning, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
