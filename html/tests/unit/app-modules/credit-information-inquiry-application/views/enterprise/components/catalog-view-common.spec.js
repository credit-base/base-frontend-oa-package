import DetailViewCommon from '@/app-modules/credit-information-inquiry-application/views/enterprise/components/catalog-view-common'

const id = `MA`

describe(`DetailViewCommon - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(DetailViewCommon, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
        propsData: {
          category: 'MA',
        },
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
