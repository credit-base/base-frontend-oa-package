import TabListPromise from '@/app-modules/credit-information-inquiry-application/views/enterprise/components/tab-list-promise'

const id = `MA`

describe(`TabListPromise - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(TabListPromise, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
