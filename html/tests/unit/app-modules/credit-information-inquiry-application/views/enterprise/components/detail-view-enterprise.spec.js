import DetailViewEnterprise from '@/app-modules/credit-information-inquiry-application/views/enterprise/components/detail-view-enterprise'

const id = `MA`

describe(`DetailViewEnterprise - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(DetailViewEnterprise, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
