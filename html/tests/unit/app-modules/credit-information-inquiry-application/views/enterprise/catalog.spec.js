jest.mock('@/app-modules/credit-information-inquiry-application/services')
import CurrentUnitResourceCatalogDetail from '@/app-modules/credit-information-inquiry-application/views/enterprise/catalog'

import { fetchTemplateDetail } from '@/app-modules/credit-information-inquiry-application/services'

const id = `MA`

describe(`CurrentUnitResourceCatalogDetail - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(CurrentUnitResourceCatalogDetail, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test method fetchTemplateDetail', async () => {
    fetchTemplateDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(fetchTemplateDetail).toBeCalled()
    expect(wrapper.vm.pageLoading).toBeFalsy()
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
