import Detail from '@/app-modules/credit-information-inquiry-application/views/enterprise/detail'

const id = `MA`

describe(`Detail - enterprise`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(Detail, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
