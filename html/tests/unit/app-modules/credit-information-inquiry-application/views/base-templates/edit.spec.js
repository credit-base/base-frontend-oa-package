import BaseTemplatesEdit from '@/app-modules/credit-information-inquiry-application/views/base-templates/edit'

describe(`BaseTemplatesEdit - BaseTemplate`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(BaseTemplatesEdit, global.createComponentMocks({
      mocks: {
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
