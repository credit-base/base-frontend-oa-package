jest.mock('@/app-modules/credit-information-inquiry-application/services/base-template')
jest.mock('@/app-modules/credit-information-inquiry-application/helpers/translate')
import TemplateDetail from '@/app-modules/credit-information-inquiry-application/views/base-templates/detail'
import { TABS_NAME } from '@/app-modules/credit-information-inquiry-application/helpers/constants'

import { fetchBaseTemplateDetail, fetchTemplateVersionList } from '@/app-modules/credit-information-inquiry-application/services/base-template'
import { translateTemplateDetail, translateTemplateVersionList } from '@/app-modules/credit-information-inquiry-application/helpers/translate'

const id = `MA`

describe(`TemplateDetail - BaseTemplate`, () => {
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(TemplateDetail, global.createComponentMocks({
      mocks: {
        $route: { params: { id }, query: {} },
        $router: {},
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test method fetchData', async () => {
    fetchBaseTemplateDetail.mockResolvedValue({})
    translateTemplateDetail.mockResolvedValue()
    await wrapper.vm.fetchData()
    expect(fetchBaseTemplateDetail).toBeCalled()
  })

  it('Test method getTemplateVersionList', async () => {
    fetchTemplateVersionList.mockResolvedValue()
    translateTemplateVersionList.mockResolvedValue()
    await wrapper.vm.getTemplateVersionList()
    expect(fetchTemplateVersionList).toBeCalled()
  })

  it('Test method changeTabs', () => {
    fetchBaseTemplateDetail.mockResolvedValue({})
    translateTemplateDetail.mockResolvedValue()
    fetchTemplateVersionList.mockResolvedValue()
    translateTemplateVersionList.mockResolvedValue()
    wrapper.setData({
      activeTab: TABS_NAME.TEMPLATE_NAME,
    })
    wrapper.vm.changeTabs()
    expect(fetchBaseTemplateDetail).toBeCalled()
    wrapper.setData({
      activeTab: TABS_NAME.VERSION_NAME,
    })
    wrapper.vm.changeTabs()
    expect(fetchTemplateVersionList).toBeCalled()
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
