jest.mock('@/app-modules/credit-information-inquiry-application/services/base-template')
jest.mock('@/app-modules/credit-information-inquiry-application/helpers/translate')
import BaseTemplateList from '@/app-modules/credit-information-inquiry-application/views/base-templates/list'

import { fetchBaseTemplateList } from '@/app-modules/credit-information-inquiry-application/services/base-template'

import { translateTemplateList } from '@/app-modules/credit-information-inquiry-application/helpers/translate'

import { TEMPLATE_DETAIL_PATH, TEMPLATE_EDIT_PATH } from '@/app-modules/credit-information-inquiry-application/helpers/constants'

const id = 'MA'

describe(`BaseTemplateList - BaseTemplate`, () => {
  let wrapper
  const onRoutePush = jest.fn()

  beforeEach(() => {
    wrapper = global.shallowMount(BaseTemplateList, global.createComponentMocks({
      mocks: {
        $route: { query: {} },
        $router: { push: onRoutePush },
      },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', async () => {
    await wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods generateQuery', () => {
    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
        name: '',
      },
    })
    expect(wrapper.vm.generateQuery(true)).toStrictEqual({
      limit: 10,
      page: 1,
    })

    wrapper.setData({
      query: {
        limit: 10,
        page: 1,
        name: 'demo',
      },
    })
    expect(wrapper.vm.generateQuery(true)).toStrictEqual({
      limit: 10,
      page: 1,
      name: 'demo',
    })
  })

  it('Test methods fetchData fetchBaseTemplateList', async () => {
    fetchBaseTemplateList.mockResolvedValue({ total: 0, list: [] })
    translateTemplateList.mockResolvedValue({ total: 0, list: [] })
    await wrapper.vm.fetchData()
    expect(translateTemplateList).toBeCalledWith({ total: 0, list: [] })
    expect(fetchBaseTemplateList).toBeCalled()
  })

  it('Test methods searchData', async () => {
    await wrapper.vm.searchData()
    fetchBaseTemplateList.mockResolvedValue({ total: 0, list: [] })
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(fetchBaseTemplateList).toBeCalled()
  })

  it('Test methods commandChange', () => {
    let data = { id, command: 'VIEW' }
    wrapper.vm.commandChange(data)
    expect(onRoutePush).toBeCalledWith(`${TEMPLATE_DETAIL_PATH}${id}`)
    data = { id, command: 'EDIT' }
    wrapper.vm.commandChange(data)
    expect(onRoutePush).toBeCalledWith(`${TEMPLATE_EDIT_PATH}${id}`)
  })

  it('Test methods sortChange', async () => {
    let val = ''
    await wrapper.vm.sortChange(val)
    fetchBaseTemplateList.mockResolvedValue({ total: 0, list: [] })
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(fetchBaseTemplateList).toBeCalled()
    val = 'updateTime'
    await wrapper.vm.sortChange(val)
    expect(wrapper.vm.query.sort).toBe(val)
    val = '-updateTime'
    await wrapper.vm.sortChange(val)
    expect(wrapper.vm.query.sort).toBe(val)
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
