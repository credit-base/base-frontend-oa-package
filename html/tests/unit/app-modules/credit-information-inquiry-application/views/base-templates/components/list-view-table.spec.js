import ListViewTable from '@/app-modules/credit-information-inquiry-application/views/base-templates/components/list-view-table'

const { shallowMount, createComponentMocks } = global

describe('ListViewTable - baseTemplates', () => {
  let wrapper
  const onCommand = jest.fn()
  const onSort = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(ListViewTable, {
      ...createComponentMocks({
        router: true,
        mocks: {
          data: {
            isTableExpandAll: false,
          },
          $route: {
            query: {},
          },
          $router: {
          },
        },
      }),
      listeners: {
        command: onCommand,
        sort: onSort,
      },
      propsData: {
        data: [],
        category: 'MA',
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test method handleCommand', async () => {
    const command = 'VIEW'
    const data = { id: 'MA' }
    await wrapper.vm.handleCommand(command, data)
    expect(onCommand).toBeCalledWith({ command: 'VIEW', id: 'MA' })
  })

  it('Test methods sortChange', async () => {
    // ascending descending
    const column = {
      prop: 'updateTime',
      order: '',
    }

    await wrapper.vm.sortChange(column)
    expect(wrapper.emitted().sort).toStrictEqual([[undefined]])

    // column = {
    //   prop: 'updateTime',
    //   order: 'ascending',
    // }
    // await wrapper.vm.sortChange(column)
    // expect(wrapper.emitted().sort).toStrictEqual('updateTime')

    // column = {
    //   prop: 'updateTime',
    //   order: 'descending',
    // }
    // await wrapper.vm.sortChange(column)
    // expect(wrapper.emitted().sort).toStrictEqual('-updateTime')
  })

  it('Test snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
