jest.mock('@/utils/element')
import ResourceCatalogVersion from '@/app-modules/credit-information-inquiry-application/views/base-templates/components/resource-catalog-version'
import { templateVersionList } from '@/app-modules/credit-information-inquiry-application/helpers/mocks'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global

describe('ResourceCatalogVersion - BaseTemplate', () => {
  let wrapper
  const onMessage = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(ResourceCatalogVersion, createComponentMocks({
      mocks: { $message: onMessage },
    }))
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Is a component', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test method sortVersion', () => {
    wrapper.setData({
      versionData: [
        ...templateVersionList,
      ],
    })

    wrapper.vm.sortVersion()
    expect(wrapper.vm.versionData).toStrictEqual([
      {
        id: 'Mg',
        version: '3-D012A100C101',
        description: '2021年版',
        updateTime: 1576317920,
        createTime: 1576317920,
        currentRelease: false,
        addItems: [
          {
            id: 'MA',
            name: '主体名称',
          },
          {
            id: 'MQ',
            name: '法定代表人（负责人） 证件类型',
          },
        ],
        editItems: [
          {
            id: 'MA',
            name: '主体名称',
          },
          {
            id: 'MQ',
            name: '法定代表人（负责人） 证件类型',
          },
        ],
        deleteItems: [
          {
            id: 'MA',
            name: '主体名称',
          },
          {
            id: 'MQ',
            name: '法定代表人（负责人） 证件类型',
          },
        ],
        attachment: {
          name: '登记机关(DJJG).xlsx',
          identify: '登记机关(DJJG).xlsx',
        },
        crew: {
          id: 'MA',
          realName: '平台管理员',
          userGroup: {
            id: 'MA',
            name: '发展和改革委员会',
          },
        },
      },
      {
        id: 'MQ',
        version: '2-D012A100C101',
        description: '2020年版',
        updateTime: 1576317785,
        createTime: 1576317785,
        currentRelease: false,
        addItems: [
          {
            id: 'MA',
            name: '主体名称',
          },
          {
            id: 'MQ',
            name: '法定代表人（负责人） 证件类型',
          },
        ],
        editItems: [
          {
            id: 'MA',
            name: '主体名称',
          },
          {
            id: 'MQ',
            name: '法定代表人（负责人） 证件类型',
          },
        ],
        deleteItems: [
          {
            id: 'MA',
            name: '主体名称',
          },
          {
            id: 'MQ',
            name: '法定代表人（负责人） 证件类型',
          },
        ],
        attachment: {
          name: '登记机关(DJJG).xlsx',
          identify: '登记机关(DJJG).xlsx',
        },
        crew: {
          id: 'MA',
          realName: '平台管理员',
          userGroup: {
            id: 'MA',
            name: '发展和改革委员会',
          },
        },
      }, {
        id: 'MA',
        version: '1-D012A100C101',
        description: '公厅关于运用太数据技术开展城市信用监测工作的通知》(发 改办财金 〔⒛16〕 14⒆ 号)有 关要求,我 委建立了全国 城市信用状况监测预警平台,对全国“1个县级以上城市开',
        updateTime: 1576317765,
        createTime: 1576317765,
        currentRelease: true,
        addItems: [
          {
            id: 'MA',
            name: '主体名称',
          },
          {
            id: 'MQ',
            name: '法定代表人（负责人） 证件类型',
          },
        ],
        editItems: [
          {
            id: 'MA',
            name: '主体名称',
          },
          {
            id: 'MQ',
            name: '法定代表人（负责人） 证件类型',
          },
        ],
        deleteItems: [
          {
            id: 'MA',
            name: '主体名称',
          },
          {
            id: 'MQ',
            name: '法定代表人（负责人） 证件类型',
          },
        ],
        attachment: {
          name: '登记机关(DJJG).xlsx',
          identify: '登记机关(DJJG).xlsx',
        },
        crew: {
          id: 'MA',
          realName: '平台管理员',
          userGroup: {
            id: 'MA',
            name: '发展和改革委员会',
          },
        },
      },
    ])
  })

  it('Test method goCompare', () => {
    const targetValue = '20210921'
    const originValue = '20210928'

    wrapper.vm.goCompare(targetValue, originValue)
    expect(onMessage).toBeCalledWith({ message: '正在开发中...', type: 'warning' })
  })

  it('Test method restoreCurrentVersion', async () => {
    openConfirm.mockResolvedValue()
    const version = '20210921'
    wrapper.vm.restoreCurrentVersion(version)
    await openConfirm({ title: '版本还原', content: '版本号：20210911' })
    expect(onMessage).toBeCalledWith({ type: 'success', message: '版本还原成功' })
  })

  it('Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
