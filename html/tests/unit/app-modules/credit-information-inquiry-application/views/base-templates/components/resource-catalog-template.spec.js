import ResourceCatalogTemplate from '@/app-modules/credit-information-inquiry-application/views/base-templates/components/resource-catalog-template'

const { shallowMount, createComponentMocks } = global

describe('ResourceCatalogTemplate - baseTemplates', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ResourceCatalogTemplate, {
      ...createComponentMocks({
        mocks: {},
      }),
      propsData: {
        data: {
          items: [],
        },
        category: 'MA',
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test method toggleRowExpansion', () => {
    wrapper.vm.toggleRowExpansion()
    expect(wrapper.vm.isTableExpandAll).toBeTruthy()
  })

  it('Test snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
