import FormViewTemplate from '@/app-modules/credit-information-inquiry-application/views/base-templates/components/form-view-template'

const { shallowMount, createComponentMocks } = global

describe('FormViewTemplate - baseTemplates', () => {
  let wrapper
  const onCommand = jest.fn()
  const onSort = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(FormViewTemplate, {
      ...createComponentMocks({
        router: true,
        mocks: {
          data: {
            isTableExpandAll: false,
          },
          $route: {
            query: {},
          },
          $router: {
          },
        },
      }),
      listeners: {
        command: onCommand,
        sort: onSort,
      },
      propsData: {
        data: [],
        category: 'MA',
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
