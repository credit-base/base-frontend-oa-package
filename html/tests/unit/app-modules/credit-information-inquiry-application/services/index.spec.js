jest.mock('@/utils/request')
import request from '@/utils/request'
import {
  fetchEnterpriseList,
  fetchEnterpriseDetail,
  fetchTemplateList,
  fetchTemplateDetail,
  fetchDataDetail,
} from '@/app-modules/credit-information-inquiry-application/services/index'

const params = {}
const id = `MA`

describe(`Services - enterprise`, () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it(`fetchEnterpriseList`, () => {
    fetchEnterpriseList(params)

    expect(request).toBeCalledWith(`/api/enterprises`, { params })
  })

  it(`fetchEnterpriseDetail`, () => {
    fetchEnterpriseDetail(id, params)

    expect(request).toBeCalledWith(`/api/enterprises/${id}`, { params })
  })

  it(`fetchTemplateList`, () => {
    fetchTemplateList(params)

    expect(request).toBeCalledWith(`/api/commonSearchData`, { params })
  })

  it(`fetchTemplateDetail`, () => {
    fetchTemplateDetail(id, params)

    expect(request).toBeCalledWith(`/api/commonTemplates/${id}`, { params })
  })

  it(`fetchDataDetail`, () => {
    fetchDataDetail(id)

    expect(request).toBeCalledWith(`/api/commonSearchData/${id}`)
  })
})
