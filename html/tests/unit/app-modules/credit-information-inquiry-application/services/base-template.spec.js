jest.mock('@/utils/request')
import request from '@/utils/request'
import {
  updateBaseTemplate,
  fetchBaseTemplateList,
  fetchBaseTemplateDetail,
  fetchTemplateEditInfo,
  fetchTemplateConfigs,
  fetchTemplateVersionList,
} from '@/app-modules/credit-information-inquiry-application/services/base-template'
import {
  templateVersionList,
} from '@/app-modules/credit-information-inquiry-application/helpers/mocks'

const params = {}
const useMock = { useMock: true }
const id = `MA`

describe(`Services - base-template`, () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it(`updateBaseTemplate`, () => {
    updateBaseTemplate(id, params)

    expect(request).toBeCalledWith(`/api/baseTemplates/${id}/edit`, { method: 'POST', params })
  })

  it(`fetchBaseTemplateList`, () => {
    fetchBaseTemplateList(params)

    expect(request).toBeCalledWith(`/api/baseTemplates`, { params })
  })

  it(`fetchBaseTemplateDetail`, () => {
    fetchBaseTemplateDetail(id)

    expect(request).toBeCalledWith(`/api/baseTemplates/${id}`)
  })

  it(`fetchTemplateEditInfo`, () => {
    fetchTemplateEditInfo(id)

    expect(request).toBeCalledWith(`/api/baseTemplates/${id}`)
  })

  it(`fetchTemplateConfigs`, () => {
    fetchTemplateConfigs(params)

    expect(request).toBeCalledWith(`/api/template/configure`, { params })
  })

  it(`fetchTemplateVersionList`, () => {
    fetchTemplateVersionList(id, params)

    expect(request).toBeCalledWith(`/api/gbTemplates/${id}/versions`, params)

    expect(fetchTemplateVersionList(id, useMock)).toStrictEqual({
      list: [
        ...templateVersionList,
      ],
      total: 1,
    })
  })
})
