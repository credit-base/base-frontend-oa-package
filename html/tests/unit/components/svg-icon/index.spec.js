import SvgIcon from '@/components/svg-icon'

const { shallowMount } = global

describe('Test SvgIcon Components', () => {
  const name = 'business'
  let wrapper = null

  beforeEach(() => {
    wrapper = shallowMount(SvgIcon, {
      propsData: { name },
    })
  })

  it('Test svg class', () => {
    const dom = wrapper.find('svg')
    expect(dom.classes()).toContain(`svg-icon-${name}`)
  })

  it('Test SvgIcon Props', async () => {
    expect(wrapper.vm.name).toBe(name)

    wrapper.setProps({ name: 'column' })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.name).toBe('column')
    const dom = wrapper.find('svg')
    expect(dom.classes()).toContain(`svg-icon-column`)
  })

  it('Test SvgIcon Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
