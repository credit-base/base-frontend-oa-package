import AppCapsule from '@/components/app-capsule'
// import { THEME_ENUM } from '@/constants/app'

const { shallowMount } = global
const type = 'success'
const label = '国标资源目录'

describe('Test Components AppCapsule', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(AppCapsule, {
      propsData: {
        type,
        label,
      },
    })
  })

  it('It passes with info type', () => {
    const validator = AppCapsule.props.type.validator
    expect(validator('info')).toBe(true)
    expect(validator('error')).toBe(false)
    expect(validator('')).toBe(true)
  })

  it('It test computed->themeClass', () => {
    const themeClass = AppCapsule.computed.themeClass
    const localThis = { type: wrapper.props().type }
    expect(themeClass.call(localThis)).toBe('theme-' + wrapper.props().type)
    expect(themeClass.call({})).toBe('')
  })

  it('Test AppCapsule snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
