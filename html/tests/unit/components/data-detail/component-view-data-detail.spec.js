import ComponentViewDataDetail from '@/components/data-detail/component-view-data-detail'
const { shallowMount, createComponentMocks } = global

describe('Test Components ComponentViewDataDetail', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ComponentViewDataDetail, {
      ...createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test ComponentViewDataDetail Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
