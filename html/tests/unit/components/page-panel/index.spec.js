import PagePanel from '@/components/page-panel'

const { shallowMount, createComponentMocks } = global

describe('Test PagePanel Component', () => {
  let wrapper
  const onUpdate = jest.fn()
  const onTabClick = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(PagePanel, {
      ...createComponentMocks({}),
      listeners: {
        'update:activeTab': onUpdate,
        'tab-click': onTabClick,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleClick', async () => {
    const event = {
      name: global.randomString(),
    }
    await wrapper.vm.handleClick(event)

    expect(onUpdate).toBeCalled()
    expect(onTabClick).toBeCalled()
  })

  it('Test PagePanel snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
