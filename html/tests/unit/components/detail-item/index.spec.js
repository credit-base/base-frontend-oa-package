import DetailItem from '@/components/detail-item'

const { shallowMount } = global

const label = '标题'
const content = '内容'

describe('Test DetailItem Components', () => {
  let wrapper = ''

  beforeEach(() => {
    jest.resetModules()
    jest.resetAllMocks()

    wrapper = shallowMount(DetailItem, {
      propsData: { label },
      slots: {
        default: content,
      },
    })
  })

  it('Test props with slot default.', () => {
    expect(wrapper.find('.detail-row-label').text()).toBe(label + '：')
    expect(wrapper.find('.detail-row-main').text()).toBe(content)
  })

  // it('Test props with slots', () => {
  //   wrapper.setProps({
  //     label, content,
  //   })

  //   expect(wrapper.find('.detail-row-label').text()).toBe(label)
  //   expect(wrapper.find('.detail-row-main').text()).toBe(content)
  // })

  // it('Test DetailItem Snapshot', () => {
  //   expect(wrapper.html()).toMatchSnapshot()
  // })
})
