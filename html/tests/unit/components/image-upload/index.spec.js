jest.mock('@/utils/validate')
jest.mock('@/utils')
import ImageUpload from '@/components/image-upload'
import { renameFile } from '@/utils'
import { isValidFiles } from '@/utils/validate'

const { shallowMount, createComponentMocks } = global

describe('Test ImageUpload Component', () => {
  let wrapper
  const onFormatError = jest.fn()
  const onSizeExceed = jest.fn()
  const onSuccess = jest.fn()
  const onError = jest.fn()
  const onMessage = jest.fn()
  const store = {
    getters: {
      action: 'http://oss.base.qixinyun.com/',
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(ImageUpload, {
      ...createComponentMocks({
        mocks: {
          $store: store,
          $message: onMessage,
        },
      }),
      listeners: {
        'format-error': onFormatError,
        'size-exceed': onSizeExceed,
        success: onSuccess,
        error: onError,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods beforeUpload', () => {
    const file = {
      lastModified: 1621851564577,
      name: 'demo.png',
      size: 18893,
      type: '',
      uid: 1622123093993,
      webkitRelativePath: '',
    }
    wrapper.vm.beforeUpload(file)
    isValidFiles.mockResolvedValue(true)
    expect(wrapper.vm.fileConfig.identify).toBe(renameFile(file))
  })

  it('Test methods onSuccess', async () => {
    const file = {
      size: global.randomString(),
      type: global.randomString(),
    }
    const res = {}
    await wrapper.vm.onSuccess(res, file)

    expect(onSuccess).toBeCalled()
  })

  it('Test methods onError', async () => {
    const file = {
      size: global.randomString(),
      type: global.randomString(),
    }
    const error = {}
    await wrapper.vm.onError(error, file)

    expect(onError).toBeCalled()
  })

  it('Test ImageUpload snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
