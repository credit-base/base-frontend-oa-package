jest.mock('@/utils/validate')
jest.mock('@/utils')
import PicturesUpload from '@/components/pictures-upload'
import { renameFile } from '@/utils'
import { isValidFiles } from '@/utils/validate'

const { shallowMount, createComponentMocks } = global

describe('Test PicturesUpload Component', () => {
  let wrapper
  const onError = jest.fn()
  const onMessage = jest.fn()
  const store = {
    getters: {
      action: 'http://oss.base.qixinyun.com/',
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(PicturesUpload, {
      ...createComponentMocks({
        mocks: {
          $store: store,
          $message: onMessage,
        },
        listeners: { error: onError },
        propsData: {
          extensions: ['png', 'jpg', 'jpeg'],
          fileSize: 5,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods beforeUpload', () => {
    const file = {
      lastModified: 1621851564577,
      name: 'demo.png',
      size: 18893,
      type: '',
      uid: 1622123093993,
      webkitRelativePath: '',
    }
    wrapper.vm.beforeUpload(file)
    isValidFiles.mockResolvedValue(true)
    expect(wrapper.vm.fileConfig.identify).toBe(renameFile(file))
  })

  it('Test methods onSuccess onMessage', () => {
    const data = { name: global.randomString() }
    const res = {}
    const fileList = []
    wrapper.vm.onSuccess(res, data, fileList)
    expect(onMessage).toBeCalled()
  })

  it('Test methods onExceed onMessage', () => {
    wrapper.vm.onExceed()
    expect(onMessage).toBeCalled()
  })

  it('Test methods handleRemove', () => {
    const file = {
      lastModified: 1621851564577,
      name: 'demo.png',
      size: 18893,
      type: '',
      url: 'demo.png',
      uid: 1622123093993,
      webkitRelativePath: '',
    }
    wrapper.setData({
      fileList: [
        {
          lastModified: 1621851564578,
          name: 'cover.jpeg',
          size: 18894,
          type: '',
          url: 'demo.png',
          uid: 1622123093945,
          webkitRelativePath: '',
        },
        {
          lastModified: 1621851564577,
          name: 'demo.png',
          size: 18893,
          type: '',
          url: 'demo.png',
          uid: 1622123093993,
          webkitRelativePath: '',
        },
      ],
    })
    wrapper.vm.handleRemove(file)
    expect(wrapper.vm.fileList).toStrictEqual([
      {
        lastModified: 1621851564578,
        name: 'cover.jpeg',
        size: 18894,
        type: '',
        url: 'demo.png',
        uid: 1622123093945,
        webkitRelativePath: '',
      },
    ])
  })

  it('Test methods handlePictureCardPreview', () => {
    const file = {
      lastModified: 1621851564577,
      name: 'demo.png',
      size: 18893,
      type: '',
      url: 'demo.png',
      uid: 1622123093993,
      webkitRelativePath: '',
    }
    wrapper.vm.handlePictureCardPreview(file)
    expect(wrapper.vm.dialogVisible).toBeTruthy()
  })

  it('Test methods onError', async () => {
    const file = {
      lastModified: 1621851564577,
      name: 'demo.png',
      size: 18893,
      type: '',
      uid: 1622123093993,
      webkitRelativePath: '',
    }
    const error = {}
    await wrapper.vm.onError(error, file)

    expect(onError).toBeCalled()
  })

  it('Test PicturesUpload snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
