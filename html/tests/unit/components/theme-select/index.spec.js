import ThemeSelect from '@/components/theme-select'
import { THEMES_CONFIG } from '@/constants/theme'

const { shallowMount, createComponentMocks } = global

describe('Test ThemeSelect Components', () => {
  let wrapper = null
  const onChange = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(ThemeSelect, createComponentMocks({
      propsData: {
        theme: THEMES_CONFIG[0].class,
        themes: THEMES_CONFIG,
      },
      listeners: { change: onChange },
    }))
  })

  afterEach(() => {
    jest.resetModules()
  })

  it('Test Props', () => {
    expect(wrapper.vm.theme).toBe(THEMES_CONFIG[0].class)
    expect(wrapper.vm.themes).toStrictEqual(THEMES_CONFIG)
  })

  it('Test no themes', () => {
    const wrap = shallowMount(ThemeSelect, createComponentMocks({
      propsData: {
        theme: THEMES_CONFIG[0].class,
      },
    }))

    expect(wrap.vm.themes).toStrictEqual([])
  })

  it('Test methods handleCommand', () => {
    const idx = 1

    wrapper.vm.handleCommand(THEMES_CONFIG[idx].class)
    expect(onChange).toBeCalledWith(THEMES_CONFIG[idx].class)
  })

  it('Test $emit', () => {
    wrapper.vm.$emit('change', THEMES_CONFIG[1].class)

    const emitted = wrapper.emitted()

    expect(emitted.change).toBeTruthy()
  })

  it('Test ThemeSelect Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
