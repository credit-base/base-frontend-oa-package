import GoBack from '@/components/go-back'

const { shallowMount, createComponentMocks, randomString } = global

describe('Test GoBack Component', () => {
  let wrapper = null
  const onRouterPush = jest.fn()
  const onRouterGo = jest.fn()
  const onActionDispatch = jest.fn(() => Promise.resolve())

  beforeEach(() => {
    jest.resetModules()
    jest.resetAllMocks()

    wrapper = shallowMount(GoBack, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: { query: {} },
          $router: {
            push: onRouterPush,
            go: onRouterGo,
          },
          $store: {
            dispatch: onActionDispatch,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test default props', async () => {
    const btn = wrapper.find('.app-container-close')
    await btn.trigger('click')
    expect(onActionDispatch).toBeCalledWith('delView', { query: {} })
    expect(onRouterGo).toBeCalled()
  })

  it('Test click', async () => {
    wrapper.setProps({ route: randomString() })
    const btn = wrapper.find('.app-container-close')
    await btn.trigger('click')
    expect(onActionDispatch).toBeCalledWith('delView', { query: {} })
    expect(onRouterPush).toBeCalled()
  })

  it('Test GoBack Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
