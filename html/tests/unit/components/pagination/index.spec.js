import Pagination from '@/components/pagination'

const { shallowMount } = global

describe('Test Pagination Components', () => {
  let wrapper = ''
  const sizeChange = jest.fn()
  const currentChange = jest.fn()
  const pageSizes = [10, 20, 30, 50]

  beforeEach(() => {
    wrapper = shallowMount(Pagination, {
      propsData: {
        total: 0,
      },
      mocks: {
        handleSizeChange: sizeChange,
        handleCurrentChange: currentChange,
      },
    })
  })

  it('Test Default Props pageSizes', () => {
    const dom = wrapper.find('el-pagination-stub')

    expect(dom.attributes('pagesizes')).toContain(pageSizes)
  })

  it('Test set Props pageSizes', async () => {
    const tempPageSize = [1, 15, 45]
    wrapper.setProps({
      pageSizes: tempPageSize,
    })
    await wrapper.vm.$forceUpdate()

    const dom = wrapper.find('el-pagination-stub')

    expect(dom.attributes('pagesizes')).toContain(tempPageSize)
  })

  it('Test methods handleSizeChange', async () => {
    await wrapper.vm.handleSizeChange(60)
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted().pagination).toBeTruthy()
  })

  it('Test methods handleCurrentChange', async () => {
    await wrapper.vm.handleCurrentChange(2)
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted().pagination).toBeTruthy()
  })

  it('Test computed currentPage', () => {
    wrapper.vm.currentPage = 1
    expect(wrapper.emitted()['update:page']).toBeTruthy()
  })

  it('Test computed pageSize', () => {
    wrapper.vm.pageSize = 20
    expect(wrapper.emitted()['update:limit']).toBeTruthy()
  })

  it('Test Pagination Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
