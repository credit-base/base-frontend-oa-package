import DataReportingInstructions from '@/components/data-reporting-instructions'

const { shallowMount, createComponentMocks } = global

describe('Test ImageUpload Component', () => {
  let wrapper

  const store = {
    getters: {
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(DataReportingInstructions, {
      ...createComponentMocks({
        mocks: {
          $store: store,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods show', async () => {
    await wrapper.vm.show()
    expect(wrapper.vm.isVisible).toBeTruthy()
  })

  it('Test methods hide', async () => {
    await wrapper.vm.hide()
    expect(wrapper.vm.isVisible).toBeFalsy()
  })

  it('Test methods handleSubmit', async () => {
    await wrapper.vm.handleSubmit()
    expect(wrapper.vm.isCheck).toBeFalsy()
    wrapper.vm.$emit('read', true)
  })

  it('Test methods handleClose', async () => {
    await wrapper.vm.handleClose()
    expect(wrapper.vm.isCheck).toBeFalsy()
    wrapper.vm.$emit('read', false)
  })

  it('Test DataReportingInstructions snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
