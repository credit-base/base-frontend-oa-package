import './match-media.mock'
import MockTinymce from '@/components/mock-tinymce'

const { shallowMount, createComponentMocks } = global

describe('Test MockTinymce Component', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(MockTinymce, {
      ...createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test MockTinymce snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
