import VideoPlayer from '@/components/video-player'

const { shallowMount, createComponentMocks } = global

describe('Test vue VideoPlayer', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(VideoPlayer, {
      ...createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test VideoPlayer snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
