import TemplateUpload from '@/components/template-upload'

const { shallowMount, createComponentMocks } = global

describe('Test TemplateUpload Component', () => {
  let wrapper
  const onFormatError = jest.fn()
  const onSizeExceed = jest.fn()
  const onSuccess = jest.fn()
  const onError = jest.fn()
  const onMessage = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(TemplateUpload, {
      ...createComponentMocks({
        mocks: {
          $message: onMessage,
        },
        propsData: {
          uploadUrl: '/uploads',
        },
      }),
      listeners: {
        'format-error': onFormatError,
        'size-exceed': onSizeExceed,
        success: onSuccess,
        error: onError,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods show', async () => {
    await wrapper.vm.show()
    expect(wrapper.vm.isVisible).toBeTruthy()
  })

  it('Test methods hide', async () => {
    await wrapper.vm.hide()
    expect(wrapper.vm.isVisible).toBeFalsy()
  })

  it('Test methods verifyFileName', () => {
    let file = null
    expect(wrapper.vm.verifyFileName(file)).toBeFalsy()
    file = 'demo.xlsx'
    expect(wrapper.vm.verifyFileName(file)).toBeFalsy()
    file = 'DEMO01.xlsx'
    expect(wrapper.vm.verifyFileName(file)).toBeFalsy()
    file = 'DEMO_.xlsx'
    expect(wrapper.vm.verifyFileName(file)).toBeFalsy()
    file = 'DEMO_01_01.xlsx'
    expect(wrapper.vm.verifyFileName(file)).toBeTruthy()
  })

  it('Test methods handleSuccess', async () => {
    let res = {
      status: 1,
      data: {},
    }
    await wrapper.vm.handleSuccess()
    expect(onMessage).toBeCalledWith({ type: 'error', message: '接口返回错误' })

    await wrapper.vm.handleSuccess(res)
    expect(onMessage).toBeCalledWith({ type: 'success', message: '已成功上传' })

    res = {
      status: 0,
      data: {},
    }
    await wrapper.vm.handleSuccess(res)
    expect(onMessage).toBeCalledWith({ type: 'error', message: '接口返回错误' })

    res = {
      status: 0,
      data: {
        detail: '目录类别错误',
      },
    }
    await wrapper.vm.handleSuccess(res)
    expect(onMessage).toBeCalledWith({ type: 'error', message: '目录类别错误' })
  })

  it('Test methods handleSubmitUpload', async () => {
    await wrapper.vm.handleSubmitUpload()
    expect(wrapper.vm.isVisible).toBeFalsy()
    expect(wrapper.vm.fileList).toStrictEqual([])
  })

  it('Test TemplateUpload snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
