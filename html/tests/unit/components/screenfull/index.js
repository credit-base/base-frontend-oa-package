// import Screenfull from '@/components/screen-full'
// import screenfull from 'screenfull'

// const { shallowMount } = global

// describe('Test Screenfull Components ', () => {
//   const onMessage = jest.fn()
//   const onCloseSideBar = jest.fn(() => Promise.resolve())
//   let wrapper = ''

//   beforeEach(() => {
//     wrapper = shallowMount(Screenfull, {
//       mocks: {
//         $message: onMessage,
//         $store: {
//           dispatch: onCloseSideBar,
//         },
//       },
//     })
//   })

//   afterEach(() => {
//     wrapper && wrapper.destroy()
//   })

//   it('Test Default isFullscreen', () => {
//     expect(wrapper.vm.isFullscreen).toBeFalsy()
//   })

//   it('Test handleClick toBe called', () => {
//     expect(screenfull.isEnabled).toBeFalsy()
//     wrapper.vm.handleClick()
//     expect(onMessage).toBeCalled()
//   })

//   it('Test svg-icon class', async () => {
//     const dom = wrapper.find('svg-icon-stub')

//     expect(dom.attributes().name).toBe('fullscreen-in')
//     wrapper.setData({ isFullscreen: true })
//     await wrapper.vm.$forceUpdate()
//     expect(dom.attributes().name).toBe('fullscreen-out')
//   })

//   it('Test onChange', () => {
//     screenfull.isFullscreen = true
//     wrapper.vm.onChange()
//     expect(onCloseSideBar).toBeCalled()
//   })

//   it('Test Screenfull Snapshot', () => {
//     expect(wrapper.html()).toMatchSnapshot()
//   })

//   it('Test Screenfull Snapshot', () => {
//     expect(wrapper.html()).toMatchSnapshot()
//   })
// })
