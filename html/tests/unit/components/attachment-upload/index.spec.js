jest.mock('@/utils/validate')
jest.mock('@/utils')
import AttachmentsUploader from '@/components/attachments-upload'
import { renameFile } from '@/utils'
import { isValidFiles } from '@/utils/validate'

const { shallowMount, createComponentMocks } = global

describe('Test AttachmentsUploader Component', () => {
  let wrapper
  const onError = jest.fn()
  const onMessage = jest.fn()
  const store = {
    getters: {
      action: 'http://oss.base.qixinyun.com/',
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(AttachmentsUploader, {
      ...createComponentMocks({
        mocks: {
          $store: store,
          $message: onMessage,
        },
        listeners: { error: onError },
        propsData: {
          extensions: ['doc', 'docx', 'xls', 'xlsx', 'pdf', 'rar', 'zip', 'ppt', 'pptx'],
          fileSize: 5,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods beforeUpload', () => {
    const file = {
      lastModified: 1621851564577,
      name: '研发中心内部任务变更申请表（模板）.docx',
      size: 18893,
      type: '',
      uid: 1622123093993,
      webkitRelativePath: '',
    }
    wrapper.vm.beforeUpload(file)
    isValidFiles.mockResolvedValue(true)
    expect(wrapper.vm.fileConfig.identify).toBe(renameFile(file))
  })

  it('Test methods onSuccess onMessage', () => {
    const data = { name: global.randomString() }
    const res = {}
    const fileList = []
    wrapper.vm.onSuccess(res, data, fileList)
    expect(onMessage).toBeCalled()
  })

  it('Test methods onExceed onMessage', () => {
    wrapper.vm.onExceed()
    expect(onMessage).toBeCalled()
  })

  it('Test methods handleRemove', () => {
    const file = {}
    const fileList = []
    wrapper.vm.handleRemove(file, fileList)
    expect(wrapper.vm.fileList).toBe(fileList)
  })

  it('Test methods onError', async () => {
    const file = {
      size: global.randomString(),
      type: global.randomString(),
    }
    const error = {}
    await wrapper.vm.onError(error, file)

    expect(onError).toBeCalled()
  })

  it('Test AttachmentsUploader snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
