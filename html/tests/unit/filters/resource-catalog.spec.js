/**
 * @file 资源目录过滤器 - 测试
 * @module filters/resource-catalog
 */

import {
  formatDataType,
  formatSubjectCategory,
} from '@/filters/resource-catalog'

describe(`Filters - resource-catalog`, () => {
  it(`formatDataType`, () => {
    expect(formatDataType('MA')).toBe('字符型')
    expect(formatDataType('MQ')).toBe('⽇期型')
    expect(formatDataType('NQ')).toBe('集合型')
    expect(formatDataType('NA')).toBe('枚举型')
    expect(formatDataType('Mg')).toBe('整数型')
    expect(formatDataType('Mw')).toBe('浮点型')
  })

  it(`formatSubjectCategory`, () => {
    expect(formatSubjectCategory('MA')).toBe('法人及非法人组织')
    expect(formatSubjectCategory('MQ')).toBe('自然人')
    expect(formatSubjectCategory('Mg')).toBe('个体工商户')
  })
})
