/**
 * @file 规则管理过滤器 - 测试
 * @module filters/rule-manage
 */

import {
  formatRuleStatus,
} from '@/filters/rule-manage'

describe(`Filters - rule-manage`, () => {
  it(`formatRuleStatus`, () => {
    expect(formatRuleStatus('MQ')).toBe('已通过')
    expect(formatRuleStatus('LC0')).toBe('已驳回')
    expect(formatRuleStatus('Lw')).toBe('待审核')
    expect(formatRuleStatus('LC8')).toBe('已撤销')
  })
})
