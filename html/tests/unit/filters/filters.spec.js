/**
 * @file 全局过滤器 - 测试
 * @module filters/filters
 */

import {
  formatBoolean,
} from '@/filters/filters'

describe(`Filters - filters`, () => {
  it(`formatBoolean`, () => {
    expect(formatBoolean('MA')).toBe('是')
    expect(formatBoolean('Lw')).toBe('否')
  })
})
