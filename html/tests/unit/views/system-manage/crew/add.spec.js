import CrewAdd from '@/views/system-manage/crew/add'

const { shallowMount, createComponentMocks } = global

describe('Test vue CrewAdd', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(CrewAdd, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test ViewRedirect Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
