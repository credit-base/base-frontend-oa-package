import CrewEdit from '@/views/system-manage/crew/edit'

const { shallowMount, createComponentMocks } = global

describe('Test vue CrewEdit', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(CrewEdit, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test ViewRedirect Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
