jest.mock('@/utils/element')
jest.mock('@/services/crew')
import CrewDetail from '@/views/system-manage/crew/detail'
// import { fetchCrewDetail, updateCrewStatus } from '@/services/crew'
import { updateCrewStatus } from '@/services/crew'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()
const onMessage = jest.fn()

describe('Test vue CrewDetail', () => {
  let wrapper
  const store = {
    getters: {
      userInfo: {
        id: global.randomString(),
      },
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(CrewDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
          },
          $message: onMessage,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods updateCrewStatus - changeStatus', async () => {
    openConfirm.mockResolvedValue()
    updateCrewStatus.mockResolvedValue()

    await wrapper.vm.changeStatus('enable', id)
    expect(updateCrewStatus).toBeCalledWith(id, 'enable')
  })

  it('Test methods fetchCrewDetail - fetchData', async () => {
    // fetchCrewDetail.mockResolvedValue()
    // await wrapper.vm.fetchData(id)
    // expect(fetchCrewDetail).toBeCalledWith(id)
  })

  it('Test ViewRedirect Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
