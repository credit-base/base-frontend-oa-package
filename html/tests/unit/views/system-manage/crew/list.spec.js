jest.mock('@/services/crew')
jest.mock('@/services/department')
import CrewList from '@/views/system-manage/crew/list'
import { fetchCrew } from '@/services/crew'

const { shallowMount, createComponentMocks } = global
// const id = window.randomString()

describe('Test vue CrewList', () => {
  let wrapper
  const onMessage = jest.fn()
  const store = {
    getters: {
      userInfo: {
        id: global.randomString(),
      },
      fullUserGroup: [],
      superAdminPurview: false,
      platformAdminPurview: true,
      userGroupAdminPurview: false,
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(CrewList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
          $store: store,
          $message: onMessage,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods initQuery', async () => {
    wrapper.vm.initQuery()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
  })

  it('Test methods sortChange', async () => {
    const val = 'updateTime'
    fetchCrew.mockResolvedValue()
    await wrapper.vm.sortChange(val)
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(wrapper.vm.query.sort).toBe(val)
    expect(fetchCrew).toBeCalled()
  })

  it('Test methods searchData', async () => {
    await wrapper.vm.searchData()
    fetchCrew.mockResolvedValue()
    expect(wrapper.vm.query.page).toBe(1)
    expect(wrapper.vm.query.limit).toBe(10)
    expect(fetchCrew).toBeCalled()
  })

  it('Test methods changeSearchType', async () => {
    wrapper.setData({
      searchType: 'realName',
    })
    await wrapper.vm.changeSearchType()
    expect(wrapper.vm.searchInputPlaceholder).toBe('请输入员工姓名')
    wrapper.setData({
      searchType: 'cellphone',
    })
    await wrapper.vm.changeSearchType()
    expect(wrapper.vm.searchInputPlaceholder).toBe('请输入手机号')
  })

  it('Test CrewList Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
