import TableViewLogs from '@/views/system-manage/crew/components/table-view-logs'

const { shallowMount, createComponentMocks } = global

describe('Test vue TableViewLogs', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(TableViewLogs, {
      ...createComponentMocks({}),
      propsData: {
        data: [],
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })
  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test TableViewLogs Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
