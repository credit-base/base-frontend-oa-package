jest.mock('@/utils/element')
jest.mock('@/services/crew')
import ListViewCrew from '@/views/system-manage/crew/components/list-view-crew'
import { updateCrewStatus } from '@/services/crew'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = window.randomString()

describe('Test vue ListViewCrew', () => {
  let wrapper

  const store = {
    getters: {
      userInfo: {
        id,
      },
    },
  }
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(ListViewCrew, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: {},
            query: {},
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
      propsData: {
        isEdit: false,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods isOwn', async () => {
    expect(wrapper.vm.isOwn(id)).toBeTruthy()
  })

  it('Test methods changeStatus', async () => {
    openConfirm.mockResolvedValue()
    updateCrewStatus.mockResolvedValue()
    await wrapper.vm.changeStatus('ENABLE', id)
    expect(updateCrewStatus).toBeCalledWith(id, 'enable')
  })

  it('Test ListViewCrew Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
