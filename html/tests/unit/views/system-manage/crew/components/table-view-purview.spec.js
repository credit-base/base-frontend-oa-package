import TableViewPurview from '@/views/system-manage/crew/components/table-view-purview'

const { shallowMount, createComponentMocks } = global

describe('Test vue TableViewPurview', () => {
  let wrapper
  const store = {
    getters: {
      superAdminPurview: true,
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(TableViewPurview, {
      ...createComponentMocks({
        mocks: {
          $store: store,
        },

      }),
      propsData: {
        purview: [],
        category: 'MA',
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })
  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test TableViewPurview Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
