jest.mock('@/services/department')
jest.mock('@/services/crew')
jest.mock('@/services/utils')
import FormViewCrew from '@/views/system-manage/crew/components/form-view-crew'
import { fetchDepartmentList } from '@/services/department'
import {
  createCrew,
  fetchCrewEditInfo,
  updateCrew,
} from '@/services/crew'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()
describe('Test vue FormViewCrew', () => {
  let wrapper

  const store = {
    getters: {
      purview: [],
      userGroup: {},
      superAdminPurview: false,
      platformAdminPurview: false,
      userGroupAdminPurview: false,
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(FormViewCrew, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: {},
            query: {},
          },
        },
      }),
      propsData: {
        isEdit: false,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })
  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods getAllDepartment - fetchDepartmentList', async () => {
    await wrapper.vm.getAllDepartment(id)
    expect(fetchDepartmentList).toBeCalled()
  })

  it('Test methods getCrewEditInfo - fetchCrewEditInfo', async () => {
    await wrapper.vm.getCrewEditInfo()
    expect(fetchCrewEditInfo).toBeCalled()
  })

  it('Test methods submitCreate - createCrew', async () => {
    const params = {
      cardId: '612724199512021203',
      category: 'MA',
      cellphone: 18800000000,
      departmentId: 'MA',
      purview: [1, 2, 3],
      realName: 'Demo',
      userGroupId: 'MA',
    }
    await wrapper.vm.submitCreate(params, 'message')
    expect(createCrew).toBeCalled()
  })

  it('Test methods submitUpdate - updateCrew', async () => {
    const params = {
      cardId: '612724199512021203',
      category: 'MA',
      cellphone: 18800000000,
      departmentId: 'MA',
      purview: [1, 2, 3],
      realName: 'Demo',
      userGroupId: 'MA',
    }
    await wrapper.vm.submitUpdate(params, 'message')
    expect(updateCrew).toBeCalled()
  })

  it('Test FormViewCrew Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
