import FormViewPurview from '@/views/system-manage/crew/components/form-view-purview'

const { shallowMount, createComponentMocks } = global

describe('Test vue FormViewPurview', () => {
  let wrapper

  const store = {
    getters: {
      purview: [],
      userGroup: {},
      superAdminPurview: false,
      platformAdminPurview: false,
      userGroupAdminPurview: false,
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(FormViewPurview, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: {},
            query: {},
          },
        },
      }),
      propsData: {
        isEdit: false,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })
  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test FormViewPurview Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
