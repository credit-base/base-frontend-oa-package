import ChartViewStatisticsTemplate from '@/views/system-manage/crew/components/chart-view-statistics-template'

const { shallowMount, createComponentMocks } = global

describe('Test vue ChartViewStatisticsTemplate', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ChartViewStatisticsTemplate, {
      ...createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })
  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test ChartViewStatisticsTemplate Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
