import TableViewPurview from '@/views/system-manage/personal-information/components/table-view-purview'

const { shallowMount, createComponentMocks } = global

describe('Test vue TableViewPurview', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(TableViewPurview, {
      ...createComponentMocks({}),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })
  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test TableViewPurview Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
