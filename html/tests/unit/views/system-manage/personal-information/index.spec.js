import PersonalInformation from '@/views/system-manage/personal-information'

const { shallowMount, createComponentMocks } = global
const store = {
  getters: {
    userInfo: {},
  },
}

describe('Test vue PersonalInformation', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(PersonalInformation, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test PersonalInformation Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
