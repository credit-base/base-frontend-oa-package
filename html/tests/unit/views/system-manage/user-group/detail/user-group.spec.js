jest.mock('@/services/user-group')
import UserGroupDetail from '@/views/system-manage/user-group/detail/user-group'
import { fetchUserGroupDetail } from '@/services/user-group'

const { shallowMount, createComponentMocks } = global
const id = 'MA'

describe('Test vue UserGroupDetail', () => {
  let wrapper
  const store = {
    getters: {
      operationPurview: true,
    },
  }
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(UserGroupDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchUserGroupDetail', async () => {
    fetchUserGroupDetail.mockResolvedValue()
    await wrapper.vm.fetchData()

    expect(fetchUserGroupDetail).toBeCalledWith(id)
  })

  it('Test methods addDepartment', async () => {
    await wrapper.vm.addDepartment()
    expect(wrapper.vm.$router.push).toBeCalledWith({
      name: 'DepartmentAdd',
      query: {
        route: `/user-groups/detail/${id}`,
        userGroupId: id,
      },
    })
  })

  it('Test methods addCrew', async () => {
    await wrapper.vm.addCrew()
    expect(wrapper.vm.$router.push).toBeCalledWith({
      name: 'CrewAdd',
      query: {
        route: `/user-groups/detail/${id}`,
        userGroupId: id,
      },
    })
  })

  it('Test UserGroupDetail Snapshot', () => {
    expect(wrapper.html()).toMatchInlineSnapshot(`
      <page-panel-stub tabtitle="委办局详情" activetab="" isback="true" route="/user-groups"><template></template>
        <div class="panel">
          <div class="panel-header">
            <h3 class="panel-title">
              基本信息
            </h3>
          </div>
          <div class="panel-content">
            <el-row-stub tag="div" gutter="24" justify="start">
              <el-col-stub span="24" tag="div" lg="24">
                <div class="page-container el-loading-parent--relative">
                  <detail-item-stub label="委办局名称">

                  </detail-item-stub>
                  <detail-item-stub label="委办局简称">

                  </detail-item-stub>
                  <detail-item-stub label="委办局下属科室数量">
                    0 个
                  </detail-item-stub>
                  <detail-item-stub label="委办局下属员工数量">
                    0 位
                  </detail-item-stub>
                  <div class="el-loading-mask" style="display: none;">
                    <div class="el-loading-spinner"><svg viewBox="25 25 50 50" class="circular">
                        <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
                      </svg>
                      <!---->
                    </div>
                  </div>
                </div>
              </el-col-stub>
            </el-row-stub>
          </div>
        </div>
        <el-tabs-stub value="department" tabposition="top">
          <el-tab-pane-stub label="科室列表(0)" name="department">
            <tabviewdepartment-stub data="" route="/user-groups/detail/MA"></tabviewdepartment-stub>
          </el-tab-pane-stub>
        </el-tabs-stub>
      </page-panel-stub>
    `)
  })
})
