jest.mock('@/services/department')
import DepartmentDetail from '@/views/system-manage/user-group/detail/department'
import { fetchDepartmentDetail } from '@/services/department'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()

describe('Test vue DepartmentDetail', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(DepartmentDetail, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            params: {
              id,
            },
            query: {},
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchData - fetchDepartmentDetail', async () => {
    fetchDepartmentDetail.mockResolvedValue()
    await wrapper.vm.fetchData(id)
    expect(fetchDepartmentDetail).toBeCalled()
  })

  it('Test methods goEdit', async () => {
    await wrapper.vm.goEdit(id)
    expect(wrapper.vm.$router.push).toBeCalledWith(`/department/edit/${id}`)
  })

  it('Test DepartmentDetail Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
