
import TabViewDepartment from '@/views/system-manage/user-group/components/tab-view-department'

const { shallowMount, createComponentMocks } = global
const route = window.randomString()
describe('Test vew TabViewDepartment', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(TabViewDepartment, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $router: {
            push: onRouterPush,
          },
        },
      }),
      propsData: {
        route: route,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleCommand', async () => {
    const id = global.randomString()
    await wrapper.vm.handleCommand('VIEW', id)
    expect(wrapper.vm.$router.push).toBeCalled()
    await wrapper.vm.handleCommand('EDIT', id)
    expect(wrapper.vm.$router.push).toBeCalled()
  })

  it('Test TabViewDepartment Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
