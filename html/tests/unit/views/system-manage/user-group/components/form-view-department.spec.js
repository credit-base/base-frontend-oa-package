jest.mock('@/services/department')
import FormViewDepartment from '@/views/system-manage/user-group/components/form-view-department'
import { createDepartment, fetchDepartmentEdit, updateDepartment } from '@/services/department'

const { shallowMount, createComponentMocks } = global
const id = global.randomString()

describe('Test vue FormViewDepartment', () => {
  let wrapper
  const onRouterPush = jest.fn()
  const onRouterGo = jest.fn()
  const store = {
    getters: {
      userGroupAdminPurview: true,
      operationPurview: false,
      userGroup: {},
      fullUserGroup: [],
    },
  }
  beforeEach(() => {
    wrapper = shallowMount(FormViewDepartment, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: {
              id,
            },
            query: {
              userGroupId: id,
            },
          },
          $router: { push: onRouterPush, go: onRouterGo },
        },
      }),
      propsData: {
        isEdit: true,
        pageTitle: 'demo',
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods init', () => {
    fetchDepartmentEdit.mockResolvedValue()
    wrapper.vm.init(id)
    expect(fetchDepartmentEdit).toBeCalledWith(id)
  })

  it('Test methods getDepartment - fetchDepartmentEdit', async () => {
    const res = {
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
      name: '章三',
    }
    fetchDepartmentEdit.mockResolvedValue({ ...res })
    await wrapper.vm.getDepartment(id)
    expect(fetchDepartmentEdit).toBeCalledWith(id)
    expect(wrapper.vm.formValue).toStrictEqual({
      userGroupId: 'MA',
      name: '章三',
    })
  })

  it('Test methods resetForm', () => {
    // onRouterGo
    wrapper.vm.$refs.form.resetFields = jest.fn()
    wrapper.vm.resetForm('form')
    expect(onRouterGo).toBeCalled()
    // onRouterPush
    wrapper = shallowMount(FormViewDepartment, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: {
              id,
            },
            query: {
              userGroupId: id,
              route: global.randomString(),
            },
          },
          $router: { push: onRouterPush, go: onRouterPush },
        },
      }),
      propsData: {
        isEdit: false,
        pageTitle: 'demo',
      },
    })
    wrapper.vm.$refs.form.resetFields = jest.fn()
    wrapper.vm.resetForm('form')
    expect(onRouterPush).toBeCalled()
  })

  it('Test methods submit', async () => {
    updateDepartment.mockResolvedValue()
    createDepartment.mockResolvedValue()
    wrapper.vm.$refs.form.validate = jest.fn(callback => { callback(true) })
    await wrapper.vm.submit('form')
    expect(updateDepartment).toBeCalled()

    wrapper = shallowMount(FormViewDepartment, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: {
              id,
            },
            query: {
              userGroupId: id,
            },
          },
          $router: { push: onRouterPush, go: onRouterPush },
        },
      }),
      propsData: {
        isEdit: false,
        pageTitle: 'demo',
      },
    })
    wrapper.vm.$refs.form.validate = jest.fn(callback => { callback(true) })
    await wrapper.vm.submit('form')
    expect(createDepartment).toBeCalled()
  })

  it('Test methods submitCreate - createDepartment', async () => {
    createDepartment.mockResolvedValue()
    const params = {
      userGroupId: id,
      name: window.randomString(),
    }
    const message = 'success'
    await wrapper.vm.submitCreate(params, message)
    expect(createDepartment).toBeCalled()
  })

  it('Test methods submitUpdate - updateDepartment', async () => {
    updateDepartment.mockResolvedValue()
    const params = {
      userGroupId: id,
      name: window.randomString(),
    }
    const message = 'success'
    await wrapper.vm.submitUpdate(params, message)
    expect(updateDepartment).toBeCalled()
  })

  it('Test FormViewDepartment Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
