import DetailViewUserGroup from '@/views/system-manage/user-group/components/detail-view-user-group'

const { shallowMount, createComponentMocks } = global
const id = 'MA'
describe('Test vue DetailViewUserGroup', () => {
  let wrapper
  const store = {
    getters: {
      operationPurview: false,
    },
  }
  const onRouterPush = jest.fn()
  beforeEach(() => {
    wrapper = shallowMount(DetailViewUserGroup, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            params: {
              id,
            },
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
      propsData: {
        tabsEnum: [],
        userGroupDetail: {
          departmentList: [],
          crewList: [],
        },
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods renderTabLabel', () => {
    wrapper.setData({
      detail: {
        departmentTotal: 4,
        crewTotal: 5,
      },
    })
    let tab = {
      label: 'DEPARTMENT',
      name: 'department',
    }
    expect(wrapper.vm.renderTabLabel(tab)).toBe('科室列表(4)')
    tab = {
      label: 'CREW',
      name: 'crew',
    }
    expect(wrapper.vm.renderTabLabel(tab)).toBe('员工列表(5)')
  })

  it('Test methods resetFetchData', () => {
    const crewList = [
      { id: 'MA', name: '章三' },
      { id: 'MQ', name: '里斯' },
    ]
    const departmentList = [
      { id: 'MA', name: '综合信息办公司' },
    ]
    wrapper.setData({
      detail: {
        id: 'MA',
        crewList,
        departmentList,
      },
      activeTab: 'crew',
    })
    wrapper.vm.resetFetchData()
    expect(wrapper.vm.list).toStrictEqual([...crewList])
    wrapper.setData({
      detail: {
        id: 'MA',
        crewList,
        departmentList,
      },
      activeTab: 'department',
    })
    wrapper.vm.resetFetchData()
    expect(wrapper.vm.list).toStrictEqual([...departmentList])
  })

  it('Test methods getUserGroupInfo', () => {
    wrapper.vm.getUserGroupInfo()
    expect(wrapper.emitted().change).toBeTruthy()
  })

  it('Test methods addDepartment', () => {
    wrapper.vm.addDepartment()
    expect(onRouterPush).toBeCalledWith({
      name: 'DepartmentAdd',
      query: {
        route: `/user-groups`,
        userGroupId: id,
      },
    })
  })

  it('Test methods addCrew', () => {
    wrapper.vm.addCrew()
    expect(onRouterPush).toBeCalledWith({
      name: 'CrewAdd',
      query: {
        route: `/user-groups`,
        userGroupId: id,
      },
    })
  })

  it('Test DetailViewUserGroup Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
