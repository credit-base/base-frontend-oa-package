import ListViewDepartment from '@/views/system-manage/user-group/components/list-view-department'

const { shallowMount, createComponentMocks } = global

describe('Test vue ListViewDepartment', () => {
  let wrapper
  const onRouterPush = jest.fn()
  const store = {
    getters: {
      userGroupAdminPurview: false,
      operationPurview: false,
    },
  }
  beforeEach(() => {
    wrapper = shallowMount(ListViewDepartment, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $router: {
            push: onRouterPush,
          },
        },
      }),
      propsData: {
        loading: false,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleCommand', async () => {
    const id = global.randomString()
    await wrapper.vm.handleCommand('VIEW', id)
    expect(wrapper.vm.$router.push).toBeCalled()
    await wrapper.vm.handleCommand('EDIT', id)
    expect(wrapper.vm.$router.push).toBeCalled()
  })

  it('Test ListViewDepartment Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
