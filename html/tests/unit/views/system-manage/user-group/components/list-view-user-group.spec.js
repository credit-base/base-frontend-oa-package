import ListViewUserGroup from '@/views/system-manage/user-group/components/list-view-user-group'

const { shallowMount, createComponentMocks } = global

describe('Test vue ListViewUserGroup', () => {
  let wrapper
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(ListViewUserGroup, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $router: {
            push: onRouterPush,
          },
        },
      }),
      propsData: {
        loading: false,
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleCommand', async () => {
    const id = global.randomString()
    await wrapper.vm.handleCommand('VIEW', id)
    expect(wrapper.vm.$router.push).toBeCalled()
  })

  it('Test methods sortChange', () => {
    const val = {
      order: 'updateTime',
    }
    wrapper.vm.sortChange(val)
    expect(wrapper.emitted().sort).toBeTruthy()
  })
  it('Test ListViewUserGroup Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
