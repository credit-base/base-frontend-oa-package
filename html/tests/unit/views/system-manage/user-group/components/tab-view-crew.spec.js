jest.mock('@/utils/element')
jest.mock('@/services/crew')
import TabViewCrew from '@/views/system-manage/user-group/components/tab-view-crew'
import { updateCrewStatus } from '@/services/crew'
import { openConfirm } from '@/utils/element'

const { shallowMount, createComponentMocks } = global
const id = window.randomString()
const route = window.randomString()

describe('Test vue TabViewCrew', () => {
  let wrapper
  const store = {
    getters: {
      userInfo: {
        id: id,
      },
    },
  }
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(TabViewCrew, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $router: {
            push: onRouterPush,
          },
        },
      }),
      propsData: {
        route: route,
        data: [],
      },
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods isOwn', async () => {
    expect(wrapper.vm.isOwn(id)).toBeTruthy()
  })

  it(`methods - handleCommand`, async () => {
    openConfirm.mockResolvedValue()
    updateCrewStatus.mockResolvedValue()

    await wrapper.vm.handleCommand('VIEW', { id })
    expect(wrapper.vm.$router.push).toBeCalledWith({ path: `/crew/detail/${id}`, query: { route } })

    await wrapper.vm.handleCommand('EDIT', { id })
    expect(wrapper.vm.$router.push).toBeCalledWith({ path: `/crew/edit/${id}`, query: { route } })

    await wrapper.vm.handleCommand('ENABLE', { id })
    expect(updateCrewStatus).toBeCalledWith(id, 'enable')

    await wrapper.vm.handleCommand('DISABLE', { id })
    expect(updateCrewStatus).toBeCalledWith(id, 'disable')
  })

  it('Test methods changeStatus - openConfirm updateCrewStatus', async () => {
    openConfirm.mockResolvedValue()
    updateCrewStatus.mockResolvedValue()

    await wrapper.vm.changeStatus('ENABLE', id)
    expect(updateCrewStatus).toBeCalled()
  })

  it('Test ListViewCrew Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
