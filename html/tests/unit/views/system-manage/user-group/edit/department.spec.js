import DepartmentEdit from '@/views/system-manage/user-group/edit/department'

const { shallowMount, createComponentMocks } = global

describe('Test vue DepartmentEdit', () => {
  let wrapper
  const route = '/'

  beforeEach(() => {
    wrapper = shallowMount(DepartmentEdit, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {
              route,
            },
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test DepartmentEdit Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
