jest.mock('@/services/user-group')
jest.mock('@/services/department')
import UserGroupList from '@/views/system-manage/user-group/list'
import { fetchUserGroupList, fetchUserGroupDetail } from '@/services/user-group'
import { fetchDepartmentList } from '@/services/department'

const { shallowMount, createComponentMocks } = global

describe('Test vue UserGroupList', () => {
  let wrapper
  const store = {
    getters: {
      fullUserGroup: [],
      userGroup: {},
      superAdminPurview: false,
      platformAdminPurview: false,
      userGroupAdminPurview: false,
      operationPurview: false,
    },
  }
  const onRouterPush = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(UserGroupList, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $store: store,
          $route: {
            query: {},
          },
          $router: {
            push: onRouterPush,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods addDepartment', () => {
    wrapper.vm.addDepartment()
    expect(onRouterPush).toBeCalledWith({
      name: 'DepartmentAdd',
      query: {
        route: `/user-groups?activeTab=department`,
      },
    })
  })

  it('Test methods fetchData - fetchUserGroupList, fetchDepartmentList', async () => {
    fetchUserGroupList.mockResolvedValue()
    fetchDepartmentList.mockResolvedValue()
    await wrapper.vm.fetchData(true)
    expect(fetchUserGroupList).toBeCalled()
    await wrapper.vm.fetchData(false)
    expect(fetchDepartmentList).toBeCalled()
  })

  it('Test methods fetchUserGroupData - fetchUserGroupDetail, fetchDepartmentList', async () => {
    fetchUserGroupDetail.mockResolvedValue()
    fetchDepartmentList.mockResolvedValue()
    await wrapper.vm.fetchUserGroupData(true)
    expect(fetchUserGroupDetail).toBeCalled()
    await wrapper.vm.fetchUserGroupData(false)
    expect(fetchDepartmentList).toBeCalled()
  })

  it('Test UserGroupList Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
