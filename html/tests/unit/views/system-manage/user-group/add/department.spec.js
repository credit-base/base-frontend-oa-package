import DepartmentAdd from '@/views/system-manage/user-group/add/department'

const { shallowMount, createComponentMocks } = global

describe('Test vue DepartmentAdd', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(DepartmentAdd, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            query: {},
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test DepartmentAdd Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
