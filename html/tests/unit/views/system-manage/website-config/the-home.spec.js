jest.mock('@/utils/element')
import TheHome from '@/views/system-manage/website-config/the-home'
import { openConfirm } from '@/utils/element'
import {
  STATUS_MEMORIAL,
  STATUS_DISABLE,
  ACTION_TYPE,
  UPLOAD_TIPS_OPTIONS,
  // WEBSITE_PAGE_MAP,
  // WEBSITE_PAGE_CATEGORY,
} from '@/views/system-manage/website-config/helpers/constants'

const { shallowMount, createComponentMocks } = global

describe('Test vue component TheHome', () => {
  let wrapper
  const onMessage = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(TheHome, {
      ...createComponentMocks({
        mocks: {
          $message: onMessage,
          $bus: {
            $emit: jest.fn(),
            $on: jest.fn(),
          },
        },
        propsData: {
          activeConfig: {},
          originConfig: {},
        },
        provide: {
          reactive: {
            isPreview: false,
            memorialStatus: STATUS_MEMORIAL.no,
            udpatedKeys: [],
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods onWebsiteConfigClick', async () => {
    await wrapper.vm.onWebsiteConfigClick()
    expect(wrapper.vm.activeRelatedLinkIdx).toBeNull()
  })

  it('Test methods showDialog', async () => {
    wrapper.vm.$refs.dialog.show = jest.fn()
    await wrapper.vm.showDialog({ title: '专题设置', data: {} })
    expect(wrapper.vm.dialogFormTitle).toBe('专题设置')
    expect(wrapper.vm.dialogFormData).toStrictEqual({})
  })

  it('Test methods onConfigItemAdd', async () => {
    wrapper.vm.$refs.dialog.show = jest.fn()
    wrapper.vm.showDialog = jest.fn()
    let type = 'FOOTER_BANNER_SETTING'
    // 轮播专题设置
    await wrapper.vm.onConfigItemAdd(type)
    expect(wrapper.vm.configItem.key).toBe(type)
    expect(wrapper.vm.configItem.action).toBe(ACTION_TYPE.add)
    expect(wrapper.vm.showDialog).toBeCalledWith({ title: '轮播专题设置[新增]' })

    wrapper.setData({
      organizationGroup: [
        {
          status: STATUS_DISABLE,
          url: '/',
          image: {
            name: '',
            identify: 'https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/home/link1.jpg',
          },
        },
        {
          status: STATUS_DISABLE,
          url: '/',
          image: {
            name: '',
            identify: 'https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/home/link1.jpg',
          },
        },
        {
          status: STATUS_DISABLE,
          url: '/',
          image: {
            name: '',
            identify: 'https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/home/link1.jpg',
          },
        },
        {
          status: STATUS_DISABLE,
          url: '/',
          image: {
            name: '',
            identify: 'https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/home/link1.jpg',
          },
        },
        {
          status: STATUS_DISABLE,
          url: '/',
          image: {
            name: '',
            identify: 'https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/home/link1.jpg',
          },
        },
        {
          status: STATUS_DISABLE,
          url: '/',
          image: {
            name: '',
            identify: 'https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/home/link1.jpg',
          },
        },
        {
          status: STATUS_DISABLE,
          url: '/',
          image: {
            name: '',
            identify: 'https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/home/link1.jpg',
          },
        },
      ],
    })
    type = 'ORGANIZATION_GROUP_SETTING'
    await wrapper.vm.onConfigItemAdd(type)
  })

  it('Test methods onConfigItemEdit', async () => {
    wrapper.vm.$refs.dialog.show = jest.fn()
    wrapper.vm.showDialog = jest.fn()
    const key = 'SPECIAL_COLUMN_SETTING'
    const index = 0
    const list = []
    // 专题专栏设置
    await wrapper.vm.onConfigItemEdit(list, index, key)
    expect(wrapper.vm.configItem.key).toBe(key)
    expect(wrapper.vm.configItem.index).toBe(index)
    expect(wrapper.vm.uploadTip).toBe(UPLOAD_TIPS_OPTIONS[key])
    expect(wrapper.vm.configItem.action).toBe(ACTION_TYPE.edit)
    expect(wrapper.vm.showDialog).toBeCalledWith({ title: '专题专栏设置[编辑]' })
  })

  it('Test methods onSpecialSettingEdit', async () => {
    wrapper.vm.$refs.dialog.show = jest.fn()
    wrapper.vm.showDialog = jest.fn()
    const key = 'LOGO_SETTING'
    const list = []
    // Logo设置
    await wrapper.vm.onSpecialSettingEdit(list, key)
    expect(wrapper.vm.configItem.key).toBe(key)
    expect(wrapper.vm.configItem.action).toBe(ACTION_TYPE.edit)
    expect(wrapper.vm.showDialog).toBeCalledWith({ title: 'Logo设置[编辑]', data: list })
  })

  // it('Test methods onDialogConfirm', async () => {
  //   // wrapper.vm.$refs.form.validate = jest.fn()
  //   console.log('============')
  //   console.log(wrapper.vm.$refs.form)
  //   wrapper.vm.$refs.form.validate = jest.fn(callback => { callback(true) })
  //   wrapper.setData({
  //     configItem: {
  //       key: 'HEADER_BAR_RIGHT_SETTING',
  //       action: ACTION_TYPE.edit,
  //       index: 0,
  //     },
  //   })
  //   wrapper.vm.$refs.dialog.hide = jest.fn()
  //   await wrapper.vm.onDialogConfirm()
  //   expect(wrapper.vm.$refs.dialog.hide).toBeCalled()
  // })

  it('Test methods onConfigItemRevert', async () => {
    expect(wrapper.vm.onConfigItemRevert()).toBeFalsy()
  })

  it('Test methods onConfigItemDelete', async () => {
    openConfirm.mockResolvedValue()
    const data = [
      {
        name: '简体/繁体',
        status: 'MQ',
        url: '',
        type: 2,
      },
      {
        name: '无障碍阅读',
        status: 'MQ',
        url: '',
        type: 3,
      },
      {
        name: '登录',
        status: 'MQ',
        url: '',
        type: 4,
      },
      {
        name: '注册',
        status: 'MQ',
        url: '',
        type: 5,
      },
      {
        name: '用户中心',
        status: 'MQ',
        url: '/members',
        type: 6,
      },
      {
        name: '网站声明',
        status: 'MQ',
        url: 'navigations/index',
        type: 0,
      },
    ]
    await wrapper.vm.onConfigItemDelete(0, data)
    expect(data).toStrictEqual([
      {
        name: '无障碍阅读',
        status: 'MQ',
        url: '',
        type: 3,
      },
      {
        name: '登录',
        status: 'MQ',
        url: '',
        type: 4,
      },
      {
        name: '注册',
        status: 'MQ',
        url: '',
        type: 5,
      },
      {
        name: '用户中心',
        status: 'MQ',
        url: '/members',
        type: 6,
      },
      {
        name: '网站声明',
        status: 'MQ',
        url: 'navigations/index',
        type: 0,
      },
    ])
  })

  it('Test TheHome Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
