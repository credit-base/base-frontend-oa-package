jest.mock('@/services/website-config')
import ConfigOperationItem from '@/views/system-manage/website-config/components/config-operation-item'

const { shallowMount, createComponentMocks } = global

describe('Test vue component ConfigOperationItem', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ConfigOperationItem, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          deleteable: false,
          tag: 'span',
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods handleClick', async () => {
    await wrapper.vm.handleClick()
    // wrapper.vm.$emit(`click`)
    expect(wrapper.emitted().click).toBeTruthy()
  })

  it('Test methods handleDelete', async () => {
    await wrapper.vm.handleDelete()
    // wrapper.vm.$emit(`delete`)
    expect(wrapper.emitted().delete).toBeTruthy()
  })

  it('Test ConfigOperationItem Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
