jest.mock('@/services/website-config')
import FormNavSetting from '@/views/system-manage/website-config/components/form-nav-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormNavSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormNavSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormNavSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
