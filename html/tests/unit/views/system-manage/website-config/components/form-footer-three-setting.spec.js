jest.mock('@/services/website-config')
import FormFooterThreeSetting from '@/views/system-manage/website-config/components/form-footer-three-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormFooterThreeSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormFooterThreeSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormFooterThreeSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
