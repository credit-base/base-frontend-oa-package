jest.mock('@/services/website-config')
import FormRelatedLinksSetting from '@/views/system-manage/website-config/components/form-related-links-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormRelatedLinksSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormRelatedLinksSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormRelatedLinksSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
