jest.mock('@/services/website-config')
import FormOrganizationGroup from '@/views/system-manage/website-config/components/form-organization-group'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormOrganizationGroup', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormOrganizationGroup, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormOrganizationGroup Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
