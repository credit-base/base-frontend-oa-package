jest.mock('@/services/website-config')
import FormThemeSetting from '@/views/system-manage/website-config/components/form-theme-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormThemeSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormThemeSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormThemeSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
