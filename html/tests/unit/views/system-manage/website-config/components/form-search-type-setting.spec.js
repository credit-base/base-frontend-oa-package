jest.mock('@/services/website-config')
import FormSearchTypeSetting from '@/views/system-manage/website-config/components/form-search-type-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormSearchTypeSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormSearchTypeSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormSearchTypeSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
