jest.mock('@/services/website-config')
import FormSpecialColumnSetting from '@/views/system-manage/website-config/components/form-special-column-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormSpecialColumnSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormSpecialColumnSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormSpecialColumnSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
