jest.mock('@/services/website-config')
import FormSilhouetteSetting from '@/views/system-manage/website-config/components/form-silhouette-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormSilhouetteSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormSilhouetteSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormSilhouetteSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
