jest.mock('@/services/website-config')
import FormFooterTwoSetting from '@/views/system-manage/website-config/components/form-footer-two-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormFooterTwoSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormFooterTwoSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormFooterTwoSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
