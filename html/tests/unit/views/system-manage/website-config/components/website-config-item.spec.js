jest.mock('@/services/website-config')
import WebsiteConfig from '@/views/system-manage/website-config/components/website-config-item'
import {
  STATUS_MEMORIAL,
  // WEBSITE_PAGE_MAP,
  // WEBSITE_PAGE_CATEGORY,
} from '@/views/system-manage/website-config/helpers/constants'

const { shallowMount, createComponentMocks } = global

describe('Test vue component WebsiteConfig', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(WebsiteConfig, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
        provide: {
          reactive: {
            isPreview: true,
            memorialStatus: STATUS_MEMORIAL.no,
            udpatedKeys: [],
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods onBtnClick', async () => {
    expect(wrapper.vm.onBtnClick()).toBeFalsy()
    wrapper = shallowMount(WebsiteConfig, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          addable: true,
          editable: false,
          isEdit: false,
        },
        provide: {
          reactive: {
            isPreview: false,
            memorialStatus: STATUS_MEMORIAL.no,
            udpatedKeys: [],
          },
        },
      }),
    })
    await wrapper.vm.onBtnClick()
    expect(wrapper.emitted().add).toBeTruthy()

    wrapper = shallowMount(WebsiteConfig, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          addable: false,
          editable: true,
        },
        provide: {
          reactive: {
            isPreview: false,
            memorialStatus: STATUS_MEMORIAL.no,
            udpatedKeys: [],
          },
        },
      }),
    })
    await wrapper.vm.onBtnClick()
    expect(wrapper.emitted().edit).toBeTruthy()
  })

  it('Test methods onBtnRevert', async () => {
    expect(wrapper.vm.onBtnRevert()).toBeFalsy()
    wrapper = shallowMount(WebsiteConfig, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
        provide: {
          reactive: {
            isPreview: false,
            memorialStatus: STATUS_MEMORIAL.no,
            udpatedKeys: [],
          },
        },
      }),
    })
    await wrapper.vm.onBtnRevert()
    expect(wrapper.emitted().revert).toBeTruthy()
  })

  it('Test WebsiteConfig Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
