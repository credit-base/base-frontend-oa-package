jest.mock('@/services/website-config')
import AsideToolbox from '@/views/system-manage/website-config/components/aside-toolbox'
import { STATUS_MEMORIAL } from '@/views/system-manage/website-config/helpers/constants'
import { getWebsiteConfigVersionList } from '@/services/website-config'

const { shallowMount, createComponentMocks } = global

describe('Test vue component AsideToolbox', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(AsideToolbox, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          category: 'theHome',
          activeVersionId: '',
        },
        provide: {
          reactive: {
            isPreview: false,
            memorialStatus: STATUS_MEMORIAL.no,
            udpatedKeys: [],
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods fetchVersionList', async () => {
    getWebsiteConfigVersionList.mockResolvedValue({ list: [] })
    await wrapper.vm.fetchVersionList()
    expect(wrapper.vm.versionList).toStrictEqual([])
  })

  it('Test methods onPreviewCommand', async () => {
    await wrapper.vm.onPreviewCommand(`STATUS_ENABLE`)
    expect(wrapper.emitted()[`set-preview`]).toBeTruthy()
  })

  it('Test methods onMemorialStatusChange', async () => {
    const status = STATUS_MEMORIAL.yes
    await wrapper.vm.onMemorialStatusChange(status)
    wrapper.vm.$emit(`set-memorial-status`, status)
    expect(wrapper.emitted()[`set-memorial-status`]).toBeTruthy()
  })

  it('Test methods onVersionCommand', async () => {
    const command = 'MS0'
    await wrapper.vm.onVersionCommand(command)
    wrapper.vm.$emit(`update:active-version-id`, command)
    expect(wrapper.emitted()[`update:active-version-id`]).toBeTruthy()
  })

  it('Test AsideToolbox Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
