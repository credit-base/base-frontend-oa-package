jest.mock('@/services/website-config')
import DialogFormItem from '@/views/system-manage/website-config/components/dialog-form-item'

const { shallowMount, createComponentMocks } = global

describe('Test vue component DialogFormItem', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(DialogFormItem, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          deleteable: false,
          tag: 'span',
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods confirm', async () => {
    await wrapper.vm.confirm()
    // wrapper.vm.$emit(`confirm`)
    expect(wrapper.emitted().confirm).toBeTruthy()
  })

  it('Test methods show', async () => {
    await wrapper.vm.show()
    expect(wrapper.vm.isVisible).toBeTruthy()
  })

  it('Test methods hide', async () => {
    await wrapper.vm.hide()
    expect(wrapper.vm.isVisible).toBeFalsy()
  })

  it('Test DialogFormItem Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
