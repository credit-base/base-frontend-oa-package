jest.mock('@/services/website-config')
import FormToolBarSetting from '@/views/system-manage/website-config/components/form-tool-bar-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormToolBarSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormToolBarSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormToolBarSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
