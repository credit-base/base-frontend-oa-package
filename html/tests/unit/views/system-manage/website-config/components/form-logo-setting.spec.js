jest.mock('@/services/website-config')
import FormLogoSetting from '@/views/system-manage/website-config/components/form-logo-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormLogoSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormLogoSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormLogoSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
