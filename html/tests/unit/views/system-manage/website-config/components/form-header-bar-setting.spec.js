jest.mock('@/services/website-config')
import FormHeaderBarSetting from '@/views/system-manage/website-config/components/form-header-bar-setting'

const { shallowMount, createComponentMocks } = global

describe('Test vue component FormHeaderBarSetting', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(FormHeaderBarSetting, {
      ...createComponentMocks({
        mocks: {
        },
        propsData: {
          data: {},
          isEdit: false,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods validate', async () => {
    wrapper.vm.$refs.form.validate = jest.fn()
    await wrapper.vm.validate()
    expect(wrapper.vm.validate).toBeTruthy()
  })

  it('Test FormHeaderBarSetting Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
