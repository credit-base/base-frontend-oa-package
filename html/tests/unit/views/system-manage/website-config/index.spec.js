jest.mock('@/utils/element')
jest.mock('@/services/website-config')
import WebsiteConfig from '@/views/system-manage/website-config'
import { openConfirm } from '@/utils/element'
import { getWebsiteConfigOfHome, getWebsiteConfigVersionDetail } from '@/services/website-config'
import { STATUS_MEMORIAL } from '@/views/system-manage/website-config/helpers/constants'

const { shallowMount, createComponentMocks } = global

describe('Test vue component WebsiteConfig', () => {
  let wrapper
  const onMessage = jest.fn()
  const onRouterPush = jest.fn()
  const TheHomeStub = {
    render: () => {},
    methods: {
      getConfig: () => ({}),
    },
  }

  beforeEach(() => {
    wrapper = shallowMount(WebsiteConfig, {
      ...createComponentMocks({
        mocks: {
          $message: onMessage,
          $bus: {
            $emit: jest.fn(),
            $on: jest.fn(),
          },
          $route: { fullPath: `` },
          $router: { push: onRouterPush },
        },
        provide: {
          reactive: {
            isPreview: false,
            memorialStatus: STATUS_MEMORIAL.no,
            udpatedKeys: [],
          },
        },
        stubs: {
          TheHome: TheHomeStub,
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test methods save', async () => {
    openConfirm.mockResolvedValue()
    await wrapper.vm.save()
    // expect(onMessage).toBeCalledWith({ type: 'success', message: '保存成功' })
  })

  it('Test methods publish', async () => {
    openConfirm.mockResolvedValue()
    await wrapper.vm.publish()
    // expect(onMessage).toBeCalledWith({ type: 'success', message: '发布成功' })
  })

  it('Test methods onSetPreview', async () => {
    await wrapper.vm.onSetPreview(true)
    expect(wrapper.vm.reactive.isPreview).toBeTruthy()
  })

  it('Test methods fetchWebsiteConfigByCategory', async () => {
    getWebsiteConfigOfHome.mockResolvedValue({ id: 'MA', content: {} })
    await wrapper.vm.fetchWebsiteConfigByCategory()
    expect(wrapper.vm.originVersionId).toBe('MA')
    expect(wrapper.vm.activeVersionId).toBe('MA')
    expect(wrapper.vm.originWebsiteConfig).toStrictEqual({})
  })

  it('Test methods fetchWebsiteConfigById', async () => {
    getWebsiteConfigVersionDetail.mockResolvedValue({ content: {} })
    wrapper.setData({
      activeVersionId: 'MA',
      originVersionId: 'MQ',
    })

    await wrapper.vm.fetchWebsiteConfigById()
    expect(wrapper.vm.activeWebsiteConfig).toStrictEqual({})
    // expect(wrapper.vm.getUpdatedKeys).toBeCalled()
  })

  it('Test methods onMemorialStatusChange', async () => {
    await wrapper.vm.onMemorialStatusChange(true)
    expect(wrapper.vm.reactive.memorialStatus).toBeTruthy()
  })

  it('Test WebsiteConfig Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
