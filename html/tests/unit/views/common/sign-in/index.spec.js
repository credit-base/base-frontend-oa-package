jest.mock('@/services/utils')
jest.mock('@/utils/encrypt')
import SignIn from '@/views/common/sign-in/index.vue'
import { getHash } from '@/services/utils'
import { encrypt } from '@/utils/encrypt'

describe('Test vue SignIn', () => {
  const store = {
    getters: {
      theme: 'theme-blue',
    },
    dispatch: () => ({ jwt: global.randomString() }),
  }
  const onRouterPush = jest.fn()
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(SignIn, global.createComponentMocks({
      router: true,
      mocks: {
        $route: { query: {} },
        $router: {
          push: onRouterPush,
        },
        $store: store,
      },
    }))
  })

  afterEach(() => {
    jest.resetAllMocks()
    wrapper && wrapper.destroy()
  })

  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it(`Methods handleLogin success`, async () => {
    const BtnLogin = wrapper.find('.login-btn')

    wrapper.vm.$refs.form.validate = jest.fn(callback => { callback(true) })
    getHash.mockResolvedValue({ hash: global.randomString() })
    encrypt.mockReturnValue(global.randomString())
    jest.useFakeTimers()

    await BtnLogin.trigger('click')
    jest.runTimersToTime(1000)

    // expect(onRouterPush).toBeCalledWith('/')
  })

  // it(`Methods handleLogin fail`, async () => {
  //   const BtnLogin = wrapper.find('.login-btn')

  //   wrapper.vm.$refs.form.validate = jest.fn(callback => { callback(true) })
  //   encrypt.mockRejectedValue(new Error(''))

  //   BtnLogin.trigger('click')
  // })

  it('Test SignIn Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
