import ViewRedirect from '@/views/common/view-redirect'

const { shallowMount, createComponentMocks } = global

describe('Test vue ViewRedirect', () => {
  let wrapper
  const onRouterReplace = jest.fn()

  beforeEach(() => {
    wrapper = shallowMount(ViewRedirect, {
      ...createComponentMocks({
        router: true,
        mocks: {
          $route: {
            params: {},
            query: {},
          },
          $router: {
            replace: onRouterReplace,
          },
        },
      }),
    })
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })
  it('Test shallowMount', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('Test ViewRedirect Snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
