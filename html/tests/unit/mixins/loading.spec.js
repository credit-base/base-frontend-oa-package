
import Loading from '@/mixins/loading'

describe(`Mixins - loading`, () => {
  const Component = {
    render () {},
    mixins: [Loading],
  }
  let wrapper

  beforeEach(() => {
    wrapper = global.shallowMount(Component)
  })

  afterEach(() => {
    wrapper && wrapper.destroy()
  })

  it(`Init value is false`, () => {
    expect(wrapper.vm.$_isLoading).toBeFalsy()
  })

  it(`Return true when show`, () => {
    wrapper.vm.$_showLoading()
    expect(wrapper.vm.$_isLoading).toBeTruthy()
  })

  it(`Return false when hide`, () => {
    wrapper.vm.$_showLoading()
    wrapper.vm.$_hideLoading()
    expect(wrapper.vm.$_isLoading).toBeFalsy()
  })
})
