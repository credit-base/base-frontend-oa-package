/**
 * Jest setupFile
 * @module tests/unit/setupFile
 */

import Vue from 'vue'
import {
  mount,
  shallowMount,
  createLocalVue,
} from '@vue/test-utils'
// import { CONSTANTS } from '@/plugins/constants'

// Utils
import cryptoRandomString from 'crypto-random-string'

// ===
// 全局导入
// ===
import i18n from '@/i18n'
import '@/mixins'
import '@/filters'
import '@/components'
import '@/plugins/constants'
import '@/plugins/element'
import '@/plugins/echarts'
// import { PERMISSION_MAP } from '@/constants/permissions'

// ===
// 配置 Vue
// ===

// Don't warn about not using the production build of Vue, as
// we care more about the quality of errors than performance
// for tests.
Vue.config.productionTip = false

// ===
// Global helpers
// ===

// https://vue-test-utils.vuejs.org/api/#mount
global.mount = mount

// https://vue-test-utils.vuejs.org/api/#shallowmount
global.shallowMount = shallowMount

// A helper for creating Vue component mocks
global.createComponentMocks = ({
  router,
  mocks = {},
  stubs = {},
  ...options
} = {}) => {
  // Use a local version of Vue, to avoid polluting the global
  // Vue and thereby affecting other tests.
  // https://vue-test-utils.vuejs.org/api/#createlocalvue
  const localVue = createLocalVue()

  // localVue.prototype.$CONSTANTS = this.$CONSTANTS

  const returnOptions = {
    localVue,
    // https://github.com/Illyism/vue-i18n-jest-example/blob/master/src/tests/vue.js
    i18n,
    ...options,
  }

  // https://vue-test-utils.vuejs.org/api/options.html#stubs
  returnOptions.stubs = { ...stubs }

  // https://vue-test-utils.vuejs.org/api/options.html#mocks
  returnOptions.mocks = {
    // $permissions: PERMISSION_MAP, // Mock permissions map
    // $CONSTANTS: CONSTANTS,
    ...mocks,
  }

  localVue.directive('permission', {
    inserted () {},
    update () {},
  })

  // If using `router: true`, we'll automatically stub out
  // components from Vue Router.
  if (router) {
    returnOptions.stubs['router-link'] = true
    returnOptions.stubs['router-view'] = true
  }

  return returnOptions
}

// Utils bind to global
global.randomString = ({
  length = 10,
  ...options
} = {}) => cryptoRandomString({ length, ...options })

// Log error when catch unhandledRejection
// process.on('unhandledRejection', err => {
//   // console.log(err)
// })
