/**
 * @file ESLint 配置
 */

module.exports = {
  root: true,

  env: {
    node: true,
  },

  extends: ['@qxy/vue'],

  rules: {
    'vue/script-indent': 'off',
  },

  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/__mock__/*.{j,y}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],

      env: {
        jest: true,
      },

      globals: {
        mount: false,
        shallowMount: false,
        createComponentMocks: false,
        createModuleStore: false,
      },

      rules: {
        'import/first': 'off',
        'node/no-callback-literal': 'off',
        'max-lines-per-function': ['error', { max: 1000 }],
      },
    },
  ],
}
