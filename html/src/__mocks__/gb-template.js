
/**
 * 列表
 */
export const list = [
  {
    id: 'MA',
    name: '登记信息',
    identify: 'FR_DJXX',
    subjectCategory: [
      {
        id: 'MA',
        name: '自然人',
      },
      {
        id: 'MQ',
        name: '个体工商户',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    infoClassify: {
      id: 'MA',
      name: '行政处罚',
    },
    version: '1-D012A100C101',
    updateTime: 1576317765,
  },
  {
    id: 'MQ',
    name: '变更登记信息',
    identify: 'FR_DJBGXX',
    subjectCategory: [
      {
        id: 'MA',
        name: '自然人',
      },
      {
        id: 'MQ',
        name: '个体工商户',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    infoClassify: {
      id: 'MA',
      name: '行政处罚',
    },
    version: '2-D012A100C101',
    updateTime: 1576317765,
  },
]
