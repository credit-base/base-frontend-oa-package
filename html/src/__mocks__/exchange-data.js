/**
 * @file 交换数据
 * @module mocks/exchange-data
 */

export const dataList = [
  {
    id: 'MA',
    subjectName: '海星啤酒集团有限公司',
    subjectIdentify: '912132324374799324',
    storageTime: 1576317765,
  },
  {
    id: 'MQ',
    subjectName: '广州姿绿贸易有限公司',
    subjectIdentify: '123123214324234354365',
    storageTime: 1576127365,
  },
]

export const taskList = [
  {
    supplier: '省信息中心',
    successCount: 232,
    createTime: 1576127365,
  },
  {
    supplier: 'XX市税务局',
    successCount: 96,
    createTime: 1576317765,
  },
]

export const exportTaskList = [
  {
    username: '张三',
    startTime: 1576127365,
    endTime: 1576317765,
    dataCount: 394,
    createTime: 1576127365,
  },
  {
    username: '李四',
    startTime: 1572124365,
    endTime: 1578313765,
    dataCount: 74,
    createTime: 1571827365,
  },
]
