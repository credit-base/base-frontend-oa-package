
/**
 * 列表
 */
export const list = [
  {
    id: 'MA',
    gbTemplate: 'Lw',
    name: '行政许可',
    identify: 'XZXK',
    subjectCategory: [
      { id: 'MA', name: '法⼈及⾮法⼈组织' },
    ],
    dimension: { id: 'MA', name: '政务共享' },
    exchangeFrequency: { id: 'MA', name: '每一年' },
    infoClassify: { id: 'MA', name: '行政处罚' },
    infoCategory: { id: 'MA', name: '基础信息' },
    description: '目录描述目录描述目录描述目录描述目录描述目录描述目录描述目录描述目录描述目录描述目录描述目录描述目录描述',
    template: [
      {
        name: '信息项名称4',
        identify: 'HMD',
        type: '字符型',
        length: 200,
        options: ['存续', '吊销'],
        dimension: { id: 'MA', name: '政务共享' },
        isNecessary: { id: 'MA', name: '是' },
        isMasked: { id: 'Lw', name: '否' },
        maskRule: [1, 2],
        description: '模板描述',
      },
      {
        name: '信息项名称5',
        identify: 'DJIN',
        type: '字符型',
        length: 200,
        options: ['存续', '吊销'],
        dimension: { id: 'MA', name: '政务共享' },
        isNecessary: { id: 'MA', name: '是' },
        isMasked: { id: 'Lw', name: '否' },
        maskRule: [1, 2],
        description: '模板描述',
      },
    ],
    updateTime: 1576317765,
    version: '2-D012A100C101',
  },
  {
    id: 'MQ',
    gbTemplate: 'Lw',
    name: '行政处罚',
    identify: 'XZCF',
    subjectCategory: [
      { id: 'MR', name: '自然人' },
      { id: 'MQ', name: '个体工商户' },
    ],
    dimension: { id: 'MQ', name: '授权查询' },
    exchangeFrequency: { id: 'MA', name: '每一年' },
    infoClassify: { id: 'MA', name: '行政处罚' },
    infoCategory: { id: 'MA', name: '基础信息' },
    description: '目录描述目录描述目录描述目录描述目录描述',
    template: [
      {
        name: '信息项名称1',
        identify: 'HMD',
        type: '字符型',
        length: 200,
        options: ['存续', '吊销'],
        dimension: { id: 'MA', name: '政务共享' },
        isNecessary: { id: 'MA', name: '是' },
        isMasked: { id: 'Lw', name: '否' },
        maskRule: [1, 2],
        description: '模板描述',
      },
      {
        name: '信息项名称2',
        identify: '数据标识',
        type: '字符型',
        length: 200,
        options: ['存续', '吊销'],
        dimension: { id: 'MA', name: '政务共享' },
        isNecessary: { id: 'MA', name: '是' },
        isMasked: { id: 'Lw', name: '否' },
        maskRule: [1, 2],
        description: '模板描述模板描述模板描述模板描述模板描述',
      },
      {
        name: '信息项名称3',
        identify: 'TYSHXYDM',
        type: '字符型',
        length: 100,
        options: ['存续', '吊销'],
        dimension: { id: 'MA', name: '政务共享' },
        isNecessary: { id: 'MA', name: '否' },
        isMasked: { id: 'MQ', name: '是' },
        maskRule: [2, 3],
        description: '模板描述模板描述模板描述模板描述模板描述模板描述模板描述模板描述模板描述模板描述模板描述',
      },
      {
        name: '信息项名称777',
        identify: 'TYSHXYDM',
        type: '字符型',
        length: 100,
        options: ['存续'],
        dimension: { id: 'MA', name: '政务共享' },
        isNecessary: { id: 'MA', name: '否' },
        isMasked: { id: 'MQ', name: '是' },
        maskRule: [2, 3],
        description: '模板描述模板描述模板描述模板描述模板描述模板描述模板描述模板描述模板描述模板描述模板描述',
      },
    ],
    updateTime: 1576317765,
    version: '2-D012A100C101',
  },
]

/**
 * 本级目录初始化配置
 */
export const initConfigs = {
  subjectCategory: [
    { id: 'MA', name: '法⼈及⾮法⼈组织' },
    { id: 'MQ', name: '⾃然⼈' },
    { id: 'Mw', name: '个体⼯商户' },
  ],

  dimension: [
    { id: 'MA', name: '社会公开' },
    { id: 'MQ', name: '政务共享' },
    { id: 'Mw', name: '授权查询' },
  ],

  exchangeFrequency: [
    { id: 'MA', name: '实时' },
    { id: 'MQ', name: '每日' },
    { id: 'Mw', name: '每周' },
    { id: 'Mg', name: '每月' },
    { id: 'NA', name: '每季度' },
    { id: 'LC', name: '每半年' },
    { id: 'OK', name: '每一年' },
    { id: 'TS', name: '每两年' },
    { id: 'OS', name: '每三年' },
  ],

  infoClassify: [
    { id: 'MA', name: '⾏政许可' },
    { id: 'MQ', name: '⾏政处罚' },
    { id: 'Mw', name: '红名单' },
    { id: 'Mg', name: '黑名单' },
    { id: 'NA', name: '其他' },
  ],

  infoCategory: [
    { id: 'MA', name: '基础信息' },
    { id: 'MQ', name: '守信信息' },
    { id: 'Mw', name: '失信信息' },
    { id: 'Mg', name: '其他信息' },
  ],
}
