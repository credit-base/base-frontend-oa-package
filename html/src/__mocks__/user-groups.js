export const USER_GROUP_DETAIL_DATA = {
  id: 'MA',
  name: '发展和改革委员会',
  shortName: '发改委',
  crewList: {
    total: 3,
    list: [
      {
        id: 'MS0',
        cellphone: '18800000002',
        realName: '开发人员',
        category: {
          id: 'Mg',
          name: '委办局管理员',
        },
        status: {
          type: 'success',
          name: '启用',
          id: 'Lw',
        },
        updateTime: 1583980685,
        updateTimeFormat: '2021年04月26日 11点15分',
      },
      {
        id: 'MTQ',
        cellphone: '14599888888',
        realName: '乔文',
        category: {
          id: 'Mg',
          name: '委办局管理员',
        },
        status: {
          type: 'success',
          name: '启用',
          id: 'Lw',
        },
        updateTime: 1583980685,
        updateTimeFormat: '2021年04月26日 11点15分',
      },
      {
        id: 'MA',
        cellphone: '18800000000',
        realName: '张科',
        category: {
          id: 'MA',
          name: '超级管理员',
        },
        status: {
          type: 'warning',
          name: '禁用',
          id: 'LC0',
        },
        updateTime: 1583980685,
        updateTimeFormat: '2021年04月26日 11点15分',
      },
    ],
  },
  departmentList: {
    total: 7,
    list: [
      {
        id: 'Niw',
        name: '技术科',
        updateTime: 1597629740,
        updateTimeFormat: '2021年04月26日 11点15分',
      },
      {
        id: 'NS8',
        name: '市场部总部',
        updateTime: 1597629740,
        updateTimeFormat: '2021年04月26日 11点15分',
      },
      {
        id: 'NCs',
        name: '综合办公室',
        updateTime: 1597629740,
        updateTimeFormat: '2021年04月26日 11点15分',
      },
      {
        id: 'My4',
        name: '保卫科',
        updateTime: 1597629740,
        updateTimeFormat: '2021年04月26日 11点15分',
      },
      {
        id: 'MDA',
        name: '财金科',
        updateTime: 1597629740,
        updateTimeFormat: '2021年04月26日 11点15分',
      },
      {
        id: 'Nw',
        name: '管理科办公室',
        updateTime: 1597629740,
        updateTimeFormat: '2021年04月26日 11点15分',
      },
      {
        id: 'MA',
        name: '主任办公室',
        updateTime: 1597629740,
        updateTimeFormat: '2021年04月26日 11点15分',
      },
    ],
  },
}
