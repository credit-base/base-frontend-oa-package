/**
 * @file 国标规则列表
 * @module mocks/gb-rule
 */

import { getUnixTimeStamp } from '@/utils/date-time'
import { RULE_STATUS_VALUES } from '@/constants/modules/rule-manage'

const ruleDetail = {
  transformItems: [
    {
      from: '企业名称',
      to: '机构名称',
      catalog: 'A级纳税人',
    },
    {
      from: '企业认定部门',
      to: '认定部门',
      catalog: 'A级纳税人',
    },
  ],

  completeItems: [
    {
      from: '企业名称',
      to: '部门名称',
      catalog: 'A级纳税人',
    },
    {
      from: '企业名称',
      to: '机构名称',
      catalog: '地方性红名单',
    },
    {
      from: '统一社会信用代码',
      to: '机构代码',
      catalog: '企业法人信息',
    },
  ],

  compareItems: [
    {
      from: '企业名称',
      to: '机构名称',
      catalog: 'A级纳税人',
    },
    {
      from: '统一社会信用代码',
      to: '机构代码',
      catalog: '地方性红名单',
    },
  ],

  deduplicateItems: [
    { id: 'MA', name: '红名单名称' },
    { id: 'MQ', name: '企业认定部门' },
    { id: 'Mw', name: '统一社会信用代码' },
    { id: 'Mg', name: '身份证号' },
    { id: 'Mg', name: '自然人名称' },
  ],

  deduplicateWay: { id: 'MA', name: '丢弃' },
}

export const ruleList = [
  {
    id: 'MA',
    gbTemplate: '失信企业名录',
    wbjTemplate: '行政许可',
    sourceUnit: '发改委',
    userGroupCount: 123,
    ruleCount: 42,
    transformCount: 8329323,
    status: RULE_STATUS_VALUES.approved,
    updateTime: getUnixTimeStamp(),
    ...ruleDetail,
  },
  {
    id: 'MQ',
    gbTemplate: '自然人信息黑名单',
    wbjTemplate: '股权结构信息',
    sourceUnit: '发改委',
    userGroupCount: 723,
    ruleCount: 234,
    transformCount: 545543553,
    status: RULE_STATUS_VALUES.approved,
    updateTime: getUnixTimeStamp(),
    ...ruleDetail,
  },
  {
    id: 'Mw',
    gbTemplate: '守信自然人信息',
    wbjTemplate: '行政处罚',
    sourceUnit: '发改委',
    userGroupCount: 634,
    ruleCount: 565,
    transformCount: 2132435,
    status: RULE_STATUS_VALUES.approved,
    updateTime: getUnixTimeStamp(),
    ...ruleDetail,
  },
]

export const reviewList = [
  {
    id: 'Mg',
    gbTemplate: '企业法人信息',
    wbjTemplate: '行政许可',
    sourceUnit: '信用办',
    userGroupCount: 123,
    transformCount: 8329323,
    status: RULE_STATUS_VALUES.pending,
    updateTime: getUnixTimeStamp(),
    ...ruleDetail,
  },
  {
    id: 'Mc',
    gbTemplate: '自然人信息黑名单',
    wbjTemplate: '行政处罚',
    sourceUnit: '发改委',
    userGroupCount: 546456,
    transformCount: 5435435,
    status: RULE_STATUS_VALUES.rejected,
    rejectReason: '资源目录信息项公开范围不匹配',
    updateTime: getUnixTimeStamp(),
    ...ruleDetail,
  },
  {
    id: 'Nc',
    gbTemplate: '守信自然人信息',
    wbjTemplate: '行政处罚',
    sourceUnit: '信用办',
    userGroupCount: 312,
    transformCount: 342423,
    status: RULE_STATUS_VALUES.rejected,
    rejectReason: '资源目录信息项统一社会信用代码为必填项',
    updateTime: getUnixTimeStamp(),
    ...ruleDetail,
  },
]
