/**
 * 中文 - 应用
 * @module zh/app
 */

import { DASHBOARD_TITLE } from '@/settings'

export default {
  DASHBOARD_TITLE,

  // 主题
  THEME_GOLD: '香槟金',
  THEME_BLUE: '宝石蓝',
  THEME_RED: '石榴红',

  DASHBOARD: '首页',
  PERSONAL_INFORMATION: '个人中心',
  SIGN_OUT: '退出',
  UPDATE_PASSWORD: '更新密码',
}
