export default {
  ROUTER: {
    DASHBOARD: '首页',
  },
  TABS: {},
  STATISTICS_MODULE: {
    CREDIT_RANK: '信用排名',
    RESOURCE_CATALOG_NUMBER: '资源目录数据总量',
    DATA_COMPARISON: '数据对比',
    REPORT_COVERAGE: '报送覆盖率',
    LHJC_NUMBER: '联合奖惩数据总量',
    USER_GROUP_NUMBER: '委办局数据总量',
    KEY_MONITORING: '重点监测',
    CREDIT_WARNING: '行业信用预警趋势分析',
    NATIONAL_CREDIT_RANKING: '全国信用排名',
    PROVINCIAL_CREDIT_RANKING: '省内信用排名',
  },
}
