import common from '../common-message'

const {
  COMMON_SU_ADD,
  COMMON_SU_EDIT,
  COMMON_PLACEHOLDER_SELECT,
} = common

export default {
  DEPARTMENT_ADD_SUCCESS: COMMON_SU_ADD('科室'),
  DEPARTMENT_EDIT_SUCCESS: COMMON_SU_EDIT('科室'),
  LABEL: {
    USER_GROUP: '所属委办局',
    DEPARTMENT_NAME: '科室名称',
  },
  PLACEHOLDER: {
    USER_GROUP: COMMON_PLACEHOLDER_SELECT('所属委办局'),
  },
  ACTION: {
    ADD_DEPARTMENT: '新增科室',
  },
}
