export default {
  // action
  ADD_DEPARTMENT: '新增科室',
  ADD_CREW: '新增员工',
  PERMISSIONS_SCOPE: '员工范围',
  USER_GROUP_BASE_INFO: '基本信息',
  NAME: '委办局名称',
  SHORT_NAME: '委办局简称',
  DEPARTMENT_LIST: '科室列表',
  CREW_LIST: '员工列表',
  DEPARTMENT_COUNT: '委办局下属科室数量',
  CREW_COUNT: '委办局下属员工数量',
}
