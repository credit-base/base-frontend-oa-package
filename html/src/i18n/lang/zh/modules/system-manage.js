import common from '../common-message'

const {
  ER_FORMAT,
  ER_REQUIRED_EMPTY,
  COMMON_PLACEHOLDER_INPUT,
  COMMON_PLACEHOLDER_SELECT,
} = common

export default {
  // 系统设置
  ROUTER: {
    PERSONAL_INFORMATION: '个人中心',
    SYSTEM_MANAGE: '系统管理',
    USER_GROUP_LIST: '委办局管理',

    CREW_ADD: '新增员工',
    CREW_EDIT: '编辑员工',
    CREW_LIST: '员工管理',
    CREW_DETAIL: '员工详情',

    DEPARTMENT_LIST: '科室管理',
    DEPARTMENT_DETAIL: '科室详情',
    DEPARTMENT_ADD: '新增科室',
    DEPARTMENT_EDIT: '编辑科室',

    USER_GROUP_DETAIL: '委办局详情',
    USER_GROUP_EDIT: '编辑委办局员工',

    ADMIN_CREW_LIST: '平台管理员管理',
    USER_GROUP_CREW_LIST: '委办局员工管理',
    GENERAL_CREW_LIST: '普通员工管理',

    WEBSITE_CONFIG: `门户配置`,
  },
  FIELD: {
    USER_GROUP_EMPTY: ER_REQUIRED_EMPTY('所属委办局'),
    // 科室
    DEPARTMENT_EMPTY: ER_REQUIRED_EMPTY('科室名称'),
    DEPARTMENT_FORMAT: ER_FORMAT('科室名称', '2-15位汉字'),
    DEPARTMENT_REG: ER_FORMAT('科室名称', '2-15位汉字'),
    FIRST_LEVEL: '一级模块',
    SECONDARY_LEVEL: '二级模块',
    THREE_LEVEL: '三级模块',
    FOUR_LEVEL: '操作',
  },
  LABEL: {
    // 科室
    USER_GROUP: '委办局',
    DEPARTMENT_NAME: '科室名称',
    SELECT_USER_GROUP: COMMON_PLACEHOLDER_SELECT('委办局'),
    SELECT_DEPARTMENT: COMMON_PLACEHOLDER_SELECT('科室'),
  },
  PLACEHOLDER: {
    // 员工
    USER_GROUP: '所属委办局',
    CELLPHONE: COMMON_PLACEHOLDER_INPUT('手机号'),
    PASSWORD: COMMON_PLACEHOLDER_INPUT('密码'),
    CAPTCHA: COMMON_PLACEHOLDER_INPUT('验证码'),
    CREW_NAME: COMMON_PLACEHOLDER_INPUT('员工姓名'),
    ADMIN_CREW_NAME: COMMON_PLACEHOLDER_INPUT('平台管理员名称'),
    SELECT_USER_GROUP: COMMON_PLACEHOLDER_SELECT('委办局'),
    SELECT_DEPARTMENT: COMMON_PLACEHOLDER_SELECT('科室'),
    // 委办局
    USER_GROUP_CREW_NAME: COMMON_PLACEHOLDER_INPUT('委办局管理员名称'),
    USER_GROUP_NAME: COMMON_PLACEHOLDER_INPUT('委办局名称'),
    // 科室
    DEPARTMENT_NAME: COMMON_PLACEHOLDER_INPUT('科室名称'),
    SELECT_CREW_CATEGORY: COMMON_PLACEHOLDER_SELECT('员工类型'),
  },
  LOGS: {
    MESSAGE: '信息',
    REAL_NAME: '员工',
    IP_ADDRESS: 'IP地址',
    USER_GROUP: '委办局',
    OPERATION: '操作',
    CATEGORY: '记录对象',
    DATE: '时间',
  },
}
