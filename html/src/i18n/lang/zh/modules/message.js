/**
 * 中文 - 弹出窗口消息
 *
 * @module zh/message
 */

export default {
  NO_JWT_FIELD: '缺少 JWT',

  CANCEL_SUCCESS: '取消成功',
  ADD_SUCCESS: '新增成功',
  EDIT_SUCCESS: '编辑成功',
  ENABLE_SUCCESS: '启用成功',
  DISABLE_SUCCESS: '禁用成功',
  SIGN_IN_SUCCESS: '登录成功',
  SIGN_OUT_SUCCESS: '退出成功',
  FEEDBACK_SUCCESS: '反馈成功',
  UPLOAD_SUCCESS: '上传成功',
  ER_HASH: '请刷新页面重新获取HASH',
}
