/**
 * 中文 - 字段名
 *
 * @module zh/field
 */
import settings from '@/settings'

const {
  APP_TITLE,
  CREDIT,
  REGION_SHORT_NAME,
} = settings

export default {
  STATUS: '状态',
  REVIEW_STATUS: '审核状态',
  OPERATION: '操作',
  REVOKE: '撤销',
  YEAR: '年版',
  INDEX: '编号',
  CURRENT_RELEASE: '当前发布',
  UPDATE_TIME: '最后更新时间',
  CREATE_TIME: '创建时间',
  START_TIME: '开始时间',
  END_TIME: '终结时间',
  VERSION: '当前版本',
  VERSION_DESCRIPTION: '版本描述',
  ASCENDING: '正序',
  DESCENDING: '倒序',
  DISPLAY: '展开全部',
  SHUT_DOWN: '隐藏全部',
  COMPARE_VERSION: '版本比较',
  REVERT_VERSION: '还原版本',
  ADD_ITEM: '新增信息项',
  EDIT_ITEM: '编辑信息项',
  DELETE_ITEM: '删除信息项',
  DOWNLOAD_TEMPLATE: '下载模版',
  ORIGIN_COMPARE_VERSION: '当前版本',
  TARGET_COMPARE_VERSION: '对比版本',
  REVERSE_ORDER: '倒序',
  // 资源目录版本比较，资源目录项差异：新增、编辑、删除
  ADD: '新增',
  EDIT: '编辑',
  ADD_TITLE: '新增',
  EDIT_TITLE: '编辑',
  DELETE: '删除',
  TIP: '提示',
  EXPORT_DATA: '导出{title}数据',
  UNIT_PERSON: '位',
  UNIT_PIECES: '个',
  DELETE_TITLE: '删除信息项',
  DELETE_CONFIRM_TEXT: '您确定删除该信息项吗？',

  SIDEBAR_TITLE: APP_TITLE,
  LOGIN_SUB_TITLE: `${APP_TITLE}管理系统`,
  LOGIN_TITLE: `${CREDIT}${REGION_SHORT_NAME}`,
  UPDATE_CAPTCHA: '点击图片更换验证码',
  INPUT_REASON: '请输入原因',
  LABEL_NAME: '名称',
  LABEL_NUMBER: '数量',

  USER_GROUP: {
    NAME: '委办局名称',
    SHORT_NAME: '委办局简称',
  },

  DEPARTMENT: {
    NAME: '科室名称',
  },

  CREW: {
    CELLPHONE: '手机号',
    CARD_ID: '身份证号',
    CATEGORY: '员工类型',
    REAL_NAME: '姓名',
    STATUS: '账号状态',
    USER_GROUP: '所属委办局',
    DEPARTMENT: '所属科室',
    BASE_INFO_TITLE: '基本信息',
    PURVIEW_TITLE: '权限范围',
    OPERATION_LOG: '操作记录',
    REGISTER_ACCOUNT_TITLE: '账号信息',
    CREW_ADD_SUCCESS: '员工新增成功',
    CREW_EDIT_SUCCESS: '员工编辑成功',
    LABEL_REAL_NAME: '姓名',
    LABEL_CELLPHONE: '手机号',
    LABEL_CARD_ID: '身份证号',
    LABEL_USER_GROUP: '所属委办局',
    LABEL_CATEGORY: '员工类型',
    LABEL_DEPARTMENT: '所属科室',
    LABEL_PASSWORD: '密码',
    LABEL_CONFIRM_PASSWORD: '确认密码',

    // TODO: refactor
    TIPS_CELLPHONE: '注：当前手机号会作为登录手机号使用',
    PLACEHOLDER_USER_NAME: '请输入2-15位中文',
    PLACEHOLDER_REAL_NAME: '请输入姓名',
    PLACEHOLDER_CELLPHONE: '请输入11位手机号',
    PLACEHOLDER_CARD_ID: '请输入15或18位居民二代身份证号码',
    PLACEHOLDER_USER_GROUP: '请选择所属委办局',
    PLACEHOLDER_DEPARTMENT: '请选择科室',
    PLACEHOLDER_PASSWORD: '请输入8-20密码，密码需包含大写字母+小写字母+数字+特殊字符',
    PLACEHOLDER_CONFIRM_PASSWORD: '请输入确认密码',
  },
}
