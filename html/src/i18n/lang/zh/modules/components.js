import common from '../common-message'

const {
  ER_FORMAT_UPLOAD,
  SU_UPLOAD,
} = common

export default {
  TEMPLATE_FILE_FORMAT: ER_FORMAT_UPLOAD(`文件`, `Excel格式`),
  TEMPLATE_FILE_NAME_FORMAT: ER_FORMAT_UPLOAD(`文件名称`, `“TEMPLATE_00_1”格式`),
  TEMPLATE_FILE_SIZE_FORMAT: `文件大小不得超过10M`,
  TEMPLATE_UPLOAD_SUCCESS: SU_UPLOAD(),
}
