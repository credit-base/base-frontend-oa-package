import common from '../common-message'

const {
  COMMON_PLACEHOLDER_INPUT,
} = common

export default {
  SIGN_IN: {
    PLACEHOLDER: {
      CELLPHONE: COMMON_PLACEHOLDER_INPUT('账号'),
      PASSWORD: COMMON_PLACEHOLDER_INPUT('密码'),
      CAPTCHA: COMMON_PLACEHOLDER_INPUT('验证码'),
    },
  },
}
