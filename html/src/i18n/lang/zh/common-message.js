/**
 * @file 错误提示
 * @module zh/common-message
 * @see https://code.aliyun.com/credit-base/base-doc/blob/master/credit-common/%E9%80%9A%E7%94%A8%E6%96%87%E6%A1%A3/%E9%94%99%E8%AF%AF%E6%8F%90%E7%A4%BA.md
 */

// ErCommon1 内容为空XX不能为空
export const ER_CONTENT_EMPTY = name => `${name}不能为空`
// ErCommon2 长度不符合要求xx长度应为xx-xx字符
export const ER_LENGTH = (name, { min = 0, max } = {}) => `${name}格式不正确，应为${min}-${max}字符，请重新输入`
// ErCommon3 必填项为空尚未填写xx，请继续完善信息
export const ER_REQUIRED_EMPTY = name => `${name}不能为空，请继续完善信息`
// ErCommon4 格式不符合规范xx应为xx（汉字/数字/汉字+数字）格式，请重新输入
export const ER_FORMAT = (name, type) => `${name}格式不正确，应为${type}，请重新输入`
// ErCommon4 格式不符合规范xx应为xx（汉字/数字/汉字+数字）格式，请重新输入
export const ER_FORMAT_UPLOAD = (name, type) => `${name}格式不正确，应为${type}，请重新上传`
// ErCommon5 身份证位数或格式不对身份证号为15或18位数字或大写字母，请核对后输入
export const ER_ID_FORMAT = () => `身份证号为15或18位数字或大写字母，请核对后输入`
// ErCommon6 手机号位数或格式请输入正确的11位手机号
export const ER_PHONE_NUMBER = () => `请输入正确的11位手机号`
// ErCommon7 编辑/设置密码时密码格式或长度不符合规范密码应为8-20位大写字母+小写字母+数字+特殊字符的组合
export const ER_PASSWORD_FORMAT = () => `密码应为8-20位大写字母+小写字母+数字+特殊字符的组合`
// ER-Common101 由于网络等其他原因未成功提交失败，请稍后再试
export const ER_NETWORK_REASONS = (name = '提交') => `${name}失败，请稍后再试`
// ER-Common102 新增/编辑操作成功已成功提交
export const SU_SUBMIT = () => `已成功提交`
export const SU_UPLOAD = () => `已成功上传`
// ER-UserAccount5更换密码时两次密码不一致两次输入密码不一致，请重新输入
export const ER_COMMON_CONFIRM_PASSWORD_FORMAT = () => `密码与确认密码不一致，请重新输入`
export const ER_IDENTICAL_DENIED = (origin, target) => `${origin}与${target}不一致，请重新输入`
// 唯一项重复
export const ER_DUPLICATE_UNIQUE_ITEM = name => `${name}内容重复，请重新输入`
export const ER_TYPE_DATA_WRONG = (name, type = '输入') => `${name}不正确，请重新${type}` // type 输入/选择

export const MESSAGE_OPTIONS_STATUS = (option, title) => `${option}该${title}后，该${title}将进入审核程序，只有通过审核后${title}的相关状态才会编辑。您确定${option}该${title}吗？`
export const MESSAGE_OPTIONS_EDIT = (option, title) => `${option}该${title}后，该${title}将进入审核程序。您确定${option}该${title}吗？`
export const MESSAGE_OPTIONS_CANCEL = (option, title) => `${option}该${title}后，该${title}相关状态将直接编辑。您确定${option}该${title}吗？`
export const MESSAGE_APPROVE = (title) => `通过审核后，该${title}将离开审核程序。您确定该${title}的审核通过吗？`
export const MESSAGE_PUBLICITY = () => `公示该数据后，数据会在门户网显示，您确定公示该条数据吗？`
export const MESSAGE_RE_PUBLICITY = () => `取消公示该数据后，数据不会在门户网显示，您确定取消公示该条数据吗？`

export const COMMON_PLACEHOLDER_INPUT = name => `请输入${name}`
export const COMMON_PLACEHOLDER_SELECT = name => `请选择${name}`
export const COMMON_PLACEHOLDER_UPLOAD = name => `请上传${name}`

export const BUTTON_COMMON_ADD = name => `新增${name}`
export const BUTTON_COMMON_RESET = name => `取消${name}`
export const BUTTON_COMMON_SUBMIT = name => `确定${name}`
export const BUTTON_COMMON_EDIT = name => `编辑${name}`
export const BUTTON_COMMON_SEARCH = name => `搜索${name}`
export const INPUT_COMMON_SELECT = name => `请选择${name}`

export const COMMON_TITLE = name => `${name}详情`
export const COMMON_BASE_INFO_TITLE = name => `${name}基本信息`
export const COMMON_PURVIEW_TITLE = name => `${name}员工范围`
export const COMMON_UPDATE_TIME = () => `最后更新时间`
export const COMMON_SU_ADD = name => `${name}添加成功`
export const COMMON_SU_EDIT = name => `${name}编辑成功`

export default {
  ER_CONTENT_EMPTY,
  ER_LENGTH,
  ER_REQUIRED_EMPTY,
  ER_FORMAT,
  ER_FORMAT_UPLOAD,
  ER_ID_FORMAT,
  ER_PHONE_NUMBER,
  ER_PASSWORD_FORMAT,
  ER_COMMON_CONFIRM_PASSWORD_FORMAT,
  ER_NETWORK_REASONS,
  SU_SUBMIT,
  SU_UPLOAD,
  BUTTON_COMMON_ADD,
  BUTTON_COMMON_EDIT,
  BUTTON_COMMON_SEARCH,
  COMMON_TITLE,
  COMMON_BASE_INFO_TITLE,
  COMMON_PURVIEW_TITLE,
  COMMON_UPDATE_TIME,
  BUTTON_COMMON_RESET,
  BUTTON_COMMON_SUBMIT,
  INPUT_COMMON_SELECT,
  COMMON_SU_ADD,
  COMMON_SU_EDIT,
  COMMON_PLACEHOLDER_INPUT,
  COMMON_PLACEHOLDER_SELECT,
}
