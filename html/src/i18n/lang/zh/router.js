/**
 * 中文 - 路由
 *
 * @module zh/router
 */

import permission from './modules/router/permission'
import resourceCatalog from '@/app-modules/resource-catalog/lang'
// @import

export default {
  DASHBOARD: '首页',
  SIGN_OUT: '退出',
  UPDATE_PASSWORD: '更新密码',

  // 系统设置
  ...permission,
  ...resourceCatalog,
  // @route
  // 双公示数据质量监测系统

  // 规则管理
  RULE_MANAGE_SUBSYSTEM: '规则管理子系统',
  RULE_MANAGE_GB_LIST: '国标目录规则管理',
  RULE_MANAGE_GB_DETAIL: '国标目录规则详情',
}
