/**
 * i18n - 中文
 *
 * @module i18n/zh
 */

import app from './modules/app'
import common from './modules/common'
import tip from './modules/tip'
import field from './modules/field'
import action from './modules/action'
import message from './modules/message'
import userGroup from './modules/user-group'
import department from './modules/department'
import systemManage from './modules/system-manage'
import dashboard from './modules/dashboard'
import components from './modules/components'
import websiteConfig from './modules/website-config'

// @import
import resourceCatalog from '@/app-modules/resource-catalog/lang'
import ruleManage from '@/app-modules/rule-manage/lang'
import dataManage from '@/app-modules/data-manage/lang'
import exchangeSharing from '@/app-modules/exchange-sharing/lang'
import news from '@/app-modules/news/lang'
import members from '@/app-modules/members/lang'
import journals from '@/app-modules/journals/lang'
import creditPhotography from '@/app-modules/credit-photography/lang'
import creditInformationInquiryApplication from '@/app-modules/credit-information-inquiry-application/lang'
import business from '@/app-modules/business/lang'

export default {
  app,
  common,
  tip,
  field,
  action,
  message,
  userGroup,
  department,
  dashboard,
  components,
  websiteConfig,
  systemManage,
  resourceCatalog,
  ruleManage,
  dataManage,
  exchangeSharing,
  news,
  members,
  journals,
  creditPhotography,
  creditInformationInquiryApplication,
  business,
}
