/**
 * @file i18n
 * @module src/i18n
 */

import Vue from 'vue'
import VueI18n from 'vue-i18n'
import * as Storage from '@/utils/storage'
import elementZhLocale from 'element-ui/lib/locale/lang/zh-CN'
import zhLocale from '@/i18n/lang/zh'

Vue.use(VueI18n)

const DEFAULT_I18N_LOCALE = 'zh'
const {
  VUE_APP_I18N_LOCALE = DEFAULT_I18N_LOCALE,
  VUE_APP_I18N_FALLBACK_LOCALE = DEFAULT_I18N_LOCALE,
} = process.env
const messages = {
  zh: {
    ...zhLocale,
    ...elementZhLocale,
  },
}
const i18n = new VueI18n({
  locale: Storage.getLanguage() || VUE_APP_I18N_LOCALE,
  fallbackLocale: VUE_APP_I18N_FALLBACK_LOCALE,
  messages,
  silentTranslationWarn: process.env.NODE_ENV === 'test',
})

export { i18n as default }
