/**
 * @file 注册全局过滤器
 * @module filters/index
 */

import Vue from 'vue'
import * as filters from '@/filters/filters'

Object.keys(filters).forEach(k => Vue.filter(k, filters[k]))
