/**
 * @file 规则管理过滤器
 * @module filters/rule-manage
 */

import { makeMap } from '@/utils'
import {
  RULE_STATUS_OPTIONS,
} from '@/constants/modules/rule-manage'

// 规则状态
export const formatRuleStatus = status => makeMap(RULE_STATUS_OPTIONS).get(status)
