/**
 * @file 全局过滤器
 * @module filters/filters
 */

import { makeMap } from '@/utils'
import {
  YES_OR_NO_OPTIONS,
} from '@/constants/response'

// 格式化布尔值
export const formatBoolean = bool => makeMap(YES_OR_NO_OPTIONS).get(bool)

// 格式化数字
export { formatNumber } from '@/utils'

// 格式化时间
export { formatTime } from '@/utils/date-time'

// 资源目录
export * from '@/filters/resource-catalog'

// 规则管理
export * from '@/filters/rule-manage'
