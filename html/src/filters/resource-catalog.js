/**
 * @file 资源目录过滤器
 * @module filters/resource-catalog
 */

import { makeMap } from '@/utils'
import {
  DATA_TYPE_OPTIONS,
  SUBJECT_CATEGORY_OPTIONS,
} from '@/constants/modules/resource-catalog'

// 格式化资源目录数据项类型
export const formatDataType = type => makeMap(DATA_TYPE_OPTIONS).get(type)

// 格式化主体类别
export const formatSubjectCategory = subjectCategory => makeMap(SUBJECT_CATEGORY_OPTIONS).get(subjectCategory)
