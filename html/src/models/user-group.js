/**
 * 模型 - 委办局
 *
 * @module models/UserGroup
 */

import Base from '@/models/base'

class UserGroup extends Base {
  constructor (params = {}) {
    super(params)

    const {
      id,
      name,
      shortName,
      crewList = { total: 0, list: [] },
      departmentList = { total: 0, list: [] },
    } = params

    this.id = id
    this.name = name
    this.shortName = shortName
    this.crewList = crewList
    this.departmentList = departmentList
  }
}

export default UserGroup
