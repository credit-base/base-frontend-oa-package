/**
 * 基类
 * @module models/Base
 */

class Base {
  get (keys = []) {
    const result = {}

    if (!Array.isArray(keys) || !keys.length) return result

    for (let idx = 0; idx < keys.length; idx++) {
      const key = keys[idx]

      if (typeof keys[idx] === 'string') {
        result[key] = this[key]
      }
    }

    return result
  }
}

export default Base
