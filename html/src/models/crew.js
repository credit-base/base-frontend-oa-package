import Base from '@/models/base'

class Crew extends Base {
  constructor (params = {}) {
    super(params)

    const {
      id,
      cellphone,
      realName,
      updateTime,
      updateTimeFormat,
      createTime,
      userGroup = {},
      category = {},
      cardId,
      department = {},
      status = {},
      purview = [],
    } = params

    this.id = id
    this.cellphone = cellphone
    this.realName = realName
    this.cardId = cardId
    this.updateTime = updateTime
    this.updateTimeFormat = updateTimeFormat
    this.createTime = createTime
    this.userGroup = userGroup.name
    this.category = category.name
    this.department = department.name
    this.status = status
    this.purview = purview.map(prop => parseInt(prop))
  }
}

export default Crew
