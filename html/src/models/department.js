/**
 * 模型 - 科室
 *
 * @module models/Department
 */

import Base from '@/models/base'

class Department extends Base {
  constructor (params = {}) {
    super(params)

    const {
      id,
      name,
      updateTime,
      updateTimeFormat,
      userGroup = {},
    } = params

    this.id = id
    this.name = name
    this.updateTime = updateTime
    this.updateTimeFormat = updateTimeFormat
    this.userGroup = userGroup.name
  }
}

export default Department
