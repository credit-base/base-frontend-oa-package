/**
 * @file Global Getters
 * @module store/getters
 */

import { THEMES_CONFIG } from '@/constants/theme'
import { CONSTANTS } from '@/plugins/constants'

const { PURVIEW = {} } = CONSTANTS

export default {
  // app
  language: state => state.app.language,
  theme: state => state.app.theme,
  device: state => state.app.device,
  sidebar: state => state.app.sidebar,
  fullUserGroup: state => state.app.fullUserGroup,
  isFullScreen: state => state.app.isFullScreen,
  isNetworkEnable: state => state.app.isNetworkEnable,
  isPurviewEnable: state => state.app.isPurviewEnable,
  primaryColor: state => THEMES_CONFIG.find(theme => theme.class === state.app.theme).primaryColor,

  // user
  purview: state => state.user.purview,
  userInfo: state => state.user.userInfo,
  userGroup: state => state.user.userGroup,
  department: state => state.user.department,
  category: state => state.user.category,
  crewId: state => state.user.crewId,
  realName: state => state.user.realName,

  // purview
  superAdminPurview: state => state.user.category.id === PURVIEW.SUPER_ADMIN,
  platformAdminPurview: state => state.user.category.id === PURVIEW.PLATFORM_ADMIN,
  userGroupAdminPurview: state => state.user.category.id === PURVIEW.USER_GROUP_ADMIN,
  operationPurview: state => state.user.category.id === PURVIEW.OPERATION_CREW,

  // router
  routes: state => state.permission.routers,

  // view
  visitedViews: state => state.view.visitedViews,
  cachedViews: state => state.view.cachedViews,

  // Dashboard
  categoryStat: state => state.dashboard.categoryStat,
  regionId: state => state.dashboard.regionId,
  regionName: state => state.dashboard.regionName,
  mapNameList: state => state.dashboard.mapNameList,
  currentMapName: state => state.dashboard.mapNameList.slice(-1)[0],

  exchangeSharingRules: state => state.exchangeSharingRules.templateData,

  // oss
  action: state => state.oss.action,
}
