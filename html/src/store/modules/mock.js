/**
 * @file Store module mock
 * @module store/modules/mock
 */

import { list as gbTemplateList } from '@/__mocks__/gb-template'
import {
  list as bjTemplateList,
  initConfigs as bjTemplateInitConfigs,
} from '@/__mocks__/bj-template'
import {
  ruleList as gbRuleList,
  reviewList as gbReviewList,
} from '@/__mocks__/gb-rule'
import {
  dataList as exchangeDataList,
  taskList as exchangeTaskList,
  exportTaskList as exchangeExportTaskList,
} from '@/__mocks__/exchange-data'

export const state = {
  // 目录管理
  gbTemplateList,
  bjTemplateList,
  bjTemplateInitConfigs,

  // 规则管理
  gbRuleList,
  gbReviewList,

  // 交换共享
  exchangeDataList,
  exchangeTaskList,
  exchangeExportTaskList,
}

export const getters = {
  // 目录管理
  gbTemplateList: state => state.gbTemplateList,
  bjTemplateList: state => state.bjTemplateList,
  bjTemplateInitConfigs: state => state.bjTemplateInitConfigs,

  // 规则管理
  gbRuleList: state => state.gbRuleList,
  gbReviewList: state => state.gbReviewList,

  // 交换共享
  exchangeDataList: state => state.exchangeDataList,
  exchangeTaskList: state => state.exchangeTaskList,
  exchangeExportTaskList: state => state.exchangeExportTaskList,
}

export const actions = {}

export const mutations = {}

export default {
  state,
  getters,
  actions,
  mutations,
}
