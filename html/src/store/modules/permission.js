import { asyncRouterMap, constantRouterMap } from '@/router'
import { filterAsyncRoutes } from '@/utils/filter-permission'
import { CONSTANTS } from '@/plugins/constants'
import cloneDeep from 'lodash.clonedeep'

import { filterViewRouter } from '@/utils'

const { PURVIEW, FILTER_TYPE } = CONSTANTS

const state = {
  roles: [],
  routers: constantRouterMap,
}

const mutations = {
  SET_ROUTERS: (state, routers) => {
    state.addRouters = routers
    state.routers = constantRouterMap.concat(routers)
  },
}

const actions = {
  GenerateRoutes ({ commit, rootGetters }, data) {
    const { purview = [], category = {} } = data
    let accessedRouters = []

    // 禁用权限
    if (!rootGetters.isPurviewEnable) {
      return new Promise(resolve => {
        accessedRouters = filterViewRouter(accessedRouters.concat(asyncRouterMap), FILTER_TYPE.VISIBLE) || []

        commit('SET_ROUTERS', accessedRouters)
        resolve(accessedRouters)
      })
    }

    return new Promise(resolve => {
      if (category.id === PURVIEW.SUPER_ADMIN) {
        accessedRouters = filterViewRouter(accessedRouters.concat(asyncRouterMap), FILTER_TYPE.VISIBLE) || []
      } else if (purview.length) {
        accessedRouters = filterViewRouter(filterAsyncRoutes(asyncRouterMap, purview), FILTER_TYPE.VISIBLE) || []
      } else {
        accessedRouters = []
      }

      if (category.id !== PURVIEW.SUPER_ADMIN && category.id !== PURVIEW.PLATFORM_ADMIN) {
        const _routers = cloneDeep(accessedRouters)
        accessedRouters = filterViewRouter(_routers, FILTER_TYPE.PURVIEW)
      }

      commit('SET_ROUTERS', accessedRouters)
      resolve(accessedRouters)
    })
  },
}

export default {
  state,
  actions,
  mutations,
}
