/**
 * @file 应用状态
 * @module store/modules/app
 */

import * as Storage from '@/utils/storage'
import {
  IS_NETWORK_ENABLE,
  IS_PURVIEW_ENABLE,
} from '@/settings'
import { CONSTANTS } from '@/plugins/constants'
import UserGroup from '@/models/user-group'
import { fetchUserGroupList } from '@/services/user-group'

// 翻译所有委办局
export function translateFullUserGroup (res = {}) {
  const list = Array.isArray(res.list)
    ? res.list.map(item => new UserGroup(item)
      .get(['id', 'name']))
    : []

  return list
}

const { PAGE_SCENE } = CONSTANTS

export const state = {
  language: Storage.getLanguage() || 'zh',
  theme: Storage.getTheme() || 'theme-gold',
  isNetworkEnable: IS_NETWORK_ENABLE,
  isPurviewEnable: IS_PURVIEW_ENABLE,
  device: 'desktop',
  sidebar: {
    isOpen: true,
    withoutAnimation: false,
  },
  fullUserGroup: [],
  isFullScreen: false,
}

export const actions = {
  toggleSidebar ({ commit }) {
    commit('TOGGLE_SIDEBAR')
  },

  closeSideBar ({ commit }, { withoutAnimation = false } = {}) {
    commit('CLOSE_SIDEBAR', withoutAnimation)
  },

  toggleDevice ({ commit }, device) {
    commit('SET_DEVICE', device)
  },

  toggleTheme ({ commit }, theme) {
    Storage.setTheme(theme)
    commit('SET_THEME', theme)
  },

  toggleFullScreen ({ commit }, isFullScreen) {
    commit(`SET_FULL_SCREEN`, isFullScreen)
  },

  setLanguage ({ commit }, language) {
    Storage.setLanguage(language)
    commit('SET_LANGUAGE', language)
  },

  getFullUserGroup ({ commit }) {
    return new Promise((resolve, reject) => {
      fetchUserGroupList({ pageScene: PAGE_SCENE.ALL }).then(res => {
        const list = translateFullUserGroup(res)
        commit('SET_ALL_USER_GROUP', list)
        resolve(list)
      }).catch(reject)
    })
  },
}

export const mutations = {
  TOGGLE_SIDEBAR (state) {
    state.sidebar.isOpen = !state.sidebar.isOpen
    state.sidebar.withoutAnimation = false
  },

  CLOSE_SIDEBAR (state, withoutAnimation) {
    state.sidebar.isOpen = false
    state.sidebar.withoutAnimation = withoutAnimation
  },

  SET_DEVICE (state, device) {
    state.device = device
  },

  SET_THEME (state, theme) {
    state.theme = theme
  },

  SET_FULL_SCREEN (state, isFullScreen) {
    state.isFullScreen = isFullScreen
  },

  SET_LANGUAGE (state, language) {
    state.language = language
  },

  SET_ALL_USER_GROUP (state, userGroup) {
    state.fullUserGroup = userGroup
  },
}

export default {
  state,
  actions,
  mutations,
}
