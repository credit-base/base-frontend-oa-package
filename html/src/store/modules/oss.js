import { getUid } from '@/utils'
import { getOssToken } from '@/services/oss'

export const state = {
  action: ``,
}

export const actions = {
  async getToken ({ commit }) {
    const { token } = await getOssToken()
    commit('SET_ACTION', token)
  },
}

export const mutations = {
  SET_ACTION (state, token) {
    const uid = getUid()
    state.action = `${process.env.VUE_APP_OSS_HOST}/upload?token=${token}&X-Progress-ID=${uid}`
  },
}

export default {
  state,
  actions,
  mutations,
}
