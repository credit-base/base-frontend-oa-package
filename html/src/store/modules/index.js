/**
 * @file Store modules
 * @module store/modules/index
 */

import app from './app'
import user from './user'
import view from './view'
import mock from './mock'
import permission from './permission'
import oss from './oss'
import exchangeSharingRules from './exchange-sharing-rules'
import dashboard from './dashboard'

export default {
  app,
  user,
  view,
  mock,
  permission,
  oss,
  dashboard,
  exchangeSharingRules,
}
