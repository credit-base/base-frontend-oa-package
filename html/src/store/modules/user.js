/**
 * @file 员工状态
 * @module store/modules/user
 */

import * as Storage from '@/utils/storage'
import { CONSTANTS } from '@/plugins/constants'
import {
  signIn,
  signOut,
  getUserInfo,
} from '@/services/auth'
import { resetRouter } from '@/router'

const { PURVIEW } = CONSTANTS

export const state = {
  token: Storage.getToken() || '',
  purview: [],
  userInfo: {},
  userGroup: {},
  department: {},
  category: {},
  crewId: '',
  realName: '',
}

export const actions = {
  async signIn ({ commit }, loginInfo) {
    return new Promise((resolve, reject) => {
      signIn(loginInfo)
        .then(res => {
          Storage.setToken(res.jwt)
          commit('SET_TOKEN', res.jwt)
          resolve(res)
        })
        .catch(reject)
    })
  },

  async getUserInfo ({ commit }) {
    return new Promise((resolve, reject) => {
      getUserInfo()
        .then(res => {
          const {
            id,
            realName,
            purview = [],
            category = {},
            department = {},
            userGroup = {},
          } = res

          if (PURVIEW.SUPER_ADMIN !== category.id && (!Array.isArray(purview) || !purview.length)) {
            reject(new Error('该账号未分配权限，请联系管理员！'))
            return
          }

          const tempUserData = {
            ...res,
            purview: purview.map(prop => parseFloat(prop)),
          }

          commit('SET_PURVIEW', tempUserData.purview)
          commit('SET_USER_INFO', res)
          commit('SET_CREW_ID', id)
          commit('SET_REAL_NAME', realName)
          commit('SET_USER_GROUP', userGroup)
          commit('SET_DEPARTMENT', department)
          commit('SET_CATEGORY', category)
          resolve({
            ...tempUserData,
          })
        })
        .catch(reject)
    })
  },

  async signOut ({ commit, dispatch }) {
    return new Promise((resolve, reject) => {
      signOut()
        .then(res => {
          commit('SET_TOKEN', '')
          commit('SET_PURVIEW', [])
          commit('SET_CATEGORY', {})

          Storage.removeToken()
          resetRouter()

          // reset visited views and cached views
          // to fixed https://github.com/PanJiaChen/vue-element-admin/issues/2485
          dispatch('delAllViews', null, { root: true })

          resolve(res)
        })
        .catch(reject)
    })
  },

  resetToken ({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_PURVIEW', [])

      Storage.removeToken()
      resetRouter()
      resolve()
    })
  },
}

export const mutations = {
  SET_TOKEN (state, token) {
    state.token = token
  },

  SET_PURVIEW (state, purview) {
    state.purview = purview
  },

  SET_USER_INFO (state, userInfo) {
    state.userInfo = userInfo
  },

  SET_CREW_ID (state, crewId) {
    state.crewId = crewId
  },

  SET_REAL_NAME (state, realName) {
    state.realName = realName
  },

  SET_USER_GROUP (state, userGroup) {
    state.userGroup = userGroup
  },

  SET_DEPARTMENT (state, department) {
    state.department = department
  },

  SET_CATEGORY (state, category) {
    state.category = category
  },
}

export default {
  state,
  actions,
  mutations,
}
