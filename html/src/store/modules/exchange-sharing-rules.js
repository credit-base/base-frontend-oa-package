/**
 * @file 交换共享规则设置资源目录数据
 * @module store/modules/exchange-sharing-rules
 */

import * as Storage from '@/utils/storage'

export const state = {
  templateData: Storage.getRulesTemplateData() || [],
}

export const actions = {
  toggleRulesTemplateData ({ commit }, data) {
    Storage.setRulesTemplateData(data)
    commit('SET_RULES_TEMPLATE_DATA', data)
  },
}

export const mutations = {
  SET_RULES_TEMPLATE_DATA (state, data) {
    state.templateData = data
  },
}

export default {
  state,
  actions,
  mutations,
}
