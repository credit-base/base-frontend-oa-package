/**
 * @file 驾驶舱状态
 * @module store/modules/dashboard
 */

import axios from 'axios'
import * as Echarts from 'echarts/core'
import { purgeMapName } from '@/plugins/echarts/utils'
import echartsMapUrlList from '@/plugins/echarts/echarts-map-url'

import settings, {
  REGION_NAME,
  FALLBACK_REGION_NAME,
  INIT_MAP_NAME_LIST,
} from '@/settings'

const INIT_REGION_ID = 'Lw'

/**
  * 获取初始化驾驶舱地图名
  */
const getInitMapName = () => FALLBACK_REGION_NAME

/**
  * 获取初始化驾驶舱地图名数组
  */
const getInitMapNameList = () => INIT_MAP_NAME_LIST

/**
  * 是否存在匹配的 GeoJson 数据
  *
  * @param {string} mapName 图表名
  */
function getMatchedGeoJsonConfig (mapName) {
  const purgedMapName = purgeMapName(mapName)
  let geoJsonConfig = {}

  for (let idx = 0; idx < echartsMapUrlList.length; idx++) {
    const item = echartsMapUrlList[idx]

    if (item.name === purgedMapName || item.aliases.includes(purgedMapName)) {
      geoJsonConfig = { name: purgedMapName, path: item.path }
      break
    }
  }

  return geoJsonConfig
}

const state = {
  regionId: INIT_REGION_ID,
  settings,
  regionName: REGION_NAME,
  mapNameList: getInitMapNameList(),
  categoryStat: {
    totalRed: [],
    totalBlack: [],
    totalLicense: [],
    totalSanction: [],
  },
}

const actions = {
  loadMap ({ commit }, mapName = getInitMapName()) {
    // eslint-disable-next-line no-async-promise-executor
    const promise = new Promise(async (resolve, reject) => {
      const { name, path } = getMatchedGeoJsonConfig(mapName)

      if (!path) return reject(new Error('暂无此地图数据'))

      if (Echarts.getMap(name)) {
        commit('PUSH_MAP_NAME', mapName)
        return resolve({ name, geoJson: Echarts.getMap(name).geoJson })
      }

      try {
        const { data: geoJson } = await axios.get(`/static/maps/${path}`)

        Echarts.registerMap(name, geoJson)
        commit('PUSH_MAP_NAME', mapName)
        resolve({ name, geoJson })
      } catch (err) {
        reject(new Error('地图数据请求失败'))
      }
    })

    return promise
  },

  pushMapName ({ commit }, mapName) {
    commit('PUSH_MAP_NAME', mapName)
  },

  popMapName ({ commit }) {
    commit('POP_MAP_NAME')
  },

  jumpToMapName ({ commit }, mapName) {
    commit('JUMP_TO_MAP_NAME', mapName)
  },

  resetMapNameList ({ commit }) {
    commit('RESET_MAP_NAME_LIST')
  },
}

const mutations = {
  PUSH_MAP_NAME (state, mapName) {
    if (state.mapNameList.includes(mapName)) return
    const { name, path } = getMatchedGeoJsonConfig(mapName)
    if (!name || !path) return
    state.mapNameList.push(mapName)
  },

  POP_MAP_NAME (state) {
    state.mapNameList.pop()

    if (!state.mapNameList.length) {
      state.mapNameList = [getInitMapName()]
    }
  },

  JUMP_TO_MAP_NAME (state, mapName) {
    const idx = state.mapNameList.indexOf(mapName)
    state.mapNameList = state.mapNameList.slice(0, idx + 1)
  },

  RESET_MAP_NAME_LIST (state) {
    state.mapNameList = [getInitMapName()]
  },
}

export default {
  state,
  actions,
  mutations,
}
