/**
 * @file 图表常量
 * @module constants
 */

import { useGraphIcons } from '@/utils'

// 图谱尺寸(非像素)
export const GRAPH_CHART_SIZE = 1e3

// 图谱共5列
export const GRAPH_COLUMN_COUNT = 5

// echarts dataZoom 组件 id
export const DATA_ZOOM_ID = {
  SLIDER_Y: `DATA_ZOOM_SLIDER_Y`,
  INSIDE_Y: `DATA_ZOOM_INSIDE_Y`,
}

// 右键菜单指令
export const CONTEXT_MENU_COMMAND = {
  viewCatalogDetail: {
    key: `VIEW_CATALOG_DETAIL`,
    text: `查看目录详情`,
  },
  viewCatalogData: {
    key: `VIEW_CATALOG_DATA`,
    text: `查看目录数据`,
  },
  viewCatalogList: {
    key: `VIEW_CATALOG_LIST`,
    text: `查看目录列表`,
  },
  viewStatChart: {
    key: `VIEW_STAT_CHART`,
    text: `查看统计图表`,
  },
  createReportRule: {
    key: `CREATE_REPORT_RULE`,
    text: `创建报送规则`,
  },
  exportCatalogData: {
    key: `EXPORT_CATALOG_DATA`,
    text: `导出目录数据`,
  },
  closeUsageInterface: {
    key: `CLOSE_USAGE_INTERFACE`,
    text: `关闭应用接口`,
  },
  openUsageInterface: {
    key: `OPEN_USAGE_INTERFACE`,
    text: `开启应用接口`,
  },
  viewRuleDetail: {
    key: `VIEW_RULE_DETAIL`,
    text: `查看规则详情`,
  },
  viewRuleResult: {
    key: `VIEW_RULE_RESULT`,
    text: `查看规则效果`,
  },
  viewHistoryVersion: {
    key: `VIEW_HISTROY_VERSION`,
    text: `查看历史版本`,
  },
  enableRule: {
    key: `ENABLE_RULE`,
    text: `启用当前规则`,
  },
  disableRule: {
    key: `DISABLE_RULE`,
    text: `禁用当前规则`,
  },
  viewNodeRule: {
    key: `VIEW_NODE_RULE`,
    text: `查看节点规则`,
  },
}

// 节点类型
export const NODE_CATEGORY = {
  wbj: `wbj`, // 委办局
  wbjCatalog: `wbjCatalog`, // 委办局资源目录
  gbCatalog: `gbCatalog`, // 国标资源目录
  bjCatalog: `bjCatalog`, // 本级资源目录
  cloudDatabase: `cloudDatabase`, // 前置机资源目录
  dataUsage: `dataUsage`, // 数据应用
}

// 节点规则合理性
export const NODE_CREATE_RULE_MAP = {
  wbjCatalog: [`gbCatalog`, `bjCatalog`, `cloudDatabase`],
  gbCatalog: [`cloudDatabase`],
  bjCatalog: [`cloudDatabase`],
  cloudDatabase: [`dataUsage`],
}

// 节点前缀
export const NODE_PREFIX_MAP = new Map([
  [NODE_CATEGORY.wbj, `WBJ`],
  [NODE_CATEGORY.wbjCatalog, `WBJ_CATALOG`],
  [NODE_CATEGORY.gbCatalog, `GB_CATALOG`],
  [NODE_CATEGORY.bjCatalog, `BJ_CATALOG`],
  [NODE_CATEGORY.cloudDatabase, `CLOUD_DATABASE`],
  [NODE_CATEGORY.dataUsage, `DATA_USAGE`],
])

// 节点状态
export const NODE_STATUS = {
  success: `success`,
  warning: `warning`,
  danger: `danger`,
  disabled: `disabled`,
  add: `add`,
}

// 向量方向
export const VECTOR_DIRECTORY = {
  forward: `FORWARD`,
  backward: `BACKWARD`,
  both: `BOTH`,
}

// 图标类型
export const ICON_CATEGORY = {
  wbj: `wbj`,
  wbjCatalog: `wbjCatalog`,
  gbCatalog: `gbCatalog`,
  bjCatalog: `bjCatalog`,
  qzjCatalog: `qzjCatalog`,
  usage: `usage`,
}

// 节点图标
export const GRAPH_ICONS = {
  ...useGraphIcons(ICON_CATEGORY.wbj),
  ...useGraphIcons(ICON_CATEGORY.wbjCatalog),
  ...useGraphIcons(ICON_CATEGORY.gbCatalog),
  ...useGraphIcons(ICON_CATEGORY.bjCatalog),
  ...useGraphIcons(ICON_CATEGORY.qzjCatalog),
  ...useGraphIcons(ICON_CATEGORY.usage),
}

// 图表颜色
export const GRAPH_COLORS = {
  primary: `#0085ff`,
  warning: `#ff0000`,
  label: `#ccc`,
  disabled: `#667390`,
}

// 图表系列类型
export const CHART_SERIES_TYPE = {
  graph: `graph`,
  lines: `lines`,
}

// 图表数据类型
export const CHART_DATA_TYPE = {
  node: `node`,
}

// 数据类型
export const DATA_TYPE_ENUM = {
  TYPE_RULE_DETAIL: `TYPE_RULE_DETAIL`,
  TYPE_RULE_LIST: `TYPE_RULE_LIST`,
  TYPE_TEMPLATE: `TYPE_TEMPLATE`,
  TYPE_DATA: `TYPE_DATA`,
  TYPE_VERSION: `TYPE_VERSION`,
}

// Tooltip 偏移量
export const TOOLTIP_OFFSET = {
  x: 40,
  y: 20,
}

// 图谱模式
export const GRAPH_MODE = {
  normal: `NORMAL`, // 通用 正常情况模式 不包括委办局资源目录
  whole: `WHOLE`, // 全部 点击委办局时或筛选时 包括委办局资源目录
  reverse: `REVERSE`, // 反向 点击非委办局相关节点
  search: `SEARCH`, // 图谱搜索
}

// 节点高度期望，用于计算初始缩放比
export const EXPECTED_NODE_HEIGHT = 120

// 节点图标尺寸
export const GRAPH_NODE_SYMBOL_SIZE = {
  normal: 50,
  selected: 60,
}
