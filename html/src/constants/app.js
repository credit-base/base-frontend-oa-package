/**
 * @file 应用配置
 * @module constants/app
 */

const time = new Date()
const year = time.getFullYear()

// 版权商
export const COPYRIGHT_BRAND_TIME = year
export const FAVICON_LINK = '/static/favicon.ico'

// 分页器
export const PAGINATION_INIT_PAGE = 1
export const PAGINATION_INIT_LIMIT = 10

// 排序
export const SORT_BY_UPDATE_TIME = {
  ascending: 'updateTime',
  descending: '-updateTime',
}

export const SORT_BY_END_TIME = {
  ascending: 'endTime',
  descending: '-endTime',
}

// 默认ID
export const DEFAULT_ID = 'Lw'

// Plugins
export const CONSTANTS_PLUGIN_KEY = '$CONSTANTS'
export const CONSTANTS_PLUGIN_EXPORT_KEY = '__VUE_APP_CONSTANTS_PLUGIN__'

export const THEME_ENUM = ['success', 'info', 'warning', 'danger']
