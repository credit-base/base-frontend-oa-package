/**
 * @file 主题配置
 * @module constants/theme
 */

const THEMES_CONFIG = [
  { name: 'THEME_GOLD', class: 'theme-gold', primaryColor: '#ffd179' },
  { name: 'THEME_BLUE', class: 'theme-blue', primaryColor: '#0085ff' },
  { name: 'THEME_RED', class: 'theme-red', primaryColor: '#ff4447' },
]

export { THEMES_CONFIG }
