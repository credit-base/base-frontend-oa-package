/**
 * @file 通用接口常量
 * @module constants/response
 */

// 是或者否
export const YES_OR_NO_VALUES = { yes: 'MA', no: 'Lw' }
export const YES_OR_NO_OPTIONS = [
  { id: YES_OR_NO_VALUES.yes, name: '是' },
  { id: YES_OR_NO_VALUES.no, name: '否' },
]
