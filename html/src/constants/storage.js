/**
 * @file 数据存储
 * @module constants/storage
 */

const { VUE_APP_BRAND_PREFIX = '' } = process.env

/**
 * Token storage key
 */
export const APP_TOKEN_KEY = `${VUE_APP_BRAND_PREFIX}TOKEN`

/**
 * Username storage key
 */
export const APP_USERNAME_KEY = `${VUE_APP_BRAND_PREFIX}USERNAME`

/**
 * Language storage key
 */
export const APP_LANGUAGE_KEY = `${VUE_APP_BRAND_PREFIX}LANGUAGE`

/**
 * Theme storage key
 */
export const APP_THEME_KEY = `${VUE_APP_BRAND_PREFIX}THEME`

/**
 * rules template data
 */
export const APP_RULES_TEMPLATE_KEY = `${VUE_APP_BRAND_PREFIX}_RULES_TEMPLATE_DATA`
