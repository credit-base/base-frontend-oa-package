/**
 * @file 图谱数据Mock
 * @module constants/graph-chart
 */

import { LinearGradient } from 'echarts/lib/util/graphic'

const textStyle = { color: '#a4aabd' }

export const barChartOption = {
  color: [`#C89C38`],
  grid: {
    top: 20,
    left: 10,
    right: 10,
    bottom: 10,
    containLabel: true,
  },
  tooltip: {
    backgroundColor: `rgba(0, 0, 0, 0.3)`,
    borderWidth: 0,
    borderRadius: 2,
    textStyle: {
      color: '#fff',
    },
  },
  xAxis: {
    type: `category`,
    axisTick: { show: false },
    splitLine: { show: false },
    data: [
      `1月`,
      `2月`,
      `3月`,
      `4月`,
      `5月`,
      `6月`,
      `7月`,
      `8月`,
      `9月`,
      `10月`,
      `11月`,
      `12月`,
    ],
  },
  yAxis: {
    type: `value`,
    axisTick: { show: false },
    splitLine: { show: false },
    axisLine: { show: true },
  },
  series: [
    {
      type: 'line',
      symbolSize: 1,
      data: [243, 212, 235, 166, 80, 123, 186, 239, 124, 186, 123, 67].map(
        n => n + Math.floor(Math.random() * 60 + 40),
      ),
    },
    {
      type: 'bar',
      itemStyle: {
        borderRadius: [4, 4, 0, 0],
        color: new LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: 'rgba(146, 225, 255, 1)',
          },
          {
            offset: 1,
            color: 'rgba(0, 151, 251, 1)',
          },
        ]),
      },
      barWidth: '30%',
      data: [243, 212, 235, 166, 80, 123, 186, 239, 124, 186, 123, 67],
    },
  ],
}

export const ringChartOption = {
  color: ['#0083FF', '#D9A83B', '#BE434D'],
  grid: {
    top: 10,
    left: 10,
    right: 10,
    bottom: 10,
  },
  tooltip: {
    backgroundColor: `rgba(0, 0, 0, 0.3)`,
    borderWidth: 0,
    borderRadius: 2,
    textStyle: {
      color: '#fff',
    },
  },
  series: [
    {
      name: `检测正常`,
      type: 'pie',
      radius: ['80%', '90%'],
      clockwise: false,
      emphasis: { scale: false },
      label: { show: false },
      data: [
        { name: '检测正常', value: 50 },
        {
          name: '总数',
          value: 100,
          tooltip: { show: false },
          itemStyle: {
            color: '#0083FF',
            opacity: 0.2,
          },
        },
        {
          name: '背景',
          value: 40,
          tooltip: { show: false },
          itemStyle: {
            color: `rgba(0, 0, 0, 0)`,
          },
        },
      ],
    },
    {
      name: `警告预警`,
      type: 'pie',
      radius: ['55%', '65%'],
      clockwise: false,
      emphasis: { scale: false },
      label: { show: false },
      data: [
        { name: '警告预警', value: 30 },
        {
          name: '总数',
          value: 100,
          tooltip: { show: false },
          itemStyle: {
            color: '#D9A83B',
            opacity: 0.2,
          },
        },
        {
          name: '背景',
          value: 40,
          tooltip: { show: false },
          itemStyle: {
            color: `rgba(0, 0, 0, 0)`,
          },
        },
      ],
    },
    {
      name: `危险预警`,
      type: 'pie',
      radius: ['30%', '40%'],
      clockwise: false,
      emphasis: { scale: false },
      label: { show: false },
      data: [
        { name: '危险预警', value: 10 },
        {
          name: '总数',
          value: 100,
          tooltip: { show: false },
          itemStyle: {
            color: '#BE434D',
            opacity: 0.2,
          },
        },
        {
          name: '背景',
          value: 40,
          tooltip: { show: false },
          itemStyle: {
            color: `rgba(0, 0, 0, 0)`,
          },
        },
      ],
    },
  ],
}

export const pieChartOption = {
  color: [`#0081FF`, `#8291A9`, `#22CCE2`, `#FDBF5E`],
  grid: {
    top: 50,
    bottom: 20,
    left: 20,
    right: 20,
    containLabel: true,
  },
  legend: {
    top: 20,
    right: 40,
    icon: `roundRect`,
    orient: `vertical`,
    itemWidth: 14,
    itemHeight: 14,
    itemGap: 15,
    textStyle,
  },
  tooltip: {
    backgroundColor: `rgba(0, 0, 0, 0.3)`,
    borderWidth: 0,
    borderRadius: 2,
    formatter: v => `${v.marker} ${v.name} ${v.percent}%`,
    textStyle: {
      color: '#fff',
    },
  },
  series: [
    {
      name: '数据应用',
      type: 'pie',
      center: ['32%', '50%'],
      radius: ['55%', '95%'],
      minAngle: 40,
      avoidLabelOverlap: false,
      label: {
        position: `inside`,
        formatter: `{d}%`,
        color: '#fff',
        align: 'center',
      },
      labelLine: { show: false },
      data: [
        { value: 60, name: '市政局' },
        { value: 4, name: '税务局' },
        { value: 16, name: '地震局' },
        { value: 20, name: '市妇联' },
      ],
    },
  ],
}

// 折线图
export const lineChartOption = {
  grid: {
    top: 50,
    bottom: 20,
    left: 20,
    right: 20,
    containLabel: true,
  },
  tooltip: {
    backgroundColor: `rgba(0, 0, 0, 0.3)`,
    borderWidth: 0,
    borderRadius: 2,
    formatter: v =>
      [
        v.seriesName,
        `${v.marker} <span style="font-size: 0.8em;">总数据报送量: ${v.value}</span>`,
      ].join(`<br/>`),
    textStyle: {
      color: '#fff',
    },
  },
  legend: {
    left: 20,
    icon: 'roundRect',
    itemWidth: 14,
    itemHeight: 14,
    itemGap: 40,
    textStyle,
  },
  yAxis: {
    type: `value`,
    axisTick: { show: false },
    splitLine: { show: false },
    axisLine: { show: true },
  },
  xAxis: {
    type: `category`,
    axisTick: { show: false },
    splitLine: { show: false },
    data: [
      `1月`,
      `2月`,
      `3月`,
      `4月`,
      `5月`,
      `6月`,
      `7月`,
      `8月`,
      `9月`,
      `10月`,
      `11月`,
      `12月`,
    ],
  },
  series: [
    {
      name: `市政局`,
      type: `line`,
      symbolSize: 4,
      smooth: true,
      data: [
        1494, 1235, 1693, 1127, 998, 1073, 1289, 1235, 1398, 967, 1692, 1172,
      ],
    },
    {
      name: `税务局`,
      type: `line`,
      symbolSize: 4,
      smooth: true,
      data: [
        1735, 1398, 967, 1692, 1172, 1494, 1235, 1693, 1127, 998, 1073, 1289,
      ],
    },
    {
      name: `地震局`,
      type: `line`,
      symbolSize: 4,
      smooth: true,
      data: [
        1693, 1127, 998, 1073, 1289, 1735, 1398, 967, 1692, 1172, 1494, 1235,
      ].map(n => n - 500),
    },
    {
      name: `市妇联`,
      type: `line`,
      symbolSize: 4,
      smooth: true,
      data: [
        967, 1692, 1172, 1494, 1235, 1693, 1127, 1238, 1073, 1289, 1135, 1398,
      ].map(n => n - 500),
    },
  ],
}

// 折线图
export const wbjChartOption = {
  grid: {
    top: 50,
    bottom: 20,
    left: 20,
    right: 20,
    containLabel: true,
  },
  tooltip: {
    backgroundColor: `rgba(0, 0, 0, 0.3)`,
    borderWidth: 0,
    borderRadius: 2,
    formatter: v =>
      [
        v.seriesName,
        `${v.marker} <span style="font-size: 0.8em;">总数据报送量: ${v.value}</span>`,
      ].join(`<br/>`),
    textStyle: {
      color: '#fff',
    },
  },
  legend: {
    left: 20,
    icon: 'roundRect',
    itemWidth: 14,
    itemHeight: 14,
    itemGap: 40,
    textStyle,
  },
  yAxis: {
    type: `value`,
    axisTick: { show: false },
    splitLine: { show: false },
    axisLine: { show: true },
  },
  xAxis: {
    type: `category`,
    axisTick: { show: false },
    splitLine: { show: false },
    data: [
      `1月`,
      `2月`,
      `3月`,
      `4月`,
      `5月`,
      `6月`,
      `7月`,
      `8月`,
      `9月`,
      `10月`,
      `11月`,
      `12月`,
    ],
  },
  series: [
    {
      name: `南阳市`,
      type: `bar`,
      symbolSize: 4,
      smooth: true,
      data: [
        1494, 1235, 1693, 1127, 998, 1073, 1289, 1235, 1398, 967, 1692, 1172,
      ],
    },
    {
      name: `卧龙区`,
      type: `bar`,
      symbolSize: 4,
      smooth: true,
      data: [
        1735, 1398, 967, 1692, 1172, 1494, 1235, 1693, 1127, 998, 1073, 1289,
      ],
    },
    {
      name: `宛城区`,
      type: `bar`,
      symbolSize: 4,
      smooth: true,
      data: [
        1693, 1127, 998, 1073, 1289, 1735, 1398, 967, 1692, 1172, 1494, 1235,
      ].map(n => n - 500),
    },
    {
      name: `邓州市`,
      type: `bar`,
      symbolSize: 4,
      smooth: true,
      data: [
        967, 1692, 1172, 1494, 1235, 1693, 1127, 1238, 1073, 1289, 1135, 1398,
      ].map(n => n - 500),
    },
  ],
}

// 数据统计图
export const statisticsChartOptions = {
  color: [`#F4B54F`, `#22D2E4`, `#0083FF`, `#65D400`],
  backgroundColor: `rgba(42, 52, 61, 0.8)`,
  grid: {
    top: 100,
    bottom: 20,
    left: 20,
    right: 40,
    containLabel: true,
  },
  tooltip: {
    backgroundColor: `rgba(0, 0, 0, 0.3)`,
    borderWidth: 0,
    borderRadius: 2,
    formatter: v =>
      [
        v.seriesName,
        `${v.marker} <span style="font-size: 0.8em;">总数据报送量: ${v.value}</span>`,
      ].join(`<br/>`),
    textStyle: {
      color: '#fff',
    },
  },
  legend: {
    top: 80,
    left: `center`,
    icon: 'roundRect',
    itemWidth: 12,
    itemHeight: 12,
    itemGap: 40,
    textStyle,
  },
  yAxis: {
    type: `value`,
    axisTick: { show: false },
    splitLine: { show: false },
    axisLine: { show: false },
    axisLabel: { color: `#a4aabd` },
  },
  xAxis: {
    type: `category`,
    axisTick: { show: false },
    splitLine: { show: false },
    axisLabel: { color: `#a4aabd` },
    data: [
      `一月`,
      `二月`,
      `三月`,
      `四月`,
      `五月`,
      `六月`,
      `七月`,
      `八月`,
      `九月`,
      `十月`,
      `十一月`,
      `十二月`,
    ],
  },
  series: [
    {
      name: `南阳市`,
      type: `line`,
      symbol: `circle`,
      symbolSize: 8,
      markLine: {
        silent: true,
        lineStyle: { color: `#65D400` },
        // data: {
        //   0: { yAxis: 600 },
        //   1: { yAxis: 600 },
        // },
        label: {
        },
        data: [
          { name: `预警线`, yAxis: 600 },
        ],
      },
      markPoint: {
        data: [{ type: `max`, label: { color: '#fff' } }],
      },
      data: [
        1494, 1235, 1693, 1127, 998, 1073, 1289, 1235, 1398, 967, 1692, 1172,
      ],
    },
    {
      name: `卧龙区`,
      type: `line`,
      symbol: `circle`,
      symbolSize: 8,
      markPoint: {
        data: [{ type: `max`, label: { color: '#fff' } }],
      },
      data: [
        1735, 1398, 967, 1692, 1172, 1494, 1235, 1693, 1127, 998, 1073, 1289,
      ],
    },
    {
      name: `宛城区`,
      type: `line`,
      symbol: `circle`,
      symbolSize: 8,
      markPoint: {
        data: [{ type: `min`, label: { color: '#fff' } }],
      },
      data: [
        1693, 1127, 998, 1073, 1289, 1735, 1398, 967, 1692, 1172, 1494, 1235,
      ].map(n => n - 500),
    },
    {
      name: `邓州市`,
      type: `line`,
      symbol: `circle`,
      symbolSize: 8,
      markPoint: {
        data: [{ type: `max`, label: { color: '#fff' } }],
      },
      data: [
        967, 1692, 1172, 1494, 1235, 1693, 1127, 1238, 1073, 1289, 1135, 1398,
      ].map(n => n - 500),
    },
  ],
}
