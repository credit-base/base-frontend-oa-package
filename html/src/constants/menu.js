export const SYSTEM_MENU = [
  {
    id: 0,
    name: '首页',
    icon: 'dashboard',
    link: '/',
  },
  {
    id: 1,
    name: '员工管理',
    icon: 'crew',
    link: '/crew',
  },
  {
    id: 2,
    name: '目录管理',
    icon: 'resource-catalog-subsystem',
    link: '/resource-catalog/gb-template',
  },
  {
    id: 3,
    name: '规则管理',
    icon: 'rule-manage-subsystem',
    link: '/rule-manage/gb',
  },
  {
    id: 4,
    name: '数据管理',
    icon: 'data-manage-subsystem',
    link: '/data-manage/raw-data',
  },
  {
    id: 5,
    name: '前置机系统',
    icon: 'exchange-sharing-subsystem',
    link: '/front-end-data-management/templates',
  },
]
export const CUSTOM_MENU = [
  {
    id: 0,
    name: '本级目录',
    icon: 'bj-template',
    link: '/resource-catalog/bj-template/add',
  },
  {
    id: 1,
    name: '国标目录',
    icon: 'edit',
    link: '/resource-catalog/gb-template/add',
  },
  {
    id: 2,
    name: '委办局目录',
    icon: 'edit',
    link: '/resource-catalog/wbj-template/add',
  },
  {
    id: 3,
    name: '上传数据',
    icon: 'edit',
    link: '/data-manage/raw-template/detail/MA',
  },
  {
    id: 4,
    name: '目录总览',
    icon: 'wbj-template',
    link: '/data-manage/raw-data',
  },
]
export const SETTING_MENU = [
  {
    id: 0,
    name: '设置',
    icon: 'system',
    type: 'SETTING',
  },
  {
    id: 1,
    name: '帮助',
    icon: 'help',
    type: 'HELP',
  },
]

export const SHORTCUT_BUTTON_ENUM = [
  {
    id: 1,
    name: '员工管理',
    icon: 'crew',
    link: '/',
  },
  {
    id: 2,
    name: '委办局管理',
    icon: 'user-group',
    link: '/',
  },
  {
    id: 3,
    name: '国标规则',
    icon: 'gb-rule',
    link: '/',
  },
  {
    id: 4,
    name: '国标目录',
    icon: 'gb-template',
    link: '/',
  },
  {
    id: 5,
    name: '委办局目录',
    icon: 'wbj-template',
    link: '/',
  },
  {
    id: 6,
    name: '本级目录',
    icon: 'bj-template',
    link: '/',
  },
  {
    id: 7,
    name: '本级目录',
    icon: 'bj-template',
    link: '/',
  },
  {
    id: 8,
    name: '本级目录',
    icon: 'edit',
    link: '/',
  },
  {
    id: 9,
    name: '国标目录',
    icon: 'edit',
    link: '/',
  },
  {
    id: 10,
    name: '委办局目录',
    icon: 'edit',
    link: '/',
  },
  {
    id: 11,
    name: '上传数据',
    icon: 'edit',
    link: '/',
  },
  {
    id: 12,
    name: '目录总览',
    icon: 'wbj-template',
    link: '/',
  },
  {
    id: 13,
    name: '规则审核',
    icon: 'edit',
    link: '/',
  },
]
