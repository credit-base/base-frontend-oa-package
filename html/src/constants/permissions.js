
export default {
  USER_GROUP_ID: 1, // 委办局
  CREW_ID: 2, // 员工
  NEWS_ID: 11, // 新闻管理
  UN_AUDITED_NEWS_ID: 12, // 新闻审核管理
  MEMBER_ID: 13, // 用户管理
  JOURNALS_ID: 14, // 信用刊物管理
  UN_AUDITED_JOURNALS_ID: 15, // 信用刊物审核
  CREDIT_PHOTOGRAPHY_ID: 16, // 信用随手拍
  // 目录管理子系统
  // |- 目录管理
  GB_TEMPLATE_ID: 3, // 国标目录管理
  BJ_TEMPLATE_ID: 4, // 本级目录管理
  WBJ_TEMPLATE_ID: 5, // 委办局目录管理
  // |- 规则管理
  GB_RULE_MANAGE_ID: 6, // 国标目录规则管理
  UN_AUDITED_GB_RULE_MANAGE_ID: 18, // 国标目录规则管理
  BJ_RULE_MANAGE_ID: 19, // 本级目录规则审核
  UN_AUDITED_BJ_RULE_MANAGE_ID: 20, // 本级目录规则审核
  // |- 数据管理
  GB_DATA_MANAGE_ID: 7, // 国标数据管理
  BJ_DATA_MANAGEMENT_ID: 8, // 本级数据管理
  WBJ_DATA_MANAGE_ID: 9, // 委办局数据管理
  // |- 前置机交换子系统
  FRONT_END_DATA_MANAGEMENT_ID: 17, // 前置机数据管理
  EXCHANGE_SHARING_RULE_ID: 10, // 共享规则管理
  // 信用信息查询应用系统
  // |- 企业管理
  ENTERPRISE_ID: 21,
  BASE_TEMPLATE_ID: 22,
  // FIXME: 获取基础规则id
  // BASE_RULE_ID: 23,
  // 业务信息管理应用系统
  // |- 业务管理
  COMPLAINT_ID: 23,
  PRAISE_ID: 24,
  QA_ID: 25,
  APPEAL_ID: 26,
  FEEDBACK_ID: 27,
  // |- 业务审核
  UN_AUDITED_COMPLAINT_ID: 28,
  UN_AUDITED_PRAISE_ID: 29,
  UN_AUDITED_QA_ID: 30,
  UN_AUDITED_APPEAL_ID: 31,
  UN_AUDITED_FEEDBACK_ID: 32,
}
