/**
 * @file 三方应用配置
 * @module constants/config
 */

const {
  VUE_APP_OSS_UPLOAD_HOST,
  VUE_APP_OSS_UPLOAD_PATH,
  VUE_APP_BAIDU_MAP_APIKEY,
  VUE_APP_TINYMCE_API_KEY,
} = process.env

/**
 * 阿里云 OSS
 * @see https://oss.console.aliyun.com/overview
 */
// OSS 上传成功状态码
export const OSS_SUCCESS_ACTION_STATUS = '200'

// OSS 上传域名
export const OSS_UPLOAD_HOST = VUE_APP_OSS_UPLOAD_HOST

// OSS 上传路径
export const OSS_UPLOAD_PATH = VUE_APP_OSS_UPLOAD_PATH

// OSS 上传文件前缀
export const OSS_PREFIX = `${OSS_UPLOAD_HOST}/${OSS_UPLOAD_PATH}`

/**
 * 百度地图 ApiKey
 * @see http://lbsyun.baidu.com/apiconsole/key
 */
export const BAIDU_MAP_APIKEY = VUE_APP_BAIDU_MAP_APIKEY

/**
 * Tinymce ApiKey
 * @see https://apps.tiny.cloud/my-account/confirmation-free-trial
 */
export const TINYMCE_API_KEY = VUE_APP_TINYMCE_API_KEY
