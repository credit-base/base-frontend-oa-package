/**
 * @file 数据请求常量
 * @module constants/request
 */

// 请求成功
export const REQUEST_STATUS_SUCCESS = 1

// 请求错误
export const REQUEST_STATUS_ERROR = 0

// 请求响应 ID 17 需登录
export const RESPONSE_ID_NEED_LOGIN = 17

// 请求过期时间
export const REQUEST_TIMEOUT_MILLISECONDS = 30 * 1000
