/**
 * @file 正则表达式
 * @module constants/regexp
 * https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/WidgetRule/common.md
 */

// require.context API 去路径后缀
export const RE_REQUIRE_CONTEXT_DEHYDRATE = /\.\/|\.js/g

// 外部链接
export const RE_EXTERNAL_LINK = /^(https?:|mailto:|tel:|[a-zA-Z]{4,}:)/

// ===================================
//                表单校验
// ===================================

// 手机号
export const RE_CELLPHONE = /^1\d{10}$/

// 密码
// eslint-disable-next-line max-len
// export const RE_PASSWORD = /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[~!@#$%^*.()_+-={}:;?,])[\da-zA-Z~!@#$%^*.()_+-={}:;?,]{8,20}$/
// eslint-disable-next-line no-useless-escape
export const RE_PASSWORD = /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[~,!@#$%^*.()_+{}:;?=-])[\da-zA-Z~,!@#$%^*.()_+{}:;?=-]{8,20}$/

// 验证码
export const RE_CAPTCHA = /\S/

// 身份证号15～18位
export const RE_CARD_ID = /(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9X]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}$)/

// 汉字
export const RE_CHINESE = /^[\u4e00-\u9fa5]*$/

// 驳回原因
export const RE_REJECT_REASON = /^.{1,200}$/

// 撤销原因
export const RE_REVOKE_REASON = /^.{1,200}$/

// 描述
export const RE_DESCRIPTION = /^.{1,200}$/

// i18n 提示语
export const RE_I18N_TIP = /.+/

// 姓名
export const RE_REAL_NAME = /^[\u4E00-\u9FA5]{1,5}[·(\u4E00-\u9FA5)]{1,10}$/

// 通用Title
export const RE_TITLE = /^.{5,80}$/

// NAME
export const RE_NAME = /^[\u4e00-\u9fa5]{2,50}$/

// 来源
export const RE_SOURCE = /^.{2,15}$/

// 科室名称
export const RE_DEPARTMENT_TITLE = /^[\u4e00-\u9fa5]{2,15}$/

// 地址
export const RE_ADDRESS = /^.{5,100}$/

// 原因
export const RE_REASON = /^.{2,200}$/

// =========================================================
//                           业务
// =========================================================

// 资源目录标识
export const RE_RESOURCE_CATALOG_IDENTIFY = /^[A-Z][A-Z_]{0,98}[A-Z]$/

// 资源目录模板长度
export const RE_RESOURCE_CATALOG_LENGTH = /^[1-9][0-9]{0,99}$/

export const CASE_AWARD_PENALTY_IDENTIFY_PATTERN = /^[a-zA-Z0-9]{15,18}$/
export const DATE_RULE = /^[0-9-]{10}$/
export const NO_TRIM = /(^[\s])([\s]$)/

// 资源目录文件名正则

export const RE_TEMPLATE_NAME = /^[A-Z]+_[0-9]+_[0-9]+$/

// 文件名分割正则

export const RE_FILE_NAME = /(.*\/)*([^.]+).*/ig
