/**
 * @file 资源目录常量
 * @module constants/modules/resource-catalog
 */

export const DATA_TYPE_VALUES = {
  char: 'MA',
  date: 'MQ',
  collection: 'NQ',
  enum: 'NA',
  integer: 'Mg',
  float: 'Mw',
}
export const DATA_TYPE_OPTIONS = [
  { id: DATA_TYPE_VALUES.char, name: '字符型' },
  { id: DATA_TYPE_VALUES.date, name: '⽇期型' },
  { id: DATA_TYPE_VALUES.integer, name: '整数型' },
  { id: DATA_TYPE_VALUES.float, name: '浮点型' },
  { id: DATA_TYPE_VALUES.enum, name: '枚举型' },
  { id: DATA_TYPE_VALUES.collection, name: '集合型' },
]

// 主体类别
export const SUBJECT_CATEGORY_VALUES = {
  legalPerson: 'MA',
  naturalPerson: 'MQ',
  individualBusiness: 'Mg',
}
export const SUBJECT_CATEGORY_OPTIONS = [
  {
    id: SUBJECT_CATEGORY_VALUES.legalPerson,
    name: '法人及非法人组织',
    requiredIdentifies: ['ZTMC', 'TYSHXYDM'],
  },
  {
    id: SUBJECT_CATEGORY_VALUES.naturalPerson,
    name: '自然人',
    requiredIdentifies: ['ZTMC', 'ZJHM'],
  },
  {
    id: SUBJECT_CATEGORY_VALUES.individualBusiness,
    name: '个体工商户',
    requiredIdentifies: ['ZTMC', 'TYSHXYDM'],
  },
]
