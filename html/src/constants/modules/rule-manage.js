/**
 * @file
 */

export const RULE_STATUS_VALUES = {
  approved: 'MQ',
  rejected: 'LC0',
  pending: 'Lw',
  revoked: 'LC8',
}
export const RULE_STATUS_OPTIONS = [
  { id: RULE_STATUS_VALUES.approved, name: '已通过' },
  { id: RULE_STATUS_VALUES.rejected, name: '已驳回' },
  { id: RULE_STATUS_VALUES.pending, name: '待审核' },
  { id: RULE_STATUS_VALUES.revoked, name: '已撤销' },
]
