
/**
 * 后端要求无数据的文件传 `[]`
 */
export function transformFile (file = {}) {
  return file.identify ? file : []
}

/**
 * 创建提交数据
 */
export function createWebsiteConfig (config = {}, options = {}) {
  const {
    nav,
    logo,
    theme,
    headerBg,
    headerSearch,
    headerBarLeft,
    headerBarRight,
    silhouette,
    footerNav,
    footerTwo,
    footerThree,
    frameWindow,
    relatedLinks,
    footerBanner,
    centerDialog,
    specialColumn,
    animateWindow,
    leftFloatCard,
    rightToolBar,
    memorialStatus,
    organizationGroup,
    partyAndGovernmentOrgans,
    governmentErrorCorrection,
  } = config
  const content = {
    nav: nav.map(item => ({ ...item, image: transformFile(item.image) })),
    logo: transformFile(logo),
    theme: { ...theme, image: transformFile(theme.image) },
    headerBg: transformFile(headerBg),
    headerSearch,
    headerBarLeft,
    headerBarRight,
    silhouette: { ...silhouette, image: transformFile(silhouette.image) },
    footerNav,
    footerTwo,
    footerThree,
    frameWindow,
    relatedLinks,
    footerBanner: footerBanner
      .map(item => ({ ...item, image: transformFile(item.image) })),
    centerDialog: {
      ...centerDialog,
      image: transformFile(centerDialog.image),
    },
    specialColumn: specialColumn
      .map(item => ({ ...item, image: transformFile(item.image) })),
    animateWindow: animateWindow
      .map(item => ({ ...item, image: transformFile(item.image) })),
    leftFloatCard: leftFloatCard
      .map(item => ({ ...item, image: transformFile(item.image) })),
    rightToolBar: rightToolBar
      .map(item => ({
        ...item,
        images: (item.images || [])
          .filter(subItem => subItem.identify)
          .map(subItem => ({ ...subItem, image: transformFile(subItem.image) })),
      })),
    memorialStatus,
    organizationGroup: organizationGroup
      .map(item => ({ ...item, image: transformFile(item.image) })),
    partyAndGovernmentOrgans: {
      ...partyAndGovernmentOrgans,
      image: transformFile(partyAndGovernmentOrgans.image),
    },
    governmentErrorCorrection: {
      ...governmentErrorCorrection,
      image: transformFile(governmentErrorCorrection.image),
    },
  }
  return content
}

/**
 * 创建映射
 * @param {string[]} list
 */
export function makeMap (list) {
  if (!Array.isArray(list)) return {}
  return list.reduce((obj, key) => ({ ...obj, [key]: key }), {})
}
