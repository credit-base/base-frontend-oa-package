
import { makeMap } from './utils'

// 门户页面
export const WEBSITE_PAGE_CATEGORY = {
  home: `MA`,
}
export const WEBSITE_PAGE_MAP = new Map([
  [WEBSITE_PAGE_CATEGORY.home, `TheHome`],
])

// 新增按钮位置
export const ACTION_POSITION_VALUES = {
  top: `top`,
  right: `right`,
  left: `left`,
  bottom: `bottom`,
}
export const ACTION_POSITION_OPTIONS = [
  ACTION_POSITION_VALUES.top,
  ACTION_POSITION_VALUES.right,
  ACTION_POSITION_VALUES.bottom,
  ACTION_POSITION_VALUES.left,
]

// 外边框样式
export const OUTLINE_TYPE_VALUES = {
  primary: `primary`,
  theme: `theme`,
  topic: `topic`,
}
export const OUTLINE_TYPE_OPTIONS = [
  OUTLINE_TYPE_VALUES.primary,
  OUTLINE_TYPE_VALUES.theme,
  OUTLINE_TYPE_VALUES.topic,
]

// 首页功能
// 登录注册 用户中心 合并为 Mw
export const WEBSITE_SERVICE_TYPE = {
  NULL: `Lw`, // 默认
  PV: `MA`, // PV统计
  FONT: `MQ`, // 简繁切换
  ACCESSIBLE_READING: `Mw`, // 无障碍阅读
  SIGN_IN: `Mg`, // 登录
  SIGN_UP: `NQ`, // 注册
  USER_CENTER: `NQ`, // 用户中心
  FOR_THE_RECORD: `Ng`, // 备案
  PUBLIC_NETWORK_SECURITY: `Nw`, // 公网安备
  WEBSITE_IDENTIFY: `OA`, // 网站标识码
}

// 启用禁用
export const STATUS_ENABLE = 'Lw'
export const STATUS_DISABLE = 'LC0'
// 状态(0未发布 2 发布)
export const STATUS_OPTIONS = [
  { id: STATUS_ENABLE, name: '启用' },
  { id: STATUS_DISABLE, name: '禁用' },
]

// 内容类型
export const CATEGORY_IMAGE = 'MA'
export const CATEGORY_LINK = 'MQ'
export const CATEGORY_WRITTEN_WORDS = 'Mg'

// 1 图片、 2 链接、 3 文字、
export const CATEGORY_OPTIONS = [
  { id: CATEGORY_IMAGE, name: '图片' },
  { id: CATEGORY_LINK, name: '链接' },
  { id: CATEGORY_WRITTEN_WORDS, name: '文字' },
]

// 哀悼模式
export const STATUS_MEMORIAL = {
  yes: `MA`,
  no: `Lw`,
}

// 操作类型
export const ACTION_TYPE = {
  add: `ADD`,
  edit: `EDIT`,
  delete: `DELETE`,
  update: `UPDATE`,
}

// 操作功能
export const WEBSITE_CONFIG_ACTION = makeMap([
  `headerBarLeft`,
  `headerBarRight`,
  `headerSearch`,
  `logo`,
  `nav`,
  `specialColumn`,
  `footerBanner`,
  `relatedLinks`,
  `relatedSubLinks`,
  `organizationGroup`,
  `silhouette`,
  `footerNav`,
  `footerTwo`,
  `footerThree`,
  `partyAndGovernmentOrgans`,
  `governmentErrorCorrection`,
  `centerDialog`,
  `animateWindow`,
  `leftFloatCard`,
  `rightToolbar`,
])

// 表单组件
export const FORM_COMPONENTS = {
  THEME_SETTING: 'FormThemeSetting',
  HEADER_BG_SETTING: 'FormHeaderBgSetting',
  HEADER_BAR_LEFT_SETTING: 'FormHeaderBarSetting',
  HEADER_BAR_RIGHT_SETTING: 'FormHeaderBarSetting',
  LOGO_SETTING: 'FormLogoSetting',
  HEADER_SEARCH_SETTING: 'FormSearchTypeSetting',
  NAV_SETTING: 'FormNavSetting',
  CENTER_DIALOG_SETTING: 'FormSpecialColumnSetting',
  FRAME_WINDOW_SETTING: `FormFrameWindowSetting`,
  SPECIAL_COLUMN_SETTING: 'FormSpecialColumnSetting',
  FOOTER_BANNER_SETTING: 'FormSpecialColumnSetting',
  ANIMATE_WINDOW_SETTING: 'FormSpecialColumnSetting',
  LEFT_FLOAT_CARD_SETTING: 'FormSpecialColumnSetting',
  RELATED_LINKS_SETTING: 'FormRelatedLinksSetting',
  RELATED_SUB_LINKS_SETTING: 'FormHeaderBarSetting',
  ORGANIZATION_GROUP_SETTING: `FormOrganizationGroup`,
  FOOTER_NAV_SETTING: `FormFooterNavSetting`,
  FOOTER_TWO_SETTING: `FormFooterTwoSetting`,
  FOOTER_THREE_SETTING: `FormFooterThreeSetting`,
  RIGHT_TOOL_BAR_SETTING: 'FormToolBarSetting',
  SILHOUETTE_SETTING: 'FormSilhouetteSetting',
  PARTY_AND_GOVERNMENT_ORGANS_SETTING: 'FormSpecialColumnSetting',
  GOVERNMENT_ERROR_CORRECTION_SETTING: 'FormSpecialColumnSetting',
}

export const UPLOAD_TIPS_OPTIONS = {
  SPECIAL_COLUMN_SETTING: '只能上传png,jpg,jpeg文件，宽590px 高114px，且文件大小不能超过2M',
  FOOTER_BANNER_SETTING: '只能上传png,jpg,jpeg文件，宽1200px 高144px，且文件大小不能超过2M',
  ANIMATE_WINDOW_SETTING: '只能上传png,jpg,jpeg文件，宽266px 高140px，且文件大小不能超过2M',
  LEFT_FLOAT_CARD_SETTING: '只能上传png,jpg,jpeg文件，宽76px，且文件大小不能超过2M',
}
