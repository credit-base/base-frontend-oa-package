/**
 * 翻译器 - 委办局
 *
 * @module translators/user-group
 */

import UserGroup from '@/models/user-group'
import Department from '@/models/department'
import Crew from '@/models/crew'

// 所有委办局
export function translateFullUserGroup (res = {}) {
  const list = Array.isArray(res.list)
    ? res.list.map(item => new UserGroup(item)
      .get([
        'id',
        'name',
      ]))
    : []

  return list
}

// 委办局列表
export function translateUserGroupList (res = {}) {
  const { total = 0 } = res
  const list = Array.isArray(res.list)
    ? res.list.map(item => new UserGroup(item)
      .get([
        'id',
        'name',
        'shortName',
      ]))
    : []

  return {
    list,
    total,
  }
}

// 委办局详情
export function translateUserGroupDetail (res = {}) {
  return new UserGroup(res)
    .get([
      'id',
      'name',
      'shortName',
      'crewList',
      'departmentList',
    ])
}

// 所有科室
export function translateFullDepartment (res = {}) {
  const list = Array.isArray(res.list)
    ? res.list.map(item => new Department(item)
      .get([
        'id',
        'name',
      ]))
    : []

  return list
}

// 科室列表
export function translateDepartmentList (res = {}) {
  const { total = 0 } = res
  const list = Array.isArray(res.list)
    ? res.list.map(item => new Department(item)
      .get([
        'id',
        'name',
        'updateTimeFormat',
        'userGroup',
      ]))
    : []

  return {
    list,
    total,
  }
}

// 科室详情
export function translateDepartmentDetail (res = {}) {
  return new Department(res)
    .get([
      'id',
      'name',
      'updateTimeFormat',
      'userGroup',
    ])
}

// 人员列表
export function translateCrewList (res = {}) {
  const { total = 0, list = [] } = res
  const dataList = Array.isArray(list)
    ? list.map(item => new Crew(item)
      .get([
        'id',
        'cellphone',
        'userGroup',
        'category',
        'status',
        'department',
        'realName',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: dataList,
    total,
  }
}

// 人员详情
export function translateCrewDetail (res = {}) {
  return new Crew(res).get([
    'id',
    'cellphone',
    'realName',
    'cardId',
    'updateTimeFormat',
    'createTime',
    'userGroup',
    'category',
    'department',
    'status',
    'purview',
  ])
}
