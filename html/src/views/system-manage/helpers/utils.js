
/**
 * 路由是否可见
 */
const isRouteVisible = route => route.isTabRouter !== undefined || !route.hidden

/**
 * 两个节点是否相同
 */
const isSameNode = (node1, node2) => node1.path === node2.path && node1.name === node2.name

/**
 * 过滤不可见路由
 */
function filterHiddenRoutes (routes) {
  const res = []

  routes.forEach(route => {
    const hasChildren = Boolean(route.children && route.children.length)
    const temp = { ...route, hasChildren }

    if (isRouteVisible(temp)) {
      if (temp.children) {
        temp.children = filterHiddenRoutes(temp.children)
      }

      res.push(temp)
    }
  })

  return res
}

/**
 * 树转数组
 */
export function flattenRoutes (routes) {
  const result = []
  let queen = []

  queen = queen.concat(routes)

  while (queen.length) {
    const first = queen.shift()

    if (first.children) {
      queen = queen.concat(first.children)
      delete first.children
    }

    result.push(first)
  }

  return result
}

/**
 * 获取所有叶子节点
 */
function getAllLeafs (data) {
  const result = []

  function getLeaf (arr) {
    arr.forEach(item => {
      if (!item.children) {
        result.push(item)
      } else {
        getLeaf(item.children)
      }
    })
  }
  getLeaf(data)

  return result
}

/**
 * 获取根至叶子节点路径
 */
function findLeafPathInTree (tree, targetNode) {
  let path = null

  for (let i = 0; i < tree.length; i++) {
    const node = tree[i]

    if (node.children && node.children.length) {
      path = findLeafPathInTree(node.children, targetNode)

      if (path) {
        path.unshift(node)
        return path
      }
    } else {
      if (isSameNode(node, targetNode)) {
        return [node]
      }
    }
  }

  return path
}

function dehydrateUnusedLeaf (nodes) {
  const length = nodes.length

  if (length === 1) {
    return nodes
  } else {
    const targetIdx = length - 2

    return nodes[targetIdx].alwaysShow
      ? nodes
      : nodes.filter((_, idx) => idx !== targetIdx)
  }
}

/**
 * 标记叶子节点
 */
function markLeafNode (nodes) {
  const [last, ...rest] = [...nodes].reverse()

  rest.reverse()

  return [...rest, { ...last, isLeaf: true }]
}

/**
 * 扁平化路由树转化为数组
 * @param {array} routes 路由
 */
export function normalizeRoutes (routes = []) {
  const visibleRoutes = filterHiddenRoutes(routes)
  const leafs = getAllLeafs(visibleRoutes)
  const list = leafs
    .map(leaf => findLeafPathInTree(visibleRoutes, leaf))
    .map(dehydrateUnusedLeaf)
    .map(markLeafNode)

  return list
}

export function generateSelectData (val, type) {
  return val.map(item => {
    return {
      value: item.id,
      label: item[type],
    }
  })
}
