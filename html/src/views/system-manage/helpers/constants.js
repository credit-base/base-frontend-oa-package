/**
 * 常量 - 委办局
 *
 * @module constants/user-group
 */

import { CONSTANTS } from '@/plugins/constants'
import permissions from '@/constants/permissions'

const { USER_GROUP_ID, CREW_ID } = permissions

const { PURVIEW } = CONSTANTS

export const USER_GROUP_PAGE_TABS = {
  ADMIN_TABS: [
    { name: 'user-group', label: 'USER_GROUP_LIST', component: 'ListViewUserGroup' },
    { name: 'department', label: 'DEPARTMENT_LIST', component: 'ListViewDepartment' },
  ],
  USER_GROUP_TABS: [
    { name: 'user-group', label: 'USER_GROUP_DETAIL', component: 'DetailViewUserGroup' },
    { name: 'department', label: 'DEPARTMENT_LIST', component: 'ListViewDepartment' },
  ],
}

export const USER_GROUP_DETAIL_TABS = [
  { name: 'department', label: 'DEPARTMENT', component: 'Department' },
  { name: 'crew', label: 'CREW', component: 'Crew' },
]

export const CREW_TABS = [
  { name: 'admin-crew', label: 'ADMIN_CREW' },
  { name: 'user-group-crew', label: 'USER_GROUP_CREW' },
  { name: 'general-crew', label: 'GENERAL_CREW' },
]

export const DEPARTMENT_ROUTE = {
  name: 'UserGroupList',
  params: {
    tabType: 'department',
  },
}

export const CATEGORY_MENU = {
  SUPER_ADMIN: [
    {
      label: PURVIEW.PLATFORM_ADMIN,
      title: '平台管理员',
    },
    {
      label: PURVIEW.USER_GROUP_ADMIN,
      title: '委办局管理员',
    },
    {
      label: PURVIEW.OPERATION_CREW,
      title: '操作员工',
    },
  ],
  PLATFORM_ADMIN: [
    {
      label: PURVIEW.USER_GROUP_ADMIN,
      title: '委办局管理员',
    },
    {
      label: PURVIEW.OPERATION_CREW,
      title: '操作员工',
    },
  ],
  USER_GROUP_ADMIN: [
    {
      label: PURVIEW.OPERATION_CREW,
      title: '操作员工',
    },
  ],
}

export const SEARCH_TYPE = [
  {
    label: '姓名',
    value: 'realName',
  },
  {
    label: '手机号',
    value: 'cellphone',
  },
]
// Permissions
export const PURVIEW_CREW = CREW_ID // 人员管理
export const PLATFORM_ADMIN_PURVIEW = [ // 平台管理员
  CREW_ID,
  USER_GROUP_ID,
]
