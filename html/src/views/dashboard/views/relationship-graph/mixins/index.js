/**
 * @file mixins
 * @module mixins/index
 */

export const Loading = {
  data () {
    return {
      isLoading: false,
    }
  },
  methods: {
    showLoading () {
      this.isLoading = true
    },

    hideLoading () {
      this.isLoading = false
    },
  },
}

export const Dialog = {
  data () {
    return {
      // 新增弹窗
      dialogAddedVisible: false,
      dialogAddedTitle: ``,

      // 表单弹窗 导出
      dialogFormVisible: false,
      dialogFormTitle: ``,
      dialogFormType: ``,

      // 预览弹窗 浮动层
      dialogPreviewVisible: false,
      dialogPreviewCategory: ``,
      dialogPreviewTitle: ``,
      dialogPreviewType: ``,
    }
  },

  methods: {
    showPreviewDialog ({ type = ``, title = ``, category = `` } = {}) {
      this.dialogPreviewVisible = true
      this.dialogPreviewType = type
      this.dialogPreviewTitle = title
      this.dialogPreviewCategory = category
    },

    hidePreviewDialog () {
      this.dialogPreviewVisible = false
      this.dialogPreviewType = ``
      this.dialogPreviewTitle = ``
      this.dialogPreviewCategory = ``
    },

    showAddedDialog ({ title = `` } = {}) {
      this.dialogAddedVisible = true
      this.dialogAddedTitle = title
    },

    hideAddedDialog () {
      this.dialogAddedVisible = false
      this.dialogAddedTitle = ``
      this.addNodeFormValues = { title: ``, type: `` }
    },

    showFormDialog ({ type = ``, title = `` } = {}) {
      this.dialogFormVisible = true
      this.dialogFormTitle = title
      this.dialogFormType = type
    },

    hideFormDialog () {
      this.dialogFormVisible = false
      this.dialogFormTitle = ``
      this.dialogFormType = ``
      this.nodeExportFormValues = { userGroup: ``, dateRange: [] }
    },
  },
}
