import Base from './base'

class GbTemplateVersion extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      version,
      description,
      updateTime,
      createTime,
      currentRelease,
      addItems = [],
      editItems = [],
      deleteItems = [],
      attachment = {},
      crew = {},
    } = params

    this.id = id
    this.version = version
    this.description = description
    this.updateTime = updateTime
    this.createTime = createTime
    this.currentRelease = currentRelease
    this.addItems = addItems
    this.editItems = editItems
    this.deleteItems = deleteItems
    this.attachment = attachment
    this.crew = crew
  }
}

export default GbTemplateVersion
