import Base from '@/models/base'

class Subtasks extends Base {
  constructor (params = {}) {
    super(params)

    const {
      id,
      assignObject = {},
      parentTask = {},
      status = {},
      createTime,
      updateTime,
    } = params

    this.id = id
    this.assignObject = assignObject
    this.parentTask = parentTask
    this.status = status
    this.createTime = createTime
    this.updateTime = updateTime
  }
}

export default Subtasks
