/**
 * @file 帮助方法
 * @module graph/helpers
 */

import {
  GRAPH_ICONS,
  NODE_STATUS,
  NODE_PREFIX_MAP,
  GRAPH_CHART_SIZE,
  GRAPH_COLUMN_COUNT,
} from '@/constants/graph'

/**
 * 使用 Symbol
 */
export function useSymbol (type, { status } = {}) {
  if (!type) return ``
  status = status[0].toUpperCase() + status.slice(1)
  return GRAPH_ICONS[type + status]
}

/**
 * Tooltip
 */
export function useTooltip (html, { title, status, classNames = [] } = {}) {
  if (status === NODE_STATUS.disabled) return ``

  return `
    <div class="${[`graph-tooltip-container`, ...classNames].join(' ')}">
      ${title && `<div class='graph-tooltip-title'>${title}</div>`}
      <div class="graph-tooltip-body">
        ${html}
      </div>
      <span class="graph-tooltip-corner is-top-left"></span>
      <span class="graph-tooltip-corner is-top-right"></span>
      <span class="graph-tooltip-corner is-bottom-left"></span>
      <span class="graph-tooltip-corner is-bottom-right"></span>
    </div>
  `
}

/**
 * 获取时间戳
 */
export function useTimeStamp () {
  return Date.now()
}

/**
 * 生成图谱节点
 */
export function genGraphNode (options = {}) {
  const {
    title,
    type,
    status = `danger`,
    statusText = `暂无规则`,
    value = 0,
  } = options
  const ts = useTimeStamp()
  const node = {
    name: `${title}_${ts}`,
    id: `${NODE_PREFIX_MAP.get(type)}_${ts}`,
    status,
    statusText,
    value,
    targets: [],
  }

  return node
}

// 生成图谱规则
export function genGraphLink (options = {}) {
  const { source, target, title } = options
  const ts = useTimeStamp()
  const link = {
    id: target.id,
    sourceId: source.id,
    rule: {
      name: title,
      id: `RULE_${ts}`,
    },
  }

  return link
}

/**
 * 计算节点位置
 */
export function useNodePosition (
  { rows, rowIdx, colIdx, startY = 0, endY = 0, value = 0 } = {},
  {
    cols = GRAPH_COLUMN_COUNT,
    viewWidth = GRAPH_CHART_SIZE,
    viewHeight = GRAPH_CHART_SIZE,
    gutter = 0,
  } = {},
) {
  const colWidth = (viewWidth - 2 * gutter) / cols
  const x = gutter + ((2 * colIdx + 1) / 2) * colWidth
  let rowHeight = 0
  let y = 0

  if (startY && !endY) {
    rowHeight = (viewHeight - startY - 2 * gutter) / rows
  } else if (endY && !startY) {
    rowHeight = (viewHeight - endY - 2 * gutter) / rows
  } else if (startY && endY) {
    rowHeight = (endY - startY) / rows
  } else {
    rowHeight = (viewHeight - 2 * gutter) / rows
  }

  if (startY && !endY) {
    y = gutter + startY + ((2 * rowIdx + 1) / 2) * rowHeight
  } else if (endY && !startY) {
    y = gutter + ((2 * rowIdx + 1) / 2) * rowHeight
  } else if (startY && endY) {
    y = startY + ((2 * rowIdx + 1) / 2) * rowHeight
  } else {
    y = gutter + ((2 * rowIdx + 1) / 2) * rowHeight
  }

  return [x - 50, viewHeight - y, value]
}
