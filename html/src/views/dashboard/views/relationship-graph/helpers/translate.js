import Template from './template'
import TemplateData from './template-data'
import Task from './task'

// 国标
export function translateGbTemplateList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Template(item)
      .get([
        'id',
        'name',
        'ruleCount',
        'exchangeFrequency',
        'dataTotal',
        'reportStatus',
        'subjectCategory',
        'infoClassify',
        'dimension',
        'infoCategory',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateGbTemplateDetail (res = {}) {
  return new Template(res)
    .get([
      'id',
      'name',
      'identify',
      'exchangeFrequency',
      'infoClassify',
      'dimension',
      'infoCategory',
    ])
}

export function translateGbDataList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new TemplateData(item)
      .get([
        'id',
        'name',
        'identify',
        'status',
        'updateTimeFormat',
      ]))
    : []
  return {
    list: tempList,
    total,
  }
}

export function translateGbDataDetail (res = {}) {
  return new TemplateData(res)
    .get([
      'id',
      'name',
      'identify',
      'infoClassify',
      'infoCategory',
      'expirationDateFormat',
      'subjectCategory',
      'dimension',
      'itemsData',
      'status',
      'updateTime',
      'updateTimeFormat',
    ])
}

export function translateGbTaskList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Task(item)
      .get([
        'id',
        'crew',
        'total',
        'successesNumber',
        'failuresNumber',
        'status',
        'createTimeFormat',
      ]))
    : []
  return {
    list: tempList,
    total,
  }
}

// 本级
export function translateBjTemplateList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Template(item)
      .get([
        'id',
        'name',
        'ruleCount',
        'exchangeFrequency',
        'dataTotal',
        'reportStatus',
        'subjectCategory',
        'infoClassify',
        'dimension',
        'infoCategory',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateBjTemplateDetail (res = {}) {
  return new Template(res)
    .get([
      'id',
      'name',
      'identify',
      'exchangeFrequency',
      'infoClassify',
      'dimension',
      'infoCategory',
    ])
}

export function translateBjDataList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new TemplateData(item)
      .get([
        'id',
        'name',
        'identify',
        'status',
        'updateTimeFormat',
      ]))
    : []
  return {
    list: tempList,
    total,
  }
}

export function translateBjDataDetail (res = {}) {
  return new TemplateData(res)
    .get([
      'id',
      'name',
      'identify',
      'infoClassify',
      'infoCategory',
      'expirationDateFormat',
      'subjectCategory',
      'dimension',
      'itemsData',
      'status',
      'updateTime',
      'updateTimeFormat',
    ])
}

export function translateBjTaskList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Task(item)
      .get([
        'id',
        'crew',
        'total',
        'successesNumber',
        'failuresNumber',
        'status',
        'createTimeFormat',
      ]))
    : []
  return {
    list: tempList,
    total,
  }
}

// 委办局
export function translateWbjTemplateList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Template(item)
      .get([
        'id',
        'name',
        'exchangeFrequency',
        'dataTotal',
        'reportStatus',
        'subjectCategory',
        'infoClassify',
        'dimension',
        'infoCategory',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateWbjTemplateDetail (res = {}) {
  return new Template(res)
    .get([
      'id',
      'name',
      'identify',
      'exchangeFrequency',
      'infoClassify',
      'dimension',
      'infoCategory',
    ])
}

export function translateWbjDataList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new TemplateData(item)
      .get([
        'id',
        'name',
        'identify',
        'status',
        'updateTimeFormat',
      ]))
    : []
  return {
    list: tempList,
    total,
  }
}

export function translateWbjDataDetail (res = {}) {
  return new TemplateData(res)
    .get([
      'id',
      'name',
      'identify',
      'infoClassify',
      'infoCategory',
      'sourceUnit',
      'expirationDateFormat',
      'subjectCategory',
      'dimension',
      'itemsData',
      'status',
      'updateTime',
      'updateTimeFormat',
    ])
}

export function translateWbjTaskList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Task(item)
      .get([
        'id',
        'crew',
        'total',
        'successesNumber',
        'failuresNumber',
        'status',
        'createTimeFormat',
      ]))
    : []
  return {
    list: tempList,
    total,
  }
}
