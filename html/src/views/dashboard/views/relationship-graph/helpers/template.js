import Base from './base'

class Template extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      name,
      ruleCount,
      identify,
      exchangeFrequency = {},
      dataTotal,
      reportStatus = {},
      subjectCategory = [],
      infoClassify = {},
      dimension = {},
      infoCategory = {},
      updateTime,
      updateTimeFormat,
      createTime,
      createTimeFormat,
    } = params

    this.id = id
    this.name = name
    this.identify = identify
    this.ruleCount = ruleCount
    this.exchangeFrequency = exchangeFrequency.name
    this.dataTotal = dataTotal
    this.reportStatus = reportStatus.name
    this.subjectCategory = subjectCategory
    this.infoClassify = infoClassify
    this.dimension = dimension
    this.infoCategory = infoCategory
    this.updateTime = updateTime
    this.updateTimeFormat = updateTimeFormat
    this.createTime = createTime
    this.createTimeFormat = createTimeFormat
  }
}

export default Template
