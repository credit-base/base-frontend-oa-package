import Base from './base'

class TemplateData extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      name,
      identify,
      infoClassify = {},
      infoCategory = {},
      expirationDateFormat,
      subjectCategory = {},
      dimension = {},
      status = {},
      sourceUnit = {},
      itemsData = [],
      updateTime,
      updateTimeFormat,
      createTime,
      createTimeFormat,
    } = params

    this.id = id
    this.name = name
    this.identify = identify
    this.infoClassify = infoClassify
    this.infoCategory = infoCategory
    this.expirationDateFormat = expirationDateFormat
    this.subjectCategory = subjectCategory
    this.dimension = dimension
    this.status = status
    this.sourceUnit = sourceUnit.name
    this.itemsData = itemsData
    this.updateTime = updateTime
    this.updateTimeFormat = updateTimeFormat
    this.createTime = createTime
    this.createTimeFormat = createTimeFormat
  }
}

export default TemplateData
