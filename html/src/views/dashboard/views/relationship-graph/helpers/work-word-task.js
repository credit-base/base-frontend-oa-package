import Base from '@/models/base'

class WorkOrderTask extends Base {
  constructor (params = {}) {
    super(params)

    const {
      id,
      title,
      assignObject = {},
      status = {},
      template = {},
      templateType = {},
      parentTask = {},
      reason,
      createTime,
      updateTime,
      statusTime,
      endTime,

      schedule,
      feedbackRecords = [],
      compareResult = [],
      assignedTo = {},
    } = params

    this.id = id
    this.title = title
    this.assignObject = assignObject
    this.status = status
    this.template = template
    this.templateType = templateType
    this.parentTask = parentTask
    this.reason = reason
    this.createTime = createTime
    this.updateTime = updateTime
    this.statusTime = statusTime
    this.endTime = endTime

    this.feedbackRecords = feedbackRecords
    this.compareResult = compareResult
    this.schedule = schedule
    this.assignedTo = assignedTo
  }
}

export default WorkOrderTask
