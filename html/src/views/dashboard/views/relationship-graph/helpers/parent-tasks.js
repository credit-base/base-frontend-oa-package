import Base from '@/models/base'

class ParentTasks extends Base {
  constructor (params = {}) {
    super(params)

    const {
      id,
      title,
      templateType = {},
      ratio,
      status = {},
      template = {},
      createTime,
      endTime,
      updateTime,
    } = params

    this.id = id
    this.title = title
    this.templateType = templateType
    this.ratio = ratio
    this.status = status
    this.template = template
    this.createTime = createTime
    this.endTime = endTime
    this.updateTime = updateTime
  }
}

export default ParentTasks
