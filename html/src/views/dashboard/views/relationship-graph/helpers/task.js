import Base from './base'

class Task extends Base {
  constructor (params = {}) {
    super(params)

    const {
      id,
      crew = {},
      sourceUnit = {},
      wbjTemplate = {},
      total,
      successesNumber,
      failuresNumber,
      conversionRate,
      status = {},
      updateTime,
      updateTimeFormat,
      createTime,
      createTimeFormat,
    } = params

    this.id = id
    this.crew = crew.name
    this.sourceUnit = sourceUnit
    this.wbjTemplate = wbjTemplate
    this.total = total
    this.successesNumber = successesNumber
    this.failuresNumber = failuresNumber
    this.conversionRate = conversionRate
    this.status = status
    this.updateTime = updateTime
    this.updateTimeFormat = updateTimeFormat
    this.createTime = createTime
    this.createTimeFormat = createTimeFormat
  }
}

export default Task
