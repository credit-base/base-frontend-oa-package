import settings from '@/settings'

// 信用地图数据
// 用于 驾驶舱 - 信用地图
export const CREDIT_DATA = {
  dishonestPerson: [
    {
      id: 1,
      name: '尹文竹',
      cardID: '360521197310017020',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '恶意欠费的个人信息',
    },
    {
      id: 2,
      name: '夏云剑',
      cardID: '360313196212080039',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '行政处罚公示信息',
    },
    {
      id: 3,
      name: '肖 柳',
      cardID: '360122198002160019',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '行政处罚公示信息',
    },
    {
      id: 4,
      name: '张敏萍',
      cardID: '360312197410082017',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '行政处罚公示信息',
    },
    {
      id: 5,
      name: '吴彩晚',
      cardID: '360311197503061649',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '行政处罚公示信息',
    },
    {
      id: 6,
      name: '徐雪芳',
      cardID: '360302196512080541',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '行政处罚公示信息',
    },
    {
      id: 7,
      name: '汤道云',
      cardID: '360311197801252574',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '行政处罚公示信息',
    },
    {
      id: 8,
      name: '刘爱芬',
      cardID: '360312197501104322',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '行政处罚公示信息',
    },
    {
      id: 9,
      name: '廖志群',
      cardID: '360302197607214527',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '行政处罚公示信息',
    },
    {
      id: 10,
      name: '张发明',
      cardID: '360311196212230014',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '行政处罚公示信息',
    },
  ],
  honestPerson: [
    {
      id: 11,
      name: '陈华群',
      cardID: '511023198102274128',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '好人榜信息',
    },
    {
      id: 12,
      name: '尹文竹',
      cardID: '360521197310017020',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '好人榜信息',
    },
    {
      id: 13,
      name: '张发明',
      cardID: '360311196212230014',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '好人榜信息',
    },
    {
      id: 14,
      name: '廖志群',
      cardID: '360302197607214527',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '好人榜信息',
    },
    {
      id: 15,
      name: '刘爱芬',
      cardID: '360312197501104322',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '好人榜信息',
    },
    {
      id: 16,
      name: '徐雪芳',
      cardID: '360302196512080541',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '好人榜信息',
    },
    {
      id: 17,
      name: '汤道云',
      cardID: '360311197801252574',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '好人榜信息',
    },
    {
      id: 18,
      name: '刘爱芬',
      cardID: '360312197501104322',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '好人榜信息',
    },
    {
      id: 19,
      name: '廖志群',
      cardID: '360302197607214527',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '好人榜信息',
    },
    {
      id: 20,
      name: '肖 柳',
      cardID: '360122198002160019',
      status: false,
      address: settings.mapNameKeyword,
      typeName: '好人榜信息',
    },
  ],
}

// 地区地图数据
// 用于 驾驶舱 - 信用大数据 - 地图
export const REGION_MAP_DATA = [
  { name: settings.REGION_NAME, value: [...settings.REGION_NAME_COORDINATE, 0] },
]

export const cityMockData = [
  {
    name: '安源区',
    value: [113.877444, 27.621308, 0],
  },
  {
    name: '经开区',
    value: [113.955044, 27.655826, 0],
  },
  {
    name: '莲花县',
    value: [113.968541, 27.133462, 0],
  },
  {
    name: '芦溪县',
    value: [114.036516, 27.636604, 0],
  },
  {
    name: '上栗县',
    value: [113.801537, 27.886415, 0],
  },
  {
    name: '湘东区',
    value: [113.740497, 27.645875, 0],
  },
]

// 信用排名数据
// 用于驾驶舱 - 信用大数据 - 信用排名
export const RANK_DATA = settings.RANK_DATA

export const interfaceStatistics = [
  {
    id: 1,
    name: '委办局API',
    status: true,
    grade: 3,
    requestsTotal: 720,
  },
  {
    id: 2,
    name: '资源目录模板API',
    status: true,
    grade: 2,
    requestsTotal: 620,
  },
  {
    id: 3,
    name: '资源目录数据API',
    status: true,
    grade: 1,
    requestsTotal: 37,
  },
  {
    id: 4,
    name: '联合奖惩措施API',
    status: true,
    grade: 1,
    requestsTotal: 620,
  },
  {
    id: 5,
    name: '员工API',
    status: true,
    grade: 1,
    requestsTotal: 145,
  },
  {
    id: 6,
    name: '联合奖惩条目API',
    status: true,
    grade: 1,
    requestsTotal: 127,
  },
  {
    id: 7,
    name: '企业API',
    status: true,
    grade: 1,
    requestsTotal: 15,
  },
  {
    id: 8,
    name: '员工API',
    status: true,
    grade: 1,
    requestsTotal: 203,
  },
  {
    id: 9,
    name: '信用评分等级API',
    status: true,
    grade: 1,
    requestsTotal: 125,
  },
  {
    id: 10,
    name: '信用评分API',
    status: true,
    grade: 1,
    requestsTotal: 68,
  },
  {
    id: 11,
    name: '联合奖惩案例API',
    status: true,
    grade: 1,
    requestsTotal: 99,
  },
]

export const userGroupList = [
  {
    id: 'MC0y',
    name: '陕西省人民防空办公室',
    shortName: '省人防办',
  },
  {
    id: 'MC0x',
    name: '陕西省邮政管理局',
    shortName: '省邮政',
  },
  {
    id: 'MC0w',
    name: '陕西省房管局',
    shortName: '房管局',
  },
  {
    id: 'MC0p',
    name: '陕西省消防救援支队',
    shortName: '消防',
  },
  {
    id: 'MCws',
    name: '陕西省总工会',
    shortName: '省工会',
  },
  {
    id: 'MCsv',
    name: '陕西省妇女联合会',
    shortName: '省妇联',
  },
  {
    id: 'ODQ',
    name: '陕西省公积金管理中心',
    shortName: '省公积',
  },
  {
    id: 'OC0',
    name: '陕西海关',
    shortName: '省海关',
  },
  {
    id: 'NzA',
    name: '陕西省退役军人事务局',
    shortName: '军人局',
  },
  {
    id: 'NjM',
    name: '陕西省扶贫办公室',
    shortName: '扶贫办',
  },
  {
    id: 'Niw',
    name: '陕西省医疗保障局',
    shortName: '医保局',
  },
  {
    id: 'NS8',
    name: '陕西水务公司',
    shortName: '水务',
  },
  {
    id: 'NDI',
    name: '陕西省审计局',
    shortName: '审计局',
  },
  {
    id: 'NCs',
    name: '陕西省政府金融办',
    shortName: '金融办',
  },
  {
    id: 'MzE',
    name: '陕西省科学技术局',
    shortName: '科技局',
  },
  {
    id: 'MzA',
    name: '陕西省档案局',
    shortName: '档案局',
  },
  {
    id: 'My8',
    name: '陕西省体育局',
    shortName: '体育局',
  },
  {
    id: 'My4',
    name: '陕西省机构编制委员会办公室',
    shortName: '省编办',
  },
  {
    id: 'My0',
    name: '中国联通陕西分公司',
    shortName: '联通',
  },
  {
    id: 'Myw',
    name: '陕西省城省管理局',
    shortName: '城管局',
  },
  {
    id: 'Mys',
    name: '中国电信陕西分公司',
    shortName: '电信',
  },
  {
    id: 'MjQ',
    name: '陕西省公共政务管理局',
    shortName: '政务局',
  },
  {
    id: 'MjM',
    name: '陕西省银监分局',
    shortName: '银监局',
  },
  {
    id: 'MjI',
    name: '陕西火车站、陕西北站',
    shortName: '北站',
  },
  {
    id: 'MjE',
    name: '中国移动陕西分公司',
    shortName: '移动',
  },
  {
    id: 'MjA',
    name: '陕西省应急管理局',
    shortName: '应急局',
  },
  {
    id: 'Mi8',
    name: '陕西省省场监管管理局',
    shortName: '省监局',
  },
  {
    id: 'Mi4',
    name: '陕西省统计局',
    shortName: '统计局',
  },
  {
    id: 'Mi0',
    name: '陕西省地方税务局',
    shortName: '税务局',
  },
  {
    id: 'Miw',
    name: '陕西省税务局',
    shortName: '税务局',
  },
  {
    id: 'Mis',
    name: '陕西省卫生健康委员会',
    shortName: '卫康委',
  },
  {
    id: 'MTQ',
    name: '陕西省自然资源和规化局',
    shortName: '国土局',
  },
  {
    id: 'MTM',
    name: '陕西省文化广电新闻出版旅游局',
    shortName: '文广新',
  },
  {
    id: 'MTI',
    name: '陕西省农业农村局',
    shortName: '农业局',
  },
  {
    id: 'MTE',
    name: '陕西省民政局',
    shortName: '民政局',
  },
  {
    id: 'MTA',
    name: '陕西省商务局',
    shortName: '商务局',
  },
  {
    id: 'MS8',
    name: '陕西省生态环境局',
    shortName: '生态局',
  },
  {
    id: 'MS4',
    name: '陕西省房地产管理局',
    shortName: '房管局',
  },
  {
    id: 'MS0',
    name: '陕西省旅游发展委员会',
    shortName: '旅发委',
  },
  {
    id: 'MSw',
    name: '陕西省交通运输局',
    shortName: '交通局',
  },
  {
    id: 'MSs',
    name: '陕西省林业局',
    shortName: '林业局',
  },
  {
    id: 'MDQ',
    name: '国网江西省电力公司陕西供电分公司',
    shortName: '供电局',
  },
  {
    id: 'MDM',
    name: '陕西省水利局',
    shortName: '水利局',
  },
  {
    id: 'MDI',
    name: '陕西省燃气有限公司',
    shortName: '省燃气',
  },
  {
    id: 'MDE',
    name: '陕西省规划局',
    shortName: '规划局',
  },
  {
    id: 'MDA',
    name: '陕西省建设局',
    shortName: '建设局',
  },
  {
    id: 'MC8',
    name: '陕西省人力资源和社会保障局',
    shortName: '人社局',
  },
  {
    id: 'MC4',
    name: '陕西省司法局',
    shortName: '司法局',
  },
  {
    id: 'MC0',
    name: '陕西省教育局',
    shortName: '教育局',
  },
  {
    id: 'MCw',
    name: '陕西省国有资产监督管理委员会',
    shortName: '国资委',
  },
  {
    id: 'MCs',
    name: '陕西省工业和信息化局',
    shortName: '工信局',
  },
  {
    id: 'OA',
    name: '陕西省财政局',
    shortName: '财政局',
  },
  {
    id: 'Nw',
    name: '陕西省公安局',
    shortName: '公安局',
  },
  {
    id: 'Ng',
    name: '陕西省粮食局',
    shortName: '粮食局',
  },
  {
    id: 'NQ',
    name: '陕西省中级人民法院',
    shortName: '省中院',
  },
  {
    id: 'NA',
    name: '陕西省气象局',
    shortName: '气象局',
  },
  {
    id: 'Mw',
    name: '人民银行陕西省中心支行',
    shortName: '人行',
  },
  {
    id: 'Mg',
    name: '陕西省文明办',
    shortName: '文明办',
  },
  {
    id: 'MQ',
    name: '共青团陕西省委',
    shortName: '团省委',
  },
  {
    id: 'MA',
    name: '陕西省发展和改革委员会',
    shortName: '发改委',
  },
]
