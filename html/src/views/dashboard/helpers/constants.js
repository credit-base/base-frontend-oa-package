/**
 * 常量 - 驾驶舱
 */

export const SPECIAL_MONITORS = [
  { category: 'MQ', label: '自然人' },
  { category: 'MA', label: '企业' },
]

export const ADMIN_TABS = [
  { label: '信用大数据分析系统', icon: 'tabs-analysis', name: 'CreditBigData', alwaysVisible: true },
  { label: '信用地图', icon: 'tabs-map', name: 'CreditMap', requireNetwork: true },
  { label: '平台运行情况', icon: 'tabs-platform', name: 'PlatformStatistics' },
  { label: '系统数据总览', icon: 'tabs-overview', name: 'SystemSummary' },
  // { label: '企业行业信用监测评价', icon: 'tabs-monitor', name: 'EnterpriseIndustry' },
  // { label: '委办局数据统计分析', icon: 'tabs-static', name: 'OfficeAnalysisList' },
  { label: '接口使用统计', icon: 'tabs-interface', name: 'InterfaceStatistics' },
  { label: '关系图谱', icon: 'relationship-graph', name: 'RelationshipGraph' },
]

export const COMMON_CHART_COMMANDS = [{ label: '详情', value: 'view' }]
