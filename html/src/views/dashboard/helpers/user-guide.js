const steps = [
  {
    element: '.hamburger-drag',
    popover: {
      title: '侧导航开关',
      description: '打开或者关闭侧导航',
      position: 'bottom',
    },
  },
  {
    element: '.avatar-container',
    popover: {
      title: '登录员工信息',
      description: '登录员工信息，包含首页、个人中心、待办事项、退出功能',
      position: 'left',
    },
  },
  {
    element: '.theme-setting',
    popover: {
      title: '主题切换',
      description: '可以设置主题风格，有红色、绿色、和黄色',
      position: 'left',
    },
  },
  {
    element: '.refresh',
    popover: {
      title: '清除缓存',
      description: '清除缓存功能',
      position: 'left',
    },
  },
  {
    element: '.screenfull',
    popover: {
      title: '全屏切换',
      description: '将操作界面全屏切换',
      position: 'left',
    },
  },
  {
    element: '.data-chart',
    popover: {
      title: '信用大数据分析系统',
      description: '这里是展示平台整体统计数据',
      position: 'bottom',
    },
  },
  {
    element: '.data-map',
    popover: {
      title: '数据地图',
      description: '数据地图将呈现最新',
      position: 'bottom',
    },
  },
  {
    element: '.data-creditDataAnalysis',
    popover: {
      title: '平台运行情况',
      description: '监控服务器运行情况',
      position: 'bottom',
    },
  },
  {
    element: '.data-overview',
    popover: {
      title: '系统数据总览',
      description: '查看地市、委办局数据流向',
      position: 'bottom',
    },
  },
  {
    element: '.setting-chart-data',
    popover: {
      title: '数据配置',
      description: '开启配置统计图的数据项',
      position: 'left',
    },
  },
  {
    element: '.export-data-menu',
    popover: {
      title: '导出数据',
      description: '根据不同参数，导出不同数据报表',
      position: 'left',
    },
  },
  {
    element: '.fullscreen-menu',
    popover: {
      title: '驾驶舱全屏',
      description: '驾驶舱投屏',
      position: 'left',
    },
  },
]

export default steps
