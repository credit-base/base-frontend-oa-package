import Snap from 'snapsvg-cjs'

let snapData

const bumenList = {
  NQ: 'M440,188V139',
  MTI: 'M443,239V158h34V64',
  NDI: 'M446,241V161h66V139',
  MTE: 'M449,245V164H550V64',
  NCs: 'M452,248V167H587V139',
  MS8: 'M455,250V169l168,1V65',
  MA: 'M458,250V174H661V139',
  MS4: 'M461,250V176H703V65',
  MSs: 'M443,238H713V100h99',
  MzE: 'M446,241H715V137h22',
  MDQ: 'M449,244H717V175h94',
  MzA: 'M452,247H720V212h16',
  MDM: 'M450,250H812',
  My8: 'M452,253H720v35h16',
  MDI: 'M449,256H717v69h94',
  My4: 'M446,259H715V363h22',
  MDE: 'M443,262H713V400h99',
  MC0: 'M442,261v81h34v94',
  Mys: 'M445,259v80h66v22',
  MC4: 'M448,255v81H549V436',
  Myw: 'M451,252v81H586v28',
  MC8: 'M454,250v81l168-1V435',
  My0: 'M457,250v76H660v35',
  MDA: 'M460,250v74H702V435',
  MjQ: 'M440,362V313',
  MCw: 'M438,261v81H404v94',
  MjM: 'M435,259v80H369v22',
  MCs: 'M432,255v81H331V436',
  MjI: 'M429,252v81H294v28',
  OA: 'M426,250v81l-168-1V435',
  MjE: 'M423,250v76H220v35',
  Nw: 'M420,250v74H178V435',
  Ng: 'M440,262H170V400H71',
  MjA: 'M437,259H168V363H146',
  NA: 'M434,256H166v69H72',
  Mi8: 'M431,253H163v35H147',
  Mw: 'M70,250H432',
  MQ: 'M440,238H170V100H71',
  Mi0: 'M437,241H168V137H146',
  Mg: 'M434,244H166V175H72',
  MTM: 'M437,239V158H403V64',
  MTA: 'M434,241V161H368V139',
  MTQ: 'M431,245V164H330V64',
  MSw: 'M428,248V167H293V139',
  Mis: 'M425,250V169l-168,1V65',
  MS0: 'M422,250V174H219V139',
  Miw: 'M419,250V176H177V65',
}

let pathSvg = []
let intervalHandle = null

export default function (id, time) {
  const argTime = time || 2000
  snapData = Snap(id)

  for (const path of pathSvg) {
    // eslint-disable-next-line no-prototype-builtins
    if (pathSvg.hasOwnProperty(path)) path.remove()
  }

  pathSvg = []
  for (const key in bumenList) {
    // eslint-disable-next-line no-prototype-builtins
    if (bumenList.hasOwnProperty(key)) {
      const path = bumenList[key]
      pathSvg.push(
        snapData.paper.path(path).attr({
          stroke: 'transparent',
          strokeWidth: 1,
          fill: 'none',
        }),
      )
    }
  }
  if (intervalHandle) {
    window.clearInterval(intervalHandle)
  }

  intervalHandle = window.setInterval(function () {
    const rand = Math.floor(Math.random() * pathSvg.length)

    const path = pathSvg[rand]
    const pathLen = path.getTotalLength()
    // console.log(pathLen)
    const signal = getSnapSingle()

    const baseSpeed = 10
    const speed = baseSpeed * pathLen

    let from = 0
    let to = 0
    let alpha = 0
    if (Math.floor(Math.random() * 2) === 0) {
      from = pathLen
      to = 10
    } else {
      from = 0
      to = pathLen - 10
      alpha = 180
    }

    Snap.animate(
      from,
      to,
      function (value) {
        // var movePoint = Snap.path.getPointAtLength(path1, value);
        const movePoint = path.getPointAtLength(value)
        // CircleA.attr({ cx: movePoint.x, cy: movePoint.y });
        // console.log(value)
        // console.log(movePoint)
        const m = new Snap.Matrix()
        m.translate(movePoint.x, movePoint.y)
        m.rotate(movePoint.alpha + alpha) // 使飞机总是朝着曲线方向。point.alpha：点的切线和水平线形成的夹角
        signal.transform(m)
      },
      speed,
      window.mina.bounce(),
      function () {
        signal.remove()
        // console.log('animation end');
      },
    )
  }, argTime)
}

function getSnapSingle () {
  return snapData.paper.image('/static/images/icon-dot.png', -34, -11, 68, 22)
}
