/**
 * 全局混入
 *
 * @module mixins
 */

import Vue from 'vue'

import loading from './loading'

Vue.mixin(loading)
