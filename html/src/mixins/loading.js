/**
 * 混入 - loading
 *
 * @module mixins/loading
 */

export default {
  data () {
    return {
      $_isLoading: false,
    }
  },

  methods: {
    $_showLoading () {
      this.$_isLoading = true
      return this.$_isLoading
    },

    $_hideLoading () {
      this.$_isLoading = false
      return this.$_isLoading
    },
  },
}
