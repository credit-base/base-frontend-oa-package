/**
 * @file 用户角色混入
 * @module mixins/user-role
 */

import { CONSTANTS } from '@/plugins/constants'

const { PURVIEW = {} } = CONSTANTS

export default {
  computed: {
    category () {
      return this.$store.getters.category || {}
    },

    userGroup () {
      return this.$store.getters.userGroup || {}
    },

    isSuperAdmin () {
      return this.category.id === PURVIEW.SUPER_ADMIN
    },

    isPlatformAdmin () {
      return this.category.id === PURVIEW.PLATFORM_ADMIN
    },

    isWbjAdmin () {
      return this.category.id === PURVIEW.USER_GROUP_ADMIN
    },

    isOperateUser () {
      return this.category.id === PURVIEW.OPERATION_CREW
    },
  },
}
