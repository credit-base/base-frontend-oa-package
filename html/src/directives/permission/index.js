/**
 * 员工指令
 */

import * as permissions from '@/constants/permissions'
import permission from './permission'

function install (Vue) {
  Vue.directive('permission', permission)

  // eslint-disable-next-line no-prototype-builtins
  if (Vue.prototype.hasOwnProperty('$permissions')) return

  Object.defineProperties(Vue.prototype, {
    $permissions: { get: () => permissions },
  })
}

permission.install = install

export default permission
