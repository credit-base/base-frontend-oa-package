/**
 * 员工指令
 */

import store from '@/store'

function checkIfPermission (el, binding) {
  const { value: permissionsRequired } = binding
  const { permissions: permissionsOwned = [] } = store.getters

  if (Array.isArray(permissionsRequired) && permissionsRequired.length) {
    const hasPermission = permissionsOwned
      .some(permission => permissionsRequired.includes(permission))

    if (!hasPermission) {
      el.parentNode && el.parentNode.removeChild(el)
    }
  } else {
    throw new Error(`Permissions required! Like v-permission="[$permissions.]"`)
  }
}

export default {
  inserted (el, binding) {
    checkIfPermission(el, binding)
  },

  updated (el, binding) {
    checkIfPermission(el, binding)
  },
}
