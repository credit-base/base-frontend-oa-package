/**
 * @file 全局指令
 * @module directives/index
 */

import Vue from 'vue'

// 自定义指令
import permission from './permission'

Vue.use(permission)
