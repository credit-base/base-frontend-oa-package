/**
 * @file 新闻 - 路由
 * @module async-routes/credit-photography
 */

import Layout from '@/layout'
import permissions from '@/constants/permissions'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const { STATUS_CREDIT_PHOTOGRAPHY } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/credit-photography',
    name: 'CreditPhotography',
    component: Layout,
    alwaysShow: true,
    meta: {
      title: 'creditPhotography.ROUTER.CREDIT_PHOTOGRAPHY',
      icon: 'credit-photography',
      breadcrumb: false,
      isVisible: STATUS_CREDIT_PHOTOGRAPHY,
    },
    children: [
      {
        path: '/credit-photography/list',
        name: 'CreditPhotographyList',
        component: () => import('@/app-modules/credit-photography/views/list'),
        meta: {
          title: 'creditPhotography.ROUTER.CREDIT_PHOTOGRAPHY_LIST',
          icon: 'credit-photography',
          routerId: permissions.CREDIT_PHOTOGRAPHY_ID,
        },
      },
      {
        path: '/credit-photography/audit-detail/:id',
        name: 'CreditPhotographyAuditDetail',
        component: () => import('@/app-modules/credit-photography/views/detail/audit-detail'),
        hidden: true,
        meta: {
          title: 'creditPhotography.ROUTER.CREDIT_PHOTOGRAPHY_DETAIL',
          icon: 'credit-photography',
          activeMenu: '/credit-photography/list',
        },
      },
      {
        path: '/credit-photography/official-detail/:id',
        name: 'CreditPhotographyOfficialDetail',
        component: () => import('@/app-modules/credit-photography/views/detail/official-detail'),
        hidden: true,
        meta: {
          title: 'creditPhotography.ROUTER.CREDIT_PHOTOGRAPHY_DETAIL',
          icon: 'credit-photography',
          activeMenu: '/credit-photography/list',
        },
      },
    ],
  },
]
