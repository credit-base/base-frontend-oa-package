import { CONSTANTS } from '@/plugins/constants'

const {
  APPLY_STATUS = {},
} = CONSTANTS

export const CREDIT_PHOTOGRAPHY_TABS = [
  { name: 'official-credit-photography', label: 'OFFICIAL_CREDIT_PHOTOGRAPHY', component: 'OfficialCreditPhotography' },
  { name: 'audit-credit-photography', label: 'AUDIT_CREDIT_PHOTOGRAPHY', component: 'AuditCreditPhotography' },
]

export const APPLY_STATUS_ENUM = [
  {
    text: '待审核',
    value: APPLY_STATUS.PADDING,
  },
  {
    text: '已驳回',
    value: APPLY_STATUS.REJECT,
  },
]

export const COVER_SIZE = {
  width: 320,
  height: 200,
}
