import Base from '@/models/base'

class CreditPhotography extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      member = {},
      description,
      status = {},
      attachments = [],
      applyCrew = {},
      applyStatus = {},
      rejectReason,
      updateTimeFormat,
    } = params

    this.id = id
    this.member = member.realName
    this.description = description
    this.status = status
    this.attachments = attachments
    this.applyCrew = applyCrew.realName
    this.applyStatus = applyStatus
    this.rejectReason = rejectReason
    this.updateTimeFormat = updateTimeFormat
  }
}

export default CreditPhotography
