import CreditPhotography from './credit-photography'

export function translateOfficialCreditPhotographyList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new CreditPhotography(item)
      .get([
        'id',
        'description',
        'member',
        'status',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateOfficialCreditPhotographyDetail (res = {}) {
  return new CreditPhotography(res)
    .get([
      'id',
      'member',
      'description',
      'status',
      'attachments',
      'applyStatus',
      'applyCrew',
      'updateTimeFormat',
    ])
}

export function translateAuditCreditPhotographyList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new CreditPhotography(item)
      .get([
        'id',
        'description',
        'member',
        'status',
        'applyStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateAuditCreditPhotographyDetail (res = {}) {
  return new CreditPhotography(res)
    .get([
      'id',
      'member',
      'description',
      'status',
      'attachments',
      'applyStatus',
      'applyCrew',
      'rejectReason',
      'updateTimeFormat',
    ])
}
