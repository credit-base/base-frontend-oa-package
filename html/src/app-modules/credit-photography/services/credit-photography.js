import request from '@/utils/request'

import {
  CREDIT_PHOTOGRAPHY_LIST,
  CREDIT_PHOTOGRAPHY_VIDEO_DETAIL,
  // CREDIT_PHOTOGRAPHY_IMAGE_DETAIL,
} from '@/app-modules/credit-photography/helpers/mocks'

/**
 * 1. 获取信随手拍列表
 * @param {limit,page,realName,sort,applyStatus,scene} params
 * @returns
 */
export function fetchCreditPhotographyList (params = {}) {
  if (params.useMock) {
    return {
      total: 4,
      list: [
        ...CREDIT_PHOTOGRAPHY_LIST,
      ],
    }
  }
  return request('/api/creditPhotography', { method: 'GET', params })
}

/**
 * 2. 获取信用随手拍详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function fetchCreditPhotographyDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...CREDIT_PHOTOGRAPHY_VIDEO_DETAIL,
    }
  }
  return request(`/api/creditPhotography/${id}`, { method: 'GET' })
}

/**
 * 4. 审核通过
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function updateApproveCreditPhotography (id) {
  return request(`/api/creditPhotography/${id}/approve`, { method: 'POST' })
}

/**
 * 5. 审核驳回
 * @param {*} id
 * @param { rejectReason } params
 * @returns
 */
export function updateRejectCreditPhotography (id, params = {}) {
  return request(`/api/creditPhotography/${id}/reject`, { method: 'POST', params })
}
