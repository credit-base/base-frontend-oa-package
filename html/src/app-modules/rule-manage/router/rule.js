/**
 * @file 规则管理 - 路由
 * @module async-routes/rule-manage/rule
 */
import permissions from '@/constants/permissions'

export default [
  // 规则管理
  // 正式表
  // ├── 规则列表
  {
    path: '/rule-manage/rules/release/list',
    name: 'RuleReleaseList',
    meta: {
      title: 'ruleManage.ROUTER.RULE_LIST',
      icon: 'rule',
      activeMenu: '/rule-manage/rules/release/list',
    },
    component: () => import('@/app-modules/rule-manage/views/release/rules-list'),
  },
  // ├── 规则详情
  {
    path: '/rule-manage/rules/release/detail/:id',
    name: 'RuleReleaseDetail',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.RULE_DETAIL',
      icon: 'rule',
      activeMenu: '/rule-manage/rules/release/list',
    },
    component: () => import('@/app-modules/rule-manage/views/release/rules-detail'),
  },
  // ├── 新增规则
  {
    path: '/rule-manage/rules/release/add',
    name: 'RuleReleaseAdd',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.RULE_ADD',
      icon: 'rule',
      activeMenu: '/rule-manage/rules/release/list',
    },
    component: () => import('@/app-modules/rule-manage/views/release/rules-add'),
  },
  // ├── 编辑规则
  {
    path: '/rule-manage/rules/release/edit/:id',
    name: 'RuleReleaseEdit',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.RULE_EDIT',
      icon: 'rule',
      activeMenu: '/rule-manage/rules/release/list',
    },
    component: () => import('@/app-modules/rule-manage/views/release/rules-edit'),
  },
  // 审核表
  // ├── 规则审核详情
  {
    path: '/rule-manage/rules/release/un-audited-detail/:id',
    name: 'UnAuditedRuleReleaseDetail',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_RULE_DETAIL',
      icon: 'rule',
      activeMenu: '/rule-manage/rules/release/list',
    },
    component: () => import('@/app-modules/rule-manage/views/release/un-audited-rules-detail'),
  },
  // ├── 规则重新编辑
  {
    path: '/rule-manage/rules/release/un-audited-edit/:id',
    name: 'UnAuditedRuleReleaseEdit',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_RULE_EDIT',
      icon: 'rule',
      activeMenu: '/rule-manage/rules/release/list',
    },
    component: () => import('@/app-modules/rule-manage/views/release/un-audited-rules-edit'),
  },
  // 规则审核
  // ├── 规则审核列表
  {
    path: '/rule-manage/rules/review/list',
    name: 'RuleReviewList',
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_RULE_LIST',
      icon: 'rule',
      routerId: permissions.UN_AUDITED_RULE_MANAGE_ID,
    },
    component: () => import('@/app-modules/rule-manage/views/review/un-audited-rules-list'),
  },
  // ├── 规则审核详情
  {
    path: '/rule-manage/rules/review/detail/:id',
    name: 'RuleReviewDetail',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_RULE_DETAIL',
      icon: 'rule',
      activeMenu: '/rule-manage/rules/review/list',
    },
    component: () => import('@/app-modules/rule-manage/views/review/un-audited-rules-detail'),
  },
]
