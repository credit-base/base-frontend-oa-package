/**
 * @file 本级规则管理 - 路由
 * @module async-routes/rule-manage/bj-rule
 */
import permissions from '@/constants/permissions'

export default [
  // 规则管理
  // 正式表
  // ├── 本级目录列表
  {
    path: '/rule-manage/release/bj-templates',
    name: 'RuleBjTemplates',
    meta: {
      title: 'ruleManage.ROUTER.BJ_TEMPLATES',
      icon: 'bj-rule',
      routerId: permissions.BJ_RULE_MANAGE_ID,
    },
    component: () => import('@/app-modules/rule-manage/views/release/bj-templates'),
  },
  // ├── 本级规则列表
  {
    path: '/rule-manage/bj-rules/release/list/:templateId',
    name: 'BjRuleReleaseList',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.BJ_RULE_LIST',
      icon: 'bj-rule',
      activeMenu: '/rule-manage/release/bj-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/bj-rules-list'),
  },
  // ├── 本级规则详情
  {
    path: '/rule-manage/bj-rules/release/detail/:id',
    name: 'BjRuleReleaseDetail',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.BJ_RULE_DETAIL',
      icon: 'bj-rule',
      activeMenu: '/rule-manage/release/bj-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/bj-rules-detail'),
  },
  // ├── 新增本级规则
  {
    path: '/rule-manage/bj-rules/release/add/:templateId',
    name: 'BjRuleReleaseAdd',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.BJ_RULE_ADD',
      icon: 'bj-rule',
      activeMenu: '/rule-manage/release/bj-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/bj-rules-add'),
  },
  // ├── 编辑本级规则
  {
    path: '/rule-manage/bj-rules/release/edit/:id',
    name: 'BjRuleReleaseEdit',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.BJ_RULE_EDIT',
      icon: 'bj-rule',
      activeMenu: '/rule-manage/release/bj-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/bj-rules-edit'),
  },
  // 审核表
  // ├── 本级规则审核详情
  {
    path: '/rule-manage/bj-rules/release/un-audited-detail/:id',
    name: 'UnAuditedBjRuleReleaseDetail',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_BJ_RULE_DETAIL',
      icon: 'bj-rule',
      activeMenu: '/rule-manage/release/bj-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/un-audited-bj-rules-detail'),
  },
  // ├── 本级规则重新编辑
  {
    path: '/rule-manage/bj-rules/release/un-audited-edit/:id',
    name: 'UnAuditedBjRuleReleaseEdit',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_BJ_RULE_EDIT',
      icon: 'bj-rule',
      activeMenu: '/rule-manage/release/bj-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/un-audited-bj-rules-edit'),
  },
  // 规则审核
  // ├── 本级规则审核列表
  {
    path: '/rule-manage/bj-rules/review/list',
    name: 'BjRuleReviewList',
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_BJ_RULE_LIST',
      icon: 'bj-rule',
      routerId: permissions.UN_AUDITED_BJ_RULE_MANAGE_ID,
    },
    component: () => import('@/app-modules/rule-manage/views/review/un-audited-bj-rules-list'),
  },
  // ├── 本级规则审核详情
  {
    path: '/rule-manage/bj-rules/review/detail/:id',
    name: 'BjRuleReviewDetail',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_BJ_RULE_DETAIL',
      icon: 'bj-rule',
      activeMenu: '/rule-manage/bj-rules/review/list',
    },
    component: () => import('@/app-modules/rule-manage/views/review/un-audited-bj-rules-detail'),
  },
]
