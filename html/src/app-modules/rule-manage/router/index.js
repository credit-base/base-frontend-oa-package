/**
 * @file 规则管理 - 路由
 * @module async-routes/rule-manage
 */

import rule from './rule'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const { STATUS_RULE_MANAGE_SUBSYSTEM } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/rule-manage-subsystem',
    name: 'RuleManageSubsystem',
    component: () => import('@/app-modules/rule-manage/views'),
    alwaysShow: true,
    meta: {
      title: 'ruleManage.ROUTER.RULE_MANAGE_SUBSYSTEM',
      // TODO: change icon
      icon: 'rule-manage-subsystem',
      breadcrumb: false,
      isVisible: STATUS_RULE_MANAGE_SUBSYSTEM,
    },
    children: [
      ...rule,
    ],
  },
]
