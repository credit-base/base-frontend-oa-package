/**
 * @file 国标规则管理 - 路由
 * @module async-routes/rule-manage/gb-rule
 */
import permissions from '@/constants/permissions'

export default [
  // 规则管理
  // 正式表
  // ├── 国标目录列表
  {
    path: '/rule-manage/release/gb-templates',
    name: 'RuleGbTemplates',
    meta: {
      title: 'ruleManage.ROUTER.GB_TEMPLATES',
      icon: 'gb-rule',
      routerId: permissions.GB_RULE_MANAGE_ID,
    },
    component: () => import('@/app-modules/rule-manage/views/release/gb-templates'),
  },
  // ├── 国标规则列表
  {
    path: '/rule-manage/gb-rules/release/list/:templateId',
    name: 'GbRuleReleaseList',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.GB_RULE_LIST',
      icon: 'gb-rule',
      activeMenu: '/rule-manage/release/gb-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/gb-rules-list'),
  },
  // ├── 国标规则详情
  {
    path: '/rule-manage/gb-rules/release/detail/:id',
    name: 'GbRuleReleaseDetail',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.GB_RULE_DETAIL',
      icon: 'gb-rule',
      activeMenu: '/rule-manage/release/gb-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/gb-rules-detail'),
  },
  // ├── 新增国标规则
  {
    path: '/rule-manage/gb-rules/release/add/:templateId',
    name: 'GbRuleReleaseAdd',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.GB_RULE_ADD',
      icon: 'gb-rule',
      activeMenu: '/rule-manage/release/gb-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/gb-rules-add'),
  },
  // ├── 编辑国标规则
  {
    path: '/rule-manage/gb-rules/release/edit/:id',
    name: 'GbRuleReleaseEdit',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.GB_RULE_EDIT',
      icon: 'gb-rule',
      activeMenu: '/rule-manage/release/gb-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/gb-rules-edit'),
  },
  // 审核表
  // ├── 国标规则审核详情
  {
    path: '/rule-manage/gb-rules/release/un-audited-detail/:id',
    name: 'UnAuditedGbRuleReleaseDetail',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_GB_RULE_DETAIL',
      icon: 'gb-rule',
      activeMenu: '/rule-manage/release/gb-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/un-audited-gb-rules-detail'),
  },
  // ├── 国标规则重新编辑
  {
    path: '/rule-manage/gb-rules/release/un-audited-edit/:id',
    name: 'UnAuditedGbRuleReleaseEdit',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_GB_RULE_EDIT',
      icon: 'gb-rule',
      activeMenu: '/rule-manage/release/gb-templates',
    },
    component: () => import('@/app-modules/rule-manage/views/release/un-audited-gb-rules-edit'),
  },
  // 规则审核
  // ├── 国标规则审核列表
  {
    path: '/rule-manage/gb-rules/review/list',
    name: 'GbRuleReviewList',
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_GB_RULE_LIST',
      icon: 'gb-rule',
      routerId: permissions.UN_AUDITED_GB_RULE_MANAGE_ID,
    },
    component: () => import('@/app-modules/rule-manage/views/review/un-audited-gb-rules-list'),
  },
  // ├── 国标规则审核详情
  {
    path: '/rule-manage/gb-rules/review/detail/:id',
    name: 'GbRuleReviewDetail',
    hidden: true,
    meta: {
      title: 'ruleManage.ROUTER.UN_AUDITED_GB_RULE_DETAIL',
      icon: 'gb-rule',
      activeMenu: '/rule-manage/gb-rules/review/list',
    },
    component: () => import('@/app-modules/rule-manage/views/review/un-audited-gb-rules-detail'),
  },
]
