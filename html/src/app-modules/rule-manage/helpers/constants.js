import { CONSTANTS } from '@/plugins/constants'

const { APPLY_STATUS = {} } = CONSTANTS
/**
 * @file 规则管理 - 常量
 * @module rule-manage/constants
 */

export const RULE_TABS = [
  { name: 'rule-list', label: 'RULE', component: 'Rules' },
  { name: 'rule-un-audited-list', label: 'RULE_UN_AUDITED', component: 'UnAuditedRules' },
]

export const GB_RULE_TABS = [
  { name: 'gb-rule-list', label: 'RULE', component: 'Rules' },
  { name: 'gb-rule-un-audited-list', label: 'RULE_UN_AUDITED', component: 'UnAuditedRules' },
]

export const BJ_RULE_TABS = [
  { name: 'bj-rule-list', label: 'RULE', component: 'Rules' },
  { name: 'bj-rule-un-audited-list', label: 'RULE_UN_AUDITED', component: 'UnAuditedRules' },
]

export const RULE_DETAIL_TABS = [
  { name: 'detail-view-business', label: 'RULE_BUSINESS', component: 'DetailViewBusiness' },
  { name: 'detail-view-content', label: 'RULE_DETAIL', component: 'DetailViewContent' },
  { name: 'detail-view-gb-record', label: 'RULE_RECORD', component: 'DetailViewRecord' },
]

export const GB_RULE_DETAIL_TABS = [
  { name: 'detail-view-business', label: 'RULE_BUSINESS', component: 'DetailViewBusiness' },
  { name: 'detail-view-content', label: 'RULE_DETAIL', component: 'DetailViewContent' },
  { name: 'detail-view-gb-record', label: 'RULE_RECORD', component: 'DetailViewGbRecord' },
]

export const BJ_RULE_DETAIL_TABS = [
  { name: 'detail-view-business', label: 'RULE_BUSINESS', component: 'DetailViewBusiness' },
  { name: 'detail-view-content', label: 'RULE_DETAIL', component: 'DetailViewContent' },
  { name: 'detail-view-bj-record', label: 'RULE_RECORD', component: 'DetailViewBjRecord' },
]

export const RULE_UN_AUDITED_DETAIL_TABS = [
  { name: 'detail-view-content', label: 'RULE_DETAIL', component: 'DetailViewContent' },
  { name: 'detail-view-un-audited-info', label: 'RULE_UN_AUDITED_INFO', component: 'DetailViewUnAuditedInfo' },
]

export const RULE_UN_AUDITED_GB_DETAIL_TABS = [
  { name: 'detail-view-content', label: 'RULE_DETAIL', component: 'DetailViewContent' },
  { name: 'detail-view-un-audited-info', label: 'RULE_UN_AUDITED_INFO', component: 'DetailViewGbUnAuditedInfo' },
]

export const RULE_UN_AUDITED_BJ_DETAIL_TABS = [
  { name: 'detail-view-content', label: 'RULE_DETAIL', component: 'DetailViewContent' },
  { name: 'detail-view-un-audited-info', label: 'RULE_UN_AUDITED_INFO', component: 'DetailViewBjUnAuditedInfo' },
]

export const RULES_STEPS_VALUE = {
  StepTransformationRule: {
    index: 0,
    name: 'StepTransformationRule',
  },
  StepCompletionRule: {
    index: 1,
    name: 'StepCompletionRule',
  },
  StepComparisonRule: {
    index: 2,
    name: 'StepComparisonRule',
  },
  StepDeDuplicationRule: {
    index: 3,
    name: 'StepDeDuplicationRule',
  },
  StepPreviewConfirm: {
    index: 4,
    name: 'StepPreviewConfirm',
  },
}

export const RULES_STEPS_CONFIG = [
  {
    title: 'STEP_TRANSFORMATION_RULE',
    index: 0,
    component: 'StepTransformationRule',
    icon: 'step-template-info',
  },
  {
    title: 'STEP_COMPLETION_RULE',
    index: 1,
    component: 'StepCompletionRule',
    icon: 'step-template-info',
  },
  {
    title: 'STEP_COMPARISON_RULE',
    index: 2,
    component: 'StepComparisonRule',
    icon: 'step-template-info',
  },
  {
    title: 'STEP_DE_DUPLICATION_RULE',
    index: 3,
    component: 'StepDeDuplicationRule',
    icon: 'step-template-info',
  },
  {
    title: 'STEP_PREVIEW_CONFIRM',
    index: 4,
    component: 'StepPreviewConfirm',
    icon: 'step-preview-confirm',
  },
]

export const DATA_TYPE_ENUM = {
  TYPE_STRING: 'MA', // 字符型
  TYPE_DATE: 'MQ', // 日期型
  TYPE_INTEGER: 'Mg', // 整数型
  TYPE_FLOAT: 'Mw', // 浮点型
  TYPE_ENUMERATED: 'NA', // 枚举型
  TYPE_COLLECTIVE: 'NQ', // 集合型
}

export const PUBLIC_SCOPE_ENUM = {
  OPEN_TO_THE_PUBLIC: 'MA', // 社会公开
  GOVERNMENT_AFFAIRS_SHARING: 'MQ', // 政务共享
  AUTHORIZATION_QUERY: 'Mg', // 授权查询
}

export const SUBJECT_CATEGORY = {
  LEGAL_PERSON: 'MA', // 法人及非法人组织
  NATURAL_PERSON: 'MQ', // 自然人
  INDIVIDUAL_BUSINESSES: 'Mg', // 个体工商户
}

export const BASIC_VALUES = {
  NAME: 'MA',
  CODE: 'MQ',
}

export const BASIS_OPTIONS = [
  {
    id: BASIC_VALUES.NAME,
    name: '企业名称',
    isDisabled: false,
  },
  {
    id: BASIC_VALUES.CODE,
    name: '统一社会信用代码/身份证号',
    isDisabled: false,
  },
]

/**
 * 国标资源目录必填项
 * 主体类别: subjectCategory
 * 公开范围: dimension
 * 更新频率: exchangeFrequency
 * 信息分类: infoClassify
 */

export const gbTemplateRequiredKey = [
  { key: 'exchangeFrequency', value: 'EXCHANGE_FREQUENCY' },
  { key: 'dimension', value: 'DIMENSION' },
  { key: 'infoClassify', value: 'INFO_CLASSIFY' },
  { key: 'subjectCategory', value: 'SUBJECT_CATEGORY' },
]

/**
 * 国标资源目录模板必填项
 * 数据类型 type DATA_TYPE
 * 数据长度 length DATA_LENGTH
 * 可选范围 options OPTIONAL_SCOPE
 * 公开范围 dimension DIMENSION
 * 是否必填 isNecessary IS_NECESSARY
 * 是否脱敏 isMasked IS_MASKED
 * 脱敏规则 maskRule MASK_RULE
 */
export const gbItemsRequiredKey = [
  { key: 'type', value: 'DATA_TYPE' },
  { key: 'length', value: 'DATA_LENGTH' },
  { key: 'options', value: 'OPTIONAL_SCOPE' },
  { key: 'dimension', value: 'DIMENSION' },
  { key: 'isNecessary', value: 'IS_NECESSARY' },
  { key: 'isMasked', value: 'IS_MASKED' },
  { key: 'maskRule', value: 'MASK_RULE' },
]

export const DE_DUPLICATION_RESULT_PROCESSING_OPTIONS = [
  {
    id: 'MA',
    name: 'THROW_AWAY',
    isDisabled: false,
  },
  {
    id: 'MQ',
    name: 'COVER',
    isDisabled: true,
  },
]

export const IS_NECESSARY_VALUE = 'MA'

export const IDENTIFY_VALUES = {
  NAME_IDENTIFY: 'ZTMC',
  UNIFIED_SOCIAL_CREDIT_CODE: 'TYSHXYDM',
  IDENTITY_NUMBER: 'ZJHM',
}

export const COMPLEMENTARY_BASIS_OPTIONS = [
  {
    id: 'MA',
    requiredIdentifies: [IDENTIFY_VALUES.NAME_IDENTIFY],
  },
  {
    id: 'MQ',
    requiredIdentifies: [IDENTIFY_VALUES.UNIFIED_SOCIAL_CREDIT_CODE, IDENTIFY_VALUES.IDENTITY_NUMBER],
  },
]

export const APPLY_STATUS_ENUM = [
  {
    text: '待审核',
    value: APPLY_STATUS.PADDING,
  },
  {
    text: '已驳回',
    value: APPLY_STATUS.REJECT,
  },
]

export const SOURCE_TYPE = {
  GbRuleReleaseEdit: 'GB_TEMPLATE',
}

export const TEMPLATE_CATEGORY = [
  { label: '委办局', value: 'MCs' },
  { label: '本级', value: 'MA' },
  { label: '国标', value: 'MQ' },
  { label: '基础', value: 'Mg' },
  { label: '前置机委办局', value: 'MSs' },
  { label: '前置机本级', value: 'MSw' },
  { label: '前置机国标', value: 'MS0' },
]

// 目录类型值
export const TEMPLATE_CATEGORY_VALUE = {
  WBJ: 'MCs',
  BJ: 'MA',
  GB: 'MQ',
  BASE: 'Mg',
  FRONTEND_WBJ: 'MSs',
  FRONTEND_BJ: 'MSw',
  FRONTEND_GB: 'MS0',
}

// 规则管理目录类型
export const RULE_MANAGE_CATALOG_CATEGORY = [
  { id: TEMPLATE_CATEGORY_VALUE.WBJ, name: `委办局` },
  { id: TEMPLATE_CATEGORY_VALUE.BJ, name: `本级` },
  { id: TEMPLATE_CATEGORY_VALUE.GB, name: `国标` },
  { id: TEMPLATE_CATEGORY_VALUE.FRONTEND_WBJ, name: `前置机委办局` },
  { id: TEMPLATE_CATEGORY_VALUE.FRONTEND_BJ, name: `前置机本级` },
  { id: TEMPLATE_CATEGORY_VALUE.FRONTEND_GB, name: `前置机国标` },
]

// 基础规则来源目录类型
export const BASE_RULE_CATALOG_CATEGORY = [
  { id: TEMPLATE_CATEGORY_VALUE.GB, name: `国标` },
  { id: TEMPLATE_CATEGORY_VALUE.BJ, name: `本级` },
  { id: TEMPLATE_CATEGORY_VALUE.WBJ, name: `委办局` },
]

// 目录类型
export const CATEGORY_TYPE = {
  source: `SOURCE`,
  target: `TARGET`,
}
