import {
  DATA_TYPE_ENUM,
  PUBLIC_SCOPE_ENUM,
} from '@/app-modules/rule-manage/helpers/constants'
import { YES_OR_NO_VALUES } from '@/constants/response'

/**
 * 1. 验证补全依据的合理性: subjectCategory
 * > 1. 如果补全资源目录为"自然人",依据必须[包含]"身份证号"
 * 2. 验证比对依据的合理性: subjectCategory
 * > 1. 如果比对项为"统一社会信用代码/身份证号", 那比对依据不能为"统一社会信用代码/身份证号"
 * > 2. 如果比对项为"企业名称", 比对依据不能为"企业名称"
 * > 3. 如果比对资源目录为"自然人",依据必须[包含]"身份证号" // 如果是证件号码字段，不做比对
 */

/**
 * 数据类型匹配
 * @param {*} target
 * @param {*} origin
 */
function matchType (target = {}, origin = {}) {
  const {
    TYPE_DATE,
    TYPE_COLLECTIVE,
    TYPE_ENUMERATED,
    TYPE_INTEGER,
    TYPE_FLOAT,
    TYPE_STRING,
  } = DATA_TYPE_ENUM

  const dataTypeMap = new Map([
    [TYPE_DATE, [TYPE_DATE]],
    [TYPE_COLLECTIVE, [TYPE_COLLECTIVE, TYPE_ENUMERATED]],
    [TYPE_ENUMERATED, [TYPE_ENUMERATED]],
    [TYPE_INTEGER, [TYPE_INTEGER]],
    [TYPE_FLOAT, [TYPE_FLOAT, TYPE_INTEGER]],
    [TYPE_STRING, [
      TYPE_DATE,
      TYPE_COLLECTIVE,
      TYPE_ENUMERATED,
      TYPE_INTEGER,
      TYPE_FLOAT,
      TYPE_STRING,
    ]],
  ])

  return (dataTypeMap.get(target.id) || []).includes(origin.id)
}

/**
 * 目标数据数组是否包含源数据数组项
 * @param {*} target
 * @param {*} origin
 */
function isContain (target, origin) {
  return origin.every(val => target.includes(val))
}

/**
 * 数据长度匹配
 * @param {*} target
 * @param {*} origin
 */
function matchLength (target, origin) {
  return parseFloat(target) >= parseFloat(origin)
}

/**
 * 公开范围匹配
 * @param {*} target
 * @param {*} origin
 */
function matchPublicScope (target = {}, origin = {}) {
  const {
    OPEN_TO_THE_PUBLIC,
    GOVERNMENT_AFFAIRS_SHARING,
    AUTHORIZATION_QUERY,
  } = PUBLIC_SCOPE_ENUM

  const publicScopeMap = new Map([
    [OPEN_TO_THE_PUBLIC, [OPEN_TO_THE_PUBLIC]],
    [GOVERNMENT_AFFAIRS_SHARING, [OPEN_TO_THE_PUBLIC, GOVERNMENT_AFFAIRS_SHARING]],
    [AUTHORIZATION_QUERY, [AUTHORIZATION_QUERY, OPEN_TO_THE_PUBLIC, GOVERNMENT_AFFAIRS_SHARING]],
  ])

  return (publicScopeMap.get(target.id) || []).includes(origin.id)
}

/**
 * 是否脱敏匹配
 * @param {*} target
 * @param {*} origin
 */
function matchMasked (target = {}, origin = {}) {
  const { yes, no } = YES_OR_NO_VALUES
  const MASKED = yes // 脱敏
  const UNMASKED = no // 不脱敏
  const maskedMap = new Map([
    [MASKED, [MASKED, UNMASKED]],
    [UNMASKED, [UNMASKED]],
  ])

  return (maskedMap.get(target.id) || []).includes(origin.id)
}

/**
 * 脱敏规则匹配
 * @param {*} target
 * @param {*} origin
 */
function matchMaskRule (target = [], origin = []) {
  return parseFloat(target[0]) <= parseFloat(origin[0]) && parseFloat(target[1]) <= parseFloat(origin[1])
}

/**
 * 补全规则验证
 * @param {*} target
 * @param {*} origin
 */
export function filterRuleItems (target = {}, origin = {}) {
  /**
   * 资源目录数据项匹配依据：
   * (1). 数据类型对应关系(目标:来源): type
   *  > 1. 日期: 日期
   *  > 2. 集合: 集合
   *  > 3. 枚举: 枚举
   *  > 4. 整型: 整型
   *  > 5. 浮点: 浮点、整型
   *  > 6. 字符串: 全部
   * (2). 数据长度: 目标>=来源(全部),目标项包含来源(集合-枚举) length
   * (3). 公开范围: 目标>=来源 dimension
   * (4). 是否脱敏: 不脱敏>脱敏 isMasked
   * (5). 脱敏规则: 目标>=来源 maskRule
   */
  // 第一步：数据类型校验 type
  // 1、类型比较
  if (!matchType(target.type, origin.type)) return false

  // 2、类型包含 如果 target 类型为枚举、集合，target必须包含 origin 所有数据项
  const { type: { id } = {} } = target
  if (id === DATA_TYPE_ENUM.TYPE_COLLECTIVE || id === DATA_TYPE_ENUM.TYPE_ENUMERATED) {
    return isContain(target.options, origin.options)
  }
  // 第二步：数据长度校验 length
  if (!isNaN(target.length) && !matchLength(target.length, origin.length)) return false
  // 第三步：公开范围校验 dimension
  if (!matchPublicScope(target.dimension, origin.dimension)) return false
  // 第四步：是否脱敏校验 -> 脱敏规则校验 isMasked
  if (!matchMasked(target.isMasked, origin.isMasked)) return false
  // 第五步：脱敏规则 目标>=来源 maskRule
  if (target.isMasked && target.isMasked.id === YES_OR_NO_VALUES.yes && !matchMaskRule(target.maskRule, origin.maskRule)) return false

  return true
}

export function filterComparisonItem (target = {}, origin = {}) {
  /**
   * 资源目录数据项匹配依据：
   * (1). 数据类型对应关系(目标:来源): type
   *  > 1. 日期: 日期
   *  > 2. 集合: 集合
   *  > 3. 枚举: 枚举
   *  > 4. 整型: 整型
   *  > 5. 浮点: 浮点、整型
   *  > 6. 字符串: 全部
   * (2). 数据长度: 目标>=来源(全部),目标项包含来源(集合-枚举) length
   */

  // 第一步：数据类型校验 type
  // 1、类型比较
  if (!matchType(target.type, origin.type)) return false
  // 2、类型包含 如果 target 类型为枚举、集合，target必须包含 origin 所有数据项
  const { type: { id } = {} } = target
  if (id === DATA_TYPE_ENUM.TYPE_COLLECTIVE || id === DATA_TYPE_ENUM.TYPE_ENUMERATED) {
    return isContain(target.options, origin.options)
  }
  // 第二步：数据长度校验 length
  if (!isNaN(target.length) && !matchLength(target.length, origin.length)) return false
  return true
}
