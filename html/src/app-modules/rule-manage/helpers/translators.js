/**
 * @file 规则管理 - 翻译器
 * @module rule-manage/translators
 */
import Template from './template'
import Rule from './rule'
import cloneDeep from 'lodash.clonedeep'
import { IS_NECESSARY_VALUE } from '@/app-modules/rule-manage/helpers/constants'
// import { filterRuleItems } from '@/app-modules/rule-manage/helpers/validation'

export function translateTemplates (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Template(item)
      .get([
        'id',
        'name',
        'ruleCount',
        'userGroupCount',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateRuleList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Rule(item)
      .get([
        'id',
        'sourceCategory',
        'sourceTemplate',
        'transformationCategory',
        'transformationTemplate',
        'userGroup',
        'dataTotal',
        'version',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateRuleDetail (res = {}) {
  return new Rule(res)
    .get([
      'id',
      'transformationCategory',
      'transformationTemplate',
      'sourceCategory',
      'sourceTemplate',
      'content',
      'version',
      'crew',
      'testResult',
      'versionRecode',
      'status',
      'updateTimeFormat',
    ])
}

export function translateRuleContent (data) {
  if (!Array.isArray(data) || !data.length) return []

  const tempData = []

  data.forEach(item => {
    if (Array.isArray(item) && item.length) {
      tempData.push(...item)
    }
  })

  return tempData
}

export function translateUnAuditedRuleList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Rule(item)
      .get([
        'id',
        'sourceCategory',
        'sourceTemplate',
        'transformationCategory',
        'transformationTemplate',
        'userGroup',
        'applyStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateUnAuditedRuleDetail (res = {}) {
  return new Rule(res)
    .get([
      'id',
      'sourceCategory',
      'sourceTemplate',
      'transformationCategory',
      'transformationTemplate',
      'content',
      'version',
      'crew',
      'testResult',
      'rejectReason',
      'applyStatus',
      'unAuditedInformation',
      'updateTimeFormat',
    ])
}

function renderTransformationRuleData (data = []) {
  const _data = cloneDeep(data)
  if (!Array.isArray(_data) || !_data.length) return []

  const filterNoTransformationData = _data.filter(prop => !!prop.originIdentify)

  if (!Array.isArray(filterNoTransformationData) || !filterNoTransformationData.length) return []
  const transformationData = {}

  filterNoTransformationData.forEach(item => {
    const { originIdentify, targetIdentify } = item
    const tempObj = {}
    tempObj[targetIdentify] = originIdentify
    Object.assign(transformationData, tempObj)
  })

  return transformationData
}

function renderCompletionRule (data = []) {
  const _data = cloneDeep(data)
  if (!Array.isArray(_data) || !_data.length) return []
  const filterCompletionRuleData = _data.filter(prop => Array.isArray(prop.origin) && prop.origin.length)
  if (!Array.isArray(filterCompletionRuleData) || !filterCompletionRuleData.length) return []

  const resData = {}
  filterCompletionRuleData.forEach(item => {
    const { origin = [], target = {} } = item
    let originData = []
    const tempObj = {}

    if (Array.isArray(origin) && origin.length) {
      originData = origin.map(prop => {
        return {
          id: prop.template,
          base: prop.base,
          item: prop.item,
        }
      })
    }

    tempObj[target.targetIdentify] = originData
    Object.assign(resData, tempObj)
  })
  return resData
}

function renderComparisonRuleRule (data = []) {
  const _data = cloneDeep(data)
  if (!Array.isArray(_data) || !_data.length) return []
  const filterComparisonRuleData = _data.filter(prop => Array.isArray(prop.origin) && prop.origin.length)
  if (!Array.isArray(filterComparisonRuleData) || !filterComparisonRuleData.length) return []
  const resData = {}
  filterComparisonRuleData.forEach(item => {
    let originData = []
    const tempObj = {}

    if (Array.isArray(item.origin) && item.origin.length) {
      originData = item.origin.map(prop => {
        return {
          id: prop.template,
          base: prop.base,
          item: prop.item,
        }
      })
    }

    tempObj[item.target.identify] = originData
    Object.assign(resData, tempObj)
  })
  return resData
}

export function translateSubmitData (data = {}) {
  const {
    comparisonRule = {},
    completionRule = {},
    deDuplicationRule = {},
    transformationRule = {},
  } = data

  const result = {
    transformationTemplates: transformationRule.transformationTemplates,
    sourceTemplates: transformationRule.sourceTemplates,
    rules: {
      transformationRule: renderTransformationRuleData(transformationRule.items),
      completionRule: renderCompletionRule(completionRule.items),
      comparisonRule: renderComparisonRuleRule(comparisonRule.items),
      deDuplicationRule: {
        result: deDuplicationRule.result,
        items: deDuplicationRule.items.map(prop => prop.identify),
      },
    },
  }
  return result
}

export function translateFullGbTemplates (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Template(item)
      .get([
        'id',
        'name',
        'identify',
        'subjectCategory',
        'exchangeFrequency',
        'dimension',
        'infoClassify',
        'infoCategory',
        'items',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateFullWbjTemplates (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Template(item)
      .get([
        'id',
        'name',
        'identify',
        'subjectCategory',
        'exchangeFrequency',
        'dimension',
        'infoClassify',
        'infoCategory',
        'items',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function renderItems (data = []) {
  if (!data.length) return false
  const itemsList = []

  try {
    data.forEach((prop, index) => {
      itemsList.push({
        index: index + 1,
        name: prop.name,
        type: prop.type,
        length: prop.length,
        identify: prop.identify,
        dimension: prop.dimension,
        isMasked: prop.isMasked,
        maskRule: prop.maskRule,
        targetIdentify: prop.identify,
        target: prop,
        isNecessary: {
          id: prop.isNecessary && prop.isNecessary.id,
          type: (prop.isNecessary && prop.isNecessary.id === IS_NECESSARY_VALUE)
            ? 'danger'
            : 'info',
          name: prop.isNecessary && prop.isNecessary.name,
        },
        origin: null,
        originIdentify: null,
      })
    })
  } catch (error) {
    // ignore
    console.log(error)
  }

  return itemsList
}

export function initRuleItems (data = []) {
  const _data = cloneDeep(data)
  const resData = []
  _data.forEach(item => {
    const itemData = {
      target: item,
      isNecessary: {
        id: item.isNecessary && item.isNecessary.id,
        type: (item.isNecessary && item.isNecessary.id === IS_NECESSARY_VALUE)
          ? 'danger'
          : 'info',
        name: item.isNecessary && item.isNecessary.name,
      },
      /**
       * 补全信息项 origin[] -> item:
       * {
       *   base: [],
       *   item: {},
       *   template: {
       *     id: id,
       *     name: name,
       *   },
       * },
       */
      origin: [],
    }

    resData.push(itemData)
  })
  return resData
}

/**
 * 规则编辑
 */
export function translateEditRuleDetail (res = {}) {
  const {
    sourceCategory = {},
    sourceTemplate = {},
    transformationCategory = {},
    transformationTemplate = {},
    content = {},
  } = res
  const { deDuplicationRule = {} } = content
  const transformationRule = {}
  const completionRule = {}
  const comparisonRule = {}

  if (Array.isArray(content.transformationRule)) {
    content.transformationRule.forEach(rule => {
      transformationRule[rule.target.identify] = rule.origin.identify
    })
  }

  if (Array.isArray(content.comparisonRule)) {
    content.comparisonRule.forEach(rule => {
      if (Array.isArray(rule) && rule.length) {
        const key = rule[0].target.identify

        comparisonRule[key] = rule.map(v => ({
          id: v.template.id,
          base: v.base.map(i => i.id),
          item: v.origin.identify,
        }))
      }
    })
  }

  if (Array.isArray(content.completionRule)) {
    content.completionRule.forEach(rule => {
      if (Array.isArray(rule) && rule.length) {
        const key = rule[0].target.identify

        completionRule[key] = rule.map(v => ({
          id: v.template.id,
          base: v.base.map(i => i.id),
          item: v.origin.identify,
        }))
      }
    })
  }

  return {
    sourceTemplateId: sourceTemplate.id,
    sourceTemplateCategory: sourceCategory.id,
    targetTemplateId: transformationTemplate.id,
    targetTemplateCategory: transformationCategory.id,
    transformationRule,
    completionRule,
    comparisonRule,
    deDuplicationRule: {
      result: deDuplicationRule.result && deDuplicationRule.result.id,
      items: deDuplicationRule.items.map(item => item.identify),
    },
  }
}
