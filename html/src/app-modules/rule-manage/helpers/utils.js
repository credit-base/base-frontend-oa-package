import cloneDeep from 'lodash.clonedeep'
import { filterRuleItems } from './validation'

export function getDataType (data) {
  const temp = Object.prototype.toString.call(data)
  const type = temp.match(/\b\w+\b/g)
  return (type.length < 2) ? 'Undefined' : type[1]
}

export function objectCompare (origin, target, keys = []) {
  const differenceValue = []

  keys.forEach(prop => {
    const _origin = JSON.stringify(origin[prop.key])
    const _target = JSON.stringify(target[prop.key])

    if (_origin !== _target) {
      differenceValue.push(prop)
    }
  })
  return differenceValue
}

export function matchingSourceTemplatesByTargetTemplate (targetTemplate = {}, sourceTemplateList = []) {
  let _sourceTemplateList = cloneDeep(sourceTemplateList)
  const targetItems = targetTemplate.items || []

  _sourceTemplateList = _sourceTemplateList.filter(sourceTemplate => {
    const { items = [] } = sourceTemplate
    const filterTargetItems = []
    targetItems.forEach(targetItem => {
      const tempItems = items.filter(item => filterRuleItems(targetItem, item))
      if (tempItems.length) {
        filterTargetItems.push(tempItems.length)
      }
      // console.log(targetItem)
    })
    return filterTargetItems.length
  })
  // console.log(_sourceTemplateList)
  return _sourceTemplateList
}
