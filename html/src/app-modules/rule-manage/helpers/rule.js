import Base from '@/models/base'

class Rule extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      transformationCategory = {},
      transformationTemplate = {},
      sourceCategory = {},
      sourceTemplate = {},
      content = {
        transformationRule: [],
        completionRule: [],
        comparisonRule: [],
        deDuplicationRule: {},
      },
      dataTotal,
      version,
      rejectReason,
      crew = {},
      userGroup = {},
      testResult,
      status = {},
      applyStatus = {},
      updateTime,
      versionRecode = {},
      updateTimeFormat,
      createTime,
      createTimeFormat,
      statusTime,
      statusTimeFormat,
      unAuditedInformation = {},
    } = params

    this.id = id
    this.transformationCategory = transformationCategory.name
    this.transformationTemplate = transformationTemplate.name
    this.sourceCategory = sourceCategory.name
    this.sourceTemplate = sourceTemplate.name
    this.content = content
    this.dataTotal = dataTotal
    this.version = version
    this.crew = crew.name
    this.userGroup = userGroup.name
    this.testResult = testResult
    this.status = status
    this.applyStatus = applyStatus
    this.rejectReason = rejectReason
    this.versionRecode = versionRecode
    this.updateTime = updateTime
    this.updateTimeFormat = updateTimeFormat
    this.createTime = createTime
    this.createTimeFormat = createTimeFormat
    this.statusTime = statusTime
    this.statusTimeFormat = statusTimeFormat
    this.unAuditedInformation = unAuditedInformation
  }
}

export default Rule
