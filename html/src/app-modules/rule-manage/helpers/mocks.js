/* istanbul ignore file */
/* eslint-disable max-lines */

export const gbTemplates = [
  {
    id: 'MTQ',
    name: '地方性黑名单信息',
    ruleCount: 0,
    userGroupCount: 1,
  },
  {
    id: 'MS0',
    name: '公积金缴存信息',
    ruleCount: 0,
    userGroupCount: 1,
  },
  {
    id: 'MDA',
    name: '社保缴费信息',
    ruleCount: 0,
    userGroupCount: 1,
  },
  {
    id: 'Nw',
    name: '欠税信息',
    ruleCount: 0,
    userGroupCount: 1,
  },
  {
    id: 'MA',
    name: '纳税信息',
    ruleCount: 0,
    userGroupCount: 1,
  },
]

export const gbRuleList = [
  {
    id: 'MA',
    dataTotal: 120,
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    version: 'V20210101',
    template: { // 目标资源目录数据
      id: 'MA',
      name: '失信企业名录',
    },
    wbjTemplate: {
      id: 'MA',
      name: '自然人信息黑名单',
    },
    // 0 待审核 2 审核通过 -2 审核驳回 -4 撤销
    applyStatus: {
      id: 'Lw',
      name: '待审核',
      type: 'warning',
    },
    userGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'MC4',
    dataTotal: 1000,
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    version: 'V20210102',
    template: { // 目标资源目录数据
      id: 'MA',
      name: '失信企业名录',
    },
    wbjTemplate: {
      id: 'MA',
      name: '失信企业名录',
    },
    // 0 待审核 2 审核通过 -2 审核驳回 -4 撤销
    applyStatus: {
      id: 'LC0',
      name: '已驳回',
      type: 'danger',
    },
    userGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
]

export const gbRuleDetail = {
  id: 'MC4',
  content: {
    transformationRule: [
      {
        target: { // 目标资源目录
          identify: 'ZTML',
          name: '主体名称',
        },
        origin: { // 来源资源目录
          identify: 'QYMC',
          name: '企业名称',
        },
        template: { //
          id: 'MA',
          name: '资源目录名称',
        },
      },
      {
        target: {
          identify: 'TYSHXYDM',
          name: '统一社会信用代码',
        },
        origin: {
          identify: 'YYZZHM',
          name: '营业执证号码',
        },
        template: { //
          id: 'MA',
          name: '资源目录名称',
        },
      },
    ],
    completionRule: [
      [
        {
          target: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          origin: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          base: [],
          template: {
            id: '1',
            name: '登记信息',
          },
        },
        {
          target: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          origin: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          base: [],
          template: {
            id: '2',
            name: '公证员信息',
          },
        },
      ],
      [
        {
          target: {
            identify: 'TYSHXYDM',
            name: '统一社会信用代码',
          },
          origin: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          base: [],
          template: {
            id: '4',
            name: '黑名单信息',
          },
        },
        {
          target: {
            identify: 'TYSHXYDM',
            name: '统一社会信用代码',
          },
          origin: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          base: [],
          template: {
            id: '5',
            name: '红名单信息',
          },
        },
      ],
    ],
    comparisonRule: [
      [
        {
          target: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          origin: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          base: [],
          template: {
            id: '1',
            name: '登记信息',
          },
        },
        {
          target: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          origin: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          base: [],
          template: {
            id: '2',
            name: '公证员信息',
          },
        },
      ],
      [
        {
          target: {
            identify: 'TYSHXYDM',
            name: '统一社会信用代码',
          },
          origin: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          base: [],
          template: {
            id: '4',
            name: '黑名单信息',
          },
        },
        {
          target: {
            identify: 'TYSHXYDM',
            name: '统一社会信用代码',
          },
          origin: {
            identify: 'ZTMC',
            name: '主体名称',
          },
          base: [],
          template: {
            id: '5',
            name: '红名单信息',
          },
        },
      ],
    ],
    deDuplicationRule: {
      result: {
        id: 'MA',
        name: '丢弃',
      },
      items: [// 去重的信息项
        {
          identify: 'ZTMC',
          name: '主体名称',
        },
        {
          identify: 'TYSHXYDM',
          name: '统一社会信用代码',
        },
      ],
    },
  },
  dataTotal: 1000,
  version: 'V20210705',
  template: { // 目标资源目录数据
    id: 'MA',
    name: '失信企业名录',
    description: '描述',
  },
  wbjTemplate: { // 来源资源目录数据
    id: 'MA',
    name: '失信企业名录',
  },
  userGroup: {
    id: 'MA',
    name: '发展和改革委员会',
  },
  testResult: '暂无',
  status: {
    id: 'Lw',
    name: '',
    type: 'success',
  },
  applyStatus: {
    id: 'Lw',
    name: '待审核',
    type: 'warning',
  },
  updateTime: 1620872174,
  updateTimeFormat: '2021年05月13日 10点16分',
  statusTime: 1620869215,
  statusTimeFormat: '2021年05月13日 09点26分',
  createTime: 1620831782,
  createTimeFormat: '2021年05月12日 23点03分',
  crew: { // 发布人数据
    id: 'MA',
    realName: '张科',
  },
  unAuditedInformation: {
    total: 1,
    list: [
      {
        id: 'MA',
        crew: {
          id: 'MA',
          realName: '张帅',
          category: {
            id: 'MA',
            name: '超级管理员',
          },
        },
        userGroup: {
          id: 'MA',
          name: '发展和改革委员会',
        },
        applyStatus: {
          id: 'Lw',
          name: '待审核',
          type: 'warning',
        },
        reason: '驳回原因',
        updateTimeFormat: '2019年12月14日 18时02分',
      },
    ],
  },
  versionRecode: {
    total: 1,
    list: [
      {
        id: 'MA',
        crew: {
          id: 'MA',
          realName: '张帅',
          category: {
            id: 'MA',
            name: '超级管理员',
          },
        },
        userGroup: {
          id: 'MA',
          name: '发展和改革委员会',
        },
        version: 'V20210705',
        updateTimeFormat: '2019年12月14日 18时02分',
      },
    ],
  },
}

export const itemsWbjList = [
  {
    name: '主体名称',
    identify: 'ZTMC',
    type: { id: 'MA', name: '字符型' },
    length: '200',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'MA',
      name: '是',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '主体类别',
    identify: 'ZTLB',
    type: { id: 'MA', name: '字符型' },
    length: '2',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '统一社会信用代码',
    identify: 'TYSHXYDM',
    type: { id: 'MA', name: '字符型' },
    length: '18',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '法定代表人（负责人）',
    identify: 'FDDBR',
    type: { id: 'MA', name: '字符型' },
    length: '100',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MQ',
      name: '社会公开',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '法定代表人（负责人） 证件类型',
    identify: 'FDDBRZJLX',
    type: { id: 'MA', name: '字符型' },
    length: '100',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'Mg',
      name: '授权查询',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '成立日期',
    identify: 'CLRQ',
    type: { id: 'MA', name: '字符型' },
    length: '',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '有效期',
    identify: 'YXQ',
    type: { id: 'MA', name: '字符型' },
    length: '',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '地址',
    identify: 'DZ',
    type: { id: 'MA', name: '字符型' },
    length: '200',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '登记机关',
    identify: 'DJJG',
    type: { id: 'MA', name: '字符型' },
    length: '200',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '国别(地区)',
    identify: 'GB',
    type: { id: 'MA', name: '字符型' },
    length: '3',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '注册资本',
    identify: 'ZCZB',
    type: { id: 'MA', name: '字符型' },
    length: '(24,6)',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '注册资本币种',
    identify: 'ZCZBBZ',
    type: { id: 'MA', name: '字符型' },
    length: '3',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '行业代码',
    identify: 'HYDM',
    type: { id: 'MA', name: '字符型' },
    length: '5',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '类型',
    identify: 'LX',
    type: { id: 'MA', name: '字符型' },
    length: '3',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '经营范围',
    identify: 'JYFW',
    type: { id: 'MA', name: '字符型' },
    length: '2000',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '经营状态',
    identify: 'JYZT',
    type: { id: 'MA', name: '字符型' },
    length: '1',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
  {
    name: '经营范围描述',
    identify: 'JYFWMS',
    type: { id: 'MA', name: '字符型' },
    length: '2000',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
    remark: '',
  },
]

export const wbjTemplates = [
  {
    id: 'MA',
    name: '地方纳税人',
    identify: 'FR_DJXX',
    description: '',
    dimension: { id: 'MA', name: '社会公开' },
    exchangeFrequency: { id: 'NQ', name: '每半年' },
    infoCategory: { id: 'Mg', name: '失信信息' },
    infoClassify: { id: 'Mg', name: '红名单' },
    subjectCategory: [{ id: 'MA', name: '法人及非法人组织' }],
    items: [
      ...itemsWbjList,
    ],
    updateTime: 1576317765,
  },
  {
    id: 'MQ',
    name: '红名单',
    identify: 'FR_DJBGXX',
    description: '',
    dimension: { id: 'MA', name: '社会公开' },
    exchangeFrequency: { id: 'Ng', name: '每一年' },
    infoCategory: { id: 'Lw', name: '' },
    infoClassify: { id: 'NA', name: '其他' },
    subjectCategory: [{ id: 'MA', name: '法人及非法人组织' }],
    items: [
      ...itemsWbjList,
    ],
    updateTime: 1576317765,
  },
  {
    id: 'Mg',
    name: '股权结构信息',
    identify: 'FR_GQJGXX',
    description: '',
    dimension: { id: 'MA', name: '社会公开' },
    exchangeFrequency: { id: 'Ng', name: '每一年' },
    infoCategory: { id: 'Lw', name: '' },
    infoClassify: { id: 'NA', name: '其他' },
    subjectCategory: [{ id: 'MA', name: '法人及非法人组织' }],
    items: [
      ...itemsWbjList,
    ],
    updateTime: 1576317765,
  },
  {
    id: 'Mw',
    name: '分支机构信息',
    identify: 'FR_FZJGXX',
    description: '',
    dimension: { id: 'MA', name: '社会公开' },
    exchangeFrequency: { id: 'Ng', name: '每一年' },
    infoCategory: { id: 'Lw', name: '' },
    infoClassify: { id: 'NA', name: '其他' },
    subjectCategory: [{ id: 'MA', name: '法人及非法人组织' }],
    items: [
      ...itemsWbjList,
    ],
    updateTime: 1576317765,
  },
  {
    id: 'MTQ',
    name: '高管人员信息',
    identify: 'FR_GGRYXX',
    description: '',
    dimension: { id: 'MA', name: '社会公开' },
    exchangeFrequency: { id: 'Ng', name: '每一年' },
    infoCategory: { id: 'Lw', name: '' },
    infoClassify: { id: 'NA', name: '其他' },
    subjectCategory: [{ id: 'MA', name: '法人及非法人组织' }],
    items: [
      ...itemsWbjList,
    ],
    updateTime: 1576317765,
  },
  {
    id: 'MjE',
    name: '企业年报信息',
    identify: 'FR_QYNBXX',
    description: '',
    dimension: { id: 'MA', name: '社会公开' },
    exchangeFrequency: { id: 'Ng', name: '每一年' },
    infoCategory: { id: 'Lw', name: '' },
    infoClassify: { id: 'NA', name: '其他' },
    subjectCategory: [{ id: 'MA', name: '法人及非法人组织' }],
    items: [
      ...itemsWbjList,
    ],
    updateTime: 1576317765,
  },
]

export const templateItemsNoTyshxydm = [
  {
    name: '\u8363\u8a89\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'RYMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u673a\u6784\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '\u4fe1\u7528\u4e3b\u4f53\u7684\u6cd5\u5b9a\u540d\u79f0',
    identify: 'JGMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  },
  {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '100',
    options: [],
    remarks: '\u884c\u653f\u76f8\u5bf9\u4eba\u4e3a\u673a\u6784\u65f6\u586b \u5199\uff0c\u4e0e\u8bc1\u4ef6\u7c7b\u578b\u53ca\u8bc1\u4ef6\u53f7 \u7801\u5bf9\u5e94',
    identify: 'FDDBR',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u7c7b\u578b',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '3',
    options: [],
    remarks: '\u884c\u653f\u76f8\u5bf9\u4eba\u4e3a\u673a\u6784\u65f6\u586b \u5199\uff0c\u4e0e\u8bc1\u4ef6\u7c7b\u578b\u53ca\u8bc1\u4ef6\u53f7 \u7801\u5bf9\u5e94',
    identify: 'FDDBRZJLX',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u53f7\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'FDDBRZJHM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u90e8\u95e8',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'SYBM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u90e8\u95e8\u7edf\u4e00\u793e\u4f1a\u4fe1\u7528\u4ee3\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'SYBMTYSHXYDM',
    isMasked: {
      id: 'MA',
      name: '\u662f',
    },
    maskRule: ['4', '4'],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'Lw',
      name: '\u5426',
    },
  }, {
    name: '\u6388\u4e88\u4f9d\u636e',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '2000',
    options: [],
    remarks: '',
    identify: 'SYYJ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u65e5\u671f',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'SYRQ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6709\u6548\u671f\u59cb',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'YXQS',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6709\u6548\u671f\u81f3',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'YXQZ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u8868\u5f70\u4e8b\u7531',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '2000',
    options: [],
    remarks: '',
    identify: 'BZSY',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6ce8\u518c\u8d44\u91d1',
    type: {
      id: 'Mw',
      name: '\u6d6e\u70b9\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'ZCZJ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7f34\u8d39\u7c7b\u578b',
    type: {
      id: 'NA',
      name: '\u679a\u4e3e\u578b',
    },
    length: '4',
    options: ['\u516c\u5e10\u5bf9\u51b2', '\u73b0\u91d1\u8f6c\u8d26', '\u7f51\u7edc\u8f6c\u8d26', '\u6c47\u7b97'],
    remarks: '',
    identify: 'JFLX',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7f34\u8d39\u8303\u56f4',
    type: {
      id: 'NQ',
      name: '\u96c6\u5408\u578b',
    },
    length: '8',
    options: ['\u5de5\u5546\u7a0e\u52a1', '\u8d22\u52a1', '\u7a0e\u52a1', '\u91c7\u8d2d', '\u5e7f\u544a', '\u7269\u4e1a', '\u4e0a\u5348', '\u85aa\u8d44', '\u56e2\u5efa'],
    remarks: '',
    identify: 'JFFW',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u4e3b\u4f53\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'ZTMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  },
]

export const templateItemsNoZtmc = [
  {
    name: '\u8363\u8a89\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'RYMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u673a\u6784\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '\u4fe1\u7528\u4e3b\u4f53\u7684\u6cd5\u5b9a\u540d\u79f0',
    identify: 'JGMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7edf\u4e00\u793e\u4f1a\u4fe1\u7528\u4ee3\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'TYSHXYDM',
    isMasked: {
      id: 'MA',
      name: '\u662f',
    },
    maskRule: ['4', '4'],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '100',
    options: [],
    remarks: '\u884c\u653f\u76f8\u5bf9\u4eba\u4e3a\u673a\u6784\u65f6\u586b \u5199\uff0c\u4e0e\u8bc1\u4ef6\u7c7b\u578b\u53ca\u8bc1\u4ef6\u53f7 \u7801\u5bf9\u5e94',
    identify: 'FDDBR',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u7c7b\u578b',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '3',
    options: [],
    remarks: '\u884c\u653f\u76f8\u5bf9\u4eba\u4e3a\u673a\u6784\u65f6\u586b \u5199\uff0c\u4e0e\u8bc1\u4ef6\u7c7b\u578b\u53ca\u8bc1\u4ef6\u53f7 \u7801\u5bf9\u5e94',
    identify: 'FDDBRZJLX',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u53f7\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'FDDBRZJHM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u90e8\u95e8',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'SYBM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u90e8\u95e8\u7edf\u4e00\u793e\u4f1a\u4fe1\u7528\u4ee3\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'SYBMTYSHXYDM',
    isMasked: {
      id: 'MA',
      name: '\u662f',
    },
    maskRule: ['4', '4'],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'Lw',
      name: '\u5426',
    },
  }, {
    name: '\u6388\u4e88\u4f9d\u636e',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '2000',
    options: [],
    remarks: '',
    identify: 'SYYJ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u65e5\u671f',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'SYRQ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6709\u6548\u671f\u59cb',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'YXQS',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6709\u6548\u671f\u81f3',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'YXQZ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u8868\u5f70\u4e8b\u7531',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '2000',
    options: [],
    remarks: '',
    identify: 'BZSY',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6ce8\u518c\u8d44\u91d1',
    type: {
      id: 'Mw',
      name: '\u6d6e\u70b9\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'ZCZJ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7f34\u8d39\u7c7b\u578b',
    type: {
      id: 'NA',
      name: '\u679a\u4e3e\u578b',
    },
    length: '4',
    options: ['\u516c\u5e10\u5bf9\u51b2', '\u73b0\u91d1\u8f6c\u8d26', '\u7f51\u7edc\u8f6c\u8d26', '\u6c47\u7b97'],
    remarks: '',
    identify: 'JFLX',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7f34\u8d39\u8303\u56f4',
    type: {
      id: 'NQ',
      name: '\u96c6\u5408\u578b',
    },
    length: '8',
    options: ['\u5de5\u5546\u7a0e\u52a1', '\u8d22\u52a1', '\u7a0e\u52a1', '\u91c7\u8d2d', '\u5e7f\u544a', '\u7269\u4e1a', '\u4e0a\u5348', '\u85aa\u8d44', '\u56e2\u5efa'],
    remarks: '',
    identify: 'JFFW',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  },
]

export const templateItemsAll = [
  {
    name: '\u8363\u8a89\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'RYMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u673a\u6784\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '\u4fe1\u7528\u4e3b\u4f53\u7684\u6cd5\u5b9a\u540d\u79f0',
    identify: 'JGMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7edf\u4e00\u793e\u4f1a\u4fe1\u7528\u4ee3\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'TYSHXYDM',
    isMasked: {
      id: 'MA',
      name: '\u662f',
    },
    maskRule: ['4', '4'],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '100',
    options: [],
    remarks: '\u884c\u653f\u76f8\u5bf9\u4eba\u4e3a\u673a\u6784\u65f6\u586b \u5199\uff0c\u4e0e\u8bc1\u4ef6\u7c7b\u578b\u53ca\u8bc1\u4ef6\u53f7 \u7801\u5bf9\u5e94',
    identify: 'FDDBR',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u7c7b\u578b',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '3',
    options: [],
    remarks: '\u884c\u653f\u76f8\u5bf9\u4eba\u4e3a\u673a\u6784\u65f6\u586b \u5199\uff0c\u4e0e\u8bc1\u4ef6\u7c7b\u578b\u53ca\u8bc1\u4ef6\u53f7 \u7801\u5bf9\u5e94',
    identify: 'FDDBRZJLX',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u53f7\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'FDDBRZJHM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u90e8\u95e8',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'SYBM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u90e8\u95e8\u7edf\u4e00\u793e\u4f1a\u4fe1\u7528\u4ee3\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'SYBMTYSHXYDM',
    isMasked: {
      id: 'MA',
      name: '\u662f',
    },
    maskRule: ['4', '4'],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'Lw',
      name: '\u5426',
    },
  }, {
    name: '\u6388\u4e88\u4f9d\u636e',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '2000',
    options: [],
    remarks: '',
    identify: 'SYYJ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u65e5\u671f',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'SYRQ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6709\u6548\u671f\u59cb',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'YXQS',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6709\u6548\u671f\u81f3',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'YXQZ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u8868\u5f70\u4e8b\u7531',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '2000',
    options: [],
    remarks: '',
    identify: 'BZSY',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6ce8\u518c\u8d44\u91d1',
    type: {
      id: 'Mw',
      name: '\u6d6e\u70b9\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'ZCZJ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7f34\u8d39\u7c7b\u578b',
    type: {
      id: 'NA',
      name: '\u679a\u4e3e\u578b',
    },
    length: '4',
    options: ['\u516c\u5e10\u5bf9\u51b2', '\u73b0\u91d1\u8f6c\u8d26', '\u7f51\u7edc\u8f6c\u8d26', '\u6c47\u7b97'],
    remarks: '',
    identify: 'JFLX',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7f34\u8d39\u8303\u56f4',
    type: {
      id: 'NQ',
      name: '\u96c6\u5408\u578b',
    },
    length: '8',
    options: ['\u5de5\u5546\u7a0e\u52a1', '\u8d22\u52a1', '\u7a0e\u52a1', '\u91c7\u8d2d', '\u5e7f\u544a', '\u7269\u4e1a', '\u4e0a\u5348', '\u85aa\u8d44', '\u56e2\u5efa'],
    remarks: '',
    identify: 'JFFW',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u4e3b\u4f53\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'ZTMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  },
]

export const allGbTemplates = [
  {
    id: 'NTA',
    name: '工商税务',
    identify: 'GSSW',
    items: [
      ...templateItemsAll,
    ],
  },
  {
    id: 'NCs',
    name: '行政许可信息',
    identify: 'HZXKXX',
    items: [
      ...templateItemsAll,
    ],
  },
  {
    id: 'NS8',
    name: '地方性黑名单信息',
    identify: 'DFXHMDXX',
    items: [
      ...templateItemsNoTyshxydm,
    ],
  },
  {
    id: 'NDI',
    name: '行政处罚信息',
    identify: 'HZCFXX',
    items: [
      ...templateItemsNoZtmc,
    ],
  },
  {
    id: 'My4',
    name: '统一代码对照表',
    identify: 'TYDMDZB',
    items: [
      ...templateItemsNoZtmc,
    ],
  },
  {
    id: 'MjE',
    name: '企业年报信息',
    identify: 'QYNBXX',
    items: [
      ...templateItemsAll,
    ],
  },
  {
    id: 'MTQ',
    name: '高管人员信息',
    identify: 'GGRYXX',
    items: [
      ...templateItemsAll,
    ],
  },
  {
    id: 'MS0',
    name: '分支机构信息',
    identify: 'FZJGXX',
    items: [
      ...templateItemsAll,
    ],
  },
  {
    id: 'MDA',
    name: '股权结构信息',
    identify: 'GQJGXX',
    items: [
      ...templateItemsAll,
    ],
  },
  {
    id: 'Nw',
    name: '变更登记信息',
    identify: 'BGDJXX',
    items: [
      ...templateItemsAll,
    ],
  },
  {
    id: 'MA',
    name: '登记信息',
    identify: 'DJXX',
    items: [
      ...templateItemsAll,
    ],
  },
]

export const wbjTempItems = [
  {
    name: '\u8363\u8a89\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'RYMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u673a\u6784\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '180',
    options: [],
    remarks: '\u4fe1\u7528\u4e3b\u4f53\u7684\u6cd5\u5b9a\u540d\u79f0',
    identify: 'JGMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7edf\u4e00\u793e\u4f1a\u4fe1\u7528\u4ee3\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'TYSHXYDM',
    isMasked: {
      id: 'MA',
      name: '\u662f',
    },
    maskRule: ['4', '4'],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '100',
    options: [],
    remarks: '\u884c\u653f\u76f8\u5bf9\u4eba\u4e3a\u673a\u6784\u65f6\u586b \u5199\uff0c\u4e0e\u8bc1\u4ef6\u7c7b\u578b\u53ca\u8bc1\u4ef6\u53f7 \u7801\u5bf9\u5e94',
    identify: 'FDDBR',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u7c7b\u578b',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '3',
    options: [],
    remarks: '\u91c7\u7528 \u300a\u5e38\u7528\u8bc1 \u4ef6\u4ee3\u7801\u300b',
    identify: 'FDDBRZJLX',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u53f7\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'FDDBRZJHM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u90e8\u95e8',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'SYBM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u90e8\u95e8\u7edf\u4e00\u793e\u4f1a\u4fe1\u7528\u4ee3\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'SYBMTYSHXYDM',
    isMasked: {
      id: 'MA',
      name: '\u662f',
    },
    maskRule: ['4', '4'],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'Lw',
      name: '\u5426',
    },
  }, {
    name: '\u6388\u4e88\u4f9d\u636e',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '2000',
    options: [],
    remarks: '',
    identify: 'SYYJ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6388\u4e88\u65e5\u671f',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'SYRQ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6709\u6548\u671f\u59cb',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'YXQS',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6709\u6548\u671f\u81f3',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'YXQZ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u8868\u5f70\u4e8b\u7531',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '2000',
    options: [],
    remarks: '',
    identify: 'BZSY',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u6ce8\u518c\u8d44\u91d1',
    type: {
      id: 'Mw',
      name: '\u6d6e\u70b9\u578b',
    },
    length: '',
    options: [],
    remarks: '',
    identify: 'ZCZJ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7f34\u8d39\u7c7b\u578b',
    type: {
      id: 'NA',
      name: '\u679a\u4e3e\u578b',
    },
    length: '4',
    options: ['\u516c\u5e10\u5bf9\u51b2', '\u73b0\u91d1\u8f6c\u8d26', '\u7f51\u7edc\u8f6c\u8d26', '\u6c47\u7b97'],
    remarks: '',
    identify: 'JFLX',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u7f34\u8d39\u8303\u56f4',
    type: {
      id: 'NQ',
      name: '\u96c6\u5408\u578b',
    },
    length: '8',
    options: ['\u5de5\u5546\u7a0e\u52a1', '\u8d22\u52a1', '\u7a0e\u52a1', '\u91c7\u8d2d', '\u5e7f\u544a', '\u7269\u4e1a', '\u4e0a\u5348', '\u85aa\u8d44', '\u56e2\u5efa'],
    remarks: '',
    identify: 'JFFW',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  }, {
    name: '\u4e3b\u4f53\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'ZTMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'Mg',
      name: '\u6388\u6743\u67e5\u8be2',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
  },
]

export const allWbjTemplates = [
  {
    id: 'NTA',
    name: '\u8363\u8a89\u8868\u5f70\u4fe1\u606f',
    identify: 'RYBZXX',
    items: [
      ...wbjTempItems,
    ],
  },
  {
    id: 'NS8',
    name: '\u5730\u65b9\u6027\u9ed1\u540d\u5355\u4fe1\u606f',
    identify: 'DFXHMDXX',
    items: [
      ...wbjTempItems,
    ],
  },
  {
    id: 'NDI',
    name: '\u793e\u4fdd\u7f34\u8d39\u4fe1\u606f',
    identify: 'SBJFXX',
    items: [
      ...wbjTempItems,
    ],
  },
  {
    id: 'NCs',
    name: '\u5730\u65b9\u6027\u9ed1\u540d\u5355\u4fe1\u606f',
    identify: 'DFXHMDXX',
    items: [
      ...wbjTempItems,
    ],
  },
  {
    id: 'My4',
    name: '\u516c\u79ef\u91d1\u7f34\u5b58\u4fe1\u606f',
    identify: 'GJJJCXX',
    items: [
      ...wbjTempItems,
    ],
  },
  {
    id: 'MjE',
    name: '\u884c\u653f\u5904\u7f5a\u4fe1\u606f',
    identify: 'HZCFXX',
    items: [
      ...wbjTempItems,
    ],
  },
  {
    id: 'MTQ',
    name: '\u5730\u65b9\u6027\u9ed1\u540d\u5355\u4fe1\u606f',
    identify: 'DFXHMDXX',
    items: [
      ...wbjTempItems,
    ],
  },
  {
    id: 'MDA',
    name: '\u767b\u8bb0\u4fe1\u606f',
    identify: 'DJXX',
    items: [
      ...wbjTempItems,
    ],
  },
  {
    id: 'Nw',
    name: '\u5730\u65b9\u6027\u7ea2\u540d\u5355\u4fe1\u606f',
    identify: 'DFXHMDXX',
    items: [
      ...wbjTempItems,
    ],
  },
  {
    id: 'MA',
    name: '\u68c0\u67e5\u62bd\u67e5\u4fe1\u606f',
    identify: 'JCCCXX',
    items: [
      ...wbjTempItems,
    ],
  },
]
