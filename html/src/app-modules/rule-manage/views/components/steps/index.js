/**
 * 组件导出
 */

// 转换规则 TransformationRule
export { default as StepTransformationRule } from './step-transformation-rule'
// 补全规则 CompletionRule
export { default as StepCompletionRule } from './step-completion-rule'
// 比对规则 ComparisonRule
export { default as StepComparisonRule } from './step-comparison-rule'
// 去重规则 DeDuplicationRule
export { default as StepDeDuplicationRule } from './step-de-duplication-rule'
// 预览
export { default as StepPreviewConfirm } from './step-preview-confirm'
