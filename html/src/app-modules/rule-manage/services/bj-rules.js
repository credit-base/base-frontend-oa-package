/**
 * @file 本级规则管理 - 接口
 * @module services/bj-rule
 */

// import store from '@/store'
import request from '@/utils/request'
import {
  gbTemplates,
  gbRuleList,
  gbRuleDetail,
  // allBjTemplates,
  // allWbjTemplates,
  // wbjTemplates,
} from '../helpers/mocks'

// 正式表
/**
 * 本级目录列表
 * @param {limit, page} params
 */
export function fetchBjTemplateList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...gbTemplates,
      ],
      total: 5,
    }
  }

  return request('/api/bjRule/bjTemplates', { params })
}

// 正式表
/**
 * 委办局目录列表
 * @param {limit, page} params
 */
export function fetchWbjTemplateList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...gbTemplates,
      ],
      total: 5,
    }
  }

  return request('/api/wbjRule/wbjTemplates', { params })
}

/**
 * 本级规则列表
 * @param {*} params
 */
export function fetchBjRulesList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...gbRuleList,
      ],
      total: 2,
    }
  }
  return request('/api/bjRules', { params })
}

/**
 * 本级规则详情
 * @param {*} id
 * @param {*} params
 */
export function fetchBjRulesDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...gbRuleDetail,
    }
  }
  return request(`/api/bjRules/${id}`)
}

/**
 * 新增规则
 * @param {*} params
 */
export function createBjRules (params = {}) {
  return request('/api/bjRules/add', { method: 'POST', params })
}

/**
 * 获取编辑本级规则信息
 * @param {*} id
 * @param {*} params
 */
export function fetchEditBjRules (id, params = {}) {
  if (params.useMock) {
    return {
      ...gbRuleDetail,
    }
  }
  return request(`/api/bjRules/${id}`)
}

/**
 * 提交编辑信息
 * @param {*} id
 * @param {*} params
 */
export function updateEditBjRules (id, params = {}) {
  return request(`/api/bjRules/${id}/edit`, { method: 'POST', params })
}

/**
 * 删除规则
 * @param {*} id
 */
export function deleteBjRules (id) {
  return request(`/api/bjRules/${id}/delete`, { method: 'POST' })
}
// 审核表

/**
 * 本级规则列表
 * @param {*} params
 */
export function fetchUnAuditedBjRules (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...gbRuleList,
      ],
      total: 2,
    }
  }
  return request('/api/unAuditedBjRules', { params })
}

/**
 * 本级规则详情
 * @param {*} id
 * @param {*} params
 */
export function fetchUnAuditedBjRulesDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...gbRuleDetail,
    }
  }
  return request(`/api/unAuditedBjRules/${id}`)
}

/**
 * 获取重新编辑信息
 * @param {*} id
 * @param {*} params
 */
export function fetchUnAuditedEditBjRules (id, params = {}) {
  if (params.useMock) {
    return {
      ...gbRuleDetail,
    }
  }

  return request(`/api/unAuditedBjRules/${id}`)
}

/**
 * 提交重新编辑信息
 * @param {*} id
 * @param {*} params
 */
export function updateUnAuditedEditBjRules (id, params = {}) {
  return request(`/api/unAuditedBjRules/${id}/edit`, { method: 'POST', params })
}

/**
 * 撤销
 * @param {*} id
 */
export function revokeUnAuditedBjRules (id) {
  return request(`/api/unAuditedBjRules/${id}/revoke`, { method: 'POST' })
}

/**
 * 审核通过
 * @param {*} id
 */
export function approveUnAuditedBjRules (id) {
  return request(`/api/unAuditedBjRules/${id}/approve`, { method: 'POST' })
}

/**
 * 审核驳回
 * @param {*} id
 * @param {*} params
 */
export function rejectUnAuditedBjRules (id, params = {}) {
  return request(`/api/unAuditedBjRules/${id}/reject`, { method: 'POST', params })
}

/**
 * 获取所有本级资源目录
 */
export function fetchAllBjTemplates () {
  const params = {
    limit: 10000,
    page: 1,
  }
  return request('/api/bjTemplates', { params })
}

export function fetchAllWbjTemplates () {
  const params = {
    limit: 10000,
    page: 1,
  }
  return request('/api/wbjTemplates', { params })
}
