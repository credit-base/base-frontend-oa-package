/**
 * @file 国标规则管理 - 接口
 * @module services/gb-rule
 */

// import store from '@/store'
import request from '@/utils/request'
import {
  gbTemplates,
  gbRuleList,
  gbRuleDetail,
  // allGbTemplates,
  // allWbjTemplates,
  // wbjTemplates,
} from '../helpers/mocks'

// 正式表
/**
 * 国标目录列表
 * @param {limit, page} params
 */
export function fetchGbTemplateList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...gbTemplates,
      ],
      total: 5,
    }
  }

  return request('/api/gbRule/gbTemplates', { params })
}

// 正式表
/**
 * 委办局目录列表
 * @param {limit, page} params
 */
export function fetchWbjTemplateList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...gbTemplates,
      ],
      total: 5,
    }
  }

  return request('/api/wbjRule/wbjTemplates', { params })
}

/**
 * 国标规则列表
 * @param {*} params
 */
export function fetchGbRulesList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...gbRuleList,
      ],
      total: 2,
    }
  }
  return request('/api/gbRules', { params })
}

/**
 * 国标规则详情
 * @param {*} id
 * @param {*} params
 */
export function fetchGbRulesDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...gbRuleDetail,
    }
  }
  return request(`/api/gbRules/${id}`)
}

/**
 * 新增规则
 * @param {*} params
 */
export function createGbRules (params = {}) {
  return request('/api/gbRules/add', { method: 'POST', params })
}

/**
 * 获取编辑国标规则信息
 * @param {*} id
 * @param {*} params
 */
export function fetchEditGbRules (id, params = {}) {
  if (params.useMock) {
    return {
      ...gbRuleDetail,
    }
  }
  return request(`/api/gbRules/${id}`)
}

/**
 * 提交编辑信息
 * @param {*} id
 * @param {*} params
 */
export function updateEditGbRules (id, params = {}) {
  return request(`/api/gbRules/${id}/edit`, { method: 'POST', params })
}

/**
 * 删除规则
 * @param {*} id
 */
export function deleteGbRules (id) {
  return request(`/api/gbRules/${id}/delete`, { method: 'POST' })
}
// 审核表

/**
 * 国标规则列表
 * @param {*} params
 */
export function fetchUnAuditedGbRules (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...gbRuleList,
      ],
      total: 2,
    }
  }
  return request('/api/unAuditedGbRules', { params })
}

/**
 * 国标规则详情
 * @param {*} id
 * @param {*} params
 */
export function fetchUnAuditedGbRulesDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...gbRuleDetail,
    }
  }
  return request(`/api/unAuditedGbRules/${id}`)
}

/**
 * 获取重新编辑信息
 * @param {*} id
 * @param {*} params
 */
export function fetchUnAuditedEditGbRules (id, params = {}) {
  if (params.useMock) {
    return {
      ...gbRuleDetail,
    }
  }

  return request(`/api/unAuditedGbRules/${id}`)
}

/**
 * 提交重新编辑信息
 * @param {*} id
 * @param {*} params
 */
export function updateUnAuditedEditGbRules (id, params = {}) {
  return request(`/api/unAuditedGbRules/${id}/edit`, { method: 'POST', params })
}

/**
 * 撤销
 * @param {*} id
 */
export function revokeUnAuditedGbRules (id) {
  return request(`/api/unAuditedGbRules/${id}/revoke`, { method: 'POST' })
}

/**
 * 审核通过
 * @param {*} id
 */
export function approveUnAuditedGbRules (id) {
  return request(`/api/unAuditedGbRules/${id}/approve`, { method: 'POST' })
}

/**
 * 审核驳回
 * @param {*} id
 * @param {*} params
 */
export function rejectUnAuditedGbRules (id, params = {}) {
  return request(`/api/unAuditedGbRules/${id}/reject`, { method: 'POST', params })
}

/**
 * 获取所有国标资源目录
 */
export function fetchAllGbTemplates () {
  const params = {
    limit: 10000,
    page: 1,
  }
  return request('/api/gbTemplates', { params })
}

export function fetchAllWbjTemplates () {
  const params = {
    limit: 10000,
    page: 1,
  }
  return request('/api/wbjTemplates', { params })
}
