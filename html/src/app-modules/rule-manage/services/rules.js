/**
 * @file 国标规则管理 - 接口
 * @module services/gb-rule
 */

import request from '@/utils/request'

/**
 * 获取规则列表
 * @param {*} params
 * * params.transformationCategory
 * * params.transformationTemplateId
 * * params.sourceCategory
 * * params.sourceTemplateId
 * * params.sort
 * * params.page
 * * params.limit
 */
export function fetchRulesList (params = {}) {
  return request(`/api/rules`, { params })
}

/**
 * 获取规则详情
 * @param {*} id
 */
export function fetchRulesDetail (id) {
  return request(`/api/rules/${id}`)
}

/**
 * 新增规则
 * @param {*} params
 * * params.transformationTemplateId
 * * params.sourceTemplateId
 * * params.transformationCategory
 * * params.sourceCategory
 * * params.rules.transformationRule = {}
 * * params.rules.completionRule = {}
 * * params.rules.comparisonRule = {}
 * * params.rules.deDuplicationRule = {}
 */
export function createRules (params = {}) {
  return request(`/api/rules/add`, { method: 'POST', params })
}

/**
 * 获取编辑规则信息
 * @param {*} id
 */
export function fetchEditRules (id) {
  return request(`/api/rules/${id}`)
}

/**
 * 编辑规则
 * @param {*} id
 * @param {*} params
 * * params.rules.transformationRule = {}
 * * params.rules.completionRule = {}
 * * params.rules.comparisonRule = {}
 * * params.rules.deDuplicationRule = {}
 */
export function updateEditRules (id, params = {}) {
  return request(`/api/rules/${id}/edit`, { method: 'POST', params })
}

/**
 * 删除规则
 * @param {*} id
 */
export function deleteRules (id) {
  return request(`/api/rules/${id}/delete`, { method: 'POST' })
}
/**
 * 获取审核规则列表
 * @param {*} params
 * * params.transformationCategory
 * * params.transformationTemplateId
 * * params.sourceCategory
 * * params.sourceTemplateId
 * * params.sort
 * * params.page
 * * params.limit
 */
export function fetchUnAuditedRulesList (params = {}) {
  return request(`/api/unAuditedRules`, { params })
}

/**
 * 获取审核规则详情
 * @param {*} id
 */
export function fetchUnAuditedRulesDetail (id) {
  return request(`/api/unAuditedRules/${id}`)
}

/**
 * 获取重新编辑规则信息
 * @param {*} id
 */
export function fetchEditUnAuditedRulesDetail (id) {
  return request(`/api/unAuditedRules/${id}`)
}

/**
 * 重新编辑规则
 * @param {*} id
 * @param {*} params
 * * params.rules.transformationRule = {}
 * * params.rules.completionRule = {}
 * * params.rules.comparisonRule = {}
 * * params.rules.deDuplicationRule = {}
 */
export function updateUnAuditedRules (id, params = {}) {
  return request(`/api/unAuditedRules/${id}/edit`, { method: 'POST', params })
}

/**
 * 审核通过
 * @param {*} id
 */
export function updateApproveUnAuditedRules (id) {
  return request(`/api/unAuditedRules/${id}/approve`, { method: 'POST' })
}

/**
 * 审核驳回
 * @param {*} id
 * @param {*} params
 */
export function updateRejectUnAuditedRules (id, params = {}) {
  return request(`/api/unAuditedRules/${id}/reject`, { method: 'POST', params })
}

/**
 * 撤消规则
 * @param {*} id
 * @param {*} params
 */
export function updateRevokeUnAuditedRules (id, params = {}) {
  return request(`/api/unAuditedRules/${id}/revoke`, { method: 'POST', params })
}

/**
 * ##############################
 * #       公共资源目录接口        #
 * ##############################
 */

/**
 * 获取本级资源目录
 */
export function fetchCommonBjTemplateList () {
  return request(`/api/commonBjTemplates`)
}

/**
 * 获取本级资源目录
 */
export function fetchCommonGbTemplateList () {
  return request(`/api/commonGbTemplates`)
}

/**
 * 获取本级资源目录
 */
export function fetchCommonWbjTemplateList () {
  return request(`/api/commonWbjTemplates`)
}

/**
 * 获取基础资源目录
 */
export function fetchCommonBaseTemplateList (params = {}) {
  return request(`/api/commonBaseTemplates`, { params })
}
