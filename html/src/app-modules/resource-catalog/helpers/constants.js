/**
 * @file Constants of resource-catalog
 * @module resource-catalog/constants
 */

/* istanbul ignore file */
export const TEMPLATE_CATEGORY = {
  GB: 'national-standard',
  WBJ: 'user-group',
  BJ: 'current-unit',
  QZJ: `frontend-processor`,
}
// 国标
export const NATIONAL_STANDARD_TABS = [
  { name: TEMPLATE_CATEGORY.GB, label: 'NATIONAL_STANDARD_RESOURCE_CATALOG', component: 'Common' },
]

// 本级
export const CURRENT_UNIT_TABS = [
  { name: TEMPLATE_CATEGORY.BJ, label: 'CURRENT_UNIT_RESOURCE_CATALOG', component: 'Common' },
]

// 委办局
export const USER_GROUP_TABS = [
  { name: TEMPLATE_CATEGORY.WBJ, label: 'USER_GROUP_RESOURCE_CATALOG', component: 'Common' },
  { name: 'task-order', label: 'TASK_ORDER_RESOURCE_CATALOG', component: 'OrderTask' },
]

// 反馈资源目录
export const FEEDBACK_TEMPLATE_TABS = [
  { name: 'all-feedback-template', label: 'ALL_FEEDBACK_TEMPLATE', icon: '', type: 'all' },
  { name: 'add-feedback-template', label: 'ADD_FEEDBACK_TEMPLATE', icon: 'add', type: 'add' },
  { name: 'edit-feedback-template', label: 'EDIT_FEEDBACK_TEMPLATE', icon: 'edit', type: 'edit' },
  { name: 'delete-feedback-template', label: 'DELETE_FEEDBACK_TEMPLATE', icon: 'delete', type: 'delete' },
]

export const ORDER_TASK_RESOURCE_CATALOG_ROUTE = {
  name: 'WbjTemplateList',
  params: { tabType: 'task-order' },
}

export const RESOURCE_CATALOG_DETAIL_TABS = [
  {
    name: 'resource-catalog-template',
    label: 'TEMPLATE_TITLE',
    component: 'ResourceCatalogTemplate',
  },
  {
    name: 'resource-catalog-version',
    label: 'VERSION_TITLE',
    component: 'ResourceCatalogVersion',
  },
  {
    name: 'resource-catalog-relation-chart',
    label: 'RELATION_CHART',
    component: 'ResourceCatalogRelationChart',
  },
]

// 国标资源目录步骤配置
export const GB_TEMPLATE_STEPS_META_VALUE = {
  StepCatalogInfo: {
    index: 0,
    name: 'StepCatalogInfo',
  },
  StepTemplateInfo: {
    index: 1,
    name: 'StepTemplateInfo',
  },
  StepPreviewConfirm: {
    index: 2,
    name: 'StepPreviewConfirm',
  },
}

export const GB_TEMPLATE_STEPS_META_CONFIG = [
  {
    title: 'STEP_CATALOG_INFO',
    index: 0,
    component: 'StepCatalogInfo',
    icon: 'step-catalog-info',
  },
  {
    title: 'STEP_TEMPLATE_INFO',
    index: 1,
    component: 'StepTemplateInfo',
    icon: 'step-template-info',
  },
  {
    title: 'STEP_PREVIEW_CONFIRM',
    index: 2,
    component: 'StepPreviewConfirm',
    icon: 'step-preview-confirm',
  },
]

// 委办局资源目录步骤配置
export const WBJ_TEMPLATE_STEPS_META_VALUE = {
  StepCatalogInfo: {
    index: 0,
    name: 'StepCatalogInfo',
  },
  StepTemplateInfo: {
    index: 1,
    name: 'StepTemplateInfo',
  },
  StepPreviewConfirm: {
    index: 2,
    name: 'StepPreviewConfirm',
  },
}

export const WBJ_TEMPLATE_STEPS_META_CONFIG = [
  {
    title: 'STEP_CATALOG_INFO',
    index: 0,
    component: 'StepCatalogInfo',
    icon: 'step-catalog-info',
  },
  {
    title: 'STEP_TEMPLATE_INFO',
    index: 1,
    component: 'StepTemplateInfo',
    icon: 'step-template-info',
  },
  {
    title: 'STEP_PREVIEW_CONFIRM',
    index: 2,
    component: 'StepPreviewConfirm',
    icon: 'step-preview-confirm',
  },
]

// 本级目录步骤
export const BJ_TEMPLATE_STEPS_VALUE = {
  StepNationCatalog: {
    index: 0,
    name: 'StepNationCatalog',
  },
  StepCatalogInfo: {
    index: 1,
    name: 'StepCatalogInfo',
  },
  StepTemplateInfo: {
    index: 2,
    name: 'StepTemplateInfo',
  },
  StepPreviewConfirm: {
    index: 3,
    name: 'StepPreviewConfirm',
  },
}
export const BJ_TEMPLATE_STEPS_CONFIG = [
  {
    title: 'STEP_NATION_CATALOG',
    index: 0,
    component: 'StepNationCatalog',
    icon: 'step-nation-catalog',
  },
  {
    title: 'STEP_CATALOG_INFO',
    index: 1,
    component: 'StepCatalogInfo',
    icon: 'step-catalog-info',
  },
  {
    title: 'STEP_TEMPLATE_INFO',
    index: 2,
    component: 'StepTemplateInfo',
    icon: 'step-template-info',
  },
  {
    title: 'STEP_PREVIEW_CONFIRM',
    index: 3,
    component: 'StepPreviewConfirm',
    icon: 'step-preview-confirm',
  },
]

// 前置机目录步骤
export const QZJ_TEMPLATE_STEPS_VALUE = {
  StepCatalogInfo: {
    index: 0,
    name: 'StepCatalogInfo',
  },
  StepTemplateInfo: {
    index: 1,
    name: 'StepTemplateInfo',
  },
  StepPreviewConfirm: {
    index: 2,
    name: 'StepPreviewConfirm',
  },
}
export const QZJ_TEMPLATE_STEPS_CONFIG = [
  {
    title: 'STEP_CATALOG_INFO',
    index: 0,
    component: 'StepCatalogInfo',
    icon: 'step-catalog-info',
  },
  {
    title: 'STEP_TEMPLATE_INFO',
    index: 1,
    component: 'StepTemplateInfo',
    icon: 'step-template-info',
  },
  {
    title: 'STEP_PREVIEW_CONFIRM',
    index: 2,
    component: 'StepPreviewConfirm',
    icon: 'step-preview-confirm',
  },
]

// 工单任务
export const TASK_STEPS_VALUE = {
  StepBaseCatalog: {
    index: 0,
    name: 'StepBaseCatalog',
  },
  StepSelectCatalog: {
    index: 1,
    name: 'StepSelectCatalog',
  },
  StepFillInformation: {
    index: 2,
    name: 'StepFillInformation',
  },
  StepPreviewConfirm: {
    index: 3,
    name: 'StepPreviewConfirm',
  },
}
export const TASK_STEPS_CONFIG = [
  {
    title: 'STEP_BASE_CATALOG',
    index: 0,
    icon: 'step-nation-catalog',
    component: 'StepBaseCatalog',
  },
  {
    title: 'STEP_SELECT_CATALOG',
    index: 1,
    icon: 'step-template-info',
    component: 'StepSelectCatalog',
  },
  {
    title: 'STEP_FILL_INFORMATION',
    index: 2,
    icon: 'step-catalog-info',
    component: 'StepFillInformation',
  },
  {
    title: 'STEP_PREVIEW_CONFIRM',
    index: 3,
    icon: 'step-preview-confirm',
    component: 'StepPreviewConfirm',
  },
]

// 委办局反馈
export const FEEDBACK_STEPS_VALUE = {
  exist: {
    StepSelectCatalog: {
      index: 0,
      name: 'StepSelectCatalog',
    },
    StepCatalogCompare: {
      index: 1,
      name: 'StepCatalogCompare',
    },
    StepFeedbackMessage: {
      index: 2,
      name: 'StepFeedbackMessage',
    },
    StepPreviewConfirm: {
      index: 3,
      name: 'StepPreviewConfirm',
    },
  },

  notFound: {
    StepUpdateCatalog: {
      index: 0,
      name: 'StepUpdateCatalog',
    },
    StepFeedbackMessage: {
      index: 1,
      name: 'StepFeedbackMessage',
    },
    StepPreviewConfirm: {
      index: 2,
      name: 'StepPreviewConfirm',
    },
  },
}
export const FEEDBACK_STEPS_CONFIG = {
  exist: [
    {
      title: 'STEP_SELECT_CATALOG',
      index: 0,
      icon: 'selected-template',
      component: 'StepSelectCatalog',
    },
    {
      title: 'STEP_CATALOG_COMPARE',
      index: 1,
      icon: 'step-template-info',
      component: 'StepCatalogCompare',
    },
    {
      title: 'STEP_FEEDBACK_MESSAGE',
      index: 2,
      icon: 'feedback-message',
      component: 'StepFeedbackMessage',
    },
    {
      title: 'STEP_PREVIEW_CONFIRM',
      index: 3,
      icon: 'step-preview-confirm',
      component: 'StepPreviewConfirm',
    },
  ],

  notFound: [
    {
      title: 'STEP_UPDATE_CATALOG',
      index: 0,
      icon: 'step-nation-catalog',
      component: 'StepUpdateCatalog',
    },
    {
      title: 'STEP_FEEDBACK_MESSAGE',
      index: 1,
      icon: 'feedback-message',
      component: 'StepFeedbackMessage',
    },
    {
      title: 'STEP_PREVIEW_CONFIRM',
      index: 2,
      icon: 'step-preview-confirm',
      component: 'StepPreviewConfirmNotFound',
    },
  ],
}

// 发改委反馈
export const ADMIN_FEEDBACK_STEPS_VALUE = {
  StepTemplateInfo: {
    index: 0,
    name: 'StepTemplateInfo',
  },
  StepFeedbackMessage: {
    index: 1,
    name: 'StepFeedbackMessage',
  },
  StepPreviewConfirm: {
    index: 2,
    name: 'StepPreviewConfirm',
  },
}
export const ADMIN_FEEDBACK_STEPS_CONFIG = [
  {
    title: 'STEP_EDIT_TEMPLATE',
    index: 0,
    icon: 'step-template-info',
    component: 'StepTemplateInfo',
  },
  {
    title: 'STEP_FEEDBACK_MESSAGE',
    index: 1,
    icon: 'feedback-message',
    component: 'StepFeedbackMessage',
  },
  {
    title: 'STEP_PREVIEW_CONFIRM',
    index: 2,
    icon: 'step-preview-confirm',
    component: 'StepPreviewConfirm',
  },
]
// 资源目录类别
export const CATALOG_TYPE_VALUE = { gb: 'MA', bj: 'MQ' }
export const CATALOG_TYPE_NUMBER = { gb: 1, bj: 2 }
export const CATALOG_TYPE_CONFIG = [
  {
    name: 'GB_TEMPLATE',
    fullName: 'GB_TEMPLATE_FULL',
    type: CATALOG_TYPE_VALUE.gb,
    icon: 'gb-template',
  },
  {
    name: 'BJ_TEMPLATE',
    fullName: 'BJ_TEMPLATE_FULL',
    type: CATALOG_TYPE_VALUE.bj,
    icon: 'bj-template',
  },
]

// 委办局反馈
export const WBJ_CATALOG_STATUS = { exist: 1, notFound: 0 }
export const WBJ_FEEDBACK_CONFIG = [
  {
    name: 'CATALOG_EXIST',
    existStatus: WBJ_CATALOG_STATUS.exist,
    icon: 'catalog-not-found',
  },
  {
    name: 'CATALOG_NOT_FOUND',
    existStatus: WBJ_CATALOG_STATUS.notFound,
    icon: 'catalog-not-found',
  },
]

export const VERSION_OPTIONS = [
  { id: 'MA', name: '1-D012A100C101' },
  { id: 'MQ', name: '2-D012A100C101' },
  { id: 'Mg', name: '3-D012A100C101' },
  { id: 'Mw', name: '4-D012A100C101' },
  { id: 'NQ', name: '5-D012A100C101' },
  { id: 'Na', name: '6-D012A100C101' },
]

export const STATUS_OPTIONS = {
  STATUS_CONFIRMED: 0, // 待确认
  STATUS_REVOKED: 1, // 已撤销
  STATUS_IDENTIFIED: 2, // 已确认
  STATUS_FOLLOWING_UP: 3, // 跟进中
  STATUS_ENDED: 4, // 已终结
}

export const GB_TEMPLATE_LIST_PATH = `/resource-catalog/gb-template`
export const GB_TEMPLATE_DETAIL_PATH = `/resource-catalog/gb-template/detail/`
export const GB_TEMPLATE_EDIT_PATH = `/resource-catalog/gb-template/edit/`

// 目录类别
export const QZJ_CATALOG_CATEGORY_VALUES = {
  WBJ: `MSs`,
  BJ: `MSw`,
  GB: `MS0`,
}
export const QZJ_CATALOG_CATEGORY = [
  { id: QZJ_CATALOG_CATEGORY_VALUES.WBJ, name: `委办局` },
  { id: QZJ_CATALOG_CATEGORY_VALUES.BJ, name: `本级` },
  { id: QZJ_CATALOG_CATEGORY_VALUES.GB, name: `国标` },
]
