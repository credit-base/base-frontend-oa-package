import Base from '@/models/base'

class GbTemplate extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      name,
      identify,
      year,
      version,
      category,
      subjectCategory = [],
      dimension = {},
      gbTemplate = {},
      exchangeFrequency = {},
      infoClassify = {},
      infoCategory = {},
      description,
      items = [],
      updateTime,
      createTime,
      crew = {},
      sourceUnit = {},
    } = params

    this.id = id
    this.name = name
    this.identify = identify
    this.year = year
    this.category = category
    this.version = version
    this.subjectCategory = subjectCategory
    this.dimension = dimension
    this.gbTemplate = gbTemplate
    this.exchangeFrequency = exchangeFrequency
    this.infoClassify = infoClassify
    this.infoCategory = infoCategory
    this.description = description
    this.items = items
    this.updateTime = updateTime
    this.createTime = createTime
    this.crew = crew
    this.sourceUnit = sourceUnit
  }
}

export default GbTemplate
