/**
 * @file Translators of gbTemplate
 * @module resource-catalog/translators
 */

import GbTemplate from './gb-template'
import GbTemplateVersion from './gb-template-version'
import WorkOrderTask from './work-word-task'
import Subtasks from './subtasks'
import ParentTasks from './parent-tasks'
import UserGroup from '@/models/user-group'

// 所有委办局
export function translateFullUserGroup (res = {}) {
  const list = Array.isArray(res.list)
    ? res.list.map(item => new UserGroup(item)
      .get([
        'id',
        'name',
        'shortName',
      ]))
    : []

  return list
}

// 国标资源目录翻译
export function translateGbTemplateList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new GbTemplate(item)
      .get([
        'id',
        'name',
        'identify',
        'subjectCategory',
        'dimension',
        'infoClassify',
        'version',
        'updateTime',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateGbTemplateDetail (res = {}) {
  return new GbTemplate(res)
    .get([
      'id',
      'name',
      'identify',
      'year',
      'version',
      'subjectCategory',
      'dimension',
      'exchangeFrequency',
      'infoClassify',
      'infoCategory',
      'description',
      'items',
      'updateTime',
      'createTime',
      'crew',
    ])
}

export function translateGbTemplateVersionList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new GbTemplateVersion(item)
      .get([
        'id',
        'version',
        'description',
        'createTime',
        'updateTime',
        'currentRelease',
        'addItems',
        'editItems',
        'deleteItems',
        'attachment',
        'crew',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateWbjTemplateList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new GbTemplate(item)
      .get([
        'id',
        'name',
        'sourceUnit',
        'subjectCategory',
        'identify',
        'dimension',
        'infoClassify',
        'updateTime',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateWbjTemplateDetail (res = {}) {
  return new GbTemplate(res)
    .get([
      'id',
      'name',
      'identify',
      'year',
      'version',
      'sourceUnit',
      'subjectCategory',
      'dimension',
      'exchangeFrequency',
      'infoClassify',
      'infoCategory',
      'description',
      'items',
      'updateTime',
      'createTime',
      'crew',
    ])
}

export function translateWbjTemplateVersionList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new GbTemplateVersion(item)
      .get([
        'id',
        'version',
        'description',
        'createTime',
        'updateTime',
        'currentRelease',
        'addItems',
        'editItems',
        'deleteItems',
        'attachment',
        'crew',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateBjTemplateList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new GbTemplate(item)
      .get([
        'id',
        'name',
        'identify',
        'category',
        'gbTemplate',
        'dimension',
        'subjectCategory',
        'infoClassify',
        'updateTime',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateBjTemplateDetail (res = {}) {
  return new GbTemplate(res)
    .get([
      'id',
      'name',
      'identify',
      'year',
      'version',
      'category',
      'subjectCategory',
      'dimension',
      'gbTemplate',
      'exchangeFrequency',
      'infoClassify',
      'infoCategory',
      'description',
      'items',
      'sourceUnit',
      'updateTime',
      'createTime',
      'crew',
    ])
}

export function translateQzjTemplateDetail (res = {}) {
  return new GbTemplate(res)
    .get([
      'id',
      'name',
      'identify',
      'year',
      'version',
      'category',
      'subjectCategory',
      'dimension',
      'exchangeFrequency',
      'infoClassify',
      'infoCategory',
      'description',
      'items',
      'sourceUnit',
      'updateTime',
      'createTime',
      'crew',
    ])
}

export function translateBjTemplateVersionList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new GbTemplateVersion(item)
      .get([
        'id',
        'version',
        'description',
        'createTime',
        'updateTime',
        'currentRelease',
        'addItems',
        'editItems',
        'deleteItems',
        'attachment',
        'crew',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateAdminWorkOrderTaskList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new ParentTasks(item)
      .get([
        'id',
        'title',
        'templateType',
        'template',
        'ratio',
        'updateTime',
        'endTime',
      ]))
    : []
  return {
    list: tempList,
    total,
  }
}

export function translateUserGroupWorkOrderTaskList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new WorkOrderTask(item)
      .get([
        'id',
        'title',
        'template',
        'parentTask',
        'status',
        'updateTime',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateSubtasksList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Subtasks(item)
      .get([
        'id',
        'assignObject',
        'template',
        'parentTask',
        'status',
        'updateTime',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateWorkOrderTasksDetail (res = {}) {
  return new WorkOrderTask(res)
    .get([
      'id',
      'title',
      'parentTask',
      'template',
      'templateType',
      'reason',
      'compareResult',
      'feedbackRecords',
      'assignObject',
      'status',
      'endTime',
      'updateTime',
      'createTime',
    ])
}

const isValidField = field => typeof field === 'object' &&
field.name !== undefined &&
field.id !== undefined

export function dehydrate (field, { filterLw } = {}) {
  if (!isValidField(field)) return ''
  if (filterLw && field.id === 'Lw') return ''
  return field.id
}

export function translateBjTemplateEdit (res = {}) {
  const {
    id,
    name,
    identify,
    version,
    description,
  } = res

  const gbTemplate = dehydrate(res.gbTemplate)
  const dimension = dehydrate(res.dimension)
  const sourceUnit = dehydrate(res.sourceUnit)
  const exchangeFrequency = dehydrate(res.exchangeFrequency)
  const infoClassify = dehydrate(res.infoClassify)
  const infoCategory = dehydrate(res.infoCategory)
  const subjectCategory = Array.isArray(res.subjectCategory)
    ? res.subjectCategory.map(dehydrate)
    : []
  const items = Array.isArray(res.items)
    ? res.items.map(item => {
      const {
        type,
        dimension,
        isMasked,
        isNecessary,
      } = item

      return {
        ...item,
        dimension: dehydrate(dimension),
        type: dehydrate(type),
        isMasked: dehydrate(isMasked),
        isNecessary: dehydrate(isNecessary),
      }
    })
    : []

  return {
    gbTemplate,
    id,
    name,
    identify,
    version,
    sourceUnit,
    subjectCategory,
    dimension,
    exchangeFrequency,
    infoClassify,
    infoCategory,
    description,
    items,
  }
}

export function translateQzjTemplateEdit (res = {}) {
  const {
    id,
    name,
    identify,
    version,
    description,
  } = res

  const category = dehydrate(res.category)
  const dimension = dehydrate(res.dimension)
  const sourceUnit = dehydrate(res.sourceUnit)
  const exchangeFrequency = dehydrate(res.exchangeFrequency)
  const infoClassify = dehydrate(res.infoClassify)
  const infoCategory = dehydrate(res.infoCategory)
  const subjectCategory = Array.isArray(res.subjectCategory)
    ? res.subjectCategory.map(dehydrate)
    : []
  const items = Array.isArray(res.items)
    ? res.items.map(item => {
      const {
        type,
        dimension,
        isMasked,
        isNecessary,
      } = item

      return {
        ...item,
        dimension: dehydrate(dimension),
        type: dehydrate(type),
        isMasked: dehydrate(isMasked),
        isNecessary: dehydrate(isNecessary),
      }
    })
    : []

  return {
    id,
    name,
    category,
    identify,
    version,
    sourceUnit,
    subjectCategory,
    dimension,
    exchangeFrequency,
    infoClassify,
    infoCategory,
    description,
    items,
  }
}

export function translateGbTemplateEdit (res = {}) {
  const {
    id,
    name,
    identify,
    version,
    description,
  } = res

  const dimension = dehydrate(res.dimension)
  const exchangeFrequency = dehydrate(res.exchangeFrequency)
  const infoClassify = dehydrate(res.infoClassify)
  const infoCategory = dehydrate(res.infoCategory, { filterLw: true })
  const subjectCategory = Array.isArray(res.subjectCategory)
    ? res.subjectCategory.map(dehydrate)
    : []
  const items = Array.isArray(res.items)
    ? res.items.map(item => {
      const {
        type,
        dimension,
        isMasked,
        isNecessary,
      } = item

      return {
        ...item,
        dimension: dehydrate(dimension),
        type: dehydrate(type),
        isMasked: dehydrate(isMasked),
        isNecessary: dehydrate(isNecessary),
      }
    })
    : []

  return {
    id,
    name,
    identify,
    version,
    subjectCategory,
    dimension,
    exchangeFrequency,
    infoClassify,
    infoCategory,
    description,
    items,
  }
}

/**
 * 翻译反馈记录模板项
 * @param {array} res 反馈记录模板项
 */
export function translateFeedbackRecordItems (res = []) {
  if (!Array.isArray(res)) return []

  const result = res.map(item => ({
    ...item,
    type: dehydrate(item.type),
    dimension: dehydrate(item.dimension),
    isMasked: dehydrate(item.isMasked),
    isNecessary: dehydrate(item.isNecessary),
  }))

  return result
}
