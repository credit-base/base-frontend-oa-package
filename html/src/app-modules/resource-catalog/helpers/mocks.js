/* istanbul ignore file */
/* eslint-disable max-lines */

export const gbTemplateList = [
  {
    id: 'MA',
    name: '登记信息',
    identify: 'FR_DJXX',
    subjectCategory: [
      {
        id: 'MA',
        name: '自然人',
      },
      {
        id: 'MQ',
        name: '个体工商户',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    infoClassify: {
      id: 'MA',
      name: '行政处罚',
    },
    version: '1-D012A100C101',
    updateTime: 1576317765,
  },
  {
    id: 'MQ',
    name: '变更登记信息',
    identify: 'FR_DJBGXX',
    subjectCategory: [
      {
        id: 'MA',
        name: '自然人',
      },
      {
        id: 'MQ',
        name: '个体工商户',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    infoClassify: {
      id: 'MA',
      name: '行政处罚',
    },
    version: '2-D012A100C101',
    updateTime: 1576317765,
  },
]

export const gbTemplateDetail = {
  id: 'MA',
  name: '登记机关',
  identify: 'DJJG',
  year: '2019 年版',
  version: '1-D012A100C101',
  subjectCategory: [
    {
      id: 'MA',
      name: '自然人',
    },
    {
      id: 'MQ',
      name: '个体工商户',
    },
  ],
  dimension: {
    id: 'MA',
    name: '政务共享',
  },
  exchangeFrequency: {
    id: 'MA',
    name: '每一年',
  },
  infoClassify: {
    id: 'MA',
    name: '行政处罚',
  },
  infoCategory: {
    id: 'MA',
    name: '基础信息',
  },
  description: '为贯彻落实 《国务院关于印发社会信用体系建设规划纲 要 (201⒋20⒛ 年 )的 通知》(国 发 〔2014〕 21号 ),按 照《国 务院办公厅关于运用太数据加强对市场主体服务和监管的 若干意见》(国 办发 〔2015〕 51号 )和 《国家发展改革委办 公厅关于运用太数据技术开展城市信用监测工作的通知》(发 改办财金 〔⒛16〕 14⒆ 号)有 关要求,我 委建立了全国 城市信用状况监测预警平台,对全国“1个县级以上城市开 展在线监测评价,在促进城市提升信用体系建设方面,取得 了显著成效。为继续深化城市信用体系建设,我们在原指标 基础上,进 一步细化和完善了相关指标,丰 富了监测内容, 提升了监测科学性。',
  items: [
    {
      name: '主体名称',
      identify: 'ZTMC',
      type: 'C(字符型)',
      length: '200',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '主体类别',
      identify: 'ZTLB',
      type: 'C(字符型)',
      length: '2',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '统一社会信用代码',
      identify: 'TYSHXYDM',
      type: 'C(字符型)',
      length: '18',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '法定代表人（负责人）',
      identify: 'FDDBR',
      type: 'C(字符型)',
      length: '100',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '法定代表人（负责人） 证件类型',
      identify: 'FDDBRZJLX',
      type: 'C(字符型)',
      length: '100',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '成立日期',
      identify: 'CLRQ',
      type: 'D(日期型)',
      length: '',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '有效期',
      identify: 'YXQ',
      type: 'D(日期型)',
      length: '',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '地址',
      identify: 'DZ',
      type: 'C(字符型)',
      length: '200',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '登记机关',
      identify: 'DJJG',
      type: 'C(字符型)',
      length: '200',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '国别(地区)',
      identify: 'DJJG',
      type: 'GB',
      length: '3',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '注册资本',
      identify: 'ZCZB',
      type: 'N(数值型)',
      length: '(24,6)',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '注册资本币种',
      identify: 'ZCZBBZ',
      type: 'C(字符型)',
      length: '3',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '行业代码',
      identify: 'HYDM',
      type: 'C(字符型)',
      length: '5',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '类型',
      identify: 'LX',
      type: 'C(字符型)',
      length: '3',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '经营范围',
      identify: 'JYFW',
      type: 'C(字符型)',
      length: '2000',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '经营状态',
      identify: 'JYZT',
      type: 'C(字符型)',
      length: '1',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '经营范围描述',
      identify: 'JYFWMS',
      type: 'C(字符型)',
      length: '2000',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
  ],
  updateTime: 1576317765,
  createTime: 1576317765,
  crew: {
    id: 'NDQ',
    realName: '管理员',
  },
}

export const gbTemplateVersionList = [
  {
    id: 'MA',
    version: '1-D012A100C101',
    description: '公厅关于运用太数据技术开展城市信用监测工作的通知》(发 改办财金 〔⒛16〕 14⒆ 号)有 关要求,我 委建立了全国 城市信用状况监测预警平台,对全国“1个县级以上城市开',
    updateTime: 1576317765,
    createTime: 1576317765,
    currentRelease: true,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
  {
    id: 'MQ',
    version: '2-D012A100C101',
    description: '2020年版',
    updateTime: 1576317785,
    createTime: 1576317785,
    currentRelease: false,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
  {
    id: 'Mg',
    version: '3-D012A100C101',
    description: '2021年版',
    updateTime: 1576317920,
    createTime: 1576317920,
    currentRelease: false,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
]

export const bjTemplateList = [
  {
    id: 'MA',
    name: '登记机关',
    category: `MA`,
    gbTemplate: {
      id: 'MA',
      name: '国标目录名称',
    },
    subjectCategory: [
      {
        id: 'MA',
        name: '自然人',
      },
      {
        id: 'MQ',
        name: '个体工商户',
      },
    ],
    identify: 'DJJG',
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    infoClassify: {
      id: 'MA',
      name: '行政处罚',
    },
    updateTime: 1576317765,
  },
]

export const bjTemplateDetail = {
  id: 'MA',
  name: '目录名称',
  identify: '目录标识',
  category: `MA`,
  sourceUnit: {
    id: 'MA',
    name: '发改委',
  },
  year: '2019 年版',
  version: '1-D012A100C101',
  gbTemplate: {
    id: 'MA',
    name: '国标目录名称',
  },
  subjectCategory: [
    {
      id: 'MA',
      name: '自然人',
    },
    {
      id: 'MQ',
      name: '个体工商户',
    },
  ],
  dimension: {
    id: 'MA',
    name: '政务共享',
  },
  exchangeFrequency: {
    id: 'MA',
    name: '每一年',
  },
  infoClassify: {
    id: 'MA',
    name: '行政处罚',
  },
  infoCategory: {
    id: 'MA',
    name: '基础信息',
  },
  description: '目录描述',
  items: [
    {
      name: '信用主体名称',
      identify: 'ZTMC',
      type: '字符型',
      length: '200',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '统一社会信用代码',
      identify: 'TYSHXYDM',
      type: '字符型',
      length: '200',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [3, 3],
      remark: '',
    },
    {
      name: '身份证号',
      identify: 'ZJHM',
      type: '字符型',
      length: '200',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [4, 3],
      remark: '',
    },
  ],
  updateTime: 1576317765,
  createTime: 1576317765,
  crew: {
    id: 'NDQ',
    realName: '管理员',
  },
}

export const bjTemplateVersionList = [
  {
    id: 'MA',
    version: '1-D012A100C101',
    description: '公厅关于运用太数据技术开展城市信用监测工作的通知》(发 改办财金 〔⒛16〕 14⒆ 号)有 关要求,我 委建立了全国 城市信用状况监测预警平台,对全国“1个县级以上城市开',
    updateTime: 1576317765,
    createTime: 1576317765,
    currentRelease: true,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
  {
    id: 'MQ',
    version: '2-D012A100C101',
    description: '2020年版',
    updateTime: 1576317785,
    createTime: 1576317785,
    currentRelease: false,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
  {
    id: 'Mg',
    version: '3-D012A100C101',
    description: '2021年版',
    updateTime: 1576317920,
    createTime: 1576317920,
    currentRelease: false,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
]

export const wbjTemplateList = [
  {
    id: 'MA',
    name: '登记机关',
    identify: 'DJJG',
    sourceUnit: {
      id: 'MQ',
      name: '市场监督管理局',
    },
    subjectCategory: [
      {
        id: 'MA',
        name: '自然人',
      },
      {
        id: 'MQ',
        name: '个体工商户',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    infoClassify: {
      id: 'MA',
      name: '行政处罚',
    },
    updateTime: 1576317765,
  },
]

export const wbjTemplateDetail = {
  id: 'MA',
  name: '目录名称',
  identify: 'DJJG',
  year: '2019 年版',
  version: '1-D012A100C101',
  subjectCategory: [
    {
      id: 'MA',
      name: '自然人',
    },
    {
      id: 'MQ',
      name: '个体工商户',
    },
  ],
  dimension: {
    id: 'MA',
    name: '政务共享',
  },
  exchangeFrequency: {
    id: 'MA',
    name: '每一年',
  },
  infoClassify: {
    id: 'MA',
    name: '行政处罚',
  },
  infoCategory: {
    id: 'MA',
    name: '基础信息',
  },
  description: '目录描述',
  items: [
    {
      name: '信用主体名称',
      identify: 'ZTMC',
      type: '字符型',
      length: '200',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [1, 2],
      remark: '',
    },
    {
      name: '统一社会信用代码',
      identify: 'TYSHXYDM',
      type: '字符型',
      length: '200',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [3, 3],
      remark: '',
    },
    {
      name: '身份证号',
      identify: 'ZJHM',
      type: '字符型',
      length: '200',
      options: [
        {
          id: 'MA',
          name: '存续',
        },
      ],
      dimension: {
        id: 'MA',
        name: '政务共享',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [4, 3],
      remark: '',
    },
  ],
  updateTime: 1576317765,
  createTime: 1576317765,
  crew: {
    id: 'NDQ',
    realName: '管理员',
  },
  sourceUnit: {
    id: 'MQ',
    name: '市场监督管理局',
  },
}

export const wbjTemplateVersionList = [
  {
    id: 'MA',
    version: '1-D012A100C101',
    description: '公厅关于运用太数据技术开展城市信用监测工作的通知》(发 改办财金 〔⒛16〕 14⒆ 号)有 关要求,我 委建立了全国 城市信用状况监测预警平台,对全国“1个县级以上城市开',
    updateTime: 1576317765,
    createTime: 1576317765,
    currentRelease: true,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
  {
    id: 'MQ',
    version: '2-D012A100C101',
    description: '2020年版',
    updateTime: 1576317785,
    createTime: 1576317785,
    currentRelease: false,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
  {
    id: 'Mg',
    version: '3-D012A100C101',
    description: '2021年版',
    updateTime: 1576317920,
    createTime: 1576317920,
    currentRelease: false,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
]

export const compareResult = [
  {
    compareCategory: 'add',
    name: '主体名称',
    identify: 'ZTMC',
    type: 'C(字符型)',
    length: '200',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    compareCategory: 'add',
    name: '主体类别',
    identify: 'ZTLB',
    type: 'C(字符型)',
    length: '2',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    compareCategory: 'add',
    name: '统一社会信用代码',
    identify: 'TYSHXYDM',
    type: 'C(字符型)',
    length: '18',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    compareCategory: 'edit',
    latestRelease: {
      isNecessary: {
        id: 'Lw',
        name: '否',
      },
    },
    name: '法定代表人（负责人）',
    identify: 'FDDBR',
    type: 'C(字符型)',
    length: '100',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    compareCategory: 'edit',
    latestRelease: {
      dimension: {
        id: 'MQ',
        name: '社会公开',
      },
    },
    name: '法定代表人（负责人） 证件类型',
    identify: 'FDDBRZJLX',
    type: 'C(字符型)',
    length: '100',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    compareCategory: 'edit',
    latestRelease: {
      name: '创建日期',
      identify: 'CJRQ',
    },
    name: '成立日期',
    identify: 'CLRQ',
    type: 'D(日期型)',
    length: '',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    compareCategory: 'delete',
    name: '有效期',
    identify: 'YXQ',
    type: 'D(日期型)',
    length: '',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    compareCategory: 'delete',
    name: '地址',
    identify: 'DZ',
    type: 'C(字符型)',
    length: '200',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    compareCategory: 'delete',
    name: '登记机关',
    identify: 'DJJG',
    type: 'C(字符型)',
    length: '200',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    compareCategory: 'delete',
    name: '国别(地区)',
    identify: 'DJJG',
    type: 'GB',
    length: '3',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
]

export const workOrderTasksList = [
  {
    id: 'MA',
    title: '上报2020年度资源目录',
    template: {
      id: 'MA',
      name: '企业年报信息',
    },
    templateType: {
      id: 'MA',
      name: '国标',
    },
    ratio: 0,
    status: {
      id: 'MA',
      name: '待确认',
    },
    createTime: 1576317765,
    updateTime: 1576317765,
  },
  {
    id: 'MQ',
    title: '上报2020年度资源目录',
    template: {
      id: 'MQ',
      name: '纳税信息',
    },
    templateType: {
      id: 'MQ',
      name: '委办局',
    },
    ratio: 60,
    status: {
      id: 'MQ',
      name: '已确认',
    },
    createTime: 1576317765,
    updateTime: 1576317765,
  },
]

export const subtasksList = [
  {
    id: 'MA',
    assignObject: {
      id: 'MA',
      name: '发展和改革委员会',
    },
    status: {
      id: 'Mg',
      name: '跟进中',
    },
    updateTime: 1576317765,
  },
  {
    id: 'MQ',
    assignObject: {
      id: 'Mg',
      name: '监督管理局',
    },
    status: {
      id: 'Mg',
      name: '待确认',
    },
    updateTime: 1576317765,
  },
]

export const workOrderTasksDetail = {
  title: '上报2020年度资源目录',
  reason: '',
  parentTask: {
    id: 'MA',
    templateType: 1,
    title: '归集地方性黑名单信息',
    description: '依据国家要求，现需要按要求归集地方性黑名单信息',
    endTime: '2020-01-01',
    attachment: {
      name: '国203号文',
      identify: 'b900638af896a26de13bc89ce4f64124.pdf',
    },
    ratio: 50,
    reason: '',
    status: 0,
  },
  assignObject: {
    id: 'MA',
    name: '委办局名称',
  },
  template: {
    id: 'MA',
    name: '地方性黑名单',
    identify: 'DFXHMD',
    subjectCategory: [
      1,
      3,
    ],
    dimension: 1,
    exchangeFrequency: 1,
    infoClassify: 1,
    infoCategory: 1,
    description: '目录描述信息',
    items: [
      {
        name: '主体名称',
        identify: 'ZTMC',
        type: 1,
        length: 200,
        options: [],
        dimension: 1,
        isNecessary: 1,
        isMasked: 0,
        maskRule: [],
        remarks: '信用主体名称',
      },
    ],
  },
  templateType: {
    id: 'MA',
    name: '国标',
  },
  feedbackRecords: [
    {
      crew: 2,
      crewName: '人社局管理员',
      assignObject: 3,
      userGroupName: '人力和社会资源保障局',
      isExistedTemplate: 1,
      templateId: 1,
      items: [
        {
          name: '主体名称',
          identify: 'ZTMC',
          type: 1,
          length: 200,
          options: [],
          dimension: 1,
          isNecessary: 1,
          isMasked: 0,
          maskRule: [],
          remarks: '信用主体名称',
        },
        {
          name: '统一社会信用代码',
          identify: 'TYSHXYDM',
          type: 1,
          length: 50,
          options: [],
          dimension: 1,
          isNecessary: 1,
          isMasked: 1,
          maskRule: [
            3,
            4,
          ],
          remarks: '信用主体代码',
        },
        {
          name: '信息类别',
          identify: 'XXLB',
          type: 5,
          length: 50,
          options: [
            '基础信息',
            '守信信息',
            '失信信息',
            '其他信息',
          ],
          dimension: 2,
          isNecessary: 1,
          isMasked: 1,
          maskRule: [
            1,
            2,
          ],
          remarks: '信息性质类型，支持单选',
        },
      ],
      reason: '人力和社会资源保障局的反馈原因',
      feedbackTime: 1614422868,
    },
    {
      crew: 1,
      crewName: '发改委管理员',
      userGroup: 1,
      userGroupName: '发展和改革委员会',
      isExistedTemplate: 1,
      templateId: 1,
      items: [
        {
          name: '主体名称',
          identify: 'ZTMC',
          type: 1,
          length: 200,
          options: [],
          dimension: 1,
          isNecessary: 1,
          isMasked: 0,
          maskRule: [],
          remarks: '信用主体名称',
        },
        {
          name: '统一社会信用代码',
          identify: 'TYSHXYDM',
          type: 1,
          length: 50,
          options: [],
          dimension: 1,
          isNecessary: 1,
          isMasked: 1,
          maskRule: [
            3,
            4,
          ],
          remarks: '信用主体代码',
        },
        {
          name: '信息类别',
          identify: 'XXLB',
          type: 5,
          length: 50,
          options: [
            '基础信息',
            '守信信息',
            '失信信息',
            '其他信息',
          ],
          dimension: 2,
          isNecessary: 1,
          isMasked: 1,
          maskRule: [
            1,
            2,
          ],
          remarks: '信息性质类型，支持单选',
        },
      ],
      reason: '发展和改革委员会的反馈原因',
      feedbackTime: 1614509268,
    },
  ],
  status: {
    id: 'MA',
    name: '待确认',
  },
  createTime: 1531897790,
  updateTime: 1531897790,
  endTime: 1531897790,
  statusTime: 0,
}

export const feedbackTemplate = [
  {
    feedbackType: 'add',
    name: '主体名称',
    identify: 'ZTMC',
    type: 'C(字符型)',
    length: '200',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    feedbackType: 'add',
    name: '主体类别',
    identify: 'ZTLB',
    type: 'C(字符型)',
    length: '2',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    feedbackType: 'add',
    name: '统一社会信用代码',
    identify: 'TYSHXYDM',
    type: 'C(字符型)',
    length: '18',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    feedbackType: 'edit',
    latestRelease: {
      isNecessary: {
        id: 'Lw',
        name: '否',
      },
    },
    name: '法定代表人（负责人）',
    identify: 'FDDBR',
    type: 'C(字符型)',
    length: '100',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    feedbackType: 'edit',
    latestRelease: {
      dimension: {
        id: 'MQ',
        name: '社会公开',
      },
    },
    name: '法定代表人（负责人） 证件类型',
    identify: 'FDDBRZJLX',
    type: 'C(字符型)',
    length: '100',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    feedbackType: 'edit',
    latestRelease: {
      name: '创建日期',
      identify: 'CJRQ',
    },
    name: '成立日期',
    identify: 'CLRQ',
    type: 'D(日期型)',
    length: '',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    feedbackType: 'delete',
    name: '有效期',
    identify: 'YXQ',
    type: 'D(日期型)',
    length: '',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    feedbackType: 'delete',
    name: '地址',
    identify: 'DZ',
    type: 'C(字符型)',
    length: '200',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    feedbackType: 'delete',
    name: '登记机关',
    identify: 'DJJG',
    type: 'C(字符型)',
    length: '200',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
  {
    feedbackType: 'delete',
    name: '国别(地区)',
    identify: 'DJJG',
    type: 'GB',
    length: '3',
    options: [
      {
        id: 'MA',
        name: '存续',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政务共享',
    },
    isNecessary: {
      id: 'MA',
      name: '是',
    },
    isMasked: {
      id: 'Lw',
      name: '否',
    },
    maskRule: [1, 2],
  },
]
