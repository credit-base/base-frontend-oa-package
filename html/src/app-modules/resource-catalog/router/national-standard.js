/**
 * @file 国标资源目录
 * @module resource-catalog/gbTemplate
 */
import permissions from '@/constants/permissions'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const {
  STATUS_NATIONAL_STANDARD_RESOURCE_CATALOG,
  STATUS_NATIONAL_STANDARD_VERSION_COMPARE,
} = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/gb-template/version-compare/:id/:origin/:target',
    name: 'GbTemplateVersionCompare',
    hidden: true,
    meta: {
      title: 'resourceCatalog.ROUTER.VERSION_COMPARE',
      activeMenu: '/resource-catalog/gb-template',
      isVisible: STATUS_NATIONAL_STANDARD_VERSION_COMPARE,
    },
    component: () => import('@/app-modules/resource-catalog/views/detail/gb-template-version-compare'),
  },
  {
    path: '/resource-catalog/gb-template/edit/:id',
    name: 'GbTemplateEdit',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/gb-template',
      title: 'resourceCatalog.ROUTER.NATIONAL_STANDARD_RESOURCE_CATALOG_EDIT',
    },
    component: () => import('@/app-modules/resource-catalog/views/edit/gb-template'),
  },
  {
    path: '/resource-catalog/gb-template/add',
    name: 'GbTemplateAdd',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/gb-template',
      title: 'resourceCatalog.ROUTER.NATIONAL_STANDARD_RESOURCE_CATALOG_ADD',
    },
    component: () => import('@/app-modules/resource-catalog/views/add/gb-template'),
  },
  {
    path: '/resource-catalog/gb-template/detail/:id',
    name: 'GbTemplateDetail',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/gb-template',
      title: 'resourceCatalog.ROUTER.NATIONAL_STANDARD_RESOURCE_CATALOG_DETAIL',
    },
    component: () => import('@/app-modules/resource-catalog/views/detail/gb-template'),
  },
  {
    path: '/resource-catalog/gb-template',
    name: 'GbTemplateList',
    meta: {
      title: 'resourceCatalog.ROUTER.NATIONAL_STANDARD_RESOURCE_CATALOG',
      icon: 'gb-template',
      activeMenu: '/resource-catalog/gb-template',
      isVisible: STATUS_NATIONAL_STANDARD_RESOURCE_CATALOG,
      routerId: permissions.GB_TEMPLATE_ID,
    },
    component: () => import('@/app-modules/resource-catalog/views/list/gb-template'),
  },
]
