/**
 * @file 前置机目录管理
 * @module resource-catalog/qzjTemplate
 */

import permissions from '@/constants/permissions'
// import { ROUTER_VISIBLE_CONFIG } from '@/settings'

export default [
  {
    path: `/qzj-template/list`,
    name: `QzjTemplateList`,
    meta: {
      title: 'resourceCatalog.ROUTER.QZJ_TEMPLATE',
      icon: 'bj-template',
      // isVisible: STATUS_CURRENT_UNIT_RESOURCE_CATALOG,
      routerId: permissions.BJ_TEMPLATE_ID,
    },
    component: () => import('@/app-modules/resource-catalog/views/list/qzj-template'),
  },
  {
    path: '/qzj-template/detail/:id',
    name: 'QzjTemplateDetail',
    hidden: true,
    meta: {
      activeMenu: '/qzj-template/list',
      title: 'resourceCatalog.ROUTER.QZJ_TEMPLATE_DETAIL',
    },
    component: () => import('@/app-modules/resource-catalog/views/detail/qzj-template'),
  },
  {
    path: '/qzj-template/add/',
    name: 'QzjTemplateAdd',
    hidden: true,
    meta: {
      activeMenu: '/qzj-template/list',
      title: 'resourceCatalog.ROUTER.QZJ_TEMPLATE_ADD',
    },
    component: () => import('@/app-modules/resource-catalog/views/add/qzj-template'),
  },
  {
    path: '/qzj-template/edit/:id',
    name: 'QzjTemplateEdit',
    hidden: true,
    meta: {
      activeMenu: '/qzj-template/list',
      title: 'resourceCatalog.ROUTER.QZJ_TEMPLATE_EDIT',
    },
    component: () => import('@/app-modules/resource-catalog/views/edit/qzj-template'),
  },
]
