/**
 * @file 资源目录管理
 * @module async-routes/resource-catalog
 */

import currentUnit from './current-unit'
import nationalStandard from './national-standard'
import userGroup from './user-group'
import frontendProcessor from './frontend-processor'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const { STATUS_RESOURCE_CATALOG_SUBSYSTEM } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/resource-catalog-subsystem',
    name: 'ResourceCatalogSubsystem',
    component: () => import('@/app-modules/resource-catalog/views/index.vue'),
    alwaysShow: true,
    meta: {
      title: 'resourceCatalog.ROUTER.RESOURCE_CATALOG_SUBSYSTEM',
      icon: 'resource-catalog-subsystem',
      breadcrumb: false,
      isVisible: STATUS_RESOURCE_CATALOG_SUBSYSTEM,
    },
    children: [
      ...nationalStandard,
      ...currentUnit,
      ...userGroup,
      ...frontendProcessor,
    ],
  },
]
