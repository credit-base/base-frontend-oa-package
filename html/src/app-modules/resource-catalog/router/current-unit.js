/**
 * @file 本级资源目录
 * @module resource-catalog/bjTemplate
 */

import permissions from '@/constants/permissions'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const {
  STATUS_CURRENT_UNIT_RESOURCE_CATALOG,
  STATUS_CURRENT_UNIT_VERSION_COMPARE,
} = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/bj-template/version-compare/:id/:origin/:target',
    name: 'BjTemplateVersionCompare',
    hidden: true,
    meta: {
      title: 'resourceCatalog.ROUTER.VERSION_COMPARE',
      activeMenu: '/resource-catalog/bj-template',
      isVisible: STATUS_CURRENT_UNIT_VERSION_COMPARE,
    },
    component: () => import('@/app-modules/resource-catalog/views/detail/bj-template-version-compare'),
  },
  {
    path: '/resource-catalog/bj-template/edit/:id',
    name: 'BjTemplateEdit',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/bj-template',
      title: 'resourceCatalog.ROUTER.CURRENT_UNIT_RESOURCE_CATALOG_EDIT',
    },
    component: () => import('@/app-modules/resource-catalog/views/edit/bj-template'),
  },
  {
    path: '/resource-catalog/bj-template/add',
    name: 'BjTemplateAdd',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/bj-template',
      title: 'resourceCatalog.ROUTER.CURRENT_UNIT_RESOURCE_CATALOG_ADD',
    },
    component: () => import('@/app-modules/resource-catalog/views/add/bj-template'),
  },
  {
    path: '/resource-catalog/bj-template/detail/:id',
    name: 'BjTemplateDetail',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/bj-template',
      title: 'resourceCatalog.ROUTER.CURRENT_UNIT_RESOURCE_CATALOG_DETAIL',
    },
    component: () => import('@/app-modules/resource-catalog/views/detail/bj-template'),
  },
  {
    path: '/resource-catalog/bj-template',
    name: 'BjTemplateList',
    meta: {
      title: 'resourceCatalog.ROUTER.CURRENT_UNIT_RESOURCE_CATALOG',
      icon: 'bj-template',
      activeMenu: '/resource-catalog/bj-template',
      isVisible: STATUS_CURRENT_UNIT_RESOURCE_CATALOG,
      routerId: permissions.BJ_TEMPLATE_ID,
    },
    component: () => import('@/app-modules/resource-catalog/views/list/bj-template'),
  },
]
