/**
 * @file 委办局资源目录
 * @module resource-catalog/wbjTemplate
 */

import permissions from '@/constants/permissions'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const {
  STATUS_USER_GROUP_RESOURCE_CATALOG,
  STATUS_USER_GROUP_VERSION_COMPARE,
  STATUS_SUBTASKS,
  STATUS_FEEDBACK_TEMPLATE,
} = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/feedback-user-group/:id',
    name: 'OrderTaskFeedBack',
    hidden: true,
    meta: {
      title: 'resourceCatalog.ROUTER.ORDER_TASK_FEEDBACK',
      activeMenu: '/resource-catalog/wbj-template',
      isVisible: STATUS_USER_GROUP_RESOURCE_CATALOG,
    },
    component: () => import('@/app-modules/resource-catalog/views/feedback'),
  },
  {
    path: '/feedback-admin/:id',
    name: 'AdminOrderTaskFeedBack',
    hidden: true,
    meta: {
      title: 'resourceCatalog.ROUTER.ORDER_TASK_FEEDBACK',
      activeMenu: '/resource-catalog/wbj-template',
      isVisible: STATUS_USER_GROUP_RESOURCE_CATALOG,
    },
    component: () => import('@/app-modules/resource-catalog/views/feedback-admin'),
  },
  {
    path: '/feedback-template/:id/:key/:sort',
    name: 'FeedBackTemplate',
    hidden: true,
    meta: {
      title: 'resourceCatalog.ROUTER.FEEDBACK_TEMPLATE',
      activeMenu: '/resource-catalog/wbj-template',
      isVisible: STATUS_FEEDBACK_TEMPLATE,
    },
    component: () => import('@/app-modules/resource-catalog/views/detail/order-task-feedback-template'),
  },
  {
    path: '/wbj-template/subtasks/:id',
    name: 'Subtasks',
    hidden: true,
    meta: {
      title: 'resourceCatalog.ROUTER.SUBTASKS',
      activeMenu: '/resource-catalog/wbj-template',
      isVisible: STATUS_SUBTASKS,
    },
    component: () => import('@/app-modules/resource-catalog/views/list/list-view-subtasks'),
  },
  {
    path: '/wbj-template/version-compare/:id/:origin/:target',
    name: 'WbjTemplateVersionCompare',
    hidden: true,
    meta: {
      title: 'resourceCatalog.ROUTER.VERSION_COMPARE',
      activeMenu: '/resource-catalog/bj-template',
      isVisible: STATUS_USER_GROUP_VERSION_COMPARE,
    },
    component: () => import('@/app-modules/resource-catalog/views/detail/wbj-template-version-compare'),
  },
  // 工单任务
  {
    path: '/resource-catalog/order-task/detail/:id',
    name: 'OrderTaskResourceCatalogDetail',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/wbj-template',
      title: 'resourceCatalog.ROUTER.ORDER_TASK_RESOURCE_CATALOG_DETAIL',
    },
    component: () => import('@/app-modules/resource-catalog/views/detail/order-task'),
  },
  {
    path: '/resource-catalog/order-task/add',
    name: 'OrderTaskResourceCatalogAdd',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/wbj-template',
      title: 'resourceCatalog.ROUTER.ORDER_TASK_RESOURCE_CATALOG_ASSIGN',
    },
    component: () => import('@/app-modules/resource-catalog/views/add/order-task'),
  },
  // 委办局资源目录
  {
    path: '/resource-catalog/wbj-template/edit/:id',
    name: 'WbjTemplateEdit',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/wbj-template',
      title: 'resourceCatalog.ROUTER.USER_GROUP_RESOURCE_CATALOG_EDIT',
    },
    component: () => import('@/app-modules/resource-catalog/views/edit/wbj-template'),
  },
  {
    path: '/resource-catalog/wbj-template/add',
    name: 'WbjTemplateAdd',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/wbj-template',
      title: 'resourceCatalog.ROUTER.USER_GROUP_RESOURCE_CATALOG_ADD',
    },
    component: () => import('@/app-modules/resource-catalog/views/add/wbj-template'),
  },
  {
    path: '/resource-catalog/wbj-template/detail/:id',
    name: 'WbjTemplateDetail',
    hidden: true,
    meta: {
      activeMenu: '/resource-catalog/wbj-template',
      title: 'resourceCatalog.ROUTER.USER_GROUP_RESOURCE_CATALOG_DETAIL',
    },
    component: () => import('@/app-modules/resource-catalog/views/detail/wbj-template'),
  },
  {
    path: '/resource-catalog/wbj-template',
    name: 'WbjTemplateList',
    meta: {
      title: 'resourceCatalog.ROUTER.USER_GROUP_RESOURCE_CATALOG',
      icon: 'wbj-template',
      activeMenu: '/resource-catalog/wbj-template',
      isVisible: STATUS_USER_GROUP_RESOURCE_CATALOG,
      routerId: permissions.WBJ_TEMPLATE_ID,
    },
    component: () => import('@/app-modules/resource-catalog/views/list/wbj-template'),
  },
]
