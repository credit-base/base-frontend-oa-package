/**
 * @file 接口 - 前置机目录管理
 * @module resource-catalog/qzjTemplate
 */

import request from '@/utils/request'

/**
 * 前置机目录列表
 */
export function getQzjTemplateList (params = {}) {
  return request('/api/qzjTemplates', { params })
}

/**
 * 前置机目录详情
 * @param {string} id 前置机目录id
 */
export function getQzjTemplateDetail (id, params = {}) {
  return request(`/api/qzjTemplates/${id}`, { params })
}

/**
 * 新增前置机目录
 */
export function addQzjTemplate (params = {}) {
  return request(`/api/qzjTemplates/add`, { method: 'POST', params })
}

/**
 * 编辑前置机目录
 * @param {string} id 前置机目录id
 */
export function editQzjTemplate (id, params = {}) {
  return request(`/api/qzjTemplates/${id}/edit`, { method: 'POST', params })
}
