import request from '@/utils/request'
import {
  wbjTemplateList,
  wbjTemplateDetail,
  wbjTemplateVersionList,
} from '../helpers/mocks'

/**
 * 获取委办局资源目录列表
 * @param {limit, page} params
 *
 */
export function fetchWbjTemplateList (params) {
  if (params.useMock) {
    return {
      list: [
        ...wbjTemplateList,
      ],
      total: 1,
    }
  }

  return request('/api/wbjTemplates', { method: 'GET', params })
}

/**
 * 获取委办局资源目录详情
 * @param {string} id 资源目录id
 */
export function fetchWbjTemplateDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...wbjTemplateDetail,
    }
  }
  return request(`api/wbjTemplates/${id}`, { method: 'GET' })
}

/**
 * 新增委办局资源目录
 */
export function createWbjTemplate (params = {}) {
  return request('/api/wbjTemplates/add', { method: 'POST', params })
}

/**
 * 根据ID获取要编辑的委办局资源目录
 * @param {string} id 资源目录id
 */
export function fetchWbjTemplateEditInfo (id, params = {}) {
  if (params.useMock) {
    return {
      status: 1,
      data: {
        ...wbjTemplateDetail,
      },
    }
  }
  return request(`/api/wbjTemplates/${id}`, { method: 'GET' })
}

/**
 * 编辑委办局资源目录
 * @param {string} id 资源目录id
 */
export function updateWbjTemplateInfo (id, params) {
  return request(`/api/wbjTemplates/${id}/edit`, { method: 'POST', params })
}

/**
 * 获取资源目录版本信息
 * @param {string} id 资源目录id
 */
export function fetchWbjTemplateVersionList (id, params) {
  if (params.useMock) {
    return {
      list: [
        ...wbjTemplateVersionList,
      ],
      total: 1,
    }
  }
  return request(`/api/wbjTemplates/${id}/versions`, params)
}

export { fetchWbjTemplateList as getWbjTemplateList }
export { fetchWbjTemplateDetail as getWbjTemplateDetail }
