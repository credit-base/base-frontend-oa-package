/**
 * @file 接口 - 国标资源目录
 * @module resource-catalog/gbTemplate
 */

import request from '@/utils/request'
import { gbTemplateVersionList } from '../helpers/mocks'

/**
 * 获取国标资源目录列表
 * @param {limit, page} params
 */
export function fetchGbTemplateList (params = {}) {
  return request('/api/gbTemplates', { params })
}

/**
 * 获取国标资源目录详情
 * @param {string} id 资源目录id
 */
export function fetchGbTemplateDetail (id, params = {}) {
  return request(`/api/gbTemplates/${id}`, { params })
}

/**
 * 新增国标资源目录
 */
export function createGbTemplate (params) {
  return request('/api/gbTemplates/add', { method: 'POST', params })
}

/**
 * 根据ID获取要编辑的国标资源目录
 * @param {string} id 资源目录id
 */
export function fetchGbTemplateEditInfo (id, params = {}) {
  return request(`/api/gbTemplates/${id}`, { params })
}

/**
 * 编辑国标资源目录
 * @param {*} id
 * @param {*} params
 */
export function updateGbTemplateInfo (id, params) {
  return request(`/api/gbTemplates/${id}/edit`, { method: 'POST', params })
}

/**
 * 获取资源目录版本信息
 * @param {*} id
 * @param {*} params
 */
export function fetchGbTemplateVersionList (id, params) {
  if (params.useMock) {
    return {
      list: [
        ...gbTemplateVersionList,
      ],
      total: 1,
    }
  }
  return request(`/api/gbTemplates/${id}/versions`, params)
}

export { fetchGbTemplateList as getGbTemplateList }
export { fetchGbTemplateDetail as getGbTemplateDetail }
