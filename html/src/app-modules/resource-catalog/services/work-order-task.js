/**
 * @file 接口 - 工单任务
 * @module resource-catalog/work-order-task
 */

import request from '@/utils/request'
import {
  workOrderTasksList,
  subtasksList,
  workOrderTasksDetail,
  feedbackTemplate,
} from '../helpers/mocks'

/**
 * 获取工单父级任务列表
 */
export function fetchWorkOrderTasksList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...workOrderTasksList,
      ],
      total: 1,
    }
  }

  return request('/api/parentTasks', { params })
}

/**
 * 获取工单子任务
 */
export function fetchSubtasks (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...subtasksList,
      ],
      total: 1,
    }
  }

  return request('/api/workOrderTasks', { params })
}

/**
 * 获取工单任务详情
 * @param {string} id 工单任务id
 */
export function fetchWorkOrderTasksDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...workOrderTasksDetail,
    }
  }

  return request(`/api/workOrderTasks/${id}`, { params })
}

/**
 * 获取反馈任务信息
 * @param {string} id 工单任务id
 */
export function fetchFeedbackWorkOrderTasksInfo (id, params = {}) {
  if (params.useMock) {
    return {
      ...workOrderTasksDetail,
    }
  }

  return request(`/api/workOrderTasks/${id}`, { params })
}

/**
 * 工单任务指派
 */
export function addWorkOrderTask (params) {
  return request(`/api/parentTasks/assign`, { method: 'POST', params })
}

/**
 * 工单任务反馈
 * @param {string}} id 工单任务id
 */
export function feedbackWorkOrderTask (id, params = {}) {
  return request(`/api/workOrderTasks/${id}/feedback`, { method: 'POST', params })
}

/**
 * 获取反馈资源目录信息项
 */
export function fetchFeedbackTemplates (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...feedbackTemplate,
      ],
      total: 1,
    }
  }

  return request('/api/feedbackTemplates', { params })
}

/**
 * 终结
 * @param {*} id
 */
export function endWorkOrderTasks (id, params) {
  return request(`/api/workOrderTasks/${id}/end`, { method: 'POST', params })
}

/**
 * 确认
 * @param {*} id
 */
export function confirmWorkOrderTasks (id) {
  return request(`/api/workOrderTasks/${id}/confirm`, { method: 'POST' })
}

/**
 * 撤销父任务
 * @param {*} id
 */
export function revokeParentTasks (id, params) {
  return request(`/api/parentTasks/${id}/revoke`, { method: 'POST', params })
}

/**
 * 撤销子任务
 * @param {*} id
 */
export function revokeSubTasks (id, params) {
  return request(`/api/workOrderTasks/${id}/revoke`, { method: 'POST', params })
}

export { fetchWorkOrderTasksDetail as getWorkOrderTaskDetail }
