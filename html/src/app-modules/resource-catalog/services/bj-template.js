/**
 * @file 接口 - 本级资源目录
 * @module resource-catalog/gbTemplate
 */

import store from '@/store'
import request from '@/utils/request'
import {
  bjTemplateList,
  bjTemplateDetail,
  bjTemplateVersionList,
} from '../helpers/mocks'

/**
 * 获取本级资源目录列表
 */
export function fetchBjTemplateList (params) {
  if (params.useMock) {
    return {
      list: [
        ...bjTemplateList,
      ],
      total: 1,
    }
  }

  return request('/api/bjTemplates', { params })
}

/**
 * 获取本级资源目录详情
 * @param {string} id 资源目录id
 */
export function fetchBjTemplateDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...bjTemplateDetail,
    }
  }
  return request(`api/bjTemplates/${id}`, { params })
}

/**
 * 新增本级资源目录
 */
export function createBjTemplate (params) {
  return request('/api/bjTemplates/add', { method: 'POST', params })
}

/**
 * 根据ID获取要编辑的本级资源目录
 * @param {string} id 资源目录 id
 */
export function fetchBjTemplateEditInfo (id, params) {
  if (params.useMock) {
    return {
      status: 1,
      data: {
        ...bjTemplateDetail,
      },
    }
  }
  return request(`/api/bjTemplates/${id}`, { params })
}

/**
 * 编辑本级资源目录
 * @param {string} id 资源目录id
 */
export function updateBjTemplateInfo (id, params) {
  return request(`/api/bjTemplates/${id}/edit`, { method: 'POST', params })
}

/**
 * 获取资源目录版本信息
 * @param {string} id 资源目录id
 */
export function fetchBjTemplateVersionList (id, params) {
  if (params.useMock) {
    return {
      list: [
        ...bjTemplateVersionList,
      ],
      total: 1,
    }
  }
  return request(`/api/bjTemplates/${id}/versions`, { params })
}

/**
 * 获取本级资源目录列表
 */
export function getBjTemplateList (params) {
  if (params.useMock) {
    return {
      list: store.getters.bjTemplateList,
      total: store.getters.bjTemplateList.length,
    }
  }

  return request(`/api/bjTemplates`, { params })
}

/**
 * 获取资源目录详情
 * @param {string} id 资源目录id
 */
export function getBjTemplateDetail (id, params = {}) {
  if (params.useMock) {
    const item = store.getters.bjTemplateList.find(item => item.id === id)

    return item || {}
  }
  return request(`/api/bjTemplates/${id}`, { params })
}

/**
 * 新增本级资源目录
 */
export function addBjTemplate (params) {
  return request(`/api/bjTemplates/add`, { method: 'POST', params })
}

/**
 * 编辑本级资源目录
 * @param {string} id 资源目录id
 */
export function editBjTemplate (id, params) {
  return request(`/api/bjTemplates/${id}/edit`, { method: 'POST', params })
}

/**
 * 获取资源目录初始化配置
 */
export function fetchTemplateConfigs (params = {}) {
  if (params.useMock) {
    return store.getters.bjTemplateInitConfigs
  }
  return request(`/api/template/configure`, { params })
}
