/**
 * @file 接口 - 问题反馈
 * @module business/feedback
 */

import request from '@/utils/request'

/**
 * 问题反馈列表
 * @param {*} params
 * @doc https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/Api/feedbackApi.md
 * @returns
 */
export function getFeedbackList (params = {}) {
  return request(`/api/feedbacks`, { params })
}

/**
 * 问题反馈详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function getFeedbackDetail (id, params = {}) {
  return request(`/api/feedbacks/${id}`, { params })
}

/**
 * 受理问题反馈
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function acceptFeedback (id, params = {}) {
  return request(`/api/feedbacks/${id}/accept`, { method: 'POST', params })
}

/**
 * 问题反馈审核列表
 * @param {*} params
 * @doc https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/Api/unAuditedFeedbackApi.md
 * @returns
 */
export function getUnAuditedFeedbackList (params = {}) {
  return request(`/api/unAuditedFeedbacks`, { params })
}

/**
 * 问题反馈审核详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function getUnAuditedFeedbackDetail (id, params = {}) {
  return request(`/api/unAuditedFeedbacks/${id}`, { params })
}

/**
 * 问题反馈重新受理
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function resubmitFeedback (id, params = {}) {
  return request(`/api/unAuditedFeedbacks/${id}/resubmit`, { method: 'POST', params })
}

/**
 * 问题反馈审核通过
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function approveUnAuditedFeedback (id, params = {}) {
  return request(`/api/unAuditedFeedbacks/${id}/approve`, { method: 'POST', params })
}

/**
 * 问题反馈审核驳回
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function rejectUnAuditedFeedback (id, params = {}) {
  return request(`/api/unAuditedFeedbacks/${id}/reject`, { method: 'POST', params })
}
