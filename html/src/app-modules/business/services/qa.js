/**
 * @file 接口 - 问题反馈
 * @module business/qa
 */

import request from '@/utils/request'

/**
 * 问题反馈列表
 * @param {*} params
 * @doc https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/Api/qaApi.md
 * @returns
 */
export function getQaList (params = {}) {
  return request(`/api/qas`, { params })
}

/**
 * 问题反馈详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function getQaDetail (id, params = {}) {
  return request(`/api/qas/${id}`, { params })
}

/**
 * 受理问题反馈
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function acceptQa (id, params = {}) {
  return request(`/api/qas/${id}/accept`, { method: 'POST', params })
}

/**
 * 公示问题反馈
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function publishQa (id, params = {}) {
  return request(`/api/qas/${id}/publish`, { method: 'POST', params })
}

/**
 * 取消公示问题反馈
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function unPublishQa (id, params = {}) {
  return request(`/api/qas/${id}/unPublish`, { method: 'POST', params })
}

/**
 * 问题反馈审核列表
 * @param {*} params
 * @doc https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/Api/unAuditedQaApi.md
 * @returns
 */
export function getUnAuditedQaList (params = {}) {
  return request(`/api/unAuditedQas`, { params })
}

/**
 * 问题反馈审核详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function getUnAuditedQaDetail (id, params = {}) {
  return request(`/api/unAuditedQas/${id}`, { params })
}

/**
 * 问题反馈重新受理
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function resubmitQa (id, params = {}) {
  return request(`/api/unAuditedQas/${id}/resubmit`, { method: 'POST', params })
}

/**
 * 问题反馈审核通过
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function approveUnAuditedQa (id, params = {}) {
  return request(`/api/unAuditedQas/${id}/approve`, { method: 'POST', params })
}

/**
 * 问题反馈审核驳回
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function rejectUnAuditedQa (id, params = {}) {
  return request(`/api/unAuditedQas/${id}/reject`, { method: 'POST', params })
}
