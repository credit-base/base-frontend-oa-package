/**
 * @file 接口 - 信用投诉
 * @module business/complaint
 */

import request from '@/utils/request'

/**
 * 信用投诉列表
 * @param {*} params
 * @doc https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/Api/complaintApi.md
 * @returns
 */
export function getComplaintList (params = {}) {
  return request(`/api/complaints`, { params })
}

/**
 * 信用投诉详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function getComplaintDetail (id, params = {}) {
  return request(`/api/complaints/${id}`, { params })
}

/**
 * 受理信用投诉
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function acceptComplaint (id, params = {}) {
  return request(`/api/complaints/${id}/accept`, { method: 'POST', params })
}

/**
 * 信用投诉审核列表
 * @param {*} params
 * @doc https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/Api/unAuditedComplaintApi.md
 * @returns
 */
export function getUnAuditedComplaintList (params = {}) {
  return request(`/api/unAuditedComplaints`, { params })
}

/**
 * 信用投诉审核详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function getUnAuditedComplaintDetail (id, params = {}) {
  return request(`/api/unAuditedComplaints/${id}`, { params })
}

/**
 * 信用投诉重新受理
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function resubmitComplaint (id, params = {}) {
  return request(`/api/unAuditedComplaints/${id}/resubmit`, { method: 'POST', params })
}

/**
 * 信用投诉审核通过
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function approveUnAuditedComplaint (id, params = {}) {
  return request(`/api/unAuditedComplaints/${id}/approve`, { method: 'POST', params })
}

/**
 * 信用投诉审核驳回
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function rejectUnAuditedComplaint (id, params = {}) {
  return request(`/api/unAuditedComplaints/${id}/reject`, { method: 'POST', params })
}
