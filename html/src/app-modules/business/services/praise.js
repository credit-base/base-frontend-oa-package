/**
 * @file 接口 - 信用表扬
 * @module business/praise
 */

import request from '@/utils/request'

/**
 * 信用表扬列表
 * @param {*} params
 * @doc https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/Api/praiseApi.md
 * @returns
 */
export function getPraiseList (params = {}) {
  return request(`/api/praises`, { params })
}

/**
 * 信用表扬详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function getPraiseDetail (id, params = {}) {
  return request(`/api/praises/${id}`, { params })
}

/**
 * 受理信用表扬
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function acceptPraise (id, params = {}) {
  return request(`/api/praises/${id}/accept`, { method: 'POST', params })
}

/**
 * 公示信用信用表扬
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function publishPraise (id, params = {}) {
  return request(`/api/praises/${id}/publish`, { method: 'POST', params })
}

/**
 * 取消公示信用表扬
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function unPublishPraise (id, params = {}) {
  return request(`/api/praises/${id}/unPublish`, { method: 'POST', params })
}

/**
 * 信用表扬审核列表
 * @param {*} params
 * @doc https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/Api/unAuditedPraiseApi.md
 * @returns
 */
export function getUnAuditedPraiseList (params = {}) {
  return request(`/api/unAuditedPraises`, { params })
}

/**
 * 信用表扬审核详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function getUnAuditedPraiseDetail (id, params = {}) {
  return request(`/api/unAuditedPraises/${id}`, { params })
}

/**
 * 信用表扬重新受理
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function resubmitPraise (id, params = {}) {
  return request(`/api/unAuditedPraises/${id}/resubmit`, { method: 'POST', params })
}

/**
 * 信用表扬审核通过
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function approveUnAuditedPraise (id, params = {}) {
  return request(`/api/unAuditedPraises/${id}/approve`, { method: 'POST', params })
}

/**
 * 信用表扬审核驳回
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function rejectUnAuditedPraise (id, params = {}) {
  return request(`/api/unAuditedPraises/${id}/reject`, { method: 'POST', params })
}
