/**
 * @file 接口 - 信用表扬
 * @module business/appeal
 */

import request from '@/utils/request'

/**
 * 信用表扬列表
 * @param {*} params
 * @doc https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/Api/appealApi.md
 * @returns
 */
export function getAppealList (params = {}) {
  return request(`/api/appeals`, { params })
}

/**
 * 信用表扬详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function getAppealDetail (id, params = {}) {
  return request(`/api/appeals/${id}`, { params })
}

/**
 * 受理信用表扬
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function acceptAppeal (id, params = {}) {
  return request(`/api/appeals/${id}/accept`, { method: 'POST', params })
}

/**
 * 信用表扬审核列表
 * @param {*} params
 * @doc https://code.aliyun.com/credit-base/common-frontend-oa/blob/master/docs/Api/unAuditedAppealApi.md
 * @returns
 */
export function getUnAuditedAppealList (params = {}) {
  return request(`/api/unAuditedAppeals`, { params })
}

/**
 * 信用表扬审核详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function getUnAuditedAppealDetail (id, params = {}) {
  return request(`/api/unAuditedAppeals/${id}`, { params })
}

/**
 * 信用表扬重新受理
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function resubmitAppeal (id, params = {}) {
  return request(`/api/unAuditedAppeals/${id}/resubmit`, { method: 'POST', params })
}

/**
 * 信用表扬审核通过
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function approveUnAuditedAppeal (id, params = {}) {
  return request(`/api/unAuditedAppeals/${id}/approve`, { method: 'POST', params })
}

/**
 * 信用表扬审核驳回
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function rejectUnAuditedAppeal (id, params = {}) {
  return request(`/api/unAuditedAppeals/${id}/reject`, { method: 'POST', params })
}
