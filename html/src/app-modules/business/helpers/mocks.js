export const businessList = [
  {
    id: 'MA',
    title: '信用投诉',
    type: {
      id: 'Lw',
      name: '个人',
    },
    name: '陈瑞',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'info',
      id: 'Lw',
      name: '待受理',
    },
    admissibility: {},
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'MC4',
    title: '标题',
    type: {
      id: 'MA',
      name: '企业',
    },
    name: '姓名/企业名称',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
]

export const businessDetail = {
  id: 'My4',
  reply: {
    acceptStatus: {
      type: 'info',
      id: 'Lw',
      name: '待受理',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
    crew: {
      id: 'MA',
      realName: '张科',
    },
    content: '回复内容',
    images: [
      {
        name: '回复图片',
        identify: '回复图片.jpg',
      },
    ],
    time: 1620872174, // 回复时间
    timeFormat: '2021年05月13日 10点16分',
  },
  complaintData: {
    title: '标题',
    type: {
      id: 'MA',
      name: '企业',
    },
    subject: '文文造纸厂',
    name: '姓名/企业名称',
    identify: '统一社会信用代码/身份证号',
    certificates: [
      {
        name: '上传身份证/营业执照',
        identify: '上传身份证/营业执照.jpg',
      },
    ],
    images: [
      {
        name: '图片',
        identify: '图片.jpg',
      },
    ],
    content: '内容',
    contact: '联系方式',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    member: {
      id: 'MA',
      name: '发展和改革委员会',
    },
  },
}

export const praiseList = [
  {
    id: 'MA',
    title: '信用表扬',
    type: {
      id: 'Lw',
      name: '个人',
    },
    name: '陈瑞',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'info',
      id: 'Lw',
      name: '待受理',
    },
    admissibility: {},
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'MQ',
    title: '标题',
    type: {
      id: 'MA',
      name: '企业',
    },
    name: '姓名/企业名称',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'Mg',
    title: '信用表扬',
    type: {
      id: 'Lw',
      name: '个人',
    },
    name: '陈瑞',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {},
    status: {
      type: '',
      id: 'MQ',
      name: '公示',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'MC4',
    title: '标题',
    type: {
      id: 'MA',
      name: '企业',
    },
    name: '姓名/企业名称',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    status: {
      type: 'danger',
      id: 'LC0',
      name: '撤销',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
]
export const praiseDetail = {
  id: 'My4',
  reply: {
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
    crew: {
      id: 'MA',
      realName: '张科',
    },
    content: '回复内容',
    images: [
      {
        name: '回复图片',
        identify: '回复图片.jpg',
      },
    ],
    time: 1620872174, // 回复时间
    timeFormat: '2021年05月13日 10点16分',
  },
  praiseData: {
    title: '标题',
    type: {
      id: 'MA',
      name: '企业',
    },
    subject: '文文造纸厂',
    name: '姓名/企业名称',
    identify: '统一社会信用代码/身份证号',
    certificates: [
      {
        name: '上传身份证/营业执照',
        identify: '上传身份证/营业执照.jpg',
      },
    ],
    images: [
      {
        name: '图片',
        identify: '图片.jpg',
      },
    ],
    content: '内容',
    contact: '联系方式',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    member: {
      id: 'MA',
      name: '发展和改革委员会',
    },
  },
}
export const appealList = [
  {
    id: 'MA',
    title: '信用表扬',
    type: {
      id: 'Lw',
      name: '个人',
    },
    name: '陈瑞',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'info',
      id: 'Lw',
      name: '待受理',
    },
    admissibility: {},
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'MQ',
    title: '标题',
    type: {
      id: 'MA',
      name: '企业',
    },
    name: '姓名/企业名称',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'Mg',
    title: '信用表扬',
    type: {
      id: 'Lw',
      name: '个人',
    },
    name: '陈瑞',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {},
    status: {
      type: '',
      id: 'MQ',
      name: '公示',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'MC4',
    title: '标题',
    type: {
      id: 'MA',
      name: '企业',
    },
    name: '姓名/企业名称',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    status: {
      type: 'danger',
      id: 'LC0',
      name: '撤销',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
]
export const appealDetail = {
  id: 'My4',
  reply: {
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
    crew: {
      id: 'MA',
      realName: '张科',
    },
    content: '回复内容',
    images: [
      {
        name: '回复图片',
        identify: '回复图片.jpg',
      },
    ],
    time: 1620872174, // 回复时间
    timeFormat: '2021年05月13日 10点16分',
  },
  appealData: {
    title: '标题',
    type: {
      id: 'MA',
      name: '企业',
    },
    subject: '文文造纸厂',
    name: '姓名/企业名称',
    identify: '统一社会信用代码/身份证号',
    certificates: [
      {
        name: '上传身份证/营业执照',
        identify: '上传身份证/营业执照.jpg',
      },
    ],
    images: [
      {
        name: '图片',
        identify: '图片.jpg',
      },
    ],
    content: '内容',
    contact: '联系方式',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    member: {
      id: 'MA',
      name: '发展和改革委员会',
    },
  },
}

export const unAuditedBusinessList = [
  {
    id: 'MA',
    title: '信用投诉',
    type: {
      id: 'Lw',
      name: '个人',
    },
    name: '陈瑞',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    status: {
      id: 'Lw',
      name: '正常',
    },
    applyStatus: {
      type: 'danger',
      id: 'LC0',
      name: '审核驳回',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
    reason: '受理内容错误',
  },
  {
    id: 'MC4',
    title: '标题',
    type: {
      id: 'MA',
      name: '企业',
    },
    name: '姓名/企业名称',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    applyStatus: {
      type: 'info',
      id: 'Lw',
      name: '待审核',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
]

export const unAuditedBusinessDetail = {
  id: 'My4',
  reason: '驳回原因',
  applyStatus: {
    type: 'info',
    id: 'Lw',
    name: '待受理',
  },
  reply: { // 受理信息
    acceptStatus: {
      type: 'success',
      id: 'MQ',
      name: '受理完成',
    },
    admissibility: {
      id: 'MA',
      name: '不予受理',
    },
    acceptUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
    crew: {
      id: 'MA',
      realName: '张科',
    },
    content: '回复内容',
    images: [
      {
        name: '回复图片',
        identify: '回复图片.jpg',
      },
    ],
    time: 1620872174, // 回复时间
    timeFormat: '2021年05月13日 10点16分',
  },
  complaintData: { // 反馈信息
    title: '标题',
    type: {
      id: 'MA',
      name: '企业',
    },
    name: '姓名/企业名称',
    subject: '文文造纸厂',
    identify: '统一社会信用代码/身份证号',
    certificates: [
      {
        name: '上传身份证/营业执照',
        identify: '上传身份证/营业执照.jpg',
      },
    ],
    images: [
      {
        name: '图片',
        identify: '图片.jpg',
      },
    ],
    content: '内容',
    contact: '联系方式',
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    status: {
      type: 'success',
      id: 'Lw',
      name: '正常',
    },
    member: {
      id: 'MA',
      name: '发展和改革委员会',
    },
  },
}

export const data = {
  totle: 0,
  pages: '1',
  pageNum: 0,
  pageSize: '1',
  rows: [
    {
      askbh: ' 提问ID',
      ask: ' 提问内容',
      ajmc: '案件名称',
      ajbh: '案件编号',
      sex: '性别（1:男;2:女）',
      age: '年龄',
    },
  ],
}
