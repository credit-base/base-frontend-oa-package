/* eslint-disable max-lines-per-function */
import Base from '@/models/base'
import { CONSTANTS } from '@/plugins/constants'
import { renderOSSUrl } from '@/utils'

const { ACCEPT_STATUS = {} } = CONSTANTS

class Business extends Base {
  constructor (params = {}) {
    super(params)

    const {
      id,
      title,
      type = {},
      name,
      acceptStatus = {},
      admissibility = {},
      status = {},
      applyStatus = {},
      rejectReason,
      acceptUserGroup = {},
      reply = {
        acceptStatus: {},
        admissibility: {},
        acceptUserGroup: {},
        crew: {},
        content: '',
        images: [],
        createTimeFormat: '',
      },
      complaintData = {
        title: '',
        type: {},
        name: '',
        identify: '',
        certificates: [],
        images: [],
        content: '',
        contact: '',
        subject: '',
        updateTimeFormat: '',
        statusTimeFormat: '',
        createTimeFormat: '',
        status: {},
        member: {},
      },
      praiseData = {
        title: '',
        type: {},
        name: '',
        identify: '',
        certificates: [],
        images: [],
        content: '',
        contact: '',
        subject: '',
        updateTimeFormat: '',
        statusTimeFormat: '',
        createTimeFormat: '',
        status: {},
        member: {},
      },
      appealData = {
        title: '',
        type: {},
        name: '',
        identify: '',
        certificates: [],
        images: [],
        content: '',
        contact: '',
        subject: '',
        updateTimeFormat: '',
        statusTimeFormat: '',
        createTimeFormat: '',
        status: {},
        member: {},
      },
      feedbackData = {
        title: '',
        name: '',
        identify: '',
        certificates: [],
        images: [],
        content: '',
        contact: '',
        subject: '',
        updateTimeFormat: '',
        statusTimeFormat: '',
        createTimeFormat: '',
        status: {},
        member: {},
      },
      qaData = {
        title: '',
        name: '',
        identify: '',
        certificates: [],
        images: [],
        content: '',
        contact: '',
        subject: '',
        updateTimeFormat: '',
        statusTimeFormat: '',
        createTimeFormat: '',
        status: {},
        member: {},
      },
      createTimeFormat,
      statusTimeFormat,
      updateTimeFormat,
    } = params

    this.id = id
    this.title = title
    this.type = type.name
    this.name = name
    this.acceptStatus = acceptStatus
    this.admissibility = acceptStatus.id === ACCEPT_STATUS.COMPLETED ? admissibility.name : '--'
    this.status = status
    this.applyStatus = applyStatus
    this.rejectReason = rejectReason
    this.acceptUserGroup = acceptUserGroup.name
    this.reply = {
      ...reply,
      admissibility: reply.admissibility && reply.admissibility.name,
      acceptUserGroup: reply.acceptUserGroup && reply.acceptUserGroup.name,
      crew: reply.crew && reply.crew.realName,
      images: renderOSSUrl(reply.images),
    }
    this.complaintData = {
      ...complaintData,
      type: complaintData.type && complaintData.type.name,
      member: complaintData.member && complaintData.member.realName,
      images: renderOSSUrl(complaintData.images),
    }
    this.praiseData = {
      ...praiseData,
      type: praiseData.type && praiseData.type.name,
      member: praiseData.member && praiseData.member.realName,
      images: renderOSSUrl(praiseData.images),
    }
    this.appealData = {
      ...appealData,
      type: appealData.type && appealData.type.name,
      member: appealData.member && appealData.member.realName,
      images: renderOSSUrl(appealData.images),
    }
    this.feedbackData = {
      ...feedbackData,
      member: feedbackData.member && feedbackData.member.realName,
      images: renderOSSUrl(feedbackData.images),
    }
    this.qaData = {
      ...qaData,
      member: qaData.member && qaData.member.realName,
      images: renderOSSUrl(qaData.images),
    }
    this.createTimeFormat = createTimeFormat
    this.statusTimeFormat = statusTimeFormat
    this.updateTimeFormat = updateTimeFormat
  }
}
export default Business
