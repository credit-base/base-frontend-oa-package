/**
 * @file 业务信息管理应用系统 - 翻译器
 * @module business/translators
 */

import Business from './business'
// 信用投诉
export function translateComplaintList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Business(item)
      .get([
        'id',
        'title',
        'type',
        'name',
        'acceptUserGroup',
        'acceptStatus',
        'admissibility',
        'status',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateComplaintDetail (res = {}) {
  return new Business(res)
    .get([
      'id',
      'reply',
      'complaintData',
    ])
}

export function translateUnAuditedComplaintList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Business(item)
      .get([
        'id',
        'title',
        'type',
        'name',
        'acceptUserGroup',
        'admissibility',
        'applyStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateUnAuditedComplaintDetail (res = {}) {
  return new Business(res)
    .get([
      'id',
      'applyStatus',
      'rejectReason',
      'reply',
      'complaintData',
    ])
}

// 信用表扬
export function translatePraiseList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Business(item)
      .get([
        'id',
        'title',
        'type',
        'name',
        'acceptUserGroup',
        'acceptStatus',
        'admissibility',
        'status',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translatePraiseDetail (res = {}) {
  return new Business(res)
    .get([
      'id',
      'reply',
      'praiseData',
    ])
}

export function translateUnAuditedPraiseList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Business(item)
      .get([
        'id',
        'title',
        'type',
        'name',
        'acceptUserGroup',
        'admissibility',
        'applyStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateUnAuditedPraiseDetail (res = {}) {
  return new Business(res)
    .get([
      'id',
      'applyStatus',
      'rejectReason',
      'reply',
      'praiseData',
    ])
}

// 异议申诉
export function translateAppealList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Business(item)
      .get([
        'id',
        'title',
        'type',
        'name',
        'acceptUserGroup',
        'acceptStatus',
        'admissibility',
        'status',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateAppealDetail (res = {}) {
  return new Business(res)
    .get([
      'id',
      'reply',
      'appealData',
    ])
}

export function translateUnAuditedAppealList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Business(item)
      .get([
        'id',
        'title',
        'type',
        'name',
        'acceptUserGroup',
        'admissibility',
        'applyStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateUnAuditedAppealDetail (res = {}) {
  return new Business(res)
    .get([
      'id',
      'applyStatus',
      'rejectReason',
      'reply',
      'appealData',
    ])
}

// 问题反馈
export function translateFeedbackList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Business(item)
      .get([
        'id',
        'title',
        'acceptUserGroup',
        'acceptStatus',
        'admissibility',
        'status',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateFeedbackDetail (res = {}) {
  return new Business(res)
    .get([
      'id',
      'reply',
      'feedbackData',
    ])
}

export function translateUnAuditedFeedbackList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Business(item)
      .get([
        'id',
        'title',
        'acceptUserGroup',
        'admissibility',
        'applyStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateUnAuditedFeedbackDetail (res = {}) {
  return new Business(res)
    .get([
      'id',
      'applyStatus',
      'rejectReason',
      'reply',
      'feedbackData',
    ])
}

// 信用问答
export function translateQaList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Business(item)
      .get([
        'id',
        'title',
        'acceptUserGroup',
        'acceptStatus',
        'admissibility',
        'status',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateQaDetail (res = {}) {
  return new Business(res)
    .get([
      'id',
      'reply',
      'qaData',
    ])
}

export function translateUnAuditedQaList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Business(item)
      .get([
        'id',
        'title',
        'acceptUserGroup',
        'admissibility',
        'applyStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateUnAuditedQaDetail (res = {}) {
  return new Business(res)
    .get([
      'id',
      'applyStatus',
      'rejectReason',
      'reply',
      'qaData',
    ])
}
