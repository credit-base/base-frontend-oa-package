import { CONSTANTS } from '@/plugins/constants'

const {
  STATUS = {},
  APPLY_STATUS = {},
  ACCEPT_STATUS = {},
  ADMISSIBILITY_VALUE = {},
} = CONSTANTS

export const BUSINESS_TABS = [
  { name: 'formal-business', label: 'FORMAL_BUSINESS', component: 'Formal' },
  { name: 'acceptance-business', label: 'ACCEPTANCE_BUSINESS', component: 'Acceptance' },
]

export const STATUS_ENUM = [
  {
    text: '正常',
    value: STATUS.NORMAL,
  },
  {
    text: '撤销',
    value: STATUS.REVOKE,
  },
]

export const PRAISE_STATUS_ENUM = [
  {
    text: '正常',
    value: STATUS.NORMAL,
  },
  {
    text: '公示',
    value: STATUS.PUBLICITY,
  },
  {
    text: '撤销',
    value: STATUS.REVOKE,
  },
]
export const QA_STATUS_ENUM = [
  {
    text: '正常',
    value: STATUS.NORMAL,
  },
  {
    text: '公示',
    value: STATUS.PUBLICITY,
  },
  {
    text: '撤销',
    value: STATUS.REVOKE,
  },
]

export const ACCEPT_STATUS_ENUM = [
  {
    text: '待受理',
    value: ACCEPT_STATUS.PADDING,
  },
  {
    text: '受理中',
    value: ACCEPT_STATUS.ACCEPTING,
  },
  {
    text: '受理完成',
    value: ACCEPT_STATUS.COMPLETED,
  },
]

export const APPLY_STATUS_ENUM = [
  {
    text: '待审核',
    value: APPLY_STATUS.PADDING,
  },
  {
    text: '已驳回',
    value: APPLY_STATUS.REJECT,
  },
]

export const COVER_SIZE = {
  width: 210,
  height: 297,
}

export const FILE_LIMIT = 9

export const ADMISSIBILITY_ENUM = [
  {
    label: 'BE_ACCEPTED',
    value: ADMISSIBILITY_VALUE.BE_ACCEPTED,
  },
  {
    label: 'NOT_ACCEPTED',
    value: ADMISSIBILITY_VALUE.NOT_ACCEPTED,
  },
]
