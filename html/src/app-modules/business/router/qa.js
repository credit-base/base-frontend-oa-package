/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/business/qa
 */

// import Layout from '@/layout'
import permissions from '@/constants/permissions'

export default [
  {
    path: '/formal/qa',
    name: 'Qa',
    component: () => import('@/app-modules/business/views/formal/qa/list'),
    meta: {
      title: 'business.ROUTER.QA',
      icon: 'qa',
      routerId: permissions.QA_ID,
    },
  },
  {
    path: '/formal/qa/formal-detail/:id',
    name: 'FormalQaDetail',
    component: () => import('@/app-modules/business/views/formal/qa/formal-detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.QA_DETAIL',
      icon: 'qa',
      activeMenu: '/formal/qa',
    },
  },
  {
    path: '/formal/qa/acceptance-detail/:id',
    name: 'AcceptanceQaDetail',
    component: () => import('@/app-modules/business/views/formal/qa/acceptance-detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.QA_DETAIL',
      icon: 'qa',
      activeMenu: '/formal/qa',
    },
  },
  {
    path: '/formal/qa/accept/:id',
    name: 'FormalQaAccept',
    component: () => import('@/app-modules/business/views/formal/qa/accept'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.QA_ACCEPT',
      icon: 'qa',
      activeMenu: '/formal/qa',
    },
  },
  {
    path: '/formal/qa/re-accept/:id',
    name: 'AcceptanceQaReAccept',
    component: () => import('@/app-modules/business/views/formal/qa/re-accept'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.QA_RE_ACCEPT',
      icon: 'qa',
      activeMenu: '/formal/qa',
    },
  },
]
