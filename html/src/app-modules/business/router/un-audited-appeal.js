/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/business/un-audited-appeal
 */

// import Layout from '@/layout'
import permissions from '@/constants/permissions'

export default [
  {
    path: '/un-audited/appeal',
    name: 'unAuditedAppeal',
    component: () => import('@/app-modules/business/views/un-audited/appeal/list'),
    meta: {
      title: 'business.ROUTER.UN_AUDITED_APPEAL',
      icon: 'appeal',
      routerId: permissions.UN_AUDITED_APPEAL_ID,
    },
  },
  {
    path: '/un-audited/appeal/detail/:id',
    name: 'unAuditedAppealDetail',
    component: () => import('@/app-modules/business/views/un-audited/appeal/detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.UN_AUDITED_APPEAL_DETAIL',
      icon: 'appeal',
      activeMenu: '/un-audited/appeal',
    },
  },
]
