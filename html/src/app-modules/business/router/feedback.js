/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/business/feedback
 */

// import Layout from '@/layout'
import permissions from '@/constants/permissions'

export default [
  {
    path: '/formal/feedback',
    name: 'Feedback',
    component: () => import('@/app-modules/business/views/formal/feedback/list'),
    meta: {
      title: 'business.ROUTER.FEEDBACK',
      icon: 'feedback',
      routerId: permissions.FEEDBACK_ID,
    },
  },
  {
    path: '/formal/feedback/formal-detail/:id',
    name: 'FormalFeedbackDetail',
    component: () => import('@/app-modules/business/views/formal/feedback/formal-detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.FEEDBACK_DETAIL',
      icon: 'feedback',
      activeMenu: '/formal/feedback',
    },
  },
  {
    path: '/formal/feedback/acceptance-detail/:id',
    name: 'AcceptanceFeedbackDetail',
    component: () => import('@/app-modules/business/views/formal/feedback/acceptance-detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.FEEDBACK_DETAIL',
      icon: 'feedback',
      activeMenu: '/formal/feedback',
    },
  },
  {
    path: '/formal/feedback/accept/:id',
    name: 'FormalFeedbackAccept',
    component: () => import('@/app-modules/business/views/formal/feedback/accept'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.FEEDBACK_ACCEPT',
      icon: 'feedback',
      activeMenu: '/formal/feedback',
    },
  },
  {
    path: '/formal/feedback/re-accept/:id',
    name: 'AcceptanceFeedbackReAccept',
    component: () => import('@/app-modules/business/views/formal/feedback/re-accept'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.FEEDBACK_RE_ACCEPT',
      icon: 'feedback',
      activeMenu: '/formal/feedback',
    },
  },
]
