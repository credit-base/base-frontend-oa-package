/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/business/appeal
 */

// import Layout from '@/layout'
import permissions from '@/constants/permissions'

export default [
  {
    path: '/formal/appeal',
    name: 'Appeal',
    component: () => import('@/app-modules/business/views/formal/appeal/list'),
    meta: {
      title: 'business.ROUTER.APPEAL',
      icon: 'appeal',
      routerId: permissions.APPEAL_ID,
    },
  },
  {
    path: '/formal/appeal/formal-detail/:id',
    name: 'FormalAppealDetail',
    component: () => import('@/app-modules/business/views/formal/appeal/formal-detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.APPEAL_DETAIL',
      icon: 'appeal',
      activeMenu: '/formal/appeal',
    },
  },
  {
    path: '/formal/appeal/acceptance-detail/:id',
    name: 'AcceptanceAppealDetail',
    component: () => import('@/app-modules/business/views/formal/appeal/acceptance-detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.APPEAL_DETAIL',
      icon: 'appeal',
      activeMenu: '/formal/appeal',
    },
  },
  {
    path: '/formal/appeal/accept/:id',
    name: 'FormalAppealAccept',
    component: () => import('@/app-modules/business/views/formal/appeal/accept'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.APPEAL_ACCEPT',
      icon: 'appeal',
      activeMenu: '/formal/appeal',
    },
  },
  {
    path: '/formal/appeal/re-accept/:id',
    name: 'AcceptanceAppealReAccept',
    component: () => import('@/app-modules/business/views/formal/appeal/re-accept'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.APPEAL_RE_ACCEPT',
      icon: 'appeal',
      activeMenu: '/formal/appeal',
    },
  },
]
