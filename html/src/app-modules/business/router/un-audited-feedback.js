/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/business/un-audited-feedback
 */

// import Layout from '@/layout'
import permissions from '@/constants/permissions'

export default [
  {
    path: '/un-audited/feedback',
    name: 'unAuditedFeedback',
    component: () => import('@/app-modules/business/views/un-audited/feedback/list'),
    meta: {
      title: 'business.ROUTER.UN_AUDITED_FEEDBACK',
      icon: 'feedback',
      routerId: permissions.UN_AUDITED_FEEDBACK_ID,
    },
  },
  {
    path: '/un-audited/feedback/detail/:id',
    name: 'unAuditedFeedbackDetail',
    component: () => import('@/app-modules/business/views/un-audited/feedback/detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.UN_AUDITED_FEEDBACK_DETAIL',
      icon: 'feedback',
      activeMenu: '/un-audited/feedback',
    },
  },
]
