/**
 * @file 业务信息管理应用系统 - 路由
 * @module async-routes/business
 */
import Layout from '@/layout'
// import permissions from '@/constants/permissions'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

import complaint from './complaint'
import unAuditedComplaint from './un-audited-complaint'
import praise from './praise'
import unAuditedPraise from './un-audited-praise'
import appeal from './appeal'
import unAuditedAppeal from './un-audited-appeal'
import feedback from './feedback'
import unAuditedFeedback from './un-audited-feedback'
import qa from './qa'
import unAuditedQa from './un-audited-qa'

const { STATUS_BUSINESS } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/business',
    name: 'Business',
    component: Layout,
    alwaysShow: true,
    meta: {
      title: 'business.ROUTER.BUSINESS',
      icon: 'business',
      breadcrumb: false,
      isVisible: STATUS_BUSINESS,
    },
    children: [
      {
        path: '/business/formal',
        name: 'business.formal',
        component: () => import('@/app-modules/business/views/index'),
        alwaysShow: true,
        meta: {
          title: 'business.ROUTER.BUSINESS_FORMAL',
          icon: 'business',
          breadcrumb: false,
        },
        children: [
          ...complaint,
          ...praise,
          ...appeal,
          ...feedback,
          ...qa,
        ],
      },
      {
        path: '/business/un-audited',
        name: 'business.unAudited',
        component: () => import('@/app-modules/business/views/index'),
        alwaysShow: true,
        meta: {
          title: 'business.ROUTER.BUSINESS_UN_AUDITED',
          icon: 'business',
          breadcrumb: false,
        },
        children: [
          ...unAuditedComplaint,
          ...unAuditedPraise,
          ...unAuditedAppeal,
          ...unAuditedFeedback,
          ...unAuditedQa,
        ],
      },
    ],
  },
]
