/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/business/un-audited-complaint
 */

// import Layout from '@/layout'
import permissions from '@/constants/permissions'

export default [
  {
    path: '/un-audited/complaint',
    name: 'unAuditedComplaint',
    component: () => import('@/app-modules/business/views/un-audited/complaint/list'),
    meta: {
      title: 'business.ROUTER.UN_AUDITED_COMPLAINT',
      icon: 'complaint',
      routerId: permissions.UN_AUDITED_COMPLAINT_ID,
    },
  },
  {
    path: '/un-audited/complaint/detail/:id',
    name: 'unAuditedComplaintDetail',
    component: () => import('@/app-modules/business/views/un-audited/complaint/detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.UN_AUDITED_COMPLAINT_DETAIL',
      icon: 'complaint',
      activeMenu: '/un-audited/complaint',
    },
  },
]
