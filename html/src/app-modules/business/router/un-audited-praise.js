/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/business/un-audited-praise
 */

// import Layout from '@/layout'
import permissions from '@/constants/permissions'

export default [
  {
    path: '/un-audited/praise',
    name: 'unAuditedPraise',
    component: () => import('@/app-modules/business/views/un-audited/praise/list'),
    meta: {
      title: 'business.ROUTER.UN_AUDITED_PRAISE',
      icon: 'praise',
      routerId: permissions.UN_AUDITED_PRAISE_ID,
    },
  },
  {
    path: '/un-audited/praise/detail/:id',
    name: 'unAuditedPraiseDetail',
    component: () => import('@/app-modules/business/views/un-audited/praise/detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.UN_AUDITED_PRAISE_DETAIL',
      icon: 'praise',
      activeMenu: '/un-audited/praise',
    },
  },
]
