/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/business/un-audited-qa
 */

// import Layout from '@/layout'
import permissions from '@/constants/permissions'

export default [
  {
    path: '/un-audited/qa',
    name: 'unAuditedQa',
    component: () => import('@/app-modules/business/views/un-audited/qa/list'),
    meta: {
      title: 'business.ROUTER.UN_AUDITED_QA',
      icon: 'qa',
      routerId: permissions.UN_AUDITED_QA_ID,
    },
  },
  {
    path: '/un-audited/qa/detail/:id',
    name: 'unAuditedQaDetail',
    component: () => import('@/app-modules/business/views/un-audited/qa/detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.UN_AUDITED_QA_DETAIL',
      icon: 'qa',
      activeMenu: '/un-audited/qa',
    },
  },
]
