/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/business/praise
 */

// import Layout from '@/layout'
import permissions from '@/constants/permissions'

export default [
  {
    path: '/formal/praise',
    name: 'Praise',
    component: () => import('@/app-modules/business/views/formal/praise/list'),
    meta: {
      title: 'business.ROUTER.PRAISE',
      icon: 'praise',
      routerId: permissions.PRAISE_ID,
    },
  },
  {
    path: '/formal/praise/formal-detail/:id',
    name: 'FormalPraiseDetail',
    component: () => import('@/app-modules/business/views/formal/praise/formal-detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.PRAISE_DETAIL',
      icon: 'praise',
      activeMenu: '/formal/praise',
    },
  },
  {
    path: '/formal/praise/acceptance-detail/:id',
    name: 'AcceptancePraiseDetail',
    component: () => import('@/app-modules/business/views/formal/praise/acceptance-detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.PRAISE_DETAIL',
      icon: 'praise',
      activeMenu: '/formal/praise',
    },
  },
  {
    path: '/formal/praise/accept/:id',
    name: 'FormalPraiseAccept',
    component: () => import('@/app-modules/business/views/formal/praise/accept'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.PRAISE_ACCEPT',
      icon: 'praise',
      activeMenu: '/formal/praise',
    },
  },
  {
    path: '/formal/praise/re-accept/:id',
    name: 'AcceptancePraiseReAccept',
    component: () => import('@/app-modules/business/views/formal/praise/re-accept'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.PRAISE_RE_ACCEPT',
      icon: 'praise',
      activeMenu: '/formal/praise',
    },
  },
]
