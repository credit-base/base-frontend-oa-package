/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/business/complaint
 */

// import Layout from '@/layout'
import permissions from '@/constants/permissions'

export default [
  {
    path: '/formal/complaint',
    name: 'Complaint',
    component: () => import('@/app-modules/business/views/formal/complaint/list'),
    meta: {
      title: 'business.ROUTER.COMPLAINT',
      icon: 'complaint',
      routerId: permissions.COMPLAINT_ID,
    },
  },
  {
    path: '/formal/complaint/formal-detail/:id',
    name: 'FormalComplaintDetail',
    component: () => import('@/app-modules/business/views/formal/complaint/formal-detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.COMPLAINT_DETAIL',
      icon: 'complaint',
      activeMenu: '/formal/complaint',
    },
  },
  {
    path: '/formal/complaint/acceptance-detail/:id',
    name: 'AcceptanceComplaintDetail',
    component: () => import('@/app-modules/business/views/formal/complaint/acceptance-detail'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.COMPLAINT_DETAIL',
      icon: 'complaint',
      activeMenu: '/formal/complaint',
    },
  },
  {
    path: '/formal/complaint/accept/:id',
    name: 'FormalComplaintAccept',
    component: () => import('@/app-modules/business/views/formal/complaint/accept'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.COMPLAINT_ACCEPT',
      icon: 'complaint',
      activeMenu: '/formal/complaint',
    },
  },
  {
    path: '/formal/complaint/re-accept/:id',
    name: 'AcceptanceComplaintReAccept',
    component: () => import('@/app-modules/business/views/formal/complaint/re-accept'),
    hidden: true,
    meta: {
      title: 'business.ROUTER.COMPLAINT_RE_ACCEPT',
      icon: 'complaint',
      activeMenu: '/formal/complaint',
    },
  },
]
