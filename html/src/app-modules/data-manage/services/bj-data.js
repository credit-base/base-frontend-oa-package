/**
 * @file 接口 - 本级数据管理
 * @module data-manage/bj-data
 */

import request from '@/utils/request'
import {
  templateList,
  templateDetail,
  templateDataList,
  templateDataDetail,
  taskList,
} from '@/app-modules/data-manage/helpers/mocks'

/**
  * 获取本级资源目录
  * @param {*} params
  */
export function fetchBjTemplateList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...templateList,
      ],
      total: 3,
    }
  }

  return request('/api/bjTemplates', { params })
}

// fetchBjTemplateDetail

/**
  * 获取本级资源目录详情
  * @param {*} id
  */
export function fetchBjTemplateDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...templateDetail,
    }
  }
  return request(`/api/bjTemplates/${id}`, { params })
}

/**
  * 获取本级数据列表
  * @param {*} params
  */
export function fetchBjSearchData (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...templateDataList,
      ],
      total: 7,
    }
  }

  return request(`/api/bjSearchData`, { params })
}

/**
  * 获取本级数据详情
  * @param {*} id
  * @param {*} params
  */
export function fetchBjSearchDataDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...templateDataDetail,
    }
  }
  return request(`/api/bjSearchData/${id}`)
}

/**
  * 确认
  * @param {*} id
  */
export function updateBjDataConfirm (id) {
  return request(`/api/bjSearchData/${id}/confirm`, { method: 'POST' })
}

/**
  * 屏蔽
  * @param {*} id
  */
export function updateBjDataDisable (id) {
  return request(`/api/bjSearchData/${id}/disable`, { method: 'POST' })
}

// 封存
export function updateBjDataDelete (id) {
  return request(`/api/bjSearchData/${id}/delete`, { method: 'POST' })
}

/**
  * 本级任务
  * @param {*} id
  * @param {*} params
  */
export function fetchBjTaskList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...taskList,
      ],
      total: 3,
    }
  }

  return request(`/api/bjTasks`, { params })
}

export function fetchDataConversionRate (id, params = {}) {
  if (params.useMock) {
    const tempData = {
      columns: ['category', 'total'],
      rows: [
        { category: '数据转换', total: 24 },
        { category: '数据补全', total: 87 },
        { category: '数据比对', total: 35 },
        { category: '数据去重', total: 39 },
      ],
    }

    return {
      data: {
        status: 1,
        data: { ...tempData },
      },
    }
  }

  return request(`/api/statisticals/staticsByCategoryTotal?administrativeArea=${id}`, { params })
}

export function fetchDataVolume () {
  const list = [
    { date: '2日', area: 45, areaLimit: 100, industry: 80 },
    { date: '3日', area: 43, areaLimit: 267, industry: 120 },
    { date: '4日', area: 120, areaLimit: 122, industry: 60 },
    { date: '5日', area: 45, areaLimit: 100, industry: 80 },
    { date: '6日', area: 68, areaLimit: 267, industry: 120 },
    { date: '7日', area: 72, areaLimit: 122, industry: 60 },
    { date: '8日', area: 54, areaLimit: 82, industry: 110 },
  ]

  return {
    data: {
      status: 1,
      data: { list },
    },
  }
}

export function fetchStaticsByUserGroup (id, params = {}) {
  if (params.useMock) {
    const mockData = {
      columns: [
        'name',
        '发展和改革委员会',
        '市场监督管理局',
        '民政局',
        '法院',
        '公安局',
        '交通厅',
      ],
      rows: {
        name: '',
        发展和改革委员会: '237',
        市场监督管理局: '261',
        民政局: '173',
        法院: '147',
        公安局: '210',
        交通厅: '220',
      },
    }
    return {
      data: {
        status: 1,
        data: { ...mockData },
      },
    }
  }

  return request(`/api/statisticals/staticsByCategory?=administrativeArea=${id}`, { params })
}
