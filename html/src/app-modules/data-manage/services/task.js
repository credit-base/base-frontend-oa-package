/**
 * @file 接口 - 错误数据
 * @module data-manage/wbj-data
 */

import request from '@/utils/request'
import {
  errorDataList,
  errorDataDetail,
} from '@/app-modules/data-manage/helpers/mocks'

/**
 * 获取失败数据列表
 * @param {*} id
 * @param {*} params
 */
export function fetchErrorDataList (params = {}) {
  if (params.useMock) {
    return {
      total: 1,
      ...errorDataList,
    }
  }
  return request(`/api/errorData`, { params })
}

/**
 * 获取失败数据详情
 * @param {*} id
 * @param {*} params
 */
export function fetchErrorDataDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...errorDataDetail,
    }
  }
  return request(`/api/errorData/${id}`)
}

/**
 * 获取编辑失败数据接口
 * @param {*} id
 */
export function fetchEditErrorData (id) {
  return request(`/api/errorData/${id}/edit`)
}
