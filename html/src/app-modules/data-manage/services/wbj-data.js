/**
 * @file 接口 - 委办局数据管理
 * @module data-manage/wbj-data
 */

import request from '@/utils/request'

import {
  templateList,
  templateDetail,
  templateWbjDataList,
  templateWbjDataDetail,
  taskList,
} from '@/app-modules/data-manage/helpers/mocks'

/**
  * 获取委办局资源目录
  * @param {*} params
  */
export function fetchWbjTemplateList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...templateList,
      ],
      total: 3,
    }
  }

  return request('/api/wbjTemplates', { params })
}

// fetchWbjTemplateDetail

/**
  * 获取委办局资源目录详情
  * @param {*} id
  */
export function fetchWbjTemplateDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...templateDetail,
    }
  }
  return request(`/api/wbjTemplates/${id}`, { params })
}

/**
  * 获取委办局数据列表
  * @param {*} params
  */
export function fetchWbjSearchData (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...templateWbjDataList,
      ],
      total: 8,
    }
  }

  return request(`/api/wbjSearchData`, { params })
}

/**
  * 获取委办局数据详情
  * @param {*} id
  * @param {*} params
  */
export function fetchWbjSearchDataDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...templateWbjDataDetail,
    }
  }
  return request(`/api/wbjSearchData/${id}`)
}

/**
  * 屏蔽
  * @param {*} id
  */
export function updateWbjDataDisable (id) {
  return request(`/api/wbjSearchData/${id}/disable`, { method: 'POST' })
}

/**
  * 委办局任务
  * @param {*} id
  * @param {*} params
  */
export function fetchWbjTaskList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...taskList,
      ],
      total: 3,
    }
  }

  return request(`/api/wbjTasks`, { params })
}

export function fetchFillInOnlineInfo (id, params = {}) {
  if (params.useMock) {
    return {}
  }
  return request(`/api/wbjTemplates/${id}`)
}

export function createFillInOnlineInfo (id, params = {}) {
  return request(`/api/wbjSearchData/${id}/reportOnline`, { method: 'POST', params })
}

export function fetchDataConversionRate (id, params = {}) {
  if (params.useMock) {
    const tempData = {
      columns: ['category', 'total'],
      rows: [
        { category: '数据转换', total: 24 },
        { category: '数据补全', total: 87 },
        { category: '数据比对', total: 35 },
        { category: '数据去重', total: 39 },
      ],
    }

    return {
      data: {
        status: 1,
        data: { ...tempData },
      },
    }
  }

  return request(`/api/statisticals/staticsByCategoryTotal?administrativeArea=${id}`, { params })
}

export function fetchDataVolume () {
  const list = [
    { date: '2日', area: 45, areaLimit: 100, industry: 80 },
    { date: '3日', area: 43, areaLimit: 267, industry: 120 },
    { date: '4日', area: 120, areaLimit: 122, industry: 60 },
    { date: '5日', area: 45, areaLimit: 100, industry: 80 },
    { date: '6日', area: 68, areaLimit: 267, industry: 120 },
    { date: '7日', area: 72, areaLimit: 122, industry: 60 },
    { date: '8日', area: 54, areaLimit: 82, industry: 110 },
  ]

  return {
    data: {
      status: 1,
      data: { list },
    },
  }
}

export function fetchStaticsByUserGroup (id, params = {}) {
  if (params.useMock) {
    const mockData = {
      columns: [
        'name',
        '发展和改革委员会',
        '市场监督管理局',
        '民政局',
        '法院',
        '公安局',
        '交通厅',
      ],
      rows: {
        name: '',
        发展和改革委员会: '237',
        市场监督管理局: '261',
        民政局: '173',
        法院: '147',
        公安局: '210',
        交通厅: '220',
      },
    }
    return {
      data: {
        status: 1,
        data: { ...mockData },
      },
    }
  }

  return request(`/api/statisticals/staticsByCategory?=administrativeArea=${id}`, { params })
}
