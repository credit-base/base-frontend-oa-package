/**
 * @file 接口 - 国标数据管理
 * @module data-manage/gb-data
 */

import request from '@/utils/request'

import {
  templateList,
  templateDetail,
  templateDataList,
  templateDataDetail,
  taskList,
} from '../helpers/mocks'

/**
 * 获取国标资源目录
 * @param {*} params
 */
export function fetchGbTemplateList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...templateList,
      ],
      total: 3,
    }
  }
  return request('/api/gbTemplates', { params })
}

// fetchGbTemplateDetail

/**
 * 获取国标资源目录详情
 * @param {*} id
 */
export function fetchGbTemplateDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...templateDetail,
    }
  }
  return request(`/api/gbTemplates/${id}`, { params })
}

/**
 * 获取国标数据列表
 * @param {*} params
 */
export function fetchGbSearchData (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...templateDataList,
      ],
      total: 7,
    }
  }

  return request(`/api/gbSearchData`, { params })
}

/**
 * 获取国标数据详情
 * @param {*} id
 * @param {*} params
 */
export function fetchGbSearchDataDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...templateDataDetail,
    }
  }
  return request(`/api/gbSearchData/${id}`)
}

/**
 * 确认
 * @param {*} id
 */
export function updateGbDataConfirm (id) {
  return request(`/api/gbSearchData/${id}/confirm`, { method: 'POST' })
}

/**
 * 屏蔽
 * @param {*} id
 */
export function updateGbDataDisable (id) {
  return request(`/api/gbSearchData/${id}/disable`, { method: 'POST' })
}

// 封存
export function updateGbDataDelete (id) {
  return request(`/api/gbSearchData/${id}/delete`, { method: 'POST' })
}

/**
 * 国标任务
 * @param {*} id
 * @param {*} params
 */
export function fetchGbTaskList (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...taskList,
      ],
      total: 3,
    }
  }

  return request(`/api/gbTasks`, { params })
}

export function fetchDataConversionRate (id, params = {}) {
  if (params.useMock) {
    const tempData = {
      columns: ['category', 'total'],
      rows: [
        { category: '数据转换', total: 24 },
        { category: '数据补全', total: 87 },
        { category: '数据比对', total: 35 },
        { category: '数据去重', total: 39 },
      ],
    }

    return {
      data: {
        status: 1,
        data: { ...tempData },
      },
    }
  }

  return request(`/api/statisticals/staticsByCategoryTotal?administrativeArea=${id}`, { params })
}

export function fetchDataVolume () {
  const list = [
    { date: '2日', area: 45, areaLimit: 100, industry: 80 },
    { date: '3日', area: 43, areaLimit: 267, industry: 120 },
    { date: '4日', area: 120, areaLimit: 122, industry: 60 },
    { date: '5日', area: 45, areaLimit: 100, industry: 80 },
    { date: '6日', area: 68, areaLimit: 267, industry: 120 },
    { date: '7日', area: 72, areaLimit: 122, industry: 60 },
    { date: '8日', area: 54, areaLimit: 82, industry: 110 },
  ]

  return {
    data: {
      status: 1,
      data: { list },
    },
  }
}

export function fetchDataTask () {
  const list = [
    {
      month: '1月',
      punishment: 80,
      reward: 140,
    },
    {
      month: '2月',
      punishment: 50,
      reward: 140,
    },
    {
      month: '3月',
      punishment: 100,
      reward: 20,
    },
    {
      month: '4月',
      punishment: 70,
      reward: 120,
    },
    {
      month: '5月',
      punishment: 10,
      reward: 180,
    },
    {
      month: '6月',
      punishment: 40,
      reward: 160,
    },
  ]

  return {
    data: {
      status: 1,
      data: { list },
    },
  }
}

export function fetchStaticsByUserGroup (id, params = {}) {
  if (params.useMock) {
    const mockData = {
      columns: [
        'name',
        '发展和改革委员会',
        '市场监督管理局',
        '民政局',
        '法院',
        '公安局',
        '交通厅',
      ],
      rows: {
        name: '',
        发展和改革委员会: '237',
        市场监督管理局: '261',
        民政局: '173',
        法院: '147',
        公安局: '210',
        交通厅: '220',
      },
    }
    return {
      data: {
        status: 1,
        data: { ...mockData },
      },
    }
  }

  return request(`/api/statisticals/staticsByCategory?=administrativeArea=${id}`, { params })
}

// eslint-disable-next-line max-lines-per-function
export function getGbErrorData (id, params = {}) {
  if (params.useMock) {
    return {
      id: 'NTM',
      name: '行政许可信息',
      identify: 'HZXKXX',
      subjectCategory: [
        {
          id: 'MA',
          name: '法人及非法人组织',
        },
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      exchangeFrequency: {
        id: 'MA',
        name: '实时',
      },
      dimension: {
        id: 'MA',
        name: '社会公开',
        label: '社',
        type: 'primary',
      },
      infoClassify: {
        id: 'MA',
        name: '行政许可',
        label: '许可',
        type: 'danger',
      },
      infoCategory: {
        id: 'MA',
        name: '基础信息',
        label: '基础',
        type: 'primary',
      },
      description: '行政许可信息',
      items: [
        {
          name: '行政相对人名称',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '200',
          options: [],
          remarks: '信用主体的法定名称',
          identify: 'ZTMC',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '四川瑞煜建筑责任有限公司信阳分公司',
        },
        {
          name: '行政相对人类别',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '16',
          options: [],
          remarks: '',
          identify: 'XK_XDR_LB',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '法人及非法人组织',
        },
        {
          name: '行政相对人代码_1(统一社会信用代码)',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '18',
          options: [],
          remarks: '',
          identify: 'TYSHXYDM',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '91411500MA9F86JG95',
        },
        {
          name: '行政相对人代码_2 (工商注册号)',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '50',
          options: [],
          remarks: '',
          identify: 'XK_XDR_GSZC',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '91092',
        },
        {
          name: '行政相对人代码_3(组织机构代码)',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '9',
          options: [],
          remarks: '',
          identify: 'XK_XDR_ZZJG',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '91092',
        },
        {
          name: '行政相对人代码_4(税务登记号)',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '15',
          options: [],
          remarks: '',
          identify: 'XK_XDR_SWDJ',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '91092',
        },
        {
          name: '行政相对人代码_5(事业单位证书号)',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '12',
          options: [],
          remarks: '',
          identify: 'XK_XDR_SYDW',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '91092',
        },
        {
          name: '行政相对人代码_6(社会组织登记证号)',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '50',
          options: [],
          remarks: '',
          identify: 'XK_XDR_SHZZ',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: 'MA9F86JG-9',
        },
        {
          name: '法定代表人',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '50',
          options: [],
          remarks: '',
          identify: 'XK_FRDB',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '王灿',
        },
        {
          name: '法定代表人证件类型',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '64',
          options: [],
          remarks: '',
          identify: 'XK_FR_ZJLX',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '身份证',
        },
        {
          name: '法定代表人证件号码',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '64',
          options: [],
          remarks: '',
          identify: 'XK_FR_ZJHM',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '412825199009098765',
        },
        {
          name: '证件类型',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '64',
          options: [],
          remarks: '',
          identify: 'XK_XDR_ZJLX',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '身份证',
        },
        {
          name: '证件号码',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '64',
          options: [],
          remarks: '',
          identify: 'XK_XDR_ZJHM',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '412825199009098765',
        },
        {
          name: '行政许可决定文书名称',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '64',
          options: [],
          remarks: '',
          identify: 'XK_XKWS',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '企业注册登记',
        },
        {
          name: '行政许可决定文书号',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '64',
          options: [],
          remarks: '',
          identify: 'XK_WSH',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '411596000077514',
        },
        {
          name: '许可类别',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '256',
          options: [],
          remarks: '',
          identify: 'XK_XKLB',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '登记',
        },
        {
          name: '许可证书名称',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '64',
          options: [],
          remarks: '',
          identify: 'XK_XKZS',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '企业注册登记',
        },
        {
          name: '许可编号',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '64',
          options: [],
          remarks: '',
          identify: 'XK_XKBH',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '410000017210705A000091',
        },
        {
          name: '许可内容',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '4000',
          options: [],
          remarks: '',
          identify: 'XK_NR',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '企业注册登记',
        },
        {
          name: '许可决定日期',
          type: {
            id: 'MQ',
            name: '日期型',
          },
          length: '8',
          options: [],
          remarks: '',
          identify: 'XK_JDRQ',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '20210712',
        },
        {
          name: '有效期自',
          type: {
            id: 'MQ',
            name: '日期型',
          },
          length: '8',
          options: [],
          remarks: '',
          identify: 'XK_YXQZ',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '20210714',
        },
        {
          name: '有效期至',
          type: {
            id: 'MQ',
            name: '日期型',
          },
          length: '8',
          options: [],
          remarks: '',
          identify: 'XK_YXQZI',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '20200819',
        },
        {
          name: '许可机关',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '200',
          options: [],
          remarks: '',
          identify: 'XK_XKJG',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '信阳市市场监督管理局专业分局',
        },
        {
          name: '许可机关统一社会信用代码',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '18',
          options: [],
          remarks: '',
          identify: 'XK_XKJGDM',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '11411500747429737F',
        },
        {
          name: '当前状态',
          type: {
            id: 'Mg',
            name: '整数型',
          },
          length: '1',
          options: [],
          remarks: '',
          identify: 'XK_ZT',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '1',
        },
        {
          name: '数据来源单位',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '200',
          options: [],
          remarks: '',
          identify: 'XK_LYDW',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '信阳市发展和改革委员会',
        },
        {
          name: '数据来源单位统一社会信用代码',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '18',
          options: [],
          remarks: '',
          identify: 'XK_LYDWDM',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '11411500006067580U',
        },
        {
          name: '备注',
          type: {
            id: 'MA',
            name: '字符型',
          },
          length: '4000',
          options: [],
          remarks: '',
          identify: 'BZ',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '11411500006067580U',
        },
        {
          name: '主体类别',
          type: {
            id: 'NA',
            name: '枚举型',
          },
          length: '16',
          options: [
            '法人及非法人组织',
            '自然人',
            '个体工商户',
          ],
          remarks: '',
          identify: 'ZTLB',
          isMasked: {
            id: 'Lw',
            name: '否',
          },
          maskRule: [],
          dimension: {
            id: 'MA',
            name: '社会公开',
          },
          isNecessary: {
            id: 'MA',
            name: '是',
          },
          value: '法人及非法人组织',
        },
      ],
      updateTime: 1627559296,
      updateTimeFormat: '2021年07月29日 19时48分',
      createTime: 1626745278,
      sourceUnit: {
        id: 'MA',
        name: '发展和改革委员会',
      },
      ruleCount: 0,
      dataTotal: 0,
      category: 10,
    }
  }
  return request(`/api/${id}`)
}
