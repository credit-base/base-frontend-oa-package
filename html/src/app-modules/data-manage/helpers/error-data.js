import Base from '@/models/base'

export function translateItems (data = [], status = {}) {
  const statusId = status.id
  const statusSuccess = 'Lw'

  if (!Array.isArray(data) || !data.length) return []

  const res = data.map(item => {
    if (statusId !== statusSuccess && !item.errorReason) {
      item.errorReason = '未知错误，请联系技术人员'
    }
    return item
  })

  return res
}

class ErrorData extends Base {
  constructor (params = {}) {
    super(params)

    const {
      id,
      name,
      errorReason,
      identify,
      itemsData = translateItems(params.itemsData, params.status),
      status = {},
      createTime,
      createTimeFormat,
      updateTime,
      updateTimeFormat,
    } = params

    this.id = id
    this.name = name
    this.errorReason = errorReason
    this.identify = identify
    this.itemsData = itemsData
    this.status = status
    this.createTime = createTime
    this.createTimeFormat = createTimeFormat
    this.updateTime = updateTime
    this.updateTimeFormat = updateTimeFormat
  }
}

export default ErrorData
