/**
 * @file 工具方法
 */

import { FORM_TYPE_ENUM } from '@/app-modules/data-manage/helpers/constants'

/**
 * 获取验证触发事件
 * @param {string} type 模板项类型
 * @returns {string} 触发验证事件
 */
export function getValidatorTriggerEvents (type) {
  return [
    FORM_TYPE_ENUM.TEXT,
    FORM_TYPE_ENUM.FLOAT,
    FORM_TYPE_ENUM.NUMBER,
  ].includes(type)
    ? `blur`
    : `change`
}
