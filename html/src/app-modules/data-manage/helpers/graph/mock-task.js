/**
 * @file Mock 任务流向图
 * @module graph/mock-task
 */
import {
  formatTime,
  getUnixTimeStamp,
} from '@/utils/date-time'
import { randomNumber } from '@/utils'

// 图表尺寸
export const CHART_VIEW = { x: 1200, y: 800 }

// 图表内填充
export const CHART_PADDING = 20

// 分类配置
export const CATEGORY_CONFIGS = {
  resource: 0,
  origin: 1,
  userGroup: 2,
  gb: 3,
  bj: 4,
  cloud: 5,
  usage: 6,
}

// Echarts 配置
export const LABEL_STYLE = {
  common: {
    width: 200,
    borderWidth: 1,
    padding: [5, 12],
    borderRadius: 8,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    lineHeight: 18,
    color: 'white',
    formatter: params => {
      const { data: { fields = {} } = {} } = params
      const text = [params.name]

      if (fields.dataCount) {
        text.push(`${fields.dataCount}条`)
      }

      if (fields.successCount) {
        text.push(`成功: ${fields.successCount}条`)
      }

      if (fields.failCount) {
        text.push(`失败: ${fields.failCount}条`)
      }

      return text.join('\n')
    },
  },
}
export const TOOLTIP_STYLE = {
  common: {},
}

export function calcNodePosition ({
  rows,
  rowIdx,
  colIdx,
  nodeWidth = 0,
  nodeHeight = 0,
  startY = 0,
  endY = 0,
}, {
  cols = 5,
  viewWidth = CHART_VIEW.x,
  viewHeight = CHART_VIEW.y,
  gutter = CHART_PADDING,
} = {}) {
  const colWidth = (viewWidth - 2 * gutter) / cols
  const x = gutter + (2 * colIdx + 1) / 2 * colWidth
  let rowHeight = 0
  let y = 0

  if (startY && !endY) {
    rowHeight = (viewHeight - startY - 2 * gutter) / rows
  } else if (endY && !startY) {
    rowHeight = (viewHeight - endY - 2 * gutter) / rows
  } else if (startY && endY) {
    rowHeight = (endY - startY) / rows
  } else {
    rowHeight = (viewHeight - 2 * gutter) / rows
  }

  if (startY && !endY) {
    y = gutter + startY + (2 * rowIdx + 1) / 2 * rowHeight
  } else if (endY && !startY) {
    y = gutter + (2 * rowIdx + 1) / 2 * rowHeight
  } else if (startY && endY) {
    y = startY + (2 * rowIdx + 1) / 2 * rowHeight
  } else {
    y = gutter + (2 * rowIdx + 1) / 2 * rowHeight
  }

  return [x, y]
}

// ================================================
//                    Mock 数据
// ================================================

// 数据来源方
export const RESOURCE_DATA_NODES = [
  {
    name: '省信息中心',
    targetNames: ['原始数据库'],
    uploadTime: formatTime(getUnixTimeStamp()),
    fields: {
      dataCount: randomNumber(50),
    },
  },
  {
    name: '平台上传',
    targetNames: ['原始数据库'],
    uploadTime: formatTime(getUnixTimeStamp()),
    fields: {
      dataCount: randomNumber(50),
    },
  },
].map((node, idx) => {
  return {
    ...node,
    key: `resource_${idx + 1}`,
    value: calcNodePosition({ colIdx: 0, rowIdx: idx, rows: 2 }),
    category: CATEGORY_CONFIGS.resource,
    label: LABEL_STYLE.common,
    tooltip: TOOLTIP_STYLE.common,
  }
})

// 原始数据库
export const ORIGIN_DATA_NODES = [
  {
    name: '原始数据库',
    targetNames: ['委办局数据库', '前置机原始库'],
    uploadTime: formatTime(getUnixTimeStamp()),
    fields: {
      successCount: randomNumber(50),
      failCount: randomNumber(50),
    },
  },
].map((node, idx) => {
  return {
    ...node,
    key: `origin_${idx + 1}`,
    value: calcNodePosition({ colIdx: 1, rowIdx: idx, rows: 1 }),
    category: CATEGORY_CONFIGS.origin,
    label: LABEL_STYLE.common,
    tooltip: TOOLTIP_STYLE.common,
  }
})

// 委办局数据库
export const USER_GROUP_DATA_NODES = [
  {
    name: '委办局数据库',
    targetNames: ['国标数据库', '本级数据库', '前置机委办局库'],
    uploadTime: formatTime(getUnixTimeStamp()),
    fields: {
      successCount: randomNumber(50),
      failCount: randomNumber(50),
    },
  },
].map((node, idx) => {
  return {
    key: `user_group_${idx + 1}`,
    value: calcNodePosition({
      colIdx: 2,
      rowIdx: idx,
      rows: 1,
    }),
    category: CATEGORY_CONFIGS.userGroup,
    label: LABEL_STYLE.common,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})

// 国标数据库
export const GB_DATA_NODES = [
  {
    name: '国标数据库',
    targetNames: ['前置机国标库'],
    uploadTime: formatTime(getUnixTimeStamp()),
    fields: {
      successCount: randomNumber(50),
      failCount: randomNumber(50),
    },
  },
].map((node, idx) => {
  return {
    key: `gb_${idx + 1}`,
    value: calcNodePosition({
      colIdx: 3,
      rowIdx: idx,
      rows: 1,
      startY: CHART_VIEW.y / 4,
      endY: CHART_VIEW.y / 2,
    }),
    category: CATEGORY_CONFIGS.gb,
    label: LABEL_STYLE.common,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})

// 本级数据库
export const BJ_DATA_NODES = [
  {
    name: '本级数据库',
    targetNames: ['前置机本级库'],
    uploadTime: formatTime(getUnixTimeStamp()),
    fields: {
      successCount: randomNumber(50),
      failCount: randomNumber(50),
    },
  },
].map((node, idx) => {
  return {
    key: `bj_${idx + 1}`,
    value: calcNodePosition({
      colIdx: 3,
      rowIdx: idx,
      rows: 1,
      startY: CHART_VIEW.y / 2,
      endY: CHART_VIEW.y / 4 * 3,
    }),
    category: CATEGORY_CONFIGS.bj,
    label: LABEL_STYLE.common,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})

// 前置机
export const CLOUD_DATABASE_NODES = [
  {
    name: '前置机委办局库',
    fields: {
      successCount: randomNumber(50),
    },
  },
  {
    name: '前置机国标库',
    fields: {
      successCount: randomNumber(50),
    },
  },
  {
    name: '前置机本级库',
    fields: {
      successCount: randomNumber(50),
    },
  },
  {
    name: '前置机原始库',
    fields: {
      successCount: randomNumber(50),
    },
  },
].map((node, idx) => {
  return {
    key: `database_${idx}`,
    value: calcNodePosition({ colIdx: 4, rowIdx: idx, rows: 4 }),
    category: CATEGORY_CONFIGS.cloud,
    label: LABEL_STYLE.common,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})
