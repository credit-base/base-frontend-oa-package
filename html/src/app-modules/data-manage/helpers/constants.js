/**
 * @file 数据管理 - 常量
 * @module data-manage/constants
 */

export const DATA_MANAGE_TABS = [
  { name: 'template-list', label: 'TEMPLATE_LIST', component: 'ListViewTemplate' },
  { name: 'template-flow-diagram', label: 'TEMPLATE_FLOW_DIAGRAM', component: 'DetailViewTemplateFlowDiagram' },
]

// 国标
export const GB_DATA_DETAIL_TABS = [
  { name: 'data-traceability-graph', label: 'DATA_TRACEABILITY_GRAPH', component: 'ComponentViewTraceabilityGraph' },
  // { name: 'status-change', label: 'STATUS_CHANGE', component: 'DetailViewStatusChange' },
  { name: 'data-detail', label: 'DATA_DETAIL', component: 'DetailViewGbData' },
]

export const GB_TEMPLATE_DETAIL_TABS = [
  { name: 'data-flow-diagram', label: 'DATA_FLOW_DIAGRAM', component: 'DetailViewDataFlowDiagram' },
  { name: 'detail-view-statistics', label: 'DATA_STATISTICS', component: 'DetailViewStatistics' },
  { name: 'data-list', label: 'DATA_LIST', component: 'ListViewGbData' },
  { name: 'task-list', label: 'TASK_LIST', component: 'ListViewGbTask' },
]

// 本级
export const BJ_DATA_DETAIL_TABS = [
  { name: 'data-traceability-graph', label: 'DATA_TRACEABILITY_GRAPH', component: 'ComponentViewTraceabilityGraph' },
  // { name: 'status-change', label: 'STATUS_CHANGE', component: 'DetailViewStatusChange' },
  { name: 'data-detail', label: 'DATA_DETAIL', component: 'DetailViewBjData' },
]

export const BJ_TEMPLATE_DETAIL_TABS = [
  { name: 'data-flow-diagram', label: 'DATA_FLOW_DIAGRAM', component: 'DetailViewDataFlowDiagram' },
  { name: 'detail-view-statistics', label: 'DATA_STATISTICS', component: 'DetailViewStatistics' },
  { name: 'data-list', label: 'DATA_LIST', component: 'ListViewBjData' },
  { name: 'task-list', label: 'TASK_LIST', component: 'ListViewBjTask' },
]

// 委办局
export const WBJ_DATA_DETAIL_TABS = [
  { name: 'data-traceability-graph', label: 'DATA_TRACEABILITY_GRAPH', component: 'ComponentViewTraceabilityGraph' },
  // { name: 'status-change', label: 'STATUS_CHANGE', component: 'DetailViewStatusChange' },
  { name: 'data-detail', label: 'DATA_DETAIL', component: 'DetailViewWbjData' },
]

export const WBJ_TEMPLATE_DETAIL_TABS = [
  { name: 'data-flow-diagram', label: 'DATA_FLOW_DIAGRAM', component: 'DetailViewDataFlowDiagram' },
  { name: 'detail-view-statistics', label: 'DATA_STATISTICS', component: 'DetailViewStatistics' },
  { name: 'data-list', label: 'DATA_LIST', component: 'ListViewWbjData' },
  { name: 'task-list', label: 'TASK_LIST', component: 'ListViewWbjTask' },
]

export const TEMPLATE_TYPE_ENUM = {
  GB: 'TYPE_GB',
  BJ: 'TYPE_BJ',
  WBJ: 'TYPE_WBJ',
}

export const RAW_DATA_DETAIL_TABS = [
  { name: 'data-traceability-graph', label: 'DATA_TRACEABILITY_GRAPH', component: 'ComponentViewTraceabilityGraph' },
  { name: 'data-detail', label: 'DATA_DETAIL', component: 'DetailViewData' },
]

export const RAW_TEMPLATE_DETAIL_TABS = [
  { name: 'data-flow-diagram', label: 'DATA_FLOW_DIAGRAM', component: 'DetailViewDataFlowDiagram' },
  { name: 'detail-view-statistics', label: 'DATA_STATISTICS', component: 'DetailViewStatistics' },
  { name: 'data-list', label: 'DATA_LIST', component: 'ListViewRawData' },
  { name: 'task-list', label: 'TASK_LIST', component: 'ListViewRawTask' },
]

export const TEMPLATE_TAG_THEMES = {
  MA: 'theme-success',
  Mg: 'theme-success',
  MQ: 'theme-danger',
  Mw: 'theme-danger',
  NA: 'theme-warning',
}

export const COMMON_CHART_COMMANDS = [
  { label: '导出数据', value: 'EXPORT' },
]

export const DATE_CHART_COMMANDS = [
  { label: '导出数据', value: 'EXPORT' },
  { label: '7天', value: 7 },
  { label: '15天', value: 15 },
  { label: '1个月', value: 30 },
  { label: '半年', value: 180 },
]

export const DATA_STATUS = [
  {
    text: '待确认',
    value: 'Lw',
  },
  {
    text: '已确认',
    value: 'MQ',
  },
]

export const SEARCH_TYPE = [
  {
    label: '主体名称',
    value: 'name',
  },
  {
    label: '主体标识',
    value: 'identify',
  },
]

export const TASK_STATUS = [
  {
    text: '成功',
    value: 'MQ',
  },
  {
    text: '失败',
    value: 'LC0',
  },
]

export const DATABASE_TYPE_ENUM = [
  {
    id: 'MA',
    name: 'MySQL',
  },
  {
    id: 'MQ',
    name: 'GBase',
  },
  {
    id: 'Mg',
    name: 'Oracle',
  },
  {
    id: 'Mw',
    name: 'KingBase',
  },
  {
    id: 'NA',
    name: '达梦',
  },
  {
    id: 'NQ',
    name: 'MongoDB',
  },
]

export const GB_DATA_PATH = `/data-manage/gb-data`
export const GB_DATA_LIST_PATH = `/data-manage/gb-data/template-detail/`
export const GB_DATA_DETAIL_PATH = `/data-manage/gb-data/data-detail/`
export const GB_TASK_FLOW_GRAPH_PATH = `/data-manage/gb-data/task-flow-graph/`
export const GB_ERROR_DATA_LIST_PATH = `/data-manage/gb-data/gb-error-data-list/`
export const GB_ERROR_DATA_DETAIL_PATH = `/data-manage/gb-data/gb-error-data-detail/`
export const GB_ERROR_DATA_EDIT_PATH = `/data-manage/gb-data/gb-error-data-edit/`

export const BJ_DATA_PATH = `/data-manage/bj-data`
export const BJ_DATA_LIST_PATH = `/data-manage/bj-data/template-detail/`
export const BJ_DATA_DETAIL_PATH = `/data-manage/bj-data/data-detail/`
export const BJ_TASK_FLOW_GRAPH_PATH = `/data-manage/bj-data/task-flow-graph/`
export const BJ_ERROR_DATA_LIST_PATH = `/data-manage/bj-data/bj-error-data-list/`
export const BJ_ERROR_DATA_DETAIL_PATH = `/data-manage/bj-data/bj-error-data-detail/`
export const BJ_ERROR_DATA_EDIT_PATH = `/data-manage/bj-data/bj-error-data-edit/`

export const WBJ_DATA_PATH = `/data-manage/wbj-data`
export const WBJ_DATA_LIST_PATH = `/data-manage/wbj-data/template-detail/`
export const WBJ_DATA_DETAIL_PATH = `/data-manage/wbj-data/data-detail/`
export const WBJ_TASK_FLOW_GRAPH_PATH = `/data-manage/wbj-data/task-flow-graph/`
export const FILL_IN_ONLINE_PATH = `/data-manage/wbj-data/fill-in-online/`
export const IMPORT_FRONT_END_PROCESSOR_PATH = `/data-manage/wbj-data/front-end-processor/`
export const WBJ_ERROR_DATA_LIST_PATH = `/data-manage/wbj-data/wbj-error-data-list/`
export const WBJ_ERROR_DATA_DETAIL_PATH = `/data-manage/wbj-data/wbj-error-data-detail/`
export const WBJ_ERROR_DATA_EDIT_PATH = `/data-manage/wbj-data/wbj-error-data-edit/`

export const FORM_TYPE_ENUM = {
  TEXT: 'MA', // 字符型
  DATE: 'MQ', // 日期型
  NUMBER: 'Mg', // 整数型
  FLOAT: 'Mw', // 浮点型
  RADIO: 'NA', // 枚举型 单选
  CHECKBOX: 'NQ', // 集合型 多选
}

export const IS_NECESSARY = {
  YES: 'MA',
  NO: 'Lw',
}
