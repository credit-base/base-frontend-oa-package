/* eslint-disable max-lines */
export const templateList = [
  {
    id: 'MA',
    name: '登记信息',
    ruleCount: 3,
    exchangeFrequency: {
      id: 'MA',
      name: '实时',
    },
    dataTotal: 10,
    reportStatus: {
      id: 'Lw',
      name: '未上报',
    },
    subjectCategory: [
      {
        id: 'MA',
        name: '自然人',
      },
      {
        id: 'MQ',
        name: '个体工商户',
      },
    ],
    infoClassify: {
      id: 'MA',
      name: '行政许可',
      label: '许可',
      type: 'success',
    },
    dimension: {
      id: 'MA',
      name: '社会公开',
      label: '社',
      type: 'primary',
    },
    infoCategory: {
      id: 'MA',
      name: '基础信息',
      label: '基础',
      type: 'primary',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'MQ',
    name: '变更登记信息',
    ruleCount: 3,
    exchangeFrequency: {
      id: 'MA',
      name: '实时',
    },
    dataTotal: 10,
    reportStatus: {
      id: 'Lw',
      name: '未上报',
    },
    subjectCategory: [
      {
        id: 'MA',
        name: '自然人',
      },
      {
        id: 'MQ',
        name: '个体工商户',
      },
    ],
    infoClassify: {
      id: 'MA',
      name: '行政许可',
      label: '许可',
      type: 'success',
    },
    dimension: {
      id: 'MA',
      name: '社会公开',
      label: '社',
      type: 'primary',
    },
    infoCategory: {
      id: 'MA',
      name: '基础信息',
      label: '基础',
      type: 'primary',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'Mg',
    name: '股权结构信息',
    ruleCount: 3,
    exchangeFrequency: {
      id: 'MA',
      name: '实时',
    },
    dataTotal: 10,
    reportStatus: {
      id: 'Lw',
      name: '未上报',
    },
    subjectCategory: [
      {
        id: 'MA',
        name: '自然人',
      },
      {
        id: 'MQ',
        name: '个体工商户',
      },
    ],
    infoClassify: {
      id: 'MA',
      name: '行政许可',
      label: '许可',
      type: 'success',
    },
    dimension: {
      id: 'MA',
      name: '社会公开',
      label: '社',
      type: 'primary',
    },
    infoCategory: {
      id: 'MA',
      name: '基础信息',
      label: '基础',
      type: 'primary',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
  },
]

export const templateDetail = {
  id: 'MA',
  name: '登记信息',
  identify: 'FR_DJXX',
  ruleCount: 3,
  exchangeFrequency: {
    id: 'MA',
    name: '实时',
  },
  dataTotal: 10,
  reportStatus: {
    id: 'Lw',
    name: '未上报',
  },
  subjectCategory: [
    {
      id: 'MA',
      name: '自然人',
    },
    {
      id: 'MQ',
      name: '个体工商户',
    },
  ],
  infoClassify: {
    id: 'MA',
    name: '行政许可',
    label: '许可',
    type: 'success',
  },
  dimension: {
    id: 'MA',
    name: '社会公开',
    label: '社',
    type: 'primary',
  },
  infoCategory: {
    id: 'MA',
    name: '基础信息',
    label: '基础',
    type: 'primary',
  },
  description: '目录描述',
  updateTime: 1576317765,
  updateTimeFormat: '2021年06月24日 15时32分',
  createTime: 1576317765,
  createTimeFormat: '2021年06月24日 15时32分',
  sourceUnit: {
    id: 'MQ',
    name: '市场监督管理局',
  },
}

export const templateDataList = [
  {
    id: 'MA',
    name: 'e租出租公司',
    identify: '232523652365236520',
    status: {
      id: 'MQ',
      name: '已确认',
      type: 'success',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'MQ',
    name: '中研社食品有限责任公司',
    identify: '91120104MA06FBJY36',
    status: {
      id: 'Lw',
      name: '待确认',
      type: 'warning',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'Mg',
    name: '老百姓大药房',
    identify: '524125632541236523',
    status: {
      id: 'Lw',
      name: '待确认',
      type: 'warning',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'Mw',
    name: '博源电器公司',
    identify: '91120104MA06FBJY36',
    status: {
      id: 'Lw',
      name: '待确认',
      type: 'warning',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'NA',
    name: '锦鹏商贸有限责任公司',
    identify: '91511502692270078T',
    status: {
      id: 'Lw',
      name: '待确认',
      type: 'warning',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'NQ',
    name: '宏益汽车销售服务有限公司',
    identify: '91511502692270078T',
    status: {
      id: 'Lw',
      name: '待确认',
      type: 'warning',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'Ng',
    name: '青清废弃物治理有限责任公司',
    identify: '91511502772961282D',
    status: {
      id: 'Lw',
      name: '待确认',
      type: 'warning',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'Nw',
    name: '均田坝石厂',
    identify: '91511524793962348Y',
    status: {
      id: 'Lw',
      name: '待确认',
      type: 'warning',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
]

export const templateDataDetail = {
  id: 'MA',
  name: 'e租出租公司',
  identify: '232523652365236520',
  infoClassify: {
    id: 'MA',
    name: '行政许可',
    label: '许可',
    type: 'success',
  },
  infoCategory: {
    id: 'MA',
    name: '基础信息',
    label: '基础',
    type: 'primary',
  },
  expirationDateFormat: '2021-12-12',
  subjectCategory: [
    {
      id: 'MA',
      name: '自然人',
    },
    {
      id: 'MQ',
      name: '个体工商户',
    },
  ],
  dimension: {
    id: 'MA',
    name: '社会公开',
  },
  itemsData: [
    {
      name: '单位名称',
      value: '中研社食品有限责任公司',
    },
    {
      name: '统一社会信用代码',
      value: '91120104MA06FBJY36',
    },
    {
      name: '项目代码',
      value: '〔2017〕24号',
    },
    {
      name: '项目名称',
      value: '粮食项目',
    },
    {
      name: '事项名称',
      value: '投资项目',
    },
    {
      name: '办理部门',
      value: '发展和改革委员会',
    },
    {
      name: '办理时间',
      value: '2019-01-01',
    },
    {
      name: '主体类别',
      value: '法人和其他组织',
    },
    {
      name: '信息类别',
      value: '基础信息',
    },
    {
      name: '数据共享范围',
      value: '政务共享',
    },
    {
      name: '有效期限',
      value: '长期',
    },
    {
      name: '交换频率',
      value: '实时',
    },
    {
      name: '公开属性',
      value: '不公开',
    },
    {
      name: '对应权利类别',
      value: '无',
    },
  ],
  status: {
    id: 'Lw',
    name: '待确认',
    type: 'warning',
  },
  updateTime: 1576317765,
  updateTimeFormat: '2021年06月24日 15时32分',
}

export const templateWbjDataList = [
  {
    id: 'MA',
    name: 'e租出租公司',
    identify: '232523652365236520',
    status: {
      id: 'MQ',
      name: '正常',
      type: 'success',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'MQ',
    name: '中研社食品有限责任公司',
    identify: '91120104MA06FBJY36',
    status: {
      id: 'MQ',
      name: '正常',
      type: 'success',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'Mg',
    name: '老百姓大药房',
    identify: '524125632541236523',
    status: {
      id: 'MQ',
      name: '正常',
      type: 'success',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'Mw',
    name: '博源电器公司',
    identify: '91120104MA06FBJY36',
    status: {
      id: 'MQ',
      name: '正常',
      type: 'success',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'NA',
    name: '锦鹏商贸有限责任公司',
    identify: '91511502692270078T',
    status: {
      id: 'MQ',
      name: '正常',
      type: 'success',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'NQ',
    name: '宏益汽车销售服务有限公司',
    identify: '91511502692270078T',
    status: {
      id: 'MQ',
      name: '正常',
      type: 'success',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'Ng',
    name: '青清废弃物治理有限责任公司',
    identify: '91511502772961282D',
    status: {
      id: 'MQ',
      name: '正常',
      type: 'success',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'Nw',
    name: '均田坝石厂',
    identify: '91511524793962348Y',
    status: {
      id: 'MQ',
      name: '正常',
      type: 'success',
    },
    updateTime: 1576317765,
    updateTimeFormat: '2021年06月24日 15时32分',
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
]

export const templateWbjDataDetail = {
  id: 'MA',
  name: 'e租出租公司',
  identify: '232523652365236520',
  infoClassify: {
    id: 'MA',
    name: '行政许可',
    label: '许可',
    type: 'success',
  },
  infoCategory: {
    id: 'MA',
    name: '基础信息',
    label: '基础',
    type: 'primary',
  },
  expirationDateFormat: '2021-12-12',
  subjectCategory: [
    {
      id: 'MA',
      name: '自然人',
    },
    {
      id: 'MQ',
      name: '个体工商户',
    },
  ],
  sourceUnit: {
    id: 'MA',
    name: 'XX市税务局',
  },
  dimension: {
    id: 'MA',
    name: '社会公开',
  },
  itemsData: [
    {
      name: '单位名称',
      value: '中研社食品有限责任公司',
    },
    {
      name: '统一社会信用代码',
      value: '91120104MA06FBJY36',
    },
    {
      name: '项目代码',
      value: '〔2017〕24号',
    },
    {
      name: '项目名称',
      value: '粮食项目',
    },
    {
      name: '事项名称',
      value: '投资项目',
    },
    {
      name: '办理部门',
      value: '发展和改革委员会',
    },
    {
      name: '办理时间',
      value: '2019-01-01',
    },
    {
      name: '主体类别',
      value: '法人和其他组织',
    },
    {
      name: '信息类别',
      value: '基础信息',
    },
    {
      name: '数据共享范围',
      value: '政务共享',
    },
    {
      name: '有效期限',
      value: '长期',
    },
    {
      name: '交换频率',
      value: '实时',
    },
    {
      name: '公开属性',
      value: '不公开',
    },
    {
      name: '对应权利类别',
      value: '无',
    },
  ],
  status: {
    id: 'MQ',
    name: '正常',
    type: 'success',
  },
  updateTime: 1576317765,
  updateTimeFormat: '2021年06月24日 15时32分',
}

export const taskList = [
  {
    id: 'MA',
    crew: {
      id: 'MA',
      name: '张丹丹',
    },
    total: 1200,
    successesNumber: 1100,
    failuresNumber: 100,
    status: {
      id: 'MQ',
      name: '成功',
      type: 'success',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'MQ',
    crew: {
      id: 'MQ',
      name: '刘华',
    },
    total: 1200,
    successesNumber: 1100,
    failuresNumber: 100,
    status: {
      id: 'MQ',
      name: '成功',
      type: 'success',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
  {
    id: 'Mg',
    crew: {
      id: 'Mg',
      name: '王瑞',
    },
    total: 1200,
    successesNumber: 1100,
    failuresNumber: 100,
    status: {
      id: 'LC0',
      name: '失败',
      type: 'danger',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
    createTimeFormat: '2021年06月24日 15时32分',
  },
]

export const fillInOnline = [{ englishName: 'name', name: '企业名称', validationRules: { isReadOnly: '0', isNecessary: '0', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'entryNumber', name: '项目编号', validationRules: { isReadOnly: '-2', isNecessary: '-2', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'entryName', name: '项目名称', validationRules: { isReadOnly: '-2', isNecessary: '-2', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'entryContent', name: '项目内容及规模', validationRules: { isReadOnly: '-2', isNecessary: '0', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'unifiedSocialCreditCode', name: '统一社会信用代码', validationRules: { isReadOnly: '0', isNecessary: '0', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'principal', name: '法人代表', validationRules: { isReadOnly: '-2', isNecessary: '-2', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'contactPhone', name: '联系方式', validationRules: { isReadOnly: '-2', isNecessary: '-2', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'funding', name: '下达资金（万元）', validationRules: { isReadOnly: '-2', isNecessary: '-2', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'grantDate', name: '发放日期', validationRules: { isReadOnly: '-2', isNecessary: '0', rule: { code: '1', name: '文本', options: [] }, remark: '年月日' } }, { englishName: 'grantUnit', name: '发放机关', validationRules: { isReadOnly: '-2', isNecessary: '0', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'subjectCategory', name: '主体类别', validationRules: { isReadOnly: '0', isNecessary: '0', rule: { code: '3', name: '单选', options: ['法人和其他组织', '自然人'] }, remark: '' } }, { englishName: 'infoCategory', name: '信息类别', validationRules: { isReadOnly: '0', isNecessary: '0', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'oldDimension', name: '数据共享范围', validationRules: { isReadOnly: '0', isNecessary: '0', rule: { code: '3', name: '单选', options: ['政务共享', '有限共享', '不共享'] }, remark: '' } }, { englishName: 'expirationDate', name: '有效期限', validationRules: { isReadOnly: '0', isNecessary: '0', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'exchangeFrequency', name: '交换频率', validationRules: { isReadOnly: '0', isNecessary: '0', rule: { code: '1', name: '文本', options: [] }, remark: '' } }, { englishName: 'dimension', name: '公开属性', validationRules: { isReadOnly: '0', isNecessary: '0', rule: { code: '3', name: '单选', options: ['社会公开', '依申请公开', '不公开'] }, remark: '' } }, { englishName: 'correspondRightsCategory', name: '对应权利类别', validationRules: { isReadOnly: '0', isNecessary: '0', rule: { code: '1', name: '文本', options: [] }, remark: '' } }]

export const errorDataList = {
  list: [
    {
      createTime: 1627628634,
      createTimeFormat: '2021年07月30日 15时03分',
      id: 'MTA',
      name: '市旅游发展有限公司',
      identify: '914******7Q',
      updateTime: 1627628634,
      errorReason: '备注数据格式不正确',
      updateTimeFormat: '2021年07月30日 15时03分',
    },
  ],
  taskData: {
    createTime: 1627628634,
    createTimeFormat: '2021年07月30日 15时03分',
    crew: {
      id: 'MA',
      name: '张科',
    },
    failureNumber: 0,
    id: 'MTA',
    status: {
      id: 'MQ',
      name: '成功',
      type: 'success',
    },
    successNumber: 1,
    total: 1,
    updateTime: 1627628634,
    fileName: '',
    updateTimeFormat: '2021年07月30日 15时03分',
  },
}

export const errorDataDetail = {
  id: 'MA',
  category: 10,
  createTime: 1629888824,
  createTimeFormat: '2021\u5e7408\u670825\u65e5 18\u65f653\u5206',
  updateTime: 1629888824,
  updateTimeFormat: '2021\u5e7408\u670825\u65e5 18\u65f653\u5206',
  statusTime: 0,
  status: {
    id: 'MQ',
    name: '\u6b63\u5e38',
    type: 'success',
  },
  name: '\u56db\u5ddd\u745e\u715c\u5efa\u7b51\u6709\u9650\u516c\u53f8\u4fe1\u9633\u5206\u516c\u53f8',
  identify: '91411500MA9F86JG96',
  itemsData: [{
    name: '\u884c\u653f\u76f8\u5bf9\u4eba\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '\u4fe1\u7528\u4e3b\u4f53\u7684\u6cd5\u5b9a\u540d\u79f0',
    identify: 'ZTMC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u56db\u5ddd\u745e\u715c\u5efa\u7b51\u6709\u9650\u516c\u53f8\u4fe1\u9633\u5206\u516c\u53f8',
    errorReason: '',
  }, {
    name: '\u884c\u653f\u76f8\u5bf9\u4eba\u7c7b\u522b',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '16',
    options: [],
    remarks: '',
    identify: 'XK_XDR_LB',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u6cd5\u4eba\u53ca\u975e\u6cd5\u4eba\u7ec4\u7ec7',
    errorReason: '',
  }, {
    name: '\u884c\u653f\u76f8\u5bf9\u4eba\u4ee3\u7801_1(\u7edf\u4e00\u793e\u4f1a\u4fe1\u7528\u4ee3\u7801)',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'TYSHXYDM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '91411500MA9F86JG96',
    errorReason: '',
  }, {
    name: '\u884c\u653f\u76f8\u5bf9\u4eba\u4ee3\u7801_2 (\u5de5\u5546\u6ce8\u518c\u53f7)',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '50',
    options: [],
    remarks: '',
    identify: 'XK_XDR_GSZC',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '',
    errorReason: '\u884c\u653f\u76f8\u5bf9\u4eba\u4ee3\u7801_2 (\u5de5\u5546\u6ce8\u518c\u53f7)\u6570\u636e\u683c\u5f0f\u9519\u8bef',
  }, {
    name: '\u884c\u653f\u76f8\u5bf9\u4eba\u4ee3\u7801_3(\u7ec4\u7ec7\u673a\u6784\u4ee3\u7801)',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '9',
    options: [],
    remarks: '',
    identify: 'XK_XDR_ZZJG',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '91292',
    errorReason: '',
  }, {
    name: '\u884c\u653f\u76f8\u5bf9\u4eba\u4ee3\u7801_4(\u7a0e\u52a1\u767b\u8bb0\u53f7)',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '15',
    options: [],
    remarks: '',
    identify: 'XK_XDR_SWDJ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '',
    errorReason: '\u884c\u653f\u76f8\u5bf9\u4eba\u4ee3\u7801_4(\u7a0e\u52a1\u767b\u8bb0\u53f7)\u6570\u636e\u683c\u5f0f\u9519\u8bef',
  }, {
    name: '\u884c\u653f\u76f8\u5bf9\u4eba\u4ee3\u7801_5(\u4e8b\u4e1a\u5355\u4f4d\u8bc1\u4e66\u53f7)',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '12',
    options: [],
    remarks: '',
    identify: 'XK_XDR_SYDW',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '',
    errorReason: '\u884c\u653f\u76f8\u5bf9\u4eba\u4ee3\u7801_5(\u4e8b\u4e1a\u5355\u4f4d\u8bc1\u4e66\u53f7)\u6570\u636e\u683c\u5f0f\u9519\u8bef',
  }, {
    name: '\u884c\u653f\u76f8\u5bf9\u4eba\u4ee3\u7801_6(\u793e\u4f1a\u7ec4\u7ec7\u767b\u8bb0\u8bc1\u53f7)',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '50',
    options: [],
    remarks: '',
    identify: 'XK_XDR_SHZZ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: 'MA9F86JG-9',
    errorReason: '',
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '50',
    options: [],
    remarks: '',
    identify: 'XK_FRDB',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u738b\u707f\u56fd',
    errorReason: '',
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u7c7b\u578b',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '64',
    options: [],
    remarks: '',
    identify: 'XK_FR_ZJLX',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '',
    errorReason: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u7c7b\u578b\u6570\u636e\u683c\u5f0f\u9519\u8bef',
  }, {
    name: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u53f7\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '64',
    options: [],
    remarks: '',
    identify: 'XK_FR_ZJHM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '',
    errorReason: '\u6cd5\u5b9a\u4ee3\u8868\u4eba\u8bc1\u4ef6\u53f7\u7801\u6570\u636e\u683c\u5f0f\u9519\u8bef',
  }, {
    name: '\u8bc1\u4ef6\u7c7b\u578b',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '64',
    options: [],
    remarks: '',
    identify: 'XK_XDR_ZJLX',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '',
    errorReason: '\u8bc1\u4ef6\u7c7b\u578b\u6570\u636e\u683c\u5f0f\u9519\u8bef',
  }, {
    name: '\u8bc1\u4ef6\u53f7\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '64',
    options: [],
    remarks: '',
    identify: 'XK_XDR_ZJHM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '',
    errorReason: '\u8bc1\u4ef6\u53f7\u7801\u6570\u636e\u683c\u5f0f\u9519\u8bef',
  }, {
    name: '\u884c\u653f\u8bb8\u53ef\u51b3\u5b9a\u6587\u4e66\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '64',
    options: [],
    remarks: '',
    identify: 'XK_XKWS',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u4f01\u4e1a\u6ce8\u518c\u767b\u8bb0',
    errorReason: '',
  }, {
    name: '\u884c\u653f\u8bb8\u53ef\u51b3\u5b9a\u6587\u4e66\u53f7',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '64',
    options: [],
    remarks: '',
    identify: 'XK_WSH',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '411596000077514',
    errorReason: '',
  }, {
    name: '\u8bb8\u53ef\u7c7b\u522b',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '256',
    options: [],
    remarks: '',
    identify: 'XK_XKLB',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u767b\u8bb0',
    errorReason: '',
  }, {
    name: '\u8bb8\u53ef\u8bc1\u4e66\u540d\u79f0',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '64',
    options: [],
    remarks: '',
    identify: 'XK_XKZS',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u4f01\u4e1a\u6ce8\u518c\u767b\u8bb0',
    errorReason: '',
  }, {
    name: '\u8bb8\u53ef\u7f16\u53f7',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '64',
    options: [],
    remarks: '',
    identify: 'XK_XKBH',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '410000017210705A000092',
    errorReason: '',
  }, {
    name: '\u8bb8\u53ef\u5185\u5bb9',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '4000',
    options: [],
    remarks: '',
    identify: 'XK_NR',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u4f01\u4e1a\u6ce8\u518c\u767b\u8bb0',
    errorReason: '',
  }, {
    name: '\u8bb8\u53ef\u51b3\u5b9a\u65e5\u671f',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '8',
    options: [],
    remarks: '',
    identify: 'XK_JDRQ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '20210715',
    errorReason: '',
  }, {
    name: '\u6709\u6548\u671f\u81ea',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '8',
    options: [],
    remarks: '',
    identify: 'XK_YXQZ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '2020-06-04',
    errorReason: '\u6709\u6548\u671f\u81ea\u6570\u636e\u683c\u5f0f\u9519\u8bef',
  }, {
    name: '\u6709\u6548\u671f\u81f3',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '8',
    options: [],
    remarks: '',
    identify: 'XK_YXQZI',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '73050',
    errorReason: '\u6709\u6548\u671f\u81f3\u6570\u636e\u683c\u5f0f\u9519\u8bef',
  }, {
    name: '\u8bb8\u53ef\u673a\u5173',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'XK_XKJG',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u4fe1\u9633\u5e02\u5e02\u573a\u76d1\u7763\u7ba1\u7406\u5c40\u4e13\u4e1a\u5206\u5c40',
    errorReason: '',
  }, {
    name: '\u8bb8\u53ef\u673a\u5173\u7edf\u4e00\u793e\u4f1a\u4fe1\u7528\u4ee3\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'XK_XKJGDM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '11411500747429737F',
    errorReason: '',
  }, {
    name: '\u5f53\u524d\u72b6\u6001',
    type: {
      id: 'Mg',
      name: '\u6574\u6570\u578b',
    },
    length: '1',
    options: [],
    remarks: '',
    identify: 'XK_ZT',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '1',
    errorReason: '',
  }, {
    name: '\u6570\u636e\u6765\u6e90\u5355\u4f4d',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '200',
    options: [],
    remarks: '',
    identify: 'XK_LYDW',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u4fe1\u9633\u5e02\u53d1\u5c55\u548c\u6539\u9769\u59d4\u5458\u4f1a1',
    errorReason: '',
  }, {
    name: '\u6570\u636e\u6765\u6e90\u5355\u4f4d\u7edf\u4e00\u793e\u4f1a\u4fe1\u7528\u4ee3\u7801',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '18',
    options: [],
    remarks: '',
    identify: 'XK_LYDWDM',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '11411500006067580U',
    errorReason: '',
  }, {
    name: '\u5907\u6ce8',
    type: {
      id: 'MA',
      name: '\u5b57\u7b26\u578b',
    },
    length: '4000',
    options: [],
    remarks: '',
    identify: 'BZ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u6cb3\u5357\u7701\u9ad8\u901f\u516c\u8def\u8054\u7f51\u7ba1\u7406\u4e2d\u5fc3',
    errorReason: '',
  }, {
    name: '\u8fc7\u671f\u65f6\u95f4',
    type: {
      id: 'MQ',
      name: '\u65e5\u671f\u578b',
    },
    length: '8',
    options: [],
    remarks: '',
    identify: 'GQSJ',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MA',
      name: '\u793e\u4f1a\u516c\u5f00',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '20210816',
    errorReason: '',
  }, {
    name: '\u4e3b\u4f53\u7c7b\u522b',
    type: {
      id: 'NQ',
      name: '\u96c6\u5408\u578b',
    },
    length: '50',
    options: ['\u6cd5\u4eba\u53ca\u975e\u6cd5\u4eba\u7ec4\u7ec7', '\u4e2a\u4f53\u5de5\u5546\u6237'],
    remarks: '\u6cd5\u4eba\u53ca\u975e\u6cd5\u4eba\u7ec4\u7ec7;\u81ea\u7136\u4eba;\u4e2a\u4f53\u5de5\u5546\u6237\uff0c\u652f\u6301\u591a\u9009',
    identify: 'ZTLB',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MQ',
      name: '\u653f\u52a1\u5171\u4eab',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u6cd5\u4eba\u53ca\u975e\u6cd5\u4eba\u7ec4\u7ec7',
    errorReason: '',
  }, {
    name: '\u516c\u5f00\u8303\u56f4',
    type: {
      id: 'NA',
      name: '\u679a\u4e3e\u578b',
    },
    length: '20',
    options: ['\u793e\u4f1a\u516c\u5f00'],
    remarks: '\u652f\u6301\u5355\u9009',
    identify: 'GKFW',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MQ',
      name: '\u653f\u52a1\u5171\u4eab',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u793e\u4f1a\u516c\u5f00',
    errorReason: '',
  }, {
    name: '\u66f4\u65b0\u9891\u7387',
    type: {
      id: 'NA',
      name: '\u679a\u4e3e\u578b',
    },
    length: '20',
    options: ['\u5b9e\u65f6'],
    remarks: '\u652f\u6301\u5355\u9009',
    identify: 'GXPL',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MQ',
      name: '\u653f\u52a1\u5171\u4eab',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u5b9e\u65f6',
    errorReason: '',
  }, {
    name: '\u4fe1\u606f\u5206\u7c7b',
    type: {
      id: 'NA',
      name: '\u679a\u4e3e\u578b',
    },
    length: '50',
    options: ['\u884c\u653f\u8bb8\u53ef'],
    remarks: '\u652f\u6301\u5355\u9009',
    identify: 'XXFL',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MQ',
      name: '\u653f\u52a1\u5171\u4eab',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u884c\u653f\u8bb8\u53ef',
    errorReason: '',
  }, {
    name: '\u4fe1\u606f\u7c7b\u522b',
    type: {
      id: 'NA',
      name: '\u679a\u4e3e\u578b',
    },
    length: '50',
    options: ['\u57fa\u7840\u4fe1\u606f'],
    remarks: '\u4fe1\u606f\u6027\u8d28\u7c7b\u578b\uff0c\u652f\u6301\u5355\u9009',
    identify: 'XXLB',
    isMasked: {
      id: 'Lw',
      name: '\u5426',
    },
    maskRule: [],
    dimension: {
      id: 'MQ',
      name: '\u653f\u52a1\u5171\u4eab',
    },
    isNecessary: {
      id: 'MA',
      name: '\u662f',
    },
    value: '\u57fa\u7840\u4fe1\u606f',
    errorReason: '',
  }],
}
