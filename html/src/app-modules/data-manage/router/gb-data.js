/**
 * @file 数据管理子系统 - 国标数据管理
 * @module router/gb-data.js
 */
import permissions from '@/constants/permissions'

export default [
  // 目录列表
  {
    path: '/data-manage/gb-data',
    name: 'GbData',
    meta: {
      title: 'dataManage.ROUTER.GB_DATA',
      icon: 'gb-data-manage',
      routerId: permissions.GB_DATA_MANAGE_ID,
    },
    component: () => import('@/app-modules/data-manage/views/list/gb-templates'),
  },
  // 数据列表
  {
    path: '/data-manage/gb-data/template-detail/:templateId',
    name: 'GbDataList',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.GB_TEMPLATE_DETAIL',
      icon: 'gb-data-manage',
      activeMenu: '/data-manage/gb-data',
    },
    component: () => import('@/app-modules/data-manage/views/list/gb-data'),
  },
  // 数据详情
  {
    path: '/data-manage/gb-data/data-detail/:id',
    name: 'GbDataDetail',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.GB_DATA_DETAIL',
      icon: 'gb-data-manage',
      activeMenu: '/data-manage/gb-data',
    },
    component: () => import('@/app-modules/data-manage/views/detail/gb-data'),
  },
  // 任务总览
  {
    path: '/data-manage/gb-data/task-flow-graph/:id',
    name: 'GbTaskFlowGraph',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.TASK_FLOW_GRAPH',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/gb-data',
    },
    component: () => import('@/app-modules/data-manage/views/detail/task-flow-graph'),
  },
  // 错误数据列表
  {
    path: '/data-manage/gb-data/gb-error-data-list/:taskId/:templateId',
    name: 'GbErrorDataList',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.GB_ERROR_DATA_LIST',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/gb-data',
    },
    component: () => import('@/app-modules/data-manage/views/list/gb-error-data'),
  },
  // 错误数据详情
  {
    path: '/data-manage/gb-data/gb-error-data-detail/:id/:taskId/:templateId',
    name: 'GbErrorDataDetail',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.GB_ERROR_DATA_DETAIL',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/gb-data',
    },
    component: () => import('@/app-modules/data-manage/views/detail/gb-error-data'),
  },
  // 错误数据在线编辑
  {
    path: '/data-manage/gb-data/gb-error-data-edit/:id/:templateId',
    name: 'GbErrorDataEdit',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.GB_ERROR_DATA_EDIT',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/gb-data',
    },
    component: () => import('@/app-modules/data-manage/views/form/gb-error-edit'),
  },
]
