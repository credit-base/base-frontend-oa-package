/**
 * @file 数据管理子系统 - 本级数据管理
 * @module router/bj-data.js
 */
import permissions from '@/constants/permissions'

export default [
  // 目录列表
  {
    path: '/data-manage/bj-data',
    name: 'BjData',
    meta: {
      title: 'dataManage.ROUTER.BJ_DATA',
      icon: 'gb-data-manage',
      routerId: permissions.BJ_DATA_MANAGEMENT_ID,
    },
    component: () => import('@/app-modules/data-manage/views/list/bj-templates'),
  },
  // 数据列表
  {
    path: '/data-manage/bj-data/template-detail/:templateId',
    name: 'BjDataList',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.BJ_TEMPLATE_DETAIL',
      icon: 'gb-data-manage',
      activeMenu: '/data-manage/bj-data',
    },
    component: () => import('@/app-modules/data-manage/views/list/bj-data'),
  },
  // 数据详情
  {
    path: '/data-manage/bj-data/data-detail/:id',
    name: 'BjDataDetail',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.BJ_DATA_DETAIL',
      icon: 'gb-data-manage',
      activeMenu: '/data-manage/bj-data',
    },
    component: () => import('@/app-modules/data-manage/views/detail/bj-data'),
  },
  // 任务总览
  {
    path: '/data-manage/bj-data/task-flow-graph/:id',
    name: 'BjTaskFlowGraph',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.TASK_FLOW_GRAPH',
      icon: 'gb-data-manage',
      activeMenu: '/data-manage/bj-data',
    },
    component: () => import('@/app-modules/data-manage/views/detail/task-flow-graph'),
  },
  // 错误数据列表
  {
    path: '/data-manage/bj-data/bj-error-data-list/:taskId/:templateId',
    name: 'BjErrorDataList',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.BJ_ERROR_DATA_LIST',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/bj-data',
    },
    component: () => import('@/app-modules/data-manage/views/list/bj-error-data'),
  },
  // 错误数据详情
  {
    path: '/data-manage/bj-data/bj-error-data-detail/:id/:taskId/:templateId',
    name: 'BjErrorDataDetail',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.BJ_ERROR_DATA_DETAIL',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/bj-data',
    },
    component: () => import('@/app-modules/data-manage/views/detail/bj-error-data'),
  },
  // 错误数据在线编辑
  {
    path: '/data-manage/bj-data/bj-error-data-edit/:id/:templateId',
    name: 'BjErrorDataEdit',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.BJ_ERROR_DATA_EDIT',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/bj-data',
    },
    component: () => import('@/app-modules/data-manage/views/form/bj-error-edit'),
  },
]
