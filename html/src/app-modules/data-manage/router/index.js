/**
 * @file 数据管理 - 路由
 * @module async-routes/data-manage
 */

import gbData from './gb-data'
import bjData from './bj-data'
import wbjData from './wbj-data'

import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const { STATUS_DATA_MANAGE_SUBSYSTEM } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/data-manage-subsystem',
    name: 'DataManageSubsystem',
    component: () => import('@/app-modules/data-manage/views'),
    alwaysShow: true,
    meta: {
      title: 'dataManage.ROUTER.DATA_MANAGE_SUBSYSTEM',
      // TODO: change icon
      icon: 'data-manage-subsystem',
      breadcrumb: false,
      isVisible: STATUS_DATA_MANAGE_SUBSYSTEM,
    },
    children: [
      ...gbData,
      ...bjData,
      ...wbjData,
    ],
  },
]
