/**
 * @file 数据管理子系统 - 委办局数据管理
 * @module router/wbj-data.js
 */
import permissions from '@/constants/permissions'

export default [
  // 目录列表
  {
    path: '/data-manage/wbj-data',
    name: 'WbjData',
    meta: {
      title: 'dataManage.ROUTER.WBJ_DATA',
      icon: 'gb-data-manage',
      routerId: permissions.WBJ_DATA_MANAGE_ID,
    },
    component: () => import('@/app-modules/data-manage/views/list/wbj-templates'),
  },
  // 数据列表
  {
    path: '/data-manage/wbj-data/template-detail/:templateId',
    name: 'WbjDataList',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.WBJ_TEMPLATE_DETAIL',
      icon: 'gb-data-manage',
      activeMenu: '/data-manage/wbj-data',
    },
    component: () => import('@/app-modules/data-manage/views/list/wbj-data'),
  },
  // 数据详情
  {
    path: '/data-manage/wbj-data/data-detail/:id',
    name: 'WbjDataDetail',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.WBJ_DATA_DETAIL',
      icon: 'gb-data-manage',
      activeMenu: '/data-manage/wbj-data',
    },
    component: () => import('@/app-modules/data-manage/views/detail/wbj-data'),
  },
  // 任务总览
  {
    path: '/data-manage/wbj-data/task-flow-graph/:id',
    name: 'WbjTaskFlowGraph',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.TASK_FLOW_GRAPH',
      icon: 'gb-data-manage',
      activeMenu: '/data-manage/wbj-data',
    },
    component: () => import('@/app-modules/data-manage/views/detail/task-flow-graph'),
  },
  // 在线填报
  {
    path: '/data-manage/wbj-data/fill-in-online/:id',
    name: 'WbjFillInOnline',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.FILL_IN_ONLINE',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/wbj-data',
    },
    component: () => import('@/app-modules/data-manage/views/form/fill-in-online'),
  },
  // 前置机导入
  {
    path: '/data-manage/wbj-data/front-end-processor/:id',
    name: 'WbjFrontEndProcessor',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.FRONT_END_PROCESSOR',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/wbj-data',
    },
    component: () => import('@/app-modules/data-manage/views/form/front-end-processor'),
  },
  // 错误数据列表
  {
    path: '/data-manage/wbj-data/wbj-error-data-list/:taskId/:templateId',
    name: 'WbjErrorDataList',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.WBJ_ERROR_DATA_LIST',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/wbj-data',
    },
    component: () => import('@/app-modules/data-manage/views/list/wbj-error-data'),
  },
  // 错误数据详情
  {
    path: '/data-manage/wbj-data/wbj-error-data-detail/:id/:taskId/:templateId',
    name: 'WbjErrorDataDetail',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.WBJ_ERROR_DATA_DETAIL',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/wbj-data',
    },
    component: () => import('@/app-modules/data-manage/views/detail/wbj-error-data'),
  },
  // 错误数据在线编辑
  {
    path: '/data-manage/wbj-data/wbj-error-data-edit/:id/:templateId',
    name: 'WbjErrorDataEdit',
    hidden: true,
    meta: {
      title: 'dataManage.ROUTER.WBJ_ERROR_DATA_EDIT',
      icon: 'raw-data-manage',
      activeMenu: '/data-manage/wbj-data',
    },
    component: () => import('@/app-modules/data-manage/views/form/wbj-error-edit'),
  },
]
