/**
 * @file 新闻 - 路由
 * @module async-routes/news
 */

import Layout from '@/layout'
import permissions from '@/constants/permissions'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const { STATUS_NEWS } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/news',
    name: 'News',
    component: Layout,
    alwaysShow: true,
    meta: {
      title: 'news.ROUTER.NEWS',
      icon: 'news',
      breadcrumb: false,
      isVisible: STATUS_NEWS,
    },
    children: [
      {
        path: '/news/list',
        name: 'NewsList',
        component: () => import('@/app-modules/news/views/list/official-list'),
        meta: {
          title: 'news.ROUTER.NEWS_LIST',
          icon: 'news',
          routerId: permissions.NEWS_ID,
        },
      },
      {
        path: '/news/add',
        name: 'NewsAdd',
        hidden: true,
        component: () => import('@/app-modules/news/views/add'),
        meta: {
          title: 'news.ROUTER.NEWS_ADD',
          icon: 'news',
          activeMenu: '/news/list',
        },
      },
      {
        path: '/news/edit/:id',
        name: 'NewsEdit',
        hidden: true,
        component: () => import('@/app-modules/news/views/edit'),
        meta: {
          title: 'news.ROUTER.NEWS_EDIT',
          icon: 'news',
          activeMenu: '/news/list',
        },
      },
      {
        path: '/news/detail/:id',
        name: 'NewsDetail',
        hidden: true,
        component: () => import('@/app-modules/news/views/detail/official-detail'),
        meta: {
          title: 'news.ROUTER.NEWS_DETAIL',
          icon: 'news',
          activeMenu: '/news/list',
        },
      },
      {
        path: '/news/audit-detail/:id',
        name: 'NewsAuditDetail',
        hidden: true,
        component: () => import('@/app-modules/news/views/detail/audit-detail'),
        meta: {
          title: 'news.ROUTER.NEWS_UN_AUDITED_DETAIL',
          icon: 'news',
          activeMenu: '/audit-news/list',
        },
      },
      {
        path: '/un-audited-news/list',
        name: 'AuditNewsList',
        component: () => import('@/app-modules/news/views/list/audit-list'),
        meta: {
          title: 'news.ROUTER.AUDIT_NEWS_PURVIEW',
          icon: 'news',
          routerId: permissions.UN_AUDITED_NEWS_ID,
        },
      },
      {
        path: '/unaudited-news/detail/:id',
        name: 'UnAuditedNewsDetail',
        hidden: true,
        component: () => import('@/app-modules/news/views/detail/official-detail'),
        meta: {
          title: 'news.ROUTER.NEWS_DETAIL',
          icon: 'news',
          activeMenu: '/un-audited-news/list',
        },
      },
      {
        path: '/unaudited-news/audit-detail/:id',
        name: 'UnAuditedNewsAuditDetail',
        hidden: true,
        component: () => import('@/app-modules/news/views/detail/audit-detail'),
        meta: {
          title: 'news.ROUTER.NEWS_UN_AUDITED_DETAIL',
          icon: 'news',
          activeMenu: '/un-audited-news/list',
        },
      },
    ],
  },
]
