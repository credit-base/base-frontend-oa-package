export const newsList = [
  {
    id: 'MA',
    title: 'culpa',
    parentCategory: {
      id: 'MA',
      name: '政策法规',
    },
    category: {
      id: 'MA',
      name: '政策法规',
    },
    newsType:
    {
      id: 'MA',
      name: '政策法规',
    },
    source: '委办局',
    createTime: 950321580,
    updateTime: 258186577,
    updateTimeFormat: '2021年5月12日 17时55分',
    statusTime: 68691915,
    status: {
      id: 'LC0',
      name: '禁用',
      type: 'warning',
    },
    stick: {
      id: 'Lw',
      name: '未置顶',
      type: 'info',
    },
    homePageShowStatus: {
      id: 'MQ',
      name: '首页展示',
      type: 'success',
    },
    bannerStatus: {
      id: 'MQ',
      name: '轮播',
      type: 'success',
    },
    publishUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'MQ',
    title: 'culpa',
    parentCategory: {
      id: 'MA',
      name: '政策法规',
    },
    category: {
      id: 'MA',
      name: '政策法规',
    },
    newsType:
    {
      id: 'MA',
      name: '政策法规',
    },
    source: '委办局',
    createTime: 950321580,
    updateTime: 258186577,
    updateTimeFormat: '2021年5月12日 17时55分',
    statusTime: 68691915,
    status: {
      id: 'Lw',
      name: '启用',
      type: 'success',
    },
    stick: {
      id: 'Lw',
      name: '未置顶',
      type: 'success',
    },
    homePageShowStatus: {
      id: 'MQ',
      name: '已展示',
      type: 'success',
    },
    bannerStatus: {
      id: 'Lw',
      name: '未轮播',
      type: 'info',
    },
    publishUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
]
export const translateResList = [
  {
    id: 'MA',
    title: 'culpa',
    newsType: '政策法规',
    updateTimeFormat: '2021年5月12日 17时55分',
    status: {
      id: 'LC0',
      name: '禁用',
      type: 'warning',
    },
    stick: {
      id: 'Lw',
      name: '未置顶',
      type: 'info',
    },
    homePageShowStatus: {
      id: 'MQ',
      name: '首页展示',
      type: 'success',
    },
    bannerStatus: {
      id: 'MQ',
      name: '轮播',
      type: 'success',
    },
    publishUserGroup: '陕西省发展和改革委员会',
  },
  {
    id: 'MQ',
    title: 'culpa',
    newsType: '政策法规',
    updateTimeFormat: '2021年5月12日 17时55分',
    status: {
      id: 'Lw',
      name: '启用',
      type: 'success',
    },
    stick: {
      id: 'Lw',
      name: '未置顶',
      type: 'success',
    },
    homePageShowStatus: {
      id: 'MQ',
      name: '已展示',
      type: 'success',
    },
    bannerStatus: {
      id: 'Lw',
      name: '未轮播',
      type: 'info',
    },
    publishUserGroup: '陕西省发展和改革委员会',
  },
]

export const newsDetail = {
  id: 'MA',
  title: '新闻标题',
  content: '新闻内容',
  newsType: {
    id: 'MA',
    name: '政策法规',
  },
  source: '新闻来源',
  description: '新闻描述',
  attachments: [
    {
      name: '63ba799d1cdc5e0eebfce1b470119303',
      identify: 'a33439f4ffac27b0d1fd34cac6a0a98c',
    },
    {
      name: '958096125d5883c101bdf4c07ac53da4',
      identify: 'e2276f8679410025f4697c24958d57c6',
    },
    {
      name: '44f9937860f737f0070cf2a6e04dcd49',
      identify: '2363c9ee873a79c755fc5477533028d5',
    },
  ],
  cover: {
    name: '63ba799d1cdc5e0eebfce1b470119303',
    identify: 'a33439f4ffac27b0d1fd34cac6a0a98c.jpg',
  },
  bannerImage: {
    name: '63ba799d1cdc5e0eebfce1b470119303',
    identify: 'a33439f4ffac27b0d1fd34cac6a0a98c.jpg',
  },
  dimension: {
    id: 'MA',
    name: '政策法规',
  },
  createTime: 950321580,
  updateTime: 258186577,
  updateTimeFormat: '2021年5月12日 17时55分',
  statusTime: 68691915,
  status: {
    id: 'LC0',
    name: '禁用',
    type: 'warning',
  },
  stick: {
    id: 'Lw',
    name: '未置顶',
    type: 'info',
  },
  homePageShowStatus: {
    id: 'MQ',
    name: '首页展示',
    type: 'success',
  },
  bannerStatus: {
    id: 'MQ',
    name: '轮播',
    type: 'info',
  },
  publishUserGroup: {
    id: 'MA',
    name: '陕西省发展和改革委员会',
  },
  crew: {
    id: 'MA',
    realName: '财金科',
  },
}

export const translateResDetail = {
  id: 'MA',
  title: '新闻标题',
  content: '新闻内容',
  newsType: '政策法规',
  source: '新闻来源',
  attachments: [
    {
      name: '63ba799d1cdc5e0eebfce1b470119303',
      identify: 'a33439f4ffac27b0d1fd34cac6a0a98c',
    },
    {
      name: '958096125d5883c101bdf4c07ac53da4',
      identify: 'e2276f8679410025f4697c24958d57c6',
    },
    {
      name: '44f9937860f737f0070cf2a6e04dcd49',
      identify: '2363c9ee873a79c755fc5477533028d5',
    },
  ],
  cover: {
    name: '63ba799d1cdc5e0eebfce1b470119303',
    identify: 'a33439f4ffac27b0d1fd34cac6a0a98c.jpg',
  },
  bannerImage: {
    name: '63ba799d1cdc5e0eebfce1b470119303',
    identify: 'a33439f4ffac27b0d1fd34cac6a0a98c.jpg',
  },
  dimension: '政策法规',
  updateTimeFormat: '2021年5月12日 17时55分',
  status: {
    id: 'LC0',
    name: '禁用',
    type: 'warning',
  },
  stick: {
    id: 'Lw',
    name: '未置顶',
    type: 'info',
  },
  homePageShowStatus: {
    id: 'MQ',
    name: '首页展示',
    type: 'success',
  },
  bannerStatus: {
    id: 'MQ',
    name: '轮播',
    type: 'info',
  },
  publishUserGroup: '陕西省发展和改革委员会',
  crew: '财金科',
}

export const unAuditedNewsList = [
  {
    id: 'MA',
    applyStatus: {
      id: 'Lw',
      name: '待审核',
      type: 'warning',
    },
    rejectReason: '审核驳回原因',
    applyType: 'Mg',
    operationType: {
      id: 'MA',
      name: '新增',
    },
    title: 'culpa',
    content: 'Porro voluptatem id vero perspiciatis nulla nihil.',
    parentCategory: 'MQ',
    category: 'MQ',
    newsType:
    {
      id: 'MA',
      name: '政策法规',
    },
    source: 'nihil',
    description: 'Porro voluptatem id vero perspiciatis nulla nihil.',
    cover: {
      name: '63ba799d1cdc5e0eebfce1b470119303',
      identify: 'a33439f4ffac27b0d1fd34cac6a0a98c.jpg',
    },
    bannerImage: {
      name: '63ba799d1cdc5e0eebfce1b470119303',
      identify: 'a33439f4ffac27b0d1fd34cac6a0a98c.jpg',
    },
    attachments: [
      {
        name: '63ba799d1cdc5e0eebfce1b470119303',
        identify: 'a33439f4ffac27b0d1fd34cac6a0a98c',
      },
      {
        name: '958096125d5883c101bdf4c07ac53da4',
        identify: 'e2276f8679410025f4697c24958d57c6',
      },
      {
        name: '44f9937860f737f0070cf2a6e04dcd49',
        identify: '2363c9ee873a79c755fc5477533028d5',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政策法规',
    },
    createTime: 950321580,
    updateTime: 258186577,
    updateTimeFormat: '2021年5月12日 17时55分',
    statusTime: 68691915,
    status: {
      id: 'LC0',
      name: '禁用',
      type: 'warning',
    },
    stick: {
      id: 'Lw',
      name: '未置顶',
      type: 'info',
    },
    homePageShowStatus: {
      id: 'MQ',
      name: '首页展示',
      type: 'success',
    },
    bannerStatus: {
      id: 'MQ',
      name: '轮播',
      type: 'info',
    },
    applyUserGroup: { // 发布委办局
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
    publishUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
    applyCrew: {
      id: 'Mg',
      realName: '委办局管理员',
    },
    crew: {
      id: 'MA',
      realName: '财金科',
    },
  },
  {
    id: 'MQ',
    applyStatus: {
      id: 'LC0',
      name: '已驳回',
      type: 'danger',
    },
    rejectReason: '审核驳回原因',
    applyType: 'Mg',
    operationType: {
      id: 'MA',
      name: '新增',
    },
    title: 'culpa',
    content: 'Porro voluptatem id vero perspiciatis nulla nihil.',
    parentCategory: 'MQ',
    category: 'MQ',
    newsType:
    {
      id: 'MA',
      name: '政策法规',
    },
    source: 'nihil',
    description: 'Porro voluptatem id vero perspiciatis nulla nihil.',
    cover: {
      name: '63ba799d1cdc5e0eebfce1b470119303',
      identify: 'a33439f4ffac27b0d1fd34cac6a0a98c.jpg',
    },
    bannerImage: {
      name: '63ba799d1cdc5e0eebfce1b470119303',
      identify: 'a33439f4ffac27b0d1fd34cac6a0a98c.jpg',
    },
    attachments: [
      {
        name: '63ba799d1cdc5e0eebfce1b470119303',
        identify: 'a33439f4ffac27b0d1fd34cac6a0a98c',
      },
      {
        name: '958096125d5883c101bdf4c07ac53da4',
        identify: 'e2276f8679410025f4697c24958d57c6',
      },
      {
        name: '44f9937860f737f0070cf2a6e04dcd49',
        identify: '2363c9ee873a79c755fc5477533028d5',
      },
    ],
    dimension: {
      id: 'MA',
      name: '政策法规',
    },
    createTime: 950321580,
    updateTime: 258186577,
    updateTimeFormat: '2021年5月12日 17时55分',
    statusTime: 68691915,
    status: {
      id: 'LC0',
      name: '禁用',
      type: 'warning',
    },
    stick: {
      id: 'Lw',
      name: '未置顶',
      type: 'info',
    },
    homePageShowStatus: {
      id: 'MQ',
      name: '首页展示',
      type: 'success',
    },
    bannerStatus: {
      id: 'MQ',
      name: '轮播',
      type: 'info',
    },
    applyUserGroup: { // 发布委办局
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
    publishUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
    applyCrew: {
      id: 'Mg',
      realName: '委办局管理员',
    },
    crew: {
      id: 'MA',
      realName: '财金科',
    },
  },
]

export const translateResUnAuditedNewsList = [
  {
    id: 'MA',
    title: 'culpa',
    newsType: '政策法规',
    operationType: '新增',
    publishUserGroup: '陕西省发展和改革委员会',
    applyStatus: {
      id: 'Lw',
      name: '待审核',
      type: 'warning',
    },
    updateTimeFormat: '2021年5月12日 17时55分',
  },
  {
    id: 'MQ',
    applyStatus: {
      id: 'LC0',
      name: '已驳回',
      type: 'danger',
    },
    title: 'culpa',
    newsType: '政策法规',
    operationType: '新增',
    publishUserGroup: '陕西省发展和改革委员会',
    updateTimeFormat: '2021年5月12日 17时55分',
  },
]

export const unAuditedNewsDetail = {
  id: 'MA',
  applyStatus: {
    id: 'Lw',
    name: '待审核',
    type: 'warning',
  },
  rejectReason: '审核驳回原因',
  title: 'culpa',
  content: 'Porro voluptatem id vero perspiciatis nulla nihil.',
  parentCategory: {
    id: 'MA',
    name: '社会公开',
  },
  category: {
    id: 'MA',
    name: '社会公开',
  },
  newsType: {
    id: 'MA',
    name: '社会公开',
  },
  source: '新闻来源',
  description: 'Porro voluptatem id vero perspiciatis nulla nihil.',
  attachments: [
    {
      name: '63ba799d1cdc5e0eebfce1b470119303',
      identify: 'a33439f4ffac27b0d1fd34cac6a0a98c',
    },
    {
      name: '958096125d5883c101bdf4c07ac53da4',
      identify: 'e2276f8679410025f4697c24958d57c6',
    },
    {
      name: '44f9937860f737f0070cf2a6e04dcd49',
      identify: '2363c9ee873a79c755fc5477533028d5',
    },
  ],
  cover: {
    name: 'name',
    identify: 'identify',
  },
  bannerImage: {
    name: 'name',
    identify: 'identify',
  },
  dimension: {
    id: 'MA',
    name: '社会公开',
  },
  createTime: 950321580,
  createTimeFormat: '2020年08月21日 21点07分',
  updateTime: 258186577,
  updateTimeFormat: '2020年08月21日 21点07分',
  statusTime: 68691915,
  statusTimeFormat: '2020年08月21日 21点07分',
  status: {
    id: 'LC0',
    name: '禁用',
    type: 'warning',
  },
  stick: {
    id: 'Lw',
    name: '未置顶',
    type: 'info',
  },
  homePageShowStatus: {
    id: 'MQ',
    name: '首页展示',
    type: 'success',
  },
  bannerStatus: {
    id: 'MQ',
    name: '轮播',
    type: 'info',
  },
  applyUserGroup: {
    id: 'MA',
    name: '陕西省发展和改革委员会',
  },
  publishUserGroup: {
    id: 'MA',
    name: '陕西省发展和改革委员会',
  },
  applyCrew: {
    id: 'Mg',
    realName: '委办局管理员',
  },
  crew: {
    id: 'MA',
    realName: '财金科',
  },
}

export const translateResUnAuditedNewsDetail = {
  id: 'MA',
  applyStatus: {
    id: 'Lw',
    name: '待审核',
    type: 'warning',
  },
  rejectReason: '审核驳回原因',
  title: 'culpa',
  content: 'Porro voluptatem id vero perspiciatis nulla nihil.',
  newsType: '社会公开',
  operationType: '',
  source: '新闻来源',
  attachments: [
    {
      name: '63ba799d1cdc5e0eebfce1b470119303',
      identify: 'a33439f4ffac27b0d1fd34cac6a0a98c',
    },
    {
      name: '958096125d5883c101bdf4c07ac53da4',
      identify: 'e2276f8679410025f4697c24958d57c6',
    },
    {
      name: '44f9937860f737f0070cf2a6e04dcd49',
      identify: '2363c9ee873a79c755fc5477533028d5',
    },
  ],
  cover: {
    name: 'name',
    identify: 'identify',
  },
  bannerImage: {
    name: 'name',
    identify: 'identify',
  },
  dimension: '社会公开',
  updateTimeFormat: '2020年08月21日 21点07分',
  status: {
    id: 'LC0',
    name: '禁用',
    type: 'warning',
  },
  stick: {
    id: 'Lw',
    name: '未置顶',
    type: 'info',
  },
  homePageShowStatus: {
    id: 'MQ',
    name: '首页展示',
    type: 'success',
  },
  bannerStatus: {
    id: 'MQ',
    name: '轮播',
    type: 'info',
  },
  applyUserGroup: '陕西省发展和改革委员会',
  publishUserGroup: '陕西省发展和改革委员会',
  applyCrew: '委办局管理员',
  crew: '财金科',
}

export const newsType = [
  {
    value: 'MA',
    label: '信用动态',
    children: [
      {
        value: 'Mw',
        label: '通知公告',
        children: [
          {
            value: 11,
            label: '国家级通知公告',
          },
          {
            value: 12,
            label: '省级通知公告',
          },
          {
            value: 13,
            label: '市级通知公告',
          },
        ],
      },
      {
        value: 'NA',
        label: '工作动态',
        children: [
          {
            value: 14,
            label: '国家级工作动态',
          },
          {
            value: 15,
            label: '省级工作动态',
          },
          {
            value: 16,
            label: '市级工作动态',
          },
        ],
      },
      {
        value: 'NQ',
        label: '信用新闻',
        children: [
          {
            value: 17,
            label: '国家级信用新闻',
          },
          {
            value: 18,
            label: '省级信用新闻',
          },
          {
            value: 19,
            label: '市级信用新闻',
          },
        ],
      },
      {
        value: 'Ng',
        label: '信用建设',
        children: [
          {
            value: 20,
            label: '国家级信用建设',
          },
          {
            value: 21,
            label: '省级信用建设',
          },
          {
            value: 22,
            label: '市级信用建设',
          },
        ],
      },
    ],
  },
  {
    value: 'MQ',
    label: '联合奖惩',
    children: [
      {
        value: 'OA',
        label: '联合奖惩备忘录',
        children: [
          {
            value: 27,
            label: '联合奖惩备忘录',
          },
        ],
      },
      {
        value: 'MCs',
        label: '联合奖惩动态',
        children: [
          {
            value: 28,
            label: '联合奖惩动态',
          },
        ],
      },
      {
        value: 'MCw',
        label: '联合奖惩案例',
        children: [
          {
            value: 32,
            label: '政务诚信联合奖惩案例',
          },
          {
            value: 33,
            label: '商务诚信联合奖惩案例',
          },
          {
            value: 34,
            label: '司法公信联合奖惩案例',
          },
          {
            value: 35,
            label: '社会诚信联合奖惩案例',
          },
        ],
      },
    ],
  },
  {
    value: 'MA',
    label: '组织架构',
    children: [
      {
        value: 1,
        label: '领导小组',
      },
      {
        value: 2,
        label: '信用办',
      },
      {
        value: 3,
        label: '公示文件',
      },
      {
        value: 4,
        label: '信用窗口',
      },
    ],
  },
  {
    value: 'MQ',
    label: '政策法规',
    children: [
      {
        value: 5,
        label: '国家级政策法规',
      },
      {
        value: 6,
        label: '省级政策法规',
      },
      {
        value: 7,
        label: '市级政策法规',
      },
    ],
  },
  {
    value: 'Mg',
    label: '标准规范',
    children: [
      {
        value: 8,
        label: '国家级标准规范',
      },
      {
        value: 9,
        label: '省级标准规范',
      },
      {
        value: 10,
        label: '市级标准规范',
      },
    ],
  },
  {
    value: 'Nw',
    label: '信用研究',
    children: [
      {
        value: 23,
        label: '信用知识',
      },
      {
        value: 24,
        label: '国家级信用研究',
      },
      {
        value: 25,
        label: '省级信用研究',
      },
      {
        value: 26,
        label: '市级信用研究',
      },
    ],
  },
  {
    value: 'MC0',
    label: '信用监管',
    children: [
      {
        value: 29,
        label: '部门监管',
      },
      {
        value: 30,
        label: '媒体监管',
      },
      {
        value: 31,
        label: '行业监管',
      },
    ],
  },
  {
    value: 'MC4',
    label: '守信激励与失信惩戒',
    children: [
      {
        value: 36,
        label: '守信信息守信激励与失信惩戒',
      },
      {
        value: 37,
        label: '安全事故守信激励与失信惩戒',
      },
      {
        value: 38,
        label: '不良现象守信激励与失信惩戒',
      },
    ],
  },
  {
    value: 'MC8',
    label: '诚信建设万里行',
    children: [
      {
        value: 42,
        label: 'XX在行动',
      },
      {
        value: 43,
        label: '凡人善举树典型',
      },
    ],
  },
  {
    value: 'MDA',
    label: '信易+',
    children: [
      {
        value: 44,
        label: '信易批',
      },
      {
        value: 45,
        label: '信易贷',
      },
      {
        value: 46,
        label: '信易租',
      },
      {
        value: 47,
        label: '信易游',
      },
      {
        value: 48,
        label: '信易行',
      },
    ],
  },
  {
    value: 39,
    label: '信用承诺',
  },
  {
    value: 40,
    label: '典型案例',
  },
  {
    value: 41,
    label: '失信专项治理',
  },
]
