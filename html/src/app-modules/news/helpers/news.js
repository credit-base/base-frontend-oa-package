import Base from '@/models/base'

class News extends Base {
  // eslint-disable-next-line max-lines-per-function
  constructor (params = {}) {
    super(params)
    const {
      id,
      title,
      newsType = {
        id: '',
        name: '',
      },
      dimension = {
        id: '',
        name: '',
      },
      source,
      cover = {
        identify: '',
        name: '',
      },
      content,
      attachments = [],
      bannerImage = {
        identify: '',
        name: '',
      },
      status = {
        id: '',
        name: '',
        type: '',
      },
      homePageShowStatus = {
        id: '',
        name: '',
        type: '',
      },
      bannerStatus = {
        id: '',
        name: '',
        type: '',
      },
      stick = {
        id: '',
        name: '',
        type: '',
      },
      crew = {
        id: '',
        realName: '',
      },
      publishUserGroup = {
        id: '',
        name: '',
      },
      applyCrew = {
        id: '',
        realName: '',
      },
      applyUserGroup = {
        id: '',
        name: '',
      },
      operationType = {
        id: '',
        name: '',
      },
      applyStatus = {
        id: '',
        name: '',
        type: '',
      },
      rejectReason,
      createTime,
      createTimeFormat,
      statusTime,
      statusTimeFormat,
      updateTime,
      updateTimeFormat,
    } = params

    this.id = id
    this.title = title
    this.newsType = newsType.name
    this.dimension = dimension.name
    this.source = source
    this.cover = cover
    this.content = content
    this.attachments = attachments
    this.bannerImage = bannerImage
    this.status = status
    this.homePageShowStatus = homePageShowStatus
    this.bannerStatus = bannerStatus
    this.stick = stick
    this.crew = crew.realName
    this.publishUserGroup = publishUserGroup.name
    this.applyCrew = applyCrew.realName
    this.applyStatus = applyStatus
    this.applyUserGroup = applyUserGroup.name
    this.rejectReason = rejectReason
    this.createTime = createTime
    this.createTimeFormat = createTimeFormat
    this.statusTime = statusTime
    this.statusTimeFormat = statusTimeFormat
    this.updateTime = updateTime
    this.updateTimeFormat = updateTimeFormat
    this.operationType = operationType.name
  }
}

export default News
