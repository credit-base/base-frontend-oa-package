import News from './news'

/**
 * 发布表新闻列表翻译器
 * @param {*} res
 * @returns
 */
export function translateNewsList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new News(item)
      .get([
        'id',
        'title',
        'newsType',
        'publishUserGroup',
        'status',
        'stick',
        'homePageShowStatus',
        'bannerStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

/**
 * 发布表新闻详情翻译器
 * @param {*} res
 * @returns
 */
export function translateNewsDetail (res = {}) {
  return new News(res)
    .get([
      'id',
      'title',
      'source',
      'cover',
      'content',
      'newsType',
      'attachments',
      'status',
      'homePageShowStatus',
      'bannerStatus',
      'stick',
      'bannerImage',
      'dimension',
      'crew',
      'publishUserGroup',
      'updateTimeFormat',
    ])
}

/**
 * 审核表新闻列表翻译器
 * @param {*} res
 * @returns
 */
export function translateUnAuditedNewsList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new News(item)
      .get([
        'id',
        'title',
        'newsType',
        'operationType',
        'publishUserGroup',
        'applyStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

/**
 * 审核表新闻详情翻译器
 * @param {*} res
 * @returns
 */
export function translateUnAuditedNewsDetail (res = {}) {
  return new News(res)
    .get([
      'id',
      'title',
      'source',
      'cover',
      'content',
      'newsType',
      'attachments',
      'status',
      'homePageShowStatus',
      'bannerStatus',
      'stick',
      'bannerImage',
      'dimension',
      'crew',
      'publishUserGroup',
      'applyStatus',
      'applyCrew',
      'applyUserGroup',
      'operationType',
      'rejectReason',
      'updateTimeFormat',
    ])
}

export function translateEditNews (res = {}) {
  const {
    status = {},
    homePageShowStatus = {},
    bannerStatus = {},
    stick = {},
    title,
    source,
    newsType = {},
    content,
    dimension = {},
    cover = {},
    bannerImage = {},
    attachments = [],
  } = res

  return {
    status: status.id,
    homePageShowStatus: homePageShowStatus.id,
    bannerStatus: bannerStatus.id,
    stick: stick.id,
    title,
    source,
    newsType: newsType.id,
    content,
    dimension: dimension.id,
    cover,
    attachments,
    bannerImage,
  }
}

export function translateUpdateNewsData (data = {}, isBanner) {
  const {
    status,
    homePageShowStatus,
    bannerStatus,
    stick,
    title,
    source,
    newsType,
    content,
    dimension,
    cover = {},
    bannerImage = {},
    attachments = [],
  } = data

  const res = {
    status,
    homePageShowStatus,
    bannerStatus,
    stick,
    title,
    source,
    newsType,
    content,
    dimension,
    cover,
  }

  if (isBanner && bannerImage.name && bannerImage.identify) {
    res.bannerImage = bannerImage
  }

  if (cover && cover.name && cover.identify) {
    res.cover = cover
  }

  if (Array.isArray(attachments) && attachments.length) {
    // todo 文件服务器提供后需要编辑
    res.attachments = attachments.map(prop => {
      return {
        name: prop.name,
        identify: prop.identify ? prop.identify : prop.raw && prop.raw.identify,
      }
    })
  }

  return res
}
