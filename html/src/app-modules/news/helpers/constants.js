import { CONSTANTS } from '@/plugins/constants'

const {
  STATUS = {},
  STICK = {},
  BANNER_STATUS = {},
  HOME_PAGE_SHOW = {},
  APPLY_STATUS = {},
} = CONSTANTS

export const NEWS_TABS = [
  { name: 'official-news', label: 'OFFICIAL_NEWS', component: 'OfficialNews' },
  { name: 'audit-news', label: 'AUDIT_NEWS', component: 'AuditNews' },
]

export const DIMENSION_ENUM = [
  {
    label: '社会公开',
    value: 'MA',
  },
  {
    label: '政务共享',
    value: 'MQ',
  },
]

export const STATUS_ENUM = [
  {
    label: '启用',
    value: STATUS.ENABLE,
  },
  {
    label: '禁用',
    value: STATUS.DISABLE,
  },
]

export const BANNER_STATUS_ENUM = [
  {
    label: '轮播',
    value: BANNER_STATUS.SET,
  },
  {
    label: '不轮播',
    value: BANNER_STATUS.UN_SET,
  },
]

export const HOME_PAGE_SHOW_ENUM = [
  {
    label: '首页展示',
    value: HOME_PAGE_SHOW.SHOW,
  },
  {
    label: '不首页展示',
    value: HOME_PAGE_SHOW.UN_SHOW,
  },
]

export const STICK_ENUM = [
  {
    label: '置顶',
    value: STICK.TOP,
  },
  {
    label: '不置顶',
    value: STICK.CANCEL_TOP,
  },
]

export const APPLY_STATUS_ENUM = [
  {
    label: '待审核',
    value: APPLY_STATUS.PADDING,
  },
  {
    label: '已驳回',
    value: APPLY_STATUS.REJECT,
  },
]

export const BANNER_IMAGE_SIZE = {
  width: 320,
  height: 135,
}

export const COVER_SIZE = {
  width: 240,
  height: 135,
}
