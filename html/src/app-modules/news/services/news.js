import request from '@/utils/request'
import {
  newsType,
  unAuditedNewsList,
  unAuditedNewsDetail,
} from '../helpers/mocks'

/**
 * 一、新闻发布表
 *
 * 接口：
 * 1. 列表
 * 2. 详情
 * 3. 新增
 * 4. 编辑（获取新闻内容/更新新闻内容）
 * 5. 启用
 * 6. 禁用
 * 7. 置顶
 * 8. 取消置顶
 * 9. 移动
 */

/**
 * 1. 列表
 * @param {title, newsType, userGroup, bannerStatus, homePageShowStatus, stick, status, sort, limit, page} params
 * @returns
 */
export function fetchNewsList (params = {}) {
  return request('/api/news', { params })
}

/**
 * 2. 详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function fetchNewsDetail (id, params = {}) {
  return request(`/api/news/${id}`, { params })
}

/**
 * 3. 新增
 * @param {*} params
 * @returns
 */
export function createNews (params = {}) {
  return request('/api/news/add', { method: 'POST', params })
}

/**
 * 4. 获取编辑新闻
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function fetchEditNews (id, params = {}) {
  return request(`/api/news/${id}`, { params })
}

/**
 * 4. 更新编辑新闻
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function updateNews (id, params = {}) {
  return request(`/api/news/${id}/edit`, { method: 'POST', params })
}

/**
 * 5. 启用新闻
 * @param {*} id
 * @returns
 */
export function updateEnableNews (id) {
  return request(`/api/news/${id}/enable`, { method: 'POST' })
}

/**
 * 6. 禁用新闻
 * @param {*} id
 * @returns
 */
export function updateDisableNews (id) {
  return request(`/api/news/${id}/disable`, { method: 'POST' })
}

/**
 * 7. 置顶新闻
 * @param {*} id
 * @returns
 */
export function updateTopNews (id) {
  return request(`/api/news/${id}/top`, { method: 'POST' })
}

/**
 * 8. 取消置顶新闻
 * @param {*} id
 * @returns
 */
export function updateCancelTopNews (id) {
  return request(`/api/news/${id}/cancelTop`, { method: 'POST' })
}

/**
 * 9. 移动新闻
 * @param {*} id
 * @param {*} newType
 * @returns
 */
export function updateMoveNews (id, newType) {
  return request(`/api/news/${id}/move/${newType}`, { method: 'POST' })
}

/**
 * 二、新闻审核表
 *
 * 接口：
 * 1. 列表
 * 2. 详情
 * 3. 编辑（获取新闻内容/更新新闻内容）
 * 4. 审核通过
 * 5. 审核驳回
 */

/**
 * 1. 列表
 * @param {title, newsType, userGroup, applyStatus, sort, limit, page} params
 * @returns
 */
export function fetchUnAuditedNewsList (params = {}) {
  if (params.useMock) {
    return {
      total: 1,
      list: [
        ...unAuditedNewsList,
      ],
    }
  }

  return request('/api/unAuditedNews', { params })
}

/**
 * 2. 详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function fetchUnAuditedNewsDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...unAuditedNewsDetail,
    }
  }

  return request(`/api/unAuditedNews/${id}`, { params })
}

/**
 * 3. 获取审核表编辑新闻
 * 注：审核表新闻状态为待审核时候，不能编辑
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function fetchEditUnAuditedNews (id, params = {}) {
  if (params.useMock) {
    return {
      ...unAuditedNewsDetail,
    }
  }

  return request(`/api/unAuditedNews/${id}`, { params })
}
/**
 * 3. 更新编辑新闻
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function updateUnAuditedNews (id, params = {}) {
  return request(`/api/unAuditedNews/${id}/edit`, { method: 'POST', params })
}

/**
 * 4. 审核通过
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function updateApproveUnAuditedNews (id) {
  return request(`/api/unAuditedNews/${id}/approve`, { method: 'POST' })
}

/**
 * 5. 审核驳回
 * @param {*} id
 * @param { rejectReason } params
 * @returns
 */
export function updateRejectUnAuditedNews (id, params = {}) {
  return request(`/api/unAuditedNews/${id}/reject`, { method: 'POST', params })
}

/**
 * 三、获取新闻分类
 * @returns
 */
export function fetchNewsType (params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...newsType,
      ],
    }
  }
  return request(`/api/newsType`, { params })
}
