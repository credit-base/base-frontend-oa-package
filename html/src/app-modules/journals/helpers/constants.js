import { CONSTANTS } from '@/plugins/constants'

const {
  STATUS = {},
  APPLY_STATUS = {},
} = CONSTANTS

export const JOURNALS_TABS = [
  { name: 'official-journals', label: 'OFFICIAL_JOURNALS', component: 'ReleaseOfficialJournals' },
  { name: 'audit-journals', label: 'AUDIT_JOURNALS', component: 'ReleaseAuditJournals' },
]

export const UN_AUDITED_JOURNALS_TABS = [
  { name: 'official-journals', label: 'OFFICIAL_JOURNALS', component: 'ReviewOfficialJournals' },
  { name: 'audit-journals', label: 'AUDIT_JOURNALS', component: 'ReviewAuditJournals' },
]

export const STATUS_ENUM = [
  {
    text: '启用',
    value: STATUS.ENABLE,
  },
  {
    text: '禁用',
    value: STATUS.DISABLE,
  },
]

export const APPLY_STATUS_ENUM = [
  {
    text: '待审核',
    value: APPLY_STATUS.PADDING,
  },
  {
    text: '已驳回',
    value: APPLY_STATUS.REJECT,
  },
]

export const COVER_SIZE = {
  width: 210,
  height: 297,
}
