import Journals from './journals'

export function translateJournalsList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Journals(item)
      .get([
        'id',
        'title',
        'year',
        'publishUserGroup',
        'status',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}
export function translateJournalsDetail (res = {}) {
  return new Journals(res)
    .get([
      'id',
      'crew',
      'updateTimeFormat',
      'status',
      'title',
      'year',
      'source',
      'cover',
      'authImages',
      'attachment',
      'description',
    ])
}

export function translateUnAuditedJournalsList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Journals(item)
      .get([
        'id',
        'title',
        'year',
        'operationType',
        'publishUserGroup',
        'applyStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}
export function translateUnAuditedJournalsDetail (res = {}) {
  return new Journals(res)
    .get([
      'id',
      'applyStatus',
      'rejectReason',
      'applyCrew',
      'crew',
      'updateTimeFormat',
      'status',
      'title',
      'year',
      'source',
      'cover',
      'authImages',
      'attachment',
      'description',
    ])
}

export function translateEditJournalsDetail (res = {}) {
  const {
    status = {},
    title,
    year,
    source,
    cover = {},
    authImages = [],
    attachment = {},
    description,
  } = res

  return {
    status: status.id,
    title,
    year: new Date(`${year}-01-01 00:00:00`),
    source,
    cover: (cover.name && cover.identify) ? cover : {},
    authImages: (Array.isArray(authImages) && authImages.length) ? authImages : [],
    attachment: (attachment && attachment.name && attachment.identify) ? [attachment] : [],
    description,
  }
}

export function translateUpdateJournalsData (data = {}) {
  const {
    status,
    title,
    year,
    source,
    // cover = {},
    // attachment = [],
    // authImages = [],
    description,
  } = data

  const res = {
    status,
    title,
    year: parseInt(year) ? parseInt(year) : year.getFullYear(),
    source,
    description,
  }

  // if (cover && cover.name && cover.identify) {
  //   res.cover = cover
  // }

  // if (Array.isArray(attachment) && attachment.length) {
  //   const [{ name, raw = {}, identify }] = attachment
  //   const data = {
  //     name,
  //     identify: identify ? identify : raw.identify,
  //   }
  //   res.attachment = data
  // }

  // if (Array.isArray(authImages) && authImages.length) {
  //   // todo 文件服务器提供后需要编辑
  //   res.authImages = authImages.map(prop => {
  //     return {
  //       name: prop.name,
  //       identify: prop.identify ? prop.identify : prop.raw && prop.raw.identify,
  //     }
  //   })
  // }

  return res
}
