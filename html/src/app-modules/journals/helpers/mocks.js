export const journalsList = [
  {
    id: 'MC4',
    title: '测试标题123',
    source: '测试来源123',
    year: '2019',
    status: {
      id: 'Lw',
      name: '启用',
      type: 'success',
    },
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    crew: {
      id: 'MA',
      realName: '张科',
    },
    publishUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'MC8',
    title: '测试标题123',
    source: '测试来源123',
    year: '2019',
    status: {
      id: 'LC0',
      name: '禁用',
      type: 'danger',
    },
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    crew: {
      id: 'MA',
      realName: '张科',
    },
    publishUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'NA',
    title: '测试标题123',
    source: '测试来源123',
    year: '2019',
    status: {
      id: 'Lw',
      name: '启用',
      type: 'success',
    },
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    crew: {
      id: 'MA',
      realName: '张科',
    },
    publishUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'Mw',
    title: '测试标题123',
    source: '测试来源123',
    year: '2019',
    status: {
      id: 'Lw',
      name: '启用',
      type: 'success',
    },
    updateTime: 1620872174,
    updateTimeFormat: '2021年05月13日 10点16分',
    statusTime: 1620869215,
    statusTimeFormat: '2021年05月13日 09点26分',
    createTime: 1620831782,
    createTimeFormat: '2021年05月12日 23点03分',
    crew: {
      id: 'MA',
      realName: '张科',
    },
    publishUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
]
export const journalsDetail = {
  id: 'MC4',
  title: '测试新闻标题123',
  source: '测试新闻来源123',
  year: '2020',
  status: {
    id: 'Lw',
    name: '启用',
    type: 'success',
  },
  attachment:
  {
    name: '测试name13',
    identify: '测试identify123.doc',
  },
  authImages: [
    {
      name: '测试name13',
      identify: '测试identify123.doc',
    },
    {
      name: '测试name13',
      identify: '测试identify123.doc',
    },
    {
      name: '测试name13',
      identify: '测试identify123.doc',
    },
  ],
  description: '简介',
  cover: {
    name: '测试封面名称123',
    identify: '测试封面地址123.jpg',
  },
  updateTime: 1620872174,
  updateTimeFormat: '2021年05月13日 10点16分',
  statusTime: 1620869215,
  statusTimeFormat: '2021年05月13日 09点26分',
  createTime: 1620831782,
  createTimeFormat: '2021年05月12日 23点03分',
  crew: { // 发布人数据
    id: 'MA',
    realName: '张科',
  },
  publishUserGroup: { // 发布委办局数据
    id: 'MA',
    name: '陕西省发展和改革委员会',
  },
}

export const unAuditedJournalsList = [
  {
    id: 'Ng',
    title: '新闻标题测试',
    source: '来源',
    year: '2019',
    applyStatus: {
      id: 'LC0',
      name: '已驳回',
      type: 'danger',
    },
    operationType: {
      id: 'MA',
      name: '新增',
    },
    status: {
      id: 'Lw',
      name: '启用',
      type: 'success',
    },
    updateTime: 1620822447,
    updateTimeFormat: '2021年05月12日 20点27分',
    statusTime: 0,
    statusTimeFormat: '1970年01月01日 08点00分',
    createTime: 1620822447,
    createTimeFormat: '2021年05月12日 20点27分',
    crew: {
      id: 'MA',
      realName: '张科',
    },
    applyCrew: {
      id: 'MA',
      realName: '张科',
    },
    publishUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
    applyUserGroup: {
      id: 'MA',
      name: '陕西省发展和改革委员会',
    },
  },
  {
    id: 'MC0',
    title: '测试新闻标题',
    source: '测试新闻来源',
    year: '2019',
    applyStatus: {
      id: 'Lw',
      name: '待审核',
      type: 'warning',
    },
    operationType: {
      id: 'MA',
      name: '新增',
    },
    status: {
      id: 'LC0',
      name: '禁用',
      type: 'danger',
    },
    updateTime: 1620830909,
    updateTimeFormat: '2021年05月12日 22点48分',
    statusTime: 0,
    statusTimeFormat: '1970年01月01日 08点00分',
    createTime: 1620830909,
    createTimeFormat: '2021年05月12日 22点48分',
    crew: {
      id: 'Mg',
      realName: '李玟雨',
    },
    applyCrew: {
      id: 'Mg',
      realName: '李玟雨',
    },
    publishUserGroup: {
      id: 'Lw',
      name: '',
    },
    applyUserGroup: {
      id: 'Lw',
      name: '',
    },
  },
]
export const unAuditedJournalsDetail = {
  id: 'MA',
  title: '新闻标题测试',
  source: '来源',
  rejectReason: '审核驳回原因',
  year: '2020',
  attachment:
  {
    name: '测试name13',
    identify: '测试identify123.doc',
  },
  authImages: [
    {
      name: '测试name13',
      identify: '测试identify123.doc',
    },
    {
      name: '测试name13',
      identify: '测试identify123.doc',
    },
    {
      name: '测试name13',
      identify: '测试identify123.doc',
    },
  ],
  description: '简介',
  cover: {
    name: '测试封面名称123',
    identify: '测试封面地址123.jpg',
  },
  applyStatus: {
    id: 'Lw',
    name: '待审核',
    type: 'warning',
  },
  operationType: {
    id: 'MA',
    name: '新增',
  },
  status: {
    id: 'Lw',
    name: '启用',
    type: 'success',
  },
  updateTime: 1620821849,
  updateTimeFormat: '2021年05月12日 20点17分',
  statusTime: 0,
  statusTimeFormat: '1970年01月01日 08点00分',
  createTime: 1620821849,
  createTimeFormat: '2021年05月12日 20点17分',
  crew: {
    id: 'MA',
    realName: '张科',
  },
  applyCrew: {
    id: 'MQ',
    realName: '王文',
  },
  publishUserGroup: {
    id: 'MA',
    name: '陕西省发展和改革委员会',
  },
  applyUserGroup: {
    id: 'MA',
    name: '陕西省发展和改革委员会',
  },
}
