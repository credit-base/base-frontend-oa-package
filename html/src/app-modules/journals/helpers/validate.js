import i18n from '@/i18n'
import {
  RE_SOURCE,
  RE_TITLE,
  RE_DESCRIPTION,
} from '@/constants/regexp'

const {
  INVALID_JOURNALS_TITLE,
  INVALID_SOURCE,
  INVALID_DESCRIPTION,
  INVALID_YEAR,
} = i18n.t('journals.TIP')

export function validateTitle (_, value, callback) {
  return RE_TITLE.test(value)
    ? callback()
    : callback(new Error(INVALID_JOURNALS_TITLE))
}

export function validateSource (_, value, callback) {
  return RE_SOURCE.test(value)
    ? callback()
    : callback(new Error(INVALID_SOURCE))
}

export function validateDescription (_, value, callback) {
  return RE_DESCRIPTION.test(value)
    ? callback()
    : callback(new Error(INVALID_DESCRIPTION))
}

export function validateYear (_, value, callback) {
  const year = parseInt(value) ? parseInt(value) : value.getFullYear()
  if (year < 1901 || year > 2155) {
    callback(new Error(INVALID_YEAR))
  } else {
    callback()
  }
}
