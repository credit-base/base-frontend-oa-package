import Base from '@/models/base'

class Journals extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      title,
      source,
      rejectReason,
      year,
      attachment = {},
      authImages = [],
      description,
      cover = {},
      applyStatus = {},
      operationType = {},
      status = {},
      updateTimeFormat = {},
      crew = {},
      applyCrew = {},
      publishUserGroup = {},
      applyUserGroup = {},
    } = params

    this.id = id
    this.title = title
    this.source = source
    this.rejectReason = rejectReason
    this.year = year + '年'
    this.cover = (cover.name && cover.identify) ? cover : {}
    this.authImages = (Array.isArray(authImages) && authImages.length) ? authImages : []
    this.attachment = (attachment && attachment.name && attachment.identify) ? [attachment] : []
    this.description = description
    this.applyStatus = applyStatus
    this.operationType = operationType.name
    this.status = status
    this.updateTimeFormat = updateTimeFormat
    this.crew = crew.realName
    this.applyCrew = applyCrew.realName
    this.publishUserGroup = publishUserGroup.name
    this.applyUserGroup = applyUserGroup.name
  }
}

export default Journals
