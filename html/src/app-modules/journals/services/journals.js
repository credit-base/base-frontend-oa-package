import request from '@/utils/request'

import {
  journalsList,
  journalsDetail,
  unAuditedJournalsList,
  unAuditedJournalsDetail,
} from '../helpers/mocks'
/**
 * 一、信用刊物发布表
 *
 * 接口：
 * 1. 列表
 * 2. 详情
 * 3. 新增
 * 4. 编辑（获取信用刊物内容/更新信用刊物内容）
 * 5. 启用
 * 6. 禁用
 */

/**
 * 1. 列表
 * @param {title, userGroup, status, sort, limit, page} params
 * @returns
 */
export function fetchJournalsList (params = {}) {
  if (params.useMock) {
    return {
      total: 4,
      list: [
        ...journalsList,
      ],
    }
  }
  return request('/api/journals', { method: 'GET', params })
}

/**
 * 2. 详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function fetchJournalsDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...journalsDetail,
    }
  }

  return request(`/api/journals/${id}`, { method: 'GET' })
}

/**
 * 3. 新增
 * @param {*} params
 * @returns
 */
export function createJournals (params = {}) {
  return request('/api/journals/add', { method: 'POST', params })
}

/**
 * 4. 获取编辑信用刊物
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function fetchEditJournals (id, params = {}) {
  if (params.useMock) {
    return {
      ...journalsDetail,
    }
  }

  return request(`/api/journals/${id}`, { method: 'GET' })
}

/**
 * 4. 更新编辑信用刊物
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function updateJournals (id, params = {}) {
  return request(`/api/journals/${id}/edit`, { method: 'POST', params })
}

/**
 * 5. 启用信用刊物
 * @param {*} id
 * @returns
 */
export function updateJournalsStatus (id, type) {
  return request(`/api/journals/${id}/${type}`, { method: 'POST' })
}

/**
 * 二、信用刊物审核表
 *
 * 接口：
 * 1. 列表
 * 2. 详情
 * 3. 编辑（获取信用刊物内容/更新信用刊物内容）
 * 4. 审核通过
 * 5. 审核驳回
 */

/**
 * 1. 列表
 * @param {title, journalsType, userGroup, applyStatus, sort, limit, page} params
 * @returns
 */
export function fetchUnAuditedJournalsList (params = {}) {
  if (params.useMock) {
    return {
      total: 2,
      list: [
        ...unAuditedJournalsList,
      ],
    }
  }

  return request('/api/unAuditedJournals', { method: 'GET', params })
}

/**
 * 2. 详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function fetchUnAuditedJournalsDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...unAuditedJournalsDetail,
    }
  }

  return request(`/api/unAuditedJournals/${id}`, { method: 'GET' })
}

/**
 * 3. 获取审核表编辑信用刊物
 * 注：审核表信用刊物状态为待审核时候，不能编辑
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function fetchEditUnAuditedJournals (id, params = {}) {
  if (params.useMock) {
    return {
      ...unAuditedJournalsDetail,
    }
  }

  return request(`/api/unAuditedJournals/${id}`, { method: 'GET' })
}
/**
 * 3. 更新编辑信用刊物
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function updateUnAuditedJournals (id, params = {}) {
  return request(`/api/unAuditedJournals/${id}/edit`, { method: 'POST', params })
}

/**
 * 4. 审核通过
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function updateApproveUnAuditedJournals (id) {
  return request(`/api/unAuditedJournals/${id}/approve`, { method: 'POST' })
}

/**
 * 5. 审核驳回
 * @param {*} id
 * @param { rejectReason } params
 * @returns
 */
export function updateRejectUnAuditedJournals (id, params = {}) {
  return request(`/api/unAuditedJournals/${id}/reject`, { method: 'POST', params })
}
