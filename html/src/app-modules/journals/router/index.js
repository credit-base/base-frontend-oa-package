/**
 * @file 新闻 - 路由
 * @module async-routes/journals
 */

import Layout from '@/layout'
import permissions from '@/constants/permissions'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const { STATUS_JOURNALS } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/journals',
    name: 'Journals',
    component: Layout,
    alwaysShow: true,
    meta: {
      title: 'journals.ROUTER.JOURNALS',
      icon: 'journals',
      breadcrumb: false,
      isVisible: STATUS_JOURNALS,
    },
    children: [
      // 信用刊物管理
      {
        path: '/journals/release/list',
        name: 'JournalsReleaseList',
        component: () => import('@/app-modules/journals/views/release/list'),
        meta: {
          title: 'journals.ROUTER.JOURNALS_RELEASE_LIST',
          icon: 'journals',
          routerId: permissions.JOURNALS_ID,
        },
      },
      // 信用刊物管理发布表详情
      {
        path: '/journals/release/official-detail/:id',
        name: 'JournalsReleaseOfficialDetail',
        hidden: true,
        component: () => import('@/app-modules/journals/views/release/official-detail'),
        meta: {
          title: 'journals.ROUTER.JOURNALS_DETAIL',
          icon: 'journals',
          activeMenu: '/journals/release/list',
        },
      },
      // 信用刊物管理审核表详情
      {
        path: '/journals/release/audit-detail/:id',
        name: 'JournalsReleaseAuditDetail',
        hidden: true,
        component: () => import('@/app-modules/journals/views/release/audit-detail'),
        meta: {
          title: 'journals.ROUTER.JOURNALS_DETAIL',
          icon: 'journals',
          activeMenu: '/journals/release/list',
        },
      },
      // 新增信用刊物
      {
        path: '/journals/add',
        name: 'JournalsAdd',
        hidden: true,
        component: () => import('@/app-modules/journals/views/add'),
        meta: {
          title: 'journals.ROUTER.JOURNALS_ADD',
          icon: 'journals',
          activeMenu: '/journals/release/list',
        },
      },
      // 信用刊物发布编辑
      {
        path: '/journals/release/official-edit/:id',
        name: 'JournalsOfficialEdit',
        hidden: true,
        component: () => import('@/app-modules/journals/views/release/official-edit'),
        meta: {
          title: 'journals.ROUTER.JOURNALS_EDIT',
          icon: 'journals',
          activeMenu: '/journals/release/list',
        },
      },
      // 信用刊物审核编辑
      {
        path: '/journals/release/audit-edit/:id',
        name: 'JournalsAuditEdit',
        hidden: true,
        component: () => import('@/app-modules/journals/views/release/audit-edit'),
        meta: {
          title: 'journals.ROUTER.JOURNALS_EDIT',
          icon: 'journals',
          activeMenu: '/journals/review/list',
        },
      },
      // 信用刊物审核
      {
        path: '/journals/review/list',
        name: 'JournalsReviewList',
        component: () => import('@/app-modules/journals/views/review/list'),
        meta: {
          title: 'journals.ROUTER.JOURNALS_REVIEW_LIST',
          icon: 'journals',
          routerId: permissions.UN_AUDITED_JOURNALS_ID,
        },
      },
      // 信用刊物审核发布表详情
      {
        path: '/journals/review/official-detail/:id',
        name: 'JournalsReviewOfficialDetail',
        hidden: true,
        component: () => import('@/app-modules/journals/views/review/official-detail'),
        meta: {
          title: 'journals.ROUTER.JOURNALS_DETAIL',
          icon: 'journals',
          activeMenu: '/journals/review/list',
        },
      },
      // 信用刊物审核审核表详情
      {
        path: '/journals/review/audit-detail/:id',
        name: 'JournalsReviewAuditDetail',
        hidden: true,
        component: () => import('@/app-modules/journals/views/review/audit-detail'),
        meta: {
          title: 'journals.ROUTER.JOURNALS_DETAIL',
          icon: 'journals',
          activeMenu: '/journals/review/list',
        },
      },
    ],
  },
]
