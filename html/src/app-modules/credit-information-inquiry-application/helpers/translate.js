import Enterprise from './enterprise'
import FinancialData from './financial-data'
import TemplateDataCount from './template-data-count'
import TemplateDataList from './template-data-list'
import Template from './template'
import TemplateVersion from './template-version'
import CreditScore from './credit-score'

export function translateToEnterpriseList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Enterprise(item)
      .get([
        'id',
        'name',
        'unifiedSocialCreditCode',
        'registrationStatus',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

export function translateToEnterpriseDetail (res = {}) {
  return new Enterprise(res)
    .get([
      'id',
      'name',
      'unifiedSocialCreditCode',
      'principal',
      'principalCardId',
      'establishmentDate',
      'enterpriseType',
      'enterpriseTypeCode',
      'registrationAuthority',
      'industryCategory',
      'businessTermStart',
      'businessTermTo',
      'administrativeArea',
      'registrationStatus',
      'updateTimeFormat',
    ])
}

export function translateFinancialData (res = {}) {
  return new FinancialData(res)
    .get([
      'CWXX',
      'FXXX',
      'JYZK',
      'QYNB',
      'WXZC',
      'YLXX',
    ])
}

export function translateTemplateDataCount (res = {}) {
  return new TemplateDataCount(res)
    .get([
      'incentiveTotal',
      'licensingTotal',
      'penaltyTotal',
      'punishTotal',
      'jointWarningTotal',
      'focusTotal',
      'enterprisePromiseTotal',
      'contractPerformanceTotal',
      'doubleRandomOneOpenTotal',
    ])
}

export function translateTemplateDataList (res = {}) {
  return new TemplateDataList(res)
    .get([
      'licensing',
      'penalty',
      'incentive',
      'punish',
      'jointWarning',
      'focus',
      'enterprisePromise',
      'contractPerformance',
      'doubleRandomOneOpen',
    ])
}

export function translateCreditScore (res = {}) {
  return new CreditScore(res)
    .get([
      'totalScore',
      'grade',
    ])
}

// 基础目录列表
export function translateTemplateList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Template(item)
      .get([
        'id',
        'name',
        'identify',
        'subjectCategory',
        'dimension',
        'infoClassify',
        'version',
        'updateTime',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

// 基础目录详情
export function translateTemplateDetail (res = {}) {
  return new Template(res)
    .get([
      'id',
      'name',
      'identify',
      'year',
      'version',
      'subjectCategory',
      'dimension',
      'exchangeFrequency',
      'infoClassify',
      'infoCategory',
      'description',
      'items',
      'updateTime',
      'createTime',
      'crew',
    ])
}

export function translateTemplateVersionList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new TemplateVersion(item)
      .get([
        'id',
        'version',
        'description',
        'createTime',
        'updateTime',
        'currentRelease',
        'addItems',
        'editItems',
        'deleteItems',
        'attachment',
        'crew',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

const isValidField = field => typeof field === 'object' &&
field.name !== undefined &&
field.id !== undefined

export function dehydrate (field, { filterLw } = {}) {
  if (!isValidField(field)) return ''
  if (filterLw && field.id === 'Lw') return ''
  return field.id
}

export function translateTemplateEdit (res = {}) {
  const {
    id,
    name,
    identify,
    version,
    description,
  } = res

  const dimension = dehydrate(res.dimension)
  const exchangeFrequency = dehydrate(res.exchangeFrequency)
  const infoClassify = dehydrate(res.infoClassify)
  const infoCategory = dehydrate(res.infoCategory, { filterLw: true })
  const subjectCategory = Array.isArray(res.subjectCategory)
    ? res.subjectCategory.map(dehydrate)
    : []
  const items = Array.isArray(res.items)
    ? res.items.map(item => {
      const {
        type,
        dimension,
        isMasked,
        isNecessary,
      } = item

      return {
        ...item,
        dimension: dehydrate(dimension),
        type: dehydrate(type),
        isMasked: dehydrate(isMasked),
        isNecessary: dehydrate(isNecessary),
      }
    })
    : []

  return {
    id,
    name,
    identify,
    version,
    subjectCategory,
    dimension,
    exchangeFrequency,
    infoClassify,
    infoCategory,
    description,
    items,
  }
}
