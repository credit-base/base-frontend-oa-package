// 本级
export const DATA_DETAIL_TABS = [
  { name: 'data-traceability-graph', label: 'DATA_TRACEABILITY_GRAPH', component: 'ComponentViewTraceabilityGraph' },
  { name: 'data-detail', label: 'DATA_DETAIL', component: 'ComponentViewDataDetail' },
]

// 国标资源目录步骤配置
export const TEMPLATE_STEPS_META_VALUE = {
  StepCatalogInfo: {
    index: 0,
    name: 'StepCatalogInfo',
  },
  StepTemplateInfo: {
    index: 1,
    name: 'StepTemplateInfo',
  },
  StepPreviewConfirm: {
    index: 2,
    name: 'StepPreviewConfirm',
  },
}

export const TEMPLATE_STEPS_META_CONFIG = [
  {
    title: 'STEP_CATALOG_INFO',
    index: 0,
    component: 'StepCatalogInfo',
    icon: 'step-catalog-info',
  },
  {
    title: 'STEP_TEMPLATE_INFO',
    index: 1,
    component: 'StepTemplateInfo',
    icon: 'step-template-info',
  },
  {
    title: 'STEP_PREVIEW_CONFIRM',
    index: 2,
    component: 'StepPreviewConfirm',
    icon: 'step-preview-confirm',
  },
]

export const TABS_NAME = {
  TEMPLATE_NAME: 'resource-catalog-template',
  VERSION_NAME: 'resource-catalog-version',
  RELATION_CHART_NAME: 'resource-catalog-chart',
}

export const RESOURCE_CATALOG_DETAIL_TABS = [
  {
    name: TABS_NAME.TEMPLATE_NAME,
    label: 'TEMPLATE_TITLE',
    component: 'ResourceCatalogTemplate',
  },
  {
    name: TABS_NAME.VERSION_NAME,
    label: 'VERSION_TITLE',
    component: 'ResourceCatalogVersion',
  },
  {
    name: TABS_NAME.RELATION_CHART_NAME,
    label: 'RELATION_CHART',
    component: 'ComponentViewTraceabilityGraph',
  },
]

export const TEMPLATE_CATEGORY = {
  GB: 'national-standard',
  WBJ: 'user-group',
  BJ: 'current-unit',
}

export const VERSION_OPTIONS = [
  {
    id: 'MA',
    name: '1-D012A100C101',
  },
  {
    id: 'MQ',
    name: '2-D012A100C101',
  },
  {
    id: 'Mg',
    name: '3-D012A100C101',
  },
  {
    id: 'Mw',
    name: '4-D012A100C101',
  },
  {
    id: 'NQ',
    name: '5-D012A100C101',
  },
  {
    id: 'Na',
    name: '6-D012A100C101',
  },
]

export const TEMPLATE_DETAIL_PATH = `/template-management-subsystem/base-templates/detail/`
export const TEMPLATE_EDIT_PATH = `/template-management-subsystem/base-templates/edit/`
