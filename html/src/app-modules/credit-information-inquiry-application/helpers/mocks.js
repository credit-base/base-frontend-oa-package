/* cSpell: disable */

export const enterpriseList = [
  {
    id: 'MA',
    name: '天科医疗器械卫生有限公司',
    unifiedSocialCreditCode: '91610628305342459C',
    registrationStatus: '营业',
    updateTimeFormat: '2020年12月8日 17时57分',
  },
  {
    id: 'MQ',
    name: '天水旅游景区管理有限公司',
    unifiedSocialCreditCode: '80611590608902099E',
    registrationStatus: '营业',
    updateTimeFormat: '2020年12月8日 17时57分',
  },
  {
    id: 'Mg',
    name: '猿辅导教育有限公司',
    unifiedSocialCreditCode: '40611590308902099E',
    registrationStatus: '在营',
    updateTimeFormat: '2020年3月21日 15时6分',
  },
  {
    id: 'Mw',
    name: '保姆式居民服务有限公司',
    unifiedSocialCreditCode: '30611590108902099E',
    registrationStatus: '在册',
    updateTimeFormat: '2020年12月8日 17时57分',
  },
]

export const enterpriseDetail = {
  id: 'MA',
  name: '天科医疗器械卫生有限公司',
  unifiedSocialCreditCode: '91610628305342459C',
  principal: '李亮',
  principalCardId: '412825198907057568',
  establishmentDate: '20191014',
  enterpriseTypeCode: '个体工商户',
  registrationAuthority: '市场监督管理局',
  industryCategory: '卫生和社会工作',
  businessTermStart: '20191014',
  businessTermTo: '20321014',
  administrativeArea: '陕西省',
  registrationStatus: '营业',
  updateTimeFormat: '2020年08月15日 13时15分',
}

export const creditScore = {
  totalScore: 1200,
  grade: 'A',
}
export const industryCreditRating = {
  totalScore: 700,
  grade: 'C',
}

export const templateDataCount = {
  licensing: 1,
  penalty: 0,
  incentive: 12,
  punish: 12,
  jointWarning: 0,
  focus: 0,
  enterprisePromise: 0,
  contractPerformance: 0,
  doubleRandomOneOpen: 0,
}

export const templateDataList = {
  licensing: [],
  sanction: [],
  red: [],
  black: [],
  jointWarning: [],
  focus: [],
  enterprisePromise: [],
  contractPerformance: [],
  doubleRandomOneOpen: [],
}

export const financialData = {
  CWXX: [
    { name: '税务信息', value: 0 },
    { name: '融资信息', value: 0 },
  ],
  FXXX: [
    { name: '行政处罚', value: 0 },
    { name: '黑名单', value: 0 },
  ],
  JYZK: [
    { name: '动产抵押', value: 0 },
    { name: '产品信息', value: 0 },
    { name: '经营异常', value: 0 },
  ],
  QYNB: [
    { name: '出资信息', value: 0 },
    { name: '对外投资信息', value: 0 },
  ],
  WXZC: [
    { name: '著作权', value: 0 },
    { name: '软著信息', value: 0 },
    { name: '证书信息', value: 0 },
  ],
  YLXX: [
    { name: '行政许可', value: 0 },
    { name: '红名单', value: 0 },
  ],
}

export const creditWarningData = [
  {
    month: '08月',
    value: 0,
  },
  {
    month: '07月',
    value: 0,
  },
  {
    month: '06月',
    value: 0,
  },
  {
    month: '05月',
    value: 0,
  },
  {
    month: '04月',
    value: 0,
  },
  {
    month: '03月',
    value: 0,
  },
  {
    month: '02月',
    value: 0,
  },
  {
    month: '01月',
    value: 0,
  },
  {
    month: '12月',
    value: 0,
  },
  {
    month: '11月',
    value: 0,
  },
  {
    month: '10月',
    value: 0,
  },
  {
    month: '09月',
    value: 0,
  },
]

export const templateList = [
  {
    id: 'MA', // 资源目录数据id
    infoClassify: {
      id: 'MA',
      name: '行政许可',
      label: '许可',
      type: 'danger',
    },
    infoCategory: {
      id: 'MA',
      name: '基础信息',
      label: '基础',
      type: 'primary',
    },
    subjectCategory: [
      {
        id: 'MA',
        name: '自然人',
      },
      {
        id: 'MQ',
        name: '个体工商户',
      },
    ],
    dimension: {
      id: 'MA',
      name: '社会公开',
    },
    name: '张文',
    identify: '412819199409098765',
    expirationDate: 1618285917,
    expirationDateFormat: '2021-12-12',
    createTime: 1618300026,
    updateTime: 1618300026,
    updateTimeFormat: '2020年10月1日 17时44分',
    statusTime: 0,
    status: {
      id: 'Lw',
      name: '待确认',
      type: 'warning',
    },
    template: { // 资源目录
      id: '1', // 资源目录id（用于查看详情）
      name: '公证员信息',
      identify: 'GZYXX',
      sourceUnit: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
]

export const templateDetail = {
  id: 'MA',
  name: '登记信息',
  identify: 'DJXX',
  subjectCategory: [
    {
      id: 'MA',
      name: '法人及非法人组织',
    },
    {
      id: 'MQ',
      name: '自然人',
    },
  ],
  exchangeFrequency: {
    id: 'MA',
    name: '每日',
  },
  dimension: {
    id: 'MA',
    name: '社会公开',
  },
  infoClassify: {
    id: 'MA',
    name: '行政处罚',
  },
  infoCategory: {
    id: 'MA',
    name: '守信信息',
  },
  description: '目录描述信息',
  items: [
    {
      name: '主体名称',
      type: {
        id: 'MA',
        name: '字符型',
      },
      length: '200',
      options: [
        '信息分类',
      ],
      remarks: '信用主体名称',
      identify: 'ZTMC',
      isMasked: {
        id: 'Lw',
        name: '否',
      },
      maskRule: [
        '1',
        '2',
      ],
      dimension: {
        id: 'MA',
        name: '社会公开',
      },
      isNecessary: {
        id: 'MA',
        name: '是',
      },
    },
  ],
  sourceUnit: {
    id: 'MA',
    name: '发展和改革委员会',
  },
  gbTemplate: {
    id: 'MA',
    name: '登记信息',
  },
  updateTime: 1611822108,
  createTime: 1611822108,
}

export const templateVersionList = [
  {
    id: 'MA',
    version: '1-D012A100C101',
    description: '公厅关于运用太数据技术开展城市信用监测工作的通知》(发 改办财金 〔⒛16〕 14⒆ 号)有 关要求,我 委建立了全国 城市信用状况监测预警平台,对全国“1个县级以上城市开',
    updateTime: 1576317765,
    createTime: 1576317765,
    currentRelease: true,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
  {
    id: 'MQ',
    version: '2-D012A100C101',
    description: '2020年版',
    updateTime: 1576317785,
    createTime: 1576317785,
    currentRelease: false,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
  {
    id: 'Mg',
    version: '3-D012A100C101',
    description: '2021年版',
    updateTime: 1576317920,
    createTime: 1576317920,
    currentRelease: false,
    addItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    editItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    deleteItems: [
      {
        id: 'MA',
        name: '主体名称',
      },
      {
        id: 'MQ',
        name: '法定代表人（负责人） 证件类型',
      },
    ],
    attachment: {
      name: '登记机关(DJJG).xlsx',
      identify: '登记机关(DJJG).xlsx',
    },
    crew: {
      id: 'MA',
      realName: '平台管理员',
      userGroup: {
        id: 'MA',
        name: '发展和改革委员会',
      },
    },
  },
]
