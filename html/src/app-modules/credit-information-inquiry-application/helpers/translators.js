
/**
 * 基础规则详情
 */
export function translateBaseRuleDetail (res = {}) {
  const {
    sourceCategory = {},
    sourceTemplate = {},
    transformationCategory = {},
    transformationTemplate = {},
    content = {},
  } = res
  const { deDuplicationRule = {} } = content
  const transformationRule = {}
  const completionRule = {}
  const comparisonRule = {}

  if (Array.isArray(content.transformationRule)) {
    content.transformationRule.forEach(rule => {
      transformationRule[rule.target.identify] = rule.origin.identify
    })
  }

  if (Array.isArray(content.comparisonRule)) {
    content.comparisonRule.forEach(rule => {
      if (Array.isArray(rule) && rule.length) {
        const key = rule[0].target.identify

        comparisonRule[key] = rule.map(v => ({
          id: v.template.id,
          base: v.base.map(i => i.id),
          item: v.origin.identify,
        }))
      }
    })
  }

  if (Array.isArray(content.completionRule)) {
    content.completionRule.forEach(rule => {
      if (Array.isArray(rule) && rule.length) {
        const key = rule[0].target.identify

        completionRule[key] = rule.map(v => ({
          id: v.template.id,
          base: v.base.map(i => i.id),
          item: v.origin.identify,
        }))
      }
    })
  }

  return {
    sourceTemplateId: sourceTemplate.id,
    sourceTemplateCategory: sourceCategory.id,
    targetTemplateId: transformationTemplate.id,
    targetTemplateCategory: transformationCategory.id,
    transformationRule,
    completionRule,
    comparisonRule,
    deDuplicationRule: {
      result: deDuplicationRule.result && deDuplicationRule.result.id,
      items: deDuplicationRule.items.map(item => item.identify),
    },
  }
}
