import Base from '@/models/base'

class TemplateDataCount extends Base {
  constructor (params = {}) {
    super(params)
    const {
      incentiveTotal,
      licensingTotal,
      penaltyTotal,
      punishTotal,
      jointWarningTotal,
      focusTotal,
      enterprisePromiseTotal,
      contractPerformanceTotal,
      doubleRandomOneOpenTotal,
    } = params

    this.incentiveTotal = incentiveTotal
    this.licensingTotal = licensingTotal
    this.penaltyTotal = penaltyTotal
    this.punishTotal = punishTotal
    this.jointWarningTotal = jointWarningTotal
    this.focusTotal = focusTotal
    this.enterprisePromiseTotal = enterprisePromiseTotal
    this.contractPerformanceTotal = contractPerformanceTotal
    this.doubleRandomOneOpenTotal = doubleRandomOneOpenTotal
  }
}

export default TemplateDataCount
