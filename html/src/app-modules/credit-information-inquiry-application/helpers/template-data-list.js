import Base from '@/models/base'

class TemplateDataList extends Base {
  constructor (params = {}) {
    super(params)
    const {
      licensing = [],
      penalty = [],
      incentive = [],
      punish = [],
      jointWarning = [],
      focus = [],
      enterprisePromise = [],
      contractPerformance = [],
      doubleRandomOneOpen = [],
    } = params

    this.licensing = licensing
    this.penalty = penalty
    this.incentive = incentive
    this.punish = punish
    this.jointWarning = jointWarning
    this.focus = focus
    this.contractPerformance = contractPerformance
    this.enterprisePromise = enterprisePromise
    this.doubleRandomOneOpen = doubleRandomOneOpen
  }
}

export default TemplateDataList
