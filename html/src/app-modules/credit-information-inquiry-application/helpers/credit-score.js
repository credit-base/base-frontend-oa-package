import Base from '@/models/base'

class CreditScore extends Base {
  constructor (params = {}) {
    super(params)
    const {
      totalScore,
      grade,
    } = params

    this.totalScore = totalScore
    this.grade = grade
  }
}

export default CreditScore
