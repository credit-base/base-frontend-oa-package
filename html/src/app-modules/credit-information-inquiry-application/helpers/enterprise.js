import Base from '@/models/base'

class Enterprise extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      name,
      unifiedSocialCreditCode,
      registrationStatus,
      principal,
      principalCardId,
      establishmentDate,
      enterpriseType,
      enterpriseTypeCode,
      registrationAuthority,
      industryCategory,
      businessTermStart,
      businessTermTo,
      administrativeArea,
      updateTimeFormat,
    } = params

    this.id = id
    this.name = name
    this.unifiedSocialCreditCode = unifiedSocialCreditCode
    this.registrationStatus = registrationStatus
    this.principal = principal
    this.principalCardId = principalCardId
    this.establishmentDate = establishmentDate
    this.enterpriseType = enterpriseType
    this.enterpriseTypeCode = enterpriseTypeCode
    this.registrationAuthority = registrationAuthority
    this.industryCategory = industryCategory
    this.businessTermStart = businessTermStart
    this.businessTermTo = businessTermTo
    this.administrativeArea = administrativeArea
    this.updateTimeFormat = updateTimeFormat
  }
}

export default Enterprise
