import Base from '@/models/base'

class FinancialData extends Base {
  constructor (params = {}) {
    super(params)
    const {
      CWXX = [],
      FXXX = [],
      JYZK = [],
      QYNB = [],
      WXZC = [],
      YLXX = [],
    } = params

    this.CWXX = CWXX
    this.FXXX = FXXX
    this.JYZK = JYZK
    this.QYNB = QYNB
    this.WXZC = WXZC
    this.YLXX = YLXX
  }
}

export default FinancialData
