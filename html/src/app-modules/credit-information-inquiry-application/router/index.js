/**
 * @file 信用信息查询应用系统 - 路由
 * @module async-routes/credit-information-inquiry-application
 */

import Layout from '@/layout'
import permissions from '@/constants/permissions'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'
import baseRule from './base-rule'

const { STATUS_CREDIT_INFORMATION_INQUIRY_APPLICATION } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/credit-information-inquiry-application',
    name: 'CreditInformationInquiryApplication',
    component: Layout,
    alwaysShow: true,
    meta: {
      title: 'creditInformationInquiryApplication.ROUTER.CREDIT_INFORMATION_INQUIRY_APPLICATION',
      icon: 'app',
      breadcrumb: false,
      isVisible: STATUS_CREDIT_INFORMATION_INQUIRY_APPLICATION,
    },
    children: [
      // 企业管理
      {
        path: '/enterprise',
        name: 'EnterpriseList',
        component: () => import('@/app-modules/credit-information-inquiry-application/views/enterprise/list'),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.ENTERPRISE',
          icon: 'enterprise',
          routerId: permissions.ENTERPRISE_ID,
        },
      },
      {
        path: '/enterprise/detail/:id',
        name: 'EnterpriseDetail',
        component: () => import('@/app-modules/credit-information-inquiry-application/views/enterprise/detail'),
        hidden: true,
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.ENTERPRISE_DETAIL',
          icon: 'enterprise',
          activeMenu: '/creditInformationInquiryApplication',
        },
      },
      {
        path: `/enterprise/catalog/:id`,
        name: `EnterpriseCatalogDetail`,
        hidden: true,
        component: () => import(`@/app-modules/credit-information-inquiry-application/views/enterprise/data-detail`),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.CATALOG_DETAIL',
          icon: 'enterprise',
          activeMenu: '/creditInformationInquiryApplication',
        },
      },

      // 基础目录
      {
        path: '/template-management-subsystem',
        name: 'TemplateManagementSubsystem',
        alwaysShow: true,
        component: () => import('@/app-modules/credit-information-inquiry-application/views/base-templates/index'),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.TEMPLATE_MANAGEMENT_SUBSYSTEM',
          icon: 'resource-catalog-subsystem',
          routerId: permissions.BASE_TEMPLATE_ID,
        },
        children: [
          {
            path: '/template-management-subsystem/base-templates',
            name: 'BaseTemplateList',
            component: () => import('@/app-modules/credit-information-inquiry-application/views/base-templates/list'),
            meta: {
              title: 'creditInformationInquiryApplication.ROUTER.BASE_TEMPLATE',
              icon: 'gb-template',
              routerId: permissions.BASE_TEMPLATE_ID,
            },
          },
          {
            path: '/template-management-subsystem/base-templates/detail/:id',
            name: 'BaseTemplateDetail',
            hidden: true,
            component: () => import('@/app-modules/credit-information-inquiry-application/views/base-templates/detail'),
            meta: {
              title: 'creditInformationInquiryApplication.ROUTER.BASE_TEMPLATE_DETAIL',
              activeMenu: '/creditInformationInquiryApplication',
            },
          },
          {
            path: '/template-management-subsystem/base-templates/edit/:id',
            name: 'BaseTemplateEdit',
            hidden: true,
            component: () => import('@/app-modules/credit-information-inquiry-application/views/base-templates/edit'),
            meta: {
              title: 'creditInformationInquiryApplication.ROUTER.BASE_TEMPLATE_EDIT',
              activeMenu: '/creditInformationInquiryApplication',
            },
          },
        ],
      },

      // 基础规则
      ...baseRule,
    ],
  },
]
