/**
 * @file 基础规则路由
 * @module router/base-rule
 */

import permissions from '@/constants/permissions'

export default [
  {
    path: `/rule-management-subsystem`,
    name: `RuleManagementSubsystem`,
    alwaysShow: true,
    component: () => import('@/app-modules/credit-information-inquiry-application/views/base-rule'),
    meta: {
      title: 'creditInformationInquiryApplication.ROUTER.RULE_MANAGEMENT_SUBSYSTEM',
      icon: 'rule-manage-subsystem',
      routerId: permissions.BASE_RULE_ID,
      breadcrumb: false,
    },
    children: [
      // 基础规则管理
      {
        path: '/rule-management-subsystem/released-base-rule/list',
        name: 'ReleasedBaseRuleList',
        component: () => import('@/app-modules/credit-information-inquiry-application/views/base-rule/release/list'),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.BASE_RULE',
          icon: 'gb-template',
          routerId: permissions.BASE_RULE_ID,
        },
      },
      {
        path: '/rule-management-subsystem/released-base-rule/detail/:id',
        name: 'ReleasedBaseRuleDetail',
        hidden: true,
        component: () => import('@/app-modules/credit-information-inquiry-application/views/base-rule/release/detail'),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.BASE_RULE_DETAIL',
          activeMenu: '/rule-management-subsystem/released-base-rule/list',
        },
      },
      {
        path: '/rule-management-subsystem/released-base-rule/unaudited-detail/:id',
        name: 'ReleasedBaseRuleUnauditedDetail',
        hidden: true,
        component: () => import('@/app-modules/credit-information-inquiry-application/views/base-rule/release/unaudited-detail'),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.BASE_RULE_UNAUDITED_DETAIL',
          activeMenu: '/rule-management-subsystem/released-base-rule/list',
        },
      },
      {
        path: '/rule-management-subsystem/released-base-rule/add',
        name: 'ReleasedBaseRuleAdd',
        hidden: true,
        component: () => import('@/app-modules/credit-information-inquiry-application/views/base-rule/release/add'),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.BASE_RULE_ADD',
          activeMenu: '/rule-management-subsystem/released-base-rule/list',
        },
      },
      {
        path: '/rule-management-subsystem/released-base-rule/edit/:id',
        name: 'ReleasedBaseRuleEdit',
        hidden: true,
        component: () => import('@/app-modules/credit-information-inquiry-application/views/base-rule/release/edit'),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.BASE_RULE_EDIT',
          activeMenu: '/rule-management-subsystem/released-base-rule/list',
        },
      },
      {
        path: '/rule-management-subsystem/released-base-rule/unaudited-edit/:id',
        name: 'ReleasedBaseRuleUnauditedEdit',
        hidden: true,
        component: () => import('@/app-modules/credit-information-inquiry-application/views/base-rule/release/unaudited-edit'),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.BASE_RULE_UNAUDITED_EDIT',
          activeMenu: '/rule-management-subsystem/released-base-rule/list',
        },
      },

      // 基础规则审核
      {
        path: '/rule-management-subsystem/reviewed-base-rule/list',
        name: 'ReviewedBaseRuleList',
        component: () => import('@/app-modules/credit-information-inquiry-application/views/base-rule/review/list'),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.BASE_RULE_REVIEW_LIST',
          icon: 'gb-template',
          activeMenu: '/rule-management-subsystem/reviewed-base-rule/list',
        },
      },
      {
        path: '/rule-management-subsystem/reviewed-base-rule/detail/:id',
        name: 'ReviewedBaseRuleDetail',
        hidden: true,
        component: () => import('@/app-modules/credit-information-inquiry-application/views/base-rule/review/detail'),
        meta: {
          title: 'creditInformationInquiryApplication.ROUTER.BASE_RULE_REVIEW_DETAIL',
          activeMenu: '/rule-management-subsystem/reviewed-base-rule/list',
        },
      },
    ],
  },
]
