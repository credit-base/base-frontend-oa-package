/**
 * @file 企业接口
 */

import request from '@/utils/request'

/**
 * 获取企业列表
 */
export function fetchEnterpriseList (params = {}) {
  return request(`/api/enterprises`, { params })
}

/**
 * 获取企业详情
 * @param {*} id
 * @param {*} params
 */
export function fetchEnterpriseDetail (id, params = {}) {
  return request(`/api/enterprises/${id}`, { params })
}

export function fetchTemplateList (params = {}) {
  return request(`/api/commonSearchData`, { params })
}

export function fetchTemplateDetail (id, params = {}) {
  return request(`/api/commonTemplates/${id}`, { params })
}

export function fetchDataDetail (id) {
  return request(`/api/commonSearchData/${id}`)
}
