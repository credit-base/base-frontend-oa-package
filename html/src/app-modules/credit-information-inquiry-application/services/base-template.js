/**
 * @file 基础资源目录
 */

import request from '@/utils/request'
import {
  templateVersionList,
} from '../helpers/mocks'

/**
 * 编辑基础资源目录
 * @param {*} id
 * @param {*} params
 */
export function updateBaseTemplate (id, params = {}) {
  return request(`/api/baseTemplates/${id}/edit`, { method: 'POST', params })
}

/**
 * 获取基础资源目录列表
 * @param {*} params
 * * params.limit
 * * params.page
 * * params.name
 * * params.sort updateTime | -updateTime | ''
 */
export function fetchBaseTemplateList (params = {}) {
  return request(`/api/baseTemplates`, { params })
}

/**
 * 获取基础资源目录详情
 * @param {*} id
 */
export function fetchBaseTemplateDetail (id) {
  return request(`/api/baseTemplates/${id}`)
}

/**
 * 获取编辑基础资源目录信息
 * @param {*} id
 */
export function fetchTemplateEditInfo (id) {
  return request(`/api/baseTemplates/${id}`)
}

/**
 * 获取资源目录初始化配置
 */
export function fetchTemplateConfigs (params = {}) {
  return request(`/api/template/configure`, { params })
}

/**
 * 获取资源目录版本信息
 * @param {*} id
 * @param {*} params
 */
export function fetchTemplateVersionList (id, params) {
  if (params.useMock) {
    return {
      list: [
        ...templateVersionList,
      ],
      total: 1,
    }
  }
  return request(`/api/gbTemplates/${id}/versions`, params)
}
