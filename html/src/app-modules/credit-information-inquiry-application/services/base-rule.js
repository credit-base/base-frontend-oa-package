/**
 * @file 基础规则目录
 * @module services/base-rule
 */

import request from '@/utils/request'

/**
 * 基础规则列表
 */
export function getBaseRuleList (params = {}) {
  return request(`/api/baseRules`, { params })
}

/**
 * 基础规则详情
 * @param {string} id 规则id
 */
export function getBaseRuleDetail (id, params = {}) {
  return request(`/api/baseRules/${id}`, { params })
}

/**
 * 新增基础规则
 */
export function addBaseRule (params = {}) {
  return request(`/api/baseRules/add`, { method: 'POST', params })
}

/**
 * 编辑基础规则
 * @param {string} id 规则id
 */
export function editBaseRule (id, params = {}) {
  return request(`/api/baseRules/${id}/edit`, { method: 'POST', params })
}

/**
 * 删除基础规则
 * @param {string} id 规则id
 */
export function deleteBaseRule (id, params = {}) {
  return request(`/api/baseRules/${id}/delete`, { method: 'POST', params })
}

/**
 * 未审核基础规则列表
 */
export function getUnAuditedBaseRuleList (params = {}) {
  return request(`/api/unAuditedBaseRules`, { params })
}

/**
 * 未审核基础规则详情
 * @param {string} id 规则id
 */
export function getUnAuditedBaseRuleDetail (id, params = {}) {
  return request(`/api/unAuditedBaseRules/${id}`, { params })
}

/**
 * 编辑未审核基础规则
 * @param {string} id 规则id
 */
export function editUnAuditedBaseRule (id, params = {}) {
  return request(`/api/unAuditedBaseRules/${id}/edit`, { method: 'POST', params })
}

/**
 * 通过未审核基础规则
 * @param {string} id 规则id
 */
export function approveUnAuditedBaseRule (id, params = {}) {
  return request(`/api/unAuditedBaseRules/${id}/approve`, { method: 'POST', params })
}

/**
 * 驳回未审核基础规则
 * @param {string} id 规则id
 */
export function rejectUnAuditedBaseRule (id, params = {}) {
  return request(`/api/unAuditedBaseRules/${id}/reject`, { method: 'POST', params })
}

/**
 * 撤销未审核基础规则
 * @param {string} id 规则id
 */
export function revokeUnAuditedBaseRule (id, params = {}) {
  return request(`/api/unAuditedBaseRules/${id}/revoke`, { method: 'POST', params })
}
