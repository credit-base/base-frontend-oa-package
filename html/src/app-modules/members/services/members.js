import { membersList, membersDetail } from '../helpers/mocks'
/**
 * 接口 - 用户认证
 *
 * @module services/member
 */
import request from '@/utils/request'

/**
 * 1. 获取用户列表
 * @param {page, limit, sort, realName, cellphone, status} params
 * @returns
 */
export function fetchMembers (params = {}) {
  if (params.useMock) {
    return {
      total: 1,
      list: [
        ...membersList,
      ],
    }
  }
  return request('/api/members', { method: 'GET', params })
}

/**
 * 获取用户详情
 * @param {*} id
 * @param {*} params
 * @returns
 */
export function fetchMembersDetail (id, params = {}) {
  if (params.useMock) {
    return {
      ...membersDetail,
    }
  }

  return request(`/api/members/${id}`)
}

/**
 * 启用/禁用用户
 * @param {*} id
 * @param {*} type enable/disable
 * @returns
 */
export function updateMemberStatus (id, type) {
  return request(`/api/members/${id}/${type}`, { method: 'POST' })
}
