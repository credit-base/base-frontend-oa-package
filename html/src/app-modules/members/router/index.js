/**
 * @file 新闻 - 路由
 * @module async-routes/members
 */

import Layout from '@/layout'
import permissions from '@/constants/permissions'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const { STATUS_MEMBER } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/members',
    name: 'Members',
    component: Layout,
    alwaysShow: true,
    meta: {
      title: 'members.ROUTER.MEMBERS',
      icon: 'members',
      breadcrumb: false,
      isVisible: STATUS_MEMBER,
    },
    children: [
      {
        path: '/members/list',
        name: 'MembersList',
        component: () => import('@/app-modules/members/views/list'),
        meta: {
          title: 'members.ROUTER.MEMBERS_LIST',
          icon: 'members',
          routerId: permissions.MEMBER_ID,
        },
      },
      {
        path: '/members/detail/:id',
        name: 'MembersDetail',
        component: () => import('@/app-modules/members/views/detail'),
        hidden: true,
        meta: {
          title: 'members.ROUTER.MEMBERS_DETAIL',
          icon: 'members',
          activeMenu: '/members/list',
        },
      },
    ],
  },
]
