import Members from './members'

/**
 * 用户列表翻译
 * @param {*} res
 * @returns
 */
export function translateMembersList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Members(item)
      .get([
        'id',
        'realName',
        'cellphone',
        'gender',
        'status',
        'updateTimeFormat',
      ]))
    : []

  return {
    list: tempList,
    total,
  }
}

/**
 * 用户详情翻译
 * @param {*} res
 */
export function translateMembersDetail (res = {}) {
  return new Members(res)
    .get([
      'id',
      'realName',
      'cellphone',
      'gender',
      'cardId',
      'email',
      'contactAddress',
      'status',
      'updateTimeFormat',
    ])
}
