import Base from '@/models/base'

class Members extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      userName,
      realName,
      cellphone,
      cardId,
      contactAddress,
      email,
      gender = {},
      status = {},
      updateTime,
      updateTimeFormat,
    } = params

    this.id = id
    this.userName = userName
    this.realName = realName
    this.cellphone = cellphone
    this.cardId = cardId
    this.contactAddress = contactAddress
    this.email = email
    this.gender = gender.name
    this.status = status
    this.updateTime = updateTime
    this.updateTimeFormat = updateTimeFormat
  }
}

export default Members
