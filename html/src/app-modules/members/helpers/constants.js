import { CONSTANTS } from '@/plugins/constants'

const {
  STATUS = {},
} = CONSTANTS

export const SEARCH_TYPE = [
  {
    label: '姓名',
    value: 'realName',
  },
  {
    label: '手机号',
    value: 'cellphone',
  },
]

export const STATUS_ENUM = [
  {
    text: '启用',
    value: STATUS.ENABLE,
  },
  {
    text: '禁用',
    value: STATUS.DISABLE,
  },
]
