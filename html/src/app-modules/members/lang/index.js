import {
  SU_SUBMIT,
  COMMON_PLACEHOLDER_INPUT,
} from '@/i18n/lang/zh/common-message'

const msgMembersTitle = (title) => `${title}用户`
const msgMembersContent = (title) => `${title}该用户后，该门户网用户账号将可登录并可进行相关操作，您确定${title}该用户吗？`
const msgMembersCancelContent = (title) => `${title}该用户后，该门户网用户账号将不可登录或进行相关操作，您确定${title}该用户吗？`
export default {
  ROUTER: {
    MEMBERS: '门户网用户管理',
    MEMBERS_LIST: '门户网用户管理列表',
    MEMBERS_DETAIL: '门户网用户管理详情',
  },
  LABEL: {
    REAL_NAME: '姓名',
    CELLPHONE: '手机号',
    GENDER: '性别',
    CARD_ID: '身份证号',
    EMAIL: '邮箱',
    CONTACT_ADDRESS: '联系地址',
  },
  PLACEHOLDER: {
    REAL_NAME: COMMON_PLACEHOLDER_INPUT('姓名'),
    CELLPHONE: COMMON_PLACEHOLDER_INPUT('手机号'),
  },
  TIP: {
    TITLE_MEMBERS_ENABLE: msgMembersTitle('启用'),
    CONTENT_MEMBERS_ENABLE: msgMembersContent('启用'),
    RESET_ENABLE_SUCCESS: SU_SUBMIT(),

    TITLE_MEMBERS_DISABLE: msgMembersTitle('禁用'),
    CONTENT_MEMBERS_DISABLE: msgMembersCancelContent('禁用'),
    RESET_DISABLE_SUCCESS: SU_SUBMIT(),
  },
}
