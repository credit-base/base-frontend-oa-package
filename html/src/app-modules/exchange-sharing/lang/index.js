/**
 * @file 共享规则管理 lang
 * @module exchange-sharing/lang
 */

export default {
  ROUTER: {
    EXCHANGE_SHARING_SUBSYSTEM: '前置机交换子系统',
    EXCHANGE_SHARING_TEMPLATES: '前置机数据管理',
    EXCHANGE_SHARING_RULES_DATA: '前置机数据列表',
    EXCHANGE_SHARING_DATA_DETAIL: '前置机数据详情',
    EXCHANGE_SHARING_RULES_DETAIL: '规则详情',
    CURRENT_UNIT_RESOURCE_CATALOG_ADD: '设置规则',
    EXCHANGE_SHARING_RULES: '共享规则管理',
    EXCHANGE_SHARING_DATA: '共享规则管理',
  },
  LIST: {
    RULE_NAME: '规则名称',
    USERNAME: '员工名',
    IP: '白名单',
    NAME: '主体名称（企业名称/姓名）',
    IDENTIFY: '主体代码（统一社会信用代码/身份证号）',
    STATUS: '状态',
  },
  DETAIL: {
    RULE_NAME: '规则名称',
    USERNAME: '员工名',
    IP: '白名单',
  },
  PLACEHOLDER: {
    TEMPLATE_NAME: '请输入资源目录名称',
    RULES_NAME: '请输入规则名称',
    USERNAME: '请输入员工名',
    PASSWORD: '请输入密码',
    IP: '请输入白名单',
    SUBJECT_NAME_AND_IDENTIFY: '请输入主体名称/主体代码',
    START_TIME: '请选择开始时间',
    END_TIME: '请选择结束时间',
  },
  ACTION: {
    ADD_RULE: '设置规则',
    EXPORT_DATA: '导出数据',
  },
  LABEL: {
    RULES_NAME: '规则名称',
    USERNAME: '员工名',
    PASSWORD: '密码',
    IP: '白名单',
  },
  STATISTICS_MODULE: {
    DATA_VOLUME: '共享数据量',
    DATA_TASK: '共享任务',
  },
  TAB: {
    STATISTIC_CHART: '数据统计图',
    DATA_LIST: '数据列表',
    TASK_LIST: '任务列表',
    EXPORT_TASK_LIST: '导出任务列表',
    YS_TEMPLATE_LIST: '前置机原始数据列表',
    WBJ_TEMPLATE_LIST: '前置机委办局数据列表',
    GB_TEMPLATE_LIST: '前置机国标数据列表',
    BJ_TEMPLATE_LIST: '前置机本级数据列表',
    DATA_FLOW_CHART: '数据溯源图',
    CATALOG_OVERVIEW: '数据总览',
    STATISTICS_GRAPH: '数据统计图',
    LIST_OF_RULES: '规则列表',
  },
  TABLE: {
    DATA_SUPPLIER: '数据提供方',
    DATA_COUNT: '数据量',
    SUCCESS_COUNT: '成功数',
    START_TIME: '开始时间',
    END_TIME: '终结时间',
    USER_NAME: '员工姓名',
    SUBJECT_NAME: '主体名称(企业名称/姓名)',
    SUBJECT_IDENTIFY: '主体代码(统一社会信用代码/身份证号)',
    STORAGE_TIME: '入库时间',
  },
}
