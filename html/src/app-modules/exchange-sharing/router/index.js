/**
 * @file 数据管理 - 路由
 * @module async-routes/exchange-sharing
 */

import exchangeSharingRules from './rules'
import frontEndDataManagement from './data'
import { ROUTER_VISIBLE_CONFIG } from '@/settings'

const { STATUS_EXCHANGE_SHARING_SUBSYSTEM } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/exchange-sharing-subsystem',
    name: 'ExchangeSharingSubsystem',
    component: () => import('@/app-modules/exchange-sharing/views'),
    alwaysShow: true,
    meta: {
      title: 'exchangeSharing.ROUTER.EXCHANGE_SHARING_SUBSYSTEM',
      icon: 'exchange-sharing-subsystem',
      breadcrumb: false,
      isVisible: STATUS_EXCHANGE_SHARING_SUBSYSTEM,
    },
    children: [
      ...frontEndDataManagement,
      ...exchangeSharingRules,
    ],
  },
]
