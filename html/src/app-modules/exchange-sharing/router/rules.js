/**
 * @file 前置机交换子系统 - 共享规则管理
 * @module router/rules.js
 */
import permissions from '@/constants/permissions'

export default [
  // 交换共享规则列表
  {
    path: '/exchange-sharing-rules/templates',
    name: 'ExchangeSharingRulesTemplates',
    meta: {
      title: 'exchangeSharing.ROUTER.EXCHANGE_SHARING_RULES',
      icon: 'exchange-sharing-rules',
      routerId: permissions.EXCHANGE_SHARING_RULE_ID,
    },
    component: () => import('@/app-modules/exchange-sharing/views/list/exchange-sharing-rules-templates'),
  },
  // 交换共享规则列表
  {
    path: '/exchange-sharing-rules/data/:templateId',
    name: 'ExchangeSharingRulesList',
    hidden: true,
    meta: {
      title: 'exchangeSharing.ROUTER.EXCHANGE_SHARING_RULES_DATA',
      icon: 'exchange-sharing-rules',
      activeMenu: '/exchange-sharing-rules/templates',
    },
    component: () => import('@/app-modules/exchange-sharing/views/list/exchange-sharing-rules-data'),
  },
  // 交换共享规则详情
  {
    path: '/exchange-sharing-rules/data/detail/:templateId/:id',
    name: 'ExchangeSharingRulesDetail',
    hidden: true,
    meta: {
      title: 'exchangeSharing.ROUTER.EXCHANGE_SHARING_RULES_DETAIL',
      icon: 'exchange-sharing-rules',
      activeMenu: '/exchange-sharing-rules/templates',
    },
    component: () => import('@/app-modules/exchange-sharing/views/detail/exchange-sharing-rules-data'),
  },
  // 新增交换共享规则
  {
    path: '/exchange-sharing-rules/add/:templateId',
    name: 'ExchangeSharingRulesAdd',
    hidden: true,
    meta: {
      title: 'exchangeSharing.ROUTER.EXCHANGE_SHARING_DATA_DETAIL',
      icon: 'exchange-sharing-rules',
      activeMenu: '/exchange-sharing-rules/templates',
    },
    component: () => import('@/app-modules/exchange-sharing/views/add/exchange-sharing-rules'),
  },
  // 编辑交换共享规则
  {
    path: '/exchange-sharing-rules/edit/:templateId/:id',
    name: 'ExchangeSharingRulesEdit',
    hidden: true,
    meta: {
      title: 'exchangeSharing.ROUTER.EXCHANGE_SHARING_DATA_DETAIL',
      icon: 'exchange-sharing-rules',
      activeMenu: '/exchange-sharing-rules/templates',
    },
    component: () => import('@/app-modules/exchange-sharing/views/edit/exchange-sharing-rules'),
  },
]
