/**
 * @file 前置机交换子系统 - 前置机数据管理
 * @module router/data.js
 */
import permissions from '@/constants/permissions'

export default [
  // 前置机资源目录列表
  {
    path: '/front-end-data-management/templates',
    name: 'ExchangeSharingTemplates',
    meta: {
      title: 'exchangeSharing.ROUTER.EXCHANGE_SHARING_TEMPLATES',
      icon: 'front-end-data-management',
      routerId: permissions.FRONT_END_DATA_MANAGEMENT_ID,
    },
    component: () => import('@/app-modules/exchange-sharing/views/list/front-end-data-management-templates'),
  },
  // 前置机数据列表
  {
    path: '/front-end-data-management/data/:id',
    name: 'ExchangeSharingData',
    hidden: true,
    meta: {
      title: 'exchangeSharing.ROUTER.EXCHANGE_SHARING_DATA',
      icon: 'front-end-data-management',
      activeMenu: '/front-end-data-management/templates',
    },
    component: () => import('@/app-modules/exchange-sharing/views/list/front-end-data-management-data'),
  },
  // 前置机数据详情
  {
    path: '/front-end-data-management/data/detail/:id',
    name: 'ExchangeSharingDataDetail',
    hidden: true,
    meta: {
      title: 'exchangeSharing.ROUTER.EXCHANGE_SHARING_DATA_DETAIL',
      icon: 'front-end-data-management',
      activeMenu: '/front-end-data-management/templates',
    },
    component: () => import('@/app-modules/exchange-sharing/views/detail/front-end-data-management-data'),
  },
]
