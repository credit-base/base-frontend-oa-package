/* eslint-disable max-lines */
/**
 * @file 前置机交换子系统 - mock数据
 * @module exchange-sharing/helpers/mocks
 */

export const templateDataList = [
  {
    id: 'MA',
    name: 'e租出租公司',
    identify: '232523652365236520',
    status: {
      id: 'LC0',
      name: '屏蔽',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'MQ',
    name: '中研社食品有限责任公司',
    identify: '91120104MA06FBJY36',
    status: {
      id: 'Lw',
      name: '正常',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mg',
    name: '老百姓大药房',
    identify: '524125632541236523',
    status: {
      id: 'Lw',
      name: '正常',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mw',
    name: '博源电器公司',
    identify: '91120104MA06FBJY36',
    status: {
      id: 'Lw',
      name: '正常',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'NA',
    name: '锦鹏商贸有限责任公司',
    identify: '91511502692270078T',
    status: {
      id: 'Lw',
      name: '正常',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'NQ',
    name: '宏益汽车销售服务有限公司',
    identify: '91511502692270078T',
    status: {
      id: 'Lw',
      name: '正常',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Ng',
    name: '青清废弃物治理有限责任公司',
    identify: '91511502772961282D',
    status: {
      id: 'Lw',
      name: '正常',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Nw',
    name: '均田坝石厂',
    identify: '91511524793962348Y',
    status: {
      id: 'Lw',
      name: '正常',
    },
    updateTime: 1576317765,
    createTime: 1576317765,
  },
]

export const ysTemplateList = [
  {
    id: 'MA',
    template: {
      id: 'MA',
      name: '登记信息',
      identify: 'DJXX',
      exchangeFrequency: {
        id: 'MA',
        name: '实时',
      },
      subjectCategory: [
        {
          id: 'MA',
          name: '自然人',
        },
        {
          id: 'MQ',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'MA',
        name: '社会公开',
      },
      infoClassify: {
        id: 'MA',
        name: '行政许可',
      },
    },
    templateType: {
      id: 'MA',
      name: '原始',
    },
    sourceUnit: {
      id: 'MA',
      name: 'XX市税务局',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
      {
        id: 'MQ',
        name: '规则名称',
      },
      {
        id: 'Mg',
        name: '规则名称',
      },
    ],
    exchangeFrequency: {
      id: 'MQ',
      name: '每日',
    },
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'MQ',
    template: {
      id: 'MQ',
      name: '变更登记信息',
      identify: 'BGDJXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'MQ',
        name: '每日',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'MQ',
        name: '政务共享',
      },
      infoClassify: {
        id: 'MQ',
        name: '行政处罚',
      },
    },
    templateType: {
      id: 'MA',
      name: '原始',
    },
    sourceUnit: {
      id: 'MQ',
      name: 'XX市海关',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mg',
    template: {
      id: 'Mg',
      name: '股权结构信息',
      identify: 'GQJGXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'Mg',
        name: '每周',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'Mg',
        name: '红名单',
      },
    },
    templateType: {
      id: 'MA',
      name: '原始',
    },
    sourceUnit: {
      id: 'Mg',
      name: 'XX市市场监督管理局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mw',
    template: {
      id: 'Mw',
      name: '分支机构信息',
      identify: 'FZJGXX',
      reportStatus: {
        id: 'Lw',
        name: '未上报',
      },
      exchangeFrequency: {
        id: 'NA',
        name: '每季度',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'Mw',
        name: '黑名单',
      },
    },
    templateType: {
      id: 'MA',
      name: '原始',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'NQ',
    template: {
      id: 'NQ',
      name: '高管人员信息',
      identify: 'GGRYXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'NQ',
        name: '每半年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'MA',
      name: '原始',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Ng',
    template: {
      id: 'Ng',
      name: '企业年报信息',
      identify: 'QYNBXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'Ng',
        name: '每一年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'MA',
      name: '原始',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'OA',
    template: {
      id: 'OA',
      name: '欠税信息',
      identify: 'QSXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'OA',
        name: '每一年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'MA',
      name: '原始',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
]

export const wbjTemplateList = [
  {
    id: 'MA',
    template: {
      id: 'MA',
      name: '登记信息',
      identify: 'DJXX',
      exchangeFrequency: {
        id: 'MA',
        name: '实时',
      },
      subjectCategory: [
        {
          id: 'MA',
          name: '自然人',
        },
        {
          id: 'MQ',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'MA',
        name: '社会公开',
      },
      infoClassify: {
        id: 'MA',
        name: '行政许可',
      },
    },
    templateType: {
      id: 'MQ',
      name: '委办局',
    },
    sourceUnit: {
      id: 'MA',
      name: 'XX市税务局',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
      {
        id: 'MQ',
        name: '规则名称',
      },
      {
        id: 'Mg',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'MQ',
    template: {
      id: 'MQ',
      name: '变更登记信息',
      identify: 'BGDJXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'MQ',
        name: '每日',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'MQ',
        name: '政务共享',
      },
      infoClassify: {
        id: 'MQ',
        name: '行政处罚',
      },
    },
    templateType: {
      id: 'MQ',
      name: '委办局',
    },
    sourceUnit: {
      id: 'MQ',
      name: 'XX市海关',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mg',
    template: {
      id: 'Mg',
      name: '股权结构信息',
      identify: 'GQJGXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'Mg',
        name: '每周',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'Mg',
        name: '红名单',
      },
    },
    templateType: {
      id: 'MQ',
      name: '委办局',
    },
    sourceUnit: {
      id: 'Mg',
      name: 'XX市市场监督管理局',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mw',
    template: {
      id: 'Mw',
      name: '分支机构信息',
      identify: 'FZJGXX',
      reportStatus: {
        id: 'Lw',
        name: '未上报',
      },
      exchangeFrequency: {
        id: 'NA',
        name: '每季度',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'Mw',
        name: '黑名单',
      },
    },
    templateType: {
      id: 'MQ',
      name: '委办局',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'NQ',
    template: {
      id: 'NQ',
      name: '高管人员信息',
      identify: 'GGRYXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'NQ',
        name: '每半年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'MQ',
      name: '委办局',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Ng',
    template: {
      id: 'Ng',
      name: '企业年报信息',
      identify: 'QYNBXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'Ng',
        name: '每一年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'MQ',
      name: '委办局',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'OA',
    template: {
      id: 'OA',
      name: '欠税信息',
      identify: 'QSXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'OA',
        name: '每一年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'MQ',
      name: '委办局',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
]

export const gbTemplateList = [
  {
    id: 'MA',
    template: {
      id: 'MA',
      name: '登记信息',
      identify: 'DJXX',
      exchangeFrequency: {
        id: 'MA',
        name: '实时',
      },
      subjectCategory: [
        {
          id: 'MA',
          name: '自然人',
        },
        {
          id: 'MQ',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'MA',
        name: '社会公开',
      },
      infoClassify: {
        id: 'MA',
        name: '行政许可',
      },
    },
    templateType: {
      id: 'Mg',
      name: '国标',
    },
    sourceUnit: {
      id: 'MA',
      name: 'XX市税务局',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
      {
        id: 'MQ',
        name: '规则名称',
      },
      {
        id: 'Mg',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'MQ',
    template: {
      id: 'MQ',
      name: '变更登记信息',
      identify: 'BGDJXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'MQ',
        name: '每日',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'MQ',
        name: '政务共享',
      },
      infoClassify: {
        id: 'MQ',
        name: '行政处罚',
      },
    },
    templateType: {
      id: 'Mg',
      name: '国标',
    },
    sourceUnit: {
      id: 'MQ',
      name: 'XX市海关',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mg',
    template: {
      id: 'Mg',
      name: '股权结构信息',
      identify: 'GQJGXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'Mg',
        name: '每周',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'Mg',
        name: '红名单',
      },
    },
    templateType: {
      id: 'Mg',
      name: '国标',
    },
    sourceUnit: {
      id: 'Mg',
      name: 'XX市市场监督管理局',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mw',
    template: {
      id: 'Mw',
      name: '分支机构信息',
      identify: 'FZJGXX',
      reportStatus: {
        id: 'Lw',
        name: '未上报',
      },
      exchangeFrequency: {
        id: 'NA',
        name: '每季度',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'Mw',
        name: '黑名单',
      },
    },
    templateType: {
      id: 'Mg',
      name: '国标',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'NQ',
    template: {
      id: 'NQ',
      name: '高管人员信息',
      identify: 'GGRYXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'NQ',
        name: '每半年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'Mg',
      name: '国标',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Ng',
    template: {
      id: 'Ng',
      name: '企业年报信息',
      identify: 'QYNBXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'Ng',
        name: '每一年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'Mg',
      name: '国标',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'OA',
    template: {
      id: 'OA',
      name: '欠税信息',
      identify: 'QSXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'OA',
        name: '每一年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'Mg',
      name: '国标',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
]

export const bjTemplateList = [
  {
    id: 'MA',
    template: {
      id: 'MA',
      name: '登记信息',
      identify: 'DJXX',
      exchangeFrequency: {
        id: 'MA',
        name: '实时',
      },
      subjectCategory: [
        {
          id: 'MA',
          name: '自然人',
        },
        {
          id: 'MQ',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'MA',
        name: '社会公开',
      },
      infoClassify: {
        id: 'MA',
        name: '行政许可',
      },
    },
    templateType: {
      id: 'Mw',
      name: '本级',
    },
    sourceUnit: {
      id: 'MA',
      name: 'XX市税务局',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
      {
        id: 'MQ',
        name: '规则名称',
      },
      {
        id: 'Mg',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'MQ',
    template: {
      id: 'MQ',
      name: '变更登记信息',
      identify: 'BGDJXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'MQ',
        name: '每日',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'MQ',
        name: '政务共享',
      },
      infoClassify: {
        id: 'MQ',
        name: '行政处罚',
      },
    },
    templateType: {
      id: 'Mw',
      name: '本级',
    },
    sourceUnit: {
      id: 'MQ',
      name: 'XX市海关',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mg',
    template: {
      id: 'Mg',
      name: '股权结构信息',
      identify: 'GQJGXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'Mg',
        name: '每周',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'Mg',
        name: '红名单',
      },
    },
    templateType: {
      id: 'Mw',
      name: '本级',
    },
    sourceUnit: {
      id: 'Mg',
      name: 'XX市市场监督管理局',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mw',
    template: {
      id: 'Mw',
      name: '分支机构信息',
      identify: 'FZJGXX',
      reportStatus: {
        id: 'Lw',
        name: '未上报',
      },
      exchangeFrequency: {
        id: 'NA',
        name: '每季度',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'Mw',
        name: '黑名单',
      },
    },
    templateType: {
      id: 'Mw',
      name: '本级',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [
      {
        id: 'MA',
        name: '规则名称',
      },
    ],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'NQ',
    template: {
      id: 'NQ',
      name: '高管人员信息',
      identify: 'GGRYXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'NQ',
        name: '每半年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'Mw',
      name: '本级',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Ng',
    template: {
      id: 'Ng',
      name: '企业年报信息',
      identify: 'QYNBXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'Ng',
        name: '每一年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'Mw',
      name: '本级',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'OA',
    template: {
      id: 'OA',
      name: '欠税信息',
      identify: 'QSXX',
      reportStatus: {
        id: 'MA',
        name: '已上报',
      },
      exchangeFrequency: {
        id: 'OA',
        name: '每一年',
      },
      subjectCategory: [
        {
          id: 'Mg',
          name: '个体工商户',
        },
      ],
      dimension: {
        id: 'Mg',
        name: '授权查询',
      },
      infoClassify: {
        id: 'NA',
        name: '其他信息',
      },
    },
    templateType: {
      id: 'Mw',
      name: '本级',
    },
    sourceUnit: {
      id: 'Ng',
      name: 'XX市财政局',
    },
    rules: [],
    total: 100,
    updateTime: 1576317765,
    createTime: 1576317765,
  },
]

export const templateDetail = {
  id: 'MA',
  name: '登记信息',
  identify: 'DJXX',
  exchangeFrequency: {
    id: 'MA',
    name: '实时',
  },
  subjectCategory: [
    {
      id: 'MA',
      name: '自然人',
    },
    {
      id: 'MQ',
      name: '个体工商户',
    },
  ],
  dimension: {
    id: 'MA',
    name: '社会公开',
  },
  infoClassify: {
    id: 'MA',
    name: '行政许可',
  },
  sourceUnit: {
    id: 'Ng',
    name: 'XX市财政局',
  },
  updateTime: 1576317765,
  createTime: 1576317765,
}

export const rulesList = [
  {
    id: 'MA',
    name: '规则名称',
    username: 'fgw',
    ip: '65.23.15.27, 18.234.35.144',
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'MQ',
    name: '规则名称',
    username: 'syz',
    ip: '65.23.15.27, 18.234.35.144',
    updateTime: 1576317765,
    createTime: 1576317765,
  },
  {
    id: 'Mg',
    name: '规则名称',
    username: 'sqzj',
    ip: '65.23.15.27, 18.234.35.144',
    updateTime: 1576317765,
    createTime: 1576317765,
  },
]

export const rulesDetail = {
  id: 'MA',
  name: '规则名称',
  username: 'fgw',
  ip: '65.23.15.27, 18.234.35.144',
  template: [
    {
      id: 'MA',
      name: '登记信息',
    },
    {
      id: 'MQ',
      name: '变更登记信息',
    },
    {
      id: 'Mg',
      name: '股权结构信息',
    },
    {
      id: 'Mw',
      name: '分支机构信息',
    },
  ],
  updateTime: 1576317765,
  createTime: 1576317765,
}
