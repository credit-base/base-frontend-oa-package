/**
 * @file 前置机交换子系统 - 常量
 * @module exchange-sharing/helpers/constants
 */

export const EXCHANGE_SHARING_RULES_TEMPLATE_TABS = [
  { name: 'ys-template-list', label: 'YS_TEMPLATE_LIST', component: 'ListViewData' },
  { name: 'wbj-template-list', label: 'WBJ_TEMPLATE_LIST', component: 'ListViewData' },
  { name: 'gb-template-list', label: 'GB_TEMPLATE_LIST', component: 'ListViewData' },
  { name: 'bj-template-list', label: 'BJ_TEMPLATE_LIST', component: 'ListViewData' },
]

export const EXCHANGE_SHARING_RULES_DATA_TABS = [
  { name: 'chart-statistics-graph', label: 'STATISTICS_GRAPH', component: 'ChartStatisticsGraph' },
  { name: 'list-of-rules', label: 'LIST_OF_RULES', component: 'ListViewRules' },
]

export const EXCHANGE_SHARING_DATA_TEMPLATE_TABS = [
  { name: 'ys-template-list', label: 'YS_TEMPLATE_LIST', component: 'ListViewData' },
  { name: 'wbj-template-list', label: 'WBJ_TEMPLATE_LIST', component: 'ListViewData' },
  { name: 'gb-template-list', label: 'GB_TEMPLATE_LIST', component: 'ListViewData' },
  { name: 'bj-template-list', label: 'BJ_TEMPLATE_LIST', component: 'ListViewData' },
  { name: 'catalog-overview', label: 'CATALOG_OVERVIEW', component: 'CatalogOverview' },
]

export const TABLE_DETAIL_TABS = [
  { name: 'statisticChart', label: 'STATISTIC_CHART', component: 'StatisticChart' },
  { name: 'dataList', label: 'DATA_LIST', component: 'DataList' },
  { name: 'taskList', label: 'TASK_LIST', component: 'TaskList' },
  { name: 'exportTaskList', label: 'EXPORT_TASK_LIST', component: 'ExportTaskList' },
]

export const COMMON_CHART_COMMANDS = [
  { label: '导出数据', value: 'EXPORT' },
]

export const DATE_CHART_COMMANDS = [
  { label: '导出数据', value: 'EXPORT' },
  { label: '7天', value: 7 },
  { label: '15天', value: 15 },
  { label: '1个月', value: 30 },
  { label: '半年', value: 180 },
]
