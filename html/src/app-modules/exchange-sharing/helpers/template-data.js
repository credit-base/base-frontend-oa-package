import Base from '@/models/base'

class TemplateData extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      name,
      identify,
      status = {},
      updateTime,
      createTime,
    } = params

    this.id = id
    this.name = name
    this.identify = identify
    this.status = status
    this.updateTime = updateTime
    this.createTime = createTime
  }
}

export default TemplateData
