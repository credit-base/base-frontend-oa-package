import Base from '@/models/base'

class Template extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      name,
      identify,
      exchangeFrequency = {},
      subjectCategory = [],
      dimension = {},
      infoClassify = {},
      sourceUnit = {},
      updateTime,
      createTime,
    } = params

    this.id = id
    this.name = name
    this.identify = identify
    this.exchangeFrequency = exchangeFrequency
    this.subjectCategory = subjectCategory
    this.dimension = dimension
    this.infoClassify = infoClassify
    this.sourceUnit = sourceUnit
    this.updateTime = updateTime
    this.createTime = createTime
  }
}

export default Template
