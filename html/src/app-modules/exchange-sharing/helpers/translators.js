/**
 * @file 前置机交换系统 翻译器
 * @module exchange-sharing/translators
 */

import Template from './template'
import TemplateData from './template-data'
import ExchangeSharingRules from './rules'

export function translateTemplateList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new Template(item).get([
      'id',
      'template',
      'templateType',
      'userGroup',
      'rules',
      'total',
      'createTime',
      'updateTime',
    ]))
    : []
  return {
    list: tempList,
    total,
  }
}

export function translateRuleList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new ExchangeSharingRules(item).get([
      'id',
      'name',
      'username',
      'password',
      'ip',
      'createTime',
      'updateTime',
    ]))
    : []
  return {
    list: tempList,
    total,
  }
}

export function translateTemplateDetail (res = {}) {
  return new Template(res)
    .get([
      'id',
      'name',
      'identify',
      'exchangeFrequency',
      'subjectCategory',
      'dimension',
      'sourceUnit',
      'infoClassify',
      'updateTime',
      'createTime',
    ])
}

export function translateRuleDetail (res = {}) {
  return new ExchangeSharingRules(res)
    .get([
      'id',
      'name',
      'username',
      'ip',
      'template',
      'updateTime',
      'createTime',
    ])
}

export function translateGbDataList (res = {}) {
  const { list = [], total = 0 } = res
  const tempList = Array.isArray(list)
    ? list.map(item => new TemplateData(item)
      .get([
        'id',
        'name',
        'identify',
        'status',
        'updateTime',
      ]))
    : []
  return {
    list: tempList,
    total,
  }
}
