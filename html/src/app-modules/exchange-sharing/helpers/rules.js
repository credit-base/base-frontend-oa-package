import Base from '@/models/base'

class ExchangeSharingRules extends Base {
  constructor (params = {}) {
    super(params)
    const {
      id,
      name,
      username,
      password,
      template = [],
      ip,
      updateTime,
      createTime,
    } = params

    this.id = id
    this.name = name
    this.username = username
    this.password = password
    this.template = template
    this.ip = ip
    this.updateTime = updateTime
    this.createTime = createTime
  }
}

export default ExchangeSharingRules
