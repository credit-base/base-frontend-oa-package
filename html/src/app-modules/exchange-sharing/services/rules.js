/**
 * @file 国标规则管理 - 接口
 * @module services/rules
 */

import request from '@/utils/request'
import {
  ysTemplateList,
  wbjTemplateList,
  gbTemplateList,
  bjTemplateList,
  rulesList,
  rulesDetail,
  templateDetail,
} from '../helpers/mocks'

const templateTypeEnum = {
  MA: ysTemplateList,
  MQ: wbjTemplateList,
  Mg: gbTemplateList,
  Mw: bjTemplateList,
}
/**
 *
 * @param {MA: '原始', MQ: '委办局', Mg, '国标', Mw: '本级'} type
 * @param {*} params
 */
export function fetchRulesTemplateList (params = { type: 'MA', useMock: false }) {
  if (params.useMock) {
    const templateList = templateTypeEnum[params.type]
    return {
      list: [
        ...templateList,
      ],
      total: 6,
    }
  }
  return request('/api/templates', { method: 'GET', params })
}

export function fetchRulesTemplateDetail (id, params = { useMock: false }) {
  if (params.useMock) {
    return {
      ...templateDetail,
    }
  }
  return request(`/api/templates/${id}`, { method: 'GET', params })
}

export function fetchRulesListByTemplateId (id, params = { useMock: false }) {
  if (params.useMock) {
    return {
      list: [
        ...rulesList,
      ],
      total: 3,
    }
  }
  return request(`/api/rules/${id}`, { method: 'GET', params })
}

export function fetchRulesDetail (templateId, id, params = { useMock: false }) {
  if (params.useMock) {
    return {
      ...rulesDetail,
    }
  }
  return request(`/api/rules/detail/${templateId}/${id}`, { method: 'GET', params })
}

export function fetchConfigs (params = {}) {
  return request('/api/template/configure', { params })
}
