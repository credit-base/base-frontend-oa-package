/**
 * @file 接口 - 国标数据管理
 * @module data-manage/gb-data
 */
import request from '@/utils/request'
import { templateDataList } from '../helpers/mocks'
import { getRecentMonths } from '@/plugins/echarts/utils'
import { randomNum } from '@/utils'

// fetchDataConversionRate, fetchDataTask, fetchDataVolume, fetchStaticsByUserGroup, fetchGbTemplateDataList

export function fetchDataConversionRate (id, params = {}) {
  if (params.useMock) {
    const tempData = {
      columns: ['category', 'total'],
      rows: [
        { category: '数据转换', total: 24 },
        { category: '数据补全', total: 87 },
        { category: '数据比对', total: 35 },
        { category: '数据去重', total: 39 },
      ],
    }

    return {
      data: {
        status: 1,
        data: { ...tempData },
      },
    }
  }

  return request({
    url: `/api/statisticals/staticsByCategoryTotal?administrativeArea=${id}`,
    method: 'GET',
  })
}

export function fetchDataVolume () {
  const list = [
    { date: '2日', area: 45, areaLimit: 100, industry: 80 },
    { date: '3日', area: 43, areaLimit: 267, industry: 120 },
    { date: '4日', area: 120, areaLimit: 122, industry: 60 },
    { date: '5日', area: 45, areaLimit: 100, industry: 80 },
    { date: '6日', area: 68, areaLimit: 267, industry: 120 },
    { date: '7日', area: 72, areaLimit: 122, industry: 60 },
    { date: '8日', area: 54, areaLimit: 82, industry: 110 },
  ]

  return {
    data: {
      status: 1,
      data: { list },
    },
  }
}

export function fetchDataTask () {
  const months = getRecentMonths(6)
  const list = months.map(month => ({
    month,
    reward: randomNum(10) * 20, // 激励
    punishment: randomNum(10) * 10, // 惩戒
  }))

  return {
    data: {
      status: 1,
      data: { list },
    },
  }
}

export function fetchStaticsByUserGroup (id, params = {}) {
  if (params.useMock) {
    const mockData = {
      columns: [
        'name',
        '发展和改革委员会',
        '市场监督管理局',
        '民政局',
        '法院',
        '公安局',
        '交通厅',
      ],
      rows: {
        name: '',
        发展和改革委员会: '237',
        市场监督管理局: '261',
        民政局: '173',
        法院: '147',
        公安局: '210',
        交通厅: '220',
      },
    }
    return {
      data: {
        status: 1,
        data: { ...mockData },
      },
    }
  }

  return request({
    url: `/api/statisticals/staticsByCategory?=administrativeArea=${id}`,
    method: 'GET',
  })
}

export function fetchGbTemplateDataList (id, params = {}) {
  if (params.useMock) {
    return {
      list: [
        ...templateDataList,
      ],
      total: 7,
    }
  }

  return request(`api/go-data/templateData/${id}`, { method: 'GET', params })
}
