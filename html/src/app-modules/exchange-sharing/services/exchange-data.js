/**
 * @file 接口 - 交换数据
 * @module services/exchange-data
 */
import store from '@/store'
import request from '@/utils/request'

/**
 * 获取交换数据列表
 */
export function getDataList (params = {}) {
  if (params.useMock) {
    let list = store.getters.exchangeDataList

    if (params.sort) {
      if (params.sort === '-storageTime') {
        list = list.sort((item1, item2) => item2.storageTime - item1.storageTime)
      }

      if (params.sort === 'storageTime') {
        list = list.sort((item1, item2) => item1.storageTime - item2.storageTime)
      }
    }

    return {
      list,
      total: list.length,
    }
  }

  return request('', { params })
}

/**
 * 获取交换任务列表
 */
export function getTaskList (params = {}) {
  if (params.useMock) {
    let list = store.getters.exchangeTaskList

    if (params.sort) {
      if (params.sort === '-createTime') {
        list = list.sort((item1, item2) => item2.createTime - item1.createTime)
      }

      if (params.sort === 'createTime') {
        list = list.sort((item1, item2) => item1.createTime - item2.createTime)
      }
    }

    return {
      list,
      total: list.length,
    }
  }

  return request('', { params })
}

/**
 * 获取交换导出任务列表
 */
export function getExportTaskList (params = {}) {
  if (params.useMock) {
    let list = store.getters.exchangeExportTaskList

    if (params.sort) {
      if (params.sort === '-createTime') {
        list = list.sort((item1, item2) => item2.createTime - item1.createTime)
      }

      if (params.sort === 'createTime') {
        list = list.sort((item1, item2) => item1.createTime - item2.createTime)
      }

      if (params.sort === '-startTime') {
        list = list.sort((item1, item2) => item2.startTime - item1.startTime)
      }

      if (params.sort === 'startTime') {
        list = list.sort((item1, item2) => item1.startTime - item2.startTime)
      }

      if (params.sort === '-endTime') {
        list = list.sort((item1, item2) => item2.endTime - item1.endTime)
      }

      if (params.sort === 'endTime') {
        list = list.sort((item1, item2) => item1.endTime - item2.endTime)
      }
    }

    return {
      list,
      total: list.length,
    }
  }

  return request('', { params })
}
