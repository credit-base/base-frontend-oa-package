
export function hasPermission (purviewArr, route) {
  if (route.meta && route.meta.routerId) {
    return purviewArr.includes(parseInt(route.meta.routerId))
  } else {
    return true
  }
}

export function filterAsyncRoutes (routes, purviewArr) {
  if (!purviewArr) return routes
  const formatPurview = purviewArr.map(prop => parseFloat(prop))

  const res = []

  routes.forEach(route => {
    const hasChildren = Boolean(route.children && route.children.length)
    const temp = { ...route, hasChildren }

    /* istanbul ignore else */
    if (hasPermission(formatPurview, temp)) {
      /* istanbul ignore else */
      if (temp.children) {
        temp.children = filterAsyncRoutes(temp.children, formatPurview)
      }

      /* istanbul ignore else */
      if (!temp.children || (temp.hasChildren && Boolean(temp.children.length))) {
        res.push(temp)
      }
    }
  })
  return res
}
