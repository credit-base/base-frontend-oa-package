/**
 * @file 数据存储
 * @module utils/storage
 */

import Cookies from 'js-cookie'
import {
  APP_TOKEN_KEY,
  APP_THEME_KEY,
  APP_USERNAME_KEY,
  APP_LANGUAGE_KEY,
  APP_RULES_TEMPLATE_KEY,
} from '@/constants/storage'

export function getToken () {
  return Cookies.get(APP_TOKEN_KEY)
}

export function setToken (token) {
  return Cookies.set(APP_TOKEN_KEY, token)
}

export function removeToken () {
  return Cookies.remove(APP_TOKEN_KEY)
}

export function getTheme () {
  return Cookies.get(APP_THEME_KEY)
}

export function setTheme (theme) {
  return Cookies.set(APP_THEME_KEY, theme)
}

export function removeTheme () {
  return Cookies.remove(APP_THEME_KEY)
}

export function getUsername () {
  return Cookies.get(APP_USERNAME_KEY)
}

export function setUsername (username) {
  return Cookies.set(APP_USERNAME_KEY, username)
}

export function removeUsername () {
  return Cookies.remove(APP_USERNAME_KEY)
}

export function getLanguage () {
  return Cookies.get(APP_LANGUAGE_KEY)
}

export function setLanguage (language) {
  return Cookies.set(APP_LANGUAGE_KEY, language)
}

export function removeLanguage () {
  return Cookies.remove(APP_LANGUAGE_KEY)
}

export function setRulesTemplateData (data) {
  return Cookies.set(APP_RULES_TEMPLATE_KEY, data)
}

export function getRulesTemplateData () {
  return Cookies.get(APP_RULES_TEMPLATE_KEY)
}
