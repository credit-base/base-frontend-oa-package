/**
 * @file Crypto-js 加密
 * @module utils/encrypt
 */

import CryptoJS from 'crypto-js'

const CryptoJsAesJson = {
  stringify (cipherParams) {
    const j = {
      ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64),
    }

    cipherParams.iv && (j.iv = cipherParams.iv.toString())
    cipherParams.salt && (j.s = cipherParams.salt.toString())

    return JSON.stringify(j)
  },

  parse (jsonStr) {
    const j = JSON.parse(jsonStr)
    const cipherParams = CryptoJS.lib.CipherParams.create({
      ciphertext: CryptoJS.enc.Base64.parse(j.ct),
    })

    j.iv && (cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv))
    j.s && (cipherParams.salt = CryptoJS.enc.Hex.parse(j.s))

    return cipherParams
  },
}

/**
 * 加密方法
 * @param {string} hash 加密哈希
 * @param {any} data 被加密数据
 */
export function encrypt (hash, data) {
  const encryptString = CryptoJS.AES.encrypt(JSON.stringify(data), hash, {
    format: CryptoJsAesJson,
  }).toString()

  return encryptString
}

/**
 * 解密方法
 * @param {string} hash 解密哈希
 * @param {any} data 被解密数据
 */
export function decrypt (hash, data) {
  const bytes = CryptoJS.AES.decrypt(data, hash, { format: CryptoJsAesJson })
  const decryptString = bytes.toString(CryptoJS.enc.Utf8)

  return decryptString
}
