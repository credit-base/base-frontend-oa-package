/**
 * @file 校验
 * @module utils/validate
 */

import Vue from 'vue'
import i18n from '@/i18n'
import {
  RE_EXTERNAL_LINK,
  RE_REJECT_REASON,
  RE_REVOKE_REASON,
} from '@/constants/regexp'

const i18nTip = i18n.t('tip')

/**
 * Check if user external
 * @param {string} val url
 */
export function isExternalUrl (val) {
  return RE_EXTERNAL_LINK.test(val)
}

export function isValidRejectReason (value) {
  if (!value) return i18nTip.NO_EMPTY_REJECT_REASON
  return RE_REJECT_REASON.test(value) ? true : i18nTip.INVALID_REJECT_REASON
}

export function isValidRevokeReason (value) {
  if (!value) return i18nTip.NO_EMPTY_REVOKE_REASON
  return RE_REVOKE_REASON.test(value) ? true : i18nTip.INVALID_REVOKE_REASON
}

/**
 * 检测上传是否符合要求
 * @param { name, size } file
 * @param {*} fileSize
 * @param {*} fileExtensionsList
 * @param {messageSizeExceed, messageTypeInvalid} message
 * @returns
 */
export function isValidFiles (file = {}, fileSize, fileExtensionsList = [], message = {}) {
  if (
    !file ||
    !file.size ||
    !file.name ||
    !Array.isArray(fileExtensionsList) ||
    !fileExtensionsList.length ||
    !fileSize
  ) {
    const message = i18nTip.MISSING_PARAMETERS
    Vue.prototype.$message({ type: 'error', message })
    return false
  }

  const { size, name } = file
  const { messageSizeExceed, messageTypeInvalid } = message
  const fileExtensions = name.substring(name.lastIndexOf('.') + 1)
  const hasMatchedExtension = fileExtensionsList.includes(fileExtensions)
  const isSizeMatchedLimit = (fileSize * 1 << 20) > size

  if (!hasMatchedExtension) {
    const message = messageTypeInvalid || i18nTip.ATTACHMENT_TYPE_INVALID

    Vue.prototype.$message({ type: 'error', message })

    return false
  }

  if (!isSizeMatchedLimit) {
    const message = messageSizeExceed || i18nTip.ATTACHMENT_SIZE_EXCEED

    Vue.prototype.$message({ type: 'error', message })

    return false
  }

  return true
}
