import Vue from 'vue'
import axios from 'axios'
import * as Storage from '@/utils/storage'
import _ from 'lodash'
import settings from '@/settings'

const { OSS_HOST } = settings

function fetchDownloadFile (content, fileName) {
  const blob = new Blob([content]) // 创建一个类文件对象：Blob对象表示一个不可变的、原始数据的类文件对象
  const url = window.URL.createObjectURL(blob)// URL.createObjectURL(object)表示生成一个File对象或Blob对象
  const dom = document.createElement('a')// 设置一个隐藏的a标签，href为输出流，设置download
  dom.style.display = 'none'
  dom.href = url
  dom.setAttribute('download', fileName)// 指示浏览器下载url,而不是导航到它；因此将提示用户将其保存为本地文件
  document.body.appendChild(dom)
  dom.click()
}

export function downloadFileByAxios (url) {
  axios({
    method: 'GET', // 请求方式
    url,
    responseType: 'blob', // 服务器返回的数据类型
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'jwt-token': Storage.getToken(),
    },
  }).then(response => {
    try {
      const { data = {}, headers: { filename } = {} } = response
      if (filename && data.size) {
        fetchDownloadFile(data, filename)
      } else {
        const message = '资源目录数据异常，模版无法下载'

        Vue.prototype.$message({ message, type: 'error' })
      }
    } catch (error) {
      // ignore
    }
  })
}

export function downloadFile (url) {
  const fileLink = document.createElement('a')

  fileLink.href = url
  document.body.appendChild(fileLink)

  fileLink.click()
}

export const getUid = () => {
  const res = []
  Array.from({ length: 32 }, () => res.push(Math.floor(Math.random() * 16).toString(16)))
  return res.join('')
}
/**
 * @file Utils
 * @module utils/index
 */
/**
 * 生成范围内随机数
 * @param {number} minNum 最小值
 * @param {number} maxNum 最大值
 */
export function randomNum (maxNum, minNum) {
  let result = 0
  switch (arguments.length) {
    case 1:
      result = parseInt(Math.random() * maxNum + 1, 10)
      break
    case 2:
      result = parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10)
      break
    default:
      result = 0
      break
  }
  return result
}

/**
 * 随机数字
 * @param {number} min 最小值，只传1个参数为最大值
 * @param {number} max 最大值
 * @returns {number} 范围内随机值
 */
export function randomNumber (min, max) {
  if (!max) {
    max = min
    min = 0
  }

  return ~~(Math.random() * (max - min) + min)
}

/**
 * 延迟效果
 * @param {number} delay 延迟时间 毫秒
 * @returns {Promise<void>} promise 实例
 */
export const sleep = delay => new Promise(resolve => setTimeout(resolve(true), delay))

/**
 * 数组转Map
 * @param {array} options 配置数组
 * @param {string} labelKey Label字段key
 * @param {string} valueKey Value字段key
 */
export const makeMap = (options, {
  labelKey = 'name',
  valueKey = 'id',
} = {}) => {
  if (!Array.isArray(options)) return new Map()
  return new Map(options.map(item => [item[valueKey], item[labelKey]]))
}

/**
 * 判断是否为整数 包含0
 * @param {num} number 输入值
 */
export function isInteger (num, { allowString } = {}) {
  if (allowString) num = parseFloat(num)
  return typeof num === 'number' && ~~(num) === num
}

/**
 * 判断路由是否可视
 * @param {object} route 路由
 */
function hasViewStatus (route, filterType) {
  if (route.meta && route.meta[filterType] !== undefined) {
    return route.meta.isVisible
  } else {
    return true
  }
}

/**
 * 递归过滤可视路由
 * @param {array} routes 路由
 */
export function filterViewRouter (routes, filterType) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }

    if (hasViewStatus(route, filterType)) {
      if (tmp.children) {
        tmp.children = filterViewRouter(tmp.children, filterType)
      }

      res.push(tmp)
    }
  })

  return res
}

/**
 * 格式化数字 千分符
 * @param {number} num 被格式化数字
 */
export function formatNumber (num, options = { locale: 'en-US' }) {
  if (typeof num !== 'number') return 0
  return num.toLocaleString(options.locale, options)
}

/**
 * 指定排序的比较方法
 *
 * @export
 * @param {*} property
 * @param {ASCENDING, DESCENDING} type
 * @returns
 */
export function compare (property, type = 'ASCENDING') {
  return function (originObj, targetObj) {
    const originValue = Number(originObj[property])
    const targetValue = Number(targetObj[property])

    return type === 'ASCENDING' ? originValue - targetValue : targetValue - originValue
  }
}

/**
 *  获取随机长度字符串
 * @param {number} length 字符串长度
 */
export function getRandomString ({
  length = 10,
  chars = 'abcdefghijklmnopqrstuvwxyz123456789',
} = {}) {
  const size = chars.length
  let string = ''

  for (let idx = 0; idx < length; idx++) {
    string += chars[~~(Math.random() * size)]
  }

  return string
}

/**
 * 生成文件名
 * @param {File} file 文件
 */
export function renameFile (file) {
  const timeStamp = Date.now()
  const randomString = getRandomString()
  const suffix = file.name.substring(file.name.lastIndexOf('.'))
  const newName = `${randomString}_${timeStamp + suffix}`

  return newName
}

/**
 * 获取无扩展名的文件名
 * @param {string} identify 文件上传全名
 */
export function getNameFromIdentify (identify) {
  if (!identify) return ''

  const idx = identify.lastIndexOf('.')

  return identify.substring(0, idx)
}

/**
 * 过滤对象属性为空
 * @param {*} data
 * @returns
 */
export function filterObject (data = {}) {
  const params = {}

  for (const key in data) {
    if (!_.isEmpty(data[key])) {
      params[key] = data[key]
    }
  }
  return params
}

export function dataType (obj) {
  if (obj === null) return 'Null'
  if (obj === undefined) return 'Undefined'
  return Object.prototype.toString.call(obj).slice(8, -1)
}

export function filterObjectValue (obj) {
  const param = {}
  if (obj === null || obj === undefined || obj === '') return param

  for (const key in obj) {
    if (dataType(obj[key]) === 'Object') {
      param[key] = filterObjectValue(obj[key])
    } else if (obj[key] !== null && obj[key] !== undefined && obj[key] !== '') {
      param[key] = obj[key]
    }
  }
  return param
}

export const filterEmptyProperty = obj => {
  for (const key in obj) {
    if (obj[key] == null) delete obj[key]
    if (dataType(obj[key]) === 'String') {
      obj[key] = obj[key].trim()
      if (obj[key] === '') delete obj[key]
    } else if (dataType(obj[key]) === 'Object') {
      obj[key] = filterEmptyProperty(obj[key])
      if (JSON.stringify(obj[key]) === '{}') delete obj[key]
    } else if (dataType(obj[key]) === 'Array') {
      obj[key] = filterEmptyProperty(obj[key])
      obj[key] = obj[key].filter(arr => arr)
      if (obj[key].length === 0) delete obj[key]
    }
  }
  return obj
}

/**
 * 移除对象中的值为 [0, undefined, null, ``, false] 的键
 * @param {object} map
 */
export function dehydrate (map) {
  return Object.fromEntries(Object.entries(map)
    .filter(item => Boolean(item[1])))
}

/**
 * Prefix the given name to symbol url collection
 * @param {string} name icon name
 */
export function useGraphIcons (name) {
  const icons = {}
  const dashName = name.replace(/[A-Z]/g, m => `-${m.toLowerCase()}`)
  icons[name] = `image:///static/images/icon/${dashName}.png`
  icons[`${name}Success`] = `image:///static/images/icon/${dashName}-success.png`
  icons[`${name}Danger`] = `image:///static/images/icon/${dashName}-danger.png`
  icons[`${name}Warning`] = `image:///static/images/icon/${dashName}-warning.png`
  icons[
    `${name}Disabled`
  ] = `image:///static/images/icon/${dashName}-disabled.png`
  return icons
}

export function renderOSSUrl (data = []) {
  if (!data.length) return []

  try {
    return data.map(item => {
      return {
        name: item.name,
        identify: OSS_HOST + item.identify,
      }
    })
  } catch (error) {
    return []
  }
}
