/**
 * @file 日期时间工具函数
 * @module utils/date-time
 */

import dayjs from 'dayjs'

/**
 * 格式化时间
 *
 * @param {string} time 时间
 * @param {string} fmt 格式
 *
 * @see https://dayjs.gitee.io
 */
export function formatTime (time, fmt = 'YYYY年MM月DD日 HH时mm分') {
  if (String(time).length === 10) {
    return dayjs.unix(Number(time)).format(fmt)
  }
  return dayjs(Number(time)).format(fmt)
}

/**
 * 获取unix时间戳，默认获取当前
 * @param {time} time 时间
 */
export function getUnixTimeStamp (time) {
  if (time) {
    return dayjs(time).unix()
  }

  return dayjs().unix()
}
