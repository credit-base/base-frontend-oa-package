/**
 * @file ElementUI 工具函数
 * @module utils/element
 */

import Vue from 'vue'
import i18n from '@/i18n'

/**
 * Element Confirm 提示框
 * @param {string} title 提示标题
 * @param {string} content 提示内容
 * @param {string} [type = 'warning'] 提示类型
 * @returns {Promise} Promise实例
 */
export function openConfirm ({
  title = i18n.t('field').TIP,
  content = '',
  type = 'warning',
  confirmButtonText = i18n.t('action').CONFIRM,
  cancelButtonText = i18n.t('action').CANCEL,
  ...options
} = {}) {
  const confirmOptions = {
    type,
    confirmButtonText,
    confirmButtonClass: 'is-plain el-button--success el-button--medium',
    cancelButtonText,
    cancelButtonClass: 'is-plain el-button--medium',
    ...options,
  }

  return Vue.prototype.$confirm(content, title, confirmOptions)
}

/**
 * Element Prompt 提示框
 * @param {string} title 提示标题
 * @param {string} content 提示内容
 * @returns {Promise}  Promise实例
 */
export function openPrompt ({
  title = i18n.t('field').TIP,
  content = '',
  inputErrorMessage = '请输入原因',
  inputPlaceholder = '请输入',
  confirmButtonText = i18n.t('action').CONFIRM,
  cancelButtonText = i18n.t('action').CANCEL,
  ...options
} = {}) {
  const promptOptions = {
    confirmButtonText,
    confirmButtonClass: 'is-plain el-button--success el-button--medium',
    cancelButtonText,
    cancelButtonClass: 'is-plain el-button--medium',
    inputType: 'textarea',
    inputPattern: /\S/,
    inputErrorMessage,
    inputPlaceholder,
    ...options,
  }

  return Vue.prototype.$prompt(content, title, promptOptions)
}
