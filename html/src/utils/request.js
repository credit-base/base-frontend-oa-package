/**
 * @file HTTP请求
 * @module utils/request
 */

import Vue from 'vue'
import axios from 'axios'
import store from '@/store'
import { openConfirm } from './element'
import * as Storage from '@/utils/storage'
import {
  REQUEST_STATUS_SUCCESS,
  REQUEST_STATUS_ERROR,
  RESPONSE_ID_NEED_LOGIN,
  REQUEST_TIMEOUT_MILLISECONDS,
} from '@/constants/request'

/**
 * 创建通用 Axios 实例
 */
const instance = axios.create({
  baseURL: process.env.VUE_APP_API_HOST || '/',
  timeout: REQUEST_TIMEOUT_MILLISECONDS,
})

/**
 * 通用实例请求拦截器
 */
instance.interceptors.request.use(config => {
  // /api/authToken
  if (Storage.getToken() && !(config.url.includes(`/api/authToken`) || config.url.includes(`/oss/authToken`))) {
    config.headers['jwt-token'] = Storage.getToken()
  }

  // 后端通过此 Header 判断是否 Ajax 请求
  config.headers['X-Requested-With'] = 'XMLHttpRequest'

  return config
}, err => {
  return Promise.reject(err)
})

/**
 * 通用实例响应拦截器
 */
instance.interceptors.response.use(res => {
  const { data: { status, data = {} } = {} } = res

  if (status === undefined) {
    const message = '请求返回出错，无状态码'

    Vue.prototype.$message({ message, type: 'error' })
    return Promise.reject(new Error(message))
  }

  switch (status) {
    case REQUEST_STATUS_SUCCESS:
      return data

    case REQUEST_STATUS_ERROR:
      if (data.id === RESPONSE_ID_NEED_LOGIN) {
        const confirmOptions = {
          title: '提示',
          content: '您的登录状态已失效，您可以选择重新登录，或者停留在此页面',
          confirmButtonText: '重新登录',
        }

        openConfirm(confirmOptions)
          .then(async () => {
            await store.dispatch('resetToken')
            window.location.reload()
          })
          .catch(() => {})
      } else {
        const message = data.title || '未知的错误'
        Vue.prototype.$message({ message, type: 'error' })
        return Promise.reject(new Error(message))
      }

      break

    default: {
      const message = data.title || '未知的状态码'
      Vue.prototype.$message({ message, type: 'error' })
      return Promise.reject(new Error(message))
    }
  }
}, err => {
  if (err.code === 'ECONNABORTED' && err.message.includes('timeout')) {
    const message = err.message || '接口请求超时'
    Vue.prototype.$message({ message, type: 'error' })
  } else {
    const message = err.message || '接口响应出错'
    Vue.prototype.$message({ message, type: 'error' })
  }

  return Promise.reject(err)
})

/**
 * 数据请求方法封装
 * @param {string} url 请求路径
 */
function request (url, {
  method = 'GET',
  params = {},
  options = {},
  baseURL = '',
} = {}) {
  method = method.toUpperCase()

  switch (method) {
    case 'GET':
      return instance.get(url, { params, baseURL })

    case 'POST':
      return instance.post(url, params, options)

    case 'DELETE':
      return instance.delete(url, { params })

    case 'PUT':
      return instance.put(url, params)

    default:
      return Promise.reject(new Error(`Unknown request method: ${method}`))
  }
}

export default request
