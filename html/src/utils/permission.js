/**
 * @file 路由员工
 * @module utils/permission
 */

import Vue from 'vue'
import router from '@/router'
import store from '@/store'
import NProgress from 'nprogress'
import { CONSTANTS } from '@/plugins/constants'
import 'nprogress/nprogress.css'
import * as Storage from '@/utils/storage'

const { PURVIEW } = CONSTANTS

NProgress.configure({ showSpinner: false })

// No redirect white-list
const WHITE_LIST = ['/sign-in']

router.beforeEach(async (to, from, next) => {
  NProgress.start()
  // has token
  const hasToken = Storage.getToken()

  if (hasToken) {
    if (to.path === '/sign-in') {
      next('/')
      NProgress.done()
    } else {
      // 路由跳转
      const { purview = [], category = {} } = store.getters

      const isSuperAdmin = PURVIEW.SUPER_ADMIN === category.id
      const hasPurviewData = Array.isArray(purview) && !!purview.length
      if (isSuperAdmin || hasPurviewData) {
        next()
      } else {
        try {
          const res = await store.dispatch('getUserInfo')
          const routes = await store.dispatch('GenerateRoutes', res)

          routes.forEach(route => router.addRoute(route))
          next({ ...to, replace: true })
        } catch (err) {
          NProgress.done()
          await store.dispatch('resetToken')
          Vue.prototype.$message({ type: 'error', message: err || 'An error occurs' })
        }
      }
    }
  } else {
    // has no token
    if (WHITE_LIST.includes(to.path)) {
      next()
    } else {
      next(`/sign-in?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})
