/**
 * 应用地域配置
 * @module settings
 */

const config = require('../public/config/config.js')

const {
  APP_CONFIG: {
    REGION_NAME = 'XXX',
    REGION_LEVEL = '市',
    CREDIT = '信用',
    CHINA = '中国',
    PROVINCE = '省',
    DASHBOARD = '公共信用信息大数据驾驶舱',
    APP_TITLE = '公共信用信息平台',
    LINKS,
    TECHNICAL = {
      COPY_TITLE: '企信云2015-',
      TITLE: '北京企信云信息科技有限公司',
      URL: 'https://www.qixinyun.com/',
    },
    IS_NETWORK_ENABLE = true,
    IS_PURVIEW_ENABLE = true,
    REGION_NAME_COORDINATE = [],
    OSS_HOST,
    RANK_DATA,
  },
  ROUTER_VISIBLE_CONFIG,
} = window.$CREDIT_APP_CONFIG || config

module.exports = {
  // 站点标题
  OSS_HOST,
  TITLE: `${CREDIT}${CHINA} • ${REGION_NAME}${REGION_LEVEL}${APP_TITLE}`,
  DASHBOARD_TITLE: `${REGION_NAME}${REGION_LEVEL}${DASHBOARD}`,
  REGION_SHORT_NAME: REGION_NAME,
  APP_TITLE,
  CREDIT,
  LINKS,
  TECHNICAL,
  REGION_NAME_COORDINATE,
  IS_NETWORK_ENABLE,
  IS_PURVIEW_ENABLE,
  ROUTER_VISIBLE_CONFIG,
  REGION_NAME: `${REGION_NAME}${REGION_LEVEL}`,
  FALLBACK_REGION_NAME: PROVINCE,
  INIT_MAP_NAME_LIST: PROVINCE.includes(REGION_NAME) ? [PROVINCE] : [PROVINCE, REGION_NAME + REGION_LEVEL],
  RANK_DATA,
}
