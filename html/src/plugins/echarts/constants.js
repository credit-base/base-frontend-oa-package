/**
 * 图表常量
 */

export const WEEKS = ['一', '二', '三', '四', '五', '六', '日'].map(v => `星期${v}`)
export const MONTHS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(v => `${v}月`)

// 右键菜单
export const CONTEXT_MENU_EVENTS = {
  onlineReport: 'ONLINE_REPORT', // 在线填报
  multipleUpload: 'MULTIPLE_UPLOAD', // 批量上传
  cloudDatabaseImport: 'CLOUD_DATABASE_IMPORT', // 前置机导入
  downloadTemplate: 'DOWNLOAD_TEMPLATE', // 模板下载
  createCatalog: 'CREATE_CATALOG', // 创建目录
  viewCatalog: 'VIEW_CATALOG', // 查看目录
  editCatalog: 'EDIT_CATALOG', // 编辑目录
  createRule: 'CREATE_RULE', // 创建规则
  editRule: 'EDIT_RULE', // 编辑规则
  assignTask: 'ASSIGN_TASK', // 指派任务
  viewTask: 'VIEW_TASK', // 查看工单任务
  viewDetail: 'VIEW_DETAIL', // 查看详情
  exportData: 'EXPORT_DATA', // 导出数据
}
