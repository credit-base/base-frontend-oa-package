/**
 * 插件 - Echarts
 *
 * plugins - echarts
 */
import Vue from 'vue'
import ECharts from 'vue-echarts'
import { use } from 'echarts/core'
// 手动引入 ECharts 各模块来减小打包体积

import {
  CanvasRenderer,
} from 'echarts/renderers'

// 按需引用
import {
  BarChart,
  GraphChart,
  LineChart,
  PieChart,
  MapChart,
  RadarChart,
  ScatterChart,
  EffectScatterChart,
  LinesChart,
  GaugeChart,
} from 'echarts/charts'

import {
  DatasetComponent,
  DataZoomComponent,
  GeoComponent,
  GridComponent,
  PolarComponent,
  TooltipComponent,
  LegendComponent,
  TitleComponent,
  VisualMapComponent,
  MarkLineComponent,
  MarkPointComponent,
} from 'echarts/components'

use([
  CanvasRenderer,
  BarChart,
  GraphChart,
  LineChart,
  PieChart,
  MapChart,
  RadarChart,
  ScatterChart,
  EffectScatterChart,
  LinesChart,
  GaugeChart,
  GridComponent,
  PolarComponent,
  GeoComponent,
  TooltipComponent,
  LegendComponent,
  TitleComponent,
  VisualMapComponent,
  DatasetComponent,
  DataZoomComponent,
  MarkLineComponent,
  MarkPointComponent,
])

Vue.component('VueEcharts', ECharts)
