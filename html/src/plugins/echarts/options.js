
const color = '#667396' // 替换 #667396

/**
 * 文字
 */
export const textStyle = { color }

/**
 * 颜色
 */
export const colors = [
  '#01E3F3',
  '#0157FA',
  '#2FFFB9',
  '#FF397B',
  '#9B00FF',
  '#FFCC5E',
  '#FF6741',
]

/**
 * 图例
 */
export const legend = {
  show: true,
  top: 10,
  right: 0,
  textStyle,
  type: 'scroll',
  orient: 'horizontal',
  pageIconColor: color,
  pageIconInactiveColor: '#aaa',
  pageTextStyle: { color },
}

/**
 * 区域
 */
export const grid = {
  top: 50,
  left: 0,
  right: 0,
  bottom: 20,
  containLabel: true,
}
