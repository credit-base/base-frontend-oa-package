
import { MONTHS } from './constants'

/**
 * 获取当前时间
 */
export const getNow = () => new Date()

/**
 * 获取当前月份
 */
export const getCurrentMonth = () => getNow().getMonth() + 1

/**
 * 获取最近几月月份
 *
 * @param {number} num 数量
 */
export function getRecentMonths (num) {
  const months = []
  const currentMonthIdx = getNow().getMonth()
  const MONTH_OFFSET = 1

  if (!num) return months

  while (num) {
    const monthIdx = (currentMonthIdx - num + MONTHS.length + MONTH_OFFSET) % MONTHS.length
    months.push(MONTHS[monthIdx])
    num--
  }

  return months
}

/**
 * 阻止 Echarts 事件冒泡
 * @param {object} event echarts 事件
 * @param {string} [stopEventType='click'] 事件类型 默认 `click`
 */
export function stopEvent (event, stopEventType = 'click') {
  if (!event || event.type !== stopEventType) return

  typeof event.stop === 'function' && event.stop()
}

/**
 * 清除地图名无用信息
 */
export function purgeMapName (mapName) {
  if (typeof mapName !== 'string') return ''

  return mapName
    .replace(/省/g, '')
    .replace(/市/g, '')
    .replace(/县/g, '')
    .replace(/盟/g, '')
    .replace(/地区/g, '')
    .replace(/自治区/g, '')
    .replace(/自治旗/g, '')
    .replace(/自治州/g, '')
    .replace(/特别行政区/g, '')
}
