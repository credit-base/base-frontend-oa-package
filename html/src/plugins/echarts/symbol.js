/**
 * @file Symbols
 * @module graph/symbol
 */

export const prefixSymbol = name => `image:///static/images/chart/icon-${name}.png`

// 原始数据库
export const SYMBOL_ORIGIN = prefixSymbol('origin')

// 平台上传
export const SYMBOL_UPLOAD = prefixSymbol('upload')

// 国标数据库
export const SYMBOL_GB = prefixSymbol('gb')

export const SYMBOL_GB_ADD = prefixSymbol('gb-add')

// 本级数据库
export const SYMBOL_BJ = prefixSymbol('bj')

// 委办局数据库
export const SYMBOL_WBJ = prefixSymbol('wbj')

// 前置机回推库
export const SYMBOL_LIB_REVERSE = prefixSymbol('lib-reverse')

// 前置机原始库
export const SYMBOL_LIB_ORIGIN = prefixSymbol('lib-origin')

// 前置机国标库
export const SYMBOL_LIB_GB = prefixSymbol('lib-gb')

// 前置机本级库
export const SYMBOL_LIB_BJ = prefixSymbol('lib-bj')

// 前置机委办局库
export const SYMBOL_LIB_WBJ = prefixSymbol('lib-wbj')

export const SYMBOL_SIZE = {
  small: 40,
  large: 60,
}
