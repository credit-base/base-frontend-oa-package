/**
 * ElementUI
 *
 * @module plugins/element
 */

import Vue from 'vue'
import {
  Button,
  Badge,
  Card,
  Input,
  Select,
  Option,
  Radio,
  Carousel,
  CarouselItem,
  RadioGroup,
  RadioButton,
  DatePicker,
  TimePicker,
  Collapse,
  CollapseItem,
  Checkbox,
  CheckboxButton,
  CheckboxGroup,
  Switch,
  ColorPicker,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  Menu,
  MenuItem,
  Link,
  Alert,
  Submenu,
  Skeleton,
  SkeletonItem,
  MenuItemGroup,
  Form,
  FormItem,
  Tooltip,
  Message,
  MessageBox,
  Notification,
  Dialog,
  Scrollbar,
  Breadcrumb,
  BreadcrumbItem,
  Image,
  Table,
  TableColumn,
  Tabs,
  TabPane,
  Pagination,
  Progress,
  Popover,
  Tag,
  Upload,
  Autocomplete,
  TimeSelect,
  Row,
  Col,
  Steps,
  Step,
  Tree,
  Cascader,
  Loading,
} from 'element-ui'

import 'element-ui/lib/theme-chalk/display.css'

Vue.prototype.$ELEMENT = { size: 'medium', zIndex: 99999 }

Vue.use(Button)
Vue.use(Badge)
Vue.use(Card)
Vue.use(Alert)
Vue.use(Input)
Vue.use(Select)
Vue.use(Option)
Vue.use(Radio)
Vue.use(Carousel)
Vue.use(CarouselItem)
Vue.use(RadioGroup)
Vue.use(RadioButton)
Vue.use(DatePicker)
Vue.use(TimeSelect)
Vue.use(TimePicker)
Vue.use(Collapse)
Vue.use(ColorPicker)
Vue.use(CollapseItem)
Vue.use(Checkbox)
Vue.use(CheckboxButton)
Vue.use(CheckboxGroup)
Vue.use(Switch)
Vue.use(Dropdown)
Vue.use(DropdownItem)
Vue.use(DropdownMenu)
Vue.use(Menu)
Vue.use(Link)
Vue.use(MenuItem)
Vue.use(Submenu)
Vue.use(Skeleton)
Vue.use(SkeletonItem)
Vue.use(MenuItemGroup)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Tooltip)
Vue.use(Scrollbar)
Vue.use(Progress)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Image)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Pagination)
Vue.use(Tag)
Vue.use(Upload)
Vue.use(Popover)
Vue.use(Dialog)
Vue.use(Autocomplete)
Vue.use(Row)
Vue.use(Col)
Vue.use(Steps)
Vue.use(Step)
Vue.use(Tree)
Vue.use(Cascader)
Vue.use(Loading.directive)

Vue.prototype.$message = Message
Vue.prototype.$notify = Notification
// https://github.com/PanJiaChen/vue-element-admin/issues/2168
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$alert = Vue.prototype.$msgbox.alert
Vue.prototype.$confirm = Vue.prototype.$msgbox.confirm
Vue.prototype.$prompt = Vue.prototype.$msgbox.prompt
