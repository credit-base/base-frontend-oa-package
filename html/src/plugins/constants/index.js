/**
 * @file 全局常量 挂载至Vue实例
 * @module plugins/constants
 */

import Vue from 'vue'
import {
  CONSTANTS_PLUGIN_KEY,
  CONSTANTS_PLUGIN_EXPORT_KEY,
} from '@/constants/app'

export const CONSTANTS = {
  PAGINATION_INIT_PAGE: 1,
  PAGINATION_INIT_LIMIT: 10,
  PAGINATION_INIT_TOTAL: 0,
  SORT_BY: {
    ascending: 'updateTime',
    descending: '-updateTime',
  },
  SORT_BY_END_TIME: {
    ascending: 'endTime',
    descending: '-endTime',
  },
  SORT_TYPE: {
    ASC: 'ascending',
    DESC: 'descending',
  },
  /**
   * 操作command类型 顺序为:
   * 0 => 查看 'VIEW'
   * 1 => 编辑 'EDIT'
   * 2 => 确认 'CONFIRM'
   * 3 => 反馈 'FEEDBACK'
   * 4 => 终结 'TERMINATION'
   * 5 => 撤销 'REVOKE'
   * 6 => 启用 'ENABLE'
   * 7 => 禁用 'DISABLE'
   * 8 => 置顶 'TOP'
   * 9 => 审核 'AUDIT'
   * 10 => 受理 'ACCEPT'
   * 11 => 移动 'MOVE'
   * 12 => 设为轮播 'SET_AS_CAROUSEL'
   * 13 => 通过 'APPROVE'
   * 14 => 驳回 'REJECT'
   * 15 => 重新提交 'RE_SUBMIT
   */
  COMMAND_TYPE: {
    VIEW: 'VIEW',
    AUDIT_VIEW: 'AUDIT_VIEW',
    VIEW_ERROR_DATA: 'VIEW_ERROR_DATA',
    EDIT: 'EDIT',
    DELETE: 'DELETE',
    AUDIT_EDIT: 'AUDIT_EDIT',
    CONFIRM: 'CONFIRM',
    FEEDBACK: 'FEEDBACK',
    TERMINATION: 'TERMINATION',
    REVOKE: 'REVOKE',
    ENABLE: 'ENABLE',
    DISABLE: 'DISABLE',
    TOP: 'TOP',
    CANCEL_TOP: 'CANCEL_TOP',
    AUDIT: 'AUDIT',
    ACCEPT: 'ACCEPT',
    RE_ACCEPT: 'RE_ACCEPT',
    MOVE: 'MOVE',
    PUBLICITY: 'PUBLICITY',
    CANCEL_PUBLICITY: 'CANCEL_PUBLICITY',
    SET_AS_CAROUSEL: 'SET_AS_CAROUSEL',
    APPROVE: 'APPROVE',
    REJECT: 'REJECT',
    RE_SUBMIT: 'RE_SUBMIT',
    SHIELD: 'SHIELD',
    MOTHBALL: 'MOTHBALL',
    DOWNLOAD: 'DOWNLOAD',
    NO_OPERATION: 'NO_OPERATION',
  },
  PAGE_SCENE: {
    ALL: 'MA',
    PAGE: 'Lw',
  },
  STATUS: {
    PENDING: 'Lw',
    DISABLE: 'LC0',
    ENABLE: 'Lw',
    NORMAL: 'Lw',
    FAILURE: 'LC0',
    REVOKE: 'LC0',
    PUBLICITY: 'MQ',
    FAILURE_DOWNLOAD: 'LC4',
    SUCCESS: 'MQ',
    MOTHBALL: 'LC0',
    SHIELD: 'LC0',
    CONFIRM: 'MQ',
  },
  STICK: {
    TOP: 'MQ',
    CANCEL_TOP: 'Lw',
  },
  BANNER_STATUS: {
    SET: 'MQ',
    UN_SET: 'Lw',
  },
  HOME_PAGE_SHOW: {
    SHOW: 'MQ',
    UN_SHOW: 'Lw',
  },
  // Lw=>待审核 MQ=>已通过 LC0=>已驳回
  APPLY_STATUS: {
    PADDING: 'Lw',
    APPROVE: 'MQ',
    REJECT: 'LC0',
  },
  // Lw:待受理 MA:受理中 MQ:受理完成
  ACCEPT_STATUS: {
    PADDING: 'Lw',
    ACCEPTING: 'MA',
    COMPLETED: 'MQ',
  },
  ADMISSIBILITY_VALUE: {
    BE_ACCEPTED: 'MA',
    NOT_ACCEPTED: 'MQ',
  },
  PURVIEW: {
    SUPER_ADMIN: 'MA',
    PLATFORM_ADMIN: 'MQ',
    USER_GROUP_ADMIN: 'Mg',
    OPERATION_CREW: 'Mw',
  },
  FILTER_TYPE: {
    VISIBLE: 'isVisible',
    PURVIEW: 'dynamicPermissions',
  },
}

function install (Vue) {
  // eslint-disable-next-line no-prototype-builtins
  if (Vue.prototype.hasOwnProperty(CONSTANTS_PLUGIN_KEY)) return

  Object.defineProperties(Vue.prototype, {
    [CONSTANTS_PLUGIN_KEY]: { get: () => CONSTANTS },
  })
}

CONSTANTS.install = install

if (process.env.NODE_ENV === 'development') {
  window[CONSTANTS_PLUGIN_EXPORT_KEY] = CONSTANTS
}

Vue.use(CONSTANTS)
