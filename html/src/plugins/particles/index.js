/**
 * vue-particles
 *
 * @module plugins/particles
 */

import Vue from 'vue'
import VueParticles from 'vue-particles'

Vue.use(VueParticles)
