/**
 * 接口 - 科室
 * @module services/department
 */

import request from '@/utils/request'

/**
 * 获取科室列表
 *
 * "userGroup": "Lw",
 * "departmentId" : "科室id",
 * "departmentName" : "科室关键词",
 * "page": 1,
 * "limit": 10,
 * "sort": "updateTime", -updateTime=>按最后更新时间倒序/updateTime=>按最后更新时间正序
 * "pageScene": "Lw" Lw=> 分页获取/MA=> 获取全部
 */
export function fetchDepartmentList (params) {
  return request(`/api/departments`, { params })
}

/**
 * 获取科室详情
 */
export function fetchDepartmentDetail (id, params) {
  return request(`/api/departments/${id}`, { params })
}

/**
 * 编辑科室
 * @param {*} id
 * @param {*} params
 */
export function fetchDepartmentEdit (id, params) {
  // return request(`/api/departments/${id}/edit`, {
  return request(`/api/departments/${id}`, {
    method: 'GET',
    params,
  })
}

/**
 * 编辑科室
 * @param {*} id
 * @param {*} params
 */
export function updateDepartment (id, params) {
  return request(`/api/departments/${id}/edit`, {
    method: 'POST',
    params,
  })
}

/**
 * 新增科室
 * @param {*} params
 */
export function createDepartment (params) {
  return request(`/api/departments/add`, {
    method: 'POST',
    params,
  })
}

/**
 * 启用科室
 * @param {*} params
 */
export function enableDepartment (id) {
  return request(`/api/departments/${id}/enable`, {
    method: 'POST',
  })
}

/**
 * 禁用科室
 * @param {*} params
 */
export function disableDepartment (id) {
  return request(`/api/departments/${id}/disable`, {
    method: 'POST',
  })
}
