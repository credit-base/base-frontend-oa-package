/**
 * @file Mock Overview Large
 * @module services/overview-large
 */

const mock = {
  wbjList: [
    {
      name: '税务局',
      id: 'WBJ_1',
      status: 'success',
      statusText: '数据活跃',
      value: 100,
      targets: [
        {
          id: 'WBJ_CATALOG_1',
          rule: {
            id: 'RULE_1',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_6',
          rule: {
            id: 'RULE_17',
            status: `danger`,
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_7',
          rule: {
            id: 'RULE_18',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_8',
          rule: {
            id: 'RULE_19',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '地震局',
      id: 'WBJ_2',
      status: 'success',
      statusText: '暂无资源目录',
      value: 100,
      targets: [],
    },
    {
      name: '民政局',
      id: 'WBJ_3',
      status: 'success',
      statusText: '报送活跃',
      value: 100,
      targets: [
        {
          id: 'WBJ_CATALOG_2',
          rule: {
            id: 'RULE_2',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_4',
          rule: {
            id: 'RULE_3',
            status: `danger`,
            name: '上传',
          },
        },
      ],
    },
    {
      name: '市场监督管理局',
      id: 'WBJ_4',
      status: 'success',
      statusText: '数据迟报',
      value: 22,
      targets: [
        {
          id: 'WBJ_CATALOG_3',
          rule: {
            id: 'RULE_4',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '铁路局',
      id: 'WBJ_5',
      status: 'disabled',
      statusText: '无资源目录',
      value: 88,
      targets: [],
    },
    {
      name: '市纪委监委',
      id: 'WBJ_13',
      status: 'success',
      statusText: '数据活跃',
      value: 100,
      targets: [
        {
          id: 'WBJ_CATALOG_1',
          rule: {
            id: 'RULE_1',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_6',
          rule: {
            id: 'RULE_17',
            status: `danger`,
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_7',
          rule: {
            id: 'RULE_18',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_8',
          rule: {
            id: 'RULE_19',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '市供销社',
      id: 'WBJ_6',
      status: 'disabled',
      statusText: '数据活跃',
      value: 88,
      targets: [],
    },
    {
      name: '市妇联',
      id: 'WBJ_7',
      status: 'disabled',
      statusText: '数据过少',
      value: 5623,
      targets: [],
    },
    {
      name: '邮政管理局',
      id: 'WBJ_8',
      status: 'disabled',
      statusText: '目录出错',
      value: 2324,
      targets: [],
    },
    {
      name: '知识产权局',
      id: 'WBJ_9',
      status: 'disabled',
      statusText: '目录出错',
      value: 12888,
      targets: [],
    },
    {
      name: '市交通运输局',
      id: 'WBJ_10',
      status: 'disabled',
      statusText: '上月瞒报',
      value: 4575,
      targets: [],
    },
    {
      name: '市气象局',
      id: 'WBJ_11',
      status: 'disabled',
      statusText: '目录出错',
      value: 4575,
      targets: [],
    },
    {
      name: '市公安局',
      id: 'WBJ_12',
      status: 'disabled',
      statusText: '目录出错',
      value: 4575,
      targets: [],
    },
    {
      name: '市委编办',
      id: 'WBJ_14',
      status: 'disabled',
      statusText: '暂无资源目录',
      value: 100,
      targets: [],
    },
    {
      name: '市委宣传部（市文明办）',
      id: 'WBJ_15',
      status: 'success',
      statusText: '报送活跃',
      value: 100,
      targets: [
        {
          id: 'WBJ_CATALOG_2',
          rule: {
            id: 'RULE_2',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_4',
          rule: {
            id: 'RULE_3',
            status: `danger`,
            name: '上传',
          },
        },
      ],
    },
    {
      name: '市场监督管理局',
      id: 'WBJ_16',
      status: 'warning',
      statusText: '数据迟报',
      value: 22,
      targets: [
        {
          id: 'WBJ_CATALOG_3',
          rule: {
            id: 'RULE_4',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '民航管理处',
      id: 'WBJ_17',
      status: 'disabled',
      statusText: '无资源目录',
      value: 88,
      targets: [],
    },
    {
      name: '市法院',
      id: 'WBJ_18',
      status: 'disabled',
      statusText: '数据活跃',
      value: 88,
      targets: [],
    },
    {
      name: '人行陕西省中支',
      id: 'WBJ_19',
      status: 'disabled',
      statusText: '数据过少',
      value: 5623,
      targets: [],
    },
    {
      name: '临港经开区发展策划投资服务局',
      id: 'WBJ_20',
      status: 'disabled',
      statusText: '数据活跃',
      value: 2324,
      targets: [],
    },
    {
      name: '兴文县发展和改革局',
      id: 'WBJ_21',
      status: 'disabled',
      statusText: '数据活跃',
      value: 12888,
      targets: [],
    },
    {
      name: '高县发展和改革局',
      id: 'WBJ_22',
      status: 'disabled',
      statusText: '上月瞒报',
      value: 4575,
      targets: [],
    },
    {
      name: '改革局',
      id: 'WBJ_23',
      status: 'disabled',
      statusText: '数据活跃',
      value: 4575,
      targets: [],
    },
    {
      name: '市妇联',
      id: 'WBJ_24',
      status: 'disabled',
      statusText: '数据活跃',
      value: 4575,
      targets: [],
    },
    {
      name: '税务局',
      id: 'WBJ_25',
      status: 'success',
      statusText: '数据活跃',
      value: 100,
      targets: [
        {
          id: 'WBJ_CATALOG_1',
          rule: {
            id: 'RULE_1',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_6',
          rule: {
            id: 'RULE_17',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_7',
          rule: {
            id: 'RULE_18',
            status: 'danger',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_8',
          rule: {
            id: 'RULE_19',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '地震局',
      id: 'WBJ_26',
      status: 'success',
      statusText: '暂无资源目录',
      value: 100,
      targets: [],
    },
    {
      name: '民政局',
      id: 'WBJ_27',
      status: 'success',
      statusText: '报送活跃',
      value: 100,
      targets: [
        {
          id: 'WBJ_CATALOG_2',
          rule: {
            id: 'RULE_2',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_4',
          rule: {
            id: 'RULE_3',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '市场监督管理局',
      id: 'WBJ_28',
      status: 'success',
      statusText: '数据迟报',
      value: 22,
      targets: [
        {
          id: 'WBJ_CATALOG_3',
          rule: {
            id: 'RULE_4',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '铁路局',
      id: 'WBJ_29',
      status: 'success',
      statusText: '无资源目录',
      value: 88,
      targets: [],
    },
    {
      name: '市供销社',
      id: 'WBJ_30',
      status: 'success',
      statusText: '数据活跃',
      value: 88,
      targets: [],
    },
    {
      name: '市妇联',
      id: 'WBJ_31',
      status: 'success',
      statusText: '数据过少',
      value: 5623,
      targets: [],
    },
    {
      name: '邮政管理局',
      id: 'WBJ_32',
      status: 'success',
      statusText: '数据活跃',
      value: 2324,
      targets: [],
    },
    {
      name: '知识产权局',
      id: 'WBJ_33',
      status: 'success',
      statusText: '数据活跃',
      value: 12888,
      targets: [],
    },
    {
      name: '市交通运输局',
      id: 'WBJ_34',
      status: 'success',
      statusText: '上月瞒报',
      value: 4575,
      targets: [],
    },
    {
      name: '市气象局',
      id: 'WBJ_35',
      status: 'success',
      statusText: '数据活跃',
      value: 4575,
      targets: [],
    },
    {
      name: '市公安局',
      id: 'WBJ_36',
      status: 'success',
      statusText: '数据活跃',
      value: 4575,
      targets: [],
    },
    {
      name: '市纪委监委',
      id: 'WBJ_37',
      status: 'success',
      statusText: '数据活跃',
      value: 100,
      targets: [
        {
          id: 'WBJ_CATALOG_1',
          rule: {
            id: 'RULE_1',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_6',
          rule: {
            id: 'RULE_17',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_7',
          rule: {
            id: 'RULE_18',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_8',
          rule: {
            id: 'RULE_19',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '市委编办',
      id: 'WBJ_38',
      status: 'success',
      statusText: '暂无资源目录',
      value: 100,
      targets: [],
    },
    {
      name: '市委宣传部（市文明办）',
      id: 'WBJ_39',
      status: 'success',
      statusText: '报送活跃',
      value: 100,
      targets: [
        {
          id: 'WBJ_CATALOG_2',
          rule: {
            id: 'RULE_2',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_4',
          rule: {
            id: 'RULE_3',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '市场监督管理局',
      id: 'WBJ_40',
      status: 'success',
      statusText: '数据迟报',
      value: 22,
      targets: [
        {
          id: 'WBJ_CATALOG_3',
          rule: {
            id: 'RULE_4',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '民航管理处',
      id: 'WBJ_41',
      status: 'success',
      statusText: '无资源目录',
      value: 88,
      targets: [],
    },
    {
      name: '市法院',
      id: 'WBJ_42',
      status: 'success',
      statusText: '数据活跃',
      value: 88,
      targets: [],
    },
    {
      name: '人行陕西省中支',
      id: 'WBJ_43',
      status: 'success',
      statusText: '数据过少',
      value: 5623,
      targets: [],
    },
    {
      name: '发展策划投资服务局',
      id: 'WBJ_44',
      status: 'success',
      statusText: '数据活跃',
      value: 2324,
      targets: [],
    },
    {
      name: '兴文县发展和改革局',
      id: 'WBJ_45',
      status: 'success',
      statusText: '数据活跃',
      value: 12888,
      targets: [],
    },
    {
      name: '高县发展和改革局',
      id: 'WBJ_46',
      status: 'success',
      statusText: '上月瞒报',
      value: 4575,
      targets: [],
    },
    {
      name: '南溪区发展和改革局',
      id: 'WBJ_47',
      status: 'success',
      statusText: '数据活跃',
      value: 4575,
      targets: [],
    },
    {
      name: '市妇联',
      id: 'WBJ_48',
      status: 'success',
      statusText: '数据活跃',
      value: 4575,
      targets: [],
    },
    {
      name: '民航管理处',
      id: 'WBJ_49',
      status: 'success',
      statusText: '无资源目录',
      value: 88,
      targets: [],
    },
    {
      name: '市法院',
      id: 'WBJ_50',
      status: 'success',
      statusText: '数据活跃',
      value: 88,
      targets: [],
    },
    {
      name: '人行陕西省中支',
      id: 'WBJ_51',
      status: 'success',
      statusText: '数据过少',
      value: 5623,
      targets: [],
    },
    {
      name: '策划投资服务局',
      id: 'WBJ_52',
      status: 'success',
      statusText: '数据活跃',
      value: 2324,
      targets: [],
    },
    {
      name: '兴文县发展和改革局',
      id: 'WBJ_53',
      status: 'success',
      statusText: '数据活跃',
      value: 12888,
      targets: [],
    },
    {
      name: '高县发展和改革局',
      id: 'WBJ_54',
      status: 'success',
      statusText: '上月瞒报',
      value: 4575,
      targets: [],
    },
    {
      name: '区发展和改革局',
      id: 'WBJ_55',
      status: 'success',
      statusText: '数据活跃',
      value: 4575,
      targets: [],
    },
  ],
  wbjCatalogList: [
    {
      name: '三好学生-税务局',
      id: 'WBJ_CATALOG_1',
      status: 'success',
      statusText: '数据上传活跃',
      value: 100,
      type: 'RED_CATALOG',
      userGroup: 'WBJ_1',
      targets: [
        {
          id: 'GB_CATALOG_1',
          rule: {
            id: 'RULE_5',
            name: '转换',
          },
        },
        {
          id: 'BJ_CATALOG_1',
          rule: {
            id: 'RULE_6',
            status: `danger`,
            name: '转换',
          },
        },
      ],
    },
    {
      name: '红名单-民政局',
      id: 'WBJ_CATALOG_2',
      status: 'success',
      statusText: '数据上传活跃',
      value: 100,
      type: 'RED_CATALOG',
      userGroup: 'WBJ_3',
      targets: [
        {
          id: 'GB_CATALOG_1',
          rule: {
            id: 'RULE_7',
            name: '转换',
          },
        },
        {
          id: 'BJ_CATALOG_1',
          rule: {
            id: 'RULE_8',
            status: `danger`,
            name: '转换',
          },
        },
      ],
    },
    {
      name: '黑名单-民政局',
      id: 'WBJ_CATALOG_4',
      status: 'success',
      statusText: '特殊需求',
      type: 'BLACK_CATALOG',
      value: 100,
      userGroup: 'WBJ_3',
      targets: [
        {
          id: 'CLOUD_DATABASE_2',
          rule: {
            id: 'RULE_9',
            name: '报送',
          },
        },
      ],
    },
    {
      name: '变更登记信息-市场监督管理局',
      id: 'WBJ_CATALOG_3',
      status: 'warning',
      statusText: '特殊需求',
      value: 100,
      userGroup: 'WBJ_4',
      targets: [
        {
          id: 'CLOUD_DATABASE_4',
          rule: {
            id: 'RULE_10',
            status: `danger`,
            name: '报送',
          },
        },
        {
          id: 'BJ_CATALOG_1',
          rule: {
            id: 'RULE_99',
            name: '报送',
          },
        },
      ],
    },
    {
      name: '企业固定资产档案',
      id: 'WBJ_CATALOG_5',
      status: 'success',
      statusText: '数据活跃',
      value: 450,
      userGroup: 'WBJ_5',
      targets: [
        {
          id: 'BJ_CATALOG_2',
          rule: {
            id: 'RULE_95',
            name: '报送',
          },
        },
      ],
    },
    {
      name: '行政许可-税务局',
      id: 'WBJ_CATALOG_6',
      status: 'success',
      statusText: '数据活跃',
      value: 450,
      userGroup: 'WBJ_1',
      targets: [
        {
          id: 'GB_CATALOG_2',
          rule: {
            id: 'RULE_15',
            name: '转换',
          },
        },
        {
          id: 'BJ_CATALOG_2',
          rule: {
            id: 'RULE_16',
            name: '转换',
          },
        },
      ],
    },
    {
      name: '三好学生-税务局',
      id: 'WBJ_CATALOG_7',
      status: 'success',
      statusText: '数据活跃',
      value: 450,
      userGroup: 'WBJ_1',
      targets: [
        {
          id: 'GB_CATALOG_2',
          rule: {
            id: 'RULE_15',
            name: '转换',
          },
        },
      ],
    },
    {
      name: '守信企业信息-税务局',
      id: 'WBJ_CATALOG_8',
      status: 'success',
      statusText: '数据活跃',
      value: 450,
      userGroup: 'WBJ_1',
      targets: [
        {
          id: 'GB_CATALOG_2',
          rule: {
            id: 'RULE_15',
            status: `danger`,
            name: '转换',
          },
        },
      ],
    },
  ],
  gbCatalogList: [
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_1',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_2',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_3',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            status: `danger`,
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_4',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_5',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_6',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_7',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            status: `danger`,
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_8',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_9',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_10',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_11',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          // eslint-disable-next-line max-lines
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_12',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_13',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_14',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_15',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_16',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_17',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_18',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_19',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_20',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_21',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_22',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_23',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_24',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_25',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_26',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_27',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_28',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_29',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_30',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_31',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_32',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
  ],
  bjCatalogList: [
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_1',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_2',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_3',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_4',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            status: `danger`,
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_5',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_6',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_7',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_8',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_9',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            status: `danger`,
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_10',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_11',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_12',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_13',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_14',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_15',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_16',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_17',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_18',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_19',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_20',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_21',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_22',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_23',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_24',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_25',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_26',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_27',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_28',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_29',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_30',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_31',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_32',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_33',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
  ],
  cloudDatabaseList: [
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_1',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_2',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_3',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_4',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            status: `danger`,
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_5',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_6',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_7',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            status: `danger`,
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_8',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_9',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_10',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_11',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_12',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_13',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_14',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_15',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_16',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_17',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_18',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_19',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_20',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_21',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_22',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_23',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_24',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_25',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_26',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_27',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_28',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_29',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_30',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_31',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_32',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_33',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_34',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_35',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_36',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_37',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_38',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_39',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_40',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_41',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_42',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_43',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_44',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_45',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_46',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_47',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_48',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_49',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_50',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_51',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_52',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_53',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_54',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_55',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_56',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_57',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_58',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_59',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_60',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_61',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_62',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_63',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_64',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_5',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_65',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_67',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_68',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_6',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_69',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_8',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_70',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_6',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_71',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_5',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_72',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_73',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_74',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_9',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_75',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_76',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_78',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_79',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_80',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_81',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_82',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_83',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_84',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_85',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_86',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_87',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_88',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_89',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_90',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_91',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_92',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_93',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_94',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_95',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_96',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_97',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_98',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_99',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_100',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_101',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_102',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_103',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_104',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_105',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_106',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_107',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_108',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_109',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_110',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_111',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_112',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_113',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_114',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_115',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_116',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_117',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_118',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_119',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_120',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_121',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_122',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
  ],
  dataUsageList: [
    {
      name: '平台应用',
      id: 'DATA_USAGE_1',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '接口应用',
      id: 'DATA_USAGE_2',
      status: 'success',
      statusText: '调用正常',
      value: 70,
      targets: [],
    },
    {
      name: '信易+煤炭',
      id: 'DATA_USAGE_3',
      status: 'success',
      statusText: '调用正常',
      value: 50,
      targets: [],
    },
    {
      name: '信易贷',
      id: 'DATA_USAGE_4',
      status: 'success',
      statusText: '调用正常',
      value: 89,
      targets: [],
    },
    {
      name: '信易行',
      id: 'DATA_USAGE_5',
      status: 'warning',
      statusText: '调用不活跃',
      value: 20,
      targets: [],
    },
    {
      name: '信用玉雕6',
      id: 'DATA_USAGE_6',
      status: 'warning',
      statusText: '近期调用少',
      value: 20,
      targets: [],
    },
    {
      name: '接口应用7',
      id: 'DATA_USAGE_7',
      status: 'success',
      statusText: '调用正常',
      value: 70,
      targets: [],
    },
    {
      name: '信易+煤炭8',
      id: 'DATA_USAGE_8',
      status: 'success',
      statusText: '调用正常',
      value: 50,
      targets: [],
    },
    {
      name: '信易贷9',
      id: 'DATA_USAGE_9',
      status: 'success',
      statusText: '调用正常',
      value: 89,
      targets: [],
    },
    {
      name: '信易行10',
      id: 'DATA_USAGE_10',
      status: 'warning',
      statusText: '调用不活跃',
      value: 20,
      targets: [],
    },
  ],
}

export default mock
