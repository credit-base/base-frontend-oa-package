/**
 * @file Mock Overview All
 * @module services/overview-all
 */

const mock = {
  wbjList: [
    {
      name: '税务局',
      id: 'WBJ_1',
      status: 'success',
      statusText: '上传正常',
      value: 100,
      targets: [
        {
          id: 'WBJ_CATALOG_1',
          rule: {
            id: 'RULE_1',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_6',
          rule: {
            id: 'RULE_17',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_7',
          rule: {
            id: 'RULE_18',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_8',
          rule: {
            id: 'RULE_19',
            status: 'danger',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '地震局',
      id: 'WBJ_2',
      status: 'danger',
      statusText: '暂无资源目录',
      value: 100,
      targets: [],
    },
    {
      name: '民政局',
      id: 'WBJ_3',
      status: 'success',
      statusText: '报送活跃',
      value: 100,
      targets: [
        {
          id: 'WBJ_CATALOG_2',
          rule: {
            id: 'RULE_2',
            name: '上传',
          },
        },
        {
          id: 'WBJ_CATALOG_4',
          rule: {
            id: 'RULE_3',
            status: 'danger',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '市场监督管理局',
      id: 'WBJ_4',
      status: 'warning',
      statusText: '数据迟报',
      value: 22,
      targets: [
        {
          id: 'WBJ_CATALOG_3',
          rule: {
            id: 'RULE_4',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '铁路局',
      id: 'WBJ_5',
      status: 'disabled',
      statusText: '无资源目录',
      value: 88,
      targets: [],
    },
    {
      name: '市供销社',
      id: 'WBJ_6',
      status: 'success',
      statusText: '数据活跃',
      value: 88,
      targets: [
        {
          id: 'WBJ_CATALOG_5',
          rule: {
            id: 'RULE_98',
            name: '上传',
          },
        },
      ],
    },
    {
      name: '市妇联',
      id: 'WBJ_7',
      status: 'danger',
      statusText: '数据过少',
      value: 5623,
      targets: [],
    },
    {
      name: '邮政管理局',
      id: 'WBJ_8',
      status: 'success',
      statusText: '数据活跃',
      value: 2324,
      targets: [],
    },
    {
      name: '知识产权局',
      id: 'WBJ_9',
      status: 'success',
      statusText: '数据活跃',
      value: 12888,
      targets: [],
    },
    {
      name: '市交通运输局',
      id: 'WBJ_10',
      status: 'warning',
      statusText: '上月瞒报',
      value: 4575,
      targets: [],
    },
    {
      name: '市气象局',
      id: 'WBJ_11',
      status: 'success',
      statusText: '数据活跃',
      value: 4575,
      targets: [],
    },
    {
      name: '市公安局',
      id: 'WBJ_12',
      status: 'success',
      statusText: '数据活跃',
      value: 4575,
      targets: [],
    },
  ],
  wbjCatalogList: [
    {
      name: '三好学生-税务局',
      id: 'WBJ_CATALOG_1',
      status: 'success',
      statusText: '数据上传活跃',
      value: 100,
      type: 'RED_CATALOG',
      userGroup: 'WBJ_1',
      targets: [
        {
          id: 'GB_CATALOG_1',
          rule: {
            id: 'RULE_5',
            name: '转换',
          },
        },
        {
          id: 'BJ_CATALOG_1',
          rule: {
            id: 'RULE_6',
            name: '转换',
          },
        },
      ],
    },
    {
      name: '红名单-民政局',
      id: 'WBJ_CATALOG_2',
      status: 'success',
      statusText: '数据上传活跃',
      value: 100,
      type: 'RED_CATALOG',
      userGroup: 'WBJ_3',
      targets: [
        {
          id: 'GB_CATALOG_1',
          rule: {
            id: 'RULE_7',
            name: '转换',
          },
        },
        {
          id: 'BJ_CATALOG_1',
          rule: {
            id: 'RULE_8',
            name: '转换',
          },
        },
      ],
    },
    {
      name: '黑名单-民政局',
      id: 'WBJ_CATALOG_4',
      status: 'success',
      statusText: '特殊需求',
      type: 'BLACK_CATALOG',
      value: 100,
      userGroup: 'WBJ_3',
      targets: [
        {
          id: 'CLOUD_DATABASE_2',
          rule: {
            id: 'RULE_9',
            name: '报送',
          },
        },
      ],
    },
    {
      name: '变更登记信息-市场监督管理局',
      id: 'WBJ_CATALOG_3',
      status: 'warning',
      statusText: '特殊需求',
      value: 100,
      userGroup: 'WBJ_4',
      targets: [
        {
          id: 'CLOUD_DATABASE_4',
          rule: {
            id: 'RULE_10',
            name: '报送',
          },
        },
        {
          id: 'BJ_CATALOG_1',
          rule: {
            id: 'RULE_99',
            name: '报送',
          },
        },
      ],
    },
    {
      name: '企业固定资产档案',
      id: 'WBJ_CATALOG_5',
      status: 'success',
      statusText: '数据活跃',
      value: 450,
      userGroup: 'WBJ_5',
      targets: [
        {
          id: 'BJ_CATALOG_2',
          rule: {
            id: 'RULE_92',
            name: '报送',
          },
        },
      ],
    },
    {
      name: '行政许可-税务局',
      id: 'WBJ_CATALOG_6',
      status: 'success',
      statusText: '数据活跃',
      value: 450,
      userGroup: 'WBJ_1',
      targets: [
        {
          id: 'GB_CATALOG_2',
          rule: {
            id: 'RULE_15',
            name: '转换',
          },
        },
        {
          id: 'BJ_CATALOG_2',
          rule: {
            id: 'RULE_16',
            name: '转换',
          },
        },
      ],
    },
    {
      name: '三好学生-税务局',
      id: 'WBJ_CATALOG_7',
      status: 'success',
      statusText: '数据活跃',
      value: 450,
      userGroup: 'WBJ_1',
      targets: [
        {
          id: 'GB_CATALOG_2',
          rule: {
            id: 'RULE_15',
            name: '转换',
          },
        },
      ],
    },
    {
      name: '守信企业信息-税务局',
      id: 'WBJ_CATALOG_8',
      status: 'success',
      statusText: '数据活跃',
      value: 450,
      userGroup: 'WBJ_1',
      targets: [
        {
          id: 'GB_CATALOG_2',
          rule: {
            id: 'RULE_15',
            name: '转换',
          },
        },
      ],
    },
  ],
  gbCatalogList: [
    {
      name: '红名单-国标',
      id: 'GB_CATALOG_1',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_11',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-国标',
      id: 'GB_CATALOG_2',
      status: 'success',
      statusText: '调用正常',
      value: 200,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_20',
            name: '交换',
          },
        },
      ],
    },
  ],
  bjCatalogList: [
    {
      name: '红名单-本级',
      id: 'BJ_CATALOG_1',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_1',
          rule: {
            id: 'RULE_12',
            name: '交换',
          },
        },
      ],
    },
    {
      name: '行政许可-本级',
      id: 'BJ_CATALOG_2',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'CLOUD_DATABASE_3',
          rule: {
            id: 'RULE_21',
            name: '交换',
          },
        },
      ],
    },
  ],
  cloudDatabaseList: [
    {
      name: '红名单-前置机',
      id: 'CLOUD_DATABASE_1',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_13',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '黑名单-前置机',
      id: 'CLOUD_DATABASE_2',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_2',
          rule: {
            id: 'RULE_14',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政许可-前置机',
      id: 'CLOUD_DATABASE_3',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_3',
          rule: {
            id: 'RULE_15',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '行政处罚-前置机',
      id: 'CLOUD_DATABASE_4',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [
        {
          id: 'DATA_USAGE_1',
          rule: {
            id: 'RULE_16',
            name: '应用',
          },
        },
      ],
    },
    {
      name: '守信信息-前置机',
      id: 'CLOUD_DATABASE_5',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '失信信息-前置机',
      id: 'CLOUD_DATABASE_6',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
  ],
  dataUsageList: [
    {
      name: '平台应用',
      id: 'DATA_USAGE_1',
      status: 'success',
      statusText: '调用正常',
      value: 100,
      targets: [],
    },
    {
      name: '接口应用',
      id: 'DATA_USAGE_2',
      status: 'success',
      statusText: '调用正常',
      value: 70,
      targets: [],
    },
    {
      name: '信易+煤炭',
      id: 'DATA_USAGE_3',
      status: 'success',
      statusText: '调用正常',
      value: 50,
      targets: [],
    },
    {
      name: '信易贷',
      id: 'DATA_USAGE_4',
      status: 'success',
      statusText: '调用正常',
      value: 89,
      targets: [],
    },
    {
      name: '信易行',
      id: 'DATA_USAGE_5',
      status: 'warning',
      statusText: '调用不活跃',
      value: 20,
      targets: [],
    },
    {
      name: '信用玉雕',
      id: 'DATA_USAGE_6',
      status: 'warning',
      statusText: '近期调用少',
      value: 20,
      targets: [],
    },
  ],
}

export default mock
