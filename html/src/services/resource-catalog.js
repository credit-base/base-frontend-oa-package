/**
 * @file Mock Resource Catalog
 * @module services/overview-all
 */

const mock = [
  {
    value: 'MA',
    label: '国标',
    children: [
      {
        value: 'GB_CATALOG_1',
        label: '红名单-国标',
      },
    ],
  },
  {
    value: 'MQ',
    label: '本级',
    children: [
      {
        value: 'BJ_CATALOG_1',
        label: '红名单-本级',
      },
    ],
  },
  {
    value: 'WBJ_1',
    label: '税务局',
    children: [
      {
        value: 'WBJ_CATALOG_1',
        label: '红名单-税务局',
      },
    ],
  },
  {
    value: 'WBJ_3',
    label: '民政局',
    children: [
      {
        value: 'WBJ_CATALOG_2',
        label: '红名单-民政局',
      },
      {
        value: 'WBJ_CATALOG_4',
        label: '黑名单-民政局',
      },
    ],
  },
]

export default mock
