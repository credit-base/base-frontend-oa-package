/**
 * 接口 - 委办局
 * @module services/user-group
 */

import { USER_GROUP_DETAIL_DATA } from '@/__mocks__/user-groups'

import request from '@/utils/request'

/**
 * 获取委办局列表
 *
 * "name":"关键字",
 * "page": 1,
 * "limit": 10,
 * "sort": "-updateTime", -updateTime=>按最后更新时间倒序/updateTime=>按最后更新时间正序
 * "pageScene" : "Lw=> 分页获取  MA=> 获取全部"
 */
export function fetchUserGroupList (params = {}) {
  return request(`/api/userGroups`, { params })
}

/**
 * 获取委办局详情
 */
export function fetchUserGroupDetail (id, params = {}) {
  if (params.useMock) {
    return {
      status: 1,
      data: {
        ...USER_GROUP_DETAIL_DATA,
      },
    }
  }
  return request(`/api/userGroups/${id}`, { params })
}
