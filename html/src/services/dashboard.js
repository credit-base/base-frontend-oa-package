/**
 * 驾驶舱
 * @module services/dashboard
 */

import request from '@/utils/request'
import { getRecentMonths } from '@/plugins/echarts/utils'
import { randomNum } from '@/utils'

/**
 * 左二
 * 行政许可信息每周的数据总量
 * 行政处罚信息每周的数据总量
 * 守信红榜信息每周的数据总量
 * 失信黑榜信息每周的数据总量
 */
export function fetchStaticsByCategoryPerWeek (id, params = {}) {
  if (params.useMock) {
    const list = {
      totalBlack: [12, 42, 15, 36, 2, 27, 9],
      totalLicense: [4, 12, 42, 18, 27, 31, 29],
      totalRed: [9, 14, 12, 38, 14, 21, 17],
      totalSanction: [14, 4, 8, 8, 10, 12, 42],
    }

    return {
      data: { status: 1, data: { ...list } },
    }
  }

  return request(`/api/statisticals/staticsByCategoryPerWeek?administrativeArea=${id}`, { params })
}

/**
 * 左三
 * @param {*} id
 * @param {*} params
 */
export function fetchStaticsByCategory (id, params = {}) {
  if (params.useMock) {
    const mockData = {
      columns: [
        'name',
        '行政许可',
        '行政处罚',
        '守信红榜',
        '失信黑榜',
        '联合奖惩',
        '信用动态',
      ],
      rows: {
        name: '',
        行政许可: '237',
        行政处罚: '261',
        守信红榜: '173',
        失信黑榜: '347',
        联合奖惩: '210',
        信用动态: '320',
      },
    }
    return {
      data: { status: 1, data: { ...mockData } },
    }
  }

  return request(`/api/statisticals/staticsByCategory?=administrativeArea=${id}`, { params })
}

/**
 * 左四
 * 统计红榜,黑榜,许可信息,处罚信息,资源目录总数,信用承诺
 * 信用申诉,表扬,投诉,信用问答,问题反馈,互动类总数
 */
export function fetchStaticsByCategoryTotal (id, params = {}) {
  if (params.useMock) {
    const tempData = {
      columns: ['category', 'total'],
      rows: [
        { category: '红榜', total: 2 },
        { category: '黑榜', total: 87 },
        { category: '许可信息', total: 3 },
        { category: '处罚信息', total: 3 },
        { category: '信用承诺', total: 0 },
        { category: '信用申诉', total: 27 },
        { category: '表扬', total: 13 },
        { category: '投诉', total: 22 },
        { category: '信用问答', total: 17 },
        { category: '问题反馈', total: 13 },
        { category: '信用申报', total: 8 },
      ],
    }

    return {
      data: {
        status: 1,
        data: { ...tempData },
      },
    }
  }

  return request(`/api/statisticals/staticsByCategoryTotal?administrativeArea=${id}`, { params })
}

/**
 * 右一 联合奖惩数据
 */
export function getRewardPunishmentTotal () {
  const months = getRecentMonths(3)
  const list = months.map(month => ({
    month,
    reward: randomNum(10) * 20, // 激励
    punishment: randomNum(10) * 10, // 惩戒
  }))

  return {
    data: { status: 1, data: { list } },
  }
}

/**
 * 右二
 * 委办局数据总量
 */
export function fetchStaticsByUserGroupAll (id, params = {}) {
  if (params.useMock) {
    const tempData = {
      columns: ['category', 'total'],
      rows: [
        { category: '红榜', total: 2 },
        { category: '黑榜', total: 87 },
        { category: '许可信息', total: 3 },
        { category: '处罚信息', total: 3 },
        { category: '信用承诺', total: 0 },
        { category: '信用申诉', total: 27 },
        { category: '表扬', total: 13 },
        { category: '投诉', total: 22 },
        { category: '信用问答', total: 17 },
        { category: '问题反馈', total: 13 },
        { category: '信用申报', total: 8 },
      ],
    }

    return {
      data: {
        status: 1,
        data: { ...tempData },
      },
    }
  }
  return request(`api/statisticals/staticsByUserGroupAll?=administrativeArea=${id}`, { params })
}

/**
 * 右三
 * 重点监测
 */
export function fetchDetectList (params = {}) {
  if (params.useMock) {
    const tempData = {
      columns: ['category', 'total'],
      rows: [
        { category: '红榜', total: 2 },
        { category: '黑榜', total: 87 },
        { category: '许可信息', total: 3 },
        { category: '处罚信息', total: 3 },
        { category: '信用承诺', total: 0 },
        { category: '信用申诉', total: 27 },
        { category: '表扬', total: 13 },
        { category: '投诉', total: 22 },
        { category: '信用问答', total: 17 },
        { category: '问题反馈', total: 13 },
        { category: '信用申报', total: 8 },
      ],
    }

    return {
      data: {
        status: 1,
        data: { ...tempData },
      },
    }
  }
  return request(`/api/monitorings`, { params })
}

/**
 * 右四
 * 行业信用预警趋势分析
 */
export function getWarningTrendAnalysis () {
  const list = [
    { month: '3月', area: 0, areaLimit: 82, industry: 0, industryLimit: 110 },
    { month: '6月', area: 0, areaLimit: 122, industry: 0, industryLimit: 60 },
    { month: '9月', area: 0, areaLimit: 267, industry: 0, industryLimit: 120 },
    { month: '12月', area: 0, areaLimit: 100, industry: 0, industryLimit: 80 },
  ]

  return {
    data: { status: 1, data: { list } },
  }
}
