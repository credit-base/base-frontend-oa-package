/**
 * @file Services
 * @module src/services
 */

import overviewAll from './overview-all'
import overviewWbj from './overview-wbj'
import overviewLarge from './overview-large'
import resourceCatalog from './resource-catalog'

const overview =
  process.env.NODE_ENV === 'production' ? overviewLarge : overviewAll

/**
 * 获取图谱总览c
 */
export function getGraphOverview (params = {}) {
  return overview
}

/**
 * 获取委办局图谱总览
 */
export function getGraphOverviewWbj (params = {}) {
  return overviewWbj
}

/**
 * 获取委办局列表
 */
export function getUserGroupList (params = {}) {
  const list = overview.wbjList
  const total = list.length
  return { list, total }
}

/**
 * 获取资源目录列表
 */
export function getResourceCatalogList (params = {}) {
  const list = resourceCatalog
  return { list }
}
