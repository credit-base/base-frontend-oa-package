/**
 * 接口 - oss
 * @module services/auth
 */

import request from '@/utils/request'

const url = process.env.NODE_ENV === 'production' ? '/api/authToken' : '/oss/authToken'

export function getOssToken () {
  console.log(url)
  return request(url, { baseURL: process.env.VUE_APP_OSS_HOST })
}
