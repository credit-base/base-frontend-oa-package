/**
 * 接口 - 员工认证
 * @module services/auth
 */

import request from '@/utils/request'

export function signIn (params = {}) {
  return request('/api/signIn', { method: 'POST', params })
}

export function signOut (params = {}) {
  return request('/api/signOut', { params })
}
/**
 * 获取登录员工信息
 * @param {*} params
 */
export function getUserInfo (params = {}) {
  return request(`/api/personalInformation`, { params })
}
