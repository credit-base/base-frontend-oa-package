/**
 * 接口 - 工具
 * @module services/utils
 */

import request from '@/utils/request'

/**
 * 获取加密哈希
 * @param {object} params 请求参数
 */
export function getHash (params) {
  return request('/api/utils/hash', { params })
}

/**
 * 获取阿里OSS配置
 * @param {object} params 请求参数
 */
export function getOssConfig (params) {
  return request('/api/utils/ossResponse', { params })
}
