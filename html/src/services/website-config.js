/**
 * 获取门户配置
 */

import request from '@/utils/request'

/**
 * 获取门户配置版本列表
 */
export function getWebsiteConfigVersionList (params) {
  return request(`/api/websiteCustomizes`, { params })
}

/**
 * 获取门户配置版本详情
 */
export function getWebsiteConfigVersionDetail (id, params) {
  return request(`/api/websiteCustomizes/${id}`, { params })
}

/**
 * 获取首页配置
 */
export function getWebsiteConfigOfHome (params = {}) {
  return request(`/api/websiteCustomize/homePage`, { params })
}

/**
 * 新增门户配置
 */
export function addWebsiteConfig (params) {
  return request(`/api/websiteCustomizes/add`, { method: `POST`, params })
}

/**
 * 发布门户配置
 */
export function publishWebsiteConfig (id, params) {
  return request(`/api/websiteCustomizes/${id}/publish`, { method: `POST`, params })
}
