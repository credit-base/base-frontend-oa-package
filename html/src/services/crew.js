/**
 * 接口 - 员工认证
 * @module services/auth
 */

import request from '@/utils/request'

/**
 * 获取人员列表
 * @param {*} params
 * "search": "人员名称",
 * "userGroup": "MA",
 * "department": "MA",
 * "sort": ["-updateTime"], -updateTime=>按最后更新时间倒序/updateTime=>按最后更新时间正序
 * "page": 1,
 * "limit": 10
 * "scene":
 *    MA=>平台管理员管理
 *    MQ=>委办局管理员管理
 *    Mg=>普通员工管理
 *    Mw=>委办局管理
 *    NA=>科室管理
 */
export function fetchCrew (params) {
  return request(`/api/crews`, { params })
}

/**
 * 获取人员详情
 * @param {*} id
 * @param {*} params
 */
export function fetchCrewDetail (id) {
  return request(`/api/crews/${id}`)
}

/**
 * 新增人员
 * @param {*} params
 */
export function createCrew (params) {
  return request(`/api/crews/add`, {
    method: 'POST',
    params,
  })
}

/**
 * 获取人员编辑信息
 * @param {*} id
 * @param {*} params
 */
export function fetchCrewEditInfo (id, params) {
  return request(`/api/crews/${id}/edit`, {
    method: 'GET',
    params,
  })
}

/**
 * 编辑人员
 * @param {*} id
 * @param {*} params
 */
export function updateCrew (id, params) {
  return request(`/api/crews/${id}/edit`, {
    method: 'POST',
    params,
  })
}

/**
 * 启用/禁用员工
 * @param {*} id
 * @param {*} params
 */
export function updateCrewStatus (id, type) {
  return request(`/api/crews/${id}/${type}`, {
    method: 'POST',
  })
}
