import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'
import i18n from '@/i18n'

import '@/plugins/constants'
import '@/plugins/element'
import '@/plugins/echarts'
import '@/plugins/particles'

import '@/styles/style.scss'
import '@/utils/permission'
import '@/icons'
import '@/components'
import '@/filters'
import '@/mixins'

Vue.config.productionTip = false
Vue.prototype.$bus = new Vue()

// eslint-disable-next-line vue/require-name-property
new Vue({
  router,
  store,
  i18n,
  ...App,
}).$mount('#app')
