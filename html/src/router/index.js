/**
 * @file Router 入口
 * @module router/index
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import asyncRouter from './async-routes'

// https://stackoverflow.com/questions/55523639
if (!process || process.env.NODE_ENV !== 'test') {
  Vue.use(VueRouter)
}

// See https://github.com/vuejs/vue-router/issues/2881
const originalPush = VueRouter.prototype.push

VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

/**
 * 创建路由
 */
const createRouter = () => new VueRouter({
  routes,
  scrollBehavior: () => ({ x: 0, y: 0 }),
})

const router = createRouter()

export const constantRouterMap = routes
export const asyncRouterMap = asyncRouter
/**
 * 重置路由
 * @see https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
 */
const resetRouter = () => {
  const newRouter = createRouter()

  router.matcher = newRouter.matcher
}

export {
  resetRouter,
  router as default,
}
