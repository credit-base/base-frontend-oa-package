/**
 * @file 路由 - 员工管理
 * @module async-routes/permission
 */

import { ROUTER_VISIBLE_CONFIG } from '@/settings'
import permissions from '@/constants/permissions'

const { STATUS_CREW } = ROUTER_VISIBLE_CONFIG

export default [
  {
    path: '/crew/add',
    name: 'CrewAdd',
    hidden: true,
    meta: {
      title: 'systemManage.ROUTER.CREW_ADD',
      activeMenu: '/system-manage',
    },
    component: () => import('@/views/system-manage/crew/add'),
  },
  {
    path: '/crew/edit/:id',
    name: 'CrewEdit',
    hidden: true,
    meta: {
      title: 'systemManage.ROUTER.CREW_EDIT',
      activeMenu: '/system-manage',
    },
    component: () => import('@/views/system-manage/crew/edit'),
  },
  {
    path: '/crew/detail/:id',
    name: 'CrewDetail',
    hidden: true,
    meta: {
      title: 'systemManage.ROUTER.CREW_DETAIL',
      activeMenu: '/system-manage',
    },
    component: () => import('@/views/system-manage/crew/detail'),
  },
  // 员工
  {
    path: '/crew',
    name: 'CrewList',
    meta: {
      title: 'systemManage.ROUTER.CREW_LIST',
      activeMenu: '/crew',
      icon: 'crew',
      isVisible: STATUS_CREW,
      routerId: permissions.CREW_ID,
    },
    component: () => import('@/views/system-manage/crew/list'),
  },
]
