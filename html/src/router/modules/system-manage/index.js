/**
 * @file 路由 - 系统管理
 * @module async-routes/system-manage
 */

import Layout from '@/layout'
import crew from './crew'
import userGroup from './user-group'

export default [
  {
    path: '/system-manage',
    name: 'SystemManage',
    component: Layout,
    alwaysShow: true,
    meta: {
      title: 'systemManage.ROUTER.SYSTEM_MANAGE',
      icon: 'system',
      breadcrumb: false,
    },
    children: [
      ...userGroup,
      ...crew,
      {
        path: '/website-config',
        name: 'WebsiteConfig',
        component: () => import('@/views/system-manage/website-config'),
        meta: {
          title: `systemManage.ROUTER.WEBSITE_CONFIG`,
          icon: `system`,
          dynamicPermissions: false,
        },
      },
    ],
  },
]
