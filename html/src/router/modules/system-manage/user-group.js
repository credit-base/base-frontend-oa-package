/**
 * @file 路由 - 委办局管理
 * @module async-routes/user-group
 */

import { ROUTER_VISIBLE_CONFIG } from '@/settings'
import permissions from '@/constants/permissions'

const { STATUS_USER_GROUP } = ROUTER_VISIBLE_CONFIG

export default [
  // 科室
  {
    path: '/department/add',
    name: 'DepartmentAdd',
    hidden: true,
    meta: {
      title: 'systemManage.ROUTER.DEPARTMENT_ADD',
      activeMenu: '/user-groups',
    },
    component: () => import('@/views/system-manage/user-group/add/department'),
  },

  {
    path: '/department/detail/:id',
    name: 'DepartmentDetail',
    hidden: true,
    meta: {
      title: 'systemManage.ROUTER.DEPARTMENT_DETAIL',
      activeMenu: '/user-groups',
    },
    component: () => import('@/views/system-manage/user-group/detail/department'),
  },

  {
    path: '/department/edit/:id',
    name: 'DepartmentEdit',
    hidden: true,
    meta: {
      title: 'systemManage.ROUTER.DEPARTMENT_EDIT',
      activeMenu: '/user-groups',
    },
    component: () => import('@/views/system-manage/user-group/edit/department'),
  },

  // 委办局
  {
    path: '/user-groups/detail/:id',
    name: 'UserGroupDetail',
    hidden: true,
    meta: {
      title: 'systemManage.ROUTER.USER_GROUP_DETAIL',
      activeMenu: '/user-groups',
    },
    component: () => import('@/views/system-manage/user-group/detail/user-group'),
  },

  {
    path: '/user-groups',
    name: 'UserGroupList',
    meta: {
      title: 'systemManage.ROUTER.USER_GROUP_LIST',
      icon: 'user-group',
      isVisible: STATUS_USER_GROUP,
      routerId: permissions.USER_GROUP_ID,
    },
    component: () => import('@/views/system-manage/user-group/list'),
  },
]
