/**
 * @file 异步路由
 * @module router/async-routes
 */

import Layout from '@/layout'
import systemManage from './modules/system-manage'
// @import
import { ROUTER_VISIBLE_CONFIG } from '@/settings'
import resourceCatalogSubsystem from '@/app-modules/resource-catalog/router'
import ruleManageSubsystem from '@/app-modules/rule-manage/router'
import dataManageSubsystem from '@/app-modules/data-manage/router'
import exchangeSharingSubsystem from '@/app-modules/exchange-sharing/router'
import news from '@/app-modules/news/router'
import members from '@/app-modules/members/router'
import journals from '@/app-modules/journals/router'
import creditPhotography from '@/app-modules/credit-photography/router'
import creditInformationInquiryApplication from '@/app-modules/credit-information-inquiry-application/router'
import business from '@/app-modules/business/router'

const { STATUS_RESOURCE_CATALOG } = ROUTER_VISIBLE_CONFIG
const routes = [
  ...systemManage,
  // @route
  ...news,
  ...members,
  ...journals,
  ...creditPhotography,
  {
    path: '/resource-catalog',
    name: 'ResourceCatalog',
    component: Layout,
    meta: {
      title: 'resourceCatalog.ROUTER.RESOURCE_CATALOG',
      icon: 'resource-catalog',
      breadcrumb: false,
      isVisible: STATUS_RESOURCE_CATALOG,
    },
    children: [
      ...resourceCatalogSubsystem,
      ...ruleManageSubsystem,
      ...dataManageSubsystem,
      ...exchangeSharingSubsystem,
    ],
  },
  ...creditInformationInquiryApplication,
  ...business,

  // Fallback route
  { path: '*', redirect: '/', hidden: true },
]

export default routes
