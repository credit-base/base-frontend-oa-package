/**
 * @file 常量路由
 * @module router/routes
 */

import Layout from '@/layout'

const routes = [
  {
    path: '/redirect',
    hidden: true,
    component: Layout,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/common/view-redirect'),
      },
    ],
  },

  {
    path: '/sign-in',
    name: 'SignIn',
    hidden: true,
    component: () => import('@/views/common/sign-in'),
  },

  {
    path: '/',
    redirect: '/dashboard',
    component: Layout,
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: () => import('@/views/dashboard'),
        meta: {
          title: 'dashboard.ROUTER.DASHBOARD',
          icon: 'dashboard',
          affix: true,
        },
      },
    ],
  },

  // {
  //   path: '/personal-information',
  //   redirect: '/personal-information',
  //   component: Layout,
  //   hidden: true,
  //   children: [
  //     {
  //       path: '/personal-information',
  //       name: 'PersonalInformation',
  //       component: () => import('@/views/system-manage/personal-information'),
  //       meta: {
  //         title: 'systemManage.ROUTER.PERSONAL_INFORMATION',
  //         icon: 'crew',
  //         affix: true,
  //       },
  //     },
  //   ],
  // },
]

export default routes
