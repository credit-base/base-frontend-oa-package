/**
 * TinyMCE 配置
 */

const CHAR_SPACE = ' '

// 插件
export const plugins = [
  'table',
  'image',
  'code',
  'link',
  'nonbreaking',
  'wordcount',
  'preview',
  'autoresize',
]
  .join(CHAR_SPACE)

// 工具条
export const toolbar = [
  'undo',
  'redo',
  'fontselect',
  'fontsizeselect',
  'forecolor',
  'backcolor',
  'bold',
  'italic',
  'underline',
  'strikethrough',
  'gap1',
  'removeformat',
  'indent',
  'outdent',
  'alignleft',
  'aligncenter',
  'alignright',
  'alignjustify',
  'gap2',
  'table',
  'link',
  'image',
  'code',
  'gap3',
  'h2',
  'h3',
  'preview',
]
  .join(CHAR_SPACE)
  .replace(/gap\d/g, '|')

// 字号
export const FONTSIZE_FORMATS = [
  '8',
  '10',
  '12',
  '14',
  '16',
  '18',
  '24',
  '36',
]
  .map(v => `${v}px`)
  .join(CHAR_SPACE)

// 字体
export const FONT_FORMATS = [
  `微软雅黑=Microsoft YaHei;`,
  `宋体=SimSun;`,
  `黑体=SimHei;`,
  `仿宋=FangSong;`,
  `楷体=KaiTi;`,
  `隶书=LiSu;`,
  `幼圆=YouYuan;`,
  `Andale Mono=andale mono,times;`,
  `Arial=arial, helvetica, sans-serif;`,
  `Arial Black=arial black, avant garde;`,
]
  .join(CHAR_SPACE)
