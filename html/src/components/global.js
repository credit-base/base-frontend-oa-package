/**
 * 全局组件
 * @module components/global
 */

export { default as GoBack } from './go-back'
export { default as DetailItem } from './detail-item'
export { default as AppCapsule } from './app-capsule'
export { default as PagePanel } from './page-panel'

export { default as SvgIcon } from './svg-icon'
export { default as ScreenFull } from './screen-full'
export { default as Pagination } from './pagination'
export { default as ThemeSelect } from './theme-select'
// export { default as SearchInput } from './SearchInput'
