/**
 * 注册全局组件
 *
 * @module components
 */

import Vue from 'vue'
import * as GlobalComponents from './global'

Object.keys(GlobalComponents).forEach(componentName => {
  Vue.component(componentName, GlobalComponents[componentName])
})
