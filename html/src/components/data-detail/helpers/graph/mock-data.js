/**
 * @file Mock 数据流向图
 * @module graph/mock-data
 */

import { randomNumber } from '@/utils'
import { formatTime } from '@/utils/date-time'
import {
  SYMBOL_BJ,
  SYMBOL_GB,
  SYMBOL_WBJ,
  SYMBOL_ORIGIN,
  SYMBOL_UPLOAD,
  SYMBOL_LIB_GB,
  SYMBOL_LIB_BJ,
  SYMBOL_LIB_WBJ,
  SYMBOL_LIB_ORIGIN,
  SYMBOL_LIB_REVERSE,
  SYMBOL_SIZE,
} from '@/plugins/echarts/symbol'

// 图表尺寸
export const CHART_VIEW = { x: 1200, y: 800 }

// 图表内填充
export const CHART_PADDING = 20

// 分类配置
export const CATEGORY_CONFIGS = {
  resource: 0,
  origin: 1,
  userGroup: 2,
  gb: 3,
  bj: 4,
  cloud: 5,
  usage: 6,
}

// Echarts 配置
export const LABEL_STYLE = {
  common: { position: 'bottom', color: '#fff' },
  userGroup: { position: 'bottom', color: '#fff' },
  gb: { position: 'bottom', color: '#fff' },
  bj: { position: 'bottom', color: '#fff' },
  cloud: { position: 'bottom', color: '#fff' },
}
export const LINE_STYLE = {
  origin: { opacity: 0.5, color: '#F3A53E' },
  wbj: { opacity: 0.5, color: '#9B4242' },
  gb: { opacity: 0.5, color: '#72AFF1' },
  bj: { opacity: 0.5, color: '#72AFF1' },
  cloud: { opacity: 0.5, color: '#72AFF1' },
  usage: { opacity: 0.5, color: '#5BBFC7' },
}
export const TOOLTIP_STYLE = {
  common: {
    show: true,
    padding: 0,
    borderWidth: 0,
    backgroundColor: 'rgba(0, 0, 0, 0)',
    color: '#fff',
    formatter: params => {
      const { data: { fields = {}, category } = {} } = params
      const createTooltipRow = row => `
        <div class="tooltip-row">
          <span class="tooltip-row-label">${row.k}: </span>
          <p class="tooltip-row-content">&nbsp;&nbsp;&nbsp;&nbsp;${row.v}</p>
        </div>
      `
      const html = `
        <div class="echarts-tooltip-container">
          ${category === CATEGORY_CONFIGS.origin
          ? `
          ${fields.dataCount ? createTooltipRow({ k: '平台上传', v: `${fields.dataCount}条数据` }) : ''}
          ${fields.cloudCount ? createTooltipRow({ k: '前置机回推', v: `${fields.cloudCount}条数据` }) : ''}
          `
          : `
          ${fields.lastUsedTime ? createTooltipRow({ k: '最后入库时间', v: `${formatTime(fields.lastUsedTime, 'YYYY年MM月DD日')}` }) : ''}
          ${fields.dataCount ? createTooltipRow({ k: '总数', v: `${fields.dataCount}条数据` }) : ''}
          ${fields.usageCount ? createTooltipRow({ k: '应用次数', v: `${fields.usageCount}次` }) : ''}
          `}
        </div>
      `

      return html
    },
  },
}

export function calcNodePosition ({
  rows,
  rowIdx,
  colIdx,
  startY = 0,
  endY = 0,
}, {
  cols = 6,
  viewWidth = CHART_VIEW.x,
  viewHeight = CHART_VIEW.y,
  gutter = CHART_PADDING,
} = {}) {
  const colWidth = (viewWidth - 2 * gutter) / cols
  const x = gutter + (2 * colIdx + 1) / 2 * colWidth
  let rowHeight = 0
  let y = 0

  if (startY && !endY) {
    rowHeight = (viewHeight - startY - 2 * gutter) / rows
  } else if (endY && !startY) {
    rowHeight = (viewHeight - endY - 2 * gutter) / rows
  } else if (startY && endY) {
    rowHeight = (endY - startY) / rows
  } else {
    rowHeight = (viewHeight - 2 * gutter) / rows
  }

  if (startY && !endY) {
    y = gutter + startY + (2 * rowIdx + 1) / 2 * rowHeight
  } else if (endY && !startY) {
    y = gutter + (2 * rowIdx + 1) / 2 * rowHeight
  } else if (startY && endY) {
    y = startY + (2 * rowIdx + 1) / 2 * rowHeight
  } else {
    y = gutter + (2 * rowIdx + 1) / 2 * rowHeight
  }

  return [x, CHART_VIEW.y - y]
}

// ================================================
//                    Mock 数据
// ================================================

// 数据来源方
export const RESOURCE_DATA_NODES = [
  {
    name: '前置机回推库',
    targetNames: ['A级纳税人'],
    symbol: SYMBOL_LIB_REVERSE,
    category: CATEGORY_CONFIGS.cloud,
    sourceRule: '回推',
    fields: {
      dataCount: randomNumber(50),
      lastUsedTime: Date.now(),
    },
  },
  {
    name: '平台上传',
    targetNames: ['A级纳税人'],
    symbol: SYMBOL_UPLOAD,
    fields: {
      dataCount: randomNumber(50),
      lastUsedTime: Date.now(),
    },
  },
].map((node, idx) => {
  return {
    key: `resource_${idx + 1}`,
    symbolSize: SYMBOL_SIZE.small,
    value: calcNodePosition({ colIdx: 0, rowIdx: idx, rows: 2 }),
    category: CATEGORY_CONFIGS.resource,
    lineStyle: LINE_STYLE.resource,
    label: LABEL_STYLE.common,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})

// 原始数据库
export const ORIGIN_DATA_NODES = [
  {
    name: 'A级纳税人',
    targetNames: ['委办局-A级纳税人', '前置机原始库-A级纳税人'],
    fields: {
      dataCount: randomNumber(50),
      cloudCount: randomNumber(50),
    },
  },
].map((node, idx) => {
  return {
    key: `origin_${idx + 1}`,
    rule: '上报',
    symbol: SYMBOL_ORIGIN,
    symbolSize: SYMBOL_SIZE.large,
    value: calcNodePosition({ colIdx: 1, rowIdx: idx, rows: 1 }),
    category: CATEGORY_CONFIGS.origin,
    lineStyle: LINE_STYLE.origin,
    label: LABEL_STYLE.common,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})

// 委办局数据库
export const USER_GROUP_DATA_NODES = [
  {
    name: '委办局-A级纳税人',
    targetNames: ['国标-地方性红名单', '本级-A级纳税人'],
    symbol: SYMBOL_WBJ,
    lineStyle: LINE_STYLE.wbj,
    fields: {
      dataCount: randomNumber(50),
      lastUsedTime: Date.now(),
    },
  },
  {
    name: '前置机原始库-A级纳税人',
    sourceRule: '交换',
    targetNames: ['数据报送'],
    symbol: SYMBOL_LIB_ORIGIN,
    lineStyle: LINE_STYLE.cloud,
    fields: {
      dataCount: randomNumber(50),
    },
  },
].map((node, idx) => {
  return {
    key: `user_group_${idx + 1}`,
    rule: '清洗',
    value: calcNodePosition({
      colIdx: 2,
      rowIdx: idx,
      startY: CHART_VIEW.y / 4,
      endY: CHART_VIEW.y / 4 * 3,
      rows: 2,
    }),
    symbolSize: SYMBOL_SIZE.small,
    category: CATEGORY_CONFIGS.userGroup,
    label: LABEL_STYLE.userGroup,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})

// 国标数据库
export const GB_DATA_NODES = [
  {
    name: '国标-地方性红名单',
    targetNames: ['前置机国标库-地方性红名单'],
    fields: {
      dataCount: randomNumber(50),
      lastUsedTime: Date.now(),
    },
  },
].map((node, idx) => {
  return {
    key: `gb_${idx + 1}`,
    rule: '转换',
    symbol: SYMBOL_GB,
    symbolSize: SYMBOL_SIZE.small,
    value: calcNodePosition({
      colIdx: 3,
      rowIdx: idx,
      rows: 1,
      startY: CHART_VIEW.y / 4,
      endY: CHART_VIEW.y / 2,
    }),
    category: CATEGORY_CONFIGS.gb,
    label: LABEL_STYLE.gb,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})

// 本级数据库
export const BJ_DATA_NODES = [
  {
    name: '本级-A级纳税人',
    targetNames: ['前置机本级库-A级纳税人'],
    fields: {
      dataCount: randomNumber(50),
      lastUsedTime: Date.now(),
    },
  },
].map((node, idx) => {
  return {
    key: `bj_${idx + 1}`,
    rule: '转换',
    symbol: SYMBOL_BJ,
    symbolSize: SYMBOL_SIZE.small,
    value: calcNodePosition({
      colIdx: 3,
      rowIdx: idx,
      rows: 1,
      startY: CHART_VIEW.y / 2,
      endY: CHART_VIEW.y / 4 * 3,
    }),
    category: CATEGORY_CONFIGS.bj,
    label: LABEL_STYLE.bj,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})

// 前置机
export const CLOUD_DATABASE_NODES = [
  {
    name: '前置机国标库-地方性红名单',
    symbol: SYMBOL_LIB_GB,
    targetNames: ['平台功能应用', '接口共享应用'],
    fields: {
      dataCount: randomNumber(50),
    },
  },
  {
    name: '前置机本级库-A级纳税人',
    symbol: SYMBOL_LIB_BJ,
    targetNames: ['数据报送'],
    fields: {
      dataCount: randomNumber(50),
    },
  },
].map((node, idx) => {
  return {
    key: `database_${idx}`,
    rule: '交换',
    symbolSize: SYMBOL_SIZE.small,
    value: calcNodePosition({ colIdx: 4, rowIdx: idx, rows: 2 }),
    category: CATEGORY_CONFIGS.cloud,
    lineStyle: LINE_STYLE.cloud,
    label: LABEL_STYLE.cloud,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})

// 应用
export const USAGE_DATA_NODES = [
  {
    name: '平台功能应用',
    fields: {
      usageCount: randomNumber(50),
    },
  },
  {
    name: '接口共享应用',
    fields: {
      usageCount: randomNumber(50),
    },
  },
  {
    name: '数据报送',
    fields: {
      usageCount: randomNumber(50),
    },
  },
].map((node, idx) => {
  return {
    key: `usage_${idx}`,
    symbol: SYMBOL_LIB_WBJ,
    symbolSize: SYMBOL_SIZE.small,
    value: calcNodePosition({ colIdx: 5, rowIdx: idx, rows: 3 }),
    category: CATEGORY_CONFIGS.usage,
    lineStyle: LINE_STYLE.usage,
    label: LABEL_STYLE.common,
    tooltip: TOOLTIP_STYLE.common,
    ...node,
  }
})
