/**
 * @file SVG 图标
 * @module icons/index
 */

const svgContext = require.context('./svg', false, /\.svg$/)
const requireAll = context => context.keys().map(context)

requireAll(svgContext)
