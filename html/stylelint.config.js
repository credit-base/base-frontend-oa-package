/**
 * @file StyleLint 配置
 * @see https://stylelint.io/user-guide/configure
 */

module.exports = {
  extends: [
    '@qxy/stylelint-config-scss',
    '@qxy/stylelint-config-vue',
    '@qxy/stylelint-config-order',
    '@qxy/stylelint-config-prettier',
  ],

  rules: {
    'selector-class-pattern': '^([a-z][a-z0-9\\-]*[a-z0-9])|(el-[-_0-9a-z]*)$',
    // FIXME: Remove bellow
    'no-descending-specificity': null,
    'keyframes-name-pattern': null,
  },
}
