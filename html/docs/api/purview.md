# purviewApi

---

## 目录

-   [委办局](#userGroup)
    -   [列表](#userGroupList)
    -   [详情](#userGroupDetail)
-   [科室](#department)
    -   [科室列表](#departmentList)
    -   [科室详情](#departmentDetail)
    -   [新增科室](#addDepartment)
    -   [编辑科室](#editDepartment)
-   [员工](#crew)
    -   [员工列表](#crewList)
    -   [员工详情](#crewDetail)
    -   [新增员工](#addCrew)
    -   [编辑员工](#editCrew)
    -   [重置密码](#restPassword)

## <a href="#userGroup" id="userGroup">委办局</a>

### <a href="#userGroupList" id="userGroupList">委办局列表</a>

Request

```
url:/api/userGroups
```

Method

```
GET
```

Params

```
{
	limit: 10,
	page: 1,
	name: '委办局名称'
}
```

Response

Example

```Bash
{
	status: 1,
	data: {
		total: 1,
		list: [
			{
				id: 'MA',
				name: '发展和改革委员会',
				shortName: '发改委'
			}
		]
	}
}
```

### <a href="#userGroupDetail" id="userGroupDetail">委办局详情</a>

Request

```
url:/api/userGroups/{id}
```

Method

```
GET
```

Response

Example

```Bash
{
	status: 1,
	data: {
		id: 'MA',
		name: '发展和改革委员会',
		shortName: '发改委',
		crewList: {
			total: 1,
			list: [
				{
					id: 'MA',
					realName: '曹韶华',
					cellphone: 18800000000,
					department: {
						id: 'MA',
						name: '综合办公室'
					},
					category: {
						id: 'MA',
						name: '管理员'
					},
					status: {
						type: 'success',
						name: '启用'
					},
					userGroup: {
						id: 'MA',
						name: '发展和改革委员会',
					},
          updateTime: 1583980685,
          updateTimeFormat: '2021年04月26日 11点15分',
				}
			]
		},
		departmentList: {
			total: 1,
			list: [
				{
					id: 'MA',
					name: '综合办公室',
					userGroup: {
						id: 'MA',
						name: '发展和改革委员会',
					},
					updateTime: 1583980685,
          		updateTimeFormat: '2021年04月26日 11点15分',
				}
			]
		}
	}
}
```

## <a href="department" id="#department">科室</a>

### <a href="departmentList" id="#departmentList">科室列表</a>

Request

```
url:/api/departments/{id}
```

Method

```
GET
```

Params

```Bash
{
	limit: 10,
	page: 1,
	name: '科室名称',
	sort: 'updateTime' || '-updateTime' || '',
	userGroup: 'MA' // 委办局ID
}
```

Response

Example

```Bash
{
	status: 1,
	data: {
		total: 1,
		list: [
			{
				id: 'MA',
				name: '综合办公室',
				userGroup: {
					id: 'MA',
					name: '发展和改革委员会',
				},
				updateTime: 1583980685,
        		updateTimeFormat: '2021年04月26日 11点15分',
			}
		]
	}
}
```

### <a href="#departmentDetail" id="departmentDetail">科室详情</a>

Request

```
url:/api/departments/{id}
```

Method

```
GET
```

Response

Example

```Bash
{
	status: 1,
	data: {
		id: 'MA',
		name: '综合办公室',
		userGroup: {
			id: 'MA',
			name: '发展和改革委员会',
		},
		updateTime: 1583980685,
		updateTimeFormat: '2021年04月26日 11点15分',
	}
}
```

### <a href="#addDepartment" id="addDepartment">新增科室</a>

Request

```
url:/api/departments/add
```

Method

```
POST
```

Response

Example

```Bash
{
	userGroup: 'MA',
	name: '综合办公室',
}
```

### <a href="#editDepartment" id="editDepartment">编辑科室</a>

Request

```
url:/api/departments/{id}/edit
```

Method

```
GET
```

```Bash
{
	status: 1,
	data: {
		id: 'MA',
		name: '综合办公室',
		userGroup: {
			id: 'MA',
			name: '发展和改革委员会',
		},
		updateTime: 1583980685,
    	updateTimeFormat: '2021年04月26日 11点15分',
	}
}
```

Method

```
POST
```

Response

Example

```Bash
{
	userGroup: 'MA',
	name: '综合办公室',
}
```

## <a href="crew" id="#crew">员工</a>

### <a href="#crewList" id="crewList">员工列表</a>

Request

```
url:/api/crews
```

Method

```
GET
```

Params

```Bash
{
	limit: 10,
	page: 1,
	name: '姓名',
	cellphone: 18800000000,
	sort: 'updateTime' || '-updateTime' || '',
	category: 'MA',
	userGroup: 'MA',
}
```

Response

Example

```Bash
{
	status: 1,
	data: {
		total: 1,
		list: [
			{
				id: 'MA',
				realName: '曹韶华',
				cellphone: 18800000000,
				department: {
					id: 'MA',
					name: '综合办公室'
				},
				category: {
					id: 'MA',
					name: '管理员'
				},
				userGroup: {
					id: 'MA',
					name: '发展和改革委员会',
				},
				status: {
					type: 'success',
					name: '启用'
				},
				updateTime: 1583980685,
				updateTimeFormat: '2021年04月26日 11点15分',
			}
		]
	}
}
```

### <a href="#crewDetail" id="crewDetail">员工详情</a>

Request

```
url:/api/crews/{id}
```

Method

```
GET
```

Response

Example

```Bash
{
	status: 1,
	data: {
		id: 'MA',
		realName: '曹韶华',
		cellphone: 18800000000,
		department: {
			id: 'MA',
			name: '综合办公室'
		},
		category: {
			id: 'MA',
			name: '管理员'
		},
		userGroup: {
			id: 'MA',
			name: '发展和改革委员会',
		},
		status: {
			type: 'success',
			name: '启用'
		},
		updateTime: 1583980685,
		updateTimeFormat: '2021年04月26日 11点15分',
		purview: [1,2,3,4,5]
	}
}
```

### <a href="#addCrew" id="addCrew">新增员工</a>

Request

```
url:/api/crews/{id}/add
```

Method

```
POST
```

Response

Example

```Bash
{
	realName: '曹韶华',
	cellphone: 18800000000,
	department: 'MA',
	category: 'MA',
	userGroup: 'MA',
	purview: {
		1: [1,2,3,4,5],
		2: [1,2,3]
	},
	password: '',
	confirmPassword: ''
}
```

### <a href="#editCrew" id="editCrew">编辑员工</a>

Request

```
url:/api/crews/{id}/add
```

Method

```
GET
```

Response

Example

```Bash
{
	status: 1,
	data: {
		id: 'MA',
		realName: '曹韶华',
		cellphone: 18800000000,
		department: {
			id: 'MA',
			name: '综合办公室'
		},
		category: {
			id: 'MA',
			name: '管理员'
		},
		userGroup: {
			id: 'MA',
			name: '发展和改革委员会',
		},
		updateTime: 1583980685,
		updateTimeFormat: '2021年04月26日 11点15分',
		purview: [1,2,3,4,5]
	}
}
```

Method

```
POST
```

Response

Example

```Bash
{
	realName: '曹韶华',
	cellphone: 18800000000,
	department: 'MA',
	category: 'MA',
	userGroup: 'MA',
	purview: [1,2,3,4,5],
}
```
