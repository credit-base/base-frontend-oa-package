// 功能模块动态配置文件

/**
 * 根据设置状态来管理功能模块是否显示
 * true => 显示
 * false => 隐藏
 */

var GLOBAL_CONFIG =  {
  // 平台配置信息
  APP_CONFIG: {
    REGION_NAME: '陕西',
    REGION_LEVEL: '省',
    CREDIT: '信用',
    CHINA: '中国',
    PROVINCE: '陕西省',
    DASHBOARD: '公共信用信息大数据驾驶舱',
    APP_TITLE: '公共信用信息平台',
    OSS_HOST: 'http://oss.base.qixinyun.com',
    // 登录页面页脚链接信息
    LINKS: [
      {
        title: '信用中国',
        url: 'https://www.creditchina.gov.cn/',
      },
      {
        title: '信用陕西',
        url: 'https://credit.shaanxi.gov.cn/',
      },
    ],
    // 技术服务公司信息
    TECHNICAL: {
      COPY_TITLE: '企信云2015-',
      TITLE: '北京企信云信息科技有限公司',
      URL: 'https://www.qixinyun.com/'
    },
    // 系统是否可以访问互联网
    IS_NETWORK_ENABLE: true,
    // 是否启用权限系统
    IS_PURVIEW_ENABLE: false,
    // 地图坐标数据
    REGION_NAME_COORDINATE: [108.954355, 34.27659],
    // 驾驶舱平台信用排名
    RANK_DATA: {
      CURRENT_RANK: {
        PROVINCE: 1,
        NATIONAL_RANKING: 35,
      },
      RANK_RECORDS: [
        // 月份 全国排名 省排名 信用指数
        { month: '3月', provinceRank: 1, countryRank: 18 },
        { month: '4月', provinceRank: 1, countryRank: 18 },
        { month: '5月', provinceRank: 1, countryRank: 18 },
        { month: '6月', provinceRank: 1, countryRank: 20 },
        { month: '7月', provinceRank: 1, countryRank: 12 },
        { month: '8月', provinceRank: 1, countryRank: 15 },
        { month: '9月', provinceRank: 1, countryRank: 21 },
        { month: '10月', provinceRank: 1, countryRank: 35 },
      ],
    }
  },
  // 路由动态管理
  ROUTER_VISIBLE_CONFIG: {
    // 委办局
    STATUS_USER_GROUP: true,
    // 员工管理
    STATUS_CREW: true,
    // 信用保函管理系统
    STATUS_GUARANTEE: false,
    // 双公示数据质量监测系统
    STATUS_STANDING_BOOK: false,
    STATUS_USER_GROUP_STANDING_BOOK: true,
    STATUS_PROVINCE_STANDING_BOOK: true,
    // 信用信息综合归集系统
    STATUS_RESOURCE_CATALOG: true,
    // 数据管理子系统
    STATUS_DATA_MANAGE_SUBSYSTEM: true,
    // 目录管理子系统
    STATUS_RESOURCE_CATALOG_SUBSYSTEM: true,
    // 规则管理子系统
    STATUS_RULE_MANAGE_SUBSYSTEM: true,
    // 前置机交换子系统
    STATUS_EXCHANGE_SHARING_SUBSYSTEM: true,

    STATUS_NATIONAL_STANDARD_RESOURCE_CATALOG: true,
    STATUS_NATIONAL_STANDARD_VERSION_COMPARE: true,
    STATUS_CURRENT_UNIT_RESOURCE_CATALOG: true,
    STATUS_USER_GROUP_RESOURCE_CATALOG: true,
    STATUS_CURRENT_UNIT_VERSION_COMPARE: true,
    STATUS_USER_GROUP_VERSION_COMPARE: true,
    STATUS_SUBTASKS: true,
    STATUS_FEEDBACK_TEMPLATE: true,
    // 新闻
    STATUS_NEWS: true,
    // 用户
    STATUS_MEMBER: true,
    // 信用刊物
    STATUS_JOURNALS: true,
    // 信用随手拍
    STATUS_CREDIT_PHOTOGRAPHY: true,
    // 信用信息查询应用系统
    STATUS_CREDIT_INFORMATION_INQUIRY_APPLICATION: true,
    // 业务管理
    STATUS_BUSINESS: true,
  }
}

if ( typeof module === "object" && typeof module.exports === "object" ) {
  module.exports = GLOBAL_CONFIG
} else {
  window.$CREDIT_APP_CONFIG = GLOBAL_CONFIG
}

;(function() {
  var appConfig = GLOBAL_CONFIG.APP_CONFIG
    document.title = appConfig.CREDIT + appConfig.CHINA + ' • ' + appConfig.REGION_NAME + appConfig.APP_TITLE
}())
