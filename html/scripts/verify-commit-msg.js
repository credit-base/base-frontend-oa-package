/**
 * @file 检测提交信息
 */

const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const findUp = require('find-up')

/**
 * @see https://regexper.com/#%2F%5E%23%5Cd%7B4%2C%7D-%28docs%7Cunittest%7Cpseudocode%7Cmsg%7Cbug%29-.%7B2%2C%7D%24%2F
 */
const RE_COMMIT_MSG = /^#\d{4,}-(docs|unittest|pseudocode|msg|bug)-.{2,}$/

const gitPath = getGitRootPath()
const msgPath = path.resolve(gitPath, process.env.HUSKY_GIT_PARAMS)
const msg = fs.readFileSync(msgPath, 'utf-8').trim()

if (!RE_COMMIT_MSG.test(msg)) {
  console.log()
  console.error(`${chalk.bgRed.white(' ERROR ')} ${chalk.red(`invalid commit message format.`)}`)
  console.log()
  process.exit(1)
}

/**
 * Find Git root path
 */
function getGitRootPath () {
  const gitPath = findUp.sync('.git', { type: 'directory' })

  return gitPath ? path.dirname(gitPath) : null
}
