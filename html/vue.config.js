/**
 * @file vue-cli 配置
 */

const path = require('path')
const dayjs = require('dayjs')
const { version } = require('./package.json')
// const settings = require('./src/settings')
// const { OSS_HOST = 'http://oss.base.qixinyun.com' } = settings
const OSS_HOST = 'http://oss.base.qixinyun.com'

const resolve = (...args) => path.resolve(__dirname, ...args)
const {
  NODE_ENV,
  VUE_APP_MOCK_API,
  VUE_APP_PROXY_TARGET = `sandbox`,
} = process.env
const isProduction = NODE_ENV === `production`
const configs = {
  version,
  year: dayjs().format('YYYY'),
  env: isProduction ? 'production' : 'development',
  time: dayjs().format(`YYYY-MM-DD HH:mm:ss`),
}
const PROXY_REQUEST_TARGET_MAP = {
  // default: `http://192.168.50.18:8083`,
  default: `http://oa.base.qixinyun.com/`,
  sandbox: `http://oa.base.qixinyun.com`,
  // ADD_OTHER_DEVELOPMENT_CONFIG_HERE
}

module.exports = {
  publicPath: process.env.BASE_URL || `./`,

  assetsDir: `static`,

  productionSourceMap: !isProduction,

  transpileDependencies: [
    `screenfull`,
    `vue-echarts`,
    `resize-detector`,
  ],

  css: {
    loaderOptions: {
      sass: {
        additionalData: [
          `@import "@/styles/core/style";`,
        ].join(``),
        ...(isProduction ? { implementation: require('node-sass') } : {}),
      },
    },
  },

  devServer: {
    open: true,
    proxy: {
      '/api': {
        target: PROXY_REQUEST_TARGET_MAP[VUE_APP_PROXY_TARGET],
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/api',
        },
      },
      '/oss': {
        target: OSS_HOST,
        changeOrigin: true,
        pathRewrite: {
          '^/oss': '/api',
        },
      },
      '/upload': {
        target: OSS_HOST,
        changeOrigin: true,
      },
    },
    before: VUE_APP_MOCK_API ? require('./__mock__/server') : false,
  },

  chainWebpack: config => {
    // Disable Preload && Prefetch
    config.plugins.delete('preload')
    config.plugins.delete('prefetch')

    // Disable order check of mini-css-webpack-plugin
    config.when(isProduction, config => {
      config.plugin('extract-css')
        .tap(args => {
          args[0].ignoreOrder = true
          return args
        })
      return config
    })

    // Inject variables to HTML template
    config.plugin('html')
      .tap(args => {
        // Disable minify HTML
        // PHP template engine can't process it properly
        args[0].minify = false
        args[0].isProduction = isProduction
        args[0].configs = configs
        return args
      })

    // Config svg-sprite-loader
    config.module
      .rule('svg')
      .exclude
      .add(resolve('src/icons'))
      .end()

    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]',
      })
      .end()
  },
}
