const path = require('path')
const chalk = require('chalk')
const express = require('express')
const chokidar = require('chokidar')
const Mock = require('mockjs')

const mockDir = path.join(process.cwd(), '__mock__')

function registerRoutes (app) {
  let mockLastIndex

  const { mocks } = require('.')

  const mocksForServer = mocks.map(route => {
    return responseFake(route.url, route.type, route.response)
  })

  for (const mock of mocksForServer) {
    app[mock.type](mock.url, mock.response)
    mockLastIndex = app._router.stack.length
  }
  const mockRoutesLength = Object.keys(mocksForServer).length
  return {
    mockRoutesLength,
    mockStartIndex: mockLastIndex - mockRoutesLength,
  }
}

function unregisterRoutes () {
  Object.keys(require.cache).forEach(item => {
    if (item.includes(mockDir)) {
      delete require.cache[require.resolve(item)]
    }
  })
}

// For mock server
const responseFake = (url, type, respond) => {
  return {
    url: new RegExp(url),
    type: type || 'get',
    response (req, res) {
      console.log(`request invoked: ${req.path}`)
      res.json(Mock.mock(respond instanceof Function ? respond(req, res) : respond))
    },
  }
}

module.exports = app => {
  app.use(express.json())
  app.use(express.urlencoded({ extended: true }))

  const mockRoutes = registerRoutes(app)
  let mockRoutesLength = mockRoutes.mockRoutesLength
  let mockStartIndex = mockRoutes.mockStartIndex

  // watch files, hot reload mock server
  chokidar.watch(mockDir, {
    ignore: /server/,
    ignoreInitial: true,
  }).on('all', (event, path) => {
    if (event === 'change' || event === 'add') {
      try {
        // remove mock routes stack
        app._router.stack.splice(mockStartIndex, mockRoutesLength)

        // clear routes cache
        unregisterRoutes()

        const mockRoutes = registerRoutes(app)
        mockRoutesLength = mockRoutes.mockRoutesLength
        mockStartIndex = mockRoutes.mockStartIndex

        console.log(chalk.magentaBright(`\n > Mock Server hot reload success! changed  ${path}`))
      } catch (error) {
        console.log(chalk.redBright(error))
      }
    }
  })
}
