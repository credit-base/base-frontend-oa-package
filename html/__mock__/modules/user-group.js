/**
 * Mock - 委办局管理
 *
 * @module mock/user-group
 */

// const Mock = require('mockjs')
const { wrapRequestSuccess } = require('../utils')

const mocks = [
  {
    url: '/api/userGroups',
    type: 'get',
    response: _ => {
      return wrapRequestSuccess({

      })
    },
  },
  {
    url: '/api/userGroups/.*',
    type: 'get',
    response: _ => {
      return wrapRequestSuccess({

      })
    },
  },
]

module.exports = mocks
