/**
 * Mock - 科室管理
 *
 * @module mock/department
 */

// const Mock = require('mockjs')
const { wrapRequestSuccess } = require('../utils')

const mocks = [
  {
    url: '/api/departments',
    type: 'get',
    response: _ => {
      return wrapRequestSuccess({

      })
    },
  },
]

module.exports = mocks
