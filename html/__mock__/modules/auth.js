/**
 * Mock - 员工
 *
 * @module mock/utils
 */

const Mock = require('mockjs')
const { wrapRequestSuccess } = require('../utils')

const mocks = [
  {
    url: '/api/signIn',
    type: 'post',
    response: _ => {
      return wrapRequestSuccess({
        jwt: Mock.Random.string(10),
      })
    },
  },

  {
    url: '/api/signOut',
    type: 'post',
    response: _ => {
      return wrapRequestSuccess()
    },
  },

  {
    url: '/api/personalCenter',
    type: 'get',
    response: _ => {
      return wrapRequestSuccess({

      })
    },
  },
]

module.exports = mocks
