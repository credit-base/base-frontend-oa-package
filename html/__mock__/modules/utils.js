/**
 * Mock - 工具接口
 *
 * @module mock/utils
 */

const Mock = require('mockjs')
const { wrapRequestSuccess } = require('../utils')

const mocks = [
  {
    url: '/api/utils/hash',
    type: 'get',
    response: _ => {
      return wrapRequestSuccess({
        hash: Mock.Random.string(24),
      })
    },
  },
]

module.exports = mocks
