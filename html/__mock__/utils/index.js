/**
 * 工具方法
 *
 * @module mock/utils
 */

const REQUEST_STATUS = { SUCCESS: 1, ERROR: 0 }

/**
 * 包装接口成功返回内容
 *
 * @param {object} data 返回数据
 */
exports.wrapRequestSuccess = (data = {}) => ({
  status: REQUEST_STATUS.SUCCESS,
  data,
})

/**
 * 包装接口失败返回内容
 *
 * @param {object} data 返回数据
 */
exports.wrapRequestError = (data = {}) => ({
  status: REQUEST_STATUS.ERROR,
  data,
})

/**
 * 转换 URL search 字段为对象
 * @param {string} url
 * @returns {object}
 */
exports.param2Obj = function (url) {
  const search = decodeURIComponent(url.split('?')[1]).replace(/\+/g, ' ')
  if (!search) {
    return {}
  }
  const obj = {}
  const searchArr = search.split('&')
  searchArr.forEach(v => {
    const index = v.indexOf('=')
    if (index !== -1) {
      const name = v.substring(0, index)
      const val = v.substring(index + 1, v.length)
      obj[name] = val
    }
  })
  return obj
}
