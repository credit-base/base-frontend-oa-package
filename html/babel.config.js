/**
 * @file Babel 配置
 * @see https://babeljs.io/docs/en/options
 */

module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset',
  ],

  plugins: [
    ['component', {
      libraryName: 'element-ui',
      styleLibraryName: 'theme-chalk',
    }],
  ],

  env: {
    development: {
      plugins: [
        // https://panjiachen.github.io/vue-element-admin-site/guide/advanced/lazy-loading.html
        'dynamic-import-node',
      ],
    },
  },
}
