/**
 * @file: Jest 配置
 * @see https://jestjs.io/docs/en/configuration
 */

module.exports = {
  preset: '@vue/cli-plugin-unit-jest',

  bail: true,

  // https://github.com/facebook/jest/issues/6229#issuecomment-403539460
  transformIgnorePatterns: [
    '/node_modules/(?!element-ui|screenfull|crypto-random-string|echarts|zrender|@jest|vue-echarts|@jiaminghi/data-view)',
  ],

  moduleNameMapper: {
    '\\.(scss|sass|css)$': '<rootDir>/tests/unit/__mock__/mock-style.js',
  },

  collectCoverageFrom: [
    'src/**/*.{js,vue}',

    // =========================================
    //                以下模块不测试
    // =========================================
    '!**/node_modules/**',
    '!src/i18n/**',
    '!src/icons/**',
    '!src/router/**',
    '!src/plugins/**',
    '!src/utils/request.js',
    '!src/utils/permission.js',
    '!src/main.js',
    '!src/App.vue',
    '!src/constants/permissions.js',
    '!src/constants/storage.js',
    '!src/components/data-detail/helpers/graph/**',

    // =========================================
    //                以下模块暂不测试
    // =========================================
    '!src/directives/**',
    '!src/layout/**',
    '!src/services/**',
    '!src/models/**',
    '!src/store/**',

    // =========================================
    //              模块常量与配置
    // =========================================
    '!src/app-modules/**/lang/**',
    '!src/app-modules/**/router/**',
    '!src/app-modules/**/helpers/**/*',
    `!src/app-modules/**/views/components/**/*`,
    `!src/app-modules/credit-information-inquiry-application/views/base-templates/components/**/*`,

    // =========================================
    //                  难点
    // =========================================
    `!src/app-modules/rule-manage/views/detail/gb.vue`,
    `!src/components/data-detail/component-view-traceability-graph.vue`,

    // =========================================
    //        计划重构模块 (关系图谱，在线填报)
    // =========================================
    // `!src/app-modules/data-manage/**`,
    `!src/app-modules/exchange-sharing/**`,
    `!src/app-modules/resource-catalog/**`,
    `!src/app-modules/data-manage/views/components/*-graph.vue`,
    `!src/app-modules/data-manage/views/components/*-flow-diagram.vue`,
    `!src/app-modules/exchange-sharing/views/components/catalog-overview.vue`,
    `!src/app-modules/data-manage/views/detail/task-flow-graph.vue`,

    // =========================================
    //                以下模块未测试
    // =========================================
    '!src/views/dashboard/**',
  ],

  moduleFileExtensions: [
    'js',
    'vue',
    'json',
    'scss',
  ],

  setupFiles: [
    '<rootDir>/tests/unit/__setup__',
    '<rootDir>/tests/unit/__setup__/match-media',
    // '<rootDir>/tests/unit/__setups__/localstorage',
  ],

  globals: {
    'vue-jest': {
      // Compilation errors in the <style> tags of Vue components will
      // already result in failing builds, so compiling CSS during unit
      // tests doesn't protect us from anything. It only complicates
      // and slows down our unit tests.
      experimentalCSSCompile: false,
    },
  },
}
