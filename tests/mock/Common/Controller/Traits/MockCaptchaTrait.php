<?php
namespace Base\Package\Common\Controller\Traits;

class MockCaptchaTrait
{
    use CaptchaTrait;

    public function publicValidateCaptcha(string $phrase):bool
    {
        return $this->validateCaptcha($phrase);
    }
}
