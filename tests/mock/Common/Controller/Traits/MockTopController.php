<?php
namespace Base\Package\Common\Controller\Traits;

class MockTopController
{
    use TopControllerTrait;

    protected function topAction(int $id)
    {
        unset($id);
        return true;
    }

    protected function cancelTopAction(int $id)
    {
        unset($id);
        return false;
    }
}
