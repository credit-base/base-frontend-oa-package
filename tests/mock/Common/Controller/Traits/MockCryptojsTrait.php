<?php
namespace Base\Package\Common\Controller\Traits;

class MockCryptojsTrait
{
    use CryptojsTrait;

    public function publicDecrypt(string $jsonString)
    {
        return $this->decrypt($jsonString);
    }

    public function publicHash()
    {
        return $this->hash();
    }
}
