<?php
namespace Base\Package\Common\Controller\Traits;

class MockRevokeController
{
    use RevokeControllerTrait;

    protected function revokeAction(int $id)
    {
        unset($id);
        return true;
    }
}
