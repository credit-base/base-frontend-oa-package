<?php
namespace Base\Package\Common\Controller\Traits;

class MockFetchController
{
    use FetchControllerTrait;

    protected function filterAction()
    {
        return true;
    }

    protected function fetchOneAction(int $id)
    {
        unset($id);
        return false;
    }
}
