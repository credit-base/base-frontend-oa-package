<?php
namespace Base\Package\Common\Controller\Traits;

class MockDeleteController
{
    use DeleteControllerTrait;

    protected function deleteAction(int $id)
    {
        unset($id);
        return true;
    }
}
