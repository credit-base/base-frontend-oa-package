<?php
namespace Base\Package\Common\Controller\Traits;

class MockResubmitController
{
    use ResubmitControllerTrait;

    protected function resubmitAction(int $id)
    {
        unset($id);
        return true;
    }
}
