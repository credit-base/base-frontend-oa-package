<?php
namespace Base\Package\Common\Controller\Traits;

class MockEnableController
{
    use EnableControllerTrait;

    protected function enableAction(int $id)
    {
        unset($id);
        return true;
    }

    protected function disableAction(int $id)
    {
        unset($id);
        return false;
    }
}
