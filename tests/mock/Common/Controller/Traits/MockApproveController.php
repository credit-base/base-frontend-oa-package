<?php
namespace Base\Package\Common\Controller\Traits;

class MockApproveController
{
    use ApproveControllerTrait;

    protected function approveAction(int $id)
    {
        unset($id);
        return true;
    }

    protected function rejectAction(int $id)
    {
        unset($id);
        return false;
    }
}
