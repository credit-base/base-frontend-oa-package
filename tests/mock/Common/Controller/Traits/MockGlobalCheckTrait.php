<?php
namespace Base\Package\Common\Controller\Traits;

class MockGlobalCheckTrait
{
    use GlobalCheckTrait;

    public function publicCheckUserExist() : bool
    {
        return $this->checkUserExist();
    }

    public function publicCheckUserStatusEnable() : bool
    {
        return $this->checkUserStatusEnable();
    }

    public function publicCheckUserHasPurview($resource) : bool
    {
        return $this->checkUserHasPurview($resource);
    }
}
