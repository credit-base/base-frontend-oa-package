<?php
namespace Base\Package\Common\Controller\Traits;

class MockToolTrait
{
    use ToolTrait;

    public function publicImplodeArray(array $array) : string
    {
        return $this->implodeArray($array);
    }

    public function publicGetImplodeIds(array $objects) : string
    {
        return $this->getImplodeIds($objects);
    }

    public function publicFormatString($str)
    {
        return $this->formatString($str);
    }
}
