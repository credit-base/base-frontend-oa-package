<?php

namespace Base\Package\Common\View;

trait MockViewTrait
{
    public function encode(array $data = array()) : void
    {
        parent::encode($data);
    }
}
