<?php
namespace Base\Package\Common\View\Json;

class MockUtilsView extends UtilsView
{
    public function __construct()
    {
        parent::__construct(array());
    }

    public function getData() : array
    {
        return parent::getData();
    }
}
