<?php
namespace Base\Package\Common\Utils;

class MockRandomTokenTrait
{
    use RandomTokenTrait;

    public function publicRandomNumber(int $length = 6)
    {
        return $this->randomNumber($length);
    }
}
