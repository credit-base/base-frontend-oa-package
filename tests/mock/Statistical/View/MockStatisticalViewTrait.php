<?php
namespace Base\Package\Statistical\View;

class MockStatisticalViewTrait
{
    use StatisticalViewTrait;

    public function getPublicStatisticalTranslator(string $type)
    {
        return $this->getStatisticalTranslator($type);
    }
}
