<?php
namespace Base\Package\Statistical\Controller;

use Sdk\Statistical\Repository\Statistical\StatisticalRepository;

class MockStatisticalControllerTrait
{
    use StatisticalControllerTrait;

    public function getPublicStatisticalRepository(string $type) : StatisticalRepository
    {
        return $this->getStatisticalRepository($type);
    }
}
