<?php
namespace Base\Package\Enterprise\Controller;

use Sdk\Enterprise\Repository\EnterpriseRepository;

class MockEnterpriseFetchController extends EnterpriseFetchController
{
    public function getRepository() : EnterpriseRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function statisticalNumber(string $unifiedSocialCreditCode)
    {
        return parent::statisticalNumber($unifiedSocialCreditCode);
    }
}
