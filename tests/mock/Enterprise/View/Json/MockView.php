<?php
namespace Base\Package\Enterprise\View\Json;

use Sdk\Enterprise\Translator\EnterpriseTranslator;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Statistical\Model\Statistical;

class MockView extends View
{
    public function getEnterprise()
    {
        return parent::getEnterprise();
    }

    public function getTranslator(): EnterpriseTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(new Enterprise(1), new Statistical());
    }

    public function getEnterpriseRelationStaticsData()
    {
        return parent::getEnterpriseRelationStaticsData();
    }

    public function getStatisticalNumber()
    {
        return parent::getStatisticalNumber();
    }
}
