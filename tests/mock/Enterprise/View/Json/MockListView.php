<?php
namespace Base\Package\Enterprise\View\Json;

use Sdk\Enterprise\Translator\EnterpriseTranslator;
use Sdk\Enterprise\Model\Enterprise;

class MockListView extends ListView
{
    public function getEnterprise(): array
    {
        return parent::getEnterprise();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): EnterpriseTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct([new Enterprise(1)], 1);
    }
}
