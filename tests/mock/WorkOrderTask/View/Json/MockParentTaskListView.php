<?php
namespace Base\Package\WorkOrderTask\View\Json;

use Sdk\WorkOrderTask\Translator\ParentTaskTranslator;

class MockParentTaskListView extends ParentTaskListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getParentTask(): array
    {
        return parent::getParentTask();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getTranslator(): ParentTaskTranslator
    {
        return parent::getTranslator();
    }
}
