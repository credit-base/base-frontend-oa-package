<?php
namespace Base\Package\WorkOrderTask\View\Json;

class MockWorkOrderTaskViewTrait
{
    use WorkOrderTaskViewTrait;

    public function compareResultPublic(array $baseItems, array $feedbackRecords)
    {
        return $this->compareResult($baseItems, $feedbackRecords);
    }

    public function comparePublic(array $baseItems, array $feedbackRecord)
    {
        return $this->compare($baseItems, $feedbackRecord);
    }

    public function getResultsDataPublic(array $results, array $baseItems, array $feedbackRecords) : array
    {
        return $this->getResultsData($results, $baseItems, $feedbackRecords);
    }

    public function getIdentifiesPublic(array $items) : array
    {
        return $this->getIdentifies($items);
    }
}
