<?php
namespace Base\Package\WorkOrderTask\View\Json;

use Sdk\WorkOrderTask\Translator\WorkOrderTaskTranslator;

class MockWorkOrderTaskListView extends WorkOrderTaskListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getWorkOrderTask(): array
    {
        return parent::getWorkOrderTask();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getTranslator(): WorkOrderTaskTranslator
    {
        return parent::getTranslator();
    }
}
