<?php
namespace Base\Package\WorkOrderTask\View\Json;

use Sdk\WorkOrderTask\Model\WorkOrderTask;
use Sdk\WorkOrderTask\Translator\WorkOrderTaskTranslator;

class MockWorkOrderTaskView extends WorkOrderTaskView
{
    public function __construct()
    {
        parent::__construct(new WorkOrderTask());
    }

    public function getWorkOrderTask()
    {
        return parent::getWorkOrderTask();
    }

    public function getTranslator(): WorkOrderTaskTranslator
    {
        return parent::getTranslator();
    }
}
