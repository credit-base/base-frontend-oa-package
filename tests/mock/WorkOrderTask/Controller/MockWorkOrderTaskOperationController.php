<?php
namespace Base\Package\WorkOrderTask\Controller;

use Marmot\Framework\Classes\CommandBus;

use Sdk\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;

class MockWorkOrderTaskOperationController extends WorkOrderTaskOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function getWorkOrderTaskWidgetRule() : WorkOrderTaskWidgetRule
    {
        return parent::getWorkOrderTaskWidgetRule();
    }

    public function validateReasonScenario(
        $reason
    ) : bool {
        return parent::validateReasonScenario(
            $reason
        );
    }

    public function getItemsDecode(array $items) : array
    {
        return parent::getItemsDecode($items);
    }
}
