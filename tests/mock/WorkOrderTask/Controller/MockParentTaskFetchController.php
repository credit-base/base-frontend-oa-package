<?php
namespace Base\Package\WorkOrderTask\Controller;

use Sdk\WorkOrderTask\Repository\ParentTaskRepository;

class MockParentTaskFetchController extends ParentTaskFetchController
{
    public function getRepository() : ParentTaskRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }
    
    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
