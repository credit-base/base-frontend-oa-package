<?php
namespace Base\Package\WorkOrderTask\Controller;

use Marmot\Framework\Classes\CommandBus;

use Sdk\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;

class MockParentTaskOperationController extends ParentTaskOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function getWorkOrderTaskWidgetRule() : WorkOrderTaskWidgetRule
    {
        return parent::getWorkOrderTaskWidgetRule();
    }

    public function validateAssignScenario(
        $title,
        $description,
        $attachment,
        $templateType
    ) : bool {
        return parent::validateAssignScenario(
            $title,
            $description,
            $attachment,
            $templateType
        );
    }

    public function validateReasonScenario(
        $reason
    ) : bool {
        return parent::validateReasonScenario(
            $reason
        );
    }

    public function getAssignObjectCode(array $assignObjects) : array
    {
        return parent::getAssignObjectCode($assignObjects);
    }
}
