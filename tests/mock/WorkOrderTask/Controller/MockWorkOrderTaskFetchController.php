<?php
namespace Base\Package\WorkOrderTask\Controller;

use Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

class MockWorkOrderTaskFetchController extends WorkOrderTaskFetchController
{
    public function getRepository() : WorkOrderTaskRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
