<?php


namespace  Base\Package\UserGroup\View\Json;

use Base\Package\Common\View\MockViewTrait;
use Sdk\UserGroup\Translator\DepartmentTranslator;

class MockDepartmentListView extends DepartmentListView
{
    use MockViewTrait;

    public function getDepartment() : array
    {
        return parent::getDepartment();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getTranslator() : DepartmentTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct([], 0);
    }
}
