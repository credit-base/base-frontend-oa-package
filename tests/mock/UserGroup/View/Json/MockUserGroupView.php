<?php


namespace  Base\Package\UserGroup\View\Json;

use Base\Package\Common\View\MockViewTrait;
use Sdk\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Translator\UserGroupTranslator;
use Sdk\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\Translator\DepartmentTranslator;

class MockUserGroupView extends UserGroupView
{
    use MockViewTrait;

    public function getUserGroup()
    {
        return parent::getUserGroup();
    }

    public function getDepartmentTranslator() : DepartmentTranslator
    {
        return parent::getDepartmentTranslator();
    }
    public function getTranslator() : UserGroupTranslator
    {
        return parent::getTranslator();
    }
    public function getCrewTranslator() : CrewTranslator
    {
        return parent::getCrewTranslator();
    }

    public function getData() : array
    {
        return parent::getData();
    }


    public function __construct()
    {
        parent::__construct(new UserGroup(), []);
    }
}
