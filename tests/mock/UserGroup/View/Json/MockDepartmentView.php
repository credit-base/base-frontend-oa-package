<?php


namespace  Base\Package\UserGroup\View\Json;

use Base\Package\Common\View\MockViewTrait;
use Sdk\UserGroup\Model\Department;
use Sdk\UserGroup\Translator\DepartmentTranslator;

class MockDepartmentView extends DepartmentView
{
    use MockViewTrait;

    public function getDepartment()
    {
        return parent::getDepartment();
    }

    public function getTranslator() : DepartmentTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(new Department());
    }
}
