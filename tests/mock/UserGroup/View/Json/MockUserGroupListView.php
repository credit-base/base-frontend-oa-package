<?php


namespace  Base\Package\UserGroup\View\Json;

use Base\Package\Common\View\MockViewTrait;
use Sdk\UserGroup\Translator\UserGroupTranslator;

class MockUserGroupListView extends UserGroupListView
{
    use MockViewTrait;

    public function getUserGroup() : array
    {
        return parent::getUserGroup();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getTranslator() : UserGroupTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct([], 0);
    }
}
