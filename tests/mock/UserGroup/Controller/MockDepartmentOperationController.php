<?php
namespace Base\Package\UserGroup\Controller;

use Sdk\UserGroup\Repository\DepartmentRepository;
use Sdk\UserGroup\WidgetRules\DepartmentWidgetRules;

use Marmot\Framework\Classes\CommandBus;

use Sdk\Common\WidgetRules\WidgetRules;

class MockDepartmentOperationController extends DepartmentOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function getRepository() : DepartmentRepository
    {
        return parent::getRepository();
    }
    
    public function getWidgetRules() : WidgetRules
    {
        return parent::getWidgetRules();
    }

    public function getDepartmentWidgetRules() : DepartmentWidgetRules
    {
        return parent::getDepartmentWidgetRules();
    }

    public function validateAddScenario(
        $name,
        $userGroupId
    ) : bool {
        return parent::validateAddScenario(
            $name,
            $userGroupId
        );
    }

    public function validateEditScenario(
        string $name
    ) : bool {
        return parent::validateEditScenario(
            $name
        );
    }

    public function addView() : bool
    {
        return parent::addView();
    }

    public function addAction()
    {
        return parent::addAction();
    }

    public function editView(int $id) : bool
    {
        return parent::editView($id);
    }

    public function editAction($id)
    {
        return parent::editAction($id);
    }
}
