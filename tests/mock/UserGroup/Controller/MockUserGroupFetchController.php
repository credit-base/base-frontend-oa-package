<?php
namespace Base\Package\UserGroup\Controller;

use Sdk\UserGroup\Repository\UserGroupRepository;

use Sdk\Crew\Repository\CrewRepository;

use Sdk\UserGroup\Repository\DepartmentRepository;

use Marmot\Framework\Adapter\ConcurrentAdapter;

class MockUserGroupFetchController extends UserGroupFetchController
{
    public function getRepository() : UserGroupRepository
    {
        return parent::getRepository();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function getDepartmentRepository() : DepartmentRepository
    {
        return parent::getDepartmentRepository();
    }

    public function getConcurrentAdapter() : ConcurrentAdapter
    {
        return parent::getConcurrentAdapter();
    }

    public function filterAction():  bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }
}
