<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\WbjTemplateTranslator;
use Sdk\Template\Model\WbjTemplate;

class MockWbjTemplateDetailView extends WbjTemplateDetailView
{
    public function __construct()
    {
        parent::__construct(new WbjTemplate());
    }

    public function getWbjTemplate()
    {
        return parent::getWbjTemplate();
    }

    public function getTranslator(): WbjTemplateTranslator
    {
        return parent::getTranslator();
    }
}
