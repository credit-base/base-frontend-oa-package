<?php
namespace Base\Package\Template\View\Json;

class MockConfigureView extends ConfigureView
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getData()
    {
        return parent::getData();
    }
}
