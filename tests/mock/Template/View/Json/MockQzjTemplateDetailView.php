<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\QzjTemplateTranslator;
use Sdk\Template\Model\QzjTemplate;

class MockQzjTemplateDetailView extends QzjTemplateDetailView
{
    public function __construct()
    {
        parent::__construct(new QzjTemplate());
    }

    public function getQzjTemplate()
    {
        return parent::getQzjTemplate();
    }

    public function getTranslator(): QzjTemplateTranslator
    {
        return parent::getTranslator();
    }
}
