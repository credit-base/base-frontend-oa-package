<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\BaseTemplateTranslator;
use Sdk\Template\Model\BaseTemplate;

class MockBaseTemplateDetailView extends BaseTemplateDetailView
{
    public function __construct()
    {
        parent::__construct(new BaseTemplate());
    }

    public function getBaseTemplate()
    {
        return parent::getBaseTemplate();
    }

    public function getTranslator(): BaseTemplateTranslator
    {
        return parent::getTranslator();
    }
}
