<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\QzjTemplateTranslator;

class MockQzjTemplateListView extends QzjTemplateListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getQzjTemplateList(): array
    {
        return parent::getQzjTemplateList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): QzjTemplateTranslator
    {
        return parent::getTranslator();
    }
}
