<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\BjTemplateTranslator;

class MockBjTemplateListView extends BjTemplateListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getList(): array
    {
        return parent::getList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): BjTemplateTranslator
    {
        return parent::getTranslator();
    }
}
