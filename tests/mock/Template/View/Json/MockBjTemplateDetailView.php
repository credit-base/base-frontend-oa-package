<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\BjTemplateTranslator;
use Sdk\Template\Model\BjTemplate;

class MockBjTemplateDetailView extends BjTemplateDetailView
{
    public function __construct()
    {
        parent::__construct(new BjTemplate());
    }

    public function getBjTemplate()
    {
        return parent::getBjTemplate();
    }

    public function getTranslator(): BjTemplateTranslator
    {
        return parent::getTranslator();
    }
}
