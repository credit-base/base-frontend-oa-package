<?php
namespace Base\Package\Template\View\Json;

use Sdk\Template\Translator\GbTemplateTranslator;
use Sdk\Template\Model\GbTemplate;

class MockGbTemplateDetailView extends GbTemplateDetailView
{
    public function __construct()
    {
        parent::__construct(new GbTemplate());
    }

    public function getGbTemplate()
    {
        return parent::getGbTemplate();
    }

    public function getTranslator(): GbTemplateTranslator
    {
        return parent::getTranslator();
    }
}
