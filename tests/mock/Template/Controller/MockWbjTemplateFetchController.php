<?php
namespace Base\Package\Template\Controller;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;

use Sdk\Template\Repository\WbjTemplateRepository;
use Sdk\Template\Translator\WbjTemplateTranslator;

class MockWbjTemplateFetchController extends WbjTemplateFetchController
{
    public function getRepository() : WbjTemplateRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function getWbjTemplateTranslator() : WbjTemplateTranslator
    {
        return parent::getWbjTemplateTranslator();
    }

    public function getWbjTemplate(int $id)
    {
        return parent::getWbjTemplate($id);
    }

    public function getWbjTemplateArrayData(string $id) : array
    {
        return parent::getWbjTemplateArrayData($id);
    }

    public function getSpreadsheet() : Spreadsheet
    {
        return parent::getSpreadsheet();
    }

    public function getTemplateSpreadsheet()
    {
        return parent::getTemplateSpreadsheet();
    }

    public function getItemCellValue(array $item, Spreadsheet $spreadsheet)
    {
        return parent::getItemCellValue($item, $spreadsheet);
    }

    public function getResultWriter($filName, $spreadsheet)
    {
        return parent::getResultWriter($filName, $spreadsheet);
    }

    public function getWriter(Spreadsheet $spreadsheet) : Writer
    {
        return parent::getWriter($spreadsheet);
    }
}
