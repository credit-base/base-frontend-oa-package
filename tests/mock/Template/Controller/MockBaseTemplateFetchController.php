<?php
namespace Base\Package\Template\Controller;

use Sdk\Template\Repository\BaseTemplateRepository;

class MockBaseTemplateFetchController extends BaseTemplateFetchController
{
    public function getRepository() : BaseTemplateRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
