<?php
namespace Base\Package\Template\Controller;

use Sdk\Template\WidgetRule\TemplateWidgetRule;
use Sdk\Template\Repository\WbjTemplateRepository;

use Marmot\Framework\Classes\CommandBus;

class MockWbjTemplateOperationController extends WbjTemplateOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function getRepository() : WbjTemplateRepository
    {
        return parent::getRepository();
    }

    public function addView() : bool
    {
        return parent::addView();
    }

    public function addAction()
    {
        return parent::addAction();
    }

    public function editView(int $id) : bool
    {
        return parent::editView($id);
    }
    
    public function editAction(int $id)
    {
        return parent::editAction($id);
    }

    public function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return parent::getTemplateWidgetRule();
    }

    public function validateCommonScenario(
        $name,
        $identify,
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $description,
        $items
    ) : bool {
        return parent::validateCommonScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items
        );
    }
}
