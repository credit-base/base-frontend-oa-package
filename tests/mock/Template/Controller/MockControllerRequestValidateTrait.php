<?php
namespace Base\Package\Template\Controller;

use Sdk\Template\WidgetRule\TemplateWidgetRule;

/**
 * @SuppressWarnings(PHPMD)
 */
class MockControllerRequestValidateTrait
{
    use ControllerRequestValidateTrait;

    public function publicGetTemplateWidgetRule():TemplateWidgetRule
    {
        return $this->getTemplateWidgetRule();
    }

    public function publicGetCommonRequest()
    {
        return $this->getCommonRequest();
    }

    public function publicGetSubjectCategory($subjectCategory)
    {
        return $this->getSubjectCategory($subjectCategory);
    }

    public function publicGetItems($items)
    {
        return $this->getItems($items);
    }

    public function publicValidateCommonScenario(
        $name,
        $identify,
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $description,
        $items
    ):bool {
        return $this->validateCommonScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items
        );
    }

    public function publicGetSubjectCategoryCn($subjectCategory)
    {
        return $this->getSubjectCategoryCn($subjectCategory);
    }

    public function publicGetDimensionCn($dimension)
    {
        return $this->getDimensionCn($dimension);
    }

    public function publicGetInfoClassifyCn($infoClassify)
    {
        return $this->getInfoClassifyCn($infoClassify);
    }

    public function publicGetInfoCategoryCn($infoCategory)
    {
        return $this->getInfoCategoryCn($infoCategory);
    }

    public function publicGetExchangeFrequencyCn($exchangeFrequency)
    {
        return $this->getExchangeFrequencyCn($exchangeFrequency);
    }

    public function publicGetTypeCn($type)
    {
        return $this->getTypeCn($type);
    }
}
