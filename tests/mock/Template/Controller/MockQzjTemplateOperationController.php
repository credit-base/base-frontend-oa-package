<?php
namespace Base\Package\Template\Controller;

use Sdk\Template\WidgetRule\TemplateWidgetRule;

use Marmot\Framework\Classes\CommandBus;

class MockQzjTemplateOperationController extends QzjTemplateOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function addView() : bool
    {
        return parent::addView();
    }

    public function addAction()
    {
        return parent::addAction();
    }

    public function editView(int $id) : bool
    {
        return parent::editView($id);
    }
    
    public function editAction(int $id)
    {
        return parent::editAction($id);
    }

    public function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return parent::getTemplateWidgetRule();
    }

    public function validateQzjCommonScenario(
        $infoCategory,
        $category
    ) : bool {
        return parent::validateQzjCommonScenario(
            $infoCategory,
            $category
        );
    }
}
