<?php
namespace Base\Package\Template\Controller;

use Sdk\Template\Repository\QzjTemplateRepository;

class MockQzjTemplateFetchController extends QzjTemplateFetchController
{
    public function getRepository() : QzjTemplateRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
