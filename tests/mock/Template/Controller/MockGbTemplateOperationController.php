<?php
namespace Base\Package\Template\Controller;

use Sdk\Template\Repository\GbTemplateRepository;
use Sdk\Template\WidgetRule\TemplateWidgetRule;

use Marmot\Framework\Classes\CommandBus;

class MockGbTemplateOperationController extends GbTemplateOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function getRepository() : GbTemplateRepository
    {
        return parent::getRepository();
    }

    public function addView() : bool
    {
        return parent::addView();
    }

    public function editView(int $id) : bool
    {
        return parent::editView($id);
    }

    public function addAction()
    {
        return parent::addAction();
    }

    public function editAction(int $id)
    {
        return parent::editAction($id);
    }

    public function validateCommonScenario(
        $name,
        $identify,
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $description,
        $items
    ) : bool {
        return parent::validateCommonScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items
        );
    }
    
    public function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return parent::getTemplateWidgetRule();
    }
}
