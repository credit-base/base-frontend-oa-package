<?php
namespace Base\Package\Member\View\Json;

use Sdk\Member\Model\Member;
use Sdk\Member\Translator\MemberTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct(new Member());
    }

    public function getMember()
    {
        return parent::getMember();
    }

    public function getTranslator(): MemberTranslator
    {
        return parent::getTranslator();
    }
}
