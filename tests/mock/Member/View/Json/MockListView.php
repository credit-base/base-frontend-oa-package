<?php
namespace Base\Package\Member\View\Json;

use Sdk\Member\Translator\MemberTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getMember(): array
    {
        return parent::getMember();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): MemberTranslator
    {
        return parent::getTranslator();
    }
}
