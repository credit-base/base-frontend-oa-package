<?php
namespace Base\Package\Member\Controller;

use Sdk\Member\Repository\MemberRepository;

class MockMemberFetchController extends MemberFetchController
{
    public function getRepository() : MemberRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
