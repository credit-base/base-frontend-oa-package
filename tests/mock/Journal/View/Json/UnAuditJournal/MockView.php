<?php
namespace Base\Package\Journal\View\Json\UnAuditJournal;

use Sdk\Journal\Model\UnAuditJournal;
use Sdk\Journal\Translator\UnAuditJournalTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct(new UnAuditJournal());
    }

    public function getUnAuditJournal()
    {
        return parent::getUnAuditJournal();
    }

    public function getTranslator(): UnAuditJournalTranslator
    {
        return parent::getTranslator();
    }
}
