<?php
namespace Base\Package\Journal\View\Json\UnAuditJournal;

use Sdk\Journal\Translator\UnAuditJournalTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getList(): array
    {
        return parent::getList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): UnAuditJournalTranslator
    {
        return parent::getTranslator();
    }
}
