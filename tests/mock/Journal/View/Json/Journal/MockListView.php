<?php
namespace Base\Package\Journal\View\Json\Journal;

use Sdk\Journal\Translator\JournalTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getJournal(): array
    {
        return parent::getJournal();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): JournalTranslator
    {
        return parent::getTranslator();
    }
}
