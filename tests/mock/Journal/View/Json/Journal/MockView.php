<?php
namespace Base\Package\Journal\View\Json\Journal;

use Sdk\Journal\Model\Journal;
use Sdk\Journal\Translator\JournalTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct(new Journal());
    }

    public function getJournal()
    {
        return parent::getJournal();
    }

    public function getTranslator(): JournalTranslator
    {
        return parent::getTranslator();
    }
}
