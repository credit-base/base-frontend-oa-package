<?php
namespace Base\Package\Journal\Controller;

use Sdk\Journal\Repository\JournalRepository;

use Marmot\Framework\Classes\CommandBus;

class MockJournalOperationController extends JournalOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function getRepository() : JournalRepository
    {
        return parent::getRepository();
    }

    public function addView() : bool
    {
        return parent::addView();
    }

    public function addAction()
    {
        return parent::addAction();
    }

    public function editView(int $id) : bool
    {
        return parent::editView($id);
    }

    public function editAction(int $id)
    {
        return parent::editAction($id);
    }
}
