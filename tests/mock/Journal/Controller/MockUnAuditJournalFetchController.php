<?php
namespace Base\Package\Journal\Controller;

use Sdk\Journal\Repository\UnAuditJournalRepository;

class MockUnAuditJournalFetchController extends UnAuditJournalFetchController
{
    public function getRepository() : UnAuditJournalRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource) : bool
    {
        return parent::checkUserHasPurview($resource);
    }

    public function checkUserHasJournalPurview($resource): bool
    {
        return parent::checkUserHasJournalPurview($resource);
    }
}
