<?php
namespace Base\Package\Journal\Controller;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\Journal\WidgetRules\JournalWidgetRules;

class MockJournalValidateTrait
{
    use JournalValidateTrait;

    public function publicGetJournalWidgetRules() : JournalWidgetRules
    {
        return $this->getJournalWidgetRules();
    }

    public function publicGetWidgetRules() : WidgetRules
    {
        return $this->getWidgetRules();
    }

    public function publicValidateCommonScenario(
        $title,
        $source,
        $cover,
        $attachment,
        $authImages,
        $description,
        $year,
        $status
    ) : bool {
        return $this->validateCommonScenario(
            $title,
            $source,
            $cover,
            $attachment,
            $authImages,
            $description,
            $year,
            $status
        );
    }

    public function publicValidateRejectScenario($rejectReason) : bool
    {
        return $this->validateRejectScenario($rejectReason);
    }
}
