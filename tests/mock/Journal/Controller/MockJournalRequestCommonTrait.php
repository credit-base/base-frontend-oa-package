<?php
namespace Base\Package\Journal\Controller;

class MockJournalRequestCommonTrait
{
    use JournalRequestCommonTrait;

    public function publicGetRequestCommonData()
    {
        return $this->getRequestCommonData();
    }
}
