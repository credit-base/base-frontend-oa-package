<?php
namespace Base\Package\Journal\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditJournalApproveController extends UnAuditJournalApproveController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function approveAction(int $id): bool
    {
        return parent::approveAction($id);
    }

    public function rejectAction(int $id): bool
    {
        return parent::rejectAction($id);
    }
}
