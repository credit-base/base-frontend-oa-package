<?php
namespace Base\Package\Crew\Controller;

use Sdk\Crew\Repository\CrewRepository;

use Marmot\Framework\Classes\CommandBus;

class MockCrewOperationController extends CrewOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function getRepository() : CrewRepository
    {
        return parent::getRepository();
    }

    public function addView() : bool
    {
        return parent::addView();
    }

    public function addAction()
    {
        return parent::addAction();
    }

    public function editView(int $id) : bool
    {
        return parent::editView($id);
    }

    public function editAction(int $id)
    {
        return parent::editAction($id);
    }
}
