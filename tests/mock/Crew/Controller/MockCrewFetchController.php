<?php
namespace Base\Package\Crew\Controller;

use Sdk\Crew\Repository\CrewRepository;

class MockCrewFetchController extends CrewFetchController
{
    public function getRepository() : CrewRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
