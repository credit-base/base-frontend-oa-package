<?php
namespace Base\Package\Crew\Controller;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\User\WidgetRules\UserWidgetRules;
use Sdk\Crew\WidgetRules\CrewWidgetRules;

class MockCrewValidateTrait
{
    use CrewValidateTrait;

    public function publicGetUserWidgetRules() : UserWidgetRules
    {
        return $this->getUserWidgetRules();
    }

    public function publicGetCrewWidgetRules() : CrewWidgetRules
    {
        return $this->getCrewWidgetRules();
    }

    public function publicGetWidgetRules() : WidgetRules
    {
        return $this->getWidgetRules();
    }

    public function publicValidateCommonScenario(
        $realName,
        $cardId,
        $category,
        $purview,
        $userGroupId,
        $departmentId
    ) : bool {
        return $this->validateCommonScenario(
            $realName,
            $cardId,
            $category,
            $purview,
            $userGroupId,
            $departmentId
        );
    }

    public function publicValidateAddScenario(
        $cellphone,
        $password,
        $confirmPassword
    ) : bool {
        return $this->validateAddScenario(
            $cellphone,
            $password,
            $confirmPassword
        );
    }

    public function publicValidateSignInScenario(
        $userName,
        $password,
        $captcha
    ) : bool {
        return $this->validateSignInScenario(
            $userName,
            $password,
            $captcha
        );
    }
}
