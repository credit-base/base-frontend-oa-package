<?php
namespace Base\Package\Crew\View\Json;

use Base\Package\Common\View\MockViewTrait;
use Sdk\Crew\Translator\CrewTranslator;

class MockCrewListView extends CrewListView
{
    use MockViewTrait;

    public function getTranslator(): CrewTranslator
    {
        return parent::getTranslator();
    }

    public function getCrews() : array
    {
        return parent::getCrews();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }

    public function __construct()
    {
        parent::__construct([], 0);
    }
}
