<?php
namespace Base\Package\Crew\View\Json;

use Base\Package\Common\View\MockViewTrait;

class MockSignInView extends SignInView
{
    use MockViewTrait;

    public function __construct()
    {
        parent::__construct([]);
    }
}
