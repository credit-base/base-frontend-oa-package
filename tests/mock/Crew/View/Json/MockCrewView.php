<?php
namespace Base\Package\Crew\View\Json;

use Base\Package\Common\View\MockViewTrait;
use Sdk\Crew\Model\Crew;
use Sdk\Crew\Translator\CrewTranslator;

class MockCrewView extends CrewView
{
    use MockViewTrait;

    public function getTranslator(): CrewTranslator
    {
        return parent::getTranslator();
    }

    public function getCrew()
    {
        return parent::getCrew();
    }

    public function __construct()
    {
        parent::__construct(new Crew(2));
    }
}
