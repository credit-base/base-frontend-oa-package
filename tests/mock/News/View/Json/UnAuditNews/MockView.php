<?php
namespace Base\Package\News\View\Json\UnAuditNews;

use Sdk\News\Model\UnAuditNews;
use Sdk\News\Translator\UnAuditNewsTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct(new UnAuditNews());
    }

    public function getUnAuditNews()
    {
        return parent::getUnAuditNews();
    }

    public function getTranslator(): UnAuditNewsTranslator
    {
        return parent::getTranslator();
    }
}
