<?php
namespace Base\Package\News\View\Json\UnAuditNews;

use Sdk\News\Translator\UnAuditNewsTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getList(): array
    {
        return parent::getList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): UnAuditNewsTranslator
    {
        return parent::getTranslator();
    }
}
