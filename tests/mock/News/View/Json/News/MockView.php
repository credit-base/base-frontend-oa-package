<?php
namespace Base\Package\News\View\Json\News;

use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct(new News());
    }

    public function getNews()
    {
        return parent::getNews();
    }

    public function getTranslator(): NewsTranslator
    {
        return parent::getTranslator();
    }
}
