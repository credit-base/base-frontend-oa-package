<?php
namespace Base\Package\News\View\Json\News;

use Sdk\News\Translator\NewsTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getNews(): array
    {
        return parent::getNews();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): NewsTranslator
    {
        return parent::getTranslator();
    }
}
