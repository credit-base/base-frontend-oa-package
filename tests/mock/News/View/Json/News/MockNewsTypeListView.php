<?php


namespace Base\Package\News\View\Json\News;

class MockNewsTypeListView extends NewsTypeListView
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getData(): array
    {
        return parent::getData();
    }

    public function formartArray($data) : array
    {
        return parent::formartArray($data);
    }

    public function formartParentList($parentList) : array
    {
        return parent::formartParentList($parentList);
    }

    public function pinArray($list, $array):array
    {
        return parent::pinArray($list, $array);
    }

    public function formartCategoryList($categoryList) : array
    {
        return parent::formartCategoryList($categoryList);
    }

    public function dealNotUseData($data) : array
    {
        return parent::dealNotUseData($data);
    }
}
