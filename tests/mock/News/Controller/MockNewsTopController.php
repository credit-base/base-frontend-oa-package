<?php
namespace Base\Package\News\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockNewsTopController extends NewsTopController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function topAction(int $id): bool
    {
        return parent::topAction($id);
    }

    public function cancelTopAction(int $id): bool
    {
        return parent::cancelTopAction($id);
    }
}
