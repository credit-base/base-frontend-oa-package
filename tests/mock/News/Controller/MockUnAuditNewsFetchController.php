<?php
namespace Base\Package\News\Controller;

use Sdk\News\Repository\UnAuditNewsRepository;

class MockUnAuditNewsFetchController extends UnAuditNewsFetchController
{
    public function getRepository() : UnAuditNewsRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource) : bool
    {
        return parent::checkUserHasPurview($resource);
    }

    public function checkUserHasNewsPurview($resource): bool
    {
        return parent::checkUserHasNewsPurview($resource);
    }
}
