<?php
namespace Base\Package\News\Controller;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\News\WidgetRules\NewsWidgetRules;

class MockNewsValidateTrait
{
    use NewsValidateTrait;

    public function publicGetNewsWidgetRules() : NewsWidgetRules
    {
        return $this->getNewsWidgetRules();
    }

    public function publicGetWidgetRules() : WidgetRules
    {
        return $this->getWidgetRules();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function publicValidateCommonScenario(
        $title,
        $source,
        $cover,
        $attachments,
        $bannerImage,
        $content,
        $newsType,
        $dimension,
        $status,
        $stick,
        $bannerStatus,
        $homePageShowStatus
    ) : bool {
        return $this->validateCommonScenario(
            $title,
            $source,
            $cover,
            $attachments,
            $bannerImage,
            $content,
            $newsType,
            $dimension,
            $status,
            $stick,
            $bannerStatus,
            $homePageShowStatus
        );
    }

    public function publicValidateRejectScenario(
        $rejectReason
    ) : bool {
        return $this->validateRejectScenario(
            $rejectReason
        );
    }

    public function publicValidateMoveScenario(
        $newsType
    ) : bool {
        return $this->validateMoveScenario(
            $newsType
        );
    }
}
