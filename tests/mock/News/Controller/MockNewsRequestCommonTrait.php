<?php
namespace Base\Package\News\Controller;

class MockNewsRequestCommonTrait
{
    use NewsRequestCommonTrait;

    public function publicGetRequestCommonData()
    {
        return $this->getRequestCommonData();
    }
}
