<?php
namespace Base\Package\News\Controller;

use Sdk\News\Repository\NewsRepository;

use Marmot\Framework\Classes\CommandBus;

class MockNewsOperationController extends NewsOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function getRepository() : NewsRepository
    {
        return parent::getRepository();
    }

    public function addView() : bool
    {
        return parent::addView();
    }

    public function addAction()
    {
        return parent::addAction();
    }

    public function editView(int $id) : bool
    {
        return parent::editView($id);
    }

    public function editAction(int $id)
    {
        return parent::editAction($id);
    }
}
