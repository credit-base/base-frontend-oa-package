<?php
namespace Base\Package\News\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditNewsApproveController extends UnAuditNewsApproveController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function approveAction(int $id): bool
    {
        return parent::approveAction($id);
    }

    public function rejectAction(int $id): bool
    {
        return parent::rejectAction($id);
    }
}
