<?php
namespace Base\Package\News\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockNewsEnableController extends NewsEnableController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function enableAction(int $id): bool
    {
        return parent::enableAction($id);
    }

    public function disableAction(int $id): bool
    {
        return parent::disableAction($id);
    }
}
