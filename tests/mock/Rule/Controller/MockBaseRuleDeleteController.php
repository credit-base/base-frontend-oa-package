<?php

namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockBaseRuleDeleteController extends BaseRuleDeleteController
{
    public function deleteAction(int $id): bool
    {
        return parent::deleteAction($id);
    }

    public function getCommandBus(): CommandBus
    {
        return parent::getCommandBus();
    }
}
