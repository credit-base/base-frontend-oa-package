<?php

namespace Base\Package\Rule\Controller;

class MockRuleOperationController extends RuleOperationController
{
    public function addAction()
    {
        return parent::addAction();
    }

    public function editAction(int $id)
    {
        return parent::editAction($id);
    }

    public function getCommandBus()
    {
        return parent::getCommandBus();
    }

    public function addView(): bool
    {
        return parent::addView();
    }

    public function editView(int $id): bool
    {
        return parent::editView($id);
    }

    protected function validateAddScenario(
        $rules,
        $transformationTemplate,
        $sourceTemplate,
        $transformationCategory,
        $sourceCategory
    ): bool {
        return parent::validateAddScenario(
            $rules,
            $transformationTemplate,
            $sourceTemplate,
            $transformationCategory,
            $sourceCategory
        );
    }
}
