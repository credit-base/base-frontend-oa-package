<?php

namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockRuleDeleteController extends RuleDeleteController
{
    public function getCommandBus(): CommandBus
    {
        return parent::getCommandBus();
    }

    public function deleteAction(int $id): bool
    {
        return parent::deleteAction($id);
    }
}
