<?php

namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditRuleRevokeController extends UnAuditRuleRevokeController
{
    public function revokeAction(int $id): bool
    {
        return parent::revokeAction($id);
    }

    public function getCommandBus(): CommandBus
    {
        return parent::getCommandBus();
    }
}
