<?php


namespace Base\Package\Rule\Controller\Traits;

class MockUnAuditRuleOperationControllerTrait
{
    use UnAuditRuleOperationControllerTrait;

    public function addViewPublic() : bool
    {
        return $this->addView();
    }

    public function getCommandBusPublic()
    {
        return $this->getCommandBus();
    }

    public function editViewPublic(int $id) : bool
    {
        return $this->editView($id);
    }
}
