<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Framework\Classes\CommandBus;

class MockRuleOperationControllerTrait
{
    use RuleOperationControllerTrait;

    public function publicGetCommandBus(): CommandBus
    {
        return $this->getCommandBus();
    }

    public function publicAddView() : bool
    {
        return $this->addView();
    }
    public function publicEditView(int $id) : bool
    {
        return $this->editView($id);
    }
}
