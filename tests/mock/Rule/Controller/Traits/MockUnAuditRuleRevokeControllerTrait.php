<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditRuleRevokeControllerTrait
{
    use UnAuditRuleRevokeControllerTrait;

    protected function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}
