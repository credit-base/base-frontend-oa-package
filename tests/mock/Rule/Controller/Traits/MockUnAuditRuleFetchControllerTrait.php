<?php


namespace Base\Package\Rule\Controller\Traits;

use Sdk\Rule\Repository\UnAuditRuleRepository;

class MockUnAuditRuleFetchControllerTrait
{
    use UnAuditRuleFetchControllerTrait;


    public function getRepositoryPublic() : UnAuditRuleRepository
    {
        return $this->getRepository();
    }

    public function fetchUnAuditRuleListPublic(int $type = 0) : bool
    {
        return $this->fetchUnAuditRuleList($type);
    }

    public function fetchUnAuditRuleOnePublic(int $id) : bool
    {
        return $this->fetchUnAuditRuleOne($id);
    }
}
