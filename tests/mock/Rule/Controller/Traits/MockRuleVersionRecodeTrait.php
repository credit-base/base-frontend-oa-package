<?php
namespace Base\Package\Rule\Controller\Traits;

use Sdk\Rule\Repository\UnAuditRuleRepository;
use Sdk\Rule\Model\UnAuditRule;

class MockRuleVersionRecodeTrait
{
    use RuleVersionRecodeTrait;

    public function publicGetUnAuditRuleRepository() : UnAuditRuleRepository
    {
        return $this->getUnAuditRuleRepository();
    }

    public function publicGetUnAuditRuleList(int $relationId, string $resource = '')
    {
        return $this->getUnAuditRuleList($relationId, $resource);
    }

    public function filterFormatChangePublic(int $type = 0)
    {
        return $this->filterFormatChange($type);
    }
}
