<?php
namespace Base\Package\Rule\Controller\Traits;

use Sdk\Rule\Repository\RuleRepository;

class MockRuleFetchControllerTrait
{
    use RuleFetchControllerTrait;

    public function publicGetRepository(): RuleRepository
    {
        return $this->getRepository();
    }

    public function publicFilterFormatChange(int $type)
    {
        return $this->filterFormatChange($type);
    }
}
