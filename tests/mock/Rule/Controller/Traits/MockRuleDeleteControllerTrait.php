<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Framework\Classes\CommandBus;

class MockRuleDeleteControllerTrait
{
    use RuleDeleteControllerTrait;

    public function publicGetCommandBus(): CommandBus
    {
        return $this->getCommandBus();
    }
}
