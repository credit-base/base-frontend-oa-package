<?php
namespace Base\Package\Rule\Controller\Traits;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditRuleApproveControllerTrait
{
    use UnAuditRuleApproveControllerTrait;

    public function publicGetCommandBus() : CommandBus
    {
        return $this->getCommandBus();
    }
}
