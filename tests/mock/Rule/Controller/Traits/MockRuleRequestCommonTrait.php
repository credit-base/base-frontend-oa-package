<?php
namespace Base\Package\Rule\Controller\Traits;

class MockRuleRequestCommonTrait
{
    use RuleRequestCommonTrait;

    public function getRulesDecodePublic(array $rules):array
    {
        return $this->getRulesDecode($rules);
    }

    public function getCompletionRuleDecodePublic($completionRules):array
    {
        return $this->getCompletionRuleDecode($completionRules);
    }

    public function getBaseDecodePublic(array $base):array
    {
        return $this->getBaseDecode($base);
    }

    public function getDeDuplicationRuleDecodePublic(array $deDuplicationRule):array
    {
        return $this->getDeDuplicationRuleDecode($deDuplicationRule);
    }
}
