<?php

namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditBaseRuleRevokeController extends UnAuditBaseRuleRevokeController
{
    public function revokeAction(int $id): bool
    {
        return parent::revokeAction($id);
    }

    public function getCommandBus(): CommandBus
    {
        return parent::getCommandBus();
    }
}
