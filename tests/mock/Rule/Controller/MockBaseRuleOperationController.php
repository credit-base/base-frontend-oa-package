<?php
namespace Base\Package\Rule\Controller;

class MockBaseRuleOperationController extends BaseRuleOperationController
{
    public function addAction()
    {
        return parent::addAction();
    }

    public function editAction(int $id)
    {
        return parent::editAction($id);
    }
}
