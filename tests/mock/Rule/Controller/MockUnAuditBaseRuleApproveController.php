<?php

namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditBaseRuleApproveController extends UnAuditBaseRuleApproveController
{
    public function approveAction(int $id): bool
    {
        return parent::approveAction($id);
    }

    public function getCommandBus(): CommandBus
    {
        return parent::getCommandBus();
    }

    public function rejectAction(int $id): bool
    {
        return parent::rejectAction($id);
    }
}
