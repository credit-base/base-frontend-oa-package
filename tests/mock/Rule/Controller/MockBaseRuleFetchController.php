<?php

namespace Base\Package\Rule\Controller;

use Sdk\Rule\Repository\RuleRepository;

class MockBaseRuleFetchController extends BaseRuleFetchController
{
    public function filterAction(): bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource): bool
    {
        return parent::checkUserHasPurview($resource);
    }

    public function getRepository(): RuleRepository
    {
        return parent::getRepository();
    }
}
