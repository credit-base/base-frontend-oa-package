<?php

namespace Base\Package\Rule\Controller;

use Sdk\Rule\Repository\UnAuditRuleRepository;

class MockUnAuditBaseRuleFetchController extends UnAuditBaseRuleFetchController
{
    public function checkUserHasPurview($resource): bool
    {
        return parent::checkUserHasPurview($resource);
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }

    public function filterAction(): bool
    {
        return parent::filterAction();
    }

    public function getRepository(): UnAuditRuleRepository
    {
        return parent::getRepository();
    }

    public function fetchUnAuditRuleOne(int $id): bool
    {
        return parent::fetchUnAuditRuleOne($id);
    }
}
