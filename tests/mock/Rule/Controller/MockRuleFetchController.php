<?php

namespace Base\Package\Rule\Controller;

class MockRuleFetchController extends RuleFetchController
{
    public function filterAction(): bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource): bool
    {
        return parent::checkUserHasPurview($resource);
    }
}
