<?php

namespace Base\Package\Rule\Controller;

use Sdk\Rule\Repository\UnAuditRuleRepository;

class MockUnAuditRuleFetchController extends UnAuditRuleFetchController
{
    public function filterAction(): bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource): bool
    {
        return parent::checkUserHasPurview($resource);
    }

    public function checkUserHasRulePurview($resource, $type = 0): bool
    {
        return parent::checkUserHasRulePurview($resource, $type);
    }

    public function getRepository(): UnAuditRuleRepository
    {
        return parent::getRepository();
    }

    public function fetchUnAuditRuleList(int $type = 0): bool
    {
        return parent::fetchUnAuditRuleList($type);
    }
}
