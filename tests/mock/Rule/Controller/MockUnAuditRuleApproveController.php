<?php

namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditRuleApproveController extends UnAuditRuleApproveController
{
    public function approveAction(int $id): bool
    {
        return parent::approveAction($id);
    }

    public function rejectAction(int $id): bool
    {
        return parent::rejectAction($id);
    }

    public function getCommandBus(): CommandBus
    {
        return parent::getCommandBus();
    }
}
