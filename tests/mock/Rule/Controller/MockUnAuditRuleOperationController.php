<?php

namespace Base\Package\Rule\Controller;

use Base\Package\Rule\Controller\Traits\UnAuditRuleOperationControllerTrait;
use Sdk\Rule\WidgetRules\RuleWidgetRules;

class MockUnAuditRuleOperationController extends UnAuditRuleOperationController
{
    use UnAuditRuleOperationControllerTrait;

    public function addAction()
    {
        return parent::addAction();
    }

    public function editAction(int $id)
    {
        return parent::editAction($id);
    }

    public function getCommandBus()
    {
        return parent::getCommandBus();
    }

    public function editViews(int $id): bool
    {
        return $this->editView($id);
    }

    public function addViews(): bool
    {
        return $this->addView();
    }

    public function validateRejectScenarioPublic($rejectReason)
    {
        return $this->validateRejectScenario($rejectReason);
    }

    public function validateEditScenario($rules): bool
    {
        return parent::validateEditScenario($rules);
    }

    public function validateAddScenario(
        $rules,
        $transformationTemplate,
        $sourceTemplate,
        $transformationCategory,
        $sourceCategory
    ): bool {
        return parent::validateAddScenario(
            $rules,
            $transformationTemplate,
            $sourceTemplate,
            $transformationCategory,
            $sourceCategory
        );
    }

    public function getRuleWidgetRules(): RuleWidgetRules
    {
        return parent::getRuleWidgetRules();
    }
}
