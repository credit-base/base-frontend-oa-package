<?php

namespace Base\Package\Rule\Controller;

class MockUnAuditBaseRuleOperationController extends UnAuditBaseRuleOperationController
{
    public function addAction()
    {
        return parent::addAction();
    }

    public function editAction(int $id)
    {
        return parent::editAction($id);
    }
}
