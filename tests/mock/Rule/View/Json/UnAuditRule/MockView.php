<?php
namespace Base\Package\Rule\View\Json\UnAuditRule;

use Sdk\Rule\Model\UnAuditRule;
use Sdk\Rule\Translator\UnAuditRuleTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct(new UnAuditRule());
    }

    public function getUnAuditRule()
    {
        return parent::getUnAuditRule();
    }

    public function getUnAuditRuleTranslator(): UnAuditRuleTranslator
    {
        return parent::getUnAuditRuleTranslator();
    }

    public function getUnAuditRuleData():array
    {
        return parent::getUnAuditRuleData();
    }

    public function getVersionRecode()
    {
        return parent::getVersionRecode();
    }

    public function getUnAuditedInformation()
    {
        return parent::getUnAuditedInformation();
    }
}
