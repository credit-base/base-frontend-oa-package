<?php
namespace Base\Package\Rule\View\Json\UnAuditRule;

use Sdk\Rule\Translator\UnAuditRuleTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getUnAuditRuleList(): array
    {
        return parent::getUnAuditRuleList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getUnAuditRuleTranslator(): UnAuditRuleTranslator
    {
        return parent::getUnAuditRuleTranslator();
    }

    public function getResultUnAuditRuleList() : array
    {
        return parent::getResultUnAuditRuleList();
    }
}
