<?php

namespace Base\Package\Rule\View\Json;

class MockRulesContentFormatTrait
{
    use RulesContentFormatTrait;

    public function getRulesContentPublic($rules, $transformationTemplate, $sourceTemplate) : array
    {
        return $this->getRulesContent(
            $rules,
            $transformationTemplate,
            $sourceTemplate
        );
    }

    public function getTransformationRuleFormatPublic(
        $transformationRule,
        $transformationTemplateItems,
        $sourceTemplate
    ): array {
        return $this->getTransformationRuleFormat($transformationRule, $transformationTemplateItems, $sourceTemplate);
    }

    public function getDataByIdentifyPublic($identify, $templateItems) : array
    {
        return $this->getDataByIdentify($identify, $templateItems);
    }

    public function getCompletionRuleFormatPublic($completionRule, $templateItems) : array
    {
        return $this->getCompletionRuleFormat($completionRule, $templateItems);
    }

    public function getItemDataPublic($targetIdentify, $items, $targetTemplate) : array
    {
        return $this->getItemData($targetIdentify, $items, $targetTemplate);
    }

    public function getFormatDataByBasePublic(array $base):array
    {
        return $this->getFormatDataByBase($base);
    }

    public function getDeDuplicationRuleFormatPublic($deDuplicationRule, $templateItems) : array
    {
        return $this->getDeDuplicationRuleFormat($deDuplicationRule, $templateItems);
    }

    public function getSimplifyTemplatePublic(array $templates):array
    {
        return $this->getSimplifyTemplate($templates);
    }
}
