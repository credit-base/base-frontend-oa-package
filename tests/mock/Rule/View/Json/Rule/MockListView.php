<?php
namespace Base\Package\Rule\View\Json\Rule;

use Sdk\Rule\Translator\RuleTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getRuleList(): array
    {
        return parent::getRuleList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getRuleTranslator(): RuleTranslator
    {
        return parent::getRuleTranslator();
    }

    public function getResultRuleList() : array
    {
        return parent::getResultRuleList();
    }
}
