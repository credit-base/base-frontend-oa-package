<?php
namespace Base\Package\Rule\View\Json\Rule;

use Sdk\Rule\Model\Rule;
use Sdk\Rule\Translator\RuleTranslator;
use Sdk\Rule\Translator\UnAuditRuleTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct(new Rule());
    }

    public function getRule()
    {
        return parent::getRule();
    }

    public function getTranslator(): RuleTranslator
    {
        return parent::getTranslator();
    }

    public function getVersionRecode()
    {
        return parent::getVersionRecode();
    }

    public function getUnAuditRuleTranslator() : UnAuditRuleTranslator
    {
        return parent::getUnAuditRuleTranslator();
    }

    public function getRuleVersionRecode()
    {
        return parent::getRuleVersionRecode();
    }
}
