<?php
namespace Base\Package\ResourceCatalog\View;

class MockSearchDataFormatTrait
{
    use SearchDataFormatTrait;

    public function itemsRelationTemplatePublic(array $itemsData, array $template):array
    {
        return $this->itemsRelationTemplate($itemsData, $template);
    }

    public function getTemplateIdentifyPublic(array $items) : array
    {
        return $this->getTemplateIdentify($items);
    }
}
