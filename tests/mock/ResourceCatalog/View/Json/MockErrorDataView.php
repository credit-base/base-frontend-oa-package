<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Sdk\ResourceCatalog\Model\ErrorData;
use Sdk\ResourceCatalog\Translator\ErrorDataTranslator;

class MockErrorDataView extends ErrorDataView
{
    public function __construct()
    {
        parent::__construct(new ErrorData());
    }

    public function getErrorData()
    {
        return parent::getErrorData();
    }

    public function getTranslator(): ErrorDataTranslator
    {
        return parent::getTranslator();
    }

    public function getData() : array
    {
        return parent::getData();
    }
}
