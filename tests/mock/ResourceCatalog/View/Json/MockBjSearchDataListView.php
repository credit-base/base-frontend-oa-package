<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

class MockBjSearchDataListView extends BjSearchDataListView
{
    public function getBjSearchData(): array
    {
        return parent::getBjSearchData();
    }

    public function getTranslator(): BjSearchDataTranslator
    {
        return parent::getTranslator();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function __construct()
    {
        parent::__construct([], 0);
    }
}
