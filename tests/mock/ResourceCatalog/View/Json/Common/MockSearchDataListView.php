<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

class MockSearchDataListView extends SearchDataListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getBjSearchData(): array
    {
        return parent::getBjSearchData();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): BjSearchDataTranslator
    {
        return parent::getTranslator();
    }
}
