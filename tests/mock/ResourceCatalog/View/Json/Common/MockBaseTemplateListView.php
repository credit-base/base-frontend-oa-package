<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use Sdk\Template\Translator\BaseTemplateTranslator;

class MockBaseTemplateListView extends BaseTemplateListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getList(): array
    {
        return parent::getList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): BaseTemplateTranslator
    {
        return parent::getTranslator();
    }
}
