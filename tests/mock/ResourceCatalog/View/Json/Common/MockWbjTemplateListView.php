<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use Sdk\Template\Translator\WbjTemplateTranslator;

class MockWbjTemplateListView extends WbjTemplateListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getList(): array
    {
        return parent::getList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): WbjTemplateTranslator
    {
        return parent::getTranslator();
    }
}
