<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use Sdk\Template\Translator\GbTemplateTranslator;

class MockGbTemplateListView extends GbTemplateListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getList(): array
    {
        return parent::getList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): GbTemplateTranslator
    {
        return parent::getTranslator();
    }
}
