<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Sdk\ResourceCatalog\Model\GbSearchData;
use Sdk\ResourceCatalog\Translator\GbSearchDataTranslator;

class MockGbSearchDataView extends GbSearchDataView
{
    public function __construct()
    {
        parent::__construct(new GbSearchData());
    }

    public function getGbSearchData()
    {
        return parent::getGbSearchData();
    }

    public function getTranslator(): GbSearchDataTranslator
    {
        return parent::getTranslator();
    }
}
