<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Sdk\ResourceCatalog\Model\ErrorData;

class MockErrorDataEditView extends ErrorDataEditView
{
    public function __construct()
    {
        parent::__construct(new ErrorData());
    }
}
