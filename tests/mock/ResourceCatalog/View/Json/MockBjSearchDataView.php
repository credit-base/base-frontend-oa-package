<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Sdk\ResourceCatalog\Model\BjSearchData;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

class MockBjSearchDataView extends BjSearchDataView
{
    public function __construct()
    {
        parent::__construct(new BjSearchData());
    }

    public function getBjSearchData()
    {
        return parent::getBjSearchData();
    }

    public function getTranslator(): BjSearchDataTranslator
    {
        return parent::getTranslator();
    }
}
