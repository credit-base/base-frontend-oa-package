<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Sdk\ResourceCatalog\Translator\GbSearchDataTranslator;

class MockGbSearchDataListView extends GbSearchDataListView
{
    public function getGbSearchDatas(): array
    {
        return parent::getGbSearchDatas();
    }

    public function getTranslator(): GbSearchDataTranslator
    {
        return parent::getTranslator();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function __construct()
    {
        parent::__construct([], 0);
    }
}
