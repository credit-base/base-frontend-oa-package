<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Sdk\ResourceCatalog\Translator\WbjSearchDataTranslator;

class MockWbjSearchDataListView extends WbjSearchDataListView
{
    public function getWbjSearchDatas(): array
    {
        return parent::getWbjSearchDatas();
    }

    public function getTranslator(): WbjSearchDataTranslator
    {
        return parent::getTranslator();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function __construct()
    {
        parent::__construct([], 0);
    }
}
