<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Sdk\ResourceCatalog\Model\WbjSearchData;
use Sdk\ResourceCatalog\Translator\WbjSearchDataTranslator;

class MockWbjSearchDataView extends WbjSearchDataView
{
    public function __construct()
    {
        parent::__construct(new WbjSearchData());
    }

    public function getWbjSearchData()
    {
        return parent::getWbjSearchData();
    }

    public function getTranslator(): WbjSearchDataTranslator
    {
        return parent::getTranslator();
    }
}
