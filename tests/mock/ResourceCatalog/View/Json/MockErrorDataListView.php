<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Sdk\ResourceCatalog\Translator\ErrorDataTranslator;

class MockErrorDataListView extends ErrorDataListView
{
    public function getErrorDataList(): array
    {
        return parent::getErrorDataList();
    }

    public function getTranslator(): ErrorDataTranslator
    {
        return parent::getTranslator();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getResultErrorDataList(array $list):array
    {
        return parent::getResultErrorDataList($list);
    }

    public function getErrorDataListToReason(array $items) : string
    {
        return parent::getErrorDataListToReason($items);
    }
}
