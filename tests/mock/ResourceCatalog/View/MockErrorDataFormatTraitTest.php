<?php


namespace Base\Package\ResourceCatalog\View;

class MockErrorDataFormatTraitTest
{
    use ErrorDataFormatTrait;

    public function getErrorDataFormatNamePublic(array $errorData):array
    {
        return $this->getErrorDataFormatName($errorData);
    }

    public function getFormatItemsDataPublic(array $itemsData):array
    {
        return $this->getFormatItemsData($itemsData);
    }

    public function getErrorDataDetailPublic(array $errorData):array
    {
        return $this->getErrorDataDetail($errorData);
    }

    public function getErrorReasonCnPublic(array $errorReasonData) : string
    {
        return $this->getErrorReasonCn($errorReasonData);
    }

    public function getErrorReasonByIdentifyPublic(
        string $identify,
        string $itemName,
        array $errorReasonData
    ):string {
        return $this->getErrorReasonByIdentify($identify, $itemName, $errorReasonData);
    }

    public function getNewItemsByIdentifyPublic(
        string $identify,
        array $itemsData
    ) : string {
        return $this->getNewItemsByIdentify($identify, $itemsData);
    }

    public function getFormatErrorDataPublic(array $errorData):array
    {
        return $this->getFormatErrorData($errorData);
    }
}
