<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockBjSearchDataDeleteController extends BjSearchDataDeleteController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function deleteAction(int $id): bool
    {
        return parent::deleteAction($id);
    }
}
