<?php
namespace Base\Package\ResourceCatalog\Controller;

use Sdk\ResourceCatalog\Repository\WbjSearchDataRepository;

use Sdk\ResourceCatalog\Model\Attachment;

class MockWbjSearchDataFetchController extends WbjSearchDataFetchController
{
    public function getRepository() : WbjSearchDataRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function isExitCrewId() : bool
    {
        return parent::isExitCrewId();
    }

    public function getAttachment() : Attachment
    {
        return parent::getAttachment();
    }
}
