<?php
namespace Base\Package\ResourceCatalog\Controller;

use Sdk\ResourceCatalog\Repository\ErrorDataRepository;

class MockErrorDataFetchController extends ErrorDataFetchController
{
    public function getRepository() : ErrorDataRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource) : bool
    {
        return parent::checkUserHasPurview($resource);
    }

    public function checkUserHasErrorDataPurview($resource): bool
    {
        return parent::checkUserHasErrorDataPurview($resource);
    }
}
