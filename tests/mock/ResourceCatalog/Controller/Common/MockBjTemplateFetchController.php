<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Sdk\Template\Repository\BjTemplateRepository;

class MockBjTemplateFetchController extends BjTemplateFetchController
{
    public function getRepository() : BjTemplateRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
