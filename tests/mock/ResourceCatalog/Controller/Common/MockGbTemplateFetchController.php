<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Sdk\Template\Repository\GbTemplateRepository;

class MockGbTemplateFetchController extends GbTemplateFetchController
{
    public function getRepository() : GbTemplateRepository
    {
        return parent::getRepository();
    }

    public function filterAction():  bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }
}
