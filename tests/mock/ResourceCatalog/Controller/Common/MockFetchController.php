<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

class MockFetchController
{
    use FetchControllerTrait;

    protected function filterAction()
    {
        return true;
    }

    protected function fetchOneAction(int $id)
    {
        unset($id);
        return false;
    }
}
