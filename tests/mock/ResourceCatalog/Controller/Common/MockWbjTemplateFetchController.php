<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Sdk\Template\Repository\WbjTemplateRepository;
use Sdk\Template\Translator\WbjTemplateTranslator;

class MockWbjTemplateFetchController extends WbjTemplateFetchController
{
    public function getRepository() : WbjTemplateRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
