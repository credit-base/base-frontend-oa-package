<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Sdk\Template\Repository\BaseTemplateRepository;

class MockBaseTemplateFetchController extends BaseTemplateFetchController
{
    public function getRepository() : BaseTemplateRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
