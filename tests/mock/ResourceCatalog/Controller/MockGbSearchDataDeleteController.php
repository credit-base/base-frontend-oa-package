<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockGbSearchDataDeleteController extends GbSearchDataDeleteController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function deleteAction(int $id): bool
    {
        return parent::deleteAction($id);
    }
}
