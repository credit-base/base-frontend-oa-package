<?php
namespace Base\Package\ResourceCatalog\Controller\Task;

use Sdk\ResourceCatalog\Repository\TaskRepository;

class MockGbTaskFetchController extends GbTaskFetchController
{
    public function getRepository() : TaskRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
