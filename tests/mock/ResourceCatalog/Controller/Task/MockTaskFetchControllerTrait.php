<?php
namespace Base\Package\ResourceCatalog\Controller\Task;

use Sdk\ResourceCatalog\Repository\TaskRepository;

class MockTaskFetchControllerTrait
{
    use TaskFetchControllerTrait;

    public function publicGetRepository() : TaskRepository
    {
        return $this->getRepository();
    }

    public function publicFilterList(int $targetCategory) : bool
    {
        return $this->filterList($targetCategory);
    }

    public function publicFilterFormatChange(int $targetCategory)
    {
        return $this->filterFormatChange($targetCategory);
    }
}
