<?php
namespace Base\Package\ResourceCatalog\Controller;

use Sdk\ResourceCatalog\Repository\GbSearchDataRepository;

class MockGbSearchDataFetchController extends GbSearchDataFetchController
{
    public function getRepository() : GbSearchDataRepository
    {
        return parent::getRepository();
    }

    public function filterAction():  bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }
}
