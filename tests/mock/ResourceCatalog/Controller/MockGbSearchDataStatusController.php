<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Framework\Classes\CommandBus;

class MockGbSearchDataStatusController extends GbSearchDataStatusController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function publicConfirm(string $id) : bool
    {
        return parent::confirm($id);
    }
}
