<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Sdk\WebsiteCustomize\WidgetRules\WebsiteCustomizeWidgetRules;

class MockWebsiteCustomizeValidateTrait
{
    use WebsiteCustomizeValidateTrait;

    public function publicGetWebsiteCustomizeWidgetRules() : WebsiteCustomizeWidgetRules
    {
        return $this->getWebsiteCustomizeWidgetRules();
    }

    public function publicValidateAddScenario(
        $category,
        $status,
        $content
    ) : bool {
        return $this->validateAddScenario(
            $category,
            $status,
            $content
        );
    }

    public function publiccheckUserHasWebsiteCustomizePurview() : bool
    {
        return $this->checkUserHasWebsiteCustomizePurview();
    }
}
