<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Marmot\Framework\Classes\CommandBus;

use Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

class MockWebsiteCustomizeOperationController extends WebsiteCustomizeOperationController
{
    public function addAction(): bool
    {
        return parent::addAction();
    }

    public function addView(): bool
    {
        return parent::addView();
    }

    public function getCommandBus():CommandBus
    {
        return parent::getCommandBus();
    }

    public function editView(int $id): bool
    {
        return parent::editView($id);
    }

    public function editAction(int $id): bool
    {
        return parent::editAction($id);
    }

    public function publicCheckUserHasPurview(string $resource = '') : bool
    {
        return parent::checkUserHasPurview($resource);
    }

    public function getFormatContentByDecode(array $content):array
    {
        return parent::getFormatContentByDecode($content);
    }

    public function getItemsListByDecode(array $list):array
    {
        return parent::getItemsListByDecode($list);
    }

    public function getDataStatusByDecode(array $content):array
    {
        return parent::getDataStatusByDecode($content);
    }

    public function getRelatedLinksByDecode(array $content):array
    {
        return parent::getRelatedLinksByDecode($content);
    }

    public function checkUserHasWebsiteCustomizePurview() : bool
    {
        return parent::checkUserHasWebsiteCustomizePurview();
    }
}
