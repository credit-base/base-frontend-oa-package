<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

class MockWebsiteCustomizeFetchController extends WebsiteCustomizeFetchController
{
    public function filterAction(): bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }

    public function getRepository(): WebsiteCustomizeRepository
    {
        return parent::getRepository();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function publicCheckUserHasPurview(string $resource = '') : bool
    {
        return parent::checkUserHasPurview($resource);
    }
}
