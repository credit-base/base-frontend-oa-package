<?php
namespace Base\Package\WebsiteCustomize\View\Json;

use Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct(new WebsiteCustomize());
    }

    public function getWebsiteCustomize()
    {
        return parent::getWebsiteCustomize();
    }

    public function getTranslator(): WebsiteCustomizeTranslator
    {
        return parent::getTranslator();
    }

    public function getFormatContentByEncode(array $content):array
    {
        return parent::getFormatContentByEncode($content);
    }
    public function getItemsListByEncode(array $list):array
    {
        return parent::getItemsListByEncode($list);
    }
    public function getDataStatusByEncode(array $data):array
    {
        return parent::getDataStatusByEncode($data);
    }
    public function getRelatedLinksByEncode(array $relatedLinks):array
    {
        return parent::getRelatedLinksByEncode($relatedLinks);
    }
}
