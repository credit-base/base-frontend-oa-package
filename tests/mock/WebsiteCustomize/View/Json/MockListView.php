<?php
namespace Base\Package\WebsiteCustomize\View\Json;

use Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getWebsiteCustomize(): array
    {
        return parent::getWebsiteCustomize();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): WebsiteCustomizeTranslator
    {
        return parent::getTranslator();
    }
}
