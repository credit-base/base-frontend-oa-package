<?php
namespace Base\Package\CreditPhotography\Controller;

use Marmot\Framework\Classes\CommandBus;

use Sdk\Common\WidgetRules\WidgetRules;

class MockCreditPhotographyApproveController extends CreditPhotographyApproveController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function approveAction(int $id): bool
    {
        return parent::approveAction($id);
    }

    public function rejectAction(int $id): bool
    {
        return parent::rejectAction($id);
    }

    public function getWidgetRules(): WidgetRules
    {
        return parent::getWidgetRules();
    }

    public function validateRejectScenario($rejectReason): bool
    {
        return parent::validateRejectScenario($rejectReason);
    }
}
