<?php
namespace Base\Package\CreditPhotography\View\Json;

use Sdk\CreditPhotography\Model\CreditPhotography;
use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct(new CreditPhotography());
    }

    public function getCreditPhotography()
    {
        return parent::getCreditPhotography();
    }

    public function getTranslator(): CreditPhotographyTranslator
    {
        return parent::getTranslator();
    }
}
