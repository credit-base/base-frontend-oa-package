<?php
namespace Base\Package\CreditPhotography\View\Json;

use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getCreditPhotography(): array
    {
        return parent::getCreditPhotography();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getTranslator(): CreditPhotographyTranslator
    {
        return parent::getTranslator();
    }
}
