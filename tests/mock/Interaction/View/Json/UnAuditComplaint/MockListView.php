<?php
namespace Base\Package\Interaction\View\Json\UnAuditComplaint;

use Sdk\Interaction\Translator\Complaint\UnAuditComplaintTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getUnAuditComplaintTranslator(): UnAuditComplaintTranslator
    {
        return parent::getUnAuditComplaintTranslator();
    }
}
