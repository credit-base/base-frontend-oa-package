<?php
namespace Base\Package\Interaction\View\Json\UnAuditComplaint;

use Sdk\Interaction\Translator\Complaint\UnAuditComplaintTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getUnAuditComplaintTranslator(): UnAuditComplaintTranslator
    {
        return parent::getUnAuditComplaintTranslator();
    }
}
