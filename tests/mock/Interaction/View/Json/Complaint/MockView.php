<?php
namespace Base\Package\Interaction\View\Json\Complaint;

use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getComplaintTranslator(): ComplaintTranslator
    {
        return parent::getComplaintTranslator();
    }
}
