<?php
namespace Base\Package\Interaction\View\Json\Complaint;

use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getComplaintTranslator(): ComplaintTranslator
    {
        return parent::getComplaintTranslator();
    }
}
