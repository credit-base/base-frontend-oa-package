<?php
namespace Base\Package\Interaction\View\Json\Appeal;

use Sdk\Interaction\Translator\Appeal\AppealTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getList(): array
    {
        return parent::getList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getAppealTranslator(): AppealTranslator
    {
        return parent::getAppealTranslator();
    }
}
