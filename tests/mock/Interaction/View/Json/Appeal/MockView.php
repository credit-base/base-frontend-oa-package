<?php
namespace Base\Package\Interaction\View\Json\Appeal;

use Sdk\Interaction\Translator\Appeal\AppealTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getData()
    {
        return parent::getData();
    }

    public function getAppealTranslator(): AppealTranslator
    {
        return parent::getAppealTranslator();
    }

    public function getFormatData(array $arrayData):array
    {
        return parent::getFormatData($arrayData);
    }
}
