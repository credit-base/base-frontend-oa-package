<?php
namespace Base\Package\Interaction\View\Json\UnAuditPraise;

use Sdk\Interaction\Translator\Praise\UnAuditPraiseTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getUnAuditPraiseTranslator(): UnAuditPraiseTranslator
    {
        return parent::getUnAuditPraiseTranslator();
    }
}
