<?php
namespace Base\Package\Interaction\View\Json\UnAuditPraise;

use Sdk\Interaction\Translator\Praise\UnAuditPraiseTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getUnAuditPraiseTranslator(): UnAuditPraiseTranslator
    {
        return parent::getUnAuditPraiseTranslator();
    }
}
