<?php
namespace Base\Package\Interaction\View\Json\UnAuditAppeal;

use Sdk\Interaction\Translator\Appeal\UnAuditAppealTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getUnAuditAppealTranslator(): UnAuditAppealTranslator
    {
        return parent::getUnAuditAppealTranslator();
    }
}
