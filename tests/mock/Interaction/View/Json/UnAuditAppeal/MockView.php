<?php
namespace Base\Package\Interaction\View\Json\UnAuditAppeal;

use Sdk\Interaction\Translator\Appeal\UnAuditAppealTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getUnAuditAppealTranslator(): UnAuditAppealTranslator
    {
        return parent::getUnAuditAppealTranslator();
    }
}
