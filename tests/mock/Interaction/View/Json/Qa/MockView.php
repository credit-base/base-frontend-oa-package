<?php
namespace Base\Package\Interaction\View\Json\Qa;

use Sdk\Interaction\Translator\Qa\QaTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getQaTranslator(): QaTranslator
    {
        return parent::getQaTranslator();
    }
}
