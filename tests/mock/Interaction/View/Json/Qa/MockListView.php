<?php
namespace Base\Package\Interaction\View\Json\Qa;

use Sdk\Interaction\Translator\Qa\QaTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getQaTranslator(): QaTranslator
    {
        return parent::getQaTranslator();
    }
}
