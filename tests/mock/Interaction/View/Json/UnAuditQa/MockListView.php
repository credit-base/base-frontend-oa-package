<?php
namespace Base\Package\Interaction\View\Json\UnAuditQa;

use Sdk\Interaction\Translator\Qa\UnAuditQaTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getUnAuditQaTranslator(): UnAuditQaTranslator
    {
        return parent::getUnAuditQaTranslator();
    }
}
