<?php
namespace Base\Package\Interaction\View\Json\UnAuditQa;

use Sdk\Interaction\Translator\Qa\UnAuditQaTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getUnAuditQaTranslator(): UnAuditQaTranslator
    {
        return parent::getUnAuditQaTranslator();
    }
}
