<?php
namespace Base\Package\Interaction\View\Json\Feedback;

use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getFeedbackTranslator(): FeedbackTranslator
    {
        return parent::getFeedbackTranslator();
    }
}
