<?php
namespace Base\Package\Interaction\View\Json\Feedback;

use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getFeedbackTranslator(): FeedbackTranslator
    {
        return parent::getFeedbackTranslator();
    }
}
