<?php
namespace Base\Package\Interaction\View\Json\Praise;

use Sdk\Interaction\Translator\Praise\PraiseTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getPraiseTranslator(): PraiseTranslator
    {
        return parent::getPraiseTranslator();
    }
}
