<?php
namespace Base\Package\Interaction\View\Json\Praise;

use Sdk\Interaction\Translator\Praise\PraiseTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getPraiseTranslator(): PraiseTranslator
    {
        return parent::getPraiseTranslator();
    }
}
