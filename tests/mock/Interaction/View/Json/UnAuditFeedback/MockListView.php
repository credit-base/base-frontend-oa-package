<?php
namespace Base\Package\Interaction\View\Json\UnAuditFeedback;

use Sdk\Interaction\Translator\Feedback\UnAuditFeedbackTranslator;

class MockListView extends ListView
{
    public function __construct()
    {
        parent::__construct([], 0);
    }

    public function getUnAuditFeedbackTranslator(): UnAuditFeedbackTranslator
    {
        return parent::getUnAuditFeedbackTranslator();
    }
}
