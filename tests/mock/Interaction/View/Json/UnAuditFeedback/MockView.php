<?php
namespace Base\Package\Interaction\View\Json\UnAuditFeedback;

use Sdk\Interaction\Translator\Feedback\UnAuditFeedbackTranslator;

class MockView extends View
{
    public function __construct()
    {
        parent::__construct([]);
    }

    public function getUnAuditFeedbackTranslator(): UnAuditFeedbackTranslator
    {
        return parent::getUnAuditFeedbackTranslator();
    }
}
