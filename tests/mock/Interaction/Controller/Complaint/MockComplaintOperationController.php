<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Marmot\Framework\Classes\CommandBus;

class MockComplaintOperationController extends ComplaintOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}
