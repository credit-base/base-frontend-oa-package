<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Sdk\Interaction\Repository\UnAuditComplaintRepository;

class MockUnAuditComplaintFetchController extends UnAuditComplaintFetchController
{
    public function getRepository() : UnAuditComplaintRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource) : bool
    {
        return parent::checkUserHasPurview($resource);
    }
}
