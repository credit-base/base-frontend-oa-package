<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditComplaintOperationController extends UnAuditComplaintOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function resubmitAction(int $id) : bool
    {
        return parent::resubmitAction($id);
    }
}
