<?php
namespace Base\Package\Interaction\Controller;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\Interaction\WidgetRules\InteractionWidgetRules;

class MockInteractionValidateTrait
{
    use InteractionValidateTrait;

    public function publicGetInteractionWidgetRules() : InteractionWidgetRules
    {
        return $this->getInteractionWidgetRules();
    }

    public function publicGetWidgetRules() : WidgetRules
    {
        return $this->getWidgetRules();
    }

    public function publicValidateCommonScenario(
        $content,
        $images,
        $admissibility
    ) : bool {
        return $this->validateCommonScenario(
            $content,
            $images,
            $admissibility
        );
    }

    public function publicValidateRejectScenario($rejectReason) : bool
    {
        return $this->validateRejectScenario($rejectReason);
    }
}
