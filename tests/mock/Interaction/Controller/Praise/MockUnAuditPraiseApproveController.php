<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditPraiseApproveController extends UnAuditPraiseApproveController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function approveAction(int $id): bool
    {
        return parent::approveAction($id);
    }

    public function rejectAction(int $id): bool
    {
        return parent::rejectAction($id);
    }
}
