<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditPraiseOperationController extends UnAuditPraiseOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function resubmitAction(int $id) : bool
    {
        return parent::resubmitAction($id);
    }
}
