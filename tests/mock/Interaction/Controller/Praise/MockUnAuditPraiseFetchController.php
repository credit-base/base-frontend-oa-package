<?php
namespace Base\Package\Interaction\Controller\Praise;

use Sdk\Interaction\Repository\UnAuditPraiseRepository;

class MockUnAuditPraiseFetchController extends UnAuditPraiseFetchController
{
    public function getRepository() : UnAuditPraiseRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource) : bool
    {
        return parent::checkUserHasPurview($resource);
    }
}
