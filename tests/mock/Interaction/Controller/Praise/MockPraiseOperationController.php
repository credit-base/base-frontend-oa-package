<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Framework\Classes\CommandBus;

class MockPraiseOperationController extends PraiseOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}
