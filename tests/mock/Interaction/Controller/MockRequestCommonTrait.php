<?php
namespace Base\Package\Interaction\Controller;

use Base\Sdk\Purview\Model\PurviewCategoryFactory;

class MockRequestCommonTrait
{
    use RequestCommonTrait;

    public function publicGetReplyRequestCommonData()
    {
        return $this->getReplyRequestCommonData();
    }

    public function publicFilterFormatChange()
    {
        return $this->filterFormatChange();
    }

    public function publicGetInteractionPurview($resource) : array
    {
        return $this->getInteractionPurview($resource);
    }

    public function publicGetPurviewCategory($resource) : int
    {
        return $this->getPurviewCategory($resource);
    }
}
