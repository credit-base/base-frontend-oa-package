<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Marmot\Framework\Classes\CommandBus;

class MockAppealOperationController extends AppealOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}
