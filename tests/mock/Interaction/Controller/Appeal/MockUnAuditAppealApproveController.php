<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditAppealApproveController extends UnAuditAppealApproveController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function approveAction(int $id): bool
    {
        return parent::approveAction($id);
    }

    public function rejectAction(int $id): bool
    {
        return parent::rejectAction($id);
    }
}
