<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditAppealOperationController extends UnAuditAppealOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function resubmitAction(int $id) : bool
    {
        return parent::resubmitAction($id);
    }
}
