<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Sdk\Interaction\Repository\UnAuditAppealRepository;

class MockUnAuditAppealFetchController extends UnAuditAppealFetchController
{
    public function getRepository() : UnAuditAppealRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource) : bool
    {
        return parent::checkUserHasPurview($resource);
    }
}
