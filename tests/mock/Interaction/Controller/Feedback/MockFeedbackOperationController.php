<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Marmot\Framework\Classes\CommandBus;

class MockFeedbackOperationController extends FeedbackOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}
