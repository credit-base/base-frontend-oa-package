<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Sdk\Interaction\Repository\FeedbackRepository;

class MockFeedbackFetchController extends FeedbackFetchController
{
    public function getRepository() : FeedbackRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource) : bool
    {
        return parent::checkUserHasPurview($resource);
    }

    public function publicCheckUserHasInteractionPurview($resource) : bool
    {
        return $this->checkUserHasInteractionPurview($resource);
    }
}
