<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditFeedbackOperationController extends UnAuditFeedbackOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function resubmitAction(int $id) : bool
    {
        return parent::resubmitAction($id);
    }
}
