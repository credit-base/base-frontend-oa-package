<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Sdk\Interaction\Repository\UnAuditFeedbackRepository;

class MockUnAuditFeedbackFetchController extends UnAuditFeedbackFetchController
{
    public function getRepository() : UnAuditFeedbackRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource) : bool
    {
        return parent::checkUserHasPurview($resource);
    }
}
