<?php
namespace Base\Package\Interaction\Controller\Qa;

use Sdk\Interaction\Repository\QaRepository;

class MockQaFetchController extends QaFetchController
{
    public function getRepository() : QaRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource) : bool
    {
        return parent::checkUserHasPurview($resource);
    }

    public function publicCheckUserHasInteractionPurview($resource) : bool
    {
        return $this->checkUserHasInteractionPurview($resource);
    }
}
