<?php
namespace Base\Package\Interaction\Controller\Qa;

use Marmot\Framework\Classes\CommandBus;

class MockUnAuditQaOperationController extends UnAuditQaOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function resubmitAction(int $id) : bool
    {
        return parent::resubmitAction($id);
    }
}
