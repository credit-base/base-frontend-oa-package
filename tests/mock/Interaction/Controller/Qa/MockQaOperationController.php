<?php
namespace Base\Package\Interaction\Controller\Qa;

use Marmot\Framework\Classes\CommandBus;

class MockQaOperationController extends QaOperationController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}
