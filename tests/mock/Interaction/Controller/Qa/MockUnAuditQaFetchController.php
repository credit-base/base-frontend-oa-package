<?php
namespace Base\Package\Interaction\Controller\Qa;

use Sdk\Interaction\Repository\UnAuditQaRepository;

class MockUnAuditQaFetchController extends UnAuditQaFetchController
{
    public function getRepository() : UnAuditQaRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function checkUserHasPurview($resource) : bool
    {
        return parent::checkUserHasPurview($resource);
    }
}
