<?php
namespace Base\Package\Template\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Basecode\Classes\Server;
use Marmot\Framework\Classes\Request;

use PhpOffice\PhpSpreadsheet\Comment;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\RichText\Run;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;

use Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Model\NullWbjTemplate;
use Sdk\Template\Translator\WbjTemplateTranslator;
use Sdk\Template\Repository\WbjTemplateRepository;

use Base\Package\Template\View\Json\WbjTemplateListView;
use Base\Package\Template\View\Json\WbjTemplateDetailView;

/**
 * @SuppressWarnings(PHPMD)
 */
class WbjTemplateFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockWbjTemplateFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new WbjTemplateFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetSpreadsheet()
    {
        $this->assertInstanceof(
            'PhpOffice\PhpSpreadsheet\Spreadsheet',
            $this->stub->getSpreadsheet()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Template\Repository\WbjTemplateRepository',
            $this->stub->getRepository()
        );
    }

    public function testGetWbjTemplateTranslator()
    {
        $this->assertInstanceof(
            'Sdk\Template\Translator\WbjTemplateTranslator',
            $this->stub->getWbjTemplateTranslator()
        );
    }

    public function testGetWriter()
    {
        $this->assertInstanceOf(
            'PhpOffice\PhpSpreadsheet\Writer\Xlsx',
            $this->stub->getWriter(new Spreadsheet())
        );
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockWbjTemplateFetchController::class)
            ->setMethods(
                [
                    'getPageAndSize',
                    'filterFormatChange',
                    'getRequest',
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $request = $this->prophesize(Request::class);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $filter = array();
        $sort = ['-updateTime'];
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $page = 2;
        $size = 30;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $list = array(1,2);
        $count = 2;
        $repository = $this->prophesize(WbjTemplateRepository::class);
        $repository->scenario(Argument::exact(WbjTemplateRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new WbjTemplateListView($list, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->stub = $this->getMockBuilder(MockWbjTemplateFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new WbjTemplate($id);

        $repository = $this->prophesize(WbjTemplateRepository::class);
        $repository->scenario(Argument::exact(WbjTemplateRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new WbjTemplateDetailView($data));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        //初始化
        $sort = '-updateTime';
        $name = 'name3';
        $identify = 'identify3';
        $sourceUnitId = 2;
        $sourceUnit = marmot_encode($sourceUnitId);

        $request = $this->prophesize(Request::class);
        $request->get(Argument::exact('sort'), Argument::exact('-updateTime'))
            ->shouldBeCalledTimes(1)->willReturn($sort);
        $request->get(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        $request->get(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($name);
        $request->get(Argument::exact('sourceUnit'), Argument::exact('MA'))
            ->shouldBeCalledTimes(1)->willReturn($sourceUnit);

        $this->stub->expects($this->exactly(4))
        ->method('getRequest')->willReturn($request->reveal());

        $expected = [
            [
                'identify'=>$identify,
                'name'=>$name,
                'sourceUnit'=>marmot_decode($sourceUnit)
            ],
            [$sort]
        ];

        $result = $this->stub->filterFormatChange();
        $this->assertEquals($expected, $result);
    }

    //getWbjTemplate
    public function testGetWbjTemplate()
    {
        $controller = $this->getMockBuilder(
            MockWbjTemplateFetchController::class
        )->setMethods(['getRepository'])->getMock();

        $templateId = 1;
        $template = new WbjTemplate($templateId);

        $repository = $this->prophesize(WbjTemplateRepository::class);
        $repository->scenario(
            Argument::exact(WbjTemplateRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($templateId))->shouldBeCalledTimes(1)->willReturn($template);
    
        $controller->expects($this->exactly(1))->method(
            'getRepository'
        )->willReturn($repository->reveal());

        $result = $controller->getWbjTemplate($templateId);

        $this->assertEquals($result, $template);
    }

    //download-dataEmpty
    public function testDownloadFail()
    {
        $controller = $this->getMockBuilder(
            MockWbjTemplateFetchController::class
        )->setMethods(['getWbjTemplateArrayData', 'displayError'])->getMock();

        $id = 1;
        $data = array();

        $controller->expects($this->exactly(1))->method('getWbjTemplateArrayData')->willReturn($data);
        $controller->expects($this->exactly(1))->method('displayError');

        $result = $controller->download($id);
        $this->assertFalse($result);
    }

    public function testDownload()
    {
        $controller = $this->getMockBuilder(MockWbjTemplateFetchController::class)->setMethods([
            'getWbjTemplateArrayData',
            'getTemplateSpreadsheet',
            'getItemCellValue',
            'getResultWriter',
            'displayError'
        ])->getMock();

        $id = 1;
        $data = array(
            'items' => array('items'),
            'identify' => 'identify',
            'category' => 1
        );
        $name = 'identify_1_1';
        $spreadsheet = new Spreadsheet();

        $controller->expects($this->exactly(1))->method('getWbjTemplateArrayData')->willReturn($data);
        $controller->expects($this->exactly(1))->method('getTemplateSpreadsheet')->willReturn($spreadsheet);
        $controller->expects($this->exactly(1))->method('getItemCellValue')->willReturn($spreadsheet);
        $controller->expects($this->exactly(1))->method('getResultWriter')->with($name, $spreadsheet);
        $controller->expects($this->exactly(1))->method('displayError');

        $result = $controller->download($id);
        $this->assertFalse($result);
    }

    //getWbjTemplateArrayData
    public function testGetWbjTemplateArrayDataFail()
    {
        $controller = $this->getMockBuilder(
            MockWbjTemplateFetchController::class
        )->setMethods(['getWbjTemplate'])->getMock();

        $id = 1;
        $template = NullWbjTemplate::getInstance();

        $controller->expects($this->exactly(1))->method('getWbjTemplate')->willReturn($template);

        $result = $controller->getWbjTemplateArrayData($id);

        $this->assertEquals($result, array());
    }

    public function testGetWbjTemplateArrayData()
    {
        $controller = $this->getMockBuilder(
            MockWbjTemplateFetchController::class
        )->setMethods(['getWbjTemplate', 'getWbjTemplateTranslator'])->getMock();

        $id = 1;
        $template = new WbjTemplate($id);
        $wbjTemplateData = array('wbjTemplateData');

        $controller->expects($this->exactly(1))->method('getWbjTemplate')->willReturn($template);

        $translator = $this->prophesize(WbjTemplateTranslator::class);
        $translator->objectToArray(Argument::exact($template))->shouldBeCalledTimes(1)->willReturn($wbjTemplateData);
    
        $controller->expects($this->exactly(1))->method(
            'getWbjTemplateTranslator'
        )->willReturn($translator->reveal());

        $result = $controller->getWbjTemplateArrayData($id);

        $this->assertEquals($result, $wbjTemplateData);
    }

    //getTemplateSpreadsheet
    public function testGetTemplateSpreadsheet()
    {
        $this->assertInstanceof(
            'PhpOffice\PhpSpreadsheet\Spreadsheet',
            $this->stub->getTemplateSpreadsheet()
        );
    }

    //getItemCellValue
    public function testGetItemCellValueFail()
    {
        $item = array(array('name' => 'name'));
        $spreadsheet = new Spreadsheet();

        $result = $this->stub->getItemCellValue($item, $spreadsheet);

        $this->assertFalse($result);
    }

    public function testGetItemCellValue()
    {
        $item = array(array('name' => 'name', 'remarks' => 'remarks'));

        $spreadsheet = $this->prophesize(Spreadsheet::class);
        $sheet = $this->prophesize(Worksheet::class);
        $comment = $this->prophesize(Comment::class);
        $richText = $this->prophesize(RichText::class);
        $run = new Run();

        $richText->createTextRun('remarks')->shouldBeCalledTimes(1)->willReturn($run);
        $comment->getText()->shouldBeCalledTimes(1)->willReturn($richText->reveal());
        $sheet->setCellValue(
            WbjTemplateFetchController::HEAD_LETTER[0],
            'name'
        )->shouldBeCalledTimes(1);
        $sheet->getComment(
            WbjTemplateFetchController::HEAD_LETTER[0]
        )->shouldBeCalledTimes(1)->willReturn($comment->reveal());

        $spreadsheet->getActiveSheet()->shouldBeCalledTimes(1)->willReturn($sheet->reveal());

        $this->stub->getItemCellValue($item, $spreadsheet->reveal());
    }
}
