<?php
namespace Base\Package\Template\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Template\Model\QzjTemplate;
use Sdk\Template\Repository\QzjTemplateRepository;

use Base\Package\Template\View\Json\QzjTemplateDetailView;
use Base\Package\Template\View\Json\QzjTemplateListView;
use Base\Sdk\UserGroup\Model\UserGroup;
use Sdk\Crew\Model\Crew;

class QzjTemplateFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockQzjTemplateFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new QzjTemplateFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Template\Repository\QzjTemplateRepository',
            $this->stub->getRepository()
        );
    }

    public function testFilterActionSuccess()
    {
        $this->qzjTemplateFetchController = $this->getMockBuilder(MockQzjTemplateFetchController::class)
            ->setMethods(
                [
                    'getPageAndSize',
                    'getResponse',
                    'getRequest',
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $size = 10;
        $page = 1;

        $request = $this->prophesize(Request::class);

        $this->qzjTemplateFetchController->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->qzjTemplateFetchController->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $filter = array();
        $sort = ['-updateTime'];
        $this->qzjTemplateFetchController->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $count = 10;
        $list = array(1,2);
        
        $repository = $this->prophesize(QzjTemplateRepository::class);
        $repository->scenario(Argument::exact(QzjTemplateRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->qzjTemplateFetchController->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->qzjTemplateFetchController->expects($this->exactly(1))
            ->method('render')
            ->with(new QzjTemplateListView($list, $count));

        $result = $this->qzjTemplateFetchController->filterAction();
        $this->assertTrue($result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->stub = $this->getMockBuilder(MockQzjTemplateFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new QzjTemplate($id);

        $repository = $this->prophesize(QzjTemplateRepository::class);
        $repository->scenario(Argument::exact(QzjTemplateRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new QzjTemplateDetailView($data));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        //初始化
        $sort = '-updateTime';
        $name1 = 'identify';

        $crew = new Crew(1);
        $crew->setUserGroup(new UserGroup(1));
        Core::$container->set('crew', $crew);
        $userGroupId = Core::$container->get('crew')->getUserGroup()->getId();
        
        $request = $this->prophesize(Request::class);
        $request->get(Argument::exact('sort'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn('');
        $request->get(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($name1);

        $this->stub->expects($this->exactly(2))
        ->method('getRequest')->willReturn($request->reveal());

        if (empty($sort)) {
            $sort = '-updateTime';
        }

        $expected = [
            [
                'sourceUnit'=>$userGroupId,
                'name'=>$name1
            ],
            [$sort]
        ];

        $result = $this->stub->filterFormatChange();
        $this->assertEquals($expected, $result);
    }
}
