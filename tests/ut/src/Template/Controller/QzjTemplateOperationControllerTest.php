<?php

namespace Base\Package\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Template\Command\QzjTemplate\AddQzjTemplateCommand;
use Sdk\Template\Command\QzjTemplate\EditQzjTemplateCommand;
use Sdk\Template\WidgetRule\TemplateWidgetRule;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class QzjTemplateOperationControllerTest extends TestCase
{
    private $qzjTemplateStub;

    public function setUp()
    {
        $this->qzjTemplateStub = $this->getMockBuilder(MockQzjTemplateOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess'
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->qzjTemplateStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new QzjTemplateOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $controller = new QzjTemplateOperationController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IOperateAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->qzjTemplateStub->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->qzjTemplateStub->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->qzjTemplateStub->addAction();
        $this->assertTrue($result);
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->qzjTemplateStub->addAction();
        $this->assertFalse($result);
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     * @SuppressWarnings(PHPMD)
     */
    private function initialAdd(bool $result)
    {
        //初始化
        $this->qzjTemplateStub = $this->getMockBuilder(MockQzjTemplateOperationController::class)
            ->setMethods(
                [
                    'displaySuccess',
                    'getRequest',
                    'validateCommonScenario',
                    'displayError',
                    'getCommandBus',
                    'validateQzjCommonScenario'
                ]
            )->getMock();

     
        $subjectCategory = array();
        $dimensionId = 3;
        $dimension = marmot_encode($dimensionId);
        $gbTemplateId = 3;
        $gbTemplate = marmot_encode($gbTemplateId);
        $name = 'name3';
        $identify = 'identify3';
        $description = 'description3';
        $exchangeFrequencyId = 3;
        $exchangeFrequency = marmot_encode($exchangeFrequencyId);
        $infoClassifyId = 3;
        $infoClassify = marmot_encode($infoClassifyId);
        $category = 21;
        $categoryEncode = marmot_encode($category);
        $sourceUnitId = 3;
        $sourceUnit = marmot_encode($sourceUnitId);
        $items = array();
        $infoCategoryId = 3;
        $infoCategory = marmot_encode($infoCategoryId);

        //预言
        $request = $this->prophesize(Request::class);
        
        $request->post(Argument::exact('description'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($description);
        $request->post(Argument::exact('dimension'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($dimension);
        $request->post(Argument::exact('gbTemplate'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($gbTemplate);
        $request->post(Argument::exact('items'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($items);
        $request->post(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        $request->post(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($name);
        $request->post(Argument::exact('subjectCategory'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($subjectCategory);
        $request->post(Argument::exact('category'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($categoryEncode);
        $request->post(Argument::exact('sourceUnit'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($sourceUnit);
        $request->post(Argument::exact('exchangeFrequency'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($exchangeFrequency);
        $request->post(Argument::exact('infoClassify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoClassify);
        $request->post(Argument::exact('infoCategory'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoCategory);
        
        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddQzjTemplateCommand(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $sourceUnitId,
            $dimensionId,
            $exchangeFrequencyId,
            $infoClassifyId,
            $infoCategoryId,
            $category
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        //绑定
        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('getRequest')->willReturn($request->reveal());

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $name,
                $identify,
                $subjectCategory,
                $dimensionId,
                $exchangeFrequencyId,
                $infoClassifyId,
                $infoCategoryId,
                $description,
                $items
            )->willReturn(true);

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('validateQzjCommonScenario')
            ->with(
                $infoCategoryId,
                $category
            )->willReturn(true);
    }

    public function testEditView()
    {
        $id = 1;

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->qzjTemplateStub->editView($id);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testEditActionSuccess()
    {
        $id = 4;

        $this->initialEdit($id, true);

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->qzjTemplateStub->editAction($id);
        
        $this->assertTrue($result);
    }

    public function testEditActionFailure()
    {
        $id = 3;

        $this->initialEdit($id, false);

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->qzjTemplateStub->editAction($id);
        $this->assertFalse($result);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function initialEdit(int $id, bool $result)
    {
        $this->qzjTemplateStub = $this->getMockBuilder(MockQzjTemplateOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'validateCommonScenario',
                    'validateQzjCommonScenario'
                ]
            )->getMock();

            $name = 'name';
            $identify = 'identify';
            $subjectCategory = array();
            $description = 'description';
            $exchangeFrequencyId = 1;
            $exchangeFrequency = marmot_encode($exchangeFrequencyId);
            $dimensionId = 1;
            $dimension = marmot_encode($dimensionId);
            $infoCategoryId = 1;
            $infoCategory = marmot_encode($infoCategoryId);
            $infoClassifyId = 1;
            $infoClassify = marmot_encode($infoClassifyId);
            $gbTemplateId = 1;
            $gbTemplate = marmot_encode($gbTemplateId);
            $items = array();
            $category = 21;
            $categoryEncode = marmot_encode($category);
            $sourceUnitId = 1;
            $sourceUnit = marmot_encode($sourceUnitId);

            //预言
            $request = $this->prophesize(Request::class);
            
            $request->post(Argument::exact('identify'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($identify);
            $request->post(Argument::exact('subjectCategory'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)->willReturn($subjectCategory);
            $request->post(Argument::exact('name'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($name);
            $request->post(Argument::exact('dimension'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($dimension);
            $request->post(Argument::exact('exchangeFrequency'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($exchangeFrequency);
            $request->post(Argument::exact('description'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($description);
            
            $request->post(Argument::exact('sourceUnit'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($sourceUnit);
            $request->post(Argument::exact('gbTemplate'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($gbTemplate);
            $request->post(Argument::exact('items'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)->willReturn($items);
            $request->post(Argument::exact('infoClassify'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($infoClassify);
            $request->post(Argument::exact('infoCategory'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($infoCategory);
            $request->post(Argument::exact('category'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($categoryEncode);

        $this->qzjTemplateStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $name,
                $identify,
                $subjectCategory,
                $dimensionId,
                $exchangeFrequencyId,
                $infoClassifyId,
                $infoCategoryId,
                $description,
                $items
            )->willReturn(true);

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('validateQzjCommonScenario')
            ->with(
                $infoCategoryId,
                $category
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditQzjTemplateCommand(
                    $name,
                    $identify,
                    $description,
                    $subjectCategory,
                    $items,
                    $sourceUnitId,
                    $dimensionId,
                    $exchangeFrequencyId,
                    $infoClassifyId,
                    $infoCategoryId,
                    $category,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->qzjTemplateStub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testValidateQzjCommonScenario()
    {
        //初始化
        $this->qzjTemplateStub = $this->getMockBuilder(MockQzjTemplateOperationController::class)
            ->setMethods(
                [
                    'getTemplateWidgetRule'
                ]
            )->getMock();

        $category = 21;
        $infoCategory = 1;

        //预言
        $templateWidgetRule = $this->prophesize(TemplateWidgetRule::class);
        $templateWidgetRule->category($category)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->infoCategory($infoCategory)->shouldBeCalledTimes(1)->willReturn(true);
        //绑定
        $this->qzjTemplateStub->expects($this->any())
            ->method('getTemplateWidgetRule')
            ->willReturn($templateWidgetRule->reveal());

        //验证
        $this->qzjTemplateStub->validateQzjCommonScenario(
            $infoCategory,
            $category
        );
    }
}
