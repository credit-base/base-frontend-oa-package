<?php

namespace Base\Package\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Template\Command\BaseTemplate\AddBaseTemplateCommand;
use Sdk\Template\Command\BaseTemplate\EditBaseTemplateCommand;
use Sdk\Template\WidgetRule\TemplateWidgetRule;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class BaseTemplateOperationControllerTest extends TestCase
{
    private $controller;
    private $request;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(MockBaseTemplateOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess'
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new BaseTemplateOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $controller = new BaseTemplateOperationController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IOperateAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->controller->getCommandBus()
        );
    }

    public function testEditView()
    {
        $id = 1;

        $this->controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->controller->editView($id);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAddView()
    {
        $this->controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->controller->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAddActionFailure()
    {
        $this->controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->controller->addAction();
        $this->assertFalse($result);
    }

    public function testEditActionSuccess()
    {
        $id = 1;

        $this->initialEdit($id, true);

        $this->controller->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->controller->editAction($id);

        $this->assertTrue($result);
    }

    public function testEditActionFailure()
    {
        $id = 1;

        $this->initialEdit($id, false);

        $this->controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->controller->editAction($id);
        $this->assertFalse($result);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function initialEdit(int $id, bool $result)
    {
        $this->controller = $this->getMockBuilder(MockBaseTemplateOperationController::class)
            ->setMethods(
                [
                    'displaySuccess',
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $name = '名称';
        $identify = '标识';
        $description = '描述';
        $subjectCategory = [];
        $dimensionId = 1;
        $dimension = marmot_encode($dimensionId);
        $exchangeFrequencyId = 3;
        $exchangeFrequency = marmot_encode($exchangeFrequencyId);
        $infoClassifyId = 3;
        $infoClassify = marmot_encode($infoClassifyId);
        $infoCategoryId = 3;
        $infoCategory = marmot_encode($infoCategoryId);
        $sourceUnitId = 3;
        $sourceUnit = marmot_encode($sourceUnitId);
        $gbTemplateId = 3;
        $gbTemplate = marmot_encode($gbTemplateId);
        $items = array();
        $category = 21;
        $categoryEncode = marmot_encode($category);


        //预言
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('sourceUnit'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($sourceUnit);
        $request->post(Argument::exact('gbTemplate'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($gbTemplate);
        $request->post(Argument::exact('dimension'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($dimension);
        $request->post(Argument::exact('exchangeFrequency'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($exchangeFrequency);
        $request->post(Argument::exact('infoCategory'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoCategory);
        $request->post(Argument::exact('items'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($items);
        $request->post(Argument::exact('infoClassify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoClassify);
        $request->post(Argument::exact('subjectCategory'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($subjectCategory);
        $request->post(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($name);
        $request->post(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        $request->post(Argument::exact('description'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($description);
        $request->post(Argument::exact('category'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($categoryEncode);

        $this->controller->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $name,
                $identify,
                $subjectCategory,
                $dimensionId,
                $exchangeFrequencyId,
                $infoClassifyId,
                $infoCategoryId,
                $description,
                $items
            )->willReturn(true);

        $this->controller->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $commandBus = $this->prophesize(CommandBus::class);

        $commandBus->send(
            Argument::exact(
                new EditBaseTemplateCommand(
                    $name,
                    $identify,
                    $description,
                    $subjectCategory,
                    $items,
                    $dimensionId,
                    $exchangeFrequencyId,
                    $infoClassifyId,
                    $infoCategoryId,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testValidateCommonScenario()
    {
        //初始化
        $this->controller = $this->getMockBuilder(MockBaseTemplateOperationController::class)
            ->setMethods(['getTemplateWidgetRule'])->getMock();

        $name = 'name';
        $identify = 'identify';
        $description = 'description';
        $subjectCategory = array();
        $dimension = 2;
        $exchangeFrequency = 2;
        $infoClassify = 2;
        $infoCategory = 2;
        $items = array();

        $templateWidgetRule = $this->prophesize(TemplateWidgetRule::class);
        $templateWidgetRule->name($name)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->subjectCategory($subjectCategory)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->identify($identify)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->dimension($dimension)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->items($subjectCategory, $items)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->description($description)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->formatNumeric($exchangeFrequency)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->formatNumeric($infoClassify)->shouldBeCalledTimes(2)->willReturn(true);
        $templateWidgetRule->formatNumeric($infoCategory)->shouldBeCalledTimes(3)->willReturn(true);

        $this->controller->expects($this->any())
            ->method('getTemplateWidgetRule')
            ->willReturn($templateWidgetRule->reveal());

        $this->controller->validateCommonScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items
        );
    }
}
