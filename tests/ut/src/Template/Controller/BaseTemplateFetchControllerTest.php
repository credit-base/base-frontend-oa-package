<?php
namespace Base\Package\Template\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Template\Model\BaseTemplate;
use Sdk\Template\Repository\BaseTemplateRepository;

use Base\Package\Template\View\Json\BaseTemplateDetailView;
use Base\Package\Template\View\Json\BaseTemplateListView;

class BaseTemplateFetchControllerTest extends TestCase
{
    private $controller;
    private $request;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(MockBaseTemplateFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->request);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new BaseTemplateFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Template\Repository\BaseTemplateRepository',
            $this->controller->getRepository()
        );
    }

    public function testFilterActionSuccess()
    {
        $this->controller = $this->getMockBuilder(MockBaseTemplateFetchController::class)
            ->setMethods(
                [
                    'getPageAndSize',
                    'getResponse',
                    'getRequest',
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $size = 10;
        $page = 1;

        $request = $this->prophesize(Request::class);

        $this->controller->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->controller->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $filter = array();
        $sort = ['-updateTime'];
        $this->controller->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $count = 10;
        $list = array(1,2);
        
        $repository = $this->prophesize(BaseTemplateRepository::class);
        $repository->scenario(Argument::exact(BaseTemplateRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->controller->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->controller->expects($this->exactly(1))
            ->method('render')
            ->with(new BaseTemplateListView($list, $count));

        $result = $this->controller->filterAction();
        $this->assertTrue($result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->controller->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->controller = $this->getMockBuilder(MockBaseTemplateFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new BaseTemplate($id);

        $repository = $this->prophesize(BaseTemplateRepository::class);
        $repository->scenario(Argument::exact(BaseTemplateRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->controller->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->controller->expects($this->exactly(1))
            ->method('render')
            ->with(new BaseTemplateDetailView($data));

        $result = $this->controller->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->controller->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        $sort = '-updateTime';
        $identify = 'identify';
        $templateName = 'name';

        $request = $this->prophesize(Request::class);
        $request->get(Argument::exact('sort'), Argument::exact('-updateTime'))
            ->shouldBeCalledTimes(1)->willReturn($sort);
        $request->get(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        $request->get(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($templateName);

        $this->controller->expects($this->exactly(3))
        ->method('getRequest')->willReturn($request->reveal());

        $expected = [
            ['identify'=>$identify, 'name'=>$templateName],
            [$sort]
        ];

        $result = $this->controller->filterFormatChange();
        $this->assertEquals($expected, $result);
    }
}
