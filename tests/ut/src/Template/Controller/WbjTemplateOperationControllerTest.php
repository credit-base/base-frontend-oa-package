<?php

namespace Base\Package\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Template\Command\WbjTemplate\AddWbjTemplateCommand;
use Sdk\Template\Command\WbjTemplate\EditWbjTemplateCommand;
use Sdk\Template\WidgetRule\TemplateWidgetRule;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class WbjTemplateOperationControllerTest extends TestCase
{
    private $wbjTemplateStub;

    public function setUp()
    {
        $this->wbjTemplateStub = $this->getMockBuilder(MockWbjTemplateOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess'
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->wbjTemplateStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new WbjTemplateOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $controller = new WbjTemplateOperationController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IOperateAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->wbjTemplateStub->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->wbjTemplateStub->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->wbjTemplateStub->addAction();
        $this->assertTrue($result);
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->wbjTemplateStub->addAction();
        $this->assertFalse($result);
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        //初始化
        $this->wbjTemplateStub = $this->getMockBuilder(MockWbjTemplateOperationController::class)
            ->setMethods(
                [
                    'displaySuccess',
                    'getRequest',
                    'validateCommonScenario',
                    'displayError',
                    'getCommandBus'
                ]
            )->getMock();

        $name = 'name3';
        $identify = 'identify3';
        $description = 'description3';
        $subjectCategory = array();
        $dimensionId = 3;
        $dimension = marmot_encode($dimensionId);
        $gbTemplateId = 3;
        $gbTemplate = marmot_encode($gbTemplateId);
        $exchangeFrequencyId = 3;
        $exchangeFrequency = marmot_encode($exchangeFrequencyId);
        $infoClassifyId = 3;
        $infoClassify = marmot_encode($infoClassifyId);
        $sourceUnitId = 3;
        $sourceUnit = marmot_encode($sourceUnitId);
        $items = array();
        $infoCategoryId = 3;
        $infoCategory = marmot_encode($infoCategoryId);
        $category = 21;
        $categoryEncode = marmot_encode($category);

        //预言
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('items'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($items);
        $request->post(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        $request->post(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($name);
        $request->post(Argument::exact('description'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($description);
        $request->post(Argument::exact('dimension'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($dimension);
        $request->post(Argument::exact('gbTemplate'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($gbTemplate);
        $request->post(Argument::exact('subjectCategory'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($subjectCategory);
        $request->post(Argument::exact('infoClassify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoClassify);
        $request->post(Argument::exact('infoCategory'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoCategory);
        $request->post(Argument::exact('sourceUnit'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($sourceUnit);
        $request->post(Argument::exact('exchangeFrequency'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($exchangeFrequency);
        $request->post(Argument::exact('category'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($categoryEncode);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddWbjTemplateCommand(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $sourceUnitId,
            $dimensionId,
            $exchangeFrequencyId,
            $infoClassifyId,
            $infoCategoryId
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        //绑定
        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('getRequest')->willReturn($request->reveal());

        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $name,
                $identify,
                $subjectCategory,
                $dimensionId,
                $exchangeFrequencyId,
                $infoClassifyId,
                $infoCategoryId,
                $description,
                $items
            )->willReturn(true);
    }

    public function testEditView()
    {
        $id = 1;

        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->wbjTemplateStub->editView($id);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testEditActionSuccess()
    {
        $id = 4;

        $this->initialEdit($id, true);

        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->wbjTemplateStub->editAction($id);
        
        $this->assertTrue($result);
    }

    public function testEditActionFailure()
    {
        $id = 3;

        $this->initialEdit($id, false);

        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->wbjTemplateStub->editAction($id);
        $this->assertFalse($result);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function initialEdit(int $id, bool $result)
    {
        $this->wbjTemplateStub = $this->getMockBuilder(MockWbjTemplateOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'validateCommonScenario'
                ]
            )->getMock();

            $name = 'name';
            $identify = 'identify';
            $exchangeFrequencyId = 1;
            $exchangeFrequency = marmot_encode($exchangeFrequencyId);
            $infoClassifyId = 1;
            $infoClassify = marmot_encode($infoClassifyId);
            $description = 'description';
            $subjectCategory = array();
            $dimensionId = 1;
            $dimension = marmot_encode($dimensionId);
            $infoCategoryId = 1;
            $infoCategory = marmot_encode($infoCategoryId);
            $gbTemplateId = 1;
            $gbTemplate = marmot_encode($gbTemplateId);
            $items = array();
            $sourceUnitId = 1;
            $sourceUnit = marmot_encode($sourceUnitId);
            $category = 21;
            $categoryEncode = marmot_encode($category);

            //预言
            $request = $this->prophesize(Request::class);
            
            $request->post(Argument::exact('exchangeFrequency'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($exchangeFrequency);
            $request->post(Argument::exact('identify'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($identify);
            $request->post(Argument::exact('description'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($description);
            $request->post(Argument::exact('subjectCategory'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)->willReturn($subjectCategory);
            $request->post(Argument::exact('dimension'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($dimension);
            $request->post(Argument::exact('gbTemplate'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($gbTemplate);
            $request->post(Argument::exact('items'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)->willReturn($items);
            $request->post(Argument::exact('category'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($categoryEncode);
            $request->post(Argument::exact('infoClassify'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($infoClassify);
            $request->post(Argument::exact('infoCategory'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($infoCategory);
            $request->post(Argument::exact('sourceUnit'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($sourceUnit);
            $request->post(Argument::exact('name'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($name);

        $this->wbjTemplateStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $name,
                $identify,
                $subjectCategory,
                $dimensionId,
                $exchangeFrequencyId,
                $infoClassifyId,
                $infoCategoryId,
                $description,
                $items
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditWbjTemplateCommand(
                    $name,
                    $identify,
                    $description,
                    $subjectCategory,
                    $items,
                    $sourceUnitId,
                    $dimensionId,
                    $exchangeFrequencyId,
                    $infoClassifyId,
                    $infoCategoryId,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->wbjTemplateStub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testValidateCommonScenario()
    {
        //初始化
        $this->wbjTemplateStub = $this->getMockBuilder(MockWbjTemplateOperationController::class)
            ->setMethods(
                [
                    'getTemplateWidgetRule'
                ]
            )->getMock();

            $subjectCategory = array();
            $dimension = 1;
            $exchangeFrequency = 2;
            $infoClassify = 3;
            $infoCategory = 1;
            $items = array();
            $name = 'name3';
            $identify = 'identify3';
            $description = 'description3';

        //预言
        $templateWidgetRule = $this->prophesize(TemplateWidgetRule::class);
        $templateWidgetRule->name($name)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->subjectCategory($subjectCategory)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->identify($identify)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->items($subjectCategory, $items)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->dimension($dimension)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->formatNumeric($infoClassify)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->formatNumeric($infoCategory)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->formatNumeric($exchangeFrequency)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->description($description)->shouldBeCalledTimes(1)->willReturn(true);
        //绑定
        $this->wbjTemplateStub->expects($this->any())
            ->method('getTemplateWidgetRule')
            ->willReturn($templateWidgetRule->reveal());

        //验证
        $this->wbjTemplateStub->validateCommonScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items
        );
    }
}
