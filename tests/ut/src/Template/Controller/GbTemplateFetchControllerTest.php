<?php
namespace Base\Package\Template\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Template\Model\GbTemplate;
use Sdk\Template\Repository\GbTemplateRepository;

use Base\Package\Template\View\Json\GbTemplateDetailView;
use Base\Package\Template\View\Json\GbTemplateListView;

class GbTemplateFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockGbTemplateFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new GbTemplateFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Template\Repository\GbTemplateRepository',
            $this->stub->getRepository()
        );
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockGbTemplateFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getRequest',
                    'filterFormatChange',
                    'getResponse',
                    'getPageAndSize',
                    'render'
                ]
            )->getMock();

        $request = $this->prophesize(Request::class);

        $page = 2;
        $size = 20;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $filter = array();
        $sort = ['-updateTime'];
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $list = array(1,2);
        $count = 10;
        
        $repository = $this->prophesize(GbTemplateRepository::class);
        $repository->scenario(Argument::exact(GbTemplateRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new GbTemplateListView($list, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->stub = $this->getMockBuilder(MockGbTemplateFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new GbTemplate($id);

        $repository = $this->prophesize(GbTemplateRepository::class);
        $repository->scenario(Argument::exact(GbTemplateRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new GbTemplateDetailView($data));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        //初始化
        $sort = '-updateTime';
        $name = 'name2';
        $identify = 'identify2';

        $request = $this->prophesize(Request::class);
        $request->get(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        $request->get(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($name);
        $request->get(Argument::exact('sort'), Argument::exact('-updateTime'))
            ->shouldBeCalledTimes(1)->willReturn($sort);

        $this->stub->expects($this->exactly(3))
        ->method('getRequest')->willReturn($request->reveal());

        $expected = [
            [
                'identify'=>$identify,
                'name'=>$name
            ],
            [$sort]
        ];
        
        $result = $this->stub->filterFormatChange();
        $this->assertEquals($expected, $result);
    }
}
