<?php
namespace Base\Package\Template\Controller;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD)
 */
class ControllerRequestValidateTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockControllerRequestValidateTrait();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetTemplateWidgetRule()
    {
        $this->assertInstanceof(
            'Sdk\Template\WidgetRule\TemplateWidgetRule',
            $this->stub->publicGetTemplateWidgetRule()
        );
    }

    public function testGetTypeCn()
    {
        $data = [
            [
                'id'=> 'MA',
                'name'=> '字符型',
            ]
        ];
        $type = array(1);
        $result = $this->stub->publicGetTypeCn($type);
        
        $this->assertEquals($data, $result);
    }

    public function testGetItemCn()
    {
        $data = array(
            array(
            "name" => '主体名称',    //信息项名称
            "identify" => 'ZTMC',    //数据标识
            "type" => 1,    //数据类型
            "length" => '200',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
            "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则
            "remarks" => '信用主体名称',    //备注
        )
        );

        $expression = array(array(
            "name" => '主体名称',    //信息项名称
            "identify" => 'ZTMC',    //数据标识
            "type" => 'MA',    //数据类型
            "length" => '200',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => 'MA',    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => 'MA',    //是否必填，否 0 | 默认 是 1
            "isMasked" => 'MA',    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则
            "remarks" => '信用主体名称',    //备注
        ));
        $result = $this->stub->publicGetItems($expression);
        
        $this->assertEquals($data, $result);
    }

    public function testGetDimensionCn()
    {
        $data = [
            [
                'id'=> 'MA',
                'name'=> '社会公开',
            ]
        ];
        $dimension = array(1);
        $result = $this->stub->publicGetDimensionCn($dimension);
        
        $this->assertEquals($data, $result);
    }

    public function testGetSubjectCategory()
    {
        $data = array(1);
        $subjectCategory = array('MA');
        $result = $this->stub->publicGetSubjectCategory($subjectCategory);
        
        $this->assertEquals($data, $result);
    }

    public function testGetInfoClassifyCn()
    {
        $data = array(
            array(
                'id'=> 'MA',
                'name'=> '行政许可',
            )
        );
        $infoClassify = array(1);
        $result = $this->stub->publicGetInfoClassifyCn($infoClassify);
        
        $this->assertEquals($data, $result);
    }

    public function testGetInfoCategoryCn()
    {
        $data = array(
            array(
                'id'=> 'MA',
                'name'=> '基础信息',
            )
        );
        $infoCategory = array(1);
        $result = $this->stub->publicGetInfoCategoryCn($infoCategory);
        
        $this->assertEquals($data, $result);
    }

    public function testGetExchangeFrequencyCn()
    {
        $data = array(
            array(
                'id'=> 'MA',
                'name'=> '实时',
            )
        );
        $exchangeFrequency = array(1);
        $result = $this->stub->publicGetExchangeFrequencyCn($exchangeFrequency);
        
        $this->assertEquals($data, $result);
    }

    public function testGetItemIsNull()
    {
        $expression= array(
            array(
                "name" => '主体名称',    //信息项名称
                "identify" => 'ZTMC',    //数据标识
                "type" => null,    //数据类型
                "length" => '200',    //数据长度
                "options" => array(),    //可选范围
                "dimension" => null,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => null,    //是否必填，否 0 | 默认 是 1
                "isMasked" => null,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则
                "remarks" => '信用主体名称',    //备注
            )
        );

        $data = array(array(
            "name" => '主体名称',    //信息项名称
            "identify" => 'ZTMC',    //数据标识
            "type" => '',    //数据类型
            "length" => '200',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => '',    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => '',    //是否必填，否 0 | 默认 是 1
            "isMasked" => '',    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则
            "remarks" => '信用主体名称',    //备注
        ));
        $result = $this->stub->publicGetItems($expression);
       
        $this->assertEquals($data, $result);
    }

    public function testGetSubjectCategoryCn()
    {
        $data = array(
            array(
                'id'=> 'MA',
                'name'=> '法人及非法人组织',
            )
        );
        $subjectCategory = array(1);
        $result = $this->stub->publicGetSubjectCategoryCn($subjectCategory);
        
        $this->assertEquals($data, $result);
    }
}
