<?php

namespace Base\Package\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Template\WidgetRule\TemplateWidgetRule;
use Sdk\Template\Command\GbTemplate\EditGbTemplateCommand;
use Sdk\Template\Command\GbTemplate\AddGbTemplateCommand;

use Base\Package\Template\View\Json\ConfigureView;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class GbTemplateOperationControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockGbTemplateOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->stub->getCommandBus()
        );
    }

    public function testCorrectExtendsController()
    {
        $controller = new GbTemplateOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->stub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->stub->addAction();
        $this->assertTrue($result);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $controller = new GbTemplateOperationController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IOperateAbleController', $controller);
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->addAction();
        $this->assertFalse($result);
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     * @SuppressWarnings(PHPMD)
     */
    private function initialAdd(bool $result)
    {
        //初始化
        $this->stub = $this->getMockBuilder(MockGbTemplateOperationController::class)
            ->setMethods(
                ['getRequest','displayError','displaySuccess','getCommandBus',
                'validateCommonScenario']
            )->getMock();

        $name = 'identify';
        $identify = 'name';
        $description = 'description.';
        $subjectCategory = array();
        $dimensionId = 5;
        $dimension = marmot_encode($dimensionId);
        $infoClassifyIds = 5;
        $infoClassify = marmot_encode($infoClassifyIds);
        $sourceUnitId = 5;
        $sourceUnit = marmot_encode($sourceUnitId);
        $gbTemplateId = 5;
        $gbTemplate = marmot_encode($gbTemplateId);
        $items = array();
        $exchangeFrequencyId = 5;
        $exchangeFrequency = marmot_encode($exchangeFrequencyId);
        $infoCategoryId = 5;
        $infoCategory = marmot_encode($infoCategoryId);
        $category = 21;
        $categoryEncode = marmot_encode($category);

        //预言
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($name);
        $request->post(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        $request->post(Argument::exact('description'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($description);
        $request->post(Argument::exact('dimension'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($dimension);
        $request->post(Argument::exact('subjectCategory'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($subjectCategory);
        $request->post(Argument::exact('sourceUnit'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($sourceUnit);
        $request->post(Argument::exact('infoClassify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoClassify);
        $request->post(Argument::exact('infoCategory'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoCategory);
        $request->post(Argument::exact('gbTemplate'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($gbTemplate);
        $request->post(Argument::exact('exchangeFrequency'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($exchangeFrequency);
        $request->post(Argument::exact('items'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($items);
        $request->post(Argument::exact('category'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($categoryEncode);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddGbTemplateCommand(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $dimensionId,
            $exchangeFrequencyId,
            $infoClassifyIds,
            $infoCategoryId
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        //绑定
        $this->stub->expects($this->exactly(1))
            ->method('getRequest')->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $name,
                $identify,
                $subjectCategory,
                $dimensionId,
                $exchangeFrequencyId,
                $infoClassifyIds,
                $infoCategoryId,
                $description,
                $items
            )->willReturn(true);
    }

    public function testEditActionSuccess()
    {
        $id = 3;

        $this->initialEdit($id, true);

        $this->stub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->stub->editAction($id);
        
        $this->assertTrue($result);
    }

    public function testEditActionFailure()
    {
        $id = 4;

        $this->initialEdit($id, false);

        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->editAction($id);
        $this->assertFalse($result);
    }

    public function testValidateCommonScenario()
    {
        //初始化
        $this->stub = $this->getMockBuilder(MockGbTemplateOperationController::class)
            ->setMethods(
                [
                    'getTemplateWidgetRule'
                ]
            )->getMock();

            $name = '名称';
            $identify = 'identify2';
            $description = 'description2';
            $subjectCategory = array();
            $dimension = 2;
            $exchangeFrequency = 2;
            $infoClassify = 2;
            $infoCategory = 2;
            $items = array();

        //预言
        $templateWidgetRule = $this->prophesize(TemplateWidgetRule::class);
        $templateWidgetRule->name($name)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->identify($identify)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->dimension($dimension)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->subjectCategory($subjectCategory)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->formatNumeric($exchangeFrequency)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->formatNumeric($infoClassify)->shouldBeCalledTimes(2)->willReturn(true);
        $templateWidgetRule->formatNumeric($infoCategory)->shouldBeCalledTimes(3)->willReturn(true);
        $templateWidgetRule->description($description)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->items($subjectCategory, $items)->shouldBeCalledTimes(1)->willReturn(true);
        //绑定
        $this->stub->expects($this->any())
            ->method('getTemplateWidgetRule')
            ->willReturn($templateWidgetRule->reveal());

        //验证
        $this->stub->validateCommonScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items
        );
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function initialEdit(int $id, bool $result)
    {
        $this->stub = $this->getMockBuilder(MockGbTemplateOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'validateCommonScenario'
                ]
            )->getMock();

            $name = 'name2';
            $identify = 'identify2';
            $description = 'description2';
            $infoClassifyId = 1;
            $infoClassify = marmot_encode($infoClassifyId);
            $infoCategoryId = 1;
            $infoCategory = marmot_encode($infoCategoryId);
            $subjectCategory = array();
            $dimensionId = 1;
            $dimension = marmot_encode($dimensionId);
            $exchangeFrequencyId = 1;
            $exchangeFrequency = marmot_encode($exchangeFrequencyId);
            $gbTemplateId = 1;
            $gbTemplate = marmot_encode($gbTemplateId);
            $items = array();
            $sourceUnitId = 1;
            $sourceUnit = marmot_encode($sourceUnitId);
            $category = 21;
            $categoryEncode = marmot_encode($category);
    
            //预言
            $request = $this->prophesize(Request::class);
            $request->post(Argument::exact('name'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($name);
            $request->post(Argument::exact('subjectCategory'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)->willReturn($subjectCategory);
            $request->post(Argument::exact('dimension'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($dimension);
            $request->post(Argument::exact('description'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($description);
            $request->post(Argument::exact('exchangeFrequency'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($exchangeFrequency);
            $request->post(Argument::exact('infoCategory'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($infoCategory);
            $request->post(Argument::exact('gbTemplate'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($gbTemplate);
            $request->post(Argument::exact('sourceUnit'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($sourceUnit);
            $request->post(Argument::exact('items'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)->willReturn($items);
            $request->post(Argument::exact('identify'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($identify);
            $request->post(Argument::exact('infoClassify'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($infoClassify);
            $request->post(Argument::exact('category'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($categoryEncode);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $name,
                $identify,
                $subjectCategory,
                $dimensionId,
                $exchangeFrequencyId,
                $infoClassifyId,
                $infoCategoryId,
                $description,
                $items
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditGbTemplateCommand(
                    $name,
                    $identify,
                    $description,
                    $subjectCategory,
                    $items,
                    $dimensionId,
                    $exchangeFrequencyId,
                    $infoClassifyId,
                    $infoCategoryId,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testAddView()
    {
        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testEditView()
    {
        $id = 1;

        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->editView($id);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function testConfigure()
    {
        $this->stub = $this->getMockBuilder(MockGbTemplateOperationController::class)
            ->setMethods(
                [
                    'getSubjectCategoryCn',
                    'getDimensionCn',
                    'getExchangeFrequencyCn',
                    'getInfoClassifyCn',
                    'getInfoCategoryCn',
                    'getTypeCn',
                    'render'
                ]
            )->getMock();

        $list = json_decode('{
              "subjectCategory": [
                  {
                      "id": "MA",
                      "name": "法人及非法人组织"
                  },
                  {
                      "id": "MQ",
                      "name": "自然人"
                  },
                  {
                      "id": "Mg",
                      "name": "个体工商户"
                  }
              ],
              "dimension": [
                  {
                      "id": "MA",
                      "name": "社会公开"
                  },
                  {
                      "id": "MQ",
                      "name": "政务共享"
                  },
                  {
                      "id": "Mg",
                      "name": "授权查询"
                  }
              ],
              "exchangeFrequency": [
                  {
                      "id": "MA",
                      "name": "实时"
                  },
                  {
                      "id": "MQ",
                      "name": "每日"
                  },
                  {
                      "id": "Mg",
                      "name": "每周"
                  },
                  {
                      "id": "Mw",
                      "name": "每月"
                  },
                  {
                      "id": "NA",
                      "name": "每季度"
                  },
                  {
                      "id": "NQ",
                      "name": "每半年"
                  },
                  {
                      "id": "Ng",
                      "name": "每一年"
                  },
                  {
                      "id": "Nw",
                      "name": "每两年"
                  },
                  {
                      "id": "OA",
                      "name": "每三年"
                  }
              ],
              "infoClassify": [
                  {
                      "id": "MA",
                      "name": "行政许可"
                  },
                  {
                      "id": "MQ",
                      "name": "行政处罚"
                  },
                  {
                      "id": "Mg",
                      "name": "红名单"
                  },
                  {
                      "id": "Mw",
                      "name": "黑名单"
                  },
                  {
                      "id": "NA",
                      "name": "其他"
                  }
              ],
              "infoCategory": [
                  {
                      "id": "MA",
                      "name": "基础信息"
                  },
                  {
                      "id": "Mg",
                      "name": "失信信息"
                  },
                  {
                      "id": "Mw",
                      "name": "其他信息"
                  }
              ],
              "type": [
                  {
                      "id": "MA",
                      "name": "字符型"
                  },
                  {
                      "id": "MQ",
                      "name": "日期型"
                  },
                  {
                      "id": "Mg",
                      "name": "整数型"
                  },
                  {
                      "id": "Mw",
                      "name": "浮点型"
                  },
                  {
                      "id": "NA",
                      "name": "枚举型"
                  },
                  {
                      "id": "NQ",
                      "name": "集合型"
                  }
              ]
          }', true);

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ConfigureView($list));

        $result = $this->stub->configure();
        $this->assertTrue($result);
    }
}
