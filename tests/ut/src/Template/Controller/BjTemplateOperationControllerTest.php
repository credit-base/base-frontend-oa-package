<?php

namespace Base\Package\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Template\Command\BjTemplate\AddBjTemplateCommand;
use Sdk\Template\Command\BjTemplate\EditBjTemplateCommand;
use Sdk\Template\WidgetRule\TemplateWidgetRule;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class BjTemplateOperationControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockBjTemplateOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess'
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new BjTemplateOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $controller = new BjTemplateOperationController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IOperateAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->stub->getCommandBus()
        );
    }

    public function testEditView()
    {
        $id = 1;

        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->editView($id);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAddView()
    {
        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->stub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->stub->addAction();
        $this->assertTrue($result);
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        //初始化
        $this->stub = $this->getMockBuilder(MockBjTemplateOperationController::class)
            ->setMethods(
                ['getRequest','displayError','displaySuccess','getCommandBus',
                'validateCommonScenario']
            )->getMock();

        $dimensionId = 1;
        $dimension = marmot_encode($dimensionId);
        $exchangeFrequencyId = 1;
        $exchangeFrequency = marmot_encode($exchangeFrequencyId);
        $infoClassifyId = 2;
        $infoClassify = marmot_encode($infoClassifyId);
        $infoCategoryId = 2;
        $infoCategory = marmot_encode($infoCategoryId);
        $sourceUnitId = 2;
        $sourceUnit = marmot_encode($sourceUnitId);
        $gbTemplateId = 2;
        $gbTemplate = marmot_encode($gbTemplateId);
        $items = array();
        $name = '名称';
        $identify = '标识';
        $description = '描述';
        $subjectCategory = array();
        $category = 21;
        $categoryEncode = marmot_encode($category);

        //预言
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($name);
        $request->post(Argument::exact('infoCategory'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoCategory);
        $request->post(Argument::exact('subjectCategory'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($subjectCategory);
        $request->post(Argument::exact('dimension'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($dimension);
        $request->post(Argument::exact('infoClassify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoClassify);
        $request->post(Argument::exact('items'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($items);
        $request->post(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        $request->post(Argument::exact('description'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($description);
        $request->post(Argument::exact('exchangeFrequency'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($exchangeFrequency);
        $request->post(Argument::exact('sourceUnit'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($sourceUnit);
        $request->post(Argument::exact('gbTemplate'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($gbTemplate);
        $request->post(Argument::exact('category'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($categoryEncode);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddBjTemplateCommand(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $sourceUnitId,
            $gbTemplateId,
            $dimensionId,
            $exchangeFrequencyId,
            $infoClassifyId,
            $infoCategoryId
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        //绑定
        $this->stub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
        $this->stub->expects($this->exactly(1))
            ->method('getRequest')->willReturn($request->reveal());
        $this->stub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $name,
                $identify,
                $subjectCategory,
                $dimensionId,
                $exchangeFrequencyId,
                $infoClassifyId,
                $infoCategoryId,
                $description,
                $items
            )->willReturn(true);
    }

    public function testEditActionSuccess()
    {
        $id = 1;

        $this->initialEdit($id, true);

        $this->stub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->stub->editAction($id);
        
        $this->assertTrue($result);
    }

    public function testEditActionFailure()
    {
        $id = 1;

        $this->initialEdit($id, false);

        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->editAction($id);
        $this->assertFalse($result);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function initialEdit(int $id, bool $result)
    {
        $this->stub = $this->getMockBuilder(MockBjTemplateOperationController::class)
            ->setMethods(
                [
                    'displaySuccess',
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

            $name = '名称';
            $identify = '标识';
            $description = '描述';
            $subjectCategory = [];
            $dimensionId = 1;
            $dimension = marmot_encode($dimensionId);
            $exchangeFrequencyId = 3;
            $exchangeFrequency = marmot_encode($exchangeFrequencyId);
            $infoClassifyId = 3;
            $infoClassify = marmot_encode($infoClassifyId);
            $infoCategoryId = 3;
            $infoCategory = marmot_encode($infoCategoryId);
            $sourceUnitId = 3;
            $sourceUnit = marmot_encode($sourceUnitId);
            $gbTemplateId = 3;
            $gbTemplate = marmot_encode($gbTemplateId);
            $items = array();
            $category = 21;
            $categoryEncode = marmot_encode($category);
    
            //预言
            $request = $this->prophesize(Request::class);
            $request->post(Argument::exact('dimension'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($dimension);
            $request->post(Argument::exact('name'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($name);
            $request->post(Argument::exact('identify'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($identify);
            $request->post(Argument::exact('description'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($description);
            $request->post(Argument::exact('exchangeFrequency'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($exchangeFrequency);
            $request->post(Argument::exact('infoCategory'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($infoCategory);
            $request->post(Argument::exact('items'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)->willReturn($items);
            $request->post(Argument::exact('sourceUnit'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($sourceUnit);
            $request->post(Argument::exact('infoClassify'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($infoClassify);
            $request->post(Argument::exact('subjectCategory'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)->willReturn($subjectCategory);
            $request->post(Argument::exact('gbTemplate'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($gbTemplate);
            $request->post(Argument::exact('category'), Argument::exact(''))
                ->shouldBeCalledTimes(1)->willReturn($categoryEncode);

        $this->stub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $name,
                $identify,
                $subjectCategory,
                $dimensionId,
                $exchangeFrequencyId,
                $infoClassifyId,
                $infoCategoryId,
                $description,
                $items
            )->willReturn(true);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $commandBus = $this->prophesize(CommandBus::class);

        $commandBus->send(
            Argument::exact(
                new EditBjTemplateCommand(
                    $name,
                    $identify,
                    $description,
                    $subjectCategory,
                    $items,
                    $sourceUnitId,
                    $gbTemplateId,
                    $dimensionId,
                    $exchangeFrequencyId,
                    $infoClassifyId,
                    $infoCategoryId,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testValidateCommonScenario()
    {
        //初始化
        $this->stub = $this->getMockBuilder(MockBjTemplateOperationController::class)
            ->setMethods(
                [
                    'getTemplateWidgetRule'
                ]
            )->getMock();

            $name = 'name';
            $identify = 'identify';
            $description = 'description';
            $subjectCategory = array();
            $dimension = 1;
            $exchangeFrequency = 1;
            $infoClassify = 1;
            $infoCategory = 1;
            $items = array();

        //预言
        $templateWidgetRule = $this->prophesize(TemplateWidgetRule::class);
        $templateWidgetRule->name($name)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->subjectCategory($subjectCategory)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->identify($identify)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->dimension($dimension)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->formatNumeric($exchangeFrequency)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->formatNumeric($infoClassify)->shouldBeCalledTimes(2)->willReturn(true);
        $templateWidgetRule->formatNumeric($infoCategory)->shouldBeCalledTimes(3)->willReturn(true);
        $templateWidgetRule->items($subjectCategory, $items)->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->description($description)->shouldBeCalledTimes(1)->willReturn(true);
        //绑定
        $this->stub->expects($this->any())
            ->method('getTemplateWidgetRule')
            ->willReturn($templateWidgetRule->reveal());

        //验证
        $this->stub->validateCommonScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items
        );
    }
}
