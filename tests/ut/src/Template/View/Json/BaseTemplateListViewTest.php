<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\BaseTemplateTranslator;
use Sdk\Template\Model\BaseTemplate;

class BaseTemplateListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockBaseTemplateListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetList()
    {
        $this->assertIsArray($this->view->getList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\BaseTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockBaseTemplateListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $bjTemplate = new BaseTemplate();
        $bjTemplateData = [];
        $bjTemplates = [$bjTemplate];
        $count = 1;

        $translator = $this->prophesize(BaseTemplateTranslator::class);
        $translator->objectToArray(
            $bjTemplate,
            array(
                    'id',
                    'name',
                    'identify',
                    'subjectCategory',
                    'exchangeFrequency',
                    'dimension',
                    'infoClassify',
                    'description',
                    'updateTime',
                    'infoCategory',
                    'items',
                    'ruleCount',
                    'dataTotal'
                )
        )->shouldBeCalledTimes(1)->willReturn($bjTemplateData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('getList')->willReturn($bjTemplates);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$bjTemplateData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
