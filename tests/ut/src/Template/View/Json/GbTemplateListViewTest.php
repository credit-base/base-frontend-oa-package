<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\GbTemplateTranslator;
use Sdk\Template\Model\GbTemplate;

class GbTemplateListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockGbTemplateListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetList()
    {
        $this->assertIsArray($this->view->getList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\GbTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockGbTemplateListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $gbTemplate = new GbTemplate();
        $gbTemplateData = [];
        $gbTemplates = [$gbTemplate];
        $count = 1;

        $translator = $this->prophesize(GbTemplateTranslator::class);
        $translator->objectToArray(
            $gbTemplate,
            array(
                    'id',
                    'name',
                    'identify',
                    'subjectCategory',
                    'exchangeFrequency',
                    'dimension',
                    'infoClassify',
                    'description',
                    'updateTime',
                    'infoCategory',
                    'items',
                    'ruleCount',
                    'dataTotal'
                )
        )->shouldBeCalledTimes(1)->willReturn($gbTemplateData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getList')->willReturn($gbTemplates);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$gbTemplateData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
