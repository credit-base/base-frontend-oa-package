<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\WbjTemplateTranslator;
use Sdk\Template\Model\WbjTemplate;

class WbjTemplateListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockWbjTemplateListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetList()
    {
        $this->assertIsArray($this->view->getList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\WbjTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockWbjTemplateListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $wbjTemplate = new WbjTemplate();
        $wbjTemplateData = [];
        $wbjTemplates = [$wbjTemplate];
        $count = 1;

        $translator = $this->prophesize(WbjTemplateTranslator::class);
        $translator->objectToArray(
            $wbjTemplate,
            array(
                    'id',
                    'name',
                    'identify',
                    'subjectCategory',
                    'exchangeFrequency',
                    'dimension',
                    'infoClassify',
                    'sourceUnit',
                    'description',
                    'updateTime',
                    'infoCategory',
                    'items',
                    'ruleCount',
                    'dataTotal'
                )
        )->shouldBeCalledTimes(1)->willReturn($wbjTemplateData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getList')->willReturn($wbjTemplates);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$wbjTemplateData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
