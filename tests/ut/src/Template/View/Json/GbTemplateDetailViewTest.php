<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\GbTemplateTranslator;
use Sdk\Template\Model\GbTemplate;

class GbTemplateDetailViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockGbTemplateDetailView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetGbTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Model\GbTemplate',
            $this->view->getGbTemplate()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\GbTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockGbTemplateDetailView::class)
            ->setMethods(['getTranslator','getGbTemplate','encode'])
            ->getMock();

        $news = new GbTemplate();
        $newsData = [];

        $translator = $this->prophesize(GbTemplateTranslator::class);
        $translator->objectToArray(
            $news
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getGbTemplate')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
