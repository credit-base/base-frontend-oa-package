<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\BjTemplateTranslator;
use Sdk\Template\Model\BjTemplate;

class BjTemplateListViewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockBjTemplateListView();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetList()
    {
        $this->assertIsArray($this->stub->getList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\BjTemplateTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockBjTemplateListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $bjTemplate = new BjTemplate();
        $bjTemplateData = [];
        $bjTemplates = [$bjTemplate];
        $count = 1;

        $translator = $this->prophesize(BjTemplateTranslator::class);
        $translator->objectToArray(
            $bjTemplate,
            array(
                    'id',
                    'name',
                    'identify',
                    'subjectCategory',
                    'exchangeFrequency',
                    'dimension',
                    'infoClassify',
                    'sourceUnit',
                    'gbTemplate',
                    'description',
                    'updateTime',
                    'infoCategory',
                    'items',
                    'ruleCount',
                    'dataTotal'
                )
        )->shouldBeCalledTimes(1)->willReturn($bjTemplateData);
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($bjTemplates);
        $this->stub->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$bjTemplateData]
            ]
        );

        $this->assertNull($this->stub->display());
    }
}
