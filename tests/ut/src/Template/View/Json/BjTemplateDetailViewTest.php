<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\BjTemplateTranslator;
use Sdk\Template\Model\BjTemplate;

class BjTemplateDetailViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockBjTemplateDetailView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetBjTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Model\BjTemplate',
            $this->view->getBjTemplate()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\BjTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockBjTemplateDetailView::class)
            ->setMethods(['getTranslator','getBjTemplate','encode'])
            ->getMock();

        $news = new BjTemplate();
        $newsData = [];

        $translator = $this->prophesize(BjTemplateTranslator::class);
        $translator->objectToArray(
            $news
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getBjTemplate')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
