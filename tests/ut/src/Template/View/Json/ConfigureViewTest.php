<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;

class ConfigureViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = new MockConfigureView();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetData()
    {
        $this->assertIsArray($this->stub->getData());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockConfigureView::class)
            ->setMethods(['getData','encode'])->getMock();

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn([]);
        $this->stub->expects($this->exactly(1))->method('encode')->with([]);
        $this->assertNull($this->stub->display());
    }
}
