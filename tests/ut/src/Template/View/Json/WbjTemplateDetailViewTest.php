<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\WbjTemplateTranslator;
use Sdk\Template\Model\WbjTemplate;

class WbjTemplateDetailViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockWbjTemplateDetailView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetWbjTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Model\WbjTemplate',
            $this->view->getWbjTemplate()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\WbjTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockWbjTemplateDetailView::class)
            ->setMethods(['getTranslator','getWbjTemplate','encode'])
            ->getMock();

        $news = new WbjTemplate();
        $newsData = [];

        $translator = $this->prophesize(WbjTemplateTranslator::class);
        $translator->objectToArray(
            $news
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getWbjTemplate')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
