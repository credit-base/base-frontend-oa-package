<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\BaseTemplateTranslator;
use Sdk\Template\Model\BaseTemplate;

class BaseTemplateDetailViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockBaseTemplateDetailView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetBaseTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Model\BaseTemplate',
            $this->view->getBaseTemplate()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\BaseTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockBaseTemplateDetailView::class)
            ->setMethods(['getTranslator','getBaseTemplate','encode'])
            ->getMock();

        $news = new BaseTemplate();
        $newsData = [];

        $translator = $this->prophesize(BaseTemplateTranslator::class);
        $translator->objectToArray(
            $news
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getBaseTemplate')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
