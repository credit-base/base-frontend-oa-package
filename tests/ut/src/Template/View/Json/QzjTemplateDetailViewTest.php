<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\QzjTemplateTranslator;
use Sdk\Template\Model\QzjTemplate;

class QzjTemplateDetailViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockQzjTemplateDetailView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetQzjTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Model\QzjTemplate',
            $this->view->getQzjTemplate()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\QzjTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockQzjTemplateDetailView::class)
            ->setMethods(['getTranslator','getQzjTemplate','encode'])
            ->getMock();

        $qzjTemplate = new QzjTemplate();
        $qzjTemplateData = [];

        $translator = $this->prophesize(QzjTemplateTranslator::class);
        $translator->objectToArray(
            $qzjTemplate
        )->shouldBeCalledTimes(1)->willReturn($qzjTemplateData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getQzjTemplate')->willReturn($qzjTemplate);
        $this->view->expects($this->exactly(1))->method('encode')->with($qzjTemplateData);
        $this->assertNull($this->view->display());
    }
}
