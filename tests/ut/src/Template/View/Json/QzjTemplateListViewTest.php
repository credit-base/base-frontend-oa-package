<?php
namespace Base\Package\Template\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\QzjTemplateTranslator;
use Sdk\Template\Model\QzjTemplate;

class QzjTemplateListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockQzjTemplateListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetQzjTemplateList()
    {
        $this->assertIsArray($this->view->getQzjTemplateList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\QzjTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockQzjTemplateListView::class)
            ->setMethods(['getTranslator', 'getQzjTemplateList', 'encode','getCount'])
            ->getMock();

        $qzjTemplate = new QzjTemplate();
        $qzjTemplateData = [];
        $qzjTemplates = [$qzjTemplate];
        $count = 1;

        $translator = $this->prophesize(QzjTemplateTranslator::class);
        $translator->objectToArray(
            $qzjTemplate,
            array(
                    'id',
                    'name',
                    'identify',
                    'subjectCategory',
                    'dimension',
                    'infoClassify',
                    'category',
                    'sourceUnit',
                    'updateTime',
                )
        )->shouldBeCalledTimes(1)->willReturn($qzjTemplateData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getQzjTemplateList')->willReturn($qzjTemplates);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$qzjTemplateData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
