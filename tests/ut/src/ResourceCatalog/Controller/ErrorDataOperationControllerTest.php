<?php

namespace Base\Package\ResourceCatalog\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Sdk\ResourceCatalog\Model\ErrorData;
use Sdk\ResourceCatalog\Model\NullErrorData;
use Sdk\ResourceCatalog\Repository\ErrorDataRepository;

use Base\Package\ResourceCatalog\View\Json\ErrorDataEditView;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ErrorDataOperationControllerTest extends TestCase
{
    private $errorDataController;

    public function setUp()
    {
        $this->errorDataController = $this->getMockBuilder(MockErrorDataOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->errorDataController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $errorDataController = new ErrorDataOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $errorDataController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $errorDataController = new ErrorDataOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $errorDataController
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\ResourceCatalog\Repository\ErrorDataRepository',
            $this->errorDataController->getRepository()
        );
    }

    public function testAddView()
    {
        $this->errorDataController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->errorDataController->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAddAction()
    {
        $this->errorDataController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->errorDataController->addAction();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testEditViewFailure()
    {
        $id = 0;

        $result = $this->errorDataController->editView($id);
        $this->assertFalse($result);
    }

    public function testEditViewNullFailure()
    {
        $this->errorDataController = $this->getMockBuilder(MockErrorDataOperationController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $errorData = new NullErrorData();

        $repository = $this->prophesize(ErrorDataRepository::class);
        $repository->scenario(Argument::exact(ErrorDataRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($errorData);

        $this->errorDataController->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->errorDataController->editView($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testEditViewSuccess()
    {
        $this->errorDataController = $this->getMockBuilder(MockErrorDataOperationController::class)
            ->setMethods(['getRepository','render','checkUserHasPurview'])->getMock();

        $id = 1;
        $errorData = new ErrorData($id);

        $this->errorDataController->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->with('errorData')
            ->willReturn(true);

        $repository = $this->prophesize(ErrorDataRepository::class);
        $repository->scenario(
            Argument::exact(ErrorDataRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($errorData);

        $this->errorDataController->expects($this->exactly(1))
                                  ->method('getRepository')
                                  ->willReturn($repository->reveal());

        $this->errorDataController->expects($this->exactly(1))
            ->method('render')
            ->with(new ErrorDataEditView($errorData));

        $result = $this->errorDataController->editView($id);
        $this->assertTrue($result);
    }

    public function testEditViewPurviewFail()
    {
        $this->errorDataController = $this->getMockBuilder(MockErrorDataOperationController::class)
            ->setMethods(['getRepository','render','checkUserHasPurview'])->getMock();
        $id = 1;
        $this->errorDataController->expects($this->exactly(1))
             ->method('checkUserHasPurview')
             ->willReturn(false);
        
        $result = $this->errorDataController->editView($id);
        $this->assertFalse($result);
    }

    public function testEditAction()
    {
        $this->errorDataController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->errorDataController->editAction(1);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testCheckUserHasPurview()
    {
        $this->stub = $this->getMockBuilder(MockErrorDataOperationController::class)
            ->setMethods(
                [
                    'checkUserHasErrorDataPurview',
                ]
            )->getMock();

        $resource = 'errorData';
        $this->stub->expects($this->any())
            ->method('checkUserHasErrorDataPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->stub->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }
}
