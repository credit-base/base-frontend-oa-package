<?php
namespace Base\Package\ResourceCatalog\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;

use Sdk\ResourceCatalog\Command\BjSearchData\DisableBjSearchDataCommand;

class BjSearchDataEnableControllerTest extends TestCase
{
    private $bjSearchDataStub;

    public function setUp()
    {
        $this->bjSearchDataStub = $this->getMockBuilder(MockBjSearchDataEnableController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->bjSearchDataStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new BjSearchDataEnableController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new BjSearchDataEnableController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IEnableAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $bjSearchDataStub = new MockBjSearchDataEnableController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $bjSearchDataStub->getCommandBus()
        );
    }

    public function testEnableAction()
    {
        $id = 0;

        $result = $this->bjSearchDataStub->enableAction($id);
        $this->assertFalse($result);
    }

    public function testDisableAction()
    {
        $id = 1;

        $command = new DisableBjSearchDataCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->bjSearchDataStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->bjSearchDataStub->disableAction($id);
        $this->assertTrue($result);
    }
}
