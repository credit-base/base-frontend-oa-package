<?php
namespace Base\Package\ResourceCatalog\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;

use Sdk\ResourceCatalog\Command\GbSearchData\DeleteGbSearchDataCommand;

class GbSearchDataDeleteControllerTest extends TestCase
{
    private $gbSearchDataStub;

    public function setUp()
    {
        $this->gbSearchDataStub = $this->getMockBuilder(MockGbSearchDataDeleteController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->gbSearchDataStubgbSearchDataStubgbSearchDataStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new GbSearchDataDeleteController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new GbSearchDataDeleteController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IDeleteAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $gbSearchDataStub = new MockGbSearchDataDeleteController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $gbSearchDataStub->getCommandBus()
        );
    }

    public function testDeleteAction()
    {
        $id = 1;

        $command = new DeleteGbSearchDataCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->gbSearchDataStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->gbSearchDataStub->deleteAction($id);
        $this->assertTrue($result);
    }
}
