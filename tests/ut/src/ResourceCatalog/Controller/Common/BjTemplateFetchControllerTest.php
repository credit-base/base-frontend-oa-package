<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;

use Sdk\Template\Model\BjTemplate;
use Sdk\Template\Repository\BjTemplateRepository;
use Base\Package\Template\View\Json\BjTemplateDetailView;
use Base\Package\ResourceCatalog\View\Json\Common\BjTemplateListView;

class BjTemplateFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockBjTemplateFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->request);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new BjTemplateFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Template\Repository\BjTemplateRepository',
            $this->stub->getRepository()
        );
    }

    public function testFilterAction()
    {
        $this->stub = $this->getMockBuilder(MockBjTemplateFetchController::class)
            ->setMethods(
                [
                    'getResponse',
                    'getRequest',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter = array();
        $sort = ['-updateTime'];

        $count = 10;
        $list = array(1,2);
        
        $repository = $this->prophesize(BjTemplateRepository::class);
        $repository->scenario(Argument::exact(BjTemplateRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact(PAGE),
            Argument::exact(COMMON_SIZES)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new BjTemplateListView($list, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->templateStub = $this->getMockBuilder(MockBjTemplateFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new BjTemplate($id);

        $templateRepository = $this->prophesize(BjTemplateRepository::class);
        $templateRepository->scenario(Argument::exact(BjTemplateRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($templateRepository->reveal());

        $templateRepository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->templateStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($templateRepository->reveal());

        $this->templateStub->expects($this->exactly(1))
            ->method('render')
            ->with(new BjTemplateDetailView($data));

        $result = $this->templateStub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }
}
