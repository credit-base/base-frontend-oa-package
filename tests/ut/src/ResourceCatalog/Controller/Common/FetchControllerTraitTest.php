<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;

class FetchControllerTraitTest extends TestCase
{
    private $resourceCatalogStub;

    public function setUp()
    {
        $this->resourceCatalogStub = $this->getMockBuilder(MockFetchController::class)
        ->setMethods(
            [
                'getRequest',
                'filterAction',
                'fetchOneAction',
                'displayError',
                'displaySuccess'
            ]
        )->getMock();
    }

    public function tearDown()
    {
        unset($this->resourceCatalogStub);
    }

    public function testGetResourceCatalogPageAndSize()
    {
        $size = 10;
        $page = 1;

        $request = $this->prophesize(Request::class);

        $request->get(Argument::exact('limit'), Argument::exact(10))
                ->shouldBeCalledTimes(1)
               ->willReturn($size);

        $request->get(Argument::exact('page'), Argument::exact(1))
            ->shouldBeCalledTimes(1)
            ->willReturn($page);

        $this->resourceCatalogStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $result = $this->resourceCatalogStub->getPageAndSize();

        $this->assertEquals([$size, $page], $result);
    }

    /**
     * @dataProvider resourceCatalogDataProvider
     */
    public function testResourceCatalogFilter($action, $expected)
    {

        $this->resourceCatalogStub->expects($this->exactly(1))
            ->method('filterAction')
            ->will($this->returnValue($action));

        if (!$expected) {
            $this->resourceCatalogStub->expects($this->exactly(1))
                 ->method('displayError');
        }

        $this->resourceCatalogStub->filter();
    }

    /**
     * @dataProvider resourceCatalogDataProvider
     */
    public function testResourceCatalogFetchOne($action, $expected)
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $this->resourceCatalogStub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id)
            ->will($this->returnValue($action));

        if (!$expected) {
            $this->resourceCatalogStub->expects($this->exactly(1))
                 ->method('displayError');
        }

        $this->resourceCatalogStub->fetchOne($idEncode);
    }

    public function resourceCatalogDataProvider()
    {
        return [
            [false, false],
            [true, true]
        ];
    }
}
