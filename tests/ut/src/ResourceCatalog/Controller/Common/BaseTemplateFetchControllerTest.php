<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Sdk\Template\Repository\BaseTemplateRepository;
use Base\Package\ResourceCatalog\View\Json\Common\BaseTemplateListView;

class BaseTemplateFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockBaseTemplateFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->request);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new BaseTemplateFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Template\Repository\BaseTemplateRepository',
            $this->stub->getRepository()
        );
    }

    public function testFilterAction()
    {
        $this->stub = $this->getMockBuilder(MockBaseTemplateFetchController::class)
            ->setMethods(
                [
                    'getResponse',
                    'getRequest',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter = array();
        $sort = ['-updateTime'];

        $count = 10;
        $list = array(1,2);
        
        $repository = $this->prophesize(BaseTemplateRepository::class);
        $repository->scenario(Argument::exact(BaseTemplateRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact(PAGE),
            Argument::exact(COMMON_SIZES)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new BaseTemplateListView($list, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFetchOneAction()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }
}
