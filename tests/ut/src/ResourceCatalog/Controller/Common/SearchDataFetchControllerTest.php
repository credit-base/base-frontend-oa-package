<?php
namespace Base\Package\ResourceCatalog\Controller\Common;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;

use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;
use Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Package\ResourceCatalog\View\Json\Common\SearchDataListView;
use Base\Package\ResourceCatalog\View\Json\BjSearchDataView;

class SearchDataFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockSearchDataFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->request);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new SearchDataFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\ResourceCatalog\Repository\BjSearchDataRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->searchDataStub = $this->getMockBuilder(MockSearchDataFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new BjSearchData($id);

        $searchDataRepository = $this->prophesize(BjSearchDataRepository::class);
        $searchDataRepository->scenario(Argument::exact(BjSearchDataRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($searchDataRepository->reveal());

        $searchDataRepository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->searchDataStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($searchDataRepository->reveal());

        $this->searchDataStub->expects($this->exactly(1))
            ->method('render')
            ->with(new BjSearchDataView($data));

        $result = $this->searchDataStub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        //初始化
        $infoClassify = 1;
        $infoClassifyEncode = marmot_encode($infoClassify);

        $sort = '-updateTime';
        $identify = 'identify';
        $status = BjSearchData::DATA_STATUS['CONFIRM'];

        $request = $this->prophesize(Request::class);

        $request->get(Argument::exact('sort'), Argument::exact('-updateTime'))
            ->shouldBeCalledTimes(1)->willReturn($sort);
        $request->get(Argument::exact('infoClassify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($infoClassifyEncode);

        $request->get(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        
        $this->stub->expects($this->exactly(1))
        ->method('getRequest')->willReturn($request->reveal());

        $expected = [
            [
                'infoClassify'=>$infoClassify,
                'status'=>$status,
                'identify'=>$identify,
            ],
            [$sort]
        ];

        $result = $this->stub->filterFormatChange();
        $this->assertEquals($expected, $result);
    }

    public function testFilterActionSuccess()
    {
        $this->searchDataStub = $this->getMockBuilder(MockSearchDataFetchController::class)
            ->setMethods(
                [
                    'getPageAndSize',
                    'getResponse',
                    'getRequest',
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $size = 10;
        $page = 1;

        $this->searchDataStub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $searchDataRequest = $this->prophesize(Request::class);
        $this->searchDataStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($searchDataRequest->reveal());

        $filter = array();
        $sort = ['-updateTime'];
        $this->searchDataStub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $count = 10;
        $list = array(1,2);
        
        $searchDataRepository = $this->prophesize(BjSearchDataRepository::class);
        $searchDataRepository->scenario(Argument::exact(BjSearchDataRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($searchDataRepository->reveal());
        $searchDataRepository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->searchDataStub->expects($this->exactly(1))
            ->method('render')
            ->with(new SearchDataListView($list, $count));

        $this->searchDataStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($searchDataRepository->reveal());

        $result = $this->searchDataStub->filterAction();
        $this->assertTrue($result);
    }
}
