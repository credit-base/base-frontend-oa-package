<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Crew\Model\Crew;

use Sdk\ResourceCatalog\Model\BjSearchData;
use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

use Base\Package\ResourceCatalog\View\Json\BjSearchDataView;
use Base\Package\ResourceCatalog\View\Json\BjSearchDataListView;

class BjSearchDataFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockBjSearchDataFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->request);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new BjSearchDataFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\ResourceCatalog\Repository\BjSearchDataRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->stub = $this->getMockBuilder(MockBjSearchDataFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new BjSearchData($id);

        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->scenario(Argument::exact(BjSearchDataRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new BjSearchDataView($data));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        //初始化
        $crew = new Crew(1);
        Core::$container->set('crew', $crew);
        
        $userGroup = 1;
        $userGroupEncode = marmot_encode($userGroup);

        $sort = '-updateTime';
        $identify = 'identify';
        $name1 = 'identify';
        $status = '-2';
        $templateId = 1;
        $templateIdEncode = marmot_encode($templateId);
        $statusEncode = marmot_encode($status);

        $request = $this->prophesize(Request::class);

        
        $request->get(Argument::exact('sort'), Argument::exact('-updateTime'))
            ->shouldBeCalledTimes(1)->willReturn($sort);
        $request->get(Argument::exact('status'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($statusEncode);
        $request->get(
            Argument::exact('userGroup'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userGroupEncode);
        $request->get(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        $request->get(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($name1);
        $request->get(Argument::exact('templateId'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($templateIdEncode);
        
        $this->stub->expects($this->exactly(1))
        ->method('getRequest')->willReturn($request->reveal());

        $expected = [
            [
                'name'=>$name1,
                'status'=>$status,
                'identify'=>$identify,
                'template'=>$templateId,
                'sourceUnit'=>$userGroup
            ],
            [$sort]
        ];

        $result = $this->stub->filterFormatChange();
        $this->assertEquals($expected, $result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockBjSearchDataFetchController::class)
            ->setMethods(
                [
                    'getPageAndSize',
                    'getResponse',
                    'getRequest',
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $size = 10;
        $page = 1;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $request = $this->prophesize(Request::class);
        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $filter = array();
        $sort = ['-updateTime'];
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $count = 10;
        $list = array(1,2);
        
        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->scenario(Argument::exact(BjSearchDataRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new BjSearchDataListView($list, $count));

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }
}
