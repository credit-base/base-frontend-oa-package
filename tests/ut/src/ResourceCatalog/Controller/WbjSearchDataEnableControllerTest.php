<?php
namespace Base\Package\ResourceCatalog\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;

use Sdk\ResourceCatalog\Command\WbjSearchData\DisableWbjSearchDataCommand;

class WbjSearchDataEnableControllerTest extends TestCase
{
    private $wbjSearchDataStub;

    public function setUp()
    {
        $this->wbjSearchDataStub = $this->getMockBuilder(MockWbjSearchDataEnableController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->wbjSearchDataStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new WbjSearchDataEnableController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new WbjSearchDataEnableController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IEnableAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $wbjSearchDataStub = new MockWbjSearchDataEnableController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $wbjSearchDataStub->getCommandBus()
        );
    }

    public function testEnableAction()
    {
        $id = 0;

        $result = $this->wbjSearchDataStub->enableAction($id);
        $this->assertFalse($result);
    }

    public function testDisableAction()
    {
        $id = 1;

        $command = new DisableWbjSearchDataCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->wbjSearchDataStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->wbjSearchDataStub->disableAction($id);
        $this->assertTrue($result);
    }
}
