<?php
namespace Base\Package\ResourceCatalog\Controller\Task;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;
use Sdk\ResourceCatalog\Model\Task\Task;

class BjTaskFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockBjTaskFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->request);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new BjTaskFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testFetchOneAction()
    {
        $id = 1;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockBjTaskFetchController::class)
            ->setMethods(
                [
                    'filterList',
                ]
            )->getMock();

        $targetCategory = Task::CATEGORY['BJ'];
        $this->stub->expects($this->any())
            ->method('filterList')
            ->with($targetCategory)
            ->willReturn(true);
     
        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }
}
