<?php
namespace Base\Package\ResourceCatalog\Controller\Task;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Crew\Model\Crew;

use Sdk\ResourceCatalog\Repository\TaskRepository;

use Base\Package\ResourceCatalog\View\Json\TaskListView;

class TaskFetchControllerTraitTest extends TestCase
{
    private $trait;
    private $request;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockTaskFetchControllerTrait::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )
           ->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->request);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testGetRepository()
    {
        $trait = new MockTaskFetchControllerTrait();
        $this->assertInstanceof(
            'Sdk\ResourceCatalog\Repository\TaskRepository',
            $trait->publicGetRepository()
        );
    }

    public function testFilterFormatChange()
    {
        $trait = $this->getMockBuilder(MockTaskFetchControllerTrait::class)
                           ->setMethods(
                               [
                                    'getRequest',
                                ]
                           )
                           ->getMock();

        //初始化
        $sort = '-createTime';
        $status = '-2,-3';
        $templateId = '1';
        $templateIdEncode = marmot_encode($templateId);
        $statusEncode = marmot_encode(-2);
        $targetCategory = 1;

        $crew = new Crew(1);
        Core::$container->set('crew', $crew);
        
        $userGroup = '1';
        $userGroupEncode = marmot_encode($userGroup);

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('userGroup'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userGroupEncode);
        $request->get(Argument::exact('sort'), Argument::exact('-createTime'))
            ->shouldBeCalledTimes(1)->willReturn($sort);
        $request->get(Argument::exact('status'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($statusEncode);
        $request->get(Argument::exact('templateId'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($templateIdEncode);
        
        $trait->expects($this->exactly(1))
        ->method('getRequest')->willReturn($request->reveal());

        $expected = [
            [
                'status'=>$status,
                'targetTemplate'=>$templateId,
                'userGroup'=>$userGroup,
                'targetCategory'=>$targetCategory
            ],
            [$sort]
        ];

        $result = $trait->publicFilterFormatChange($targetCategory);

        $this->assertEquals($expected, $result);
    }

    public function testFilterList()
    {
        $trait = $this->getMockBuilder(MockTaskFetchControllerTrait::class)
            ->setMethods(
                [
                    'getPageAndSize',
                    'getResponse',
                    'getRequest',
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $size = 10;
        $page = 1;

        $trait->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $request = $this->prophesize(Request::class);
        $trait->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $filter = array();
        $sort = ['-createTime'];
        $trait->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $count = 10;
        $list = array(1,2);
        $targetCategory = 1;
        
        $repository = $this->prophesize(TaskRepository::class);
        $repository->scenario(Argument::exact(TaskRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $trait->expects($this->exactly(1))
            ->method('render')
            ->with(new TaskListView($list, $count));

        $trait->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $trait->publicFilterList($targetCategory);
        $this->assertTrue($result);
    }
}
