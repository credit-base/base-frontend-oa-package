<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Crew\Model\Crew;
use Sdk\ResourceCatalog\Model\GbSearchData;
use Sdk\ResourceCatalog\Repository\GbSearchDataRepository;

use Base\Package\ResourceCatalog\View\Json\GbSearchDataView;
use Base\Package\ResourceCatalog\View\Json\GbSearchDataListView;

class GbSearchDataFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockGbSearchDataFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->request);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new GbSearchDataFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\ResourceCatalog\Repository\GbSearchDataRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->stub = $this->getMockBuilder(MockGbSearchDataFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new GbSearchData($id);

        $repository = $this->prophesize(GbSearchDataRepository::class);
        $repository->scenario(Argument::exact(GbSearchDataRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new GbSearchDataView($data));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
         //初始化
         $sort = '-updateTime';
         $identify = 'identify';
         $name1 = 'identify';
         $templateId = 1;
         $templateIdEncode = marmot_encode($templateId);
         $status = '-2';
         $statusEncode = marmot_encode($status);
 
         $crew = new Crew(1);
         Core::$container->set('crew', $crew);
         
         $userGroup = 1;
         $userGroupEncode = marmot_encode($userGroup);
 
         $request = $this->prophesize(Request::class);
         $request->get(Argument::exact('sort'), Argument::exact('-updateTime'))
             ->shouldBeCalledTimes(1)->willReturn($sort);
         $request->get(Argument::exact('identify'), Argument::exact(''))
             ->shouldBeCalledTimes(1)->willReturn($identify);
         $request->get(Argument::exact('name'), Argument::exact(''))
             ->shouldBeCalledTimes(1)->willReturn($name1);
         $request->get(Argument::exact('templateId'), Argument::exact(''))
             ->shouldBeCalledTimes(1)->willReturn($templateIdEncode);
         $request->get(Argument::exact('status'), Argument::exact(''))
             ->shouldBeCalledTimes(1)->willReturn($statusEncode);
         $request->get(
             Argument::exact('userGroup'),
             Argument::exact('')
         )->shouldBeCalledTimes(1)->willReturn($userGroupEncode);
 
         $this->stub->expects($this->exactly(1))
         ->method('getRequest')->willReturn($request->reveal());
 
         $expected = [
             [
                 'identify'=>$identify,
                 'name'=>$name1,
                 'status'=>$status,
                 'template'=>$templateId,
                 'sourceUnit'=>$userGroup
             ],
             [$sort]
         ];
 
         $result = $this->stub->filterFormatChange();
         $this->assertEquals($expected, $result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockGbSearchDataFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getRequest',
                    'filterFormatChange',
                    'getResponse',
                    'getPageAndSize',
                    'render'
                ]
            )->getMock();
    
        $filter = array();
        $sort = ['-updateTime'];
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $request = $this->prophesize(Request::class);

        $page = 2;
        $size = 20;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $list = array(1,2);
        $count = 10;
        
        $repository = $this->prophesize(GbSearchDataRepository::class);
    
        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $repository->scenario(Argument::exact(GbSearchDataRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new GbSearchDataListView($list, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }
}
