<?php
namespace Base\Package\ResourceCatalog\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;

use Sdk\ResourceCatalog\Command\GbSearchData\DisableGbSearchDataCommand;

class GbSearchDataEnableControllerTest extends TestCase
{
    private $gbSearchDataStub;

    public function setUp()
    {
        $this->gbSearchDataStub = $this->getMockBuilder(MockGbSearchDataEnableController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->gbSearchDataStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new GbSearchDataEnableController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new GbSearchDataEnableController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IEnableAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $gbSearchDataStub = new MockGbSearchDataEnableController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $gbSearchDataStub->getCommandBus()
        );
    }

    public function testEnableAction()
    {
        $id = 0;

        $result = $this->gbSearchDataStub->enableAction($id);
        $this->assertFalse($result);
    }

    public function testDisableAction()
    {
        $id = 1;

        $command = new DisableGbSearchDataCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->gbSearchDataStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->gbSearchDataStub->disableAction($id);
        $this->assertTrue($result);
    }
}
