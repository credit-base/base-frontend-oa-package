<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\Guest;

use Sdk\ResourceCatalog\Model\WbjSearchData;
use Sdk\ResourceCatalog\Repository\WbjSearchDataRepository;

use Sdk\ResourceCatalog\Model\Attachment;

use Base\Package\ResourceCatalog\View\Json\WbjSearchDataView;
use Base\Package\ResourceCatalog\View\Json\WbjSearchDataListView;

/**
 * @SuppressWarnings(PHPMD)
 */
class WbjSearchDataFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockWbjSearchDataFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->request);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockWbjSearchDataFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getPageAndSize',
                    'filterFormatChange',
                    'getRequest',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $request = $this->prophesize(Request::class);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $filter = array();
        $sort = ['-updateTime'];
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $page = 4;
        $size = 20;
        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $list = array(4,6);
        $count = 2;
        $repository = $this->prophesize(WbjSearchDataRepository::class);
        
        $repository->scenario(Argument::exact(WbjSearchDataRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new WbjSearchDataListView($list, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testCorrectExtendsController()
    {
        $controller = new WbjSearchDataFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\ResourceCatalog\Repository\WbjSearchDataRepository',
            $this->stub->getRepository()
        );
    }

    public function testGetAttachment()
    {
        $this->assertInstanceof(
            'Sdk\ResourceCatalog\Model\Attachment',
            $this->stub->getAttachment()
        );
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->stub = $this->getMockBuilder(MockWbjSearchDataFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new WbjSearchData($id);

        $repository = $this->prophesize(WbjSearchDataRepository::class);
        $repository->scenario(Argument::exact(WbjSearchDataRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new WbjSearchDataView($data));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        //初始化
        $sort = '-updateTime';
        $name = 'name3';
        $identify = 'identify3';
        $sourceUnitId = '2';
        $sourceUnitEncode = marmot_encode($sourceUnitId);
        $status = WbjSearchData::DATA_STATUS['CONFIRM'];

        $templateId = '1';
        $templateIdEncode = marmot_encode($templateId);

        $crew = new Crew(1);
        Core::$container->set('crew', $crew);

        $request = $this->prophesize(Request::class);
        $request->get(Argument::exact('sort'), Argument::exact('-updateTime'))
            ->shouldBeCalledTimes(1)->willReturn($sort);
        $request->get(Argument::exact('identify'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($identify);
        $request->get(Argument::exact('name'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($name);
        $request->get(Argument::exact('userGroup'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($sourceUnitEncode);
    
        $request->get(Argument::exact('templateId'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($templateIdEncode);

        $this->stub->expects($this->exactly(1))
        ->method('getRequest')->willReturn($request->reveal());

        $expected = [
            [
                'identify'=>$identify,
                'name'=>$name,
                'sourceUnit'=>$sourceUnitId,
                'status'=> $status,
                'template'=>$templateId
            ],
            [$sort]
        ];

        $result = $this->stub->filterFormatChange();
        $this->assertEquals($expected, $result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testIsExitCrewIdFailure()
    {
        $crew = new Guest();
        Core::$container->set('crew', $crew);

        $result = $this->stub->isExitCrewId();
        $this->assertFalse($result);
    }

    public function testIsExitCrewIdSuccess()
    {
        $crew = new Crew(1);
        Core::$container->set('crew', $crew);

        $result = $this->stub->isExitCrewId();
        $this->assertTrue($result);
    }

    public function testUploadFail()
    {
        $this->stub = $this->getMockBuilder(MockWbjSearchDataFetchController::class)
            ->setMethods(
                [
                    'isExitCrewId',
                    'displaySuccess',
                    'displayError',
                ]
            )->getMock();

        $templateId = 'MA';

        Core::$container->get('crew')->setId(0);
                
        $result = $this->stub->upload($templateId);
        $this->assertFalse($result);
    }

    public function testUploadSuccess()
    {
        $this->stub = $this->getMockBuilder(MockWbjSearchDataFetchController::class)
            ->setMethods(
                [
                    'isExitCrewId',
                    'displaySuccess',
                    'displayError',
                    'getAttachment',
                    'getFile'
                ]
            )->getMock();

        $templateIdEncode = 'MA';
        $templateId = 1;
        $source = '/var/www/html/tests/mock/ResourceCatalog/Model/MockFile';
        $size = 10;
        $name = 'name';
        $prefix = 'file';
        $extension = 'xlsx';

        $file[$prefix]['size'] = $size;
        $file[$prefix]['name'] = $name.'.'.$extension;
        $file[$prefix]['tmp_name'] = $source;
        
        Core::$container->set('attachment.upload.path', '/var/www/html/tests/mock/ResourceCatalog/Model/MockFile');
        $crew = new Crew(1);
        Core::$container->set('crew', $crew);
        
        $this->stub->expects($this->exactly(1))
            ->method('getFile')->willReturn($file);
                   
        $attachment = $this->prophesize(Attachment::class);
        $attachment->upload(Argument::exact($templateId), Argument::exact('file'))
         ->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('isExitCrewId')->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('getAttachment')->willReturn($attachment->reveal());

        $result = $this->stub->upload($templateIdEncode);
       
        $this->assertTrue($result);
    }
}
