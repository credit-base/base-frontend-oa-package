<?php
namespace Base\Package\ResourceCatalog\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;

use Sdk\ResourceCatalog\Command\BjSearchData\ConfirmBjSearchDataCommand;

class BjSearchDataStatusControllerTest extends TestCase
{
    private $bjSearchDataStub;

    public function setUp()
    {
        $this->bjSearchDataStub = $this->getMockBuilder(MockBjSearchDataStatusController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->bjSearchDataStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new BjSearchDataStatusController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetCommandBus()
    {
        $bjSearchDataStub = new MockBjSearchDataStatusController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $bjSearchDataStub->getCommandBus()
        );
    }

    public function testConfirmSuccess()
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $command = new ConfirmBjSearchDataCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->bjSearchDataStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
        
        $this->bjSearchDataStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->bjSearchDataStub->publicConfirm($idEncode);
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $command = new ConfirmBjSearchDataCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(false);

        $this->bjSearchDataStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
        
        $this->bjSearchDataStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(true);

        $result = $this->bjSearchDataStub->publicConfirm($idEncode);
        $this->assertFalse($result);
    }
}
