<?php
namespace Base\Package\ResourceCatalog\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;

use Sdk\ResourceCatalog\Model\ErrorData;
use Sdk\Crew\Model\Crew;
use Sdk\ResourceCatalog\Model\NullErrorData;
use Sdk\ResourceCatalog\Repository\ErrorDataRepository;

use Base\Package\ResourceCatalog\View\Json\ErrorDataView;
use Base\Package\ResourceCatalog\View\Json\ErrorDataListView;

/**
 * @SuppressWarnings(PHPMD)
 */
class ErrorDataFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockErrorDataFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        Core::$container->set('crew', new Crew(1));
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new ErrorDataFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\ResourceCatalog\Repository\ErrorDataRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockErrorDataFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $errorData = new NullErrorData();

        $repository = $this->prophesize(ErrorDataRepository::class);
        $repository->scenario(Argument::exact(ErrorDataRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($errorData);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionPurviewFail()
    {
        $this->errorDataController = $this->getMockBuilder(MockErrorDataFetchController::class)
            ->setMethods(['getRepository','render','checkUserHasPurview'])->getMock();
        $id = 1;
        $this->errorDataController->expects($this->exactly(1))
             ->method('checkUserHasPurview')
             ->willReturn(false);
        
        $result = $this->errorDataController->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterActionPurviewFail()
    {
        $this->errorDataController = $this->getMockBuilder(MockErrorDataFetchController::class)
            ->setMethods(['getRepository','render','checkUserHasPurview'])->getMock();
   
        $this->errorDataController->expects($this->exactly(1))
             ->method('checkUserHasPurview')
             ->willReturn(false);
        
        $result = $this->errorDataController->filterAction();
        $this->assertFalse($result);
    }


    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockErrorDataFetchController::class)
            ->setMethods([
                'getRepository',
                'render',
                'checkUserHasPurview'
                ])->getMock();

        $id = 1;
        $errorData = new ErrorData($id);

        $this->stub->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->with('errorData')
            ->willReturn(true);

        $repository = $this->prophesize(ErrorDataRepository::class);
        $repository->scenario(
            Argument::exact(ErrorDataRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($errorData);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ErrorDataView($errorData));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockErrorDataFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render',
                    'checkUserHasPurview'
                ]
            )->getMock();

        $filter['task'] = 1;
        $filter['template'] = 1;
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->with('errorData')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $errorDataArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(ErrorDataRepository::class);
        $repository->scenario(Argument::exact(ErrorDataRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$errorDataArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ErrorDataListView($errorDataArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockErrorDataFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $sort = '-updateTime';
        $taskId = '1';
        $taskIdEncode = marmot_encode($taskId);
        $templateId = 1;
        $templateIdEncode = marmot_encode($templateId);

        $id = 1;
        $crew = new Crew($id);
        Core::$container->set('crew', $crew);

        $filter['task'] = $taskId;
        $filter['template'] = $templateId;

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $request->get(
            Argument::exact('taskId'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($taskIdEncode);

        $request->get(
            Argument::exact('templateId'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($templateIdEncode);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort)], $result);
    }

    public function testCheckUserHasPurview()
    {
        $this->stub = $this->getMockBuilder(MockErrorDataFetchController::class)
            ->setMethods(
                [
                    'checkUserHasErrorDataPurview',
                ]
            )->getMock();

        $resource = 'errorData';
        $this->stub->expects($this->any())
            ->method('checkUserHasErrorDataPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->stub->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }

    public function testCheckUserHasErrorDataPurviewSuccess()
    {
        $resource = 'gbSearchData';
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setCategory(2);

        $this->assertTrue($this->stub->checkUserHasErrorDataPurview($resource));
    }

    public function testCheckUserHasErrorDataPurviewFail()
    {
        $resource = 'string';
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setCategory(2);

        $this->assertFalse($this->stub->checkUserHasErrorDataPurview($resource));
    }
}
