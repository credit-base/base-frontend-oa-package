<?php
namespace Base\Package\ResourceCatalog\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;

use Sdk\ResourceCatalog\Command\GbSearchData\ConfirmGbSearchDataCommand;

class GbSearchDataStatusControllerTest extends TestCase
{
    private $gbSearchDataStub;

    public function setUp()
    {
        $this->gbSearchDataStub = $this->getMockBuilder(MockGbSearchDataStatusController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->gbSearchDataStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new GbSearchDataStatusController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetCommandBus()
    {
        $gbSearchDataStub = new MockGbSearchDataStatusController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $gbSearchDataStub->getCommandBus()
        );
    }

    public function testConfirmSuccess()
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $command = new ConfirmGbSearchDataCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->gbSearchDataStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
        
        $this->gbSearchDataStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->gbSearchDataStub->publicConfirm($idEncode);
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $command = new ConfirmGbSearchDataCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(false);

        $this->gbSearchDataStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
        
        $this->gbSearchDataStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(true);

        $result = $this->gbSearchDataStub->publicConfirm($idEncode);
        $this->assertFalse($result);
    }
}
