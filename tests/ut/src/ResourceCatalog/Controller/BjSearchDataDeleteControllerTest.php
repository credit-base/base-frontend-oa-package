<?php
namespace Base\Package\ResourceCatalog\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;

use Sdk\ResourceCatalog\Command\BjSearchData\DeleteBjSearchDataCommand;

class BjSearchDataDeleteControllerTest extends TestCase
{
    private $bjSearchDataStub;

    public function setUp()
    {
        $this->bjSearchDataStub = $this->getMockBuilder(MockBjSearchDataDeleteController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->bjSearchDataStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new BjSearchDataDeleteController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new BjSearchDataDeleteController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IDeleteAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $bjSearchDataStub = new MockBjSearchDataDeleteController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $bjSearchDataStub->getCommandBus()
        );
    }

    public function testDeleteAction()
    {
        $id = 1;

        $command = new DeleteBjSearchDataCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->bjSearchDataStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->bjSearchDataStub->deleteAction($id);
        $this->assertTrue($result);
    }
}
