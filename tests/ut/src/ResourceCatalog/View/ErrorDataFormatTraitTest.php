<?php


namespace Base\Package\ResourceCatalog\View;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\ErrorDataTranslator;
use Sdk\ResourceCatalog\Model\ErrorData;

class ErrorDataFormatTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockErrorDataFormatTraitTest();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetErrorDataFormatName()
    {
        $errorData = [
            'itemsData' => [
                'ZTMC' => 'ZTMC',
                'ZJHM' => 'ZJHM',
            ]
        ];

        $result = $errorData;


        $result['identify'] = $errorData['itemsData']['ZJHM'];
        $result['name'] = $errorData['itemsData']['ZTMC'];

        $this->assertEquals($result, $this->trait->getErrorDataFormatNamePublic($errorData));
    }

    public function testGetFormatErrorData()
    {
        $this->trait = $this->getMockBuilder(MockErrorDataFormatTraitTest::class)
            ->setMethods(['getErrorDataFormatName','getNewItemsByIdentify','getErrorReasonByIdentify'])->getMock();

        $errorData = [
            'itemsData' => [],
            'errorReason' => ['errorReason'],
        ];
        $value = 'string';
        $errorData['template']['items'][] = [
            'identify' => 'identify',
            'name' => 'name',
        ];

        $errorReason = 'errorReason';

        $this->trait->expects($this->exactly(1))->method('getErrorDataFormatName')
            ->with($errorData)->willReturn($errorData);

        $this->trait->expects($this->exactly(1))->method('getNewItemsByIdentify')
            ->with(
                $errorData['template']['items'][0]['identify'],
                $errorData['itemsData']
            )
            ->willReturn($value);

        $this->trait->expects($this->exactly(1))->method('getErrorReasonByIdentify')
            ->with(
                $errorData['template']['items'][0]['identify'],
                $errorData['template']['items'][0]['name'],
                $errorData['errorReason']
            )
            ->willReturn($errorReason);

        $newItemsData[] = [
            'value' => $value,
            'errorReason' => $errorData['errorReason'][0],
            'identify' => $errorData['template']['items'][0]['identify'],
            'name' => $errorData['template']['items'][0]['name'],
        ];
        $result['itemsData'] = $newItemsData;

        $this->assertEquals($result, $this->trait->getFormatErrorDataPublic($errorData));
    }

    public function testGetNewItemsByIdentify()
    {
        $identify = 0;
        $itemsData = [
            $identify => 'identify'
        ];

        $this->assertEquals($itemsData[$identify], $this->trait->getNewItemsByIdentifyPublic($identify, $itemsData));
    }

    public function testGetErrorReasonByIdentify()
    {
        $this->trait = $this->getMockBuilder(MockErrorDataFormatTraitTest::class)
            ->setMethods(['getErrorReasonCn'])->getMock();

        $identify = 0;
        $errorReasonData = [
            $identify => ['errorReasonData']
        ];

        $itemName = 'itemName';


        $this->trait->expects($this->exactly(1))
            ->method('getErrorReasonCn')
            ->with($errorReasonData[$identify])
            ->willReturn($errorReasonData[$identify][0]);


        $result = $itemName . $errorReasonData[$identify][0];
        $this->assertEquals(
            $result,
            $this->trait->getErrorReasonByIdentifyPublic($identify, $itemName, $errorReasonData)
        );
    }

    public function testGetErrorReasonCn()
    {
        $errorReasonData = [
                0 => ErrorData::ERROR_TYPE['DUPLICATION_DATA'],
                1 => ErrorData::ERROR_TYPE['FORMAT_VALIDATION_FAILED'],
                2 => ErrorData::ERROR_TYPE['MISSING_DATA'],
        ];

        $result = ErrorDataTranslator::ERROR_TYPE_CN[ErrorData::ERROR_TYPE['DUPLICATION_DATA']] . ',且' .
            ErrorDataTranslator::ERROR_TYPE_CN[ErrorData::ERROR_TYPE['FORMAT_VALIDATION_FAILED']] . ',' .
            ErrorDataTranslator::ERROR_TYPE_CN[ErrorData::ERROR_TYPE['MISSING_DATA']];

        $this->assertEquals($result, $this->trait->getErrorReasonCnPublic($errorReasonData));
    }

    public function testGetErrorDataDetail()
    {

        $this->trait = $this->getMockBuilder(MockErrorDataFormatTraitTest::class)
            ->setMethods(['getFormatItemsData'])->getMock();

        $errorData['itemsData'] = [];
        $this->trait->expects($this->exactly(1))
            ->method('getFormatItemsData')
            ->with($errorData['itemsData'])
            ->willReturn($errorData['itemsData']);


        $this->assertEquals($errorData, $this->trait->getErrorDataDetailPublic($errorData));
    }

    public function testGetFormatItemsData()
    {
        $itemsData[] = [
            'id' => 'id',
            'name' => 'name',
            'value' => 'value',
            'errorReason' => 'errorReason',
        ];

        $newItemsData[] = [
            'name' => 'name',
            'value' => 'value',
            'errorReason' => 'errorReason',
        ];

        $this->assertEquals($newItemsData, $this->trait->getFormatItemsDataPublic($itemsData));
    }
}
