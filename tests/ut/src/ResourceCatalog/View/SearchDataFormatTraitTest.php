<?php


namespace Base\Package\ResourceCatalog\View;

use PHPUnit\Framework\TestCase;

class SearchDataFormatTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockSearchDataFormatTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testItemsRelationTemplate()
    {
        $itemsData = [
            'data' => [
                0 =>['id' => 'id']
            ]
        ];
        $template = [
            'items' => [
                [
                    'identify' => 0,
                    'name' => 'name'
                ]
            ]
        ];

        $resultList[] = [
            'name'=>$template['items'][0]['name'],
            'value'=>$itemsData['data'][0],
        ];

        $this->assertEquals($resultList, $this->trait->itemsRelationTemplatePublic($itemsData, $template));
    }
}
