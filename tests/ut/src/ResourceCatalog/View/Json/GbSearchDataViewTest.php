<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\GbSearchDataTranslator;
use Sdk\ResourceCatalog\Model\GbSearchData;

class GbSearchDataViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockGbSearchDataView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetGbSearchData()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Model\GbSearchData',
            $this->view->getGbSearchData()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\GbSearchDataTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockGbSearchDataView::class)
            ->setMethods(['getTranslator','getGbSearchData','encode','itemsRelationTemplate'])
            ->getMock();

        $news = new GbSearchData();
        $newsData = [
            'itemsData' => ['id' => 1],
            'template' => ['id' => 1],
        ];

        $result = [
            'itemsData' => []
        ];

        $translator = $this->prophesize(GbSearchDataTranslator::class);
        $translator->objectToArray(
            $news
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getGbSearchData')->willReturn($news);

        $this->view->expects($this->exactly(1))
            ->method('itemsRelationTemplate')
            ->with($newsData['itemsData'], $newsData['template'])
        ->willReturn([]);

        $this->view->expects($this->exactly(1))->method('encode')->with($result);

        $this->assertNull($this->view->display());
    }
}
