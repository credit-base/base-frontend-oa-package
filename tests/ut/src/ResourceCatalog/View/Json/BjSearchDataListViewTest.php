<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;
use Sdk\ResourceCatalog\Model\BjSearchData;

class BjSearchDataListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockBjSearchDataListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetBjSearchData()
    {
        $this->assertIsArray($this->view->getBjSearchData());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\BjSearchDataTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockBjSearchDataListView::class)
            ->setMethods(['getTranslator', 'getBjSearchData', 'encode','getCount'])
            ->getMock();

        $bjSearchData = new BjSearchData();
        $bjSearchDataData = [];
        $bjSearchDatas = [$bjSearchData];
        $count = 1;

        $translator = $this->prophesize(BjSearchDataTranslator::class);
        $translator->objectToArray(
            $bjSearchData,
            array(
                    'id',
                    'name',
                    'identify',
                    'status',
                    'updateTime',
                )
        )->shouldBeCalledTimes(1)->willReturn($bjSearchDataData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getBjSearchData')->willReturn($bjSearchDatas);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$bjSearchDataData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
