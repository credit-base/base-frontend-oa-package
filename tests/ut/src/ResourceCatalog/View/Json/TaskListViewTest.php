<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\TaskTranslator;
use Sdk\ResourceCatalog\Model\Task\Task;

class TaskListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockTaskListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetList()
    {
        $this->assertIsArray($this->view->getList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\TaskTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockTaskListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $task = new Task();
        $taskData = [];
        $tasks = [$task];
        $count = 1;

        $translator = $this->prophesize(TaskTranslator::class);
        $translator->objectToArray(
            $task,
            array(
                    'id',
                    'total',
                    'successNumber',
                    'failureNumber',
                    'fileName',
                    'crew',
                    'status',
                    'updateTime',
                    'createTime'
                )
        )->shouldBeCalledTimes(1)->willReturn($taskData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getList')->willReturn($tasks);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$taskData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
