<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\WbjSearchDataTranslator;
use Sdk\ResourceCatalog\Model\WbjSearchData;

class WbjSearchDataViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockWbjSearchDataView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetWbjSearchData()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Model\WbjSearchData',
            $this->view->getWbjSearchData()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\WbjSearchDataTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockWbjSearchDataView::class)
            ->setMethods(['getTranslator','getWbjSearchData','encode','itemsRelationTemplate'])
            ->getMock();

        $news = new WbjSearchData();
        $newsData = [
            'itemsData' => ['id' => 1],
            'template' => ['id' => 1],
        ];

        $result = [
            'itemsData' => []
        ];

        $translator = $this->prophesize(WbjSearchDataTranslator::class);
        $translator->objectToArray(
            $news
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getWbjSearchData')->willReturn($news);

        $this->view->expects($this->exactly(1))
            ->method('itemsRelationTemplate')
            ->with($newsData['itemsData'], $newsData['template'])
        ->willReturn([]);

        $this->view->expects($this->exactly(1))->method('encode')->with($result);

        $this->assertNull($this->view->display());
    }
}
