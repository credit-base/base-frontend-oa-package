<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\GbSearchDataTranslator;
use Sdk\ResourceCatalog\Model\GbSearchData;

class GbSearchDataListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockGbSearchDataListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetGbSearchDatas()
    {
        $this->assertIsArray($this->view->getGbSearchDatas());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\GbSearchDataTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockGbSearchDataListView::class)
            ->setMethods(['getTranslator', 'getGbSearchDatas', 'encode','getCount'])
            ->getMock();

        $new = new GbSearchData();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(GbSearchDataTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                    'id',
                    'name',
                    'identify',
                    'status',
                    'updateTime',
                )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getGbSearchDatas')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
