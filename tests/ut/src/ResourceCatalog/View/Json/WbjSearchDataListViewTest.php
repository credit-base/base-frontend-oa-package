<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\WbjSearchDataTranslator;
use Sdk\ResourceCatalog\Model\WbjSearchData;

class WbjSearchDataListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockWbjSearchDataListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetWbjSearchDatas()
    {
        $this->assertIsArray($this->view->getWbjSearchDatas());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\WbjSearchDataTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockWbjSearchDataListView::class)
            ->setMethods(['getTranslator', 'getWbjSearchDatas', 'encode','getCount'])
            ->getMock();

        $new = new WbjSearchData();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(WbjSearchDataTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                    'id',
                    'name',
                    'identify',
                    'status',
                    'updateTime',
                )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getWbjSearchDatas')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
