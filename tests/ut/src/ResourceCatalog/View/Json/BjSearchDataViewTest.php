<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;
use Sdk\ResourceCatalog\Model\BjSearchData;

class BjSearchDataViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockBjSearchDataView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetBjSearchData()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Model\BjSearchData',
            $this->view->getBjSearchData()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\BjSearchDataTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockBjSearchDataView::class)
            ->setMethods(['getTranslator','getBjSearchData','encode','itemsRelationTemplate'])
            ->getMock();

        $news = new BjSearchData();
        $newsData = [
            'itemsData' => ['id' => 1],
            'template' => ['id' => 1],
        ];

        $result = [
            'itemsData' => []
        ];

        $translator = $this->prophesize(BjSearchDataTranslator::class);
        $translator->objectToArray(
            $news
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getBjSearchData')->willReturn($news);

        $this->view->expects($this->exactly(1))
            ->method('itemsRelationTemplate')
            ->with($newsData['itemsData'], $newsData['template'])
        ->willReturn([]);

        $this->view->expects($this->exactly(1))->method('encode')->with($result);

        $this->assertNull($this->view->display());
    }
}
