<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Model\ErrorData;
use Sdk\ResourceCatalog\Translator\ErrorDataTranslator;

class ErrorDataViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockErrorDataView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetErrorData()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Model\ErrorData',
            $this->view->getErrorData()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\ErrorDataTranslator',
            $this->view->getTranslator()
        );
    }

    public function testGetData()
    {
        $this->view = $this->getMockBuilder(MockErrorDataView::class)
            ->setMethods(['getTranslator','getErrorData','getFormatErrorData'])
            ->getMock();
        $errorData = new ErrorData();
        $errorDataArray = [
            'itemsData' => ['id' => 1],
            'template' => ['items' => ['id' => 1]],
        ];

        $result = [
            'itemsData' => []
        ];

        $translator = $this->prophesize(ErrorDataTranslator::class);
        $translator->objectToArray(
            $errorData,
            array(
                'id',
                'category',
                'template',
                'itemsData',
                'errorReason',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($errorDataArray);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getErrorData')->willReturn($errorData);

        $this->view->expects($this->exactly(1))
        ->method('getFormatErrorData')
        ->with($errorDataArray)
        ->willReturn($result);

        $this->assertEquals($result, $this->view->getData());
    }


    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockErrorDataView::class)
            ->setMethods(['getData','getErrorDataDetail','encode'])
            ->getMock();
        $this->view->expects($this->exactly(1))->method('getData')->willReturn([]);
        $this->view->expects($this->exactly(1))->method('getErrorDataDetail')->with([])->willReturn([]);
        $this->view->expects($this->exactly(1))->method('encode')->with([]);

        $this->assertNull($this->view->display());

//        $this->view = $this->getMockBuilder(MockErrorDataView::class)
//            ->setMethods(['getTranslator','getErrorData','encode','getFormatErrorData'])
//            ->getMock();
//
//        $news = new ErrorData();
//        $newsData = [
//            'itemsData' => ['id' => 1],
//            'template' => ['id' => 1],
//        ];
//
//        $result = [
//            'itemsData' => []
//        ];
//
//        $translator = $this->prophesize(ErrorDataTranslator::class);
//        $translator->objectToArray(
//            $news
//        )->shouldBeCalledTimes(1)->willReturn($newsData);
//        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
//        $this->view->expects($this->exactly(1))->method('getErrorData')->willReturn($news);
//        $this->view->expects($this->exactly(1))->method('getData')->willReturn($result);

//        $this->view->expects($this->exactly(1))
//            ->method('getFormatErrorData')
//            ->with($newsData['itemsData'],$newsData['template'])
//            ->willReturn([]);
//
//        $this->view->expects($this->exactly(1))->method('encode')->with($result);
//
//        $this->assertNull($this->view->display());
    }
}
