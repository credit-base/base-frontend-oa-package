<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;

class ErrorDataEditViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockErrorDataEditView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockErrorDataEditView::class)
            ->setMethods(['getData','encode'])->getMock();

        $this->view->expects($this->exactly(1))->method('getData')->willReturn([]);
        $this->view->expects($this->exactly(1))->method('encode')->with([]);
        $this->assertNull(
            $this->view->display()
        );
    }
}
