<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\WbjTemplateTranslator;
use Sdk\Template\Model\WbjTemplate;

class WbjTemplateListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockWbjTemplateListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetCommonWbjTemplateList()
    {
        $this->assertIsArray($this->view->getList());
    }

    public function testGetCommonWbjTemplateCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetWbjTemplateTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\WbjTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testCommonWbjTemplateDisplay()
    {
        $this->view = $this->getMockBuilder(MockWbjTemplateListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $commonWbjTemplate = new WbjTemplate();
        $commonWbjTemplateData = [];
        $commonWbjTemplates = [$commonWbjTemplate];
        $count = 1;

        $translator = $this->prophesize(WbjTemplateTranslator::class);
        $translator->objectToArray($commonWbjTemplate)->shouldBeCalledTimes(1)->willReturn($commonWbjTemplateData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getList')->willReturn($commonWbjTemplates);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$commonWbjTemplateData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
