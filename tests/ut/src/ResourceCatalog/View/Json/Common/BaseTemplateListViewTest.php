<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\BaseTemplateTranslator;
use Sdk\Template\Model\BaseTemplate;

class BaseTemplateListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockBaseTemplateListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetCommonBaseTemplateList()
    {
        $this->assertIsArray($this->view->getList());
    }

    public function testGetCommonBaseTemplateCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetBaseTemplateTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\BaseTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testCommonBaseTemplateDisplay()
    {
        $this->view = $this->getMockBuilder(MockBaseTemplateListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $commonBaseTemplate = new BaseTemplate();
        $commonBaseTemplateData = [];
        $commonBaseTemplates = [$commonBaseTemplate];
        $count = 1;

        $translator = $this->prophesize(BaseTemplateTranslator::class);
        $translator->objectToArray($commonBaseTemplate)->shouldBeCalledTimes(1)->willReturn($commonBaseTemplateData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('getList')->willReturn($commonBaseTemplates);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$commonBaseTemplateData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
