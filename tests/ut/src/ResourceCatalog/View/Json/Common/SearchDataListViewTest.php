<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;
use Sdk\ResourceCatalog\Model\BjSearchData;

class SearchDataListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockSearchDataListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetBjSearchData()
    {
        $this->assertIsArray($this->view->getBjSearchData());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\BjSearchDataTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockSearchDataListView::class)
            ->setMethods(['getTranslator', 'getBjSearchData', 'encode','getCount'])
            ->getMock();

        $new = new BjSearchData();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(BjSearchDataTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                    'id',
                    'name',
                    'identify',
                    'dimension',
                    'infoClassify',
                    'sourceUnit',
                    'template'=>['id', 'name', 'identify',],
                    'status',
                    'updateTime',
                )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getBjSearchData')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
