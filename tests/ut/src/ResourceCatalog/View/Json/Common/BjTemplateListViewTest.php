<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\BjTemplateTranslator;
use Sdk\Template\Model\BjTemplate;

class BjTemplateListViewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockBjTemplateListView();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCommonBjTemplateList()
    {
        $this->assertIsArray($this->stub->getList());
    }

    public function testGetCommonBjTemplateCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetBjTemplateTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\BjTemplateTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testCommonBjTemplateDisplay()
    {
        $this->stub = $this->getMockBuilder(MockBjTemplateListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $commonBjTemplate = new BjTemplate();
        $commonBjTemplateData = [];
        $commonBjTemplates = [$commonBjTemplate];
        $count = 1;

        $translator = $this->prophesize(BjTemplateTranslator::class);
        $translator->objectToArray($commonBjTemplate)->shouldBeCalledTimes(1)->willReturn($commonBjTemplateData);
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($commonBjTemplates);
        $this->stub->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$commonBjTemplateData]
            ]
        );

        $this->assertNull($this->stub->display());
    }
}
