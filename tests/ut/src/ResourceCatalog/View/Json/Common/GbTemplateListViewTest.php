<?php
namespace Base\Package\ResourceCatalog\View\Json\Common;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Translator\GbTemplateTranslator;
use Sdk\Template\Model\GbTemplate;

class GbTemplateListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockGbTemplateListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetCommonGbTemplateList()
    {
        $this->assertIsArray($this->view->getList());
    }

    public function testGetCommonGbTemplateCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetGbTemplateTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Template\Translator\GbTemplateTranslator',
            $this->view->getTranslator()
        );
    }

    public function testCommonGbTemplateDisplay()
    {
        $this->view = $this->getMockBuilder(MockGbTemplateListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $commonGbTemplate = new GbTemplate();
        $commonGbTemplateData = [];
        $commonGbTemplates = [$commonGbTemplate];
        $count = 1;

        $translator = $this->prophesize(GbTemplateTranslator::class);
        $translator->objectToArray($commonGbTemplate)->shouldBeCalledTimes(1)->willReturn($commonGbTemplateData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getList')->willReturn($commonGbTemplates);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$commonGbTemplateData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
