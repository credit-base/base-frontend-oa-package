<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\ErrorDataTranslator;
use Sdk\ResourceCatalog\Model\ErrorData;

class ErrorDataListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockErrorDataListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetList()
    {
        $this->assertIsArray($this->view->getErrorDataList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\ErrorDataTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockErrorDataListView::class)
            ->setMethods(['getTranslator', 'getErrorDataList', 'encode','getCount','getResultErrorDataList'])
            ->getMock();

        $errorData = new ErrorData();
        $errorDataData[] = [
            'id' => 'MA',
            'taskData' => []
        ];
        $errorDatas = [$errorData];
        $count = 1;

        $translator = $this->prophesize(ErrorDataTranslator::class);
        $translator->objectToArray(
            $errorData,
            array(
                    'id',
                    'category',
                    'template',
                    'itemsData',
                    'errorReason',
                    'taskData',
                    'status',
                    'createTime',
                    'updateTime',
                    'statusTime'
                )
        )->shouldBeCalledTimes(1)->willReturn($errorDataData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getResultErrorDataList')->willReturn($errorDataData);
        $this->view->expects($this->exactly(1))->method('getErrorDataList')->willReturn($errorDatas);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => $errorDataData,
                'taskData' => $errorDataData[0]['taskData']
            ]
        );

        $this->assertNull($this->view->display());
    }

    public function testGetErrorDataListToReason()
    {
        $list = array(
            [
                'errorReason' => 'errorReason'
            ]
        );

        $string = 'errorReason';

        $this->assertEquals($string, $this->view->getErrorDataListToReason($list));
    }

    public function testGetResultErrorDataList()
    {
        $this->view = $this->getMockBuilder(MockErrorDataListView::class)
            ->setMethods(['getErrorDataListToReason','getFormatErrorData'])
            ->getMock();

        $formatErrorData = 'errorData';

        $itemsData = ['id' =>1];

        $list = [
            [
                'itemsData' => $itemsData,
                'template' => [],
                'items' => [],
            ]
        ];

        $this->view->expects($this->exactly(1))
            ->method('getFormatErrorData')
            ->with($list[0])
            ->willReturn($list[0]);
        $this->view->expects($this->exactly(1))
            ->method('getErrorDataListToReason')
            ->with($itemsData)
            ->willReturn($formatErrorData);

        $result = [
            [
                'errorReason' => $formatErrorData
            ]
        ];

        $this->assertEquals($result, $this->view->getResultErrorDataList($list));
    }
}
