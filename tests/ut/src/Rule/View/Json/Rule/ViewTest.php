<?php
namespace Base\Package\Rule\View\Json\Rule;

use PHPUnit\Framework\TestCase;
use Sdk\Rule\Model\Rule;
use Sdk\Rule\Model\UnAuditRule;
use Sdk\Rule\Translator\RuleTranslator;
use Sdk\Rule\Translator\UnAuditRuleTranslator;

class ViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetRule()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\Model\Rule',
            $this->view->getRule()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\Translator\RuleTranslator',
            $this->view->getTranslator()
        );
    }

    public function testGetUnAuditRuleTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\Translator\UnAuditRuleTranslator',
            $this->view->getUnAuditRuleTranslator()
        );
    }

    public function testGetVersionRecode()
    {
        $this->assertIsArray($this->view->getVersionRecode());
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(
                [
                    'getTranslator',
                    'getRule',
                    'encode',
                    'getVersionRecode',
                    'getRulesContent',
                    'getRuleVersionRecode'
                ]
            )
            ->getMock();

        $news = new Rule();
        $newsData = [
            'rules' => [
                'id' => 1
            ],
            'transformationTemplate' => [],
            'sourceTemplate' => [],
            'testResult' => '暂无',
            'versionRecode' => [],
            'content' => []
        ];

        $translator = $this->prophesize(RuleTranslator::class);
        $translator->objectToArray(
            $news,
            array(
                'id',
                'rules',
                'dataTotal',
                'version',
                'transformationCategory',
                'sourceCategory',
                'sourceTemplate'=>[],
                'transformationTemplate'=>[],
                'crew'=>['id','realName'],
                'userGroup'=>['id','name'],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsData);


        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getRule')->willReturn($news);

        $this->view->expects($this->exactly(1))->method('getRulesContent')->with(
            $newsData['rules'],
            $newsData['transformationTemplate'],
            $newsData['sourceTemplate']
        )->willReturn($newsData['content']);
        unset($newsData['rules']);
        $this->view->expects($this->exactly(1))->method('getVersionRecode')->willReturn(['1']);
        $this->view->expects($this->exactly(1))->method('getRuleVersionRecode')
            ->willReturn($newsData['versionRecode']);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);

        $this->assertNull($this->view->display());
    }

    public function testGetRuleVersionRecode()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(['getUnAuditRuleTranslator', 'getVersionRecode'])
            ->getMock();

        $rule = new UnAuditRule();
        $getVersionRecode = [$rule];
        $data = [];
        $list[] = $data;


        $translator = $this->prophesize(UnAuditRuleTranslator::class);
        $translator->objectToArray(
            $rule,
            array(
                'id',
                'version',
                'crew'=>['id','realName','category'],
                'updateTime',
                'userGroup'=>['id','name']
            )
        )->shouldBeCalledTimes(1)->willReturn($data);
        $this->view->expects($this->exactly(1))
            ->method('getUnAuditRuleTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))
            ->method('getVersionRecode')->willReturn($getVersionRecode);

        $versionRecode = [
            'total'=>count($list),
            'list' => $list
        ];

        $this->assertEquals(
            $versionRecode,
            $this->view->getRuleVersionRecode()
        );
    }
}
