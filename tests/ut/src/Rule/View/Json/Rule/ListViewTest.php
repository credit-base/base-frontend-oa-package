<?php
namespace Base\Package\Rule\View\Json\Rule;

use PHPUnit\Framework\TestCase;
use Sdk\Rule\Translator\RuleTranslator;
use Sdk\Rule\Model\Rule;

class ListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetRuleList()
    {
        $this->assertIsArray($this->view->getRuleList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\Translator\RuleTranslator',
            $this->view->getRuleTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getRuleTranslator', 'getRuleList', 'encode','getCount'])
            ->getMock();

        $new = new Rule();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(RuleTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                'id',
                'version',
                'dataTotal',
                'updateTime',
                'transformationCategory',
                'sourceCategory',
                'sourceTemplate'=>['id','name'],
                'transformationTemplate'=>['id','name'],
                'userGroup'=>['id','name']
            )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getRuleTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getRuleList')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
