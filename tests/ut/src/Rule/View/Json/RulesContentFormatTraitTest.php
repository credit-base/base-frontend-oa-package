<?php

namespace Base\Package\Rule\View\Json;

use Base\Sdk\Rule\Model\IRule;
use PHPUnit\Framework\TestCase;

class RulesContentFormatTraitTest extends TestCase
{
    private $trait;
    public function setUp()
    {
        $this->trait = new MockRulesContentFormatTrait();
    }
    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetRulesContentWithComparisonRule()
    {
        $rules['comparisonRule'] = [];
        $sourceTemplate = [];
        $transformationTemplate['items'] = [];

        $this->trait = $this->getMockBuilder(MockRulesContentFormatTrait::class)
            ->setMethods(['getCompletionRuleFormat'])
            ->getMock();

        $this->trait->expects($this->exactly(1))
            ->method('getCompletionRuleFormat')
            ->with(
                $rules['comparisonRule'],
                $transformationTemplate['items']
            )->willReturn($rules['comparisonRule']);

        $this->assertEquals(
            $rules,
            $this->trait->getRulesContentPublic($rules, $transformationTemplate, $sourceTemplate)
        );
    }

    public function testGetRulesContent()
    {
        $rules['transformationRule'] = [];
        $rules['completionRule'] = [];
        $rules['deDuplicationRule'] = [];
        //$rules['comparisonRule'] = [];

        $sourceTemplate = [];
        $transformationTemplate['items'] = [];

        $this->trait = $this->getMockBuilder(MockRulesContentFormatTrait::class)
            ->setMethods(['getTransformationRuleFormat','getCompletionRuleFormat','getDeDuplicationRuleFormat'])
            ->getMock();

        $this->trait->expects($this->exactly(1))
            ->method('getTransformationRuleFormat')
            ->with(
                $rules['transformationRule'],
                $transformationTemplate['items'],
                $sourceTemplate
            )->willReturn($rules['transformationRule']);

        $this->trait->expects($this->exactly(1))
            ->method('getCompletionRuleFormat')
            ->with(
                $rules['completionRule'],
                $transformationTemplate['items']
            )->willReturn($rules['completionRule']);

        $this->trait->expects($this->exactly(1))
            ->method('getDeDuplicationRuleFormat')
            ->with(
                $rules['deDuplicationRule'],
                $transformationTemplate['items']
            )->willReturn($rules['deDuplicationRule']);

        $this->assertEquals(
            $rules,
            $this->trait->getRulesContentPublic($rules, $transformationTemplate, $sourceTemplate)
        );
    }

    public function testGetTransformationRuleFormat()
    {
        $transformationRule = ['identify' => 'identify'];

        $transformationTemplateItems[] = ['identify' => 'identify' ,'name' => 'name'];
        $sourceTemplate['items'][] = ['identify' => 'identify' ,'name' => 'name'];
        $sourceTemplate['id'] = 1;
        $sourceTemplate['name'] = 'names';

        $sourceTemplate = [
            'id' => 1,
            'name' => 'name',
            'items' => array(
                ['identify' => 'identify' ,'name' => 'name']
            )
        ];

        $result[] = [
            'target' => ['identify' => 'identify' ,'name' => 'name'],
            'origin' => ['identify' => 'identify' ,'name' => 'name'],
            'template' => ['id' => 1 ,'name' => 'name'],
        ];

        $this->assertEquals(
            $result,
            $this->trait->getTransformationRuleFormatPublic(
                $transformationRule,
                $transformationTemplateItems,
                $sourceTemplate
            )
        );
    }

    public function testGetCompletionRuleFormat()
    {
        $completionRule = [
            'identify' => 'identify'
        ];
        $templateItems = [];

        $data = [];

        $this->trait = $this->getMockBuilder(MockRulesContentFormatTrait::class)
            ->setMethods([
                'getItemData'
            ])
            ->getMock();

        $this->trait->expects($this->exactly(1))->method('getItemData')
            ->with('identify', $completionRule['identify'])
            ->willReturn($data);

        $result[] = $data;

        $this->assertEquals($result, $this->trait->getCompletionRuleFormatPublic($completionRule, $templateItems));
    }

    public function testGetItemData()
    {
        $this->trait = $this->getMockBuilder(MockRulesContentFormatTrait::class)
            ->setMethods([
                'getDataByIdentify',
                'getFormatDataByBase'
            ])
            ->getMock();

        $targetIdentify = 'targetIdentify';
        $items[] = [
            'id' => 1,
            'name' => 'name',
            'item' => 'item',
            'itemName' => 'itemName',
            'base' => [],
        ];
        $targetTemplate = [];

        $getDataByIdentify = array(
            'id' => marmot_encode($items[0]['id']),
            'name' => $items[0]['name'],
        );
        $getFormatDataByBase = array(
            $items[0]['base']
        );
        $originData = array(
            'identify' => $items[0]['item'],
            'name' => $items[0]['itemName'],
        );
        $templateData = array(
            'id'=> marmot_encode($items[0]['id']),
            'name' => $items[0]['name'],
        );

        $this->trait->expects($this->exactly(1))->method('getDataByIdentify')
            ->with($targetIdentify, $targetTemplate)
            ->willReturn($getDataByIdentify);
        $this->trait->expects($this->exactly(1))->method('getFormatDataByBase')
            ->with($items[0]['base'])
            ->willReturn($getFormatDataByBase);

        $resultItems[] = [
            'target' => $getDataByIdentify,
            'origin' => $originData,
            'sourceCategory'=> "国标",
            'base' => $getFormatDataByBase,
            'template' => $templateData,
        ];

        $this->assertEquals(
            $resultItems,
            $this->trait->getItemDataPublic($targetIdentify, $items, $targetTemplate)
        );
    }

    public function testGetFormatDataByBase()
    {
        $value = 1;
        $base[] = $value;

        $result[] = [
            'id' => marmot_encode($value),
            'name' => IRule::COMPLETION_BASE_CN[$value],
        ];

        $this->assertEquals(
            $result,
            $this->trait->getFormatDataByBasePublic($base)
        );
    }

    public function testGetDeDuplicationRuleFormatPublic()
    {
        $this->trait = $this->getMockBuilder(MockRulesContentFormatTrait::class)
            ->setMethods([
                'getDataByIdentify'
            ])
            ->getMock();

        $templateItems = [];

        $deDuplicationRule['result'] = IRule::DE_DUPLICATION_RESULT['DISCARD'];
        $deDuplicationRule['items'] = ['Identify'];


        $result['result'] = [
            'id'=> marmot_encode($deDuplicationRule['result']),
            'name'=> IRule::DE_DUPLICATION_RESULT_CN[$deDuplicationRule['result']]
        ];

        $this->trait->expects($this->exactly(1))->method('getDataByIdentify')
            ->with($deDuplicationRule['items'][0], $templateItems)
            ->willReturn([]);

        $result['items'][] = [];

        $this->assertEquals(
            $result,
            $this->trait->getDeDuplicationRuleFormatPublic($deDuplicationRule, $templateItems)
        );
    }
}
