<?php
namespace Base\Package\Rule\View\Json\UnAuditRule;

use PHPUnit\Framework\TestCase;
use Sdk\Rule\Model\UnAuditRule;
use Sdk\Rule\Translator\UnAuditRuleTranslator;
use Sdk\Rule\Model\Rule;

class ListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetUnAuditRuleList()
    {
        $this->assertIsArray($this->view->getUnAuditRuleList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetUnAuditRuleTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\Translator\UnAuditRuleTranslator',
            $this->view->getUnAuditRuleTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getUnAuditRuleTranslator', 'getUnAuditRuleList', 'encode','getCount'])
            ->getMock();

        $new = new UnAuditRule();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(UnAuditRuleTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                'id',
                'version',
                'dataTotal',
                'updateTime',
                'applyStatus',
                'rejectReason',
                'transformationCategory',
                'sourceCategory',
                'sourceTemplate'=>['id','name'],
                'transformationTemplate'=>['id','name'],
                'userGroup'=>['id','name']
            )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getUnAuditRuleTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getUnAuditRuleList')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
