<?php
namespace Base\Package\Rule\View\Json\UnAuditRule;

use PHPUnit\Framework\TestCase;

use Sdk\Rule\Model\UnAuditRule;
use Sdk\Rule\Translator\UnAuditRuleTranslator;

class ViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetUnAuditRule()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\Model\UnAuditRule',
            $this->view->getUnAuditRule()
        );
    }

    public function testGetUnAuditTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\Translator\UnAuditRuleTranslator',
            $this->view->getUnAuditRuleTranslator()
        );
    }

    public function testGetVersionRecode()
    {
        $this->assertIsArray($this->view->getVersionRecode());
    }

    public function testGetUnAuditedInformation()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(
                [
                    'getUnAuditRuleTranslator',
                    'getVersionRecode'
                ]
            )
            ->getMock();

        $rule = new UnAuditRule();
        $getVersionRecode = [$rule];
        $data = [];
        $list[] = $data;


        $translator = $this->prophesize(UnAuditRuleTranslator::class);
        $translator->objectToArray(
            $rule,
            array(
                'id',
                'version',
                'rejectReason',
                'applyStatus',
                'crew'=>['id','realName','category'],
                'updateTime',
                'userGroup'=>['id','name']
            )
        )->shouldBeCalledTimes(1)->willReturn($data);
        $this->view->expects($this->exactly(1))
            ->method('getUnAuditRuleTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))
            ->method('getVersionRecode')->willReturn($getVersionRecode);

        $versionRecode = [
            'total'=>count($list),
            'list' => $list
        ];

        $this->assertEquals(
            $versionRecode,
            $this->view->getUnAuditedInformation()
        );
    }


    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(
                [
                    'getUnAuditRuleTranslator',
                    'getUnAuditRule',
                    'encode',
                    'getVersionRecode',
                    'getRulesContent',
                    'getUnAuditedInformation'
                ]
            )
            ->getMock();

        $news = new UnAuditRule();
        $newsData = [
            'rules' => [
                'id' => 1
            ],
            'transformationTemplate' => [],
            'sourceTemplate' => [],
            'testResult' => '暂无',
            'versionRecode' => [],
            'content' => [],
            'unAuditedInformation' => []
        ];

        $translator = $this->prophesize(UnAuditRuleTranslator::class);
        $translator->objectToArray(
            $news,
            array(
                'id',
                'rules',
                'dataTotal',
                'version',
                'applyStatus',
                'operationType',
                'relationId',
                'rejectReason',
                'transformationCategory',
                'sourceCategory',
                'sourceTemplate'=>[],
                'transformationTemplate'=>[],
                'applyCrew'=>['id','realName'],
                'crew'=>['id','realName'],
                'userGroup'=>['id','name'],
                'applyUserGroup'=>['id','name'],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsData);


        $this->view->expects($this->exactly(1))
            ->method('getUnAuditRuleTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))
            ->method('getUnAuditRule')->willReturn($news);

        $this->view->expects($this->exactly(1))->method('getRulesContent')->with(
            $newsData['rules'],
            $newsData['transformationTemplate'],
            $newsData['sourceTemplate']
        )->willReturn($newsData['content']);
        unset($newsData['rules']);
        $this->view->expects($this->exactly(1))
            ->method('getVersionRecode')->willReturn(['1']);
        $this->view->expects($this->exactly(1))
            ->method('getUnAuditedInformation')->willReturn($newsData['unAuditedInformation']);

        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);

        $this->assertNull($this->view->display());
    }
}
