<?php

namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\CommandBus;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Sdk\Rule\Command\UnAuditRule\RevokeUnAuditRuleCommand;

class UnAuditRuleRevokeControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockUnAuditRuleRevokeController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->controller->getCommandBus()
        );
    }

    public function testRevokeAction()
    {
        $this->controller = $this->getMockBuilder(MockUnAuditRuleRevokeController::class)
            ->setMethods(['getCommandBus'])->getMock();
        $id = 1;

        $commandBus = $this->prophesize(CommandBus::class);

        $commandBus->send(
            Argument::exact(
                new RevokeUnAuditRuleCommand($id)
            )
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $this->assertTrue($this->controller->revokeAction($id));
    }
}
