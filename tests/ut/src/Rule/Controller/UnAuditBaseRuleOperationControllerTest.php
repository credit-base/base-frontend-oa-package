<?php

namespace Base\Package\Rule\Controller;

use PHPUnit\Framework\TestCase;

class UnAuditBaseRuleOperationControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockUnAuditBaseRuleOperationController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testAddAction()
    {
        $this->controller = $this->getMockBuilder(MockUnAuditBaseRuleOperationController::class)
            ->setMethods(['displayError'])->getMock();
        $this->controller->expects($this->exactly(1))->method('displayError')->willReturn(false);
        $this->assertFalse($this->controller->addAction());
    }

    public function testEditAction()
    {
        $this->controller = $this->getMockBuilder(MockUnAuditBaseRuleOperationController::class)
            ->setMethods(['unAuditRuleEdit'])->getMock();
        $id = 1;
        $this->controller->expects($this->exactly(1))
            ->method('unAuditRuleEdit')->with($id)->willReturn(true);
        $this->assertTrue($this->controller->editAction($id));
    }
}
