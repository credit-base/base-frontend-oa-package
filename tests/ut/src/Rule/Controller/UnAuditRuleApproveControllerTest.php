<?php

namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Request;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Sdk\Rule\Command\UnAuditRule\ApproveUnAuditRuleCommand;
use Sdk\Rule\Command\UnAuditRule\RejectUnAuditRuleCommand;

class UnAuditRuleApproveControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockUnAuditRuleApproveController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->controller->getCommandBus()
        );
    }

    public function testApproveAction()
    {
        $this->controller = $this->getMockBuilder(MockUnAuditRuleApproveController::class)
            ->setMethods(['getCommandBus'])->getMock();
        $id = 1;

        $commandBus = $this->prophesize(CommandBus::class);

        $commandBus->send(
            Argument::exact(
                new ApproveUnAuditRuleCommand($id)
            )
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $this->assertTrue($this->controller->approveAction($id));
    }

    private function initRejectAction($result)
    {
        $this->controller = $this->getMockBuilder(MockUnAuditRuleApproveController::class)
            ->setMethods(['getCommandBus','getRequest','validateRejectScenario'])->getMock();
        $id = 1;
        $rejectReason = 'rejectReason';

        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('rejectReason'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($rejectReason);
        $this->controller->expects($this->any())->method('getRequest')->willReturn($request->reveal());



        $this->controller->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn($result);

        if ($result) {
            $commandBus = $this->prophesize(CommandBus::class);
            $commandBus->send(
                Argument::exact(new RejectUnAuditRuleCommand($rejectReason, $id))
            )->shouldBeCalledTimes(1)->willReturn(true);
            $this->controller->expects($this->exactly(1))
                ->method('getCommandBus')
                ->willReturn($commandBus->reveal());
            $this->assertTrue($this->controller->rejectAction($id));
        }
        if (!$result) {
            $this->assertFalse($this->controller->rejectAction($id));
        }
    }

    public function testRejectAction()
    {
        $this->initRejectAction(true);
        $this->initRejectAction(false);
    }
}
