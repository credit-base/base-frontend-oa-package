<?php

namespace Base\Package\Rule\Controller;

use Marmot\Basecode\Classes\CommandBus;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Sdk\Rule\Command\Rule\AddRuleCommand;
use Sdk\Rule\Command\Rule\EditRuleCommand;

class RuleOperationControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockRuleOperationController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testAddView()
    {
        $this->controller = $this->getMockBuilder(MockRuleOperationController::class)
            ->setMethods(['displayError'])->getMock();
        $this->controller->expects($this->exactly(1))->method('displayError')->willReturn(false);
        $this->assertFalse($this->controller->addView());
    }

    public function testEditView()
    {
        $id = mt_rand(1, 1000);
        $this->controller = $this->getMockBuilder(MockRuleOperationController::class)
            ->setMethods(['displayError'])->getMock();
        $this->controller->expects($this->exactly(1))->method('displayError')->willReturn(false);
        $this->assertFalse($this->controller->editView($id));
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->controller->getCommandBus()
        );
    }

    public function initAddAction($result)
    {
        $this->controller = $this->getMockBuilder(MockRuleOperationController::class)
            ->setMethods([
                'getRequestCommonData',
                'validateAddScenario',
                'getCommandBus',
                'displaySuccess',
                'displayError'
            ])->getMock();

        $requestData['rules'] = array();
        $requestData['transformationTemplate'] = 1;
        $requestData['sourceTemplate'] = 1;
        $requestData['transformationCategory'] = 1;
        $requestData['sourceCategory'] = 1;

        $this->controller->expects($this->exactly(1))
            ->method('getRequestCommonData')->willReturn($requestData);

        $this->controller->expects($this->exactly(1))
            ->method('validateAddScenario')
            ->with(
                $requestData['rules'],
                $requestData['transformationTemplate'],
                $requestData['sourceTemplate'],
                $requestData['transformationCategory'],
                $requestData['sourceCategory']
            )
            ->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact(new AddRuleCommand(
            $requestData['rules'],
            $requestData['transformationTemplate'],
            $requestData['sourceTemplate'],
            $requestData['transformationCategory'],
            $requestData['sourceCategory']
        )))->shouldBeCalledTimes(1)->willReturn($result);
        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')->willReturn($commandBus->reveal());

        if ($result) {
            $this->controller->expects($this->exactly(1))
                ->method('displaySuccess')->willReturn($result);
            $this->assertTrue($this->controller->addAction());
        }
        if (!$result) {
            $this->controller->expects($this->exactly(1))
                ->method('displayError')->willReturn($result);
            $this->assertFalse($this->controller->addAction());
        }
    }
    public function testAddAction()
    {
        $this->initAddAction(true);
        $this->initAddAction(false);
    }

    public function initEditAction($result)
    {
        $id = 1;
        $this->controller = $this->getMockBuilder(MockRuleOperationController::class)
            ->setMethods([
                'getRequestCommonData',
                'validateEditScenario',
                'getCommandBus',
                'displaySuccess',
                'displayError'
            ])->getMock();

        $requestData['rules'] = array();

        $this->controller->expects($this->exactly(1))
            ->method('getRequestCommonData')->willReturn($requestData);

        $this->controller->expects($this->exactly(1))
            ->method('validateEditScenario')->with($requestData['rules'])->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact(new EditRuleCommand($requestData['rules'], $id)))
            ->shouldBeCalledTimes(1)->willReturn($result);
        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')->willReturn($commandBus->reveal());

        if ($result) {
            $this->controller->expects($this->exactly(1))
                ->method('displaySuccess')->willReturn($result);
            $this->assertTrue($this->controller->editAction($id));
        }
        if (!$result) {
            $this->controller->expects($this->exactly(1))
                ->method('displayError')->willReturn($result);
            $this->assertFalse($this->controller->editAction($id));
        }
    }

    public function testEditAction()
    {
        $this->initEditAction(true);
        $this->initEditAction(false);
    }
}
