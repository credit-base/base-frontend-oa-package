<?php

namespace Base\Package\Rule\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;
use Sdk\Rule\Command\UnAuditRule\EditUnAuditRuleCommand;

/**
 * Class UnAuditRuleOperationControllerTest
 * @package Base\Package\Rule\Controller
 * @SuppressWarnings(TooManyPublicMethods)
 */
class UnAuditRuleOperationControllerTest extends TestCase
{
    private $controller;
    private $request;

    public function setUp()
    {
        $this->controller = new MockUnAuditRuleOperationController();
        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->request);
    }

    public function testAddAction()
    {
        $this->controller = $this->getMockBuilder(MockUnAuditRuleOperationController::class)
            ->setMethods(
                ['displayError']
            )->getMock();

        $this->controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $this->assertFalse($this->controller->addAction());
    }

    public function initEditAction($result)
    {
        $this->controller = $this->getMockBuilder(MockUnAuditRuleOperationController::class)
            ->setMethods(
                [
                    'validateEditScenario',
                    'getCommandBus',
                    'displaySuccess',
                    'displayError',
                    'getRulesDecode',
                    'getRequest'
                ]
            )->getMock();

        $id = 1;
        $transformationTemplateId = 1;
        $transformationCategory = 1;
        $sourceTemplateId = 1;
        $sourceCategory = 1;
        $rules = array(
            'id' => 1
        );

        $requestData = [
            'transformationTemplate' => $transformationTemplateId,
            'transformationCategory' => $transformationCategory,
            'sourceTemplate' => $sourceTemplateId,
            'sourceCategory' => $sourceCategory,
            'rules' => $rules,
        ];

        $this->request->post(Argument::exact('transformationTemplateId'), '')
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($transformationTemplateId));
        $this->request->post(Argument::exact('transformationCategory'), '')
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($transformationCategory));
        $this->request->post(Argument::exact('sourceTemplateId'), '')
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($sourceTemplateId));
        $this->request->post(Argument::exact('sourceCategory'), '')
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($sourceCategory));
        $this->request->post(Argument::exact('rules'), array())
            ->shouldBeCalledTimes(1)->willReturn($rules);
        $this->controller->expects($this->any())
            ->method('getRequest')
            ->willReturn($this->request->reveal());

        $this->controller->expects($this->exactly(1))
            ->method('getRulesDecode')
            ->with($rules)
            ->willReturn($rules);


        $this->controller->expects($this->exactly(1))
            ->method('validateEditScenario')
            ->with($requestData['rules'])
            ->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact(new EditUnAuditRuleCommand($rules, $id)))
            ->shouldBeCalledTimes(1)->willReturn($result);
        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')->willReturn($commandBus->reveal());

        if ($result) {
            $this->controller->expects($this->exactly(1))
                ->method('displaySuccess')->willReturn(true);
            $this->assertTrue($this->controller->editAction($id));
        }
        if (!$result) {
            $this->controller->expects($this->exactly(1))
                ->method('displayError')->willReturn(true);
            $this->assertFalse($this->controller->editAction($id));
        }
    }
    public function testEditAction()
    {
        $this->initEditAction(true);
    }
    public function testEditActionFailed()
    {
        $this->initEditAction(false);
    }

    public function testEditView()
    {
        $id = 1;

        $this->controller = $this->getMockBuilder(MockUnAuditRuleOperationController::class)
            ->setMethods(['displayError'])->getMock();

        $this->controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        $this->assertFalse($this->controller->editViews($id));
    }
    public function testAddView()
    {
        $this->controller = $this->getMockBuilder(MockUnAuditRuleOperationController::class)
            ->setMethods(['displayError'])->getMock();

        $this->controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        $this->assertFalse($this->controller->addViews());
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->controller->getCommandBus()
        );
    }

    public function testValidateRejectScenario()
    {
        $this->controller = new MockUnAuditRuleOperationController();
        $rejectReason = 'rejectReason';
        $this->assertTrue($this->controller->validateRejectScenarioPublic($rejectReason));
    }

    public function testValidateEditScenario()
    {
        $rule = [];

        $this->assertTrue($this->controller->validateEditScenario($rule));
    }

    public function testValidateAddScenario()
    {
        $rules = array();
        $transformationTemplate = 1;
        $sourceTemplate = 1;
        $transformationCategory = 1;
        $sourceCategory = 1;

        $this->assertTrue($this->controller->validateAddScenario(
            $rules,
            $transformationTemplate,
            $sourceTemplate,
            $transformationCategory,
            $sourceCategory
        ));
    }

    public function testGetRuleWidgetRules()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\WidgetRules\RuleWidgetRules',
            $this->controller->getRuleWidgetRules()
        );
    }
}
