<?php

namespace Base\Package\Rule\Controller;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Sdk\Crew\Model\Crew;
use Base\Sdk\Rule\Model\IRule;

class UnAuditBaseRuleFetchControllerTest extends TestCase
{
    private $controller;
    public function setUp()
    {
        $this->controller = new MockUnAuditBaseRuleFetchController();
    }
    public function tearDown()
    {
        unset($this->controller);
    }

    public function testFetchOneAction()
    {
        $id = 1;

        $this->controller = $this->getMockBuilder(MockUnAuditBaseRuleFetchController::class)
            ->setMethods(
                ['fetchUnAuditRuleOne']
            )->getMock();

        $this->controller->expects($this->exactly(1))
            ->method('fetchUnAuditRuleOne')
            ->with($id)
            ->willReturn(true);

        $this->assertTrue($this->controller->fetchOneAction($id));
    }

    public function testCheckUserHasPurview()
    {
        $type = IRule::CATEGORY['BASE_RULE'];
        $resource = 'unAuditedBaseRules';

        $this->controller = $this->getMockBuilder(MockUnAuditBaseRuleFetchController::class)
            ->setMethods(
                ['checkUserHasRulePurview']
            )->getMock();

        $this->controller->expects($this->exactly(1))
            ->method('checkUserHasRulePurview')
            ->with($resource, $type)
            ->willReturn(true);

        $this->assertTrue($this->controller->checkUserHasPurview($resource));
    }

    public function testFilterAction()
    {
        $this->controller = $this->getMockBuilder(MockUnAuditBaseRuleFetchController::class)
            ->setMethods(
                [
                    'fetchUnAuditRuleList'
                ]
            )->getMock();

        Core::$container->set('purview.status', 0);
        $this->controller->expects($this->exactly(1))
            ->method('fetchUnAuditRuleList')
            ->willReturn(true);

        $this->assertTrue($this->controller->filterAction());
    }

    public function testFilterActionFalse()
    {
        $this->controller = $this->getMockBuilder(MockUnAuditBaseRuleFetchController::class)
            ->setMethods(['checkUserHasPurview'])->getMock();

        Core::$container->set('purview.status', 1);

        $this->controller->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->with('unAuditedBaseRules')
            ->willReturn(false);

        $this->assertFalse($this->controller->filterAction());
    }
}
