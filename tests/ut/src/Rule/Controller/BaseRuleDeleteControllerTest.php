<?php

namespace Base\Package\Rule\Controller;

use Marmot\Framework\Classes\CommandBus;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Sdk\Rule\Command\Rule\DeleteRuleCommand;

class BaseRuleDeleteControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockBaseRuleDeleteController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->controller->getCommandBus()
        );
    }

    public function testDeleteAction()
    {
        $this->controller = $this->getMockBuilder(MockBaseRuleDeleteController::class)
            ->setMethods(['getCommandBus'])->getMock();
        $id = 1;

        $commandBus = $this->prophesize(CommandBus::class);

        $commandBus->send(
            Argument::exact(
                new DeleteRuleCommand($id)
            )
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $this->assertTrue($this->controller->deleteAction($id));
    }
}
