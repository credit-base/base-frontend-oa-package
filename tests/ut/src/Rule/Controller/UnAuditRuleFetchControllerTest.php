<?php

namespace Base\Package\Rule\Controller;

use Base\Package\Rule\View\Json\UnAuditRule\View;
use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Sdk\Crew\Model\Crew;
use Sdk\Rule\Model\NullUnAuditRule;
use Sdk\Rule\Model\UnAuditRule;
use Sdk\Rule\Repository\UnAuditRuleRepository;
use Base\Package\Rule\View\Json\UnAuditRule\ListView;

class UnAuditRuleFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockUnAuditRuleFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\Repository\UnAuditRuleRepository',
            $this->controller->getRepository()
        );
    }

    public function testCheckUserHasPurview()
    {
        $resource = 'unAuditedRules';
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setCategory(1);

        $this->assertTrue($this->controller->checkUserHasPurview($resource));
    }
    
    public function testCheckUserHasRulePurview()
    {
        $resource = 'unAuditedRules';
        $type = 3;
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setCategory(2);

        $this->assertFalse($this->controller->checkUserHasRulePurview($resource, $type));
    }

    public function testFetchOneAction()
    {
        $id = 1;
        $relationId = 1;
        $unAuditedInformation = [];

        $this->controller = $this->getMockBuilder(MockUnAuditRuleFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render',
                    'getUnAuditRuleList'
                ]
            )->getMock();

        $data = new UnAuditRule($id);
        $data->setRelationId($relationId);

        $repository = $this->prophesize(UnAuditRuleRepository::class);
        $repository->scenario(Argument::exact(UnAuditRuleRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->controller->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->controller->expects($this->exactly(1))
            ->method('getUnAuditRuleList')
            ->with($relationId, false)
            ->willReturn($unAuditedInformation);

        $this->controller->expects($this->exactly(1))
            ->method('render')
            ->with(new View($data, $unAuditedInformation));

        $result = $this->controller->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionFailed()
    {
        $id = 1;

        $this->controller = $this->getMockBuilder(MockUnAuditRuleFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $data = new NullUnAuditRule($id);

        $repository = $this->prophesize(UnAuditRuleRepository::class);
        $repository->scenario(Argument::exact(UnAuditRuleRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->controller->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->assertFalse($this->controller->fetchOneAction($id));
    }

    public function testFetchUnAuditRuleList()
    {
        $type = 0;
        $page = 1;
        $size = 10;

        $filter = [];
        $sort = [];

        $unAuditRuleList = [];
        $count = 0;

        $this->controller = $this->getMockBuilder(MockUnAuditRuleFetchController::class)
            ->setMethods(
                [
                    'getPageAndSize',
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $this->controller->expects($this->exactly(1))
            ->method('getPageAndSize')->willReturn([$size,$page]);
        $this->controller->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with($type)
            ->willReturn([$filter,$sort]);

        $repository = $this->prophesize(UnAuditRuleRepository::class);
        $repository->scenario(Argument::exact(UnAuditRuleRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$unAuditRuleList]);

        $this->controller->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->controller->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($unAuditRuleList, $count));

        $this->assertTrue($this->controller->fetchUnAuditRuleList($type));
    }

    public function testFilterActionFalse()
    {
        $this->controller = $this->getMockBuilder(MockUnAuditRuleFetchController::class)
            ->setMethods(
                [
                    'checkUserHasPurview',
                    'fetchUnAuditRuleList'
                ]
            )->getMock();

        Core::$container->set('purview.status', 1);

        $this->controller->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->with('unAuditedRules')
            ->willReturn(false);

        $this->assertFalse($this->controller->filterAction());
    }

    public function testFilterAction()
    {
        $this->controller = $this->getMockBuilder(MockUnAuditRuleFetchController::class)
            ->setMethods(
                [
                    'fetchUnAuditRuleList'
                ]
            )->getMock();

        Core::$container->set('purview.status', 0);
        $this->controller->expects($this->exactly(1))
            ->method('fetchUnAuditRuleList')
            ->willReturn(true);

        $this->assertTrue($this->controller->filterAction());
    }
}
