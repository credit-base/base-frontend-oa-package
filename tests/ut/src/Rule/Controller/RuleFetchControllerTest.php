<?php

namespace Base\Package\Rule\Controller;

use Base\Sdk\Rule\Model\IRule;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class RuleFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockRuleFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testFetchOneAction()
    {
        $id = 1;

        $this->controller = $this->getMockBuilder(MockRuleFetchController::class)
            ->setMethods(
                ['getRuleOne']
            )->getMock();

        $this->controller->expects($this->exactly(1))
            ->method('getRuleOne')
            ->with($id)
            ->willReturn(true);

        $this->assertTrue($this->controller->fetchOneAction($id));
    }

    public function testCheckUserHasPurview()
    {
        $resource = 'rules';
        $this->controller = $this->getMockBuilder(MockRuleFetchController::class)
            ->setMethods(['checkUserHasRulePurview'])->getMock();
        $this->controller->expects($this->exactly(1))
            ->method('checkUserHasRulePurview')->with($resource)->willReturn(true);
        $this->assertTrue($this->controller->checkUserHasPurview($resource));
    }

    public function testFilterAction()
    {
        $this->controller = $this->getMockBuilder(MockRuleFetchController::class)
            ->setMethods(
                [
                    'getRuleList'
                ]
            )->getMock();

        Core::$container->set('purview.status', 0);
        $this->controller->expects($this->exactly(1))
            ->method('getRuleList')
            ->willReturn(true);

        $this->assertTrue($this->controller->filterAction());
    }

    public function testFilterActionFalse()
    {
        $this->controller = $this->getMockBuilder(MockRuleFetchController::class)
            ->setMethods(['checkUserHasPurview'])->getMock();

        Core::$container->set('purview.status', 1);

        $this->controller->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->with('rules')
            ->willReturn(false);

        $this->assertFalse($this->controller->filterAction());
    }
}
