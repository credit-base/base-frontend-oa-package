<?php

namespace Base\Package\Rule\Controller;

use Base\Package\Rule\View\Json\Rule\View;
use Base\Package\Rule\View\Json\Rule\ListView;
use Base\Sdk\Rule\Model\IRule;
use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Sdk\Rule\Model\NullRule;
use Sdk\Rule\Model\Rule;
use Sdk\Rule\Repository\RuleRepository;

class BaseRuleFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockBaseRuleFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testFetchOneAction()
    {
        $this->controller = $this->getMockBuilder(MockBaseRuleFetchController::class)
            ->setMethods(['getRuleOne'])->getMock();
        $id = 2;

        $this->controller->expects($this->exactly(1))
            ->method('getRuleOne')->with($id)->willReturn(true);

        $this->assertTrue($this->controller->fetchOneAction($id));
    }

    public function testCheckUserHasPurview()
    {
        $type = IRule::CATEGORY['BASE_RULE'];
        $resource = 'unAuditedBaseRules';

        $this->controller = $this->getMockBuilder(MockBaseRuleFetchController::class)
            ->setMethods(
                ['checkUserHasRulePurview']
            )->getMock();

        $this->controller->expects($this->exactly(1))
            ->method('checkUserHasRulePurview')
            ->with($resource, $type)
            ->willReturn(true);

        $this->assertTrue($this->controller->checkUserHasPurview($resource));
    }

    public function testFilterAction()
    {
        $this->controller = $this->getMockBuilder(MockBaseRuleFetchController::class)
            ->setMethods(['getRuleList'])->getMock();
        Core::$container->set('purview.status', 0);
        $this->controller->expects($this->exactly(1))->method('getRuleList')->willReturn(true);
        $this->assertTrue($this->controller->filterAction());
    }

    public function testFilterActionFalse()
    {
        $this->controller = $this->getMockBuilder(MockBaseRuleFetchController::class)
            ->setMethods(['checkUserHasPurview'])->getMock();
        Core::$container->set('purview.status', 1);
        $this->controller->expects($this->exactly(1))
            ->method('checkUserHasPurview')->with('baseRules')->willReturn(false);

        $this->assertFalse($this->controller->filterAction());
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\Repository\RuleRepository',
            $this->controller->getRepository()
        );
    }

    public function testGetRuleList()
    {
        $this->controller = $this->getMockBuilder(MockBaseRuleFetchController::class)
            ->setMethods(['getPageAndSize','filterFormatChange','getRepository','render'])
            ->getMock();

        $type = 1;
        list($size,$page) = [10,1];
        list($filter, $sort) = [array(),array()];
        $count = 0;
        $list = [];

        $this->controller->expects($this->exactly(1))->method('getPageAndSize')
            ->willReturn([$size,$page]);
        $this->controller->expects($this->exactly(1))->method('filterFormatChange')
            ->with($type)
            ->willReturn([$filter,$sort]);

        $repository = $this->prophesize(RuleRepository::class);
        $repository->scenario(Argument::exact(RuleRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);
        $this->controller->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
        $this->controller->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($list, $count));

        $this->assertTrue($this->controller->getRuleList($type));
    }

    public function testGetRuleOneFalse()
    {
        $id = mt_rand(1, 99999);
        $nullRule = new NullRule($id);

        $this->controller = $this->getMockBuilder(MockBaseRuleFetchController::class)
            ->setMethods(['getRepository'])->getMock();
        $repository = $this->prophesize(RuleRepository::class);
        $repository->scenario(Argument::exact(RuleRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)->willReturn($nullRule);
        $this->controller->expects($this->exactly(1))
            ->method('getRepository')->willReturn($repository->reveal());

        $this->assertFalse($this->controller->getRuleOne($id));
    }

    public function testGetRuleOne()
    {
        $id = mt_rand(1, 99999);
        $rule = new Rule($id);
        $versionRecode = array();

        $this->controller = $this->getMockBuilder(MockBaseRuleFetchController::class)
            ->setMethods(['getUnAuditRuleList','getRepository','render'])
            ->getMock();

        $repository = $this->prophesize(RuleRepository::class);
        $repository->scenario(Argument::exact(RuleRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)->willReturn($rule);
        $this->controller->expects($this->exactly(1))
            ->method('getRepository')->willReturn($repository->reveal());

        $this->controller->expects($this->exactly(1))
            ->method('getUnAuditRuleList')
            ->with($id, true)
            ->willReturn($versionRecode);

        $this->controller->expects($this->exactly(1))
            ->method('render')
            ->with(new View($rule, $versionRecode))
            ->willReturn(true);

        $result = $this->controller->getRuleOne($id);
        $this->assertTrue($result);
    }
}
