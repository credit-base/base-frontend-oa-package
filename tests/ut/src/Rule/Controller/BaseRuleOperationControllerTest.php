<?php

namespace Base\Package\Rule\Controller;

use PHPUnit\Framework\TestCase;

class BaseRuleOperationControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockBaseRuleOperationController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testAddAction()
    {
        $this->controller = $this->getMockBuilder(MockBaseRuleOperationController::class)
            ->setMethods(['ruleAdd'])->getMock();

        $this->controller->expects($this->exactly(1))->method('ruleAdd')->willReturn(true);

        $this->assertTrue($this->controller->addAction());
    }

    public function testEditAction()
    {
        $id = 1;
        $this->controller = $this->getMockBuilder(MockBaseRuleOperationController::class)
            ->setMethods(['ruleEdit'])->getMock();

        $this->controller->expects($this->exactly(1))
            ->method('ruleEdit')
            ->with($id)
            ->willReturn(true);

        $this->assertTrue($this->controller->editAction($id));
    }
}
