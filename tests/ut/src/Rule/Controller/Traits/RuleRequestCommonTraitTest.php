<?php


namespace Base\Package\Rule\Controller\Traits;

use PHPUnit\Framework\TestCase;
use Base\Package\Rule\View\Json\Rule\View;
use Base\Package\Rule\View\Json\Rule\ListView;
use Prophecy\Argument;
use Sdk\Rule\Repository\RuleRepository;

class RuleRequestCommonTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockRuleRequestCommonTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetDeDuplicationRuleDecode()
    {
        $deDuplicationRule['result'] = 1;
        $result['result'] = marmot_decode($deDuplicationRule['result']);

        $this->assertEquals($result, $this->trait->getDeDuplicationRuleDecodePublic($deDuplicationRule));
    }

    public function testGetBaseDecode()
    {
        $base = [marmot_encode(1), marmot_encode(2), marmot_encode(3)];
        $result = [marmot_decode($base[0]), marmot_decode($base[1]), marmot_decode($base[2])];
        $this->assertEquals($result, $this->trait->getBaseDecodePublic($base));
    }

    public function testGetCompletionRuleDecode()
    {
        $id = 1;
        $base = [];
        $item = [];

        $completionRules[] = [
            'id' => marmot_encode($id),
            'base' => $base,
            'item' => $item
        ];

        $result[] = [
            'id' => $id,
            'base' => $base,
            'item' => $item
        ];

        $this->assertEquals($result, $this->trait->getCompletionRuleDecodePublic($completionRules));
    }

    public function testGetRulesDecodeByComparisonRule()
    {
        $this->trait = $this->getMockBuilder(MockRuleRequestCommonTrait::class)
            ->setMethods(['getCompletionRuleDecode'])->getMock();

        $comparisonRule = array(
            'id' => 'MA',
            'base' => [],
            'item' => []
        );
        $rules = [
            'comparisonRule' => array($comparisonRule),
        ];

        $comparisonRuleResult = array(
            'id' => 1,
            'base' => [],
            'item' => []
        );

        $rulesResult = [
            'comparisonRule' => array($comparisonRuleResult),
        ];

        $this->trait->expects($this->any())->method('getCompletionRuleDecode')
            ->with($comparisonRule)->willReturn($comparisonRuleResult);

        $this->assertEquals(
            $rulesResult,
            $this->trait->getRulesDecodePublic($rules)
        );
    }

    public function testGetRulesDecode()
    {
        $this->trait = $this->getMockBuilder(MockRuleRequestCommonTrait::class)
            ->setMethods(['getCompletionRuleDecode', 'getDeDuplicationRuleDecode'])->getMock();

        $completionRuleValue = array(
            'id' => 'MA',
            'base' => [],
            'item' => []
        );
        $deDuplicationRule = array(
            'result' => 'MA'
        );
        $rules = [
            'completionRule' => array(
                $completionRuleValue
            ),
            'deDuplicationRule' => $deDuplicationRule
        ];

        $completionRuleResult = array(
            'id' => 1,
            'base' => [],
            'item' => []
        );
        $deDuplicationRuleResult = ['result' => 1];
        $result = [
            'completionRule' => array($completionRuleResult),
            'deDuplicationRule' => $deDuplicationRuleResult
        ];

        $this->trait->expects($this->exactly(1))->method('getCompletionRuleDecode')
            ->with($completionRuleValue)->willReturn($completionRuleResult);

        $this->trait->expects($this->exactly(1))->method('getDeDuplicationRuleDecode')
            ->with($deDuplicationRule)->willReturn($deDuplicationRuleResult);

        $this->assertEquals($result, $this->trait->getRulesDecodePublic($rules));
    }
}
