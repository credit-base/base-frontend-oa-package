<?php


namespace Base\Package\Rule\Controller\Traits;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Rule\Model\IRule;
use Marmot\Core;
use Marmot\Framework\Classes\Request;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Sdk\Rule\Model\UnAuditRule;
use Sdk\Rule\Repository\UnAuditRuleRepository;

class RuleVersionRecodeTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockRuleVersionRecodeTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetUnAuditRuleRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Rule\Repository\UnAuditRuleRepository',
            $this->trait->publicGetUnAuditRuleRepository()
        );
    }

    public function testGetUnAuditRuleList()
    {
        $page = 1;
        $maxSize = 1000;
        $relationId = 1;
        $resourceType = 1;

        $filter = ['relationId' => $relationId,'applyStatus'=> UnAuditRule::APPLY_STATUS['APPROVE']];
        $sort = ['-updateTime'];

        $count = 0;
        $lists = [];

        $this->trait = $this->getMockBuilder(MockRuleVersionRecodeTrait::class)
            ->setMethods(['getUnAuditRuleRepository'])->getMock();

        $repository = $this->prophesize(UnAuditRuleRepository::class);
        $repository->scenario(Argument::exact(UnAuditRuleRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $this->trait->expects($this->exactly(1))
            ->method('getUnAuditRuleRepository')
            ->willReturn($repository->reveal());

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($maxSize)
        )->shouldBeCalledTimes(1)->willReturn([$count,$lists]);

        $this->assertEquals(
            $lists,
            $this->trait->publicGetUnAuditRuleList($relationId, $resourceType)
        );
    }

    public function testFilterFormatChange()
    {
        $this->initFilterFormatChange(1);
        $this->initFilterFormatChange(0);
    }

    public function initFilterFormatChange($type)
    {
        $this->trait = $this->getMockBuilder(MockRuleVersionRecodeTrait::class)
            ->setMethods(['getRequest'])->getMock();

        $transformationCategory = 1;
        $transformationTemplateId  = 1;
        $sourceCategory = 1;
        $sourceTemplateId = 1;
        $applyStatus = 1;
        $sort = '';

        $userGroupId = 1;
        $crew = new Crew(1);
        $crew->getUserGroup()->setId($userGroupId);
        Core::$container->set('crew', $crew);

        $request = $this->prophesize(Request::class);
        $request->get(Argument::exact('transformationCategory'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($transformationCategory));
        $request->get(Argument::exact('transformationTemplateId'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($transformationTemplateId));
        $request->get(Argument::exact('sourceCategory'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($sourceCategory));
        $request->get(Argument::exact('sourceTemplateId'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($sourceTemplateId));
        $request->get(Argument::exact('applyStatus'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($applyStatus));
        $request->get(Argument::exact('sort'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($sort);
        $this->trait->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $filter['status'] = IEnableAble::STATUS['ENABLED'];
        $filter['applyStatus'] = UnAuditRule::APPLY_STATUS['PENDING'].','.UnAuditRule::APPLY_STATUS['REJECT'];
        $filter['applyStatus'] = $applyStatus;
        $filter['userGroup'] = $userGroupId;

        if (empty($type)) {
            $filter['excludeTransformationCategory'] = IRule::CATEGORY['BASE_RULE'];
        }
        if (!empty($type)) {
            $filter['transformationCategory'] = $type;
        }
        if (empty($sort)) {
            $sort = '-updateTime';
        }

        $filter['transformationTemplate'] = $transformationTemplateId;
        $filter['transformationCategory'] = $transformationCategory;
        $filter['sourceCategory'] = $sourceCategory;
        $filter['sourceTemplate'] = $sourceTemplateId;
        $sort = [$sort];

        $result = [
            $filter, $sort
        ];

        $this->assertEquals($result, $this->trait->filterFormatChangePublic($type));
    }
}
