<?php
namespace Base\Package\UserGroup\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\Response;

use Sdk\UserGroup\Model\Department;
use Sdk\UserGroup\Model\NullDepartment;
use Sdk\UserGroup\Repository\DepartmentRepository;

use Base\Package\UserGroup\View\Json\DepartmentListView;
use Base\Package\UserGroup\View\Json\DepartmentView;

class DepartmentFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDepartmentFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new DepartmentFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\UserGroup\Repository\DepartmentRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockDepartmentFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $department = new NullDepartment();

        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->scenario(Argument::exact(DepartmentRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($department);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockDepartmentFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;
        $department = new Department($id);

        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->scenario(Argument::exact(DepartmentRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($department);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new DepartmentView($department));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockDepartmentFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter['name'] = 'name';
        $filter['userGroup'] = 1;
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort, $page, $size]);

        $departmentArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->scenario(Argument::exact(DepartmentRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$departmentArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new DepartmentListView($departmentArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockDepartmentFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getPageAndSize'
                ]
            )->getMock();

        $pageScene = DepartmentFetchController::PAGE_SCENE['ALL'];
        $name = 'name';
        $userGroupIdEncode = 'MA';
        $userGroupId = 1;
        $sort = 'id';
        $page = 1;
        $size = COMMON_SIZES;
        $filter['name'] = $name;
        $filter['userGroup'] = $userGroupId;

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('userGroupId'),
            Argument::exact('Lw')
        )->shouldBeCalledTimes(1)->willReturn($userGroupIdEncode);

        $request->get(
            Argument::exact('name'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($name);

        $request->get(
            Argument::exact('pageScene'),
            Argument::exact('Lw')
        )->shouldBeCalledTimes(1)->willReturn($pageScene);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);
            
        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort), $page, $size], $result);
    }
}
