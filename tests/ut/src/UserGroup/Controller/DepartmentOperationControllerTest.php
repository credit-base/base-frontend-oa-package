<?php

namespace Base\Package\UserGroup\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Common\WidgetRules\WidgetRules;

use Sdk\UserGroup\Model\Department;
use Sdk\UserGroup\Model\NullDepartment;
use Sdk\UserGroup\Repository\DepartmentRepository;
use Base\Package\UserGroup\View\Json\DepartmentView;
use Sdk\UserGroup\WidgetRules\DepartmentWidgetRules;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\Response;
use Marmot\Framework\Classes\CommandBus;

use Sdk\UserGroup\Command\AddDepartmentCommand;
use Sdk\UserGroup\Command\EditDepartmentCommand;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class DepartmentOperationControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(MockDepartmentOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new DepartmentOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $controller = new DepartmentOperationController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IOperateAbleController', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\UserGroup\Repository\DepartmentRepository',
            $this->controller->getRepository()
        );
    }

    public function testGetDepartmentWidgetRules()
    {
        $this->assertInstanceof(
            'Sdk\UserGroup\WidgetRules\DepartmentWidgetRules',
            $this->controller->getDepartmentWidgetRules()
        );
    }

    public function testGetWidgetRules()
    {
        $this->assertInstanceof(
            'Sdk\Common\WidgetRules\WidgetRules',
            $this->controller->getWidgetRules()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->controller->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->controller->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        //初始化
        $this->controller = $this->getMockBuilder(MockDepartmentOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'validateAddScenario'
                ]
            )->getMock();

        $name = 'name';
        $userGroupIdEncode = 'MQ';
        $userGroupId = 2;

        //预言
        $request = $this->prophesize(Request::class);

        $request->post(
            Argument::exact('userGroupId'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userGroupIdEncode);

        $request->post(
            Argument::exact('name'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($name);
        
        $this->controller->expects($this->exactly(2))->method('getRequest')->willReturn($request->reveal());
        
        $this->controller->expects($this->exactly(1))
            ->method('validateAddScenario')
            ->with(
                $name,
                $userGroupId
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddDepartmentCommand(
            $name,
            $userGroupId
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        //绑定
        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->controller->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->controller->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->controller->addAction();
        $this->assertTrue($result);
    }

    public function testEditViewNullFailure()
    {
        $this->controller = $this->getMockBuilder(MockDepartmentOperationController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $department = new NullDepartment();

        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->scenario(Argument::exact(DepartmentRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($department);

        $this->controller->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->controller->editView($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testEditViewSuccess()
    {
        $this->controller = $this->getMockBuilder(MockDepartmentOperationController::class)
            ->setMethods(
                [
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;
        $department = new Department($id);

        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->scenario(Argument::exact(DepartmentRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($department);

        $this->controller->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->controller->expects($this->exactly(1))
            ->method('render')
            ->with(new DepartmentView($department));

        $result = $this->controller->editView($id);
        $this->assertTrue($result);
    }

    private function initialEdit(int $id, bool $result)
    {
        $this->controller = $this->getMockBuilder(MockDepartmentOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateEditScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess'
                ]
            )->getMock();

        $id = 2;
        $name = 'name';

        $request = $this->prophesize(Request::class);

        $request->post(
            Argument::exact('name'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($name);

        $this->controller->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->controller->expects($this->exactly(1))
            ->method('validateEditScenario')
            ->with(
                $name
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditDepartmentCommand(
                    $id,
                    $name
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testEditActionFailure()
    {
        $id = 2;

        $this->initialEdit($id, false);

        $this->controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->controller->editAction($id);
        $this->assertFalse($result);
    }

    public function testEditActionSuccess()
    {
        $id = 2;

        $this->initialEdit($id, true);

        $this->controller->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->controller->editAction($id);
        
        $this->assertTrue($result);
    }

    public function testValidateAddScenario()
    {
        //初始化
        $this->controller = $this->getMockBuilder(MockDepartmentOperationController::class)
            ->setMethods(
                [
                    'getDepartmentWidgetRules',
                    'getWidgetRules'
                ]
            )->getMock();

        $name = 'name';
        $userGroupId = 2;

        //预言
        $departmentWidgetRules = $this->prophesize(DepartmentWidgetRules::class);
        $widgetRules = $this->prophesize(WidgetRules::class);
        $widgetRules->formatNumeric($userGroupId, 'userGroup')->shouldBeCalledTimes(1)->willReturn(true);
        $departmentWidgetRules->departmentName($name)->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $this->controller->expects($this->exactly(1))
            ->method('getDepartmentWidgetRules')
            ->willReturn($departmentWidgetRules->reveal());
        //绑定
        $this->controller->expects($this->exactly(1))
            ->method('getWidgetRules')
            ->willReturn($widgetRules->reveal());

        //验证
        $this->controller->validateAddScenario($name, $userGroupId);
    }

    public function testValidateEditScenario()
    {
        //初始化
        $this->controller = $this->getMockBuilder(MockDepartmentOperationController::class)
            ->setMethods(
                [
                    'getDepartmentWidgetRules'
                ]
            )->getMock();

        $name = 'name';

        //预言
        $departmentWidgetRules = $this->prophesize(DepartmentWidgetRules::class);
        $departmentWidgetRules->departmentName($name)->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $this->controller->expects($this->exactly(1))
            ->method('getDepartmentWidgetRules')
            ->willReturn($departmentWidgetRules->reveal());

        //验证
        $this->controller->validateEditScenario($name);
    }
}
