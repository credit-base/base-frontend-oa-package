<?php
namespace Base\Package\UserGroup\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Base\Sdk\Common\Model\IEnableAble;

use Sdk\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Model\NullUserGroup;
use Sdk\UserGroup\Repository\UserGroupRepository;

use Base\Package\UserGroup\View\Json\UserGroupView;
use Base\Package\UserGroup\View\Json\UserGroupListView;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Repository\CrewRepository;
use Sdk\Crew\Adapter\Crew\CrewRestfulAdapter;

use Sdk\UserGroup\Repository\DepartmentRepository;
use Sdk\UserGroup\Adapter\DepartmentRestfulAdapter;

use Base\Package\Crew\Controller\CrewFetchController;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UserGroupFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new UserGroupFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\UserGroup\Repository\UserGroupRepository',
            $this->stub->getRepository()
        );
    }

    public function testGetCrewRepository()
    {
        $this->assertInstanceof(
            'Sdk\Crew\Repository\CrewRepository',
            $this->stub->getCrewRepository()
        );
    }

    public function testGetDepartmentRepository()
    {
        $this->assertInstanceof(
            'Sdk\UserGroup\Repository\DepartmentRepository',
            $this->stub->getDepartmentRepository()
        );
    }

    public function testGetConcurrentAdapter()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Adapter\ConcurrentAdapter',
            $this->stub->getConcurrentAdapter()
        );
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter['name'] = 'name';
        $sort = ['id'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort, $page, $size]);

        $userGroupArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->scenario(Argument::exact(UserGroupRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$userGroupArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new UserGroupListView($userGroupArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getPageAndSize'
                ]
            )->getMock();

        $pageScene = UserGroupFetchController::PAGE_SCENE['ALL'];
        $name = 'name';
        $sort = 'id';
        $page = 1;
        $size = COMMON_SIZES;
        $filter['name'] = $name;

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('pageScene'),
            Argument::exact('Lw')
        )->shouldBeCalledTimes(1)->willReturn($pageScene);

        $request->get(
            Argument::exact('name'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($name);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('id')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort), $page, $size], $result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $userGroup = new NullUserGroup();

        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->scenario(Argument::exact(UserGroupRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($userGroup);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }
    
    public function initialFetchOneActionSuccess($category)
    {
        $this->stub = $this->getMockBuilder(MockUserGroupFetchController::class)
            ->setMethods([
                'getRepository',
                'getCrewRepository',
                'getDepartmentRepository',
                'getPageAndSize',
                'getConcurrentAdapter',
                'render'
            ])->getMock();

        $id = 1;
        $userGroup = new UserGroup($id);
        $crew = new Crew($id);
        $crew->setCategory($category);
        $page = 1;
        $size = 10;
        $sort = ['-updateTime'];

        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->scenario(
            Argument::exact(UserGroupRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($userGroup);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))->method('getPageAndSize')->willReturn([$size, $page]);
            
        $crewFilter['userGroup'] = $id;
        Core::$container->set('crew', $crew);
        $currentLoginCrewCategory = Core::$container->get('crew')->getCategory();
        $crewFilter['category'] = isset(CrewFetchController::CATEGORY_SCENE[$currentLoginCrewCategory]) ?
        CrewFetchController::CATEGORY_SCENE[$currentLoginCrewCategory] : 0;

        $departmentFilter['userGroup'] = $id;

        $data = array('crews'=>array(1), 'departments'=>array(1));

        $crewArray = $departmentArray = array(1,2);
        $count = 2;

        $crewRestfulAdapter = new CrewRestfulAdapter();

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->scenario(Argument::exact(CrewRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($crewRepository->reveal());
        $crewRepository->searchAsync(
            Argument::exact($crewFilter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$crewArray]);
        $crewRepository->getAdapter()->shouldBeCalledTimes(1)->willReturn($crewRestfulAdapter);
        $this->stub->expects($this->exactly(1))
            ->method('getCrewRepository')
            ->willReturn($crewRepository->reveal());

        $departmentRestfulAdapter = new DepartmentRestfulAdapter();

        $departmentRepository = $this->prophesize(DepartmentRepository::class);
        $departmentRepository->scenario(Argument::exact(DepartmentRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($departmentRepository->reveal());
        $departmentRepository->searchAsync(
            Argument::exact($departmentFilter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$departmentArray]);
        $departmentRepository->getAdapter()->shouldBeCalledTimes(1)->willReturn($departmentRestfulAdapter);
        $this->stub->expects($this->exactly(1))
            ->method('getDepartmentRepository')
            ->willReturn($departmentRepository->reveal());

        $concurrentAdapter = $this->prophesize(ConcurrentAdapter::class);
        $concurrentAdapter->addPromise(
            'crews',
            [$count,$crewArray],
            $crewRestfulAdapter
        )->shouldBeCalledTimes(1);
        $concurrentAdapter->addPromise(
            'departments',
            [$count,$departmentArray],
            $departmentRestfulAdapter
        )->shouldBeCalledTimes(1);
        $concurrentAdapter->run()->shouldBeCalledTimes(1)->willReturn($data);
        $this->stub->expects($this->exactly(3))
            ->method('getConcurrentAdapter')
            ->willReturn($concurrentAdapter->reveal());
 
        $this->stub->expects($this->exactly(1))->method('render')->with(new UserGroupView($userGroup, $data));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionSuccessCategoryNull()
    {
        $category = 0;

        $this->initialFetchOneActionSuccess($category);
    }

    public function testFetchOneActionSuccess()
    {
        $category = Crew::CATEGORY['SUPERTUBE'];

        $this->initialFetchOneActionSuccess($category);
    }
}
