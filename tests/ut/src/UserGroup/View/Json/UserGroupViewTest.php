<?php

namespace Base\Package\UserGroup\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Crew\Model\Crew;
use Sdk\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\Model\Department;
use Sdk\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Translator\DepartmentTranslator;
use Sdk\UserGroup\Translator\UserGroupTranslator;

class UserGroupViewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockUserGroupView();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetUserGroup()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\Model\UserGroup',
            $this->stub->getUserGroup()
        );
    }

    public function testGetData()
    {
        $this->assertIsArray($this->stub->getData());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\Translator\UserGroupTranslator',
            $this->stub->getTranslator()
        );
    }
    public function testGetCrewTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewTranslator',
            $this->stub->getCrewTranslator()
        );
    }
    public function testGetDepartmentTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\Translator\DepartmentTranslator',
            $this->stub->getDepartmentTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupView::class)
            ->setMethods([
                'getTranslator','getCrewList','getDepartmentList','encode'
            ])->getMock();

        $userGroup = new UserGroup();
        $userGroups = [];
        $translator = $this->prophesize(UserGroupTranslator::class);
        $translator->objectToArray($userGroup)->shouldBeCalledTimes(1)->willReturn($userGroups);
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $crewList = $departmentList = [];
        $this->stub->expects($this->exactly(1))->method('getCrewList')->willReturn($crewList);
        $this->stub->expects($this->exactly(1))->method('getDepartmentList')->willReturn($departmentList);

        $this->stub->expects($this->exactly(1))->method('encode')->with(
            [
                'crewList'=>$crewList,
                'departmentList'=>$departmentList
            ]
        );

        $this->assertNull($this->stub->display());
    }

    public function testGetCrewList()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupView::class)
            ->setMethods([
                'getData','getCrewTranslator','encode'
            ])->getMock();

        $crew = new Crew();
        $crewData = [];
        $data['crews'] = [1,[$crew]];

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($data);

        $translator = $this->prophesize(CrewTranslator::class);
        $translator->objectToArray(
            $crew,
            array('id', 'realName', 'cellphone', 'category', 'updateTime', 'status', 'department')
        )->shouldBeCalledTimes(1)->willReturn($crewData);
        $this->stub->expects($this->exactly(1))->method('getCrewTranslator')->willReturn($translator->reveal());

        $crewList['total'] = $data['crews'][0];
        $crewList['list'][] = $crewData;

        $this->assertEquals($crewList, $this->stub->getCrewList());
    }

    public function testGetDepartmentList()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupView::class)
            ->setMethods([
                'getData','getDepartmentTranslator','encode'
            ])->getMock();

        $department = new Department();
        $departmentData = [];
        $data['departments'] = [1,[$department]];

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($data);

        $translator = $this->prophesize(DepartmentTranslator::class);
        $translator->objectToArray(
            $department,
            array('id', 'name', 'updateTime')
        )->shouldBeCalledTimes(1)->willReturn($departmentData);
        $this->stub->expects($this->exactly(1))->method('getDepartmentTranslator')->willReturn($translator->reveal());

        $crewList['total'] = $data['departments'][0];
        $crewList['list'][] = $departmentData;

        $this->assertEquals($crewList, $this->stub->getDepartmentList());
    }
}
