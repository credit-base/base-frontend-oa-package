<?php

namespace Base\Package\UserGroup\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Crew\Model\Crew;
use Sdk\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\Model\Department;
use Sdk\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Translator\DepartmentTranslator;
use Sdk\UserGroup\Translator\UserGroupTranslator;

class DepartmentViewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockDepartmentView();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetDepartment()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\Model\Department',
            $this->stub->getDepartment()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\Translator\DepartmentTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockDepartmentView::class)
            ->setMethods([
                'getTranslator','getDepartment','encode'
            ])->getMock();

        $department = new Department();
        $departments = [];
        $translator = $this->prophesize(DepartmentTranslator::class);
        $translator->objectToArray($department)->shouldBeCalledTimes(1)->willReturn($departments);
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->stub->expects($this->exactly(1))->method('getDepartment')->willReturn($department);

        $this->stub->expects($this->exactly(1))->method('encode')->with($departments);

        $this->assertNull($this->stub->display());
    }
}
