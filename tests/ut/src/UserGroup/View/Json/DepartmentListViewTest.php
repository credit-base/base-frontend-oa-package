<?php


namespace Base\Package\UserGroup\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\UserGroup\Model\Department;
use Sdk\UserGroup\Translator\DepartmentTranslator;

class DepartmentListViewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockDepartmentListView();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetDepartment()
    {
        $this->assertIsArray($this->stub->getDepartment());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\Translator\DepartmentTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockDepartmentListView::class)
            ->setMethods(['getTranslator','getDepartment','getCount','encode'])
            ->getMock();

        $department = new Department();
        $departments = [$department];
        $count = count($departments);

        $departmentData = [];

        $translator = $this->prophesize(DepartmentTranslator::class);
        $translator->objectToArray(
            $department,
            array( 'id',
                    'name',
                    'userGroup',
                    'updateTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($departmentData);
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->stub->expects($this->exactly(1))->method('getDepartment')->willReturn($departments);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->stub->expects($this->exactly(1))->method('encode')->with(
            ['total'=>$count,'list'=>[$departmentData]]
        );

        $this->assertNull($this->stub->display());
    }
}
