<?php


namespace Base\Package\UserGroup\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Translator\UserGroupTranslator;

class UserGroupListViewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockUserGroupListView();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetUserGroup()
    {
        $this->assertIsArray($this->stub->getUserGroup());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\Translator\UserGroupTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupListView::class)
            ->setMethods(['getTranslator','getUserGroup','getCount','encode'])
            ->getMock();

        $userGroup = new UserGroup();
        $userGroups = [$userGroup];
        $count = count($userGroups);

        $userGroupData = [];

        $this->stub->expects($this->exactly(1))->method('getUserGroup')->willReturn($userGroups);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $translator = $this->prophesize(UserGroupTranslator::class);
        $translator->objectToArray(
            $userGroup,
            ['id','name','shortName']
        )->shouldBeCalledTimes(1)->willReturn($userGroupData);
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $dataList = [
            'total' => $count,
            'list' => [$userGroupData]
        ];
        $this->stub->expects($this->exactly(1))->method('encode')->with($dataList);

        $this->assertNull($this->stub->display());
    }
}
