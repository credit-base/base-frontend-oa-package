<?php

namespace Base\Package\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Crew\Model\Crew;
use Sdk\News\Command\UnAuditNews\EditUnAuditNewsCommand;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UnAuditNewsOperationControllerTest extends TestCase
{
    use NewsRequestDataTrait;

    private $unAuditNewsController;

    public function setUp()
    {
        $this->unAuditNewsController = $this->getMockBuilder(MockUnAuditNewsOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditNewsController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $unAuditNewsController = new UnAuditNewsOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $unAuditNewsController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $unAuditNewsController = new UnAuditNewsOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $unAuditNewsController
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\News\Repository\UnAuditNewsRepository',
            $this->unAuditNewsController->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->unAuditNewsController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->unAuditNewsController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditNewsController->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testEditView()
    {
        $id = 0;
        $this->unAuditNewsController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditNewsController->editView($id);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAddAction()
    {
        $this->unAuditNewsController->expects($this->exactly(1))
        ->method('displayError')
        ->willReturn(false);

        $result = $this->unAuditNewsController->addAction();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    private function initialEdit(int $id, bool $result)
    {
        $this->unAuditNewsController = $this->getMockBuilder(MockUnAuditNewsOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->unAuditNewsController->expects($this->exactly(1))->method('getRequestCommonData')->willReturn($data);

        $this->unAuditNewsController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['title'],
                $data['source'],
                $data['cover'],
                $data['attachments'],
                $data['bannerImage'],
                $data['content'],
                $data['newsType'],
                $data['dimension'],
                $data['status'],
                $data['stick'],
                $data['bannerStatus'],
                $data['homePageShowStatus']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditUnAuditNewsCommand(
                    $data['title'],
                    $data['source'],
                    $data['content'],
                    $data['newsType'],
                    $data['dimension'],
                    $data['status'],
                    $data['stick'],
                    $data['bannerStatus'],
                    $data['homePageShowStatus'],
                    $data['cover'],
                    $data['attachments'],
                    $data['bannerImage'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->unAuditNewsController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testEditActionFailure()
    {
        $id = 2;

        $this->initialEdit($id, false);

        $this->unAuditNewsController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditNewsController->editAction($id);
        $this->assertFalse($result);
    }

    public function testEditActionSuccess()
    {
        $id = 2;

        $this->initialEdit($id, true);

        $this->unAuditNewsController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->unAuditNewsController->editAction($id);
        
        $this->assertTrue($result);
    }
}
