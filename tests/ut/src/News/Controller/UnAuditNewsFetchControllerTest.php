<?php
namespace Base\Package\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;

use Sdk\News\Model\UnAuditNews;
use Sdk\News\Model\NullUnAuditNews;
use Sdk\News\Repository\UnAuditNewsRepository;

use Sdk\Crew\Model\Crew;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Package\News\View\Json\UnAuditNews\View;
use Base\Package\News\View\Json\UnAuditNews\ListView;

/**
 * @SuppressWarnings(PHPMD)
 */
class UnAuditNewsFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditNewsFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'checkUserHasPurview'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new UnAuditNewsFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\News\Repository\UnAuditNewsRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditNewsFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $unAuditNews = new NullUnAuditNews();

        $repository = $this->prophesize(UnAuditNewsRepository::class);
        $repository->scenario(Argument::exact(UnAuditNewsRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($unAuditNews);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditNewsFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $unAuditNews = new UnAuditNews($id);

        $repository = $this->prophesize(UnAuditNewsRepository::class);
        $repository->scenario(
            Argument::exact(UnAuditNewsRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditNews);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($unAuditNews));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditNewsFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render',
                    'checkUserHasPurview'
                ]
            )->getMock();

        $filter['title'] = 'title';
        $filter['newsType'] = '1';
        $filter['applyStatus'] = '0,-2';

        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);
        
        $this->stub->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->with('unAuditedNews')
            ->willReturn(true);

        $unAuditNewsArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(UnAuditNewsRepository::class);
        $repository->scenario(Argument::exact(UnAuditNewsRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$unAuditNewsArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($unAuditNewsArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionPurviewFail()
    {
        $this->stub->expects($this->exactly(1))
             ->method('checkUserHasPurview')
             ->willReturn(false);
        
        $result = $this->stub->filterAction();
        $this->assertFalse($result);
    }

    public function testCheckUserHasPurview()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditNewsFetchController::class)
            ->setMethods(
                [
                    'checkUserHasNewsPurview',
                ]
            )->getMock();

        $resource = 'unAuditedNews';
        $this->stub->expects($this->any())
            ->method('checkUserHasNewsPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->stub->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditNewsFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $sort = '-updateTime';
        $title = 'title';
        $newsType = 2;
        $newsTypeEncode = marmot_encode($newsType);
        $applyStatus = 2;
        $applyStatusEncode = marmot_encode($applyStatus);

        $userGroup = 1;
        $userGroupEncode = marmot_encode($userGroup);

        $filter['title'] = $title;
        $filter['applyStatus'] = $applyStatus;
        $filter['applyUserGroup'] = $userGroup;
        $filter['applyInfoType'] = $newsType;

        Core::$container->set('crew', new Crew(1));

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $request->get(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($title);

        $request->get(
            Argument::exact('applyStatus'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($applyStatusEncode);

        $request->get(
            Argument::exact('newsType'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($newsTypeEncode);

        $request->get(
            Argument::exact('userGroup'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userGroupEncode);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort)], $result);
    }

    public function testCheckUserHasNewsPurviewSuccess()
    {
        $resource = 'unAuditedNews';
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setCategory(2);

        $this->assertTrue($this->stub->checkUserHasNewsPurview($resource));
    }

    public function testCheckUserHasNewsPurviewFail()
    {
        $resource = 'string';
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setCategory(2);

        $this->assertFalse($this->stub->checkUserHasNewsPurview($resource));
    }
}
