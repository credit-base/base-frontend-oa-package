<?php
namespace Base\Package\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;

use Sdk\News\Model\News;
use Sdk\News\Model\NullNews;
use Sdk\News\Repository\NewsRepository;

use Sdk\Crew\Model\Crew;

use Base\Package\News\View\Json\News\View;
use Base\Package\News\View\Json\News\ListView;
use Base\Package\News\View\Json\News\NewsTypeListView;

/**
 * @SuppressWarnings(PHPMD)
 */
class NewsFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'checkUserHasPurview',
                    'checkUserHasNewsPurview',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new NewsFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\News\Repository\NewsRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                ]
            )->getMock();

        $id = 1;
        $news = new NullNews();

        $repository = $this->prophesize(NewsRepository::class);
        $repository->scenario(Argument::exact(NewsRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($news);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $news = new News($id);

        $repository = $this->prophesize(NewsRepository::class);
        $repository->scenario(
            Argument::exact(NewsRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($news);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($news));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render',
                    'checkUserHasPurview'
                ]
            )->getMock();

        $filter['publishUserGroup'] = 1;
        $filter['title'] = 'title';
        $filter['newsType'] = 151;
        $filter['dimension'] = 1;
        $filter['status'] = 0;
        $filter['stick'] = 0;
        $filter['bannerStatus'] = 2;
        $filter['homePageShowStatus'] = 2;
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $newsArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(NewsRepository::class);
        $repository->scenario(Argument::exact(NewsRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$newsArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($newsArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $sort = '-updateTime';
        $userGroup = 1;
        $userGroupEncode = marmot_encode($userGroup);
        $title = 'title';
        $newsType = 151;
        $newsTypeEncode = marmot_encode($newsType);
        
        $dimension = 1;
        $dimensionEncode = marmot_encode($dimension);

        $status = 0;
        $statusEncode = marmot_encode($status);

        $stick = 2;
        $stickEncode = marmot_encode($stick);

        $bannerStatus = 2;
        $bannerStatusEncode = marmot_encode($bannerStatus);

        $homePageShowStatus = 2;
        $homePageShowStatusEncode = marmot_encode($homePageShowStatus);

        $filter['publishUserGroup'] = $userGroup;
        $filter['title'] = $title;
        $filter['newsType'] = $newsType;
        $filter['dimension'] = $dimension;
        $filter['status'] = $status;
        $filter['stick'] = $stick;
        $filter['bannerStatus'] = $bannerStatus;
        $filter['homePageShowStatus'] = $homePageShowStatus;

        Core::$container->set('crew', new Crew());

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $request->get(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($title);

        $request->get(
            Argument::exact('userGroup'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userGroupEncode);

        $request->get(
            Argument::exact('newsType'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($newsTypeEncode);

        $request->get(
            Argument::exact('dimension'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($dimensionEncode);

        $request->get(
            Argument::exact('status'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($statusEncode);

        $request->get(
            Argument::exact('stick'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($stickEncode);

        $request->get(
            Argument::exact('bannerStatus'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($bannerStatusEncode);

        $request->get(
            Argument::exact('homePageShowStatus'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($homePageShowStatusEncode);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort)], $result);
    }

    public function testNewsType()
    {
        $this->stub = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'render'
                ]
            )->getMock();

        $data = [];

        $this->stub->expects($this->exactly(1))
        ->method('render')
        ->with(new NewsTypeListView($data));

        $result = $this->stub->newsType();
        $this->assertTrue($result);
    }

    public function testFilterActionPurviewFail()
    {
        $this->stub->expects($this->exactly(1))
             ->method('checkUserHasPurview')
             ->willReturn(false);
        
        $result = $this->stub->filterAction();
        $this->assertFalse($result);
    }

    public function testCheckUserHasPurview()
    {
        $this->stub = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'checkUserHasNewsPurview',
                ]
            )->getMock();

        $resource = 'news';
        $this->stub->expects($this->any())
            ->method('checkUserHasNewsPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->stub->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }
}
