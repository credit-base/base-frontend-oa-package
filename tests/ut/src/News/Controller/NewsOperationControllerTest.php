<?php

namespace Base\Package\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Crew\Model\Crew;
use Sdk\News\Command\News\AddNewsCommand;
use Sdk\News\Command\News\EditNewsCommand;
use Sdk\News\Command\News\MoveNewsCommand;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class NewsOperationControllerTest extends TestCase
{
    use NewsRequestDataTrait;

    private $newsController;

    public function setUp()
    {
        $this->newsController = $this->getMockBuilder(MockNewsOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'validateMoveScenario',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->newsController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $newsController = new NewsOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $newsController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $newsController = new NewsOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $newsController
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\News\Repository\NewsRepository',
            $this->newsController->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->newsController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->newsController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->newsController->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testEditView()
    {
        $id = 0;
        $this->newsController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->newsController->editView($id);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        $this->newsController = $this->getMockBuilder(MockNewsOperationController::class)
            ->setMethods([
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequestCommonData'
                ])->getMock();

        $request = $this->getTestRequestCommonData();
        $this->newsController->expects($this->exactly(1))->method('getRequestCommonData')->willReturn($request);

        $this->newsController->expects($this->exactly(1))->method('validateCommonScenario')
            ->with(
                $request['title'],
                $request['source'],
                $request['cover'],
                $request['attachments'],
                $request['bannerImage'],
                $request['content'],
                $request['newsType'],
                $request['dimension'],
                $request['status'],
                $request['stick'],
                $request['bannerStatus'],
                $request['homePageShowStatus']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddNewsCommand(
            $request['title'],
            $request['source'],
            $request['content'],
            $request['newsType'],
            $request['dimension'],
            $request['status'],
            $request['stick'],
            $request['bannerStatus'],
            $request['homePageShowStatus'],
            $request['cover'],
            $request['attachments'],
            $request['bannerImage']
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->newsController->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->newsController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->newsController->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->newsController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->newsController->addAction();
        $this->assertTrue($result);
    }

    private function initialEdit(int $id, bool $result)
    {
        $this->newsController = $this->getMockBuilder(MockNewsOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->newsController->expects($this->exactly(1))->method('getRequestCommonData')->willReturn($data);

        $this->newsController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['title'],
                $data['source'],
                $data['cover'],
                $data['attachments'],
                $data['bannerImage'],
                $data['content'],
                $data['newsType'],
                $data['dimension'],
                $data['status'],
                $data['stick'],
                $data['bannerStatus'],
                $data['homePageShowStatus']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditNewsCommand(
                    $data['title'],
                    $data['source'],
                    $data['content'],
                    $data['newsType'],
                    $data['dimension'],
                    $data['status'],
                    $data['stick'],
                    $data['bannerStatus'],
                    $data['homePageShowStatus'],
                    $data['cover'],
                    $data['attachments'],
                    $data['bannerImage'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->newsController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testEditActionFailure()
    {
        $id = 2;

        $this->initialEdit($id, false);

        $this->newsController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->newsController->editAction($id);
        $this->assertFalse($result);
    }

    public function testEditActionSuccess()
    {
        $id = 2;

        $this->initialEdit($id, true);

        $this->newsController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->newsController->editAction($id);
        
        $this->assertTrue($result);
    }

    private function initialMove(bool $result)
    {
        //初始化
        $this->newsController = $this->getMockBuilder(MockNewsOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'render',
                    'displaySuccess',
                    'getCommandBus',
                    'validateMoveScenario',
                ]
            )->getMock();

        $newsType = 1;

        $id = 1;

        //预言
        $this->newsController->expects($this->exactly(1))
            ->method('validateMoveScenario')
            ->with($newsType)
            ->willReturn(true);

        $command = new MoveNewsCommand(
            $newsType,
            $id
        );

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->newsController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testMoveSuccess()
    {
        $this->initialMove(true);

        $newsType = 1;
        $newsTypeEncode = marmot_encode($newsType);

        $id = 1;
        $idEncode = marmot_encode($id);


        $this->newsController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->newsController->move($idEncode, $newsTypeEncode);
        $this->assertTrue($result);
    }

    public function testMoveFailure()
    {
        $this->initialMove(false);

        $newsType = 1;
        $newsTypeEncode = marmot_encode($newsType);

        $id = 1;
        $idEncode = marmot_encode($id);

        $this->newsController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->newsController->move($idEncode, $newsTypeEncode);
       
        $this->assertFalse($result);
    }
}
