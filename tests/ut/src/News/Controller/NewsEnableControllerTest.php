<?php
namespace Base\Package\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Crew\Model\Crew;

use Sdk\News\Command\News\EnableNewsCommand;
use Sdk\News\Command\News\DisableNewsCommand;

class NewsEnableControllerTest extends TestCase
{
    private $newsStub;

    public function setUp()
    {
        $this->newsStub = $this->getMockBuilder(MockNewsEnableController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->newsStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new NewsEnableController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new NewsEnableController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IEnableAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $newsStub = new MockNewsEnableController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $newsStub->getCommandBus()
        );
    }

    public function testEnableAction()
    {
        $id = 1;

        $command = new EnableNewsCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->newsStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->newsStub->enableAction($id);
        $this->assertTrue($result);
    }

    public function testDisableAction()
    {
        $id = 1;

        $command = new DisableNewsCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->newsStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->newsStub->disableAction($id);
        $this->assertTrue($result);
    }
}
