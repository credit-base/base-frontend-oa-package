<?php
namespace Base\Package\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Marmot\Core;

use Sdk\Crew\Model\Crew;

class NewsRequestCommonTraitTest extends TestCase
{
    use NewsRequestDataTrait;

    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNewsRequestCommonTrait::class)
                            ->setMethods([
                                'getRequest',
                                'post'
                            ])
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetRequestCommonData()
    {
        $data = $this->getTestRequestCommonData();
        $newsTypeEncode = marmot_encode($data['newsType']);
        $dimensionEncode = marmot_encode($data['dimension']);
        $statusEncode = marmot_encode($data['status']);
        $stickEncode = marmot_encode($data['stick']);
        $bannerStatusEncode = marmot_encode($data['bannerStatus']);
        $homePageShowStatusEncode = marmot_encode($data['homePageShowStatus']);
        $statusEncode = marmot_encode($data['status']);

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($data['title']);
        $request->post(Argument::exact('source'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['source']);
        $request->post(Argument::exact('newsType'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($newsTypeEncode);
        $request->post(
            Argument::exact('cover'),
            Argument::exact(array())
        )->shouldBeCalledTimes(1)->willReturn($data['cover']);
        $request->post(Argument::exact('dimension'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($dimensionEncode);
        $request->post(Argument::exact('status'), Argument::exact(0))
                ->shouldBeCalledTimes(1)
                ->willReturn($statusEncode);
        $request->post(Argument::exact('attachments'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['attachments']);
        $request->post(
            Argument::exact('bannerImage'),
            Argument::exact(array())
        )->shouldBeCalledTimes(1)->willReturn($data['bannerImage']);
        $request->post(Argument::exact('content'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['content']);
        $request->post(Argument::exact('stick'), Argument::exact(0))
                ->shouldBeCalledTimes(1)
                ->willReturn($stickEncode);

        $request->post(Argument::exact('bannerStatus'), Argument::exact(0))
                ->shouldBeCalledTimes(1)
                ->willReturn($bannerStatusEncode);

        $request->post(Argument::exact('homePageShowStatus'), Argument::exact(0))
                ->shouldBeCalledTimes(1)
                ->willReturn($homePageShowStatusEncode);

        $this->trait->expects($this->any())->method('getRequest')->willReturn($request->reveal());

        $result = $this->trait->publicGetRequestCommonData();

        $this->assertEquals($data, $result);
    }
}
