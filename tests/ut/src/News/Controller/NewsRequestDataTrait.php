<?php
namespace Base\Package\News\Controller;

trait NewsRequestDataTrait
{
    public function getTestRequestCommonData() : array
    {
        $data = array(
            'title' => '多地出台评价标准',
            'source' => '北京发改委',
            "content"=> "内容",
            "status"=>0,
            "stick"=>0,
            "bannerStatus"=>0,
            "homePageShowStatus"=>0,
            "newsType"=> 1,
            "dimension"=> 1,
            "cover"=> array('name' => '封面名称', 'identify' => '封面地址.jpg'),
            "attachments"=> array(
                array('name' => 'name', 'identify' => 'identify.doc'),
                array('name' => 'name', 'identify' => 'identify.doc'),
                array('name' => 'name', 'identify' => 'identify.doc')
            ),
            "bannerImage"=>array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg'),
        );

        return $data;
    }
}
