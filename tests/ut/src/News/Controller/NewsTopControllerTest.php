<?php
namespace Base\Package\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Crew\Model\Crew;
use Sdk\News\Command\News\TopNewsCommand;
use Sdk\News\Command\News\CancelTopNewsCommand;

class NewsTopControllerTest extends TestCase
{
    private $newsStub;

    public function setUp()
    {
        $this->newsStub = $this->getMockBuilder(MockNewsTopController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->newsStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new NewsTopController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new NewsTopController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\ITopAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->newsStub->getCommandBus()
        );
    }

    public function testTopAction()
    {
        $this->newsStub = $this->getMockBuilder(MockNewsTopController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();

        $id = 1;
        
        $command = new TopNewsCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->newsStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->newsStub->topAction($id);
        $this->assertTrue($result);
    }

    public function testCancelTopAction()
    {
        $this->newsStub = $this->getMockBuilder(MockNewsTopController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();

        $id = 1;

        $command = new CancelTopNewsCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->newsStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->newsStub->cancelTopAction($id);
        $this->assertTrue($result);
    }
}
