<?php
namespace Base\Package\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\News\WidgetRules\NewsWidgetRules;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class NewsValidateTraitTest extends TestCase
{
    use NewsRequestDataTrait;

    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNewsValidateTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetNewsWidgetRules()
    {
        $trait = new MockNewsValidateTrait();
        $this->assertInstanceOf(
            'Sdk\News\WidgetRules\NewsWidgetRules',
            $trait->publicGetNewsWidgetRules()
        );
    }

    public function testGetWidgetRules()
    {
        $trait = new MockNewsValidateTrait();
        $this->assertInstanceOf(
            'Sdk\Common\WidgetRules\WidgetRules',
            $trait->publicGetWidgetRules()
        );
    }

    public function testValidateCommonScenario()
    {
        $trait = $this->getMockBuilder(MockNewsValidateTrait::class)
                           ->setMethods(
                               [
                                    'getWidgetRules',
                                    'getNewsWidgetRules',
                                ]
                           )
                           ->getMock();

        $data = $this->getTestRequestCommonData();

        $commonWidgetRules = $this->prophesize(WidgetRules::class);
        $newsWidgetRules = $this->prophesize(NewsWidgetRules::class);

        $commonWidgetRules->title(
            Argument::exact($data['title']),
            Argument::exact('title')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->source(
            Argument::exact($data['source']),
            Argument::exact('source')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->status(
            Argument::exact($data['status']),
            Argument::exact('status')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->stick(
            Argument::exact($data['stick']),
            Argument::exact('stick')
        )->shouldBeCalledTimes(1)->willReturn(true);
        
        $commonWidgetRules->formatString(
            Argument::exact($data['content']),
            Argument::exact('content')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->image(
            Argument::exact($data['cover']),
            Argument::exact('cover')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->attachments(
            Argument::exact($data['attachments']),
            Argument::exact('attachments')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $newsWidgetRules->dimension(
            Argument::exact($data['dimension']),
            Argument::exact('dimension')
        )->shouldBeCalledTimes(1)->willReturn(true);
        
        $newsWidgetRules->newsType(
            Argument::exact($data['newsType']),
            Argument::exact('newsType')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $newsWidgetRules->homePageShowStatus(
            Argument::exact($data['homePageShowStatus']),
            Argument::exact('homePageShowStatus')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $newsWidgetRules->bannerImage(
            Argument::exact($data['bannerStatus']),
            Argument::exact($data['bannerImage'])
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getWidgetRules')
            ->willReturn($commonWidgetRules->reveal());

        $trait->expects($this->exactly(1))
            ->method('getNewsWidgetRules')
            ->willReturn($newsWidgetRules->reveal());

        $result = $trait->publicValidateCommonScenario(
            $data['title'],
            $data['source'],
            $data['cover'],
            $data['attachments'],
            $data['bannerImage'],
            $data['content'],
            $data['newsType'],
            $data['dimension'],
            $data['status'],
            $data['stick'],
            $data['bannerStatus'],
            $data['homePageShowStatus']
        );
       
        $this->assertTrue($result);
    }

    public function testValidateRejectScenario()
    {
        $trait = $this->getMockBuilder(MockNewsValidateTrait::class)
                           ->setMethods(
                               [
                                    'getWidgetRules',
                                ]
                           )
                           ->getMock();

        $rejectReason = $this->faker->name();

        $commonWidgetRules = $this->prophesize(WidgetRules::class);

        $commonWidgetRules->reason(
            Argument::exact($rejectReason),
            Argument::exact('rejectReason')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getWidgetRules')
            ->willReturn($commonWidgetRules->reveal());

        $result = $trait->publicValidateRejectScenario($rejectReason);

        $this->assertTrue($result);
    }

    public function testValidateMoveScenario()
    {
        $trait = $this->getMockBuilder(MockNewsValidateTrait::class)
                           ->setMethods(
                               [
                                    'getNewsWidgetRules',
                                ]
                           )
                           ->getMock();

        $newsType = 1;

        $newsWidgetRules = $this->prophesize(NewsWidgetRules::class);

        $newsWidgetRules->newsType(
            Argument::exact($newsType),
            Argument::exact('newsType')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getNewsWidgetRules')
            ->willReturn($newsWidgetRules->reveal());

        $result = $trait->publicValidateMoveScenario($newsType);

        $this->assertTrue($result);
    }
}
