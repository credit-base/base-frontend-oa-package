<?php
namespace Base\Package\News\View\Json\UnAuditNews;

use PHPUnit\Framework\TestCase;
use Sdk\News\Translator\UnAuditNewsTranslator;
use Sdk\News\Model\UnAuditNews;

class ListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetList()
    {
        $this->assertIsArray($this->view->getList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\News\Translator\UnAuditNewsTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $new = new UnAuditNews();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(UnAuditNewsTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                'id',
                'title',
                'source',
                'newsType',
                'dimension',
                'stick',
                'status',
                'bannerStatus',
                'homePageShowStatus',
                'applyStatus',
                'applyInfoType',
                'operationType',
                'relation' =>['id','realName'],
                'crew' => ['id','realName'],
                'applyCrew' => ['id','realName'],
                'applyUserGroup' => ['id','name'],
                'publishUserGroup' => ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getList')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
