<?php


namespace Base\Package\News\View\Json\UnAuditNews;

use PHPUnit\Framework\TestCase;
use Sdk\News\Translator\UnAuditNewsTranslator;
use Sdk\News\Model\UnAuditNews;

class ViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetUnAuditNews()
    {
        $this->assertInstanceOf(
            'Sdk\News\Model\UnAuditNews',
            $this->view->getUnAuditNews()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\News\Translator\UnAuditNewsTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(['getTranslator','getUnAuditNews','encode'])
            ->getMock();

        $news = new UnAuditNews();
        $newsData = [];

        $translator = $this->prophesize(UnAuditNewsTranslator::class);
        $translator->objectToArray(
            $news,
            array(
                'id',
                'title',
                'source',
                'cover',
                'rejectReason',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'stick',
                'status',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'applyStatus',
                'applyInfoType',
                'operationType',
                'relation' =>['id','realName'],
                'crew' => ['id','realName'],
                'applyCrew' => ['id','realName'],
                'applyUserGroup' => ['id','name'],
                'publishUserGroup' => ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getUnAuditNews')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
