<?php
namespace Base\Package\News\View\Json\News;

use PHPUnit\Framework\TestCase;
use Sdk\News\Translator\NewsTranslator;
use Sdk\News\Model\News;

class ListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetNews()
    {
        $this->assertIsArray($this->view->getNews());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\News\Translator\NewsTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getTranslator', 'getNews', 'encode','getCount'])
            ->getMock();

        $new = new News();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(NewsTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                'id',
                'title',
                'source',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'homePageShowStatus',
                'crew' => ['id','realName'],
                'userGroup'=> ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getNews')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );
        $this->assertNull($this->view->display());
    }
}
