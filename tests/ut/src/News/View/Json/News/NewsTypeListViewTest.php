<?php
namespace Base\Package\News\View\Json\News;

use PHPUnit\Framework\TestCase;

class NewsTypeListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockNewsTypeListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetData()
    {
        $this->assertIsArray($this->view->getData());
    }

    public function testFormartArray()
    {
        $this->view = $this->getMockBuilder(MockNewsTypeListView::class)
            ->setMethods(['formartParentList'])->getMock();

        $newsType = NEWS_TYPE['LEADING_GROUP'];
        $newsTypeName = NEWS_TYPE_CN[$newsType];

        $data['newsType'] = [$newsType=>[
            0=>1,
            1=>$newsType
        ]];
        $data['category'] = [$newsType=>''];

        $parentList = [
            array(
                'value' => marmot_encode($newsType),
                'label' => $newsTypeName,
                'pid' => $data['newsType'][$newsType][0],
                'cid' => $data['newsType'][$newsType][1],
            )
        ];

        $this->view->expects($this->exactly(1))->method('formartParentList')->with($parentList)->willReturn([]);

        $this->assertEquals(
            [],
            $this->view->formartArray($data)
        );
    }

    public function initFormartParentList($key)
    {
        $this->view = $this->getMockBuilder(MockNewsTypeListView::class)
            ->setMethods(['formartCategoryList','pinArray'])->getMock();
        $parentList = [
            array(
                'pid' =>  $key
            )
        ];

        $parentChildren = [];
        $categoryList = [];
        $resultList = [];

        $this->view->expects($this->exactly(1))->method('formartCategoryList')->willReturn($parentChildren);

        if ($key > 0) {
            $resultList = [
                $key => [
                    'value' => marmot_encode(1),
                    'label' => NEWS_PARENT_CATEGORY_CN[1],
                    'children' => array_values($parentChildren)
                ]
            ];
        }

        if ($key == 0) {
            $resultList = [];
        }

        $this->view->expects($this->exactly(1))->method('pinArray')
            ->with($resultList, $categoryList)
            ->willReturn($parentChildren);

        $this->assertEquals(
            $parentChildren,
            $this->view->formartParentList($parentList)
        );
    }

    public function testFormartParentList()
    {
        $this->initFormartParentList(1);
        $this->initFormartParentList(0);
    }

    public function testPinArray()
    {
        $list =  [];
        $array = [
            ['id' => 1]
        ];

        $this->assertEquals(
            [
                ['id' => 1]
            ],
            $this->view->pinArray($list, $array)
        );
    }

    public function initFormartCategoryList($key)
    {
        $this->view = $this->getMockBuilder(MockNewsTypeListView::class)
            ->setMethods(['dealNotUseData','pinArray'])->getMock();
        $categoryList = [
            array(
                'cid' =>  $key
            )
        ];
        $value = [];
        $newCategoryList = [];

        $this->view->expects($this->exactly(1))->method('dealNotUseData')->willReturn($value);
        if ($key > 0) {
            $newCategoryList[$key] =[
                'value' => marmot_encode($key),
                'label' => NEWS_CATEGORY_CN[$key],
                'children' => $value
            ];
        }

        $childrenList = [];
        $this->view->expects($this->exactly(1))->method('pinArray')
            ->with($newCategoryList, $childrenList)
            ->willReturn($childrenList);

        $this->assertEquals(
            $childrenList,
            $this->view->formartCategoryList($categoryList)
        );
    }

    public function testFormartCategoryList()
    {
        $this->initFormartCategoryList(NEWS_CATEGORY['ORGANIZATIONAL_STRUCTURE']);
        $this->initFormartCategoryList(0);
    }

    public function testDealNotUseData()
    {
        $data = [
            0 => [
                'id' => 1,
                'pid' => 1,
                'cid' => 1,
            ]
        ];

        $this->assertEquals(
            [
                0 => ['id' => 1]
            ],
            $this->view->dealNotUseData($data)
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockNewsTypeListView::class)
            ->setMethods(['formartArray','encode'])->getMock();

        $data = array(
            'newsType' => NEWS_TYPE_MAPPING,
            'parentCategory' => NEWS_PARENT_CATEGORY_CN,
            'category' => NEWS_CATEGORY_CN,
        );

        $this->view->expects($this->exactly(1))
            ->method('formartArray')
            ->with($data)
            ->willReturn([]);

        $dataList = [
            'total' => 0,
            'list' => []
        ];

        $this->view->expects($this->exactly(1))->method('encode')->with($dataList);
        $this->assertNull($this->view->display());
    }
}
