<?php


namespace Base\Package\News\View\Json\News;

use PHPUnit\Framework\TestCase;
use Sdk\News\Translator\NewsTranslator;
use Sdk\News\Model\News;

class ViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetNews()
    {
        $this->assertInstanceOf(
            'Sdk\News\Model\News',
            $this->view->getNews()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\News\Translator\NewsTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(['getTranslator','getNews','encode'])
            ->getMock();

        $news = new News();
        $newsData = [];

        $translator = $this->prophesize(NewsTranslator::class);
        $translator->objectToArray(
            $news,
            array(
                'id',
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'description',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew' => ['id','realName'],
                'userGroup'=> ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getNews')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
