<?php


namespace Base\Package\Journal\View\Json\UnAuditJournal;

use PHPUnit\Framework\TestCase;
use Sdk\Journal\Translator\UnAuditJournalTranslator;
use Sdk\Journal\Model\UnAuditJournal;

class ViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetUnAuditJournal()
    {
        $this->assertInstanceOf(
            'Sdk\Journal\Model\UnAuditJournal',
            $this->view->getUnAuditJournal()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Journal\Translator\UnAuditJournalTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(['getTranslator','getUnAuditJournal','encode'])
            ->getMock();

        $news = new UnAuditJournal();
        $newsData = [];

        $translator = $this->prophesize(UnAuditJournalTranslator::class);
        $translator->objectToArray(
            $news,
            array(
                'id',
                'title',
                'source',
                'cover',
                'rejectReason',
                'attachment',
                'description',
                'year',
                'status',
                'authImages',
                'applyStatus',
                'applyInfoType',
                'operationType',
                'relation' =>['id','realName'],
                'crew' => ['id','realName'],
                'applyCrew' => ['id','realName'],
                'applyUserGroup' => ['id','name'],
                'publishUserGroup' => ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getUnAuditJournal')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
