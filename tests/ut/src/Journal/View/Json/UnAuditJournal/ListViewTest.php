<?php
namespace Base\Package\Journal\View\Json\UnAuditJournal;

use PHPUnit\Framework\TestCase;
use Sdk\Journal\Translator\UnAuditJournalTranslator;
use Sdk\Journal\Model\UnAuditJournal;

class ListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetList()
    {
        $this->assertIsArray($this->view->getList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Journal\Translator\UnAuditJournalTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $journal = new UnAuditJournal();
        $journalData = [];
        $journals = [$journal];
        $count = 1;

        $translator = $this->prophesize(UnAuditJournalTranslator::class);
        $translator->objectToArray(
            $journal,
            array(
                'id',
                'title',
                'source',
                'year',
                'status',
                'applyStatus',
                'applyInfoType',
                'operationType',
                'relation' =>['id','realName'],
                'crew' => ['id','realName'],
                'applyCrew' => ['id','realName'],
                'applyUserGroup' => ['id','name'],
                'publishUserGroup' => ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($journalData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getList')->willReturn($journals);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$journalData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
