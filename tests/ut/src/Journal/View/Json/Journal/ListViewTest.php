<?php
namespace Base\Package\Journal\View\Json\Journal;

use PHPUnit\Framework\TestCase;
use Sdk\Journal\Translator\JournalTranslator;
use Sdk\Journal\Model\Journal;

class ListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetJournal()
    {
        $this->assertIsArray($this->view->getJournal());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Journal\Translator\JournalTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getTranslator', 'getJournal', 'encode','getCount'])
            ->getMock();

        $new = new Journal();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(JournalTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                'id',
                'title',
                'source',
                'year',
                'status',
                'crew' => ['id','realName'],
                'userGroup'=> ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getJournal')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );
        $this->assertNull($this->view->display());
    }
}
