<?php


namespace Base\Package\Journal\View\Json\Journal;

use PHPUnit\Framework\TestCase;
use Sdk\Journal\Translator\JournalTranslator;
use Sdk\Journal\Model\Journal;

class ViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetJournal()
    {
        $this->assertInstanceOf(
            'Sdk\Journal\Model\Journal',
            $this->view->getJournal()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Journal\Translator\JournalTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(['getTranslator','getJournal','encode'])
            ->getMock();

        $news = new Journal();
        $newsData = [];

        $translator = $this->prophesize(JournalTranslator::class);
        $translator->objectToArray(
            $news,
            array(
                'id',
                'title',
                'source',
                'cover',
                'attachment',
                'description',
                'authImages',
                'year',
                'status',
                'crew' => ['id','realName'],
                'userGroup'=> ['id','name'],
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getJournal')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
