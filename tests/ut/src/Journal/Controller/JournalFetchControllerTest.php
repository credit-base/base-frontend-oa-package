<?php
namespace Base\Package\Journal\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\Response;

use Sdk\Journal\Model\Journal;
use Sdk\Crew\Model\Crew;
use Sdk\Journal\Model\NullJournal;
use Sdk\Journal\Repository\JournalRepository;

use Base\Package\Journal\View\Json\Journal\View;
use Base\Package\Journal\View\Json\Journal\ListView;

class JournalFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockJournalFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'checkUserHasPurview',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new JournalFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Journal\Repository\JournalRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockJournalFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $journal = new NullJournal();

        $repository = $this->prophesize(JournalRepository::class);
        $repository->scenario(Argument::exact(JournalRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($journal);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockJournalFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $journal = new Journal($id);

        $repository = $this->prophesize(JournalRepository::class);
        $repository->scenario(
            Argument::exact(JournalRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($journal);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($journal));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockJournalFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render',
                    'checkUserHasPurview'
                ]
            )->getMock();

        $filter['publishUserGroup'] = 1;
        $filter['status'] = 0;
        $filter['year'] = 2019;
        $filter['title'] = 'title';
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $journalArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(JournalRepository::class);
        $repository->scenario(Argument::exact(JournalRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$journalArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($journalArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockJournalFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $sort = 'updateTime';
        $title = 'title';
        $year = '2020';
        $status = '0';
        $statusEncode = marmot_encode($status);
        $userGroupId = 1;
        $userGroupIdEncode = marmot_encode($userGroupId);

        $id = 1;
        $crew = new Crew($id);
        Core::$container->set('crew', $crew);

        $filter['title'] = $title;
        $filter['year'] = $year;
        $filter['status'] = $status;
        $filter['publishUserGroup'] = $userGroupId;

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $request->get(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($title);

        $request->get(
            Argument::exact('year'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($year);

        $request->get(
            Argument::exact('status'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($statusEncode);

        $request->get(
            Argument::exact('userGroup'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userGroupIdEncode);


        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort)], $result);
    }

    public function testFilterActionPurviewFail()
    {
        $this->stub->expects($this->exactly(1))
             ->method('checkUserHasPurview')
             ->willReturn(false);
        
        $result = $this->stub->filterAction();
        $this->assertFalse($result);
    }

    public function testCheckUserHasPurview()
    {
        $this->stub = $this->getMockBuilder(MockJournalFetchController::class)
            ->setMethods(
                [
                    'checkUserHasJournalPurview',
                ]
            )->getMock();

        $resource = 'journals';
        $this->stub->expects($this->any())
            ->method('checkUserHasJournalPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->stub->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }
}
