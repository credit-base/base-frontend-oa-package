<?php

namespace Base\Package\Journal\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Crew\Model\Crew;

use Sdk\Journal\Command\Journal\AddJournalCommand;
use Sdk\Journal\Command\Journal\EditJournalCommand;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class JournalOperationControllerTest extends TestCase
{
    use JournalRequestDataTrait;

    private $journalController;

    public function setUp()
    {
        $this->journalController = $this->getMockBuilder(MockJournalOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->journalController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $journalController = new JournalOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $journalController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $journalController = new JournalOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $journalController
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Journal\Repository\JournalRepository',
            $this->journalController->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->journalController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->journalController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->journalController->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        $this->journalController = $this->getMockBuilder(MockJournalOperationController::class)
            ->setMethods([
                    'getRequest', 'displayError', 'displaySuccess', 'getCommandBus', 'validateCommonScenario',
                    'getRequestCommonData'
                ])->getMock();

        $data = $this->getTestRequestCommonData();

        $this->journalController->expects($this->exactly(1))->method('getRequestCommonData')->willReturn($data);

        $this->journalController->expects($this->exactly(1))->method('validateCommonScenario')
            ->with(
                $data['title'],
                $data['source'],
                $data['cover'],
                $data['attachment'],
                $data['authImages'],
                $data['description'],
                $data['year'],
                $data['status']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddJournalCommand(
            $data['title'],
            $data['source'],
            $data['description'],
            $data['status'],
            $data['year'],
            CONSTANT,
            $data['cover'],
            $data['attachment'],
            $data['authImages']
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->journalController->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->journalController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->journalController->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->journalController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->journalController->addAction();
        $this->assertTrue($result);
    }

    public function testEditView()
    {
        $this->journalController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->journalController->editView(1);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }

    private function initialEdit(int $id, bool $result)
    {
        $this->journalController = $this->getMockBuilder(MockJournalOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->journalController->expects($this->exactly(1))
             ->method('getRequestCommonData')
             ->willReturn($data);

        $this->journalController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['title'],
                $data['source'],
                $data['cover'],
                $data['attachment'],
                $data['authImages'],
                $data['description'],
                $data['year'],
                $data['status']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditJournalCommand(
                    $data['title'],
                    $data['source'],
                    $data['description'],
                    $data['status'],
                    $data['year'],
                    $id,
                    $data['cover'],
                    $data['attachment'],
                    $data['authImages']
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->journalController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testEditActionFailure()
    {
        $id = 2;

        $this->initialEdit($id, false);

        $this->journalController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->journalController->editAction($id);
        $this->assertFalse($result);
    }

    public function testEditActionSuccess()
    {
        $id = 2;

        $this->initialEdit($id, true);

        $this->journalController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->journalController->editAction($id);
        
        $this->assertTrue($result);
    }
}
