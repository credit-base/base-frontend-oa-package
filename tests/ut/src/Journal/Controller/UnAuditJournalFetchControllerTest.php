<?php
namespace Base\Package\Journal\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;

use Sdk\Journal\Model\UnAuditJournal;
use Sdk\Journal\Model\NullUnAuditJournal;
use Sdk\Journal\Repository\UnAuditJournalRepository;

use Sdk\Crew\Model\Crew;

use Base\Package\Journal\View\Json\UnAuditJournal\View;
use Base\Package\Journal\View\Json\UnAuditJournal\ListView;

/**
 * @SuppressWarnings(PHPMD)
 */
class UnAuditJournalFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditJournalFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'checkUserHasPurview'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new UnAuditJournalFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Journal\Repository\UnAuditJournalRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditJournalFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $unAuditJournal = new NullUnAuditJournal();

        $repository = $this->prophesize(UnAuditJournalRepository::class);
        $repository->scenario(Argument::exact(UnAuditJournalRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($unAuditJournal);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditJournalFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $unAuditJournal = new UnAuditJournal($id);

        $repository = $this->prophesize(UnAuditJournalRepository::class);
        $repository->scenario(
            Argument::exact(UnAuditJournalRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditJournal);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($unAuditJournal));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditJournalFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render',
                    'checkUserHasPurview'
                ]
            )->getMock();

        $filter['title'] = 'title';
        $filter['operationType'] = 2;
        $filter['applyStatus'] = 2;

        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);
        
        $this->stub->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->with('unAuditedJournals')
            ->willReturn(true);

        $unAuditJournalArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(UnAuditJournalRepository::class);
        $repository->scenario(Argument::exact(UnAuditJournalRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$unAuditJournalArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($unAuditJournalArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionPurviewFail()
    {
        $this->stub->expects($this->exactly(1))
             ->method('checkUserHasPurview')
             ->willReturn(false);
        
        $result = $this->stub->filterAction();
        $this->assertFalse($result);
    }

    public function testCheckUserHasPurview()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditJournalFetchController::class)
            ->setMethods(
                [
                    'checkUserHasJournalPurview',
                ]
            )->getMock();

        $resource = 'unAuditedJournals';
        $this->stub->expects($this->any())
            ->method('checkUserHasJournalPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->stub->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditJournalFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $sort = '-updateTime';
        $title = 'title';
        $operationType = '2';
        $operationTypeEncode = marmot_encode($operationType);
        $applyStatus = '2';
        $applyStatusEncode = marmot_encode($applyStatus);

        $userGroup = '1';
        $userGroupEncode = marmot_encode($userGroup);

        $filter['title'] = $title;
        $filter['applyStatus'] = $applyStatus;
        $filter['applyUserGroup'] = $userGroup;
        $filter['operationType'] = $operationType;

        Core::$container->set('crew', new Crew(1));

        $request = $this->prophesize(Request::class);
        
        $request->get(
            Argument::exact('userGroup'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userGroupEncode);

        $request->get(
            Argument::exact('operationType'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($operationTypeEncode);

        $request->get(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($title);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);

        $request->get(
            Argument::exact('applyStatus'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($applyStatusEncode);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort)], $result);
    }

    public function testCheckUserHasJournalPurviewSuccess()
    {
        $resource = 'unAuditedJournals';
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setCategory(2);

        $this->assertTrue($this->stub->checkUserHasJournalPurview($resource));
    }

    public function testCheckUserHasJournalPurviewFail()
    {
        $resource = 'tasks';
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setCategory(2);

        $this->assertFalse($this->stub->checkUserHasJournalPurview($resource));
    }
}
