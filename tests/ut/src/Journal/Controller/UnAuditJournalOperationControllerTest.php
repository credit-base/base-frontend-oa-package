<?php

namespace Base\Package\Journal\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Crew\Model\Crew;
use Sdk\Journal\Command\UnAuditJournal\EditUnAuditJournalCommand;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UnAuditJournalOperationControllerTest extends TestCase
{
    use JournalRequestDataTrait;

    private $unAuditJournalController;

    public function setUp()
    {
        $this->unAuditJournalController = $this->getMockBuilder(MockUnAuditJournalOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditJournalController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $unAuditJournalController = new UnAuditJournalOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $unAuditJournalController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $unAuditJournalController = new UnAuditJournalOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $unAuditJournalController
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Journal\Repository\UnAuditJournalRepository',
            $this->unAuditJournalController->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->unAuditJournalController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->unAuditJournalController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditJournalController->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testEditView()
    {
        $id = 0;
        $this->unAuditJournalController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditJournalController->editView($id);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAddAction()
    {
        $this->unAuditJournalController->expects($this->exactly(1))
        ->method('displayError')
        ->willReturn(false);

        $result = $this->unAuditJournalController->addAction();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    private function initialEdit(int $id, bool $result)
    {
        $this->unAuditJournalController = $this->getMockBuilder(MockUnAuditJournalOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->unAuditJournalController->expects($this->exactly(1))->method('getRequestCommonData')->willReturn($data);

        $this->unAuditJournalController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['title'],
                $data['source'],
                $data['cover'],
                $data['attachment'],
                $data['authImages'],
                $data['description'],
                $data['year'],
                $data['status']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditUnAuditJournalCommand(
                    $data['title'],
                    $data['source'],
                    $data['description'],
                    $data['status'],
                    $data['year'],
                    $id,
                    $data['cover'],
                    $data['attachment'],
                    $data['authImages']
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->unAuditJournalController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testEditActionFailure()
    {
        $id = 2;

        $this->initialEdit($id, false);

        $this->unAuditJournalController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditJournalController->editAction($id);
        $this->assertFalse($result);
    }

    public function testEditActionSuccess()
    {
        $id = 2;

        $this->initialEdit($id, true);

        $this->unAuditJournalController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->unAuditJournalController->editAction($id);
        
        $this->assertTrue($result);
    }
}
