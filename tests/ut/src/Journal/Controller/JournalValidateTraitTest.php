<?php
namespace Base\Package\Journal\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\Journal\WidgetRules\JournalWidgetRules;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class JournalValidateTraitTest extends TestCase
{
    use JournalRequestDataTrait;

    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockJournalValidateTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetJournalWidgetRules()
    {
        $trait = new MockJournalValidateTrait();
        $this->assertInstanceOf(
            'Sdk\Journal\WidgetRules\JournalWidgetRules',
            $trait->publicGetJournalWidgetRules()
        );
    }

    public function testGetWidgetRules()
    {
        $trait = new MockJournalValidateTrait();
        $this->assertInstanceOf(
            'Sdk\Common\WidgetRules\WidgetRules',
            $trait->publicGetWidgetRules()
        );
    }

    public function testValidateCommonScenario()
    {
        $trait = $this->getMockBuilder(MockJournalValidateTrait::class)
                           ->setMethods(
                               [
                                    'getWidgetRules',
                                    'getJournalWidgetRules',
                                ]
                           )
                           ->getMock();

        $data = $this->getTestRequestCommonData();

        $commonWidgetRules = $this->prophesize(WidgetRules::class);
        $journalWidgetRules = $this->prophesize(JournalWidgetRules::class);

       
        $commonWidgetRules->source(
            Argument::exact($data['source']),
            Argument::exact('source')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->status(
            Argument::exact($data['status']),
            Argument::exact('status')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->title(
            Argument::exact($data['title']),
            Argument::exact('title')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->description(
            Argument::exact($data['description']),
            Argument::exact('description')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->image(
            Argument::exact($data['cover']),
            Argument::exact('cover')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $journalWidgetRules->authImages(
            Argument::exact($data['authImages']),
            Argument::exact('authImages')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $journalWidgetRules->attachment(
            Argument::exact($data['attachment']),
            Argument::exact('attachment')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $journalWidgetRules->year(
            Argument::exact($data['year']),
            Argument::exact('year')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getWidgetRules')
            ->willReturn($commonWidgetRules->reveal());

        $trait->expects($this->exactly(1))
            ->method('getJournalWidgetRules')
            ->willReturn($journalWidgetRules->reveal());

        $result = $trait->publicValidateCommonScenario(
            $data['title'],
            $data['source'],
            $data['cover'],
            $data['attachment'],
            $data['authImages'],
            $data['description'],
            $data['year'],
            $data['status']
        );
       
        $this->assertTrue($result);
    }

    public function testValidateRejectScenario()
    {
        $trait = $this->getMockBuilder(MockJournalValidateTrait::class)
                           ->setMethods(
                               [
                                    'getWidgetRules',
                                ]
                           )
                           ->getMock();

        $rejectReason = $this->faker->name();

        $commonWidgetRules = $this->prophesize(WidgetRules::class);

        $commonWidgetRules->reason(
            Argument::exact($rejectReason),
            Argument::exact('rejectReason')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getWidgetRules')
            ->willReturn($commonWidgetRules->reveal());

        $result = $trait->publicValidateRejectScenario($rejectReason);

        $this->assertTrue($result);
    }
}
