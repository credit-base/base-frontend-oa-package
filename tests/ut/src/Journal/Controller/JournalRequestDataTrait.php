<?php
namespace Base\Package\Journal\Controller;

trait JournalRequestDataTrait
{
    public function getTestRequestCommonData() : array
    {
        $data = array(
            'title' => '多地出台评价标准',
            'source' => '北京发改委',
            "description"=> "内容",
            "status"=>0,
            "year"=>2012,
            "cover"=> array('name' => '封面名称', 'identify' => '封面地址.jpg'),
            "attachment"=> array('name' => 'name', 'identify' => 'identify.pdf'),
            "authImages"=>array(array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg')),
        );

        return $data;
    }
}
