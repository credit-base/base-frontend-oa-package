<?php
namespace Base\Package\Journal\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Request;

use Sdk\Crew\Model\Crew;
use Sdk\Journal\Command\UnAuditJournal\ApproveUnAuditJournalCommand;
use Sdk\Journal\Command\UnAuditJournal\RejectUnAuditJournalCommand;

class UnAuditJournalApproveControllerTest extends TestCase
{
    private $unAuditJournalStub;

    public function setUp()
    {
        $this->unAuditJournalStub = $this->getMockBuilder(MockUnAuditJournalApproveController::class)
            ->setMethods(
                [
                    'validateRejectScenario',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditJournalStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new UnAuditJournalApproveController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIApproveAbleController()
    {
        $controller = new UnAuditJournalApproveController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IApproveAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $unAuditJournalStub = new MockUnAuditJournalApproveController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $unAuditJournalStub->getCommandBus()
        );
    }

    public function testApproveAction()
    {
        $id = 1;

        $crew = new Crew($id);
        Core::$container->set('crew', $crew);
        $crewId = Core::$container->get('crew')->getId();

        $command = new ApproveUnAuditJournalCommand($crewId, $id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->unAuditJournalStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->unAuditJournalStub->approveAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionSuccess()
    {
        $id = 1;
        $crew = new Crew($id);
        Core::$container->set('crew', $crew);
        $crewId = Core::$container->get('crew')->getId();
        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->unAuditJournalStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());
        
        $command = new RejectUnAuditJournalCommand($rejectReason, $crewId, $id);
       
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->unAuditJournalStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(true);
        $this->unAuditJournalStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->unAuditJournalStub->rejectAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionFail()
    {
        $id = 1;
        
        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->unAuditJournalStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());

        $this->unAuditJournalStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(false);

        $result = $this->unAuditJournalStub->rejectAction($id);
        $this->assertFalse($result);
    }

    // public function testValidateRejectScenario()
    // {
    //     $stub = $this->getMockBuilder(MockJournalApproveController::class)
    //                        ->setMethods(
    //                            [
    //                                 'getWidgetRules',
    //                             ]
    //                        )
    //                        ->getMock();

    //     $rejectReason = "内容不合理";

    //     $commonWidgetRules = $this->prophesize(WidgetRules::class);

    //     $commonWidgetRules->reason(
    //         Argument::exact($rejectReason),
    //         Argument::exact('rejectReason')
    //     )->shouldBeCalledTimes(1)->willReturn(true);

    //     $stub->expects($this->exactly(1))
    //         ->method('getWidgetRules')
    //         ->willReturn($commonWidgetRules->reveal());

    //     $result = $stub->validateRejectScenario($rejectReason);

    //     $this->assertTrue($result);
    // }
}
