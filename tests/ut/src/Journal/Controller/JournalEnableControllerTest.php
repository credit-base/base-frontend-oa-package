<?php
namespace Base\Package\Journal\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Crew\Model\Crew;

use Sdk\Journal\Command\Journal\EnableJournalCommand;
use Sdk\Journal\Command\Journal\DisableJournalCommand;

class JournalEnableControllerTest extends TestCase
{
    private $journalStub;

    public function setUp()
    {
        $this->journalStub = $this->getMockBuilder(MockJournalEnableController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->journalStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new JournalEnableController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new JournalEnableController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IEnableAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $journalStub = new MockJournalEnableController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $journalStub->getCommandBus()
        );
    }

    public function testEnableAction()
    {
        $id = 1;
        $crew = new Crew($id);
        Core::$container->set('crew', $crew);
        $crewId = Core::$container->get('crew')->getId();

        $command = new EnableJournalCommand($crewId, $id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->journalStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->journalStub->enableAction($id);
        $this->assertTrue($result);
    }

    public function testDisableAction()
    {
        $id = 1;
        $crew = new Crew($id);
        Core::$container->set('crew', $crew);
        $crewId = Core::$container->get('crew')->getId();

        $command = new DisableJournalCommand($crewId, $id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->journalStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->journalStub->disableAction($id);
        $this->assertTrue($result);
    }
}
