<?php
namespace Base\Package\Journal\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Marmot\Core;

use Sdk\Crew\Model\Crew;

class JournalRequestCommonTraitTest extends TestCase
{
    use JournalRequestDataTrait;

    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockJournalRequestCommonTrait::class)
                            ->setMethods([
                                'getRequest',
                                'post'
                            ])
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetRequestCommonData()
    {
        $data = $this->getTestRequestCommonData();
       
        $statusEncode = marmot_encode($data['status']);

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($data['title']);
        $request->post(Argument::exact('source'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['source']);
        $request->post(Argument::exact('description'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['description']);
        $request->post(Argument::exact('year'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['year']);
        $request->post(
            Argument::exact('cover'),
            Argument::exact(array())
        )->shouldBeCalledTimes(1)->willReturn($data['cover']);
        $request->post(Argument::exact('attachment'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['attachment']);
        $request->post(Argument::exact('authImages'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['authImages']);
        $request->post(Argument::exact('status'), Argument::exact(0))
                ->shouldBeCalledTimes(1)
                ->willReturn($statusEncode);

        $this->trait->expects($this->any())->method('getRequest')->willReturn($request->reveal());

        $result = $this->trait->publicGetRequestCommonData();

        $this->assertEquals($data, $result);
    }
}
