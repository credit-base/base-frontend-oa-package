<?php
namespace Base\Package\Common\Utils;

use PHPUnit\Framework\TestCase;

use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;

use Sdk\Common\Persistence\UtilsSession;

class CaptchaTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockCaptcha();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testRender()
    {
        $this->trait = $this->getMockBuilder(MockCaptcha::class)
                                ->setMethods(['getPhraseBuilder','getCaptchaBuilder',
                                'getBuildPhrase','phraseOutPut'])
                                ->getMock();

        $phraseBuilder = new PhraseBuilder(4);
        $captchaBuilder = new CaptchaBuilder(null, $phraseBuilder);

        $this->trait->expects($this->exactly(1))
                ->method('getPhraseBuilder')
                ->willReturn($phraseBuilder);
        
        $this->trait->expects($this->exactly(1))
                ->method('getCaptchaBuilder')
                ->with($phraseBuilder)
                ->willReturn($captchaBuilder);

        $this->trait->expects($this->exactly(1))
                ->method('getBuildPhrase')
                ->with($captchaBuilder)
                ->willReturn($captchaBuilder);
        $this->trait->expects($this->exactly(1))
                ->method('phraseOutPut')
                ->with($captchaBuilder);

        $result = $this->trait->render();
        $this->assertNull($result);
    }

    public function testGetPhraseBuilder()
    {
        $this->assertInstanceof(
            'Gregwar\Captcha\PhraseBuilder',
            $this->trait->getPhraseBuilder()
        );
    }

    public function testGetCaptchaBuilder()
    {
        $phraseBuilder = new PhraseBuilder(4);
        $this->assertInstanceof(
            'Gregwar\Captcha\CaptchaBuilder',
            $this->trait->getCaptchaBuilder($phraseBuilder)
        );
    }

    public function testGetBuildPhrase()
    {
        $this->trait = $this->getMockBuilder(MockCaptcha::class)
        ->setMethods(['getPhrase'])
        ->getMock();

        $builder = $this->prophesize(CaptchaBuilder::class);

        $builder->setMaxBehindLines(0)->shouldBeCalledTimes(1);
        $builder->setMaxFrontLines(0)->shouldBeCalledTimes(1);
        $builder->build()->shouldBeCalledTimes(1);
        $builder->getPhrase()->shouldBeCalledTimes(1)->willReturn('string');

        $this->trait->getBuildPhrase($builder->reveal());
    }

    public function testValidateTrue()
    {
        $phrase = 'string';
        $session = new UtilsSession();
        $session->save('captcha', $phrase);

        $result = $this->trait->validate($phrase);
        $this->assertTrue($result);
    }
}
