<?php
namespace Base\Package\Common\Utils;

use PHPUnit\Framework\TestCase;

class RandomTokenTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockRandomTokenTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testRandomNumber()
    {
        $result = $this->trait->publicRandomNumber(6);

        $this->assertIsString($result);
    }
}
