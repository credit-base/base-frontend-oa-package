<?php
namespace Base\Package\Common\Utils;

use PHPUnit\Framework\TestCase;

class MaskTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new Mask();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testMask()
    {
        $result = $this->trait->mask('string', 1);
        $this->assertIsString($result);
    }

    public function testSubstrReplaceCn()
    {
        $result = $this->trait->substrReplaceCn('string', '*', 1);
        $this->assertIsString($result);
    }

    public function testSubstrReplaceCnIsNull()
    {
        $result = $this->trait->substrReplaceCn('');
        $this->assertEquals('', $result);
    }

    public function testSubstrReplaceCnWithLength()
    {
        $result = $this->trait->substrReplaceCn('string', '*', 1, 2);
        $this->assertIsString($result);
    }
}
