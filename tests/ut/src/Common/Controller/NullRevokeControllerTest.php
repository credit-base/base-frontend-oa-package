<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class NullRevokeControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new NullRevokeController();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->controller);
    }

    public function testImplementIRevokeAbleController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IRevokeAbleController',
            $this->controller
        );
    }

    public function testImplementINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->controller
        );
    }

    public function testRevoke()
    {
        $this->controller->revoke(0);
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }
}
