<?php
namespace Base\Package\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Package\Common\Controller\Interfaces\IDeleteAbleController;

class DeleteControllerTest extends TestCase
{
    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(DeleteController::class)
                                 ->setMethods(['getDeleteController','displayError'])
                                 ->getMock();

        $this->childController = new class extends DeleteController
        {
            public function getDeleteController(string $resource) : IDeleteAbleController
            {
                return parent::getDeleteController($resource);
            }
        };

        Core::setLastError(ERROR_NOT_DEFINED);

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testGetDeleteController()
    {
        $resource = 'gbSearchData';
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IDeleteAbleController',
            $this->childController->getDeleteController($resource)
        );
    }

    /**
     * 错误格式id
     * 1. 不调用getDeleteController
     * 2. 调用displayError
     */
    public function testDeleteInvalidId()
    {
        $id = 1;
        
        $this->invalidExpects($id, 'delete');
        $this->controller->delete($this->resource, $id, 'delete');
    }

    public function testDeleteValidId()
    {
        $id = marmot_encode(1);

        $this->validExpects(marmot_decode($id), 'delete');
        $this->controller->delete($this->resource, $id, 'delete');
    }

    private function invalidExpects($id, $status)
    {
        $statusController = $this->prophesize(IDeleteAbleController::class);
        $statusController->$status(Argument::exact($id), Argument::exact($this->resource))->shouldBeCalledTimes(0);

        $this->controller->expects($this->exactly(0))
                         ->method('getDeleteController')
                         ->with($this->resource)
                         ->willReturn($statusController->reveal());

        $this->controller->expects($this->once())
                         ->method('displayError');
    }

    private function validExpects($id, $status)
    {
        $statusController = $this->prophesize(IDeleteAbleController::class);
        $statusController->$status(Argument::exact($id), Argument::exact($this->resource))->shouldBeCalledTimes(1);

        $this->controller->expects($this->exactly(1))
                         ->method('getDeleteController')
                         ->with($this->resource)
                         ->willReturn($statusController->reveal());

        $this->controller->expects($this->exactly(0))
                         ->method('displayError');
    }
}
