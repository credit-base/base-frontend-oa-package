<?php
namespace Base\Package\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Package\Common\Controller\Interfaces\IRevokeAbleController;

class RevokeControllerTest extends TestCase
{
    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(RevokeController::class)
                                 ->setMethods(['getRevokeController','displayError'])
                                 ->getMock();

        $this->childController = new class extends RevokeController
        {
            public function getRevokeController(string $resource) : IRevokeAbleController
            {
                return parent::getRevokeController($resource);
            }
        };

        Core::setLastError(ERROR_NOT_DEFINED);

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testGetRevokeController()
    {
        $resource = 'unAuditedBjRules';
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IRevokeAbleController',
            $this->childController->getRevokeController($resource)
        );
    }

    /**
     * 错误格式id
     * 1. 不调用getRevokeController
     * 2. 调用displayError
     */
    public function testRevokeInvalidId()
    {
        $id = 1;
        
        $this->invalidExpects($id, 'revoke');
        $this->controller->revoke($this->resource, $id, 'revoke');
    }

    public function testRevokeValidId()
    {
        $id = marmot_encode(1);

        $this->validExpects(marmot_decode($id), 'revoke');
        $this->controller->revoke($this->resource, $id, 'revoke');
    }

    private function invalidExpects($id, $status)
    {
        $statusController = $this->prophesize(IRevokeAbleController::class);
        $statusController->$status(Argument::exact($id), Argument::exact($this->resource))->shouldBeCalledTimes(0);

        $this->controller->expects($this->exactly(0))
                         ->method('getRevokeController')
                         ->with($this->resource)
                         ->willReturn($statusController->reveal());

        $this->controller->expects($this->once())
                         ->method('displayError');
    }

    private function validExpects($id, $status)
    {
        $statusController = $this->prophesize(IRevokeAbleController::class);
        $statusController->$status(Argument::exact($id), Argument::exact($this->resource))->shouldBeCalledTimes(1);

        $this->controller->expects($this->exactly(1))
                         ->method('getRevokeController')
                         ->with($this->resource)
                         ->willReturn($statusController->reveal());

        $this->controller->expects($this->exactly(0))
                         ->method('displayError');
    }
}
