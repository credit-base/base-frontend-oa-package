<?php
namespace Base\Package\Common\Controller\Traits;

use PHPUnit\Framework\TestCase;

use Sdk\CreditPhotography\Model\CreditPhotography;

class ToolTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockToolTrait();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplodeArray()
    {
        $encodeData = ['MA','MQ'];
        $data = "1,2";
        //验证
        $result = $this->stub->publicImplodeArray($encodeData);
        
        $this->assertEquals($result, $data);
    }

    public function testGetImplodeIds()
    {
        $data = new CreditPhotography(1);
        $data = [$data];
        //验证
        $result = $this->stub->publicGetImplodeIds($data);
       
        $this->assertEquals($result, 1);
    }

    public function testFormatString()
    {
        $encodeData = "1,2";
        $data = "1,2";
        //验证
        $result = $this->stub->publicFormatString($encodeData);
        
        $this->assertEquals($result, $data);
    }
}
