<?php
namespace Base\Package\Common\Controller\Traits;

use PHPUnit\Framework\TestCase;

class RevokeControllerTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockRevokeController::class)
        ->setMethods(
            [
                'revokeAction',
                'displayError',
                'displaySuccess',
                'globalCheck'
            ]
        )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testRevoke($action, $expected)
    {
        $id = 1;
        $resource = 'resource';

        $this->stub->expects($this->exactly(1))
            ->method('globalCheck')
            ->with($resource)
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('revokeAction')
            ->with($id)
            ->will($this->returnValue($action));

        if ($expected) {
            $this->stub->expects($this->exactly(1))
                 ->method('displaySuccess');
        }
        if (!$expected) {
            $this->stub->expects($this->exactly(1))
                 ->method('displayError');
        }

        $this->stub->revoke($id, $resource);
    }

    public function dataProvider()
    {
        return [
            [false, false],
            [true, true]
        ];
    }
}
