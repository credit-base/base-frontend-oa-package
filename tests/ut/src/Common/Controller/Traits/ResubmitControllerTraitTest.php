<?php
namespace Base\Package\Common\Controller\Traits;

use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;

class ResubmitControllerTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockResubmitController::class)
        ->setMethods(
            [
                'getRequest',
                'resubmitAction',
                'globalCheck'
            ]
        )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testResubmitAction($action, $expected)
    {
        $id = 1;
        $resource = 'resource';
        $request = $this->prophesize(Request::class);

        $this->stub->expects($this->any())->method('getRequest')->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('globalCheck')
            ->with($resource)
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('resubmitAction')
            ->with($id)
            ->will($this->returnValue($action));

        $result =$this->stub->resubmit($id, $resource);

        $this->assertEquals($expected, $result);
    }

    public function dataProvider()
    {
        return [
            [false, false],
            [true, true]
        ];
    }
}
