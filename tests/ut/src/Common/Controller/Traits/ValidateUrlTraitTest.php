<?php
namespace Base\Package\Common\Controller\Traits;

use PHPUnit\Framework\TestCase;

use Sdk\Common\WidgetRules\WidgetRules;

class ValidateUrlTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockValidateUrlTrait();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetWidgetRules()
    {
        $this->assertInstanceOf(
            'Sdk\Common\WidgetRules\WidgetRules',
            $this->stub->publicGetWidgetRules()
        );
    }

    public function testValidateIndexScenario()
    {
        $stub = $this->getMockBuilder(MockValidateUrlTrait::class)
            ->setMethods(
                [
                    'getWidgetRules'
                ]
            )->getMock();

        $id = 1;

        $widgetRules = $this->prophesize(WidgetRules::class);

        $widgetRules->formatNumeric($id, 'id')->shouldBeCalledTimes(1)->willReturn(true);

        $stub->expects($this->exactly(1))
            ->method('getWidgetRules')
            ->willReturn($widgetRules->reveal());

        //验证
        $result = $stub->publicValidateIndexScenario($id);
        $this->assertTrue($result);
    }
}
