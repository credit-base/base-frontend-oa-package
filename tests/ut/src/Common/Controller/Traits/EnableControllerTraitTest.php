<?php
namespace Base\Package\Common\Controller\Traits;

use PHPUnit\Framework\TestCase;

class EnableControllerTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEnableController::class)
        ->setMethods(
            [
                'enableAction',
                'disableAction',
                'displayError',
                'displaySuccess',
                'globalCheck'
            ]
        )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testEnable($action, $expected)
    {
        $id = 1;
        $resource = 'resource';

        $this->stub->expects($this->exactly(1))
            ->method('globalCheck')
            ->with($resource)
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('enableAction')
            ->with($id)
            ->will($this->returnValue($action));

        if ($expected) {
            $this->stub->expects($this->exactly(1))
                 ->method('displaySuccess');
        }
        if (!$expected) {
            $this->stub->expects($this->exactly(1))
                 ->method('displayError');
        }

        $this->stub->enable($id, $resource);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testDisable($action, $expected)
    {
        $id = 1;
        $resource = 'resource';

        $this->stub->expects($this->exactly(1))
            ->method('globalCheck')
            ->with($resource)
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('disableAction')
            ->with($id)
            ->will($this->returnValue($action));

        if ($expected) {
            $this->stub->expects($this->exactly(1))
                 ->method('displaySuccess');
        }
        if (!$expected) {
            $this->stub->expects($this->exactly(1))
                 ->method('displayError');
        }

        $this->stub->disable($id, $resource);
    }

    public function dataProvider()
    {
        return [
            [false, false],
            [true, true]
        ];
    }
}
