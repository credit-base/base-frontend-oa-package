<?php
namespace Base\Package\Common\Controller\Traits;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\Guest;

class GlobalCheckTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockGlobalCheckTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGlobalCheck()
    {
        $this->trait = $this->getMockBuilder(MockGlobalCheckTrait::class)
        ->setMethods(
            [
                'checkUserExist',
                'checkUserStatusEnable',
                'checkUserHasPurview'
            ]
        )->getMock();

        $resource = 'resource';

        $this->trait->expects($this->exactly(1))
            ->method('checkUserExist')
            ->willReturn(true);

        $this->trait->expects($this->exactly(1))
            ->method('checkUserStatusEnable')
            ->willReturn(true);

        $this->trait->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->with($resource)
            ->willReturn(true);

        $result = $this->trait->globalCheck($resource);

        $this->assertTrue($result);
    }

    public function testCheckUserExistTrue()
    {
        Core::$container->set('crew', new Crew(1));

        $result = $this->trait->publicCheckUserExist();

        $this->assertTrue($result);
    }

    public function testCheckUserExistFalse()
    {
        Core::$container->set('crew', new Guest());

        $result = $this->trait->publicCheckUserExist();

        // $this->assertEquals(Core::getLastError()->getId(),NEED_SIGNIN);
        $this->assertFalse($result);
    }

    public function testCheckUserStatusEnableTrue()
    {
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setStatus(0);

        $result = $this->trait->publicCheckUserStatusEnable();

        $this->assertTrue($result);
    }

    public function testCheckUserStatusEnableFalse()
    {
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setStatus(-2);

        $result = $this->trait->publicCheckUserStatusEnable();

        // $this->assertEquals(Core::getLastError()->getId(),USER_STATUS_DISABLE);
        $this->assertFalse($result);
    }

    public function testCheckUserHasPurviewSuccess()
    {
        $resource = 'enterprises';
        Core::$container->set('purview.status', false);

        $this->assertTrue($this->trait->publicCheckUserHasPurview($resource));
    }

    public function testCheckUserHasPurviewFalse()
    {
        $resource = 'string';
        Core::$container->set('crew', new Crew(1));
        Core::$container->set('purview.status', true);
        Core::$container->get('crew')->setCategory(2);

        $this->assertFalse($this->trait->publicCheckUserHasPurview($resource));
    }
}
