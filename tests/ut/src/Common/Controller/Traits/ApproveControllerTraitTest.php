<?php
namespace Base\Package\Common\Controller\Traits;

use PHPUnit\Framework\TestCase;

class ApproveControllerTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveController::class)
        ->setMethods(
            [
                'approveAction',
                'rejectAction',
                'displayError',
                'displaySuccess',
                'globalCheck'
            ]
        )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testApprove($action, $expected)
    {
        $id = 1;
        $resource = 'resource';

        $this->stub->expects($this->exactly(1))
            ->method('globalCheck')
            ->with($resource)
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('approveAction')
            ->with($id)
            ->will($this->returnValue($action));

        if ($expected) {
            $this->stub->expects($this->exactly(1))
                 ->method('displaySuccess');
        }
        if (!$expected) {
            $this->stub->expects($this->exactly(1))
                 ->method('displayError');
        }

        $this->stub->approve($id, $resource);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testReject($action, $expected)
    {
        $id = 1;
        $resource = 'resource';

        $this->stub->expects($this->exactly(1))
            ->method('globalCheck')
            ->with($resource)
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('rejectAction')
            ->with($id)
            ->will($this->returnValue($action));

        if ($expected) {
            $this->stub->expects($this->exactly(1))
                 ->method('displaySuccess');
        }
        if (!$expected) {
            $this->stub->expects($this->exactly(1))
                 ->method('displayError');
        }

        $this->stub->reject($id, $resource);
    }

    public function dataProvider()
    {
        return [
            [false, false],
            [true, true]
        ];
    }
}
