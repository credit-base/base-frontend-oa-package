<?php
namespace Base\Package\Common\Controller\Traits;

use PHPUnit\Framework\TestCase;

use Sdk\Common\Persistence\UtilsSession;

class CryptojsTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockCryptojsTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }
    
    public function testDecrypt()
    {
        $jsonString =
            '{"ct":"FTL1KxRHLHZxMjTgCGTeMw==","iv":"cfa6b5e36d3abf59127590f36cd8d6b7","s":"19a2fa0c18b33830"}';
        $hash = 'hgEfBTuHBL8hCGrwtLcjyOhQQW390BVTF9j2EXJN3NAyTmEpHv';
        $session = new UtilsSession();
        $session->save('cryptojs_hash', $hash);
        $result = $this->trait->publicDecrypt($jsonString);
       
        $this->assertIsString($result);
    }

    public function testDecryptIsNull()
    {
        $jsonString = '{"ct":"FTL1KxRHLHZxMjTgCGTeMw==","iv":"cfa6b5e36d3abf59127590f36cd8d6b7"}';
        ;
 
        $result = $this->trait->publicDecrypt($jsonString);
        
        $this->assertNull($result);
    }

    public function testHash()
    {
        $result = $this->trait->publicHash();

        $this->assertIsString($result);
    }
}
