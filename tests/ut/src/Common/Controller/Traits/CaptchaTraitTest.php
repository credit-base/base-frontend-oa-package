<?php
namespace Base\Package\Common\Controller\Traits;

use PHPUnit\Framework\TestCase;

class CaptchaTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockCaptchaTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testValidateCaptcha()
    {
        $phrase = 'phrase';

        $result = $this->trait->publicValidateCaptcha($phrase);

        $this->assertIsBool($result);
    }
}
