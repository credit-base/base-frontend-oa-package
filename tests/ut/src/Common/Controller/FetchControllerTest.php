<?php
namespace Base\Package\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

class FetchControllerTest extends TestCase
{
    private $controller;

    private $childController;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(FetchController::class)
                          ->setMethods(['getFetchController','displayError'])
                          ->getMock();

        $this->childController = new class extends FetchController
        {
            public function getFetchController(string $resource) : IFetchAbleController
            {
                return parent::getFetchController($resource);
            }
        };
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
    }

    public function testGetFetchController()
    {
        $resource = 'members';
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IFetchAbleController',
            $this->childController->getFetchController($resource)
        );
    }

    public function testFilter()
    {
        $resource = 'test';

        $fetchController = $this->prophesize(IFetchAbleController::class);
        $fetchController->filter($resource)->shouldBeCalledTimes(1);

        $this->controller->expects($this->once())
                         ->method('getFetchController')
                         ->with($resource)
                         ->willReturn($fetchController->reveal());

        $this->controller->filter($resource);
    }

    public function testFetchOneValidId()
    {
        $resource = 'test';
        $id = marmot_encode(1);

        $fetchController = $this->prophesize(IFetchAbleController::class);
        $fetchController->fetchOne(
            Argument::exact(marmot_decode($id)),
            Argument::exact($resource)
        )->shouldBeCalledTimes(1);

        $this->controller->expects($this->once())
                         ->method('getFetchController')
                         ->with($resource)
                         ->willReturn($fetchController->reveal());
        $this->controller->expects($this->exactly(0))
                         ->method('displayError');

        $this->controller->fetchOne($resource, $id);
    }

    public function testFetchOneInvalidId()
    {
        $resource = 'test';
        $id = 1;

        $fetchController = $this->prophesize(IFetchAbleController::class);
        $fetchController->fetchOne(
            Argument::exact($id),
            Argument::exact($resource)
        )->shouldBeCalledTimes(0);

        $this->controller->expects($this->exactly(0))
                         ->method('getFetchController')
                         ->with($resource)
                         ->willReturn($fetchController->reveal());
        $this->controller->expects($this->exactly(1))
                         ->method('displayError');

        $this->controller->fetchOne($resource, $id);
    }
}
