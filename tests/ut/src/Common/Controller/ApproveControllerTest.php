<?php
namespace Base\Package\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Package\Common\Controller\Interfaces\IApproveAbleController;

class ApproveControllerTest extends TestCase
{
    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(ApproveController::class)
                                 ->setMethods(['getApproveController','displayError'])
                                 ->getMock();

        $this->childController = new class extends ApproveController
        {
            public function getApproveController(string $resource) : IApproveAbleController
            {
                return parent::getApproveController($resource);
            }
        };

        Core::setLastError(ERROR_NOT_DEFINED);

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testGetApproveController()
    {
        $resource = 'unAuditedNews';
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IApproveAbleController',
            $this->childController->getApproveController($resource)
        );
    }

    /**
     * 错误格式id
     * 1. 不调用getApproveController
     * 2. 调用displayError
     */
    public function testApproveInvalidId()
    {
        $id = 1;
        
        $this->invalidExpects($id, 'approve');
        $this->controller->index($this->resource, $id, 'approve');
    }

    public function testApproveValidId()
    {
        $id = marmot_encode(1);

        $this->validExpects(marmot_decode($id), 'approve');
        $this->controller->index($this->resource, $id, 'approve');
    }

    public function testRejectInvalidId()
    {
        $id = 1;

        $this->invalidExpects($id, 'reject');
        $this->controller->index($this->resource, $id, 'reject');
    }

    private function invalidExpects($id, $status)
    {
        $statusController = $this->prophesize(IApproveAbleController::class);
        $statusController->$status(Argument::exact($id), Argument::exact($this->resource))->shouldBeCalledTimes(0);

        $this->controller->expects($this->exactly(0))
                         ->method('getApproveController')
                         ->with($this->resource)
                         ->willReturn($statusController->reveal());

        $this->controller->expects($this->once())
                         ->method('displayError');
    }

    private function validExpects($id, $status)
    {
        $statusController = $this->prophesize(IApproveAbleController::class);
        $statusController->$status(Argument::exact($id), Argument::exact($this->resource))->shouldBeCalledTimes(1);

        $this->controller->expects($this->exactly(1))
                         ->method('getApproveController')
                         ->with($this->resource)
                         ->willReturn($statusController->reveal());

        $this->controller->expects($this->exactly(0))
                         ->method('displayError');
    }
}
