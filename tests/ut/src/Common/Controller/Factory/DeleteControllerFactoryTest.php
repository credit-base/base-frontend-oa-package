<?php
namespace Base\Package\Common\Controller\Factory;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DeleteControllerFactoryTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new DeleteControllerFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $controller = $this->controller->getDeleteController('');
            $this->assertInstanceOf(
                'Base\Package\Common\Controller\NullDeleteController',
                $controller
            );
    }

    public function testGetDeleteController()
    {
        foreach (DeleteControllerFactory::MAPS as $key => $controller) {
            $this->assertInstanceOf(
                $controller,
                $this->controller->getDeleteController($key)
            );
        }
    }
}
