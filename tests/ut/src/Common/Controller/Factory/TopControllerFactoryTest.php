<?php
namespace Base\Package\Common\Controller\Factory;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class TopControllerFactoryTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new TopControllerFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $controller = $this->controller->getTopController('');
            $this->assertInstanceOf(
                'Base\Package\Common\Controller\NullTopController',
                $controller
            );
    }

    public function testGetEnableController()
    {
        foreach (TopControllerFactory::MAPS as $key => $controller) {
            $this->assertInstanceOf(
                $controller,
                $this->controller->getTopController($key)
            );
        }
    }
}
