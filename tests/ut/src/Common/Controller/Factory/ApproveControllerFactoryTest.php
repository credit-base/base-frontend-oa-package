<?php
namespace Base\Package\Common\Controller\Factory;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ApproveControllerFactoryTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new ApproveControllerFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $controller = $this->controller->getApproveController('');
            $this->assertInstanceOf(
                'Base\Package\Common\Controller\NullApproveController',
                $controller
            );
    }

    public function testGetEnableController()
    {
        foreach (ApproveControllerFactory::MAPS as $key => $controller) {
            $this->assertInstanceOf(
                $controller,
                $this->controller->getApproveController($key)
            );
        }
    }
}
