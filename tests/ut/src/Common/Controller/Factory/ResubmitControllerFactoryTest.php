<?php
namespace Base\Package\Common\Controller\Factory;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ResubmitControllerFactoryTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new ResubmitControllerFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $controller = $this->controller->getResubmitController('');
            $this->assertInstanceOf(
                'Base\Package\Common\Controller\NullResubmitController',
                $controller
            );
    }

    public function testGetResubmitController()
    {
        foreach (ResubmitControllerFactory::MAPS as $key => $controller) {
            $this->assertInstanceOf(
                $controller,
                $this->controller->getResubmitController($key)
            );
        }
    }
}
