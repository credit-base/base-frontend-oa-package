<?php
namespace Base\Package\Common\Controller\Factory;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class RevokeControllerFactoryTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new RevokeControllerFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $controller = $this->controller->getRevokeController('');
            $this->assertInstanceOf(
                'Base\Package\Common\Controller\NullRevokeController',
                $controller
            );
    }

    public function testGetRevokeController()
    {
        foreach (RevokeControllerFactory::MAPS as $key => $controller) {
            $this->assertInstanceOf(
                $controller,
                $this->controller->getRevokeController($key)
            );
        }
    }
}
