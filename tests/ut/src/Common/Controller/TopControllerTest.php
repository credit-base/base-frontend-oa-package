<?php
namespace Base\Package\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Package\Common\Controller\Interfaces\ITopAbleController;

class TopControllerTest extends TestCase
{
    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(TopController::class)
                                 ->setMethods(['getTopController','displayError'])
                                 ->getMock();

        $this->childController = new class extends TopController
        {
            public function getTopController(string $resource) : ITopAbleController
            {
                return parent::getTopController($resource);
            }
        };

        Core::setLastError(ERROR_NOT_DEFINED);

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testGetTopController()
    {
        $resource = 'news';
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\ITopAbleController',
            $this->childController->getTopController($resource)
        );
    }

    /**
     * 错误格式id
     * 1. 不调用getTopController
     * 2. 调用displayError
     */
    public function testTopInvalidId()
    {
        $id = 1;
        
        $this->invalidExpects($id, 'top');
        $this->controller->index($this->resource, $id, 'top');
    }

    public function testTopValidId()
    {
        $id = marmot_encode(1);

        $this->validExpects(marmot_decode($id), 'top');
        $this->controller->index($this->resource, $id, 'top');
    }

    public function testCancelTopInvalidId()
    {
        $id = 1;

        $this->invalidExpects($id, 'cancelTop');
        $this->controller->index($this->resource, $id, 'cancelTop');
    }

    private function invalidExpects($id, $status)
    {
        $statusController = $this->prophesize(ITopAbleController::class);
        $statusController->$status(Argument::exact($id), Argument::exact($this->resource))->shouldBeCalledTimes(0);

        $this->controller->expects($this->exactly(0))
                         ->method('getTopController')
                         ->with($this->resource)
                         ->willReturn($statusController->reveal());

        $this->controller->expects($this->once())
                         ->method('displayError');
    }

    private function validExpects($id, $status)
    {
        $statusController = $this->prophesize(ITopAbleController::class);
        $statusController->$status(Argument::exact($id), Argument::exact($this->resource))->shouldBeCalledTimes(1);

        $this->controller->expects($this->exactly(1))
                         ->method('getTopController')
                         ->with($this->resource)
                         ->willReturn($statusController->reveal());

        $this->controller->expects($this->exactly(0))
                         ->method('displayError');
    }
}
