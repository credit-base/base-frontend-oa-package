<?php
namespace Base\Package\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Controller\WebTrait;
use Base\Package\Common\Controller\Interfaces\IResubmitAbleController;

class ResubmitControllerTest extends TestCase
{
    use WebTrait;

    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(ResubmitController::class)
                ->setMethods(['getResubmitController','displayError', 'validateIndexScenario'])
                ->getMock();
                                 
        $this->childController = new class extends ResubmitController
        {
            public function getResubmitController(string $resource) : IResubmitAbleController
            {
                return parent::getResubmitController($resource);
            }
        };

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
    }

    public function testGetResubmitController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IResubmitAbleController',
            $this->childController->getResubmitController($this->resource)
        );
    }

    public function testResubmitSuccess()
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $this->controller->expects($this->once())
            ->method('validateIndexScenario')
            ->with($id)
            ->willReturn(true);

        $resubmitController = $this->prophesize(IResubmitAbleController::class);
        $resubmitController->resubmit($id, $this->resource)->shouldBeCalledTimes(1)->willReturn(true);
        $this->controller->expects($this->once())
            ->method('getResubmitController')
            ->with($this->resource)
            ->willReturn($resubmitController->reveal());

        $result = $this->controller->resubmit($this->resource, $idEncode);
        $this->assertTrue($result);
    }

    public function testResubmitFail()
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $this->controller->expects($this->once())
            ->method('validateIndexScenario')
            ->with($id)
            ->willReturn(false);

        $this->controller->expects($this->once())->method('displayError');

        $result = $this->controller->resubmit($this->resource, $idEncode);
        $this->assertFalse($result);
    }
}
