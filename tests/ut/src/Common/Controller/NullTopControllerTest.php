<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class NullTopControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new NullTopController();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->controller);
    }

    public function testImplementITopController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\ITopAbleController',
            $this->controller
        );
    }

    public function testImplementINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->controller
        );
    }

    public function testTop()
    {
        $this->controller->top(0);
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }

    public function testCancelTop()
    {
        $this->controller->cancelTop(0);
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }
}
