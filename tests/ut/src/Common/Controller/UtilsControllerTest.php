<?php
namespace Base\Package\Common\Controller;

use PHPUnit\Framework\TestCase;
use Base\Package\Common\Utils\Captcha;
use Base\Package\Common\View\Json\UtilsView;

class UtilsControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(UtilsController::class)
                                 ->setMethods(['render'])
                                 ->getMock();
        $this->childController = new class extends UtilsController
        {
            public function getCaptcha() : Captcha
            {
                return parent::getCaptcha();
            }
        };
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testGetHash()
    {
        $data = array(1,2);
        $this->controller->expects($this->exactly(1))
            ->method('render')
            ->with(new UtilsView($data));
        $result = $this->controller->getHash();
        $this->assertTrue($result);
    }

    public function testCaptcha()
    {
        $this->controller = $this->getMockBuilder(UtilsController::class)
                                ->setMethods(['getCaptcha'])
                                ->getMock();
                            
        $captcha = $this->prophesize(Captcha::class);

        $captcha->render()->shouldBeCalledTimes(1);

        $this->controller->expects($this->exactly(1))
                ->method('getCaptcha')
                ->willReturn($captcha->reveal());

        $result = $this->controller->captcha();
        
        $this->assertNull($result);
    }

    public function testGetCaptcha()
    {
        $this->assertInstanceof(
            'Base\Package\Common\Utils\Captcha',
            $this->childController->getCaptcha()
        );
    }
}
