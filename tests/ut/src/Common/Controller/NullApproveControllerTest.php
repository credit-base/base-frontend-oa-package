<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class NullApproveControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new NullApproveController();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->controller);
    }

    public function testImplementIApproveController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IApproveAbleController',
            $this->controller
        );
    }

    public function testImplementINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->controller
        );
    }

    public function testApprove()
    {
        $this->controller->approve(0);
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }

    public function testReject()
    {
        $this->controller->reject(0);
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }
}
