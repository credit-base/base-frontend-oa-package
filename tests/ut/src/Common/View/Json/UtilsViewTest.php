<?php
namespace Base\Package\Common\View\Json;

use PHPUnit\Framework\TestCase;

class UtilsViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockUtilsView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetData()
    {
        $result = $this->view->getData();

        $this->assertIsArray($result);
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(UtilsView::class)
            ->setMethods(
                [
                    'encode',
                    'getData',
                ]
            )
            ->getMock();

        $data = ["hash"=>"9mKhO2SKSuRTrmP14gdyF3rL08e8lAzdeTaxMCYqXipl5MZ2wP"];

        $this->view->expects($this->exactly(1))
            ->method('getData')->willReturn($data);

        $this->view->expects($this->exactly(1))->method('encode')->with($data);

        $this->assertNull($this->view->display());
    }
}
