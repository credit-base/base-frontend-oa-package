<?php


namespace Base\Package\Member\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Member\Translator\MemberTranslator;
use Sdk\Member\Model\Member;

class ViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetMember()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Model\Member',
            $this->view->getMember()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Translator\MemberTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(['getTranslator','getMember','encode'])
            ->getMock();

        $news = new Member();
        $newsData = [];

        $translator = $this->prophesize(MemberTranslator::class);
        $translator->objectToArray(
            $news,
            array(
                'id',
                'userName',
                'realName',
                'cardId',
                'cellphone',
                'email',
                'contactAddress',
                'gender',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getMember')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
