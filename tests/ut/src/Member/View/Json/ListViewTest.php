<?php
namespace Base\Package\Member\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Member\Translator\MemberTranslator;
use Sdk\Member\Model\Member;

class ListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetMember()
    {
        $this->assertIsArray($this->view->getMember());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Translator\MemberTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getTranslator', 'getMember', 'encode','getCount'])
            ->getMock();

        $new = new Member();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(MemberTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                'id',
                'userName',
                'realName',
                'cellphone',
                'gender',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getMember')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
