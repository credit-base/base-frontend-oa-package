<?php
namespace Base\Package\Member\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;

use Sdk\Member\Command\Member\EnableMemberCommand;
use Sdk\Member\Command\Member\DisableMemberCommand;

class MemberEnableControllerTest extends TestCase
{
    private $memberStub;

    public function setUp()
    {
        $this->memberStub = $this->getMockBuilder(MockMemberEnableController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->memberStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new MemberEnableController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new MemberEnableController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IEnableAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $memberStub = new MockMemberEnableController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $memberStub->getCommandBus()
        );
    }

    public function testEnableAction()
    {
        $id = 1;

        $command = new EnableMemberCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->memberStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->memberStub->enableAction($id);
        $this->assertTrue($result);
    }

    public function testDisableAction()
    {
        $id = 1;

        $command = new DisableMemberCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->memberStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->memberStub->disableAction($id);
        $this->assertTrue($result);
    }
}
