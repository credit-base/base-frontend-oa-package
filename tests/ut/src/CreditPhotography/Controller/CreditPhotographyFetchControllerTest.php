<?php
namespace Base\Package\CreditPhotography\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;

use Sdk\CreditPhotography\Model\CreditPhotography;
use Sdk\CreditPhotography\Model\NullCreditPhotography;
use Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

use Base\Package\CreditPhotography\View\Json\View;
use Base\Package\CreditPhotography\View\Json\ListView;

class CreditPhotographyFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new CreditPhotographyFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\CreditPhotography\Repository\CreditPhotographyRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $creditPhotography = new NullCreditPhotography();

        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->scenario(Argument::exact(CreditPhotographyRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($creditPhotography);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $creditPhotography = new CreditPhotography($id);

        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->scenario(
            Argument::exact(CreditPhotographyRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($creditPhotography);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($creditPhotography));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter['realName'] = 'realName';
        $filter['applyStatus'] = '0,-2';
        $filter['status'] = 0;
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $creditPhotographyArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->scenario(Argument::exact(CreditPhotographyRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$creditPhotographyArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($creditPhotographyArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $sort = '-updateTime';
        $realName = 'realName';
        $applyStatus = 2;
        $applyStatusEncode = marmot_encode($applyStatus);
        $status = 0;
        $statusEncode = marmot_encode($status);

        $filter['realName'] = $realName;
        $filter['applyStatus'] = $applyStatus;
        $filter['status'] = $status;

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $request->get(
            Argument::exact('realName'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($realName);

        $request->get(
            Argument::exact('applyStatus'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($applyStatusEncode);

        $request->get(
            Argument::exact('status'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($statusEncode);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort)], $result);
    }
}
