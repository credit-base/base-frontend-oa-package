<?php
namespace Base\Package\CreditPhotography\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Request;

use Sdk\Crew\Model\Crew;
use Sdk\CreditPhotography\Command\CreditPhotography\ApproveCreditPhotographyCommand;
use Sdk\CreditPhotography\Command\CreditPhotography\RejectCreditPhotographyCommand;

use Sdk\Common\WidgetRules\WidgetRules;

class CreditPhotographyApproveControllerTest extends TestCase
{
    private $creditPhotographyStub;

    public function setUp()
    {
        $this->creditPhotographyStub = $this->getMockBuilder(MockCreditPhotographyApproveController::class)
            ->setMethods(
                [
                    'validateRejectScenario',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->creditPhotographyStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new CreditPhotographyApproveController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIApproveAbleController()
    {
        $controller = new CreditPhotographyApproveController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IApproveAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $creditPhotographyStub = new MockCreditPhotographyApproveController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $creditPhotographyStub->getCommandBus()
        );
    }

    public function testApproveAction()
    {
        $id = 1;

        $command = new ApproveCreditPhotographyCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->creditPhotographyStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->creditPhotographyStub->approveAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionSuccess()
    {
        $id = 1;
        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->creditPhotographyStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());
        
        $command = new RejectCreditPhotographyCommand($rejectReason, $id);
       
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->creditPhotographyStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(true);
        $this->creditPhotographyStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->creditPhotographyStub->rejectAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionFail()
    {
        $id = 1;
        
        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->creditPhotographyStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());

        $this->creditPhotographyStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(false);

        $result = $this->creditPhotographyStub->rejectAction($id);
        $this->assertFalse($result);
    }

    public function testGetWidgetRules()
    {
        $this->assertInstanceof(
            'Sdk\Common\WidgetRules\WidgetRules',
            $this->creditPhotographyStub->getWidgetRules()
        );
    }

    public function testvalidateRejectScenario()
    {
        $stub = $this->getMockBuilder(MockCreditPhotographyApproveController::class)
                           ->setMethods(
                               [
                                    'getWidgetRules',
                                ]
                           )
                           ->getMock();

        $rejectReason = "内容不合理";

        $commonWidgetRules = $this->prophesize(WidgetRules::class);

        $commonWidgetRules->reason(
            Argument::exact($rejectReason),
            Argument::exact('rejectReason')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $stub->expects($this->exactly(1))
            ->method('getWidgetRules')
            ->willReturn($commonWidgetRules->reveal());

        $result = $stub->validateRejectScenario($rejectReason);

        $this->assertTrue($result);
    }
}
