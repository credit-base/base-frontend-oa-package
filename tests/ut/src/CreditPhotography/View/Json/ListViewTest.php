<?php
namespace Base\Package\CreditPhotography\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;
use Sdk\CreditPhotography\Model\CreditPhotography;

class ListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetCreditPhotography()
    {
        $this->assertIsArray($this->view->getCreditPhotography());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\Translator\CreditPhotographyTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getTranslator', 'getCreditPhotography', 'encode','getCount'])
            ->getMock();

        $new = new CreditPhotography();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(CreditPhotographyTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                'id',
                'description',
                'applyStatus',
                'rejectReason',
                'member'=>['id','realName'],
                'applyCrew'=>['id','realName'],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getCreditPhotography')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
