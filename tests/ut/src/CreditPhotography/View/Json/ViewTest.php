<?php


namespace Base\Package\CreditPhotography\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;
use Sdk\CreditPhotography\Model\CreditPhotography;

class ViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetCreditPhotography()
    {
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\Model\CreditPhotography',
            $this->view->getCreditPhotography()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\Translator\CreditPhotographyTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(['getTranslator','getCreditPhotography','encode'])
            ->getMock();

        $news = new CreditPhotography();
        $newsData = [];

        $translator = $this->prophesize(CreditPhotographyTranslator::class);
        $translator->objectToArray(
            $news,
            array(
                'id',
                'description',
                'attachments',
                'applyStatus',
                'rejectReason',
                'member'=>['id','realName'],
                'applyCrew'=>['id','realName'],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getCreditPhotography')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
