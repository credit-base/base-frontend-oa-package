<?php
namespace Home\Controller;

use PHPUnit\Framework\TestCase;

class HealthzControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(HealthzController::class)
            ->setMethods()->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new HealthzController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testHealthz()
    {
        $result = $this->stub->healthz();
        $this->assertTrue($result);
    }
}
