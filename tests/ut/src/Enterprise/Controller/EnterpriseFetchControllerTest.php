<?php
namespace Base\Package\Enterprise\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Model\NullEnterprise;
use Sdk\Enterprise\Repository\EnterpriseRepository;

use Base\Package\Enterprise\View\Json\View;
use Base\Package\Enterprise\View\Json\ListView;

use Sdk\Statistical\Model\Statistical;

class EnterpriseFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEnterpriseFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new EnterpriseFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Enterprise\Repository\EnterpriseRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockEnterpriseFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $enterprise = new NullEnterprise();

        $repository = $this->prophesize(EnterpriseRepository::class);
        $repository->scenario(Argument::exact(EnterpriseRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockEnterpriseFetchController::class)
            ->setMethods(['getRepository','render','statisticalNumber'])->getMock();

        $id = 1;
        $enterprise = new Enterprise($id);

        $repository = $this->prophesize(EnterpriseRepository::class);
        $repository->scenario(
            Argument::exact(EnterpriseRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $statisticalNumber = new Statistical(1);
        $unifiedSocialCreditCode = $enterprise->getUnifiedSocialCreditCode();
        $this->stub->expects($this->exactly(1))->method('statisticalNumber')
            ->with($unifiedSocialCreditCode)
            ->willReturn($statisticalNumber);

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($enterprise, $statisticalNumber));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockEnterpriseFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter['realName'] = 'realName';
        $filter['applyStatus'] = '0,-2';
        $filter['status'] = 0;
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $enterpriseArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(EnterpriseRepository::class);
        $repository->scenario(Argument::exact(EnterpriseRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$enterpriseArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($enterpriseArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockEnterpriseFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $sort = '-updateTime';
        $identify = 'identify';

        $filter = array();
        $filter['identify'] = $identify;

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $request->get(
            Argument::exact('identify'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($identify);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort)], $result);
    }

    public function testStatisticalNumber()
    {
        $this->stub = $this->getMockBuilder(MockEnterpriseFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'statistical'
                ]
            )->getMock();

        $unifiedSocialCreditCode = 'unifiedSocialCreditCode';
        $statisticalNumber = new Statistical(1);
        $filter = [];
        $filter['unifiedSocialCreditCode'] = $unifiedSocialCreditCode;

        $this->stub->expects($this->exactly(1))->method('statistical')->with(
            'enterpriseRelationInformationCount',
            $filter
        )->willReturn($statisticalNumber);
        
        $result = $this->stub->statisticalNumber($unifiedSocialCreditCode);

        $this->assertEquals($statisticalNumber, $result);
    }
}
