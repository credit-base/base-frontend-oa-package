<?php
namespace Base\Package\Enterprise\View\Json;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Statistical\Model\Statistical;
use Sdk\Enterprise\Translator\EnterpriseTranslator;

class ViewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockView::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceof(
            'Sdk\Enterprise\Translator\EnterpriseTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testGetEnterprise()
    {
        $result = $this->stub->getEnterprise();
        $this->assertEquals($result, new Enterprise(1));
    }
    
    public function testGetStatisticalNumber()
    {
        $result = $this->stub->getStatisticalNumber();
        $this->assertEquals($result, new Statistical());
    }

    public function testStatisticalNumber()
    {
        $this->stub = $this->getMockBuilder(MockView::class)
            ->setMethods(
                [
                    'statisticalArray',
                    'getStatisticalNumber'
                ]
            )->getMock();
        
        $statisticalNumberArray = array(1,2);

        $this->stub->expects($this->exactly(1))->method('statisticalArray')->willReturn($statisticalNumberArray);
        
        $result = $this->stub->getEnterpriseRelationStaticsData();

        $this->assertEquals($statisticalNumberArray, $result);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockView::class)
            ->setMethods(
                [
                    'getStatisticalNumber',
                    'getEnterpriseRelationStaticsData',
                    'encode',
                    'getTranslator',
                    'getEnterprise'
                ]
            )->getMock();
        
        $enterpriseArray = array(1,22);
        $enterprise = new Enterprise(1);
        $this->stub->expects($this->exactly(1))
                ->method('getEnterprise')
                ->willReturn($enterprise);
        $enterpriseTranslator = $this->prophesize(EnterpriseTranslator::class);
        $enterpriseTranslator->objectToArray(Argument::exact($enterprise))->shouldBeCalledTimes(1)
                ->willReturn($enterpriseArray);
        
        $this->stub->expects($this->exactly(1))
                ->method('getTranslator')
                ->willReturn($enterpriseTranslator->reveal());
        
        $statisticalNumber = new Statistical();
        $this->stub->expects($this->exactly(1))->method('getStatisticalNumber')->willReturn($statisticalNumber);
        $statisticalNumberArray =array(2,3);
        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRelationStaticsData')
            ->willReturn($statisticalNumberArray);

        $this->stub->expects($this->exactly(1))->method('encode');

        $this->assertIsNotBool($this->stub->display());
    }
}
