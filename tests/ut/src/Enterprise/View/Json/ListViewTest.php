<?php
namespace Base\Package\Enterprise\View\Json;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Statistical\Model\Statistical;
use Sdk\Enterprise\Translator\EnterpriseTranslator;

class ListViewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceof(
            'Sdk\Enterprise\Translator\EnterpriseTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testGetEnterprise()
    {
        $result = $this->stub->getEnterprise();
        $this->assertEquals($result, [new Enterprise(1)]);
    }
    
    public function testGetCount()
    {
        $result = $this->stub->getCount();
        $this->assertEquals($result, 1);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)
            ->setMethods(
                [
                    'encode',
                    'getTranslator',
                    'getEnterprise',
                    'getCount'
                ]
            )->getMock();
        $enterprise = new Enterprise(1);
        $enterpriseArray = [$enterprise];
        
        $this->stub->expects($this->exactly(1))
            ->method('getEnterprise')
            ->willReturn($enterpriseArray);

        $array = [[0]];

        $enterpriseTranslator = $this->prophesize(EnterpriseTranslator::class);
        $enterpriseTranslator->objectToArray(Argument::exact($enterprise))->shouldBeCalledTimes(1)
                ->willReturn($array);
        
        $this->stub->expects($this->exactly(1))
                ->method('getTranslator')
                ->willReturn($enterpriseTranslator->reveal());
        $this->stub->expects($this->exactly(1))
                ->method('getCount')
                ->willReturn(1);
        $this->stub->expects($this->exactly(1))->method('encode');

        $this->assertIsNotBool($this->stub->display());
    }
}
