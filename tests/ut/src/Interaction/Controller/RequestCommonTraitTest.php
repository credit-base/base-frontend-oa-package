<?php
namespace Base\Package\Interaction\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Crew\Model\Crew;
use Base\Sdk\Purview\Model\PurviewCategoryFactory;

class RequestCommonTraitTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockRequestCommonTrait::class)
                            ->setMethods([
                                'getRequest',
                                'post'
                            ])
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetReplyRequestCommonData()
    {
        $data = $this->getTestRequestCommonData();
       
        $admissibilityEncode = marmot_encode($data['admissibility']);

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('content'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($data['content']);
        $request->post(Argument::exact('images'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['images']);
        $request->post(Argument::exact('admissibility'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($admissibilityEncode);

        $this->trait->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $result = $this->trait->publicGetReplyRequestCommonData();

        $this->assertEquals($data, $result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockRequestCommonTrait::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

      

        $sort = 'updateTime';
        $title = 'title';
        $acceptStatus = '1';
        $acceptStatusEncode = marmot_encode($acceptStatus);
        $applyStatus = '0';
        $applyStatusEncode = marmot_encode($applyStatus);
        $status = '0';
        $statusEncode = marmot_encode($status);
        $userGroupId = 1;
        $userGroupIdEncode = marmot_encode($userGroupId);

        $id = 1;
        $crew = new Crew($id);
        Core::$container->set('crew', $crew);

        $filter['title'] = $title;
        $filter['acceptUserGroup'] = $userGroupId ;
        $filter['applyUserGroup'] = $userGroupId ;
        $filter['applyStatus'] = $applyStatus;
        $filter['acceptStatus'] = $acceptStatus;
        $filter['status'] = $status;
        

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $request->get(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($title);

        $request->get(
            Argument::exact('applyStatus'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($applyStatusEncode);

        $request->get(
            Argument::exact('acceptStatus'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($acceptStatusEncode);

        $request->get(
            Argument::exact('status'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($statusEncode);

        $request->get(
            Argument::exact('acceptUserGroupId'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userGroupIdEncode);


        $this->stub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $result = $this->stub->publicFilterFormatChange();

        $this->assertEquals([$filter, array($sort)], $result);
    }

    public function testPraisePurview()
    {
        $this->trait = $this->getMockBuilder(MockRequestCommonTrait::class)
            ->setMethods(
                [
                    'getPurviewCategory'
                ]
            )->getMock();

        $resource = 'praises';
        $praisePurview = [24,29];

        $this->trait->expects($this->exactly(1))
                ->method('getPurviewCategory')
                ->with($resource)
                ->willReturn($praisePurview[0]);
        
        $result = $this->trait->publicGetInteractionPurview($resource);
     
        $this->assertEquals($result, $praisePurview);
    }

    public function testComplaintPurview()
    {
        $this->trait = $this->getMockBuilder(MockRequestCommonTrait::class)
            ->setMethods(
                [
                    'getPurviewCategory'
                ]
            )->getMock();

        $resource = 'complaints';
        $complaintPurview = [23,28];

        $this->trait->expects($this->exactly(1))
                ->method('getPurviewCategory')
                ->with($resource)
                ->willReturn($complaintPurview[0]);
        
        $result = $this->trait->publicGetInteractionPurview($resource);
     
        $this->assertEquals($result, $complaintPurview);
    }

    public function testAppealPurview()
    {
        $this->trait = $this->getMockBuilder(MockRequestCommonTrait::class)
            ->setMethods(
                [
                    'getPurviewCategory'
                ]
            )->getMock();

        $resource = 'appeals';
        $appealPurview = [26,31];

        $this->trait->expects($this->exactly(1))
                ->method('getPurviewCategory')
                ->with($resource)
                ->willReturn($appealPurview[0]);
        
        $result = $this->trait->publicGetInteractionPurview($resource);
     
        $this->assertEquals($result, $appealPurview);
    }

    public function testQaPurview()
    {
        $this->trait = $this->getMockBuilder(MockRequestCommonTrait::class)
            ->setMethods(
                [
                    'getPurviewCategory'
                ]
            )->getMock();

        $resource = 'qas';
        $qaPurview = [25,30];

        $this->trait->expects($this->exactly(1))
                ->method('getPurviewCategory')
                ->with($resource)
                ->willReturn($qaPurview[0]);
        
        $result = $this->trait->publicGetInteractionPurview($resource);
     
        $this->assertEquals($result, $qaPurview);
    }

    public function testFeedbackPurview()
    {
        $this->trait = $this->getMockBuilder(MockRequestCommonTrait::class)
            ->setMethods(
                [
                    'getPurviewCategory'
                ]
            )->getMock();

        $resource = 'feedbacks';
        $feedbackPurview = [27,32];

        $this->trait->expects($this->exactly(1))
                ->method('getPurviewCategory')
                ->with($resource)
                ->willReturn($feedbackPurview[0]);
        
        $result = $this->trait->publicGetInteractionPurview($resource);
     
        $this->assertEquals($result, $feedbackPurview);
    }
}
