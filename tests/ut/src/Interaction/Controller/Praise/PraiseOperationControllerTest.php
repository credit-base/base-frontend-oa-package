<?php

namespace Base\Package\Interaction\Controller\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Praise\AcceptPraiseCommand;
use Sdk\Interaction\Command\Praise\UnPublishPraiseCommand;
use Sdk\Interaction\Command\Praise\PublishPraiseCommand;

use Base\Package\Interaction\Controller\InteractionRequestDataTrait;

class PraiseOperationControllerTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $praiseController;

    public function setUp()
    {
        $this->praiseController = $this->getMockBuilder(MockPraiseOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->praiseController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $praiseController = new PraiseOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $praiseController);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->praiseController->getCommandBus()
        );
    }

    private function initialAccept(int $id, bool $result)
    {
        $this->praiseController = $this->getMockBuilder(MockPraiseOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getReplyRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->praiseController->expects($this->exactly(1))
             ->method('getReplyRequestCommonData')
             ->willReturn($data);

        $this->praiseController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['content'],
                $data['images'],
                $data['admissibility']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AcceptPraiseCommand(
                    $data,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->praiseController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testAcceptActionFailure()
    {
        $id = 2;

        $this->initialAccept($id, false);

        $this->praiseController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);
        $idEncode = marmot_encode($id);
        $result = $this->praiseController->accept($idEncode);
        $this->assertFalse($result);
    }

    public function testAcceptActionSuccess()
    {
        $id = 2;

        $this->initialAccept($id, true);

        $this->praiseController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);
        $idEncode = marmot_encode($id);
        $result = $this->praiseController->accept($idEncode);
        
        $this->assertTrue($result);
    }

    private function initialPublish(int $id, bool $result)
    {
        $this->praiseController = $this->getMockBuilder(MockPraiseOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new PublishPraiseCommand(
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->praiseController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testPublishFailure()
    {
        $id = 2;

        $this->initialPublish($id, false);

        $this->praiseController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);
        $idEncode = marmot_encode($id);
        $result = $this->praiseController->publish($idEncode);
        $this->assertFalse($result);
    }

    public function testPublishSuccess()
    {
        $id = 2;

        $this->initialPublish($id, true);

        $this->praiseController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);
        $idEncode = marmot_encode($id);
        $result = $this->praiseController->publish($idEncode);
        
        $this->assertTrue($result);
    }

    private function initialUnPublish(int $id, bool $result)
    {
        $this->praiseController = $this->getMockBuilder(MockPraiseOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new UnPublishPraiseCommand(
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->praiseController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testUnPublishFailure()
    {
        $id = 2;

        $this->initialUnPublish($id, false);

        $this->praiseController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);
        $idEncode = marmot_encode($id);
        $result = $this->praiseController->unPublish($idEncode);
        $this->assertFalse($result);
    }

    public function testUnPublishSuccess()
    {
        $id = 2;

        $this->initialUnPublish($id, true);

        $this->praiseController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);
        $idEncode = marmot_encode($id);
        $result = $this->praiseController->unPublish($idEncode);
        
        $this->assertTrue($result);
    }
}
