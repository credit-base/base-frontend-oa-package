<?php
namespace Base\Package\Interaction\Controller\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Command\UnAuditPraise\ApproveUnAuditPraiseCommand;
use Sdk\Interaction\Command\UnAuditPraise\RejectUnAuditPraiseCommand;

class UnAuditPraiseApproveControllerTest extends TestCase
{
    private $unAuditPraiseStub;

    public function setUp()
    {
        $this->unAuditPraiseStub = $this->getMockBuilder(MockUnAuditPraiseApproveController::class)
            ->setMethods(
                [
                    'validateRejectScenario',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditPraiseStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new UnAuditPraiseApproveController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIApproveAbleController()
    {
        $controller = new UnAuditPraiseApproveController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IApproveAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $unAuditPraiseStub = new MockUnAuditPraiseApproveController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $unAuditPraiseStub->getCommandBus()
        );
    }

    public function testApproveAction()
    {
        $id = 1;

        $command = new ApproveUnAuditPraiseCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->unAuditPraiseStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->unAuditPraiseStub->approveAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionSuccess()
    {
        $id = 1;

        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->unAuditPraiseStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());
        
        $command = new RejectUnAuditPraiseCommand($rejectReason, $id);
       
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->unAuditPraiseStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(true);
        $this->unAuditPraiseStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->unAuditPraiseStub->rejectAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionFail()
    {
        $id = 1;
        
        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->unAuditPraiseStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());

        $this->unAuditPraiseStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(false);

        $result = $this->unAuditPraiseStub->rejectAction($id);
        $this->assertFalse($result);
    }
}
