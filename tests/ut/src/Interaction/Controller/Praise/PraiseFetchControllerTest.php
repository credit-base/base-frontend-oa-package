<?php
namespace Base\Package\Interaction\Controller\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Sdk\Interaction\Model\Praise;
use Sdk\Crew\Model\Crew;
use Sdk\Interaction\Model\NullPraise;
use Sdk\Interaction\Repository\PraiseRepository;

use Base\Package\Interaction\View\Json\Praise\View;
use Base\Package\Interaction\View\Json\Praise\ListView;

class PraiseFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'checkUserHasPurview',
                    'displayError',
                    'getInteractionPurview'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new PraiseFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\PraiseRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $praise = new NullPraise();

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->scenario(Argument::exact(PraiseRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($praise);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $praise = new Praise($id);

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->scenario(
            Argument::exact(PraiseRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($praise);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($praise));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter['acceptUserGroup'] = 1;
        $filter['applyUserGroup'] = 1;
        $filter['applyStatus'] = '0,-2';
        $filter['acceptStatus'] = 1;
        $filter['status'] = 0;
        $filter['title'] = 'title';
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $praiseArray = array(1,2);
        $count = 2;
        $praiseRepository = $this->prophesize(PraiseRepository::class);
        $praiseRepository->scenario(Argument::exact(PraiseRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($praiseRepository->reveal());
        $praiseRepository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$praiseArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($praiseRepository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($praiseArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testCheckUserHasPurview()
    {
        $this->praiseFetchController = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(
                [
                    'checkUserHasInteractionPurview',
                ]
            )->getMock();

        $resource = 'praises';
        $this->praiseFetchController->expects($this->exactly(1))
            ->method('checkUserHasInteractionPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->praiseFetchController->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }
}
