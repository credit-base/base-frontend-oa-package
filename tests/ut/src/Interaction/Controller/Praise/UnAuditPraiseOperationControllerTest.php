<?php

namespace Base\Package\Interaction\Controller\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\UnAuditPraise\ResubmitUnAuditPraiseCommand;

use Base\Package\Interaction\Controller\InteractionRequestDataTrait;

class UnAuditPraiseOperationControllerTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $unAuditPraiseController;

    public function setUp()
    {
        $this->unAuditPraiseController = $this->getMockBuilder(MockUnAuditPraiseOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditPraiseController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $unAuditPraiseController = new UnAuditPraiseOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $unAuditPraiseController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $unAuditPraiseController = new UnAuditPraiseOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IResubmitAbleController',
            $unAuditPraiseController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->unAuditPraiseController->getCommandBus()
        );
    }

    private function initialResubmit(int $id, bool $result)
    {
        $this->unAuditPraiseController = $this->getMockBuilder(MockUnAuditPraiseOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getReplyRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->unAuditPraiseController->expects($this->exactly(1))
                                      ->method('getReplyRequestCommonData')
                                      ->willReturn($data);

        $this->unAuditPraiseController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['content'],
                $data['images'],
                $data['admissibility']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ResubmitUnAuditPraiseCommand(
                    $data,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->unAuditPraiseController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testResubmitActionFailure()
    {
        $id = 2;

        $this->initialResubmit($id, false);

        $this->unAuditPraiseController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditPraiseController->resubmitAction($id);
        $this->assertFalse($result);
    }

    public function testResubmitActionSuccess()
    {
        $id = 2;

        $this->initialResubmit($id, true);

        $this->unAuditPraiseController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->unAuditPraiseController->resubmitAction($id);
        
        $this->assertTrue($result);
    }
}
