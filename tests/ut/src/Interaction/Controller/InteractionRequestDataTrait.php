<?php
namespace Base\Package\Interaction\Controller;

trait InteractionRequestDataTrait
{
    public function getTestRequestCommonData() : array
    {
        $data = array(
            'content' => '北京发改委',
            "admissibility"=>1,
            "images"=>array(array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg')),
        );

        return $data;
    }
}
