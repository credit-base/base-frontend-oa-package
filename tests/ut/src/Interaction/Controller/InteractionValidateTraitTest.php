<?php
namespace Base\Package\Interaction\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\Interaction\WidgetRules\InteractionWidgetRules;

class InteractionValidateTraitTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockInteractionValidateTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetInteractionWidgetRules()
    {
        $trait = new MockInteractionValidateTrait();
        $this->assertInstanceOf(
            'Sdk\Interaction\WidgetRules\InteractionWidgetRules',
            $trait->publicGetInteractionWidgetRules()
        );
    }

    public function testGetWidgetRules()
    {
        $trait = new MockInteractionValidateTrait();
        $this->assertInstanceOf(
            'Sdk\Common\WidgetRules\WidgetRules',
            $trait->publicGetWidgetRules()
        );
    }

    public function testValidateCommonScenario()
    {
        $trait = $this->getMockBuilder(MockInteractionValidateTrait::class)
                           ->setMethods(
                               [
                                    'getWidgetRules',
                                    'getInteractionWidgetRules',
                                ]
                           )
                           ->getMock();

        $data = $this->getTestRequestCommonData();

        $interactionWidgetRules = $this->prophesize(InteractionWidgetRules::class);

        $interactionWidgetRules->content(
            Argument::exact($data['content']),
            Argument::exact('content')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $interactionWidgetRules->images(
            Argument::exact($data['images']),
            Argument::exact('images')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $interactionWidgetRules->admissibility(
            Argument::exact($data['admissibility']),
            Argument::exact('admissibility')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getInteractionWidgetRules')
            ->willReturn($interactionWidgetRules->reveal());

        $result = $trait->publicValidateCommonScenario(
            $data['content'],
            $data['images'],
            $data['admissibility']
        );
       
        $this->assertTrue($result);
    }

    public function testInteractionValidateRejectScenario()
    {
        $trait = $this->getMockBuilder(MockInteractionValidateTrait::class)
                           ->setMethods(
                               [
                                    'getWidgetRules',
                                ]
                           )
                           ->getMock();

        $rejectReason = $this->faker->name();

        $commonWidgetRules = $this->prophesize(WidgetRules::class);

        $commonWidgetRules->reason(
            Argument::exact($rejectReason),
            Argument::exact('rejectReason')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getWidgetRules')
            ->willReturn($commonWidgetRules->reveal());

        $result = $trait->publicValidateRejectScenario($rejectReason);

        $this->assertTrue($result);
    }
}
