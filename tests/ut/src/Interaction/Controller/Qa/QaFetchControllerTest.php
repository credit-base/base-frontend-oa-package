<?php
namespace Base\Package\Interaction\Controller\Qa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Sdk\Interaction\Model\Qa;
use Sdk\Interaction\Model\NullQa;
use Sdk\Interaction\Repository\QaRepository;

use Base\Package\Interaction\View\Json\Qa\View;
use Base\Package\Interaction\View\Json\Qa\ListView;

class QaFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockQaFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'checkUserHasPurview',
                    'displayError',
                    'getInteractionPurview'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new QaFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\QaRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockQaFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $qaObject = new NullQa();

        $repository = $this->prophesize(QaRepository::class);
        $repository->scenario(Argument::exact(QaRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($qaObject);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockQaFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $qaObject = new Qa($id);

        $repository = $this->prophesize(QaRepository::class);
        $repository->scenario(
            Argument::exact(QaRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($qaObject);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($qaObject));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockQaFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter['acceptUserGroup'] = 1;
        $filter['applyUserGroup'] = 1;
        $filter['applyStatus'] = '0,-2';
        $filter['acceptStatus'] = 1;
        $filter['status'] = 0;
        $filter['title'] = 'title';
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $qaArray = array(1,2);
        $count = 2;
        $qaRepository = $this->prophesize(QaRepository::class);
        $qaRepository->scenario(Argument::exact(QaRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($qaRepository->reveal());
        $qaRepository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$qaArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($qaRepository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($qaArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testCheckUserHasPurview()
    {
        $this->qaFetchController = $this->getMockBuilder(MockQaFetchController::class)
            ->setMethods(
                [
                    'checkUserHasInteractionPurview',
                ]
            )->getMock();

        $resource = 'qas';
        $this->qaFetchController->expects($this->exactly(1))
            ->method('checkUserHasInteractionPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->qaFetchController->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }
}
