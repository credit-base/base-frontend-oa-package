<?php

namespace Base\Package\Interaction\Controller\Qa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\UnAuditQa\ResubmitUnAuditQaCommand;

use Base\Package\Interaction\Controller\InteractionRequestDataTrait;

class UnAuditQaOperationControllerTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $unAuditQaController;

    public function setUp()
    {
        $this->unAuditQaController = $this->getMockBuilder(MockUnAuditQaOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditQaController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $unAuditQaController = new UnAuditQaOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $unAuditQaController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $unAuditQaController = new UnAuditQaOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IResubmitAbleController',
            $unAuditQaController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->unAuditQaController->getCommandBus()
        );
    }

    private function initialResubmit(int $id, bool $result)
    {
        $this->unAuditQaController = $this->getMockBuilder(MockUnAuditQaOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getReplyRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->unAuditQaController->expects($this->exactly(1))
                                      ->method('getReplyRequestCommonData')
                                      ->willReturn($data);

        $this->unAuditQaController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['content'],
                $data['images'],
                $data['admissibility']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ResubmitUnAuditQaCommand(
                    $data,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->unAuditQaController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testResubmitActionFailure()
    {
        $id = 2;

        $this->initialResubmit($id, false);

        $this->unAuditQaController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditQaController->resubmitAction($id);
        $this->assertFalse($result);
    }

    public function testResubmitActionSuccess()
    {
        $id = 2;

        $this->initialResubmit($id, true);

        $this->unAuditQaController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->unAuditQaController->resubmitAction($id);
        
        $this->assertTrue($result);
    }
}
