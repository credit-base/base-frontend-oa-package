<?php

namespace Base\Package\Interaction\Controller\Qa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Qa\AcceptQaCommand;
use Sdk\Interaction\Command\Qa\PublishQaCommand;
use Sdk\Interaction\Command\Qa\UnPublishQaCommand;

use Base\Package\Interaction\Controller\InteractionRequestDataTrait;

class QaOperationControllerTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $qaController;

    public function setUp()
    {
        $this->qaController = $this->getMockBuilder(MockQaOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->qaController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $qaController = new QaOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $qaController);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->qaController->getCommandBus()
        );
    }

    private function initialAccept(int $id, bool $result)
    {
        $this->qaController = $this->getMockBuilder(MockQaOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getReplyRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->qaController->expects($this->exactly(1))
             ->method('getReplyRequestCommonData')
             ->willReturn($data);

        $this->qaController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['content'],
                $data['images'],
                $data['admissibility']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AcceptQaCommand(
                    $data,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->qaController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testAcceptActionFailure()
    {
        $id = 2;

        $this->initialAccept($id, false);

        $this->qaController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);
        $idEncode = marmot_encode($id);
        $result = $this->qaController->accept($idEncode);
        $this->assertFalse($result);
    }

    public function testAcceptActionSuccess()
    {
        $id = 2;

        $this->initialAccept($id, true);

        $this->qaController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);
        $idEncode = marmot_encode($id);
        $result = $this->qaController->accept($idEncode);
        
        $this->assertTrue($result);
    }

    private function initialPublish(int $id, bool $result)
    {
        $this->qaController = $this->getMockBuilder(MockQaOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new PublishQaCommand(
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->qaController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testPublishFailure()
    {
        $id = 2;

        $this->initialPublish($id, false);

        $this->qaController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);
        $idEncode = marmot_encode($id);
        $result = $this->qaController->publish($idEncode);
        $this->assertFalse($result);
    }

    public function testPublishSuccess()
    {
        $id = 2;

        $this->initialPublish($id, true);

        $this->qaController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);
        $idEncode = marmot_encode($id);
        $result = $this->qaController->publish($idEncode);
        
        $this->assertTrue($result);
    }

    private function initialUnPublish(int $id, bool $result)
    {
        $this->qaController = $this->getMockBuilder(MockQaOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new UnPublishQaCommand(
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->qaController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testUnPublishFailure()
    {
        $id = 2;

        $this->initialUnPublish($id, false);

        $this->qaController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);
        $idEncode = marmot_encode($id);
        $result = $this->qaController->unPublish($idEncode);
        $this->assertFalse($result);
    }

    public function testUnPublishSuccess()
    {
        $id = 2;

        $this->initialUnPublish($id, true);

        $this->qaController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);
        $idEncode = marmot_encode($id);
        $result = $this->qaController->unPublish($idEncode);
        
        $this->assertTrue($result);
    }
}
