<?php
namespace Base\Package\Interaction\Controller\Qa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Command\UnAuditQa\ApproveUnAuditQaCommand;
use Sdk\Interaction\Command\UnAuditQa\RejectUnAuditQaCommand;

class UnAuditQaApproveControllerTest extends TestCase
{
    private $unAuditQaStub;

    public function setUp()
    {
        $this->unAuditQaStub = $this->getMockBuilder(MockUnAuditQaApproveController::class)
            ->setMethods(
                [
                    'validateRejectScenario',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditQaStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new UnAuditQaApproveController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIApproveAbleController()
    {
        $controller = new UnAuditQaApproveController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IApproveAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $unAuditQaStub = new MockUnAuditQaApproveController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $unAuditQaStub->getCommandBus()
        );
    }

    public function testApproveAction()
    {
        $id = 1;

        $command = new ApproveUnAuditQaCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->unAuditQaStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->unAuditQaStub->approveAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionSuccess()
    {
        $id = 1;

        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->unAuditQaStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());
        
        $command = new RejectUnAuditQaCommand($rejectReason, $id);
       
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->unAuditQaStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(true);
        $this->unAuditQaStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->unAuditQaStub->rejectAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionFail()
    {
        $id = 1;
        
        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->unAuditQaStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());

        $this->unAuditQaStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(false);

        $result = $this->unAuditQaStub->rejectAction($id);
        $this->assertFalse($result);
    }
}
