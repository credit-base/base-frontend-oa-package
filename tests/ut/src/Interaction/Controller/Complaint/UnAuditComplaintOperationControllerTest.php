<?php

namespace Base\Package\Interaction\Controller\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\UnAuditComplaint\ResubmitUnAuditComplaintCommand;

use Base\Package\Interaction\Controller\InteractionRequestDataTrait;

class UnAuditComplaintOperationControllerTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $unAuditComplaintController;

    public function setUp()
    {
        $this->unAuditComplaintController = $this->getMockBuilder(MockUnAuditComplaintOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditComplaintController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $unAuditComplaintController = new UnAuditComplaintOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $unAuditComplaintController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $unAuditComplaintController = new UnAuditComplaintOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IResubmitAbleController',
            $unAuditComplaintController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->unAuditComplaintController->getCommandBus()
        );
    }

    private function initialResubmit(int $id, bool $result)
    {
        $this->unAuditComplaintController = $this->getMockBuilder(MockUnAuditComplaintOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getReplyRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->unAuditComplaintController->expects($this->exactly(1))
                                      ->method('getReplyRequestCommonData')
                                      ->willReturn($data);

        $this->unAuditComplaintController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['content'],
                $data['images'],
                $data['admissibility']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ResubmitUnAuditComplaintCommand(
                    $data,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->unAuditComplaintController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testResubmitActionFailure()
    {
        $id = 2;

        $this->initialResubmit($id, false);

        $this->unAuditComplaintController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditComplaintController->resubmitAction($id);
        $this->assertFalse($result);
    }

    public function testResubmitActionSuccess()
    {
        $id = 2;

        $this->initialResubmit($id, true);

        $this->unAuditComplaintController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->unAuditComplaintController->resubmitAction($id);
        
        $this->assertTrue($result);
    }
}
