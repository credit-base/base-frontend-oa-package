<?php

namespace Base\Package\Interaction\Controller\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Complaint\AcceptComplaintCommand;

use Base\Package\Interaction\Controller\InteractionRequestDataTrait;

class ComplaintOperationControllerTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $complaintController;

    public function setUp()
    {
        $this->complaintController = $this->getMockBuilder(MockComplaintOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->complaintController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $complaintController = new ComplaintOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $complaintController);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->complaintController->getCommandBus()
        );
    }

    private function initialAccept(int $id, bool $result)
    {
        $this->complaintController = $this->getMockBuilder(MockComplaintOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getReplyRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->complaintController->expects($this->exactly(1))
             ->method('getReplyRequestCommonData')
             ->willReturn($data);

        $this->complaintController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['content'],
                $data['images'],
                $data['admissibility']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AcceptComplaintCommand(
                    $data,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->complaintController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testAcceptActionFailure()
    {
        $id = 2;

        $this->initialAccept($id, false);

        $this->complaintController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);
        $idEncode = marmot_encode($id);
        $result = $this->complaintController->accept($idEncode);
        $this->assertFalse($result);
    }

    public function testAcceptActionSuccess()
    {
        $id = 2;

        $this->initialAccept($id, true);

        $this->complaintController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);
        $idEncode = marmot_encode($id);
        $result = $this->complaintController->accept($idEncode);
        
        $this->assertTrue($result);
    }
}
