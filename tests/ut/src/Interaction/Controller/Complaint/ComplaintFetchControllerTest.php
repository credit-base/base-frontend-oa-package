<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Sdk\Interaction\Model\Complaint;
use Sdk\Crew\Model\Crew;
use Sdk\Interaction\Model\NullComplaint;
use Sdk\Interaction\Repository\ComplaintRepository;

use Base\Package\Interaction\View\Json\Complaint\View;
use Base\Package\Interaction\View\Json\Complaint\ListView;

class ComplaintFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockComplaintFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'checkUserHasPurview',
                    'displayError',
                    'getInteractionPurview'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new ComplaintFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\ComplaintRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockComplaintFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $complaint = new NullComplaint();

        $repository = $this->prophesize(ComplaintRepository::class);
        $repository->scenario(Argument::exact(ComplaintRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($complaint);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockComplaintFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $complaint = new Complaint($id);

        $repository = $this->prophesize(ComplaintRepository::class);
        $repository->scenario(
            Argument::exact(ComplaintRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($complaint);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($complaint));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockComplaintFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $filter['acceptUserGroup'] = 1;
        $filter['applyUserGroup'] = 1;
        $filter['applyStatus'] = '0,-2';
        $filter['acceptStatus'] = 1;
        $filter['status'] = 0;
        $filter['title'] = 'title';

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $complaintArray = array(1,2);
        $count = 2;
        $complaintRepository = $this->prophesize(ComplaintRepository::class);
        $complaintRepository->scenario(Argument::exact(ComplaintRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($complaintRepository->reveal());
        $complaintRepository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$complaintArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($complaintRepository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($complaintArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testCheckUserHasPurview()
    {
        $this->complaintFetchController = $this->getMockBuilder(MockComplaintFetchController::class)
            ->setMethods(
                [
                    'checkUserHasInteractionPurview',
                ]
            )->getMock();

        $resource = 'complaints';
        $this->complaintFetchController->expects($this->exactly(1))
            ->method('checkUserHasInteractionPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->complaintFetchController->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }
}
