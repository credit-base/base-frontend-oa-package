<?php

namespace Base\Package\Interaction\Controller\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\UnAuditAppeal\ResubmitUnAuditAppealCommand;

use Base\Package\Interaction\Controller\InteractionRequestDataTrait;

class UnAuditAppealOperationControllerTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $unAuditAppealController;

    public function setUp()
    {
        $this->unAuditAppealController = $this->getMockBuilder(MockUnAuditAppealOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditAppealController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $unAuditAppealController = new UnAuditAppealOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $unAuditAppealController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $unAuditAppealController = new UnAuditAppealOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IResubmitAbleController',
            $unAuditAppealController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->unAuditAppealController->getCommandBus()
        );
    }

    private function initialResubmit(int $id, bool $result)
    {
        $this->unAuditAppealController = $this->getMockBuilder(MockUnAuditAppealOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getReplyRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->unAuditAppealController->expects($this->exactly(1))
                                      ->method('getReplyRequestCommonData')
                                      ->willReturn($data);

        $this->unAuditAppealController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['content'],
                $data['images'],
                $data['admissibility']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ResubmitUnAuditAppealCommand(
                    $data,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->unAuditAppealController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testResubmitActionFailure()
    {
        $id = 2;

        $this->initialResubmit($id, false);

        $this->unAuditAppealController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditAppealController->resubmitAction($id);
        $this->assertFalse($result);
    }

    public function testResubmitActionSuccess()
    {
        $id = 2;

        $this->initialResubmit($id, true);

        $this->unAuditAppealController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->unAuditAppealController->resubmitAction($id);
        
        $this->assertTrue($result);
    }
}
