<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Sdk\Interaction\Model\Appeal;
use Sdk\Crew\Model\Crew;
use Sdk\Interaction\Model\NullAppeal;
use Sdk\Interaction\Repository\AppealRepository;

use Base\Package\Interaction\View\Json\Appeal\View;
use Base\Package\Interaction\View\Json\Appeal\ListView;

class AppealFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockAppealFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'checkUserHasPurview',
                    'displayError',
                    'getInteractionPurview'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new AppealFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\AppealRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockAppealFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $appeal = new NullAppeal();

        $repository = $this->prophesize(AppealRepository::class);
        $repository->scenario(Argument::exact(AppealRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($appeal);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockAppealFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $appeal = new Appeal($id);

        $repository = $this->prophesize(AppealRepository::class);
        $repository->scenario(
            Argument::exact(AppealRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($appeal);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($appeal));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockAppealFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter['acceptUserGroup'] = 1;
        $filter['applyUserGroup'] = 1;
        $filter['applyStatus'] = '0,-2';
        $filter['acceptStatus'] = 1;
        $filter['status'] = 0;
        $filter['title'] = 'title';
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $appealArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(AppealRepository::class);
        $repository->scenario(Argument::exact(AppealRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$appealArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($appealArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testCheckUserHasPurview()
    {
        $this->appealFetchController = $this->getMockBuilder(MockAppealFetchController::class)
            ->setMethods(
                [
                    'checkUserHasInteractionPurview',
                ]
            )->getMock();

        $resource = 'appeals';
        $this->appealFetchController->expects($this->exactly(1))
            ->method('checkUserHasInteractionPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->appealFetchController->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }

    public function testCheckUserHasInteractionPurviewSuccess()
    {
        $resource = 'appeals';
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setCategory(2);

        $this->stub->expects($this->exactly(1))
            ->method('getInteractionPurview')
            ->with($resource)
            ->willReturn([26,31]);

        $this->assertTrue($this->stub->publicCheckUserHasInteractionPurview($resource));
    }

    public function testCheckUserHasInteractionPurviewFail()
    {
        $resource = 'unAuditAppeals';
        Core::$container->set('crew', new Crew(1));
        Core::$container->get('crew')->setCategory(2);

        $this->assertFalse($this->stub->publicCheckUserHasInteractionPurview($resource));
    }
}
