<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Command\UnAuditAppeal\ApproveUnAuditAppealCommand;
use Sdk\Interaction\Command\UnAuditAppeal\RejectUnAuditAppealCommand;

class UnAuditAppealApproveControllerTest extends TestCase
{
    private $unAuditAppealStub;

    public function setUp()
    {
        $this->unAuditAppealStub = $this->getMockBuilder(MockUnAuditAppealApproveController::class)
            ->setMethods(
                [
                    'validateRejectScenario',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditAppealStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new UnAuditAppealApproveController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIApproveAbleController()
    {
        $controller = new UnAuditAppealApproveController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IApproveAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $unAuditAppealStub = new MockUnAuditAppealApproveController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $unAuditAppealStub->getCommandBus()
        );
    }

    public function testApproveAction()
    {
        $id = 1;

        $command = new ApproveUnAuditAppealCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->unAuditAppealStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->unAuditAppealStub->approveAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionSuccess()
    {
        $id = 1;

        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->unAuditAppealStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());
        
        $command = new RejectUnAuditAppealCommand($rejectReason, $id);
       
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->unAuditAppealStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(true);
        $this->unAuditAppealStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->unAuditAppealStub->rejectAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionFail()
    {
        $id = 1;
        
        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->unAuditAppealStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());

        $this->unAuditAppealStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(false);

        $result = $this->unAuditAppealStub->rejectAction($id);
        $this->assertFalse($result);
    }
}
