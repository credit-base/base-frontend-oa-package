<?php

namespace Base\Package\Interaction\Controller\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Appeal\AcceptAppealCommand;

use Base\Package\Interaction\Controller\InteractionRequestDataTrait;

class AppealOperationControllerTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $appealController;

    public function setUp()
    {
        $this->appealController = $this->getMockBuilder(MockAppealOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->appealController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $appealController = new AppealOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $appealController);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->appealController->getCommandBus()
        );
    }

    private function initialAccept(int $id, bool $result)
    {
        $this->appealController = $this->getMockBuilder(MockAppealOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getReplyRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->appealController->expects($this->exactly(1))
             ->method('getReplyRequestCommonData')
             ->willReturn($data);

        $this->appealController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['content'],
                $data['images'],
                $data['admissibility']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AcceptAppealCommand(
                    $data,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->appealController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testAcceptActionFailure()
    {
        $id = 2;

        $this->initialAccept($id, false);

        $this->appealController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);
        $idEncode = marmot_encode($id);
        $result = $this->appealController->accept($idEncode);
        $this->assertFalse($result);
    }

    public function testAcceptActionSuccess()
    {
        $id = 2;

        $this->initialAccept($id, true);

        $this->appealController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);
        $idEncode = marmot_encode($id);
        $result = $this->appealController->accept($idEncode);
        
        $this->assertTrue($result);
    }
}
