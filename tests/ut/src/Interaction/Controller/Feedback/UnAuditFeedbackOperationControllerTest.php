<?php

namespace Base\Package\Interaction\Controller\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\UnAuditFeedback\ResubmitUnAuditFeedbackCommand;

use Base\Package\Interaction\Controller\InteractionRequestDataTrait;

class UnAuditFeedbackOperationControllerTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $unAuditFeedbackController;

    public function setUp()
    {
        $this->unAuditFeedbackController = $this->getMockBuilder(MockUnAuditFeedbackOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditFeedbackController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $unAuditFeedbackController = new UnAuditFeedbackOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $unAuditFeedbackController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $unAuditFeedbackController = new UnAuditFeedbackOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IResubmitAbleController',
            $unAuditFeedbackController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->unAuditFeedbackController->getCommandBus()
        );
    }

    private function initialResubmit(int $id, bool $result)
    {
        $this->unAuditFeedbackController = $this->getMockBuilder(MockUnAuditFeedbackOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getReplyRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->unAuditFeedbackController->expects($this->exactly(1))
                                      ->method('getReplyRequestCommonData')
                                      ->willReturn($data);

        $this->unAuditFeedbackController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['content'],
                $data['images'],
                $data['admissibility']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ResubmitUnAuditFeedbackCommand(
                    $data,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->unAuditFeedbackController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testResubmitActionFailure()
    {
        $id = 2;

        $this->initialResubmit($id, false);

        $this->unAuditFeedbackController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->unAuditFeedbackController->resubmitAction($id);
        $this->assertFalse($result);
    }

    public function testResubmitActionSuccess()
    {
        $id = 2;

        $this->initialResubmit($id, true);

        $this->unAuditFeedbackController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->unAuditFeedbackController->resubmitAction($id);
        
        $this->assertTrue($result);
    }
}
