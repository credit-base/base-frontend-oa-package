<?php

namespace Base\Package\Interaction\Controller\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Feedback\AcceptFeedbackCommand;

use Base\Package\Interaction\Controller\InteractionRequestDataTrait;

class FeedbackOperationControllerTest extends TestCase
{
    use InteractionRequestDataTrait;

    private $feedbackController;

    public function setUp()
    {
        $this->feedbackController = $this->getMockBuilder(MockFeedbackOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->feedbackController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $feedbackController = new FeedbackOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $feedbackController);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->feedbackController->getCommandBus()
        );
    }

    private function initialAccept(int $id, bool $result)
    {
        $this->feedbackController = $this->getMockBuilder(MockFeedbackOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getReplyRequestCommonData'
                ]
            )->getMock();

        $data = $this->getTestRequestCommonData();

        $this->feedbackController->expects($this->exactly(1))
             ->method('getReplyRequestCommonData')
             ->willReturn($data);

        $this->feedbackController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['content'],
                $data['images'],
                $data['admissibility']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AcceptFeedbackCommand(
                    $data,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->feedbackController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testAcceptActionFailure()
    {
        $id = 2;

        $this->initialAccept($id, false);

        $this->feedbackController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);
        $idEncode = marmot_encode($id);
        $result = $this->feedbackController->accept($idEncode);
        $this->assertFalse($result);
    }

    public function testAcceptActionSuccess()
    {
        $id = 2;

        $this->initialAccept($id, true);

        $this->feedbackController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);
        $idEncode = marmot_encode($id);
        $result = $this->feedbackController->accept($idEncode);
        
        $this->assertTrue($result);
    }
}
