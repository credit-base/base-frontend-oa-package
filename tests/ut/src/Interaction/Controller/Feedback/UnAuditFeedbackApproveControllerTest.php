<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Command\UnAuditFeedback\ApproveUnAuditFeedbackCommand;
use Sdk\Interaction\Command\UnAuditFeedback\RejectUnAuditFeedbackCommand;

class UnAuditFeedbackApproveControllerTest extends TestCase
{
    private $unAuditFeedbackStub;

    public function setUp()
    {
        $this->unAuditFeedbackStub = $this->getMockBuilder(MockUnAuditFeedbackApproveController::class)
            ->setMethods(
                [
                    'validateRejectScenario',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->unAuditFeedbackStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new UnAuditFeedbackApproveController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIApproveAbleController()
    {
        $controller = new UnAuditFeedbackApproveController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IApproveAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $unAuditFeedbackStub = new MockUnAuditFeedbackApproveController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $unAuditFeedbackStub->getCommandBus()
        );
    }

    public function testApproveAction()
    {
        $id = 1;

        $command = new ApproveUnAuditFeedbackCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->unAuditFeedbackStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->unAuditFeedbackStub->approveAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionSuccess()
    {
        $id = 1;

        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->unAuditFeedbackStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());
        
        $command = new RejectUnAuditFeedbackCommand($rejectReason, $id);
       
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->unAuditFeedbackStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(true);
        $this->unAuditFeedbackStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->unAuditFeedbackStub->rejectAction($id);
        $this->assertTrue($result);
    }

    public function testRejectActionFail()
    {
        $id = 1;
        
        $rejectReason = "内容不合理";

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('rejectReason'),
            Argument::exact('')
        )
                ->shouldBeCalledTimes(1)
                ->willReturn($rejectReason);

        $this->unAuditFeedbackStub->expects($this->any())
                ->method('getRequest')
                ->willReturn($request->reveal());

        $this->unAuditFeedbackStub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->with($rejectReason)
            ->willReturn(false);

        $result = $this->unAuditFeedbackStub->rejectAction($id);
        $this->assertFalse($result);
    }
}
