<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Sdk\Interaction\Model\UnAuditFeedback;
use Sdk\Interaction\Model\NullUnAuditFeedback;
use Sdk\Interaction\Repository\UnAuditFeedbackRepository;

use Base\Package\Interaction\View\Json\UnAuditFeedback\View;
use Base\Package\Interaction\View\Json\UnAuditFeedback\ListView;

class UnAuditFeedbackFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditFeedbackFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'checkUserHasPurview'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new UnAuditFeedbackFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\UnAuditFeedbackRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditFeedbackFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $unAuditFeedback = new NullUnAuditFeedback();

        $repository = $this->prophesize(UnAuditFeedbackRepository::class);
        $repository->scenario(Argument::exact(UnAuditFeedbackRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($unAuditFeedback);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditFeedbackFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $unAuditFeedback = new UnAuditFeedback($id);

        $repository = $this->prophesize(UnAuditFeedbackRepository::class);
        $repository->scenario(
            Argument::exact(UnAuditFeedbackRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditFeedback);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($unAuditFeedback));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditFeedbackFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render',
                    'checkUserHasPurview'
                ]
            )->getMock();

        $filter['acceptUserGroup'] = 1;
        $filter['applyUserGroup'] = 1;
        $filter['applyStatus'] = '0,-2';
        $filter['acceptStatus'] = 1;
        $filter['status'] = 0;
        $filter['title'] = 'title';

        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $unAuditFeedbackArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(UnAuditFeedbackRepository::class);
        $repository->scenario(Argument::exact(UnAuditFeedbackRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$unAuditFeedbackArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($unAuditFeedbackArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testCheckUserHasPurview()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditFeedbackFetchController::class)
            ->setMethods(
                [
                    'checkUserHasInteractionPurview',
                ]
            )->getMock();

        $resource = 'feedbacks';
        $this->stub->expects($this->any())
            ->method('checkUserHasInteractionPurview')
            ->with($resource)
            ->willReturn(true);
     
        $result = $this->stub->checkUserHasPurview($resource);
        $this->assertTrue($result);
    }
}
