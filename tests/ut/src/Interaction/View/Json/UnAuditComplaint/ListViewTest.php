<?php
namespace Base\Package\Interaction\View\Json\UnAuditComplaint;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Complaint\UnAuditComplaintTranslator;
use Sdk\Interaction\Model\UnAuditComplaint;

class ListViewTest extends TestCase
{
    private $unAuditComplaintListView;

    public function setUp()
    {
        $this->unAuditComplaintListView = new MockListView();
    }

    public function tearDown()
    {
        unset($this->unAuditComplaintListView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Complaint\UnAuditComplaintTranslator',
            $this->unAuditComplaintListView->getUnAuditComplaintTranslator()
        );
    }

    public function testDisplay()
    {
        $this->unAuditComplaintListView = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getUnAuditComplaintTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $unAuditComplaint = new UnAuditComplaint();
        $unAuditComplaintData = [];
        $unAuditComplaints = [$unAuditComplaint];
        $count = 1;

        $translator = $this->prophesize(UnAuditComplaintTranslator::class);
        $translator->objectToArray($unAuditComplaint)->shouldBeCalledTimes(1)->willReturn($unAuditComplaintData);
        $this->unAuditComplaintListView->expects($this->exactly(1))
                                    ->method('getUnAuditComplaintTranslator')
                                    ->willReturn($translator->reveal());

        $this->unAuditComplaintListView->expects($this->exactly(1))->method('getList')->willReturn($unAuditComplaints);
        $this->unAuditComplaintListView->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->unAuditComplaintListView->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$unAuditComplaintData]
            ]
        );
        $this->assertNull($this->unAuditComplaintListView->display());
    }
}
