<?php
namespace Base\Package\Interaction\View\Json\UnAuditComplaint;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Complaint\UnAuditComplaintTranslator;
use Sdk\Interaction\Model\UnAuditComplaint;

class ViewTest extends TestCase
{
    private $unAuditComplaintView;

    public function setUp()
    {
        $this->unAuditComplaintView = new MockView();
    }

    public function tearDown()
    {
        unset($this->unAuditComplaintView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Complaint\UnAuditComplaintTranslator',
            $this->unAuditComplaintView->getUnAuditComplaintTranslator()
        );
    }

    public function testDisplay()
    {
        $this->unAuditComplaintView = $this->getMockBuilder(MockView::class)
            ->setMethods(['getUnAuditComplaintTranslator', 'getData', 'encode','getFormatData'])
            ->getMock();

        $unAuditComplaint = new UnAuditComplaint();
        $unAuditComplaintData = [
            'complaintData' => [],
            'interaction'=> [],
            'reply'=>[],
        ];

        $translator = $this->prophesize(UnAuditComplaintTranslator::class);
        $translator->objectToArray($unAuditComplaint)->shouldBeCalledTimes(1)->willReturn($unAuditComplaintData);
        $this->unAuditComplaintView->expects($this->exactly(1))
                                ->method('getUnAuditComplaintTranslator')
                                ->willReturn($translator->reveal());

        $this->unAuditComplaintView->expects($this->exactly(1))->method('getData')->willReturn($unAuditComplaint);
        $this->unAuditComplaintView->expects($this->exactly(1))
                                ->method('getFormatData')
                                ->with($unAuditComplaintData)
                                ->willReturn($unAuditComplaintData);
                                
        unset($unAuditComplaintData['interaction']);
        $this->unAuditComplaintView->expects($this->exactly(1))->method('encode')->with($unAuditComplaintData);
        $this->assertNull($this->unAuditComplaintView->display());
    }
}
