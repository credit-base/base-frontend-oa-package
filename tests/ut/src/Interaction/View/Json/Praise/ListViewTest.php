<?php
namespace Base\Package\Interaction\View\Json\Praise;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Praise\PraiseTranslator;
use Sdk\Interaction\Model\Praise;

class ListViewTest extends TestCase
{
    private $praiseListView;

    public function setUp()
    {
        $this->praiseListView = new MockListView();
    }

    public function tearDown()
    {
        unset($this->praiseListView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Praise\PraiseTranslator',
            $this->praiseListView->getPraiseTranslator()
        );
    }

    public function testDisplay()
    {
        $this->praiseListView = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getPraiseTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $praise = new Praise();
        $praiseData = [];
        $praises = [$praise];
        $count = 1;

        $translator = $this->prophesize(PraiseTranslator::class);
        $translator->objectToArray(
            $praise,
            array(
                'id',
                'title',
                'name',
                'acceptStatus',
                'admissibility',
                'type',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'reply'=>[]
            )
        )->shouldBeCalledTimes(1)->willReturn($praiseData);
        $this->praiseListView->expects($this->exactly(1))
                            ->method('getPraiseTranslator')
                            ->willReturn($translator->reveal());

        $this->praiseListView->expects($this->exactly(1))->method('getList')->willReturn($praises);
        $this->praiseListView->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->praiseListView->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$praiseData]
            ]
        );
        $this->assertNull($this->praiseListView->display());
    }
}
