<?php
namespace Base\Package\Interaction\View\Json\Praise;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Praise\PraiseTranslator;
use Sdk\Interaction\Model\Praise;

class ViewTest extends TestCase
{
    private $praiseView;

    public function setUp()
    {
        $this->praiseView = new MockView();
    }

    public function tearDown()
    {
        unset($this->praiseView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Praise\PraiseTranslator',
            $this->praiseView->getPraiseTranslator()
        );
    }

    public function testDisplay()
    {
        $this->praiseView = $this->getMockBuilder(MockView::class)
            ->setMethods(['getPraiseTranslator', 'getData', 'encode','getFormatData'])
            ->getMock();

        $praise = new Praise();
        $praiseData = [
            'praiseData' => [],
            'interaction'=> [],
            'reply'=>[],
        ];

        $translator = $this->prophesize(PraiseTranslator::class);
        $translator->objectToArray(
            $praise,
            array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'member'=>['id','realName'],
                'reply'=>[],
            )
        )->shouldBeCalledTimes(1)->willReturn($praiseData);
        $this->praiseView->expects($this->exactly(1))->method('getPraiseTranslator')->willReturn($translator->reveal());

        $this->praiseView->expects($this->exactly(1))->method('getData')->willReturn($praise);
        $this->praiseView->expects($this->exactly(1))
                        ->method('getFormatData')
                        ->with($praiseData)
                        ->willReturn($praiseData);
                        
        unset($praiseData['interaction']);
        $this->praiseView->expects($this->exactly(1))->method('encode')->with($praiseData);
        $this->assertNull($this->praiseView->display());
    }
}
