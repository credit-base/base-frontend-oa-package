<?php
namespace Base\Package\Interaction\View\Json\UnAuditPraise;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Praise\UnAuditPraiseTranslator;
use Sdk\Interaction\Model\UnAuditPraise;

class ViewTest extends TestCase
{
    private $unAuditPraiseView;

    public function setUp()
    {
        $this->unAuditPraiseView = new MockView();
    }

    public function tearDown()
    {
        unset($this->unAuditPraiseView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Praise\UnAuditPraiseTranslator',
            $this->unAuditPraiseView->getUnAuditPraiseTranslator()
        );
    }

    public function testDisplay()
    {
        $this->unAuditPraiseView = $this->getMockBuilder(MockView::class)
            ->setMethods(['getUnAuditPraiseTranslator', 'getData', 'encode','getFormatData'])
            ->getMock();

        $unAuditPraise = new UnAuditPraise();
        $unAuditPraiseData = [
            'praiseData' => [],
            'interaction'=> [],
            'reply'=>[],
        ];

        $translator = $this->prophesize(UnAuditPraiseTranslator::class);
        $translator->objectToArray($unAuditPraise)->shouldBeCalledTimes(1)->willReturn($unAuditPraiseData);
        $this->unAuditPraiseView->expects($this->exactly(1))
                                ->method('getUnAuditPraiseTranslator')
                                ->willReturn($translator->reveal());

        $this->unAuditPraiseView->expects($this->exactly(1))->method('getData')->willReturn($unAuditPraise);
        $this->unAuditPraiseView->expects($this->exactly(1))
                                ->method('getFormatData')
                                ->with($unAuditPraiseData)
                                ->willReturn($unAuditPraiseData);
                                
        unset($unAuditPraiseData['interaction']);
        $this->unAuditPraiseView->expects($this->exactly(1))->method('encode')->with($unAuditPraiseData);
        $this->assertNull($this->unAuditPraiseView->display());
    }
}
