<?php
namespace Base\Package\Interaction\View\Json\UnAuditPraise;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Praise\UnAuditPraiseTranslator;
use Sdk\Interaction\Model\UnAuditPraise;

class ListViewTest extends TestCase
{
    private $unAuditPraiseListView;

    public function setUp()
    {
        $this->unAuditPraiseListView = new MockListView();
    }

    public function tearDown()
    {
        unset($this->unAuditPraiseListView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Praise\UnAuditPraiseTranslator',
            $this->unAuditPraiseListView->getUnAuditPraiseTranslator()
        );
    }

    public function testDisplay()
    {
        $this->unAuditPraiseListView = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getUnAuditPraiseTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $unAuditPraise = new UnAuditPraise();
        $unAuditPraiseData = [];
        $unAuditPraises = [$unAuditPraise];
        $count = 1;

        $translator = $this->prophesize(UnAuditPraiseTranslator::class);
        $translator->objectToArray($unAuditPraise)->shouldBeCalledTimes(1)->willReturn($unAuditPraiseData);
        $this->unAuditPraiseListView->expects($this->exactly(1))
                                    ->method('getUnAuditPraiseTranslator')
                                    ->willReturn($translator->reveal());

        $this->unAuditPraiseListView->expects($this->exactly(1))->method('getList')->willReturn($unAuditPraises);
        $this->unAuditPraiseListView->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->unAuditPraiseListView->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$unAuditPraiseData]
            ]
        );
        $this->assertNull($this->unAuditPraiseListView->display());
    }
}
