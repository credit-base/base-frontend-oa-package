<?php
namespace Base\Package\Interaction\View\Json\UnAuditAppeal;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Appeal\UnAuditAppealTranslator;
use Sdk\Interaction\Model\UnAuditAppeal;

class ViewTest extends TestCase
{
    private $unAuditAppealView;

    public function setUp()
    {
        $this->unAuditAppealView = new MockView();
    }

    public function tearDown()
    {
        unset($this->unAuditAppealView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Appeal\UnAuditAppealTranslator',
            $this->unAuditAppealView->getUnAuditAppealTranslator()
        );
    }

    public function testDisplay()
    {
        $this->unAuditAppealView = $this->getMockBuilder(MockView::class)
            ->setMethods(['getUnAuditAppealTranslator', 'getData', 'encode','getFormatData'])
            ->getMock();

        $unAuditAppeal = new UnAuditAppeal();
        $unAuditAppealData = [
            'appealData' => [],
            'interaction'=> [],
            'reply'=>[],
        ];

        $translator = $this->prophesize(UnAuditAppealTranslator::class);
        $translator->objectToArray($unAuditAppeal)->shouldBeCalledTimes(1)->willReturn($unAuditAppealData);
        $this->unAuditAppealView->expects($this->exactly(1))
                                ->method('getUnAuditAppealTranslator')
                                ->willReturn($translator->reveal());

        $this->unAuditAppealView->expects($this->exactly(1))->method('getData')->willReturn($unAuditAppeal);
        $this->unAuditAppealView->expects($this->exactly(1))
                                ->method('getFormatData')
                                ->with($unAuditAppealData)
                                ->willReturn($unAuditAppealData);
                                
        unset($unAuditAppealData['interaction']);
        $this->unAuditAppealView->expects($this->exactly(1))->method('encode')->with($unAuditAppealData);
        $this->assertNull($this->unAuditAppealView->display());
    }
}
