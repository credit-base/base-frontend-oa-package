<?php
namespace Base\Package\Interaction\View\Json\UnAuditAppeal;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Appeal\UnAuditAppealTranslator;
use Sdk\Interaction\Model\UnAuditAppeal;

class ListViewTest extends TestCase
{
    private $unAuditAppealListView;

    public function setUp()
    {
        $this->unAuditAppealListView = new MockListView();
    }

    public function tearDown()
    {
        unset($this->unAuditAppealListView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Appeal\UnAuditAppealTranslator',
            $this->unAuditAppealListView->getUnAuditAppealTranslator()
        );
    }

    public function testDisplay()
    {
        $this->unAuditAppealListView = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getUnAuditAppealTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $unAuditAppeal = new UnAuditAppeal();
        $unAuditAppealData = [];
        $unAuditAppeals = [$unAuditAppeal];
        $count = 1;

        $translator = $this->prophesize(UnAuditAppealTranslator::class);
        $translator->objectToArray($unAuditAppeal)->shouldBeCalledTimes(1)->willReturn($unAuditAppealData);
        $this->unAuditAppealListView->expects($this->exactly(1))
                                    ->method('getUnAuditAppealTranslator')
                                    ->willReturn($translator->reveal());

        $this->unAuditAppealListView->expects($this->exactly(1))->method('getList')->willReturn($unAuditAppeals);
        $this->unAuditAppealListView->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->unAuditAppealListView->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$unAuditAppealData]
            ]
        );
        $this->assertNull($this->unAuditAppealListView->display());
    }
}
