<?php
namespace Base\Package\Interaction\View\Json\Qa;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Qa\QaTranslator;
use Sdk\Interaction\Model\Qa;

class ListViewTest extends TestCase
{
    private $qaListView;

    public function setUp()
    {
        $this->qaListView = new MockListView();
    }

    public function tearDown()
    {
        unset($this->qaListView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Qa\QaTranslator',
            $this->qaListView->getQaTranslator()
        );
    }

    public function testDisplay()
    {
        $this->qaListView = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getQaTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $qaObject = new Qa();
        $qaData = [];
        $qas = [$qaObject];
        $count = 1;

        $translator = $this->prophesize(QaTranslator::class);
        $translator->objectToArray($qaObject)->shouldBeCalledTimes(1)->willReturn($qaData);
        $this->qaListView->expects($this->exactly(1))->method('getQaTranslator')->willReturn($translator->reveal());

        $this->qaListView->expects($this->exactly(1))->method('getList')->willReturn($qas);
        $this->qaListView->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->qaListView->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$qaData]
            ]
        );
        $this->assertNull($this->qaListView->display());
    }
}
