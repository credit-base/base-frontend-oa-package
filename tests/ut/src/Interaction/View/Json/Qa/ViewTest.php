<?php
namespace Base\Package\Interaction\View\Json\Qa;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Qa\QaTranslator;
use Sdk\Interaction\Model\Qa;

class ViewTest extends TestCase
{
    private $qaView;

    public function setUp()
    {
        $this->qaView = new MockView();
    }

    public function tearDown()
    {
        unset($this->qaView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Qa\QaTranslator',
            $this->qaView->getQaTranslator()
        );
    }

    public function testDisplay()
    {
        $this->qaView = $this->getMockBuilder(MockView::class)
            ->setMethods(['getQaTranslator', 'getData', 'encode','getFormatData'])
            ->getMock();

        $qaObject = new Qa();
        $qaData = [
            'qaData' => [],
            'interaction'=> [],
            'reply'=>[],
        ];

        $translator = $this->prophesize(QaTranslator::class);
        $translator->objectToArray(
            $qaObject,
            array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'member'=>['id','realName'],
                'reply'=>[],
            )
        )->shouldBeCalledTimes(1)->willReturn($qaData);
        $this->qaView->expects($this->exactly(1))->method('getQaTranslator')->willReturn($translator->reveal());

        $this->qaView->expects($this->exactly(1))->method('getData')->willReturn($qaObject);
        $this->qaView->expects($this->exactly(1))->method('getFormatData')->with($qaData)->willReturn($qaData);
        unset($qaData['interaction']);
        $this->qaView->expects($this->exactly(1))->method('encode')->with($qaData);
        $this->assertNull($this->qaView->display());
    }
}
