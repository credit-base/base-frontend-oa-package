<?php
namespace Base\Package\Interaction\View\Json\Feedback;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;
use Sdk\Interaction\Model\Feedback;

class ViewTest extends TestCase
{
    private $feedbackView;

    public function setUp()
    {
        $this->feedbackView = new MockView();
    }

    public function tearDown()
    {
        unset($this->feedbackView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Feedback\FeedbackTranslator',
            $this->feedbackView->getFeedbackTranslator()
        );
    }

    public function testDisplay()
    {
        $this->feedbackView = $this->getMockBuilder(MockView::class)
            ->setMethods(['getFeedbackTranslator', 'getData', 'encode','getFormatData'])
            ->getMock();

        $feedback = new Feedback();
        $feedbackData = [
            'feedbackData' => [],
            'interaction'=> [],
            'reply'=>[],
        ];

        $translator = $this->prophesize(FeedbackTranslator::class);
        $translator->objectToArray(
            $feedback,
            array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'member'=>['id','realName'],
                'reply'=>[],
            )
        )->shouldBeCalledTimes(1)->willReturn($feedbackData);
        $this->feedbackView->expects($this->exactly(1))
                        ->method('getFeedbackTranslator')
                        ->willReturn($translator->reveal());

        $this->feedbackView->expects($this->exactly(1))->method('getData')->willReturn($feedback);
        $this->feedbackView->expects($this->exactly(1))
                        ->method('getFormatData')
                        ->with($feedbackData)
                        ->willReturn($feedbackData);
                        
        unset($feedbackData['interaction']);
        $this->feedbackView->expects($this->exactly(1))->method('encode')->with($feedbackData);
        $this->assertNull($this->feedbackView->display());
    }
}
