<?php
namespace Base\Package\Interaction\View\Json\Feedback;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;
use Sdk\Interaction\Model\Feedback;

class ListViewTest extends TestCase
{
    private $feedbackListView;

    public function setUp()
    {
        $this->feedbackListView = new MockListView();
    }

    public function tearDown()
    {
        unset($this->feedbackListView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Feedback\FeedbackTranslator',
            $this->feedbackListView->getFeedbackTranslator()
        );
    }

    public function testDisplay()
    {
        $this->feedbackListView = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getFeedbackTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $feedback = new Feedback();
        $feedbackData = [];
        $feedbacks = [$feedback];
        $count = 1;

        $translator = $this->prophesize(FeedbackTranslator::class);
        $translator->objectToArray($feedback)->shouldBeCalledTimes(1)->willReturn($feedbackData);
        $this->feedbackListView->expects($this->exactly(1))
                            ->method('getFeedbackTranslator')
                            ->willReturn($translator->reveal());

        $this->feedbackListView->expects($this->exactly(1))->method('getList')->willReturn($feedbacks);
        $this->feedbackListView->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->feedbackListView->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$feedbackData]
            ]
        );
        $this->assertNull($this->feedbackListView->display());
    }
}
