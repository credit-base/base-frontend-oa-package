<?php
namespace Base\Package\Interaction\View\Json\Appeal;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Appeal\AppealTranslator;
use Sdk\Interaction\Model\Appeal;

class ListViewTest extends TestCase
{
    private $appealListView;

    public function setUp()
    {
        $this->appealListView = new MockListView();
    }

    public function tearDown()
    {
        unset($this->appealListView);
    }

    public function testGetList()
    {
        $this->assertIsArray($this->appealListView->getList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->appealListView->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Appeal\AppealTranslator',
            $this->appealListView->getAppealTranslator()
        );
    }

    public function testDisplay()
    {
        $this->appealListView = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getAppealTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $appeal = new Appeal();
        $appealData = [];
        $appeals = [$appeal];
        $count = 1;

        $translator = $this->prophesize(AppealTranslator::class);
        $translator->objectToArray($appeal)->shouldBeCalledTimes(1)->willReturn($appealData);
        $this->appealListView->expects($this->exactly(1))
                            ->method('getAppealTranslator')
                            ->willReturn($translator->reveal());

        $this->appealListView->expects($this->exactly(1))->method('getList')->willReturn($appeals);
        $this->appealListView->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->appealListView->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$appealData]
            ]
        );
        $this->assertNull($this->appealListView->display());
    }
}
