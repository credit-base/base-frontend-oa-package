<?php
namespace Base\Package\Interaction\View\Json\Appeal;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Appeal\AppealTranslator;
use Sdk\Interaction\Model\Appeal;

class ViewTest extends TestCase
{
    private $appealView;

    public function setUp()
    {
        $this->appealView = new MockView();
    }

    public function tearDown()
    {
        unset($this->appealView);
    }

    public function testGetData()
    {
        $this->assertIsArray($this->appealView->getData());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Appeal\AppealTranslator',
            $this->appealView->getAppealTranslator()
        );
    }

    public function testDisplay()
    {
        $this->appealView = $this->getMockBuilder(MockView::class)
            ->setMethods(['getAppealTranslator', 'getData', 'encode','getFormatData'])
            ->getMock();

        $appeal = new Appeal();
        $appealData = [
            'appealData' => [],
            'interaction'=> [],
            'reply'=>[],
        ];

        $translator = $this->prophesize(AppealTranslator::class);
        $translator->objectToArray(
            $appeal,
            array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'certificates',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'member'=>['id','realName'],
                'reply'=>[],
            )
        )->shouldBeCalledTimes(1)->willReturn($appealData);
        $this->appealView->expects($this->exactly(1))->method('getAppealTranslator')->willReturn($translator->reveal());

        $this->appealView->expects($this->exactly(1))->method('getData')->willReturn($appeal);
        $this->appealView->expects($this->exactly(1))
                        ->method('getFormatData')
                        ->with($appealData)
                        ->willReturn($appealData);
                        
        unset($appealData['interaction']);
        $this->appealView->expects($this->exactly(1))->method('encode')->with($appealData);
        $this->assertNull($this->appealView->display());
    }

    public function testGetFormatData()
    {
        $arrayData = [
            'acceptStatus'=>[],
            'acceptUserGroup'=>[],
            'reply'=>[],
            'certificates'=>[],
            'title'=>'',
            'type'=>'',
            'name'=>'',
            'identify'=>'',
            'images'=>[],
            'content'=>'',
            'contact'=>'',
            'updateTimeFormat'=>'',
            'status'=>[],
            'member'=>[],
            'subject'=>''
        ];

        $resultData = [
            'reply'=>[
                'acceptStatus'=>[],
                'acceptUserGroup'=>[],
            ],
            'interaction'=>[
                'certificates'=>[],
                'title'=>'',
                'type'=>'',
                'name'=>'',
                'identify'=>'',
                'images'=>[],
                'content'=>'',
                'contact'=>'',
                'updateTimeFormat'=>'',
                'status'=>[],
                'member'=>[],
                'subject'=>'',
            ]
        ];

        $result = $this->appealView->getFormatData($arrayData);
        $this->assertEquals($result, $resultData);
    }
}
