<?php
namespace Base\Package\Interaction\View\Json\UnAuditFeedback;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Feedback\UnAuditFeedbackTranslator;
use Sdk\Interaction\Model\UnAuditFeedback;

class ViewTest extends TestCase
{
    private $unAuditFeedbackView;

    public function setUp()
    {
        $this->unAuditFeedbackView = new MockView();
    }

    public function tearDown()
    {
        unset($this->unAuditFeedbackView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Feedback\UnAuditFeedbackTranslator',
            $this->unAuditFeedbackView->getUnAuditFeedbackTranslator()
        );
    }

    public function testDisplay()
    {
        $this->unAuditFeedbackView = $this->getMockBuilder(MockView::class)
            ->setMethods(['getUnAuditFeedbackTranslator', 'getData', 'encode','getFormatData'])
            ->getMock();

        $unAuditFeedback = new UnAuditFeedback();
        $unAuditFeedbackData = [
            'feedbackData' => [],
            'interaction'=> [],
            'reply'=>[],
        ];

        $translator = $this->prophesize(UnAuditFeedbackTranslator::class);
        $translator->objectToArray($unAuditFeedback)->shouldBeCalledTimes(1)->willReturn($unAuditFeedbackData);
        $this->unAuditFeedbackView->expects($this->exactly(1))
                                ->method('getUnAuditFeedbackTranslator')
                                ->willReturn($translator->reveal());

        $this->unAuditFeedbackView->expects($this->exactly(1))->method('getData')->willReturn($unAuditFeedback);
        $this->unAuditFeedbackView->expects($this->exactly(1))
                                ->method('getFormatData')->with($unAuditFeedbackData)
                                ->willReturn($unAuditFeedbackData);
                                
        unset($unAuditFeedbackData['interaction']);
        $this->unAuditFeedbackView->expects($this->exactly(1))->method('encode')->with($unAuditFeedbackData);
        $this->assertNull($this->unAuditFeedbackView->display());
    }
}
