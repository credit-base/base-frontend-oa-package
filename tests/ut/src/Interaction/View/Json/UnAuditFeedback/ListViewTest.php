<?php
namespace Base\Package\Interaction\View\Json\UnAuditFeedback;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Feedback\UnAuditFeedbackTranslator;
use Sdk\Interaction\Model\UnAuditFeedback;

class ListViewTest extends TestCase
{
    private $unAuditFeedbackListView;

    public function setUp()
    {
        $this->unAuditFeedbackListView = new MockListView();
    }

    public function tearDown()
    {
        unset($this->unAuditFeedbackListView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Feedback\UnAuditFeedbackTranslator',
            $this->unAuditFeedbackListView->getUnAuditFeedbackTranslator()
        );
    }

    public function testDisplay()
    {
        $this->unAuditFeedbackListView = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getUnAuditFeedbackTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $unAuditFeedback = new UnAuditFeedback();
        $unAuditFeedbackData = [];
        $unAuditFeedbacks = [$unAuditFeedback];
        $count = 1;

        $translator = $this->prophesize(UnAuditFeedbackTranslator::class);
        $translator->objectToArray($unAuditFeedback)->shouldBeCalledTimes(1)->willReturn($unAuditFeedbackData);
        $this->unAuditFeedbackListView->expects($this->exactly(1))
                                    ->method('getUnAuditFeedbackTranslator')
                                    ->willReturn($translator->reveal());

        $this->unAuditFeedbackListView->expects($this->exactly(1))->method('getList')->willReturn($unAuditFeedbacks);
        $this->unAuditFeedbackListView->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->unAuditFeedbackListView->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$unAuditFeedbackData]
            ]
        );
        $this->assertNull($this->unAuditFeedbackListView->display());
    }
}
