<?php
namespace Base\Package\Interaction\View\Json\Complaint;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;
use Sdk\Interaction\Model\Complaint;

class ViewTest extends TestCase
{
    private $complaintView;

    public function setUp()
    {
        $this->complaintView = new MockView();
    }

    public function tearDown()
    {
        unset($this->complaintView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Complaint\ComplaintTranslator',
            $this->complaintView->getComplaintTranslator()
        );
    }

    public function testDisplay()
    {
        $this->complaintView = $this->getMockBuilder(MockView::class)
            ->setMethods(['getComplaintTranslator', 'getData', 'encode','getFormatData'])
            ->getMock();

        $complaint = new Complaint();
        $complaintData = [
            'complaintData' => [],
            'interaction'=> [],
            'reply'=>[],
        ];

        $translator = $this->prophesize(ComplaintTranslator::class);
        $translator->objectToArray(
            $complaint,
            array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'updateTime',
                'status',
                'acceptUserGroup'=>['id','name'],
                'member'=>['id','realName'],
                'reply'=>[],
            )
        )->shouldBeCalledTimes(1)->willReturn($complaintData);
        $this->complaintView->expects($this->exactly(1))
                            ->method('getComplaintTranslator')
                            ->willReturn($translator->reveal());

        $this->complaintView->expects($this->exactly(1))->method('getData')->willReturn($complaint);
        $this->complaintView->expects($this->exactly(1))
                            ->method('getFormatData')
                            ->with($complaintData)
                            ->willReturn($complaintData);
                            
        unset($complaintData['interaction']);
        $this->complaintView->expects($this->exactly(1))->method('encode')->with($complaintData);
        $this->assertNull($this->complaintView->display());
    }
}
