<?php
namespace Base\Package\Interaction\View\Json\Complaint;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;
use Sdk\Interaction\Model\Complaint;

class ListViewTest extends TestCase
{
    private $complaintListView;

    public function setUp()
    {
        $this->complaintListView = new MockListView();
    }

    public function tearDown()
    {
        unset($this->complaintListView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Complaint\ComplaintTranslator',
            $this->complaintListView->getComplaintTranslator()
        );
    }

    public function testDisplay()
    {
        $this->complaintListView = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getComplaintTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $complaint = new Complaint();
        $complaintData = [];
        $complaints = [$complaint];
        $count = 1;

        $translator = $this->prophesize(ComplaintTranslator::class);
        $translator->objectToArray($complaint)->shouldBeCalledTimes(1)->willReturn($complaintData);
        $this->complaintListView->expects($this->exactly(1))
                                ->method('getComplaintTranslator')
                                ->willReturn($translator->reveal());

        $this->complaintListView->expects($this->exactly(1))->method('getList')->willReturn($complaints);
        $this->complaintListView->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->complaintListView->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$complaintData]
            ]
        );
        $this->assertNull($this->complaintListView->display());
    }
}
