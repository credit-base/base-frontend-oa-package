<?php
namespace Base\Package\Interaction\View\Json\UnAuditQa;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Qa\UnAuditQaTranslator;
use Sdk\Interaction\Model\UnAuditQa;

class ViewTest extends TestCase
{
    private $unAuditQaView;

    public function setUp()
    {
        $this->unAuditQaView = new MockView();
    }

    public function tearDown()
    {
        unset($this->unAuditQaView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Qa\UnAuditQaTranslator',
            $this->unAuditQaView->getUnAuditQaTranslator()
        );
    }

    public function testDisplay()
    {
        $this->unAuditQaView = $this->getMockBuilder(MockView::class)
            ->setMethods(['getUnAuditQaTranslator', 'getData', 'encode','getFormatData'])
            ->getMock();

        $unAuditQa = new UnAuditQa();
        $unAuditQaData = [
            'qaData' => [],
            'interaction'=> [],
            'reply'=>[],
        ];

        $translator = $this->prophesize(UnAuditQaTranslator::class);
        $translator->objectToArray($unAuditQa)->shouldBeCalledTimes(1)->willReturn($unAuditQaData);
        $this->unAuditQaView->expects($this->exactly(1))
                            ->method('getUnAuditQaTranslator')
                            ->willReturn($translator->reveal());

        $this->unAuditQaView->expects($this->exactly(1))->method('getData')->willReturn($unAuditQa);
        $this->unAuditQaView->expects($this->exactly(1))
                            ->method('getFormatData')
                            ->with($unAuditQaData)
                            ->willReturn($unAuditQaData);
                            
        unset($unAuditQaData['interaction']);
        $this->unAuditQaView->expects($this->exactly(1))->method('encode')->with($unAuditQaData);
        $this->assertNull($this->unAuditQaView->display());
    }
}
