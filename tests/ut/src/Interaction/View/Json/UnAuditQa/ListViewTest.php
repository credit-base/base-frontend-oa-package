<?php
namespace Base\Package\Interaction\View\Json\UnAuditQa;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Translator\Qa\UnAuditQaTranslator;
use Sdk\Interaction\Model\UnAuditQa;

class ListViewTest extends TestCase
{
    private $unAuditQaListView;

    public function setUp()
    {
        $this->unAuditQaListView = new MockListView();
    }

    public function tearDown()
    {
        unset($this->unAuditQaListView);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Qa\UnAuditQaTranslator',
            $this->unAuditQaListView->getUnAuditQaTranslator()
        );
    }

    public function testDisplay()
    {
        $this->unAuditQaListView = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getUnAuditQaTranslator', 'getList', 'encode','getCount'])
            ->getMock();

        $unAuditQa = new UnAuditQa();
        $unAuditQaData = [];
        $unAuditQas = [$unAuditQa];
        $count = 1;

        $translator = $this->prophesize(UnAuditQaTranslator::class);
        $translator->objectToArray($unAuditQa)->shouldBeCalledTimes(1)->willReturn($unAuditQaData);
        $this->unAuditQaListView->expects($this->exactly(1))
                                ->method('getUnAuditQaTranslator')
                                ->willReturn($translator->reveal());

        $this->unAuditQaListView->expects($this->exactly(1))->method('getList')->willReturn($unAuditQas);
        $this->unAuditQaListView->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->unAuditQaListView->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$unAuditQaData]
            ]
        );
        $this->assertNull($this->unAuditQaListView->display());
    }
}
