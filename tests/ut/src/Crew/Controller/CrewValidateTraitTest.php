<?php
namespace Base\Package\Crew\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\User\WidgetRules\UserWidgetRules;
use Sdk\Crew\WidgetRules\CrewWidgetRules;

use Sdk\Crew\Model\Crew;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CrewValidateTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockCrewValidateTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetUserWidgetRules()
    {
        $trait = new MockCrewValidateTrait();
        $this->assertInstanceOf(
            'Sdk\User\WidgetRules\UserWidgetRules',
            $trait->publicGetUserWidgetRules()
        );
    }

    public function testGetCrewWidgetRules()
    {
        $trait = new MockCrewValidateTrait();
        $this->assertInstanceOf(
            'Sdk\Crew\WidgetRules\CrewWidgetRules',
            $trait->publicGetCrewWidgetRules()
        );
    }

    public function testGetWidgetRules()
    {
        $trait = new MockCrewValidateTrait();
        $this->assertInstanceOf(
            'Sdk\Common\WidgetRules\WidgetRules',
            $trait->publicGetWidgetRules()
        );
    }

    public function testValidateCommonScenario()
    {
        $trait = $this->getMockBuilder(MockCrewValidateTrait::class)
                           ->setMethods(
                               [
                                    'getWidgetRules',
                                    'getCrewWidgetRules',
                                    'getUserWidgetRules'
                                ]
                           )
                           ->getMock();

        $realName = $this->faker->name();
        $cardId = $this->faker->creditCardNumber();
        $category = Crew::CATEGORY['SUPERTUBE'];
        $purview = array($this->faker->randomDigitNotNull());
        $userGroupId = $this->faker->randomDigitNotNull();
        $departmentId = $this->faker->randomDigitNotNull();

        $commonWidgetRules = $this->prophesize(WidgetRules::class);
        $crewWidgetRules = $this->prophesize(CrewWidgetRules::class);
        $userWidgetRules = $this->prophesize(UserWidgetRules::class);

        $userWidgetRules->realName(Argument::exact($realName))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRules->cardId(Argument::exact($cardId))->shouldBeCalledTimes(1)->willReturn(true);
        $crewWidgetRules->category(Argument::exact($category))->shouldBeCalledTimes(1)->willReturn(true);
        $crewWidgetRules->purview(Argument::exact($purview))->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->formatNumeric(
            Argument::exact($userGroupId),
            Argument::exact('userGroupId')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRules->formatNumeric(
            Argument::exact($departmentId),
            Argument::exact('departmentId')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getWidgetRules')
            ->willReturn($commonWidgetRules->reveal());

        $trait->expects($this->exactly(1))
            ->method('getCrewWidgetRules')
            ->willReturn($crewWidgetRules->reveal());

        $trait->expects($this->exactly(1))
            ->method('getUserWidgetRules')
            ->willReturn($userWidgetRules->reveal());

        $result = $trait->publicValidateCommonScenario(
            $realName,
            $cardId,
            $category,
            $purview,
            $userGroupId,
            $departmentId
        );

        $this->assertTrue($result);
    }

    public function testValidateAddScenario()
    {
        $trait = $this->getMockBuilder(MockCrewValidateTrait::class)
                           ->setMethods(
                               [
                                    'getUserWidgetRules'
                                ]
                           )
                           ->getMock();

        $cellphone = $this->faker->phoneNumber();
        $password = $this->faker->md5();
        $confirmPassword = $this->faker->md5();

        $userWidgetRules = $this->prophesize(UserWidgetRules::class);

        $userWidgetRules->cellphone(Argument::exact($cellphone))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRules->password(Argument::exact($password))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRules->confirmPassword(
            Argument::exact($password),
            Argument::exact($confirmPassword)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getUserWidgetRules')
            ->willReturn($userWidgetRules->reveal());

        $result = $trait->publicValidateAddScenario(
            $cellphone,
            $password,
            $confirmPassword
        );

        $this->assertTrue($result);
    }

    public function testValidateSignInScenario()
    {
        $trait = $this->getMockBuilder(MockCrewValidateTrait::class)
                           ->setMethods(
                               [
                                    'getUserWidgetRules',
                                    'validateCaptcha'
                                ]
                           )
                           ->getMock();

        $userName = $this->faker->phoneNumber();
        $password = $this->faker->md5();
        $captcha = $this->faker->phoneNumber();

        $userWidgetRules = $this->prophesize(UserWidgetRules::class);

        $userWidgetRules->hash()->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRules->cellphone(Argument::exact($userName))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRules->password(Argument::exact($password))->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('validateCaptcha')
            ->with($captcha)
            ->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getUserWidgetRules')
            ->willReturn($userWidgetRules->reveal());

        $result = $trait->publicValidateSignInScenario(
            $userName,
            $password,
            $captcha
        );

        $this->assertTrue($result);
    }
}
