<?php
namespace Base\Package\Crew\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Request;

use Base\Package\Common\Controller\Interfaces\IEnableAbleController;

use Sdk\Crew\Command\Crew\EnableCrewCommand;
use Sdk\Crew\Command\Crew\DisableCrewCommand;

class CrewEnableControllerTest extends TestCase
{
    private $crewStub;

    public function setUp()
    {
        $this->crewStub = $this->getMockBuilder(MockCrewEnableController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->crewStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new CrewEnableController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new CrewEnableController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IEnableAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $crewStub = new MockCrewEnableController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $crewStub->getCommandBus()
        );
    }

    public function testEnableAction()
    {
        $id = 1;

        $command = new EnableCrewCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->crewStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->crewStub->enableAction($id);
        $this->assertTrue($result);
    }

    public function testDisableAction()
    {
        $id = 1;

        $command = new DisableCrewCommand($id);
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)
            ->willReturn(true);

        $this->crewStub->expects($this->exactly($id))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        $result = $this->crewStub->disableAction($id);
        $this->assertTrue($result);
    }
}
