<?php

namespace Base\Package\Crew\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\Response;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\Guest;
use Sdk\Crew\Repository\CrewRepository;
use Sdk\Crew\Command\Crew\AddCrewCommand;
use Sdk\Crew\Command\Crew\EditCrewCommand;
use Sdk\Crew\Command\Crew\SignInCrewCommand;
use Sdk\Crew\Command\Crew\SignOutCrewCommand;
use Base\Package\Crew\View\Json\CrewView;
use Base\Package\Crew\View\Json\SignInView;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CrewOperationControllerTest extends TestCase
{
    private $crewController;

    public function setUp()
    {
        $this->crewController = $this->getMockBuilder(MockCrewOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->crewController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $crewController = new CrewOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $crewController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $crewController = new CrewOperationController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IOperateAbleController', $crewController);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Crew\Repository\CrewRepository',
            $this->crewController->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->crewController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->crewController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->crewController->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        $this->crewController = $this->getMockBuilder(MockCrewOperationController::class)
            ->setMethods([
                    'getRequest', 'displayError', 'displaySuccess', 'getCommandBus', 'validateCommonScenario',
                    'validateAddScenario', 'decrypt'
                ])->getMock();

        $faker = \Faker\Factory::create('zh_CN');

        $realName = $faker->name();
        $cellphone = $faker->phoneNumber();
        $cardId = $faker->creditCardNumber();
        $purview = array($faker->randomDigitNotNull());
        $category = Crew::CATEGORY['SUPERTUBE'];
        $categoryEncode = marmot_encode($category);
        $userGroupId = $faker->randomDigitNotNull();
        $userGroupIdEncode = marmot_encode($userGroupId);
        $departmentId = $faker->randomDigitNotNull();
        $departmentIdEncode = marmot_encode($departmentId);
        $password = 'password';
        $passwordDecrypt = 'passwordDecrypted';
        $confirmPassword = 'confirmPassword';
        $confirmPasswordDecrypt = 'confirmPasswordDecrypted';

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('realName'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($realName);
        $request->post(Argument::exact('cellphone'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($cellphone);
        $request->post(Argument::exact('cardId'), Argument::exact(''))->shouldBeCalledTimes(1)->willReturn($cardId);
        $request->post(
            Argument::exact('purview'),
            Argument::exact(array())
        )->shouldBeCalledTimes(1)->willReturn($purview);
        $request->post(Argument::exact('category'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($categoryEncode);
        $request->post(Argument::exact('userGroupId'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($userGroupIdEncode);
        $request->post(Argument::exact('departmentId'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($departmentIdEncode);
        $request->post(
            Argument::exact('password'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($password);
        $request->post(Argument::exact('confirmPassword'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($confirmPassword);

        $this->crewController->expects($this->any())->method('getRequest')->willReturn($request->reveal());

        $this->crewController->expects($this->exactly(2))->method('decrypt')
            ->will($this->returnValueMap([
                [
                    'password','passwordDecrypted',
                ],
                [
                    'confirmPassword','confirmPasswordDecrypted'
                ]
            ]));

        $this->crewController->expects($this->exactly(1))->method('validateCommonScenario')
            ->with(
                $realName,
                $cardId,
                $category,
                $purview,
                $userGroupId,
                $departmentId
            )->willReturn(true);

        $this->crewController->expects($this->exactly(1))->method('validateAddScenario')
            ->with(
                $cellphone,
                $passwordDecrypt,
                $confirmPasswordDecrypt
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddCrewCommand(
            $realName,
            $cellphone,
            $passwordDecrypt,
            $cardId,
            $userGroupId,
            $category,
            $departmentId,
            $purview
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->crewController->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->crewController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->crewController->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->crewController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->crewController->addAction();
        $this->assertTrue($result);
    }

    public function testEditViewNullFailure()
    {
        $this->crewController = $this->getMockBuilder(MockCrewOperationController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $crew = new Guest();

        $repository = $this->prophesize(CrewRepository::class);
        $repository->scenario(Argument::exact(CrewRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($crew);

        $this->crewController->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->crewController->editView($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testEditViewSuccess()
    {
        $this->crewController = $this->getMockBuilder(MockCrewOperationController::class)
            ->setMethods(
                [
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;
        $crew = new Crew($id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->scenario(Argument::exact(CrewRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($crew);

        $this->crewController->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->crewController->expects($this->exactly(1))
            ->method('render')
            ->with(new CrewView($crew));

        $result = $this->crewController->editView($id);
        $this->assertTrue($result);
    }

    private function initialEdit(int $id, bool $result)
    {
        $this->crewController = $this->getMockBuilder(MockCrewOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'validateCommonScenario',
                    'getRequest',
                    'displayError',
                    'displaySuccess'
                ]
            )->getMock();

        $faker = \Faker\Factory::create('zh_CN');

        $realName = $faker->name();
        $cardId = $faker->creditCardNumber();
        $purview = array($faker->randomDigitNotNull());
        $category = Crew::CATEGORY['SUPERTUBE'];
        $categoryEncode = marmot_encode($category);

        $userGroupId = $faker->randomDigitNotNull();
        $userGroupIdEncode = marmot_encode($userGroupId);

        $departmentId = $faker->randomDigitNotNull();
        $departmentIdEncode = marmot_encode($departmentId);

        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('realName'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($realName);
        $request->post(Argument::exact('cardId'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($cardId);
        $request->post(Argument::exact('purview'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)
                ->willReturn($purview);
        $request->post(Argument::exact('userGroupId'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($userGroupIdEncode);
        $request->post(Argument::exact('departmentId'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($departmentIdEncode);
        $request->post(Argument::exact('category'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($categoryEncode);

        $this->crewController->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->crewController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $realName,
                $cardId,
                $category,
                $purview,
                $userGroupId,
                $departmentId
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditCrewCommand(
                    $realName,
                    $cardId,
                    $id,
                    $userGroupId,
                    $category,
                    $departmentId,
                    $purview
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->crewController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testEditActionFailure()
    {
        $id = 2;

        $this->initialEdit($id, false);

        $this->crewController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->crewController->editAction($id);
        $this->assertFalse($result);
    }

    public function testEditActionSuccess()
    {
        $id = 2;

        $this->initialEdit($id, true);

        $this->crewController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->crewController->editAction($id);
        
        $this->assertTrue($result);
    }

    private function initialSignIn(bool $result)
    {
        //初始化
        $this->stub = $this->getMockBuilder(MockCrewOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'render',
                    'getCommandBus',
                    'decrypt',
                    'validateSignInScenario'
                ]
            )->getMock();

        $cellphone = '15339083333';
        $password = 'password';
        $passwordDecrypt = 'passwordDecrypted';
        $captcha = 'captcha';

        //预言
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('userName'), Argument::exact(''))
            ->shouldBeCalledTimes(1)
            ->willReturn($cellphone);
        $request->post(Argument::exact('password'), Argument::exact(''))
            ->shouldBeCalledTimes(1)
            ->willReturn($password);
        $request->post(Argument::exact('captcha'), Argument::exact(''))
            ->shouldBeCalledTimes(1)
            ->willReturn($captcha);

        $this->stub->expects($this->exactly(1))
            ->method('decrypt')
            ->with($password)
            ->willReturn($passwordDecrypt);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('validateSignInScenario')
            ->with($cellphone, $passwordDecrypt, $captcha)
            ->willReturn(true);

        $command = new SignInCrewCommand(
            $cellphone,
            $passwordDecrypt
        );

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testSignInSuccess()
    {
        $this->initialSignIn(true);

        Core::$container->set('jwt', array('jwt'));
        $res = Core::$container->get('jwt');

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new SignInView($res));

        $result = $this->stub->signIn();
        $this->assertTrue($result);
    }

    public function testSignInFailure()
    {
        $this->initialSignIn(false);

        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->signIn();
        $this->assertFalse($result);
    }
    
    public function testSignOutSuccess()
    {
        //初始化
        $this->stub = $this->getMockBuilder(MockCrewOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'displaySuccess'
                ]
            )->getMock();

        //预测
        $commandBus = $this->prophesize(CommandBus::class);
        $command = new SignOutCrewCommand();
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $this->stub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
        $this->stub->expects($this->exactly(1))
            ->method('displaySuccess');

        //验证
        $result = $this->stub->signOut();
        $this->assertTrue($result);
    }

    public function testSignOutFailure()
    {
        //初始化
        $this->stub = $this->getMockBuilder(MockCrewOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        //预测
        $commandBus = $this->prophesize(CommandBus::class);
        $command = new SignOutCrewCommand();
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn(false);

        //绑定
        $this->stub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
        $this->stub->expects($this->exactly(1))
            ->method('displayError');

        //验证
        $result = $this->stub->signOut();
        $this->assertFalse($result);
    }
}
