<?php
namespace Base\Package\Crew\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\Response;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\Guest;
use Sdk\Crew\Repository\CrewRepository;

use Base\Package\Crew\View\Json\CrewView;
use Base\Package\Crew\View\Json\CrewListView;
use Base\Package\Crew\View\Json\PersonalInformationView;

class CrewFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockCrewFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new CrewFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Crew\Repository\CrewRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockCrewFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $crew = new Guest();

        $repository = $this->prophesize(CrewRepository::class);
        $repository->scenario(Argument::exact(CrewRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($crew);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockCrewFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $crew = new Crew($id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->scenario(
            Argument::exact(CrewRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($crew);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new CrewView($crew));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockCrewFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter['realName'] = 'realName';
        $filter['cellphone'] = 'cellphone';
        $filter['category'] = Crew::CATEGORY['SUPERTUBE'];
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $crewArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(CrewRepository::class);
        $repository->scenario(Argument::exact(CrewRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$crewArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new CrewListView($crewArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockCrewFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $sort = 'updateTime';
        $realName = 'realName';
        $cellphone = 'cellphone';
        $category = Crew::CATEGORY['SUPERTUBE'];
        $categoryEncode = marmot_encode($category);
        $userGroupId = 1;
        $userGroupIdEncode = marmot_encode($userGroupId);
        $departmentId = 1;
        $departmentIdEncode = marmot_encode($departmentId);

        $id = 1;
        $crew = new Crew($id);
        $crew->getUserGroup()->setId(0);
        $crew->setCategory($category);

        Core::$container->set('crew', $crew);

        $filter['realName'] = $realName;
        $filter['cellphone'] = $cellphone;
        $filter['userGroup'] = $userGroupId;
        $filter['department'] = $departmentId;
        $filter['category'] = $category;

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $request->get(
            Argument::exact('realName'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($realName);

        $request->get(
            Argument::exact('cellphone'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($cellphone);

        $request->get(
            Argument::exact('category'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($categoryEncode);

        $request->get(
            Argument::exact('userGroupId'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userGroupIdEncode);

        $request->get(
            Argument::exact('departmentId'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($departmentIdEncode);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort)], $result);
    }

    public function testPersonalInformationNullFailure()
    {
        $this->stub = $this->getMockBuilder(MockCrewFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $guest = new Guest();

        Core::$container->set('crew', $guest);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->scenario(Argument::exact(CrewRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($guest->getId()))
            ->shouldBeCalledTimes(1)
            ->willReturn($guest);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->personalInformation();
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testPersonalInformationSuccess()
    {
        $this->stub = $this->getMockBuilder(MockCrewFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;
        $crew = new Crew($id);
        Core::$container->set('crew', $crew);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->scenario(Argument::exact(CrewRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($crew);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new PersonalInformationView($crew));

        $result = $this->stub->personalInformation();
        $this->assertTrue($result);
    }
}
