<?php
namespace Base\Package\Crew\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Crew\Model\Crew;
use Sdk\Crew\Translator\CrewTranslator;

class CrewViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockCrewView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewTranslator',
            $this->view->getTranslator()
        );
    }

    public function testGetCrew()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Model\Crew',
            $this->view->getCrew()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockCrewView::class)
            ->setMethods(['getTranslator','encode','getCrew'])->getMock();

        $crew = new Crew(2);
        $crewData = [];

        $this->view->expects($this->exactly(1))->method('getCrew')->willReturn($crew);
        $translator = $this->prophesize(CrewTranslator::class);
        $translator->objectToArray($crew)->shouldBeCalledTimes(1)->willReturn($crewData);

        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('encode')->with($crewData);

        $this->assertNull($this->view->display());
    }
}
