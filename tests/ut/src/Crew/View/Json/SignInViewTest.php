<?php
namespace Base\Package\Crew\View\Json;

use PHPUnit\Framework\TestCase;

class SignInViewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new  MockSignInView();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockSignInView::class)
            ->setMethods(['encode'])->getMock();

        $this->stub->expects($this->exactly(1))->method('encode');

        $this->assertNull($this->stub->display());
    }
}
