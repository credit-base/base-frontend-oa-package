<?php
namespace Base\Package\Crew\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Crew\Model\Crew;
use Sdk\Crew\Translator\CrewTranslator;

class CrewListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockCrewListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewTranslator',
            $this->view->getTranslator()
        );
    }

    public function testGetCount()
    {
        $this->assertIsInt(
            $this->view->getCount()
        );
    }

    public function testGetCrews()
    {
        $this->assertIsArray(
            $this->view->getCrews()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockCrewListView::class)
            ->setMethods(['encode','getTranslator','getCrews','getCount'])->getMock();

        $crew = new Crew(1);
        $crews = [$crew];
        $count = count($crews);
        $crewResult = [];

        $translator = $this->prophesize(CrewTranslator::class);
        $translator->objectToArray(
            $crew,
            array(
                'id',
                'realName',
                'cellphone',
                'category',
                'userGroup',
                'department',
                'status',
                'updateTime',
            )
        )->shouldBeCalledTimes(1)->willReturn($crewResult);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getCrews')->willReturn($crews);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $dataList = [
            'total' => $count,
            'list' => [$crewResult]
        ];
        $this->view->expects($this->exactly(1))->method('encode')->with($dataList);

        $this->assertNull($this->view->display());
    }
}
