<?php
namespace Base\Package\Crew\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\Crew\Model\Crew;
use Sdk\Crew\Translator\CrewTranslator;

class PersonalInformationViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockPersonalInformationView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetCrew()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Model\Crew',
            $this->view->getCrew()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockPersonalInformationView::class)
            ->setMethods(['encode','getTranslator','getCrew'])->getMock();

        $crew = new Crew(1);

        $this->view->expects($this->exactly(1))->method('getCrew')->willReturn($crew);
        $translator = $this->prophesize(CrewTranslator::class);
        $translator->objectToArray($crew)->shouldBeCalledTimes(1)->willReturn([]);

        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('encode')->with([]);

        $this->assertNull($this->view->display());
    }
}
