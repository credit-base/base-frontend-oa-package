<?php
namespace Base\Package\Statistical\View;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Statistical\Translator\StaticsEnterpriseRelationInformationCountTranslator;

use Sdk\Statistical\Model\Statistical;

class StatisticalViewTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockStatisticalViewTrait::class)
            ->setMethods()->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testStatisticalArray()
    {
        $this->stub = $this->getMockBuilder(MockStatisticalViewTrait::class)
            ->setMethods(
                [
                    'getStatisticalTranslator',
                ]
            )->getMock();
        $type = 'enterpriseRelationInformationCount';

        $translator = $this->prophesize(StaticsEnterpriseRelationInformationCountTranslator::class);

        $this->stub->expects($this->exactly(1))
        ->method('getStatisticalTranslator')
        ->willReturn($translator->reveal());

        $statistical = new Statistical();
        $data = [1,2];
        $translator->objectToArray(Argument::exact($statistical))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $result = $this->stub->statisticalArray($type, $statistical);
        $this->assertEquals($result, $data);
    }

    public function testGetStatisticalTranslator()
    {
        $type = 'enterpriseRelationInformationCount';

        $result = $this->stub->getPublicStatisticalTranslator($type);
       
        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Translator\StaticsEnterpriseRelationInformationCountTranslator',
            $result
        );
    }
}
