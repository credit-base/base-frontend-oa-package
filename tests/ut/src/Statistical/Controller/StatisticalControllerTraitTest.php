<?php
namespace Base\Package\Statistical\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Statistical\Repository\Statistical\StatisticalRepository;

use Sdk\Statistical\Model\Statistical;

class StatisticalControllerTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockStatisticalControllerTrait::class)
            ->setMethods(
                [
                    'getAdapter',
                    'analyse',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testStatistical()
    {
        $this->stub = $this->getMockBuilder(MockStatisticalControllerTrait::class)
            ->setMethods(
                [
                    'getStatisticalRepository',
                ]
            )->getMock();
        $type = 'enterpriseRelationInformationCount';
        $unifiedSocialCreditCode = 'unifiedSocialCreditCode';
        $filter['unifiedSocialCreditCode'] = $unifiedSocialCreditCode;

        $repository = $this->prophesize(StatisticalRepository::class);

        $this->stub->expects($this->exactly(1))
        ->method('getStatisticalRepository')
        ->willReturn($repository->reveal());

        $statistical = new Statistical();
        $repository->analyse(Argument::exact($filter))
            ->shouldBeCalledTimes(1)
            ->willReturn($statistical);

        $result = $this->stub->statistical($type, $filter);
        $this->assertEquals($result, $statistical);
    }

    public function testGetStatisticalRepository()
    {
        $type = 'enterpriseRelationInformationCount';

        $result = $this->stub->getPublicStatisticalRepository($type);
       
        $this->assertInstanceOf('Sdk\Statistical\Repository\Statistical\StatisticalRepository', $result);
    }
}
