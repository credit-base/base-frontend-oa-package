<?php


namespace Base\Package\WebsiteCustomize\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;
use Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Marmot\Framework\Classes\Filter;

class ViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetWebsiteCustomize()
    {
        $this->assertInstanceOf(
            'Sdk\WebsiteCustomize\Model\WebsiteCustomize',
            $this->view->getWebsiteCustomize()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(['getTranslator','getFormatContentByEncode','getWebsiteCustomize','encode'])
            ->getMock();

        $news = new WebsiteCustomize();

        $content = [
            'id' => 'MA'
        ];
        $newsData = [
            'content' => $content
        ];

        $translator = $this->prophesize(WebsiteCustomizeTranslator::class);
        $translator->objectToArray($news)->shouldBeCalledTimes(1)->willReturn($newsData);


        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getWebsiteCustomize')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getFormatContentByEncode')
            ->with($newsData['content'])
            ->willReturn([]);

        $result = [
            'content' => []
        ];

        $this->view->expects($this->exactly(1))->method('encode')->with($result);
        $this->assertNull($this->view->display());
    }

    public function testGetRelatedLinksByEncode()
    {
        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(['getItemsListByEncode'])
            ->getMock();

        $status = 1;
        $items = $itemsResult = [
            'id' => 1
        ];
        $relatedLinks = [
            array(
                'status' => $status,
                'items' => $items
            )
        ];

        $this->view->expects($this->exactly(1))
            ->method('getItemsListByEncode')
            ->with($items)
            ->willReturn($itemsResult);

        $result = [
            array(
                'status' => marmot_encode($status),
                'items' => $itemsResult,
            )
        ];

        $this->assertEquals(
            $result,
            $this->view->getRelatedLinksByEncode($relatedLinks)
        );
    }

    public function testGetDataStatusByEncode()
    {

        $data['status'] = 1;

        $result = ['status' => marmot_encode($data['status'])];

        $this->assertEquals(
            $result,
            $this->view->getDataStatusByEncode($data)
        );
    }

    public function testGetItemsListByEncode()
    {
        $data = [
            'status' => 1,
            'category' => 1,
            'type' => 1,
            'description' => 'description',
        ];
        $list[] = $data;

        $result[] = [
            'status' => marmot_encode($data['status']),
            'category' => marmot_encode($data['category']),
            'type' => marmot_encode($data['type']),
            'description' => Filter::dhtmlspecialchars($data['description']),
        ];

        $this->assertEquals(
            $result,
            $this->view->getItemsListByEncode($list)
        );
    }

    public function testGetFormatContentByEncode()
    {
        $content = [
            //getItemsListByEncode
            'nav' => [],
            'footerNav' => [],
            'footerTwo' => [],
            'footerThree' => [],
            'footerBanner' => [],
            'headerSearch' => [],
            'animateWindow' => [],
            'headerBarLeft' => [],
            'leftFloatCard' => [],
            'specialColumn' => [],
            'headerBarRight' => [],
            'organizationGroup' => [],
            'rightToolBar' => [],
            //getDataStatusByEncode
            'silhouette' => [],
            'frameWindow' => [],
            'centerDialog' => [],
            'partyAndGovernmentOrgans' => [],
            'governmentErrorCorrection' => [],
            //marmot_encode
            'memorialStatus' => 0,
            //getRelatedLinksByEncode
            'relatedLinks' => []
        ];

        $this->view = $this->getMockBuilder(MockView::class)
            ->setMethods(['getDataStatusByEncode','getItemsListByEncode','getRelatedLinksByEncode'])
            ->getMock();
        $this->view->expects($this->exactly(13))->method('getItemsListByEncode')->willReturn([]);
        $this->view->expects($this->exactly(5))->method('getDataStatusByEncode')->willReturn([]);
        $this->view->expects($this->exactly(1))->method('getRelatedLinksByEncode')->willReturn([]);

        $result = [
            //getItemsListByEncode
            'nav' => [],
            'footerNav' => [],
            'footerTwo' => [],
            'footerThree' => [],
            'footerBanner' => [],
            'headerSearch' => [],
            'animateWindow' => [],
            'headerBarLeft' => [],
            'leftFloatCard' => [],
            'specialColumn' => [],
            'headerBarRight' => [],
            'organizationGroup' => [],
            'rightToolBar' => [],
            //getDataStatusByEncode
            'silhouette' => [],
            'frameWindow' => [],
            'centerDialog' => [],
            'partyAndGovernmentOrgans' => [],
            'governmentErrorCorrection' => [],
            //marmot_encode
            'memorialStatus' => marmot_encode(0),
            //getRelatedLinksByEncode
            'relatedLinks' => []
        ];

        $this->assertEquals($result, $this->view->getFormatContentByEncode($content));
    }
}
