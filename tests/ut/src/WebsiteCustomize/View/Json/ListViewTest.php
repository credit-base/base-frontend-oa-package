<?php
namespace Base\Package\WebsiteCustomize\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;
use Sdk\WebsiteCustomize\Model\WebsiteCustomize;

class ListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetWebsiteCustomize()
    {
        $this->assertIsArray($this->view->getWebsiteCustomize());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockListView::class)
            ->setMethods(['getTranslator', 'getWebsiteCustomize', 'encode','getCount'])
            ->getMock();

        $new = new WebsiteCustomize();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(WebsiteCustomizeTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                'id',
                'version',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getWebsiteCustomize')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
