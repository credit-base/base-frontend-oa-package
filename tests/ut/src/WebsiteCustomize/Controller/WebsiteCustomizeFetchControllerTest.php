<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;

use Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Sdk\Crew\Model\Crew;
use Sdk\WebsiteCustomize\Model\NullWebsiteCustomize;
use Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

use Base\Package\WebsiteCustomize\View\Json\View;
use Base\Package\WebsiteCustomize\View\Json\ListView;

/**
 * @SuppressWarnings(PHPMD)
 */
class WebsiteCustomizeFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
        Core::$container->set('crew', new Crew(1));
        Core::$container->set('purview.status', true);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new WebsiteCustomizeFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository',
            $this->stub->getRepository()
        );
    }

    // public function testFetchOneActionFailure()
    // {
    //     $id = 0;

    //     $result = $this->stub->fetchOneAction($id);
    //     $this->assertFalse($result);
    // }

    // public function testFetchOneActionNullFailure()
    // {
    //     $this->stub = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
    //         ->setMethods(
    //             [
    //                 'getRepository'
    //             ]
    //         )->getMock();

    //     $id = 1;
    //     $websiteCustomize = new NullWebsiteCustomize();

    //     $repository = $this->prophesize(WebsiteCustomizeRepository::class);
    //     $repository->scenario(Argument::exact(WebsiteCustomizeRepository::FETCH_ONE_MODEL_UN))
    //         ->shouldBeCalledTimes(1)
    //         ->willReturn($repository->reveal());

    //     $repository->fetchOne(Argument::exact($id))
    //         ->shouldBeCalledTimes(1)
    //         ->willReturn($websiteCustomize);

    //     $this->stub->expects($this->exactly(1))
    //         ->method('getRepository')
    //         ->willReturn($repository->reveal());

    //     $result = $this->stub->fetchOneAction($id);
    //     $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    //     $this->assertFalse($result);
    // }

    public function testFetchOneActionPurviewFail()
    {
        $this->websiteCustomize = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(['getRepository','render','checkUserHasPurview'])->getMock();
        $id = 1;
        $this->websiteCustomize->expects($this->exactly(1))
             ->method('checkUserHasPurview')
             ->willReturn(false);
        
        $result = $this->websiteCustomize->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterActionPurviewFail()
    {
        $this->websiteCustomize = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(['getRepository','render','checkUserHasPurview'])->getMock();
   
        $this->websiteCustomize->expects($this->exactly(1))
             ->method('checkUserHasPurview')
             ->willReturn(false);
        
        $result = $this->websiteCustomize->filterAction();
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods([
                'getRepository',
                'render',
                'checkUserHasPurview'
                ])->getMock();

        $id = 1;
        $websiteCustomize = new WebsiteCustomize($id);

        $this->stub->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->willReturn(true);

        $repository = $this->prophesize(WebsiteCustomizeRepository::class);
        $repository->scenario(
            Argument::exact(WebsiteCustomizeRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($websiteCustomize);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($websiteCustomize));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'render',
                    'checkUserHasPurview'
                ]
            )->getMock();

        $filter['status'] = 0;
        $filter['category'] = 1;
        $sort = ['-id'];
        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $this->stub->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $websiteCustomizeArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(WebsiteCustomizeRepository::class);
        $repository->scenario(Argument::exact(WebsiteCustomizeRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$websiteCustomizeArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ListView($websiteCustomizeArray, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $sort = '-id';
        $category = '1';
        $categoryEncode = marmot_encode($category);
        $status = 0;
        $statusEncode = marmot_encode($status);
        $version = '202109082038';

        $id = 1;
        $crew = new Crew($id);
        Core::$container->set('crew', $crew);

        $filter['status'] = $status;
        $filter['version'] = $version;
        $filter['category'] = $category;

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-id')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        
        $request->get(
            Argument::exact('category'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($categoryEncode);

        $request->get(
            Argument::exact('status'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($statusEncode);
        $request->get(
            Argument::exact('version'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($version);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, array($sort)], $result);
    }

    public function testCheckUserHasPurview()
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'checkUserHasWebsiteCustomizePurview',
                ]
            )->getMock();

        $this->stub->expects($this->any())
            ->method('checkUserHasWebsiteCustomizePurview')
            ->willReturn(true);
     
        $result = $this->stub->publicCheckUserHasPurview();
        $this->assertTrue($result);
    }

    public function testCustomizeSuccess()
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods([
                'getRepository',
                'render',
                'checkUserHasPurview'
                ])->getMock();

        $id = 1;
        $category = 'homePage';
        $websiteCustomize = new WebsiteCustomize($id);

        $this->stub->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->willReturn(true);

        $repository = $this->prophesize(WebsiteCustomizeRepository::class);

        $repository->customize(Argument::exact($category))->shouldBeCalledTimes(1)->willReturn($websiteCustomize);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($websiteCustomize));

        $result = $this->stub->customize($category);
        $this->assertTrue($result);
    }

    // public function testCustomizeNullFailure()
    // {
    //     $this->stub = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
    //         ->setMethods(
    //             [
    //                 'getRepository'
    //             ]
    //         )->getMock();

    //     $category = 'homePage';

    //     $websiteCustomize = new NullWebsiteCustomize();

    //     $repository = $this->prophesize(WebsiteCustomizeRepository::class);

    //     $repository->customize(Argument::exact($category))
    //         ->shouldBeCalledTimes(1)
    //         ->willReturn($websiteCustomize);

    //     $this->stub->expects($this->exactly(1))
    //         ->method('getRepository')
    //         ->willReturn($repository->reveal());

    //     $result = $this->stub->customize($category);
        
    //     $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    //     $this->assertFalse($result);
    // }

    public function testCustomizePurviewFail()
    {
        $this->websiteCustomize = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(['displayError','checkUserHasPurview'])->getMock();

        $category = 'homePage';
        $this->websiteCustomize->expects($this->exactly(1))
             ->method('checkUserHasPurview')
             ->willReturn(false);
            
        $this->websiteCustomize->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);
        
        $result = $this->websiteCustomize->customize($category);
        $this->assertFalse($result);
    }
}
