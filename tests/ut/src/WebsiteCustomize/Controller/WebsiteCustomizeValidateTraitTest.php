<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Crew\Model\Crew;

use Sdk\WebsiteCustomize\WidgetRules\WebsiteCustomizeWidgetRules;

class WebsiteCustomizeValidateTraitTest extends TestCase
{
    use websiteCustomizeRequestDataTrait;

    private $trait;

    private $faker;

    public function setUp()
    {
        Core::$container->set('crew', new Crew(1));
        $this->trait = $this->getMockBuilder(MockWebsiteCustomizeValidateTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetWebsiteCustomizeWidgetRules()
    {
        $trait = new MockWebsiteCustomizeValidateTrait();
        $this->assertInstanceOf(
            'Sdk\WebsiteCustomize\WidgetRules\WebsiteCustomizeWidgetRules',
            $trait->publicGetWebsiteCustomizeWidgetRules()
        );
    }

    public function testValidateAddScenario()
    {
        $trait = $this->getMockBuilder(MockWebsiteCustomizeValidateTrait::class)
                           ->setMethods(
                               [
                                    'getWebsiteCustomizeWidgetRules',
                                ]
                           )
                           ->getMock();

        $data = $this->getTestRequestCommonData();

        $websiteCustomizeWidgetRules = $this->prophesize(WebsiteCustomizeWidgetRules::class);

        $websiteCustomizeWidgetRules->category(
            Argument::exact($data['category']),
            Argument::exact('category')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $websiteCustomizeWidgetRules->status(
            Argument::exact($data['status']),
            Argument::exact('status')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $websiteCustomizeWidgetRules->content(
            Argument::exact($data['category']),
            Argument::exact($data['content'])
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getWebsiteCustomizeWidgetRules')
            ->willReturn($websiteCustomizeWidgetRules->reveal());

        $result = $trait->publicValidateAddScenario(
            $data['category'],
            $data['status'],
            $data['content']
        );
       
        $this->assertTrue($result);
    }

    // public function testCheckUserHasWebsiteCustomizePurviewSuccess()
    // {
    //     $trait = $this->getMockBuilder(MockWebsiteCustomizeValidateTrait::class)
    //                        ->setMethods(
    //                            []
    //                        )
    //                        ->getMock();

    //     Core::$container->set('crew', new Crew(1));
        
    //     $result = $trait->publiccheckUserHasWebsiteCustomizePurview();

    //     $this->assertTrue($result);
    // }

    // public function testCheckUserHasWebsiteCustomizePurviewFail()
    // {
    //     $trait = $this->getMockBuilder(MockWebsiteCustomizeValidateTrait::class)
    //                        ->setMethods(
    //                            []
    //                        )
    //                        ->getMock();

    //     Core::$container->set('crew', new Crew(22));

    //     $result = $trait->publiccheckUserHasWebsiteCustomizePurview();

    //     $this->assertFalse($result);
    //     $this->assertEquals(PURVIEW_UNDEFINED, Core::getLastError()->getId());
    // }
}
