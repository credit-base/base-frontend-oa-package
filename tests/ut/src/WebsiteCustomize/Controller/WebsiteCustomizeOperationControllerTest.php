<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Crew\Model\Crew;
use Sdk\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand;
use Sdk\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand;

/**
 * @SuppressWarnings(PHPMD)
 */
class WebsiteCustomizeOperationControllerTest extends TestCase
{
    use WebsiteCustomizeRequestDataTrait;

    private $websiteCustomizeController;

    public function setUp()
    {
        $this->websiteCustomizeController =
            $this->getMockBuilder(MockWebsiteCustomizeOperationController::class)
                 ->setMethods(
                     [
                        'getRequest',
                        'displayError',
                        'displaySuccess',
                        'validateAddScenario',
                        'checkUserHasPurview'
                     ]
                 )->getMock();

        $this->request = $this->prophesize(Request::class);
        
        Core::$container->set('crew', new Crew(1));
        Core::$container->set('purview.status', true);
    }

    public function tearDown()
    {
        unset($this->websiteCustomizeController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $websiteCustomizeController = new WebsiteCustomizeOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $websiteCustomizeController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $websiteCustomizeController = new WebsiteCustomizeOperationController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $websiteCustomizeController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->websiteCustomizeController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->websiteCustomizeController->addView();
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testEditView()
    {
        $id = 0;
        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->websiteCustomizeController->editView($id);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testEditAction()
    {
        $id = 0;
        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->websiteCustomizeController->editAction($id);
        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        $this->websiteCustomizeController = $this->getMockBuilder(MockWebsiteCustomizeOperationController::class)
                ->setMethods([
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'validateAddScenario',
                    'checkUserHasPurview',
                    'getFormatContentByDecode'
                ])->getMock();
           
        $category = 1;
        $status = 1;
        $content = array();

        //预言
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('category'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($category));
        $request->post(Argument::exact('status'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($status));
        $request->post(Argument::exact('content'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($content);
        $this->websiteCustomizeController->expects($this->exactly(1))
        ->method('getRequest')->willReturn($request->reveal());

        $this->websiteCustomizeController->expects($this->any())
            ->method('getFormatContentByDecode')
            ->with($content)
            ->willReturn($content);

        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->willReturn(true);

        $this->websiteCustomizeController->expects($this->any())
            ->method('validateAddScenario')
            ->with(
                $category,
                $status,
                $content
            )->willReturn($result);

        if ($result == true) {
            $commandBus = $this->prophesize(CommandBus::class);
            $command = new AddWebsiteCustomizeCommand(
                $category,
                $status,
                $content
            );

            $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

            $this->websiteCustomizeController->expects($this->exactly(1))
                ->method('getCommandBus')
                ->willReturn($commandBus->reveal());
        }
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->websiteCustomizeController->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionCheckUserHasPurviewFailure()
    {
        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('checkUserHasPurview')
            ->willReturn(false);

        $result = $this->websiteCustomizeController->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->websiteCustomizeController->addAction();
        $this->assertTrue($result);
    }

    private function initialPublish(int $id, bool $result)
    {
        $this->websiteCustomizeController = $this->getMockBuilder(MockWebsiteCustomizeOperationController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new PublishWebsiteCustomizeCommand(
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testPublishFailure()
    {
        $id = 2;
        $idEncode = marmot_encode($id);

        $this->initialPublish($id, false);

        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->websiteCustomizeController->publish($idEncode);
        $this->assertFalse($result);
    }

    public function testPublishSuccess()
    {
        $id = 2;
        $idEncode = marmot_encode($id);

        $this->initialPublish($id, true);

        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->websiteCustomizeController->publish($idEncode);
        
        $this->assertTrue($result);
    }
    
    public function testGetItemsListByDecodeToCategory()
    {
        $encodeData = $this->getTestEncodeRequestCommonData();

        $data = $this->getTestRequestCommonData();

        $result = $this->websiteCustomizeController->getItemsListByDecode($encodeData['content']['rightToolBar']);
        $this->assertEquals($result, $data['content']['rightToolBar']);
    }

    public function testGetDataStatusByDecode()
    {
        $encodeData = $this->getTestEncodeRequestCommonData();

        $data = $this->getTestRequestCommonData();

        $result = $this->websiteCustomizeController->getDataStatusByDecode($encodeData['content']['frameWindow']);
        $this->assertEquals($result, $data['content']['frameWindow']);
    }

    public function testGetRelatedLinksByDecode()
    {
        $this->websiteCustomizeController = new MockWebsiteCustomizeOperationController();
        $encodeData = $this->getTestEncodeRequestCommonData();

        $data = $this->getTestRequestCommonData();
        $result = $this->websiteCustomizeController->getRelatedLinksByDecode($encodeData['content']['relatedLinks']);
        $this->assertEquals($result, $data['content']['relatedLinks']);
    }

    public function testCheckUserHasPurview()
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomizeOperationController::class)
            ->setMethods(
                [
                    'checkUserHasWebsiteCustomizePurview',
                ]
            )->getMock();

        $this->stub->expects($this->any())
            ->method('checkUserHasWebsiteCustomizePurview')
            ->willReturn(true);
     
        $result = $this->stub->publicCheckUserHasPurview();
        $this->assertTrue($result);
    }

    public function testGetFormatContentByDecode()
    {

        $content = ['nav' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'nav');

        $content = ['footerNav' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'footerNav');

        $content = ['footerTwo' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'footerTwo');

        $content = ['footerThree' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'footerThree');

        $content = ['footerBanner' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'footerBanner');

        $content = ['footerBanner' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'footerBanner');

        $content = ['headerSearch' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'headerSearch');

        $content = ['animateWindow' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'animateWindow');

        $content = ['rightToolBar' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'rightToolBar');

        $content = ['leftFloatCard' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'leftFloatCard');

        $content = ['headerBarLeft' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'headerBarLeft');

        $content = ['specialColumn' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'specialColumn');

        $content = ['headerBarRight' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'headerBarRight');

        $content = ['organizationGroup' => array()];
        $this->initGetFormatContentByDecodeItemsListByDecode($content, 'organizationGroup');

        $content = ['silhouette' => array()];
        $this->initGetFormatContentByDecodeDataStatusByDecode($content, 'silhouette');

        $content = ['frameWindow' => array()];
        $this->initGetFormatContentByDecodeDataStatusByDecode($content, 'frameWindow');

        $content = ['centerDialog' => array()];
        $this->initGetFormatContentByDecodeDataStatusByDecode($content, 'centerDialog');

        $content = ['partyAndGovernmentOrgans' => array()];
        $this->initGetFormatContentByDecodeDataStatusByDecode($content, 'partyAndGovernmentOrgans');

        $content = ['governmentErrorCorrection' => array()];
        $this->initGetFormatContentByDecodeDataStatusByDecode($content, 'governmentErrorCorrection');

        $content = ['memorialStatus' => 'MA'];
        $this->initGetFormatContentByDecodeMarmotDecode($content, 'memorialStatus');

        $content = ['relatedLinks' => array()];
        $this->initGetFormatContentByDecodeRelatedLinksByDecode($content, 'relatedLinks');
    }
    public function initGetFormatContentByDecodeItemsListByDecode($content, $key)
    {
        $this->websiteCustomizeController = $this->getMockBuilder(MockWebsiteCustomizeOperationController::class)
        ->setMethods(['getItemsListByDecode'])->getMock();
        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('getItemsListByDecode')
            ->with($content[$key])->willReturn($content[$key]);
        $this->assertEquals($content, $this->websiteCustomizeController->getFormatContentByDecode($content));
    }
    public function initGetFormatContentByDecodeDataStatusByDecode($content, $key)
    {
        $this->websiteCustomizeController = $this->getMockBuilder(MockWebsiteCustomizeOperationController::class)
            ->setMethods(['getDataStatusByDecode'])->getMock();
        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('getDataStatusByDecode')
            ->with($content[$key])->willReturn($content[$key]);
        $this->assertEquals($content, $this->websiteCustomizeController->getFormatContentByDecode($content));
    }
    public function initGetFormatContentByDecodeRelatedLinksByDecode($content, $key)
    {
        $this->websiteCustomizeController = $this->getMockBuilder(MockWebsiteCustomizeOperationController::class)
            ->setMethods(['getRelatedLinksByDecode'])->getMock();
        $this->websiteCustomizeController->expects($this->exactly(1))
            ->method('getRelatedLinksByDecode')
            ->with($content[$key])->willReturn($content[$key]);
        $this->assertEquals($content, $this->websiteCustomizeController->getFormatContentByDecode($content));
    }
    public function initGetFormatContentByDecodeMarmotDecode($content, $key)
    {
        $result[$key] = marmot_decode($content[$key]);

        $this->assertEquals($result, $this->websiteCustomizeController->getFormatContentByDecode($content));
    }

    public function testCheckUserHasWebsiteCustomizePurview()
    {
        Core::$container->get('crew')->setCategory(Crew::CATEGORY['SUPERTUBE']);
        $websiteCustomizeController = new MockWebsiteCustomizeOperationController();
        $this->assertTrue(
            $websiteCustomizeController->checkUserHasWebsiteCustomizePurview()
        );
    }
    public function testCheckUserHasWebsiteCustomizePurviewFalse()
    {
        Core::$container->get('crew')->setCategory(Crew::CATEGORY['USERGROUP_OPERATION']);
        $websiteCustomizeController = new MockWebsiteCustomizeOperationController();
        $this->assertFalse(
            $websiteCustomizeController->checkUserHasWebsiteCustomizePurview()
        );
    }
}
