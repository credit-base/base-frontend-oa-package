<?php
namespace Base\Package\WorkOrderTask\View\Json;

use PHPUnit\Framework\TestCase;

class WorkOrderTaskViewTraitTest extends TestCase
{
    private $trait;
    public function setUp()
    {
        $this->trait = new MockWorkOrderTaskViewTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetIdentifies()
    {
        $items = array(
            array(
                'identify' => 'identify'
            )
        );

        $result[] = $items[0]['identify'];

        $this->assertEquals($result, $this->trait->getIdentifiesPublic($items));
    }

    public function testGetResultsData()
    {
        $results = array(
            array(
                'addResults' => array('identify'),
                'delResults' => array('identify'),
                'diffResults' => array('identify'),
            )
        );

        $baseItems = [
            array('identify' => 'identify')
        ];

        $feedbackRecords = array(
            array(
                'items' =>array(
                    array('identify' => 'identify')
                )
            )
        );

        $addResultData = array(
            array(
                array(
                    'feedbackType' => 'add',
                    'identify' => 'identify'
                )
            )
        );
        $delResultData = array(
            array(
                array(
                'feedbackType' => 'delete',
                'identify' => 'identify',
                )
            )
        );
        $diffResultData = array(
            array(
                array(
                'feedbackType' => 'edit',
                'latestRelease' => 'identify',
                'identify' => 'identify'
                )
            )
        );

        $resultsData[] = array_merge($addResultData, $delResultData, $diffResultData);

        $this->assertEquals(
            $resultsData,
            $this->trait->getResultsDataPublic($results, $baseItems, $feedbackRecords)
        );
    }

    public function testCompare()
    {
        $baseItems = [array('identify' => 'identify')];

        $feedbackRecords = array(
                'items' =>array(
                    array('identify' => 'identify')
                )
        );
        $this->assertEquals(
            array(
                'addResults' => array(),
                'delResults' => array(),
                'diffResults' => array(),
            ),
            $this->trait->comparePublic($baseItems, $feedbackRecords)
        );
    }
    public function testCompare2()
    {
        $baseItems = array(
            array(
                'identify'=>'identify',
                'items' => array(
                    'identify'=>'identify',
                    'id' => 2,
                )
            )
        );

        $feedbackRecords = array(
            'items' =>array(
                array(
                    'identify' => 'identify',
                    'items' => array(
                        'identify'=>'identify',
                        'id' => 1,
                    )
                )
            )
        );
        $this->assertEquals(
            array(
                'addResults' => array(),
                'delResults' => array(),
                'diffResults' => array(
                    'identify' => array(
                        'items' => array(
                            'identify' => 'identify',
                            'id' => '1',
                        )
                    )
                ),
            ),
            $this->trait->comparePublic($baseItems, $feedbackRecords)
        );
    }
    public function testCompare3()
    {
        $baseItems = array(
            array(
                'identify'=>'identify',
                'items' => array(
                    'identify'=>'identify',
                )
            )
        );

        $feedbackRecords = array(
            'items' =>array(
                array(
                    'identify' => 'identify',
                    'items' => array(
                        'identify'=>'identify',
                        'id' => 1,
                    )
                )
            )
        );
        $this->assertEquals(
            array(
                'addResults' => array(),
                'delResults' => array(),
                'diffResults' => array(
                    'identify' => array(
                        'items' => array(
                            'identify' => 'identify',
                            'id' => '1',
                        )
                    )
                ),
            ),
            $this->trait->comparePublic($baseItems, $feedbackRecords)
        );
    }

    public function testCompareResult()
    {
        $this->trait = $this->getMockBuilder(MockWorkOrderTaskViewTrait::class)
            ->setMethods([
                'compare','getResultsData'
            ])->getMock();

        $baseItems = array();
        $result = array();
        $compareResult = array();
        $feedbackRecord = array(
            'id' => 1
        );
        $feedbackRecords = array(
            $feedbackRecord
        );

        $this->trait->expects($this->exactly(1))->method('compare')->with($baseItems, $feedbackRecord)
            ->willReturn($result);

        $this->trait->expects($this->exactly(1))->method('getResultsData')
            ->with(array($result), $baseItems, $feedbackRecords)
            ->willReturn($compareResult);


        $this->assertEquals($compareResult, $this->trait->compareResultPublic($baseItems, $feedbackRecords));
    }
}
