<?php
namespace Base\Package\WorkOrderTask\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\WorkOrderTask\Translator\ParentTaskTranslator;
use Sdk\WorkOrderTask\Model\ParentTask;

class ParentTaskListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockParentTaskListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetParentTask()
    {
        $this->assertIsArray($this->view->getParentTask());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\WorkOrderTask\Translator\ParentTaskTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockParentTaskListView::class)
            ->setMethods(['getTranslator', 'getParentTask', 'encode','getCount'])
            ->getMock();

        $new = new ParentTask();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(ParentTaskTranslator::class);
        $translator->objectToArray(
            $new,
            array(
                'id',
                'title',
                'ratio',
                'templateType',
                'status',
                'updateTime',
                'endTime',
                'template'
            )
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getParentTask')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
