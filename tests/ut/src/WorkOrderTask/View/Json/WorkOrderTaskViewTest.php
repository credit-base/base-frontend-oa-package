<?php
namespace Base\Package\WorkOrderTask\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\WorkOrderTask\Translator\WorkOrderTaskTranslator;
use Sdk\WorkOrderTask\Model\WorkOrderTask;

class WorkOrderTaskViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockWorkOrderTaskView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetWorkOrderTask()
    {
        $this->assertInstanceOf(
            'Sdk\WorkOrderTask\Model\WorkOrderTask',
            $this->view->getWorkOrderTask()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\WorkOrderTask\Translator\WorkOrderTaskTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockWorkOrderTaskView::class)
            ->setMethods(['getTranslator','getWorkOrderTask','encode','compareResult'])
            ->getMock();

        $news = new WorkOrderTask();
        $newsData = [
            'feedbackRecords' => [
                'id' => 1
            ],
            'template' => [
                'items' => []
            ]
        ];
        $feedbackRecords = $newsData['feedbackRecords'];
        $baseItems = $newsData['template']['items'];

        $compareResult = [];
        $newsData['compareResult'] = $compareResult;

        $translator = $this->prophesize(WorkOrderTaskTranslator::class);
        $translator->objectToArray($news)->shouldBeCalledTimes(1)->willReturn($newsData);


        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        $this->view->expects($this->exactly(1))->method('getWorkOrderTask')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('compareResult')
            ->with($baseItems, $feedbackRecords)
            ->willReturn($compareResult);
        $this->view->expects($this->exactly(1))->method('encode')->with($newsData);
        $this->assertNull($this->view->display());
    }
}
