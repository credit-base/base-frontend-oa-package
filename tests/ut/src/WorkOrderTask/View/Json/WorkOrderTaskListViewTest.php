<?php
namespace Base\Package\WorkOrderTask\View\Json;

use PHPUnit\Framework\TestCase;
use Sdk\WorkOrderTask\Translator\WorkOrderTaskTranslator;
use Sdk\WorkOrderTask\Model\WorkOrderTask;

class WorkOrderTaskListViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockWorkOrderTaskListView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetWorkOrderTask()
    {
        $this->assertIsArray($this->view->getWorkOrderTask());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\WorkOrderTask\Translator\WorkOrderTaskTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->view = $this->getMockBuilder(MockWorkOrderTaskListView::class)
            ->setMethods(['getTranslator', 'getWorkOrderTask', 'encode','getCount'])
            ->getMock();

        $new = new WorkOrderTask();
        $newData = [];
        $news = [$new];
        $count = 1;

        $translator = $this->prophesize(WorkOrderTaskTranslator::class);
        $translator->objectToArray(
            $new
        )->shouldBeCalledTimes(1)->willReturn($newData);
        $this->view->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->view->expects($this->exactly(1))->method('getWorkOrderTask')->willReturn($news);
        $this->view->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->view->expects($this->exactly(1))->method('encode')->with(
            [
                'total' => $count,
                'list' => [$newData]
            ]
        );

        $this->assertNull($this->view->display());
    }
}
