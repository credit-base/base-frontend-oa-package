<?php
namespace Base\Package\WorkOrderTask\Controller;

use Sdk\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Crew\Model\Crew;
use Sdk\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand;
use Sdk\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand;
use Sdk\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand;
use Sdk\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class WorkOrderTaskOperationControllerTest extends TestCase
{
    private $workOrderTaskStub;
    private $request;

    public function setUp()
    {
        $this->workOrderTaskStub = $this->getMockBuilder(MockWorkOrderTaskOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess'
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->workOrderTaskStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new WorkOrderTaskOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->workOrderTaskStub->getCommandBus()
        );
    }

    public function testGetWorkOrderTaskWidgetRule()
    {
        $this->assertInstanceof(
            'Sdk\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule',
            $this->workOrderTaskStub->getWorkOrderTaskWidgetRule()
        );
    }

    public function testValidateReasonScenario()
    {
        //初始化
        $this->workOrderTaskStub = $this->getMockBuilder(MockWorkOrderTaskOperationController::class)
            ->setMethods(
                [
                    'getWorkOrderTaskWidgetRule'
                ]
            )->getMock();

        $reason = 'reason';

        //预言
        $workOrderTaskWidgetRule = $this->prophesize(WorkOrderTaskWidgetRule::class);
        $workOrderTaskWidgetRule->reason($reason)->shouldBeCalledTimes(1)->willReturn(true);
       //绑定
        $this->workOrderTaskStub->expects($this->any())
            ->method('getWorkOrderTaskWidgetRule')
            ->willReturn($workOrderTaskWidgetRule->reveal());

        //验证
        $this->workOrderTaskStub->validateReasonScenario(
            $reason
        );
    }

    public function testRevokeSuccess()
    {
        $id = 0;

        $this->initialRevoke($id, true);

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->workOrderTaskStub->revoke($id);
        
        $this->assertTrue($result);
    }

    public function testRevokeFailure()
    {
        $id = 0;

        $this->initialRevoke($id, false);

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->workOrderTaskStub->revoke($id);
        $this->assertFalse($result);
    }

    private function initialRevoke(int $id, bool $result)
    {
        $this->workOrderTaskStub = $this->getMockBuilder(MockWorkOrderTaskOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displaySuccess',
                    'displayError',
                    'getCommandBus',
                    'validateReasonScenario'
                ]
            )->getMock();

        $reason = 'word';

        //预言
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('reason'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($reason);

        $this->workOrderTaskStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('validateReasonScenario')
            ->with($reason)->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new RevokeWorkOrderTaskCommand(
                    $id,
                    $reason
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testEndSuccess()
    {
        $id = 0;

        $this->initialEnd($id, true);

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->workOrderTaskStub->end($id);
        
        $this->assertTrue($result);
    }

    public function testEndFailure()
    {
        $id = 0;

        $this->initialEnd($id, false);

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->workOrderTaskStub->end($id);
        $this->assertFalse($result);
    }

    private function initialEnd(int $id, bool $result)
    {
        $this->workOrderTaskStub = $this->getMockBuilder(MockWorkOrderTaskOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'validateReasonScenario'
                ]
            )->getMock();

        $reason = 'reason';

        //预言
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('reason'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($reason);

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('validateReasonScenario')
            ->with(
                $reason
            )->willReturn(true);

        $this->workOrderTaskStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EndWorkOrderTaskCommand(
                    $id,
                    $reason
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testConfirmSuccess()
    {
        $id = 0;

        $this->initialConfirm($id, true);

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->workOrderTaskStub->confirm($id);
        
        $this->assertTrue($result);
    }

    public function testConfirmFailure()
    {
        $id = 0;

        $this->initialConfirm($id, false);

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->workOrderTaskStub->confirm($id);
        $this->assertFalse($result);
    }

    private function initialConfirm(int $id, bool $result)
    {
        $this->workOrderTaskStub = $this->getMockBuilder(MockWorkOrderTaskOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus'
                ]
            )->getMock();

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ConfirmWorkOrderTaskCommand(
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->workOrderTaskStub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testGetItemsDecode()
    {
        $type = 1;
        $isMasked = 1;
        $dimension = 1;
        $isNecessary = 1;
        $items = array(
            array(
                'type' => marmot_encode($type),
                'isMasked' => marmot_encode($isMasked),
                'dimension' => marmot_encode($dimension),
                'isNecessary' => marmot_encode($isNecessary),
            )
        );

        $result = array(
            array(
                'type' => $type,
                'isMasked' => $isMasked,
                'dimension' => $dimension,
                'isNecessary' => $isNecessary,
            )
        );

        $stub = new MockWorkOrderTaskOperationController();
        $this->assertEquals($result, $stub->getItemsDecode($items));
    }

    public function testFeedback()
    {
        $this->initFeedback(true);
        $this->initFeedback(false);
    }
    public function initFeedback($result)
    {
        $this->workOrderTaskStub = $this->getMockBuilder(MockWorkOrderTaskOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getItemsDecode',
                    'validateReasonScenario',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus'
                ]
            )->getMock();

        $id = 1;
        $reason = 'reason';
        $userGroup = 1;
        $isExistedTemplate = 0;
        $templateId = 1;
        $items = array();

        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('reason'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($reason);
        $request->post(Argument::exact('userGroup'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($userGroup));
        $request->post(Argument::exact('isExistedTemplate'), Argument::exact(0))
            ->shouldBeCalledTimes(1)->willReturn($isExistedTemplate);
        $request->post(Argument::exact('templateId'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($templateId));
        $request->post(Argument::exact('items'), Argument::exact([]))
            ->shouldBeCalledTimes(1)->willReturn($items);
        $this->workOrderTaskStub->expects($this->any())->method('getRequest')->willReturn($request->reveal());

        Core::$container->set('crew', new Crew(0));
        $feedbackRecords = array(
            array(
                'crew' => Core::$container->get('crew')->getId(),
                'userGroup' => $userGroup,
                'isExistedTemplate' => $isExistedTemplate,
                'templateId' => $templateId,
                'reason' => $reason,
                'items' => $items
            )
        );

        $this->workOrderTaskStub->expects($this->exactly(1))->method('validateReasonScenario')
            ->with($reason)->willReturn($result);

        if ($result) {
            $commandBus = $this->prophesize(CommandBus::class);
            $commandBus->send(
                Argument::exact(new FeedbackWorkOrderTaskCommand($feedbackRecords, $id))
            )->shouldBeCalledTimes(1)->willReturn(true);
            $this->workOrderTaskStub->expects($this->exactly(1))
                ->method('getCommandBus')
                ->willReturn($commandBus->reveal());

            $this->workOrderTaskStub->expects($this->exactly(1))
                ->method('displaySuccess');

            $this->assertTrue($this->workOrderTaskStub->feedback(marmot_encode($id)));
        }

        if (!$result) {
            $this->workOrderTaskStub->expects($this->exactly(1))
                ->method('displayError');

            $this->assertFalse($this->workOrderTaskStub->feedback(marmot_encode($id)));
        }
    }
}
