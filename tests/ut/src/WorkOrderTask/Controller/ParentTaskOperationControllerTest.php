<?php
namespace Base\Package\WorkOrderTask\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Sdk\WorkOrderTask\Command\ParentTask\AssignParentTaskCommand;
use Sdk\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;
use Sdk\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class ParentTaskOperationControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockParentTaskOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess'
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new ParentTaskOperationController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->stub->getCommandBus()
        );
    }

    public function testGetWorkOrderTaskWidgetRule()
    {
        $this->assertInstanceof(
            'Sdk\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule',
            $this->stub->getWorkOrderTaskWidgetRule()
        );
    }

    public function testAssignSuccess()
    {
        $this->initialAssign(true);

        $this->stub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->stub->assign();
        $this->assertTrue($result);
    }

    public function testAssignFailure()
    {
        $this->initialAssign(false);

        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->assign();
        $this->assertFalse($result);
    }

    /**
     * 指定 assign方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAssign(bool $result)
    {
        //初始化
        $this->stub = $this->getMockBuilder(MockParentTaskOperationController::class)
            ->setMethods(
                ['getRequest','displayError','displaySuccess','getCommandBus',
                'validateAssignScenario']
            )->getMock();

        $title = 'title';
        $description = 'identify';
        $endTime = '0000-00-00';
        $templateType = 1;
        $templateId = 1;
        $templateTypeCode = marmot_encode($templateType);
        $template = marmot_encode($templateId);
        $assignObjects = array();
        $attachment = array();

        //预言
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('title'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($title);
        $request->post(Argument::exact('endTime'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($endTime);
        $request->post(Argument::exact('templateType'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($templateTypeCode);
        $request->post(Argument::exact('template'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($template);
        $request->post(Argument::exact('assignObjects'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($assignObjects);
        $request->post(Argument::exact('attachment'), Argument::exact(array()))
            ->shouldBeCalledTimes(1)->willReturn($attachment);
        $request->post(Argument::exact('description'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($description);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AssignParentTaskCommand(
            $title,
            $description,
            $endTime,
            $attachment,
            $assignObjects,
            $templateType,
            $templateId
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        //绑定
        $this->stub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
        $this->stub->expects($this->exactly(1))
            ->method('getRequest')->willReturn($request->reveal());
        $this->stub->expects($this->exactly(1))
            ->method('validateAssignScenario')
            ->with(
                $title,
                $description,
                $attachment,
                $templateType
            )->willReturn(true);
    }

    public function testValidateAssignScenario()
    {
        //初始化
        $this->stub = $this->getMockBuilder(MockParentTaskOperationController::class)
            ->setMethods(
                [
                    'getWorkOrderTaskWidgetRule'
                ]
            )->getMock();

        $title = 'title';
        $description = 'description';
        $attachment = array('name' => '国203号文','identify' => 'identify.pdf');
        $templateType = 1;

        //预言
        $workOrderTaskWidgetRule = $this->prophesize(WorkOrderTaskWidgetRule::class);
        $workOrderTaskWidgetRule->title($title)->shouldBeCalledTimes(1)->willReturn(true);
        $workOrderTaskWidgetRule->description($description)->shouldBeCalledTimes(1)->willReturn(true);
        $workOrderTaskWidgetRule->attachments($attachment)->shouldBeCalledTimes(1)->willReturn(true);
        $workOrderTaskWidgetRule->formatNumeric($templateType)->shouldBeCalledTimes(1)->willReturn(true);
        //绑定
        $this->stub->expects($this->any())
            ->method('getWorkOrderTaskWidgetRule')
            ->willReturn($workOrderTaskWidgetRule->reveal());

        //验证
        $this->stub->validateAssignScenario(
            $title,
            $description,
            $attachment,
            $templateType
        );
    }

    public function testRevokeSuccess()
    {
        $id = 0;

        $this->initialRevoke($id, true);

        $this->stub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->stub->revoke($id);
        
        $this->assertTrue($result);
    }

    public function testRevokeFailure()
    {
        $id = 0;

        $this->initialRevoke($id, false);

        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->revoke($id);
        $this->assertFalse($result);
    }
    
    public function testValidateReasonScenario()
    {
        //初始化
        $this->stub = $this->getMockBuilder(MockParentTaskOperationController::class)
            ->setMethods(
                [
                    'getWorkOrderTaskWidgetRule'
                ]
            )->getMock();

        $reason = 'reason';

        //预言
        $workOrderTaskWidgetRule = $this->prophesize(WorkOrderTaskWidgetRule::class);
        $workOrderTaskWidgetRule->reason($reason)->shouldBeCalledTimes(1)->willReturn(true);
       //绑定
        $this->stub->expects($this->any())
            ->method('getWorkOrderTaskWidgetRule')
            ->willReturn($workOrderTaskWidgetRule->reveal());

        //验证
        $this->stub->validateReasonScenario(
            $reason
        );
    }

    private function initialRevoke(int $id, bool $result)
    {
        $this->stub = $this->getMockBuilder(MockParentTaskOperationController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommandBus',
                    'displaySuccess',
                    'displayError',
                    'validateReasonScenario'
                ]
            )->getMock();

        $reason = 'word';

        //预言
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('reason'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($reason);

        $this->stub->expects($this->exactly(1))
            ->method('validateReasonScenario')
            ->with(
                $reason
            )->willReturn(true);
        
        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new RevokeParentTaskCommand(
                    $id,
                    $reason
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testGetAssignObjectCode()
    {
        $this->stub = new MockParentTaskOperationController();
        $assignObjects = [
            'MA'
        ];

        $result = [
            1
        ];

        $this->assertEquals($result, $this->stub->getAssignObjectCode($assignObjects));
    }
}
