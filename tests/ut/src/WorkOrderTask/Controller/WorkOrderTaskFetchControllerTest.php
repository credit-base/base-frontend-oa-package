<?php
namespace Base\Package\WorkOrderTask\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\WorkOrderTask\Model\WorkOrderTask;
use Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

use Base\Package\WorkOrderTask\View\Json\WorkOrderTaskListView;
use Base\Package\WorkOrderTask\View\Json\WorkOrderTaskView;

class WorkOrderTaskFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockWorkOrderTaskFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new WorkOrderTaskFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository',
            $this->stub->getRepository()
        );
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockWorkOrderTaskFetchController::class)
            ->setMethods(
                [
                    'getPageAndSize',
                    'filterFormatChange',
                    'getRequest',
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $request = $this->prophesize(Request::class);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $page = 1;
        $size = 10;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $filter = array();
        $sort = ['-updateTime'];
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $list = array(1,2);
        $count = 2;
        $repository = $this->prophesize(WorkOrderTaskRepository::class);
        $repository->scenario(Argument::exact(WorkOrderTaskRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new WorkOrderTaskListView($list, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->stub = $this->getMockBuilder(MockWorkOrderTaskFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new WorkOrderTask($id);

        $repository = $this->prophesize(WorkOrderTaskRepository::class);
        $repository->scenario(Argument::exact(WorkOrderTaskRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new WorkOrderTaskView($data));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function initFilterFormatChange($type)
    {
        $this->stub = $this->getMockBuilder(MockWorkOrderTaskFetchController::class)
            ->setMethods(['getRequest'])->getMock();

        //初始化
        $sort = '-updateTime';
        $parentTask = 1;
        $parentTaskCode = marmot_encode($parentTask);
        $assignObject = 1;
        $assignObjectCode = marmot_encode($assignObject);
        $title = 'title';
        //$type = MockWorkOrderTaskFetchController::TYPE['PARENT_TASK'];

        $request = $this->prophesize(Request::class);
        $request->get(Argument::exact('sort'), Argument::exact('-updateTime'))
            ->shouldBeCalledTimes(1)->willReturn($sort);
        $request->get(Argument::exact('title'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($title);
        $request->get(Argument::exact('type'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn(marmot_encode($type));
        $request->get(Argument::exact('parentTask'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($parentTaskCode);
        $request->get(Argument::exact('assignObject'), Argument::exact(''))
            ->shouldBeCalledTimes(1)->willReturn($assignObjectCode);

        $this->stub->expects($this->exactly(1))
            ->method('getRequest')->willReturn($request->reveal());

        $filter = [
            'title'=>$title,
            'assignObject'=> $assignObject
        ];

        if ($type == MockWorkOrderTaskFetchController::TYPE['ASSIGN_OBJECT']) {
            $sort = '-endTime';
            $request->get(Argument::exact('sort'), Argument::exact('-endTime'))
                ->shouldBeCalledTimes(1)->willReturn($sort);
            $filter['status'] = '0,2,3,4';
        }
        if ($type == MockWorkOrderTaskFetchController::TYPE['PARENT_TASK']) {
            $filter['parentTask'] = $parentTask;
        }

        $expected = [
            $filter,
            [$sort]
        ];

        $result = $this->stub->filterFormatChange();
        $this->assertEquals($expected, $result);
    }

    public function testFilterFormatChange()
    {
        $this->initFilterFormatChange(MockWorkOrderTaskFetchController::TYPE['ASSIGN_OBJECT']);
        $this->initFilterFormatChange(MockWorkOrderTaskFetchController::TYPE['PARENT_TASK']);
    }
}
