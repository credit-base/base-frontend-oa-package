<?php
namespace Base\Package\WorkOrderTask\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\WorkOrderTask\Model\ParentTask;
use Sdk\WorkOrderTask\Repository\ParentTaskRepository;

use Base\Package\WorkOrderTask\View\Json\ParentTaskListView;

class ParentTaskFetchControllerTest extends TestCase
{
    private $stub;
    private $request;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockParentTaskFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new ParentTaskFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\WorkOrderTask\Repository\ParentTaskRepository',
            $this->stub->getRepository()
        );
    }

    public function testFilterActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockParentTaskFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getPageAndSize',
                    'getRepository',
                    'filterFormatChange',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $request = $this->prophesize(Request::class);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $page = 2;
        $size = 20;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$size, $page]);

        $sort = ['-updateTime'];
        $filter = array();
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with()
            ->willReturn([$filter, $sort]);

        $list = array(1,2);
        $count = 2;
        $repository = $this->prophesize(ParentTaskRepository::class);
        $repository->scenario(Argument::exact(ParentTaskRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new ParentTaskListView($list, $count));

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;
        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockParentTaskFetchController::class)
            ->setMethods(['getRequest'])->getMock();

        $request = $this->prophesize(Request::class);
        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $sort = '-endTime';
        $title = 'title';
        $request->get(
            Argument::exact('sort'),
            Argument::exact('-endTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        $request->get(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($title);

        $result = $this->stub->filterFormatChange();
        $this->assertEquals([
            ['title' => $title],
            [$sort],
        ], $result);
    }
}
